﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
struct EnvoyTerminatorSink_t1090;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
struct  EnvoyTerminatorSink_t1090  : public Object_t
{
};
struct EnvoyTerminatorSink_t1090_StaticFields{
	// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink System.Runtime.Remoting.Messaging.EnvoyTerminatorSink::Instance
	EnvoyTerminatorSink_t1090 * ___Instance_0;
};
