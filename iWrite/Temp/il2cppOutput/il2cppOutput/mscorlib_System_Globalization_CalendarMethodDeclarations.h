﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Globalization.Calendar
struct Calendar_t895;
// System.Int32[]
struct Int32U5BU5D_t494;
// System.String[]
struct StringU5BU5D_t197;
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"

// System.Void System.Globalization.Calendar::.ctor()
extern "C" void Calendar__ctor_m5010 (Calendar_t895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::CheckReadOnly()
extern "C" void Calendar_CheckReadOnly_m5011 (Calendar_t895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.Calendar::get_EraNames()
extern "C" StringU5BU5D_t197* Calendar_get_EraNames_m5012 (Calendar_t895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
