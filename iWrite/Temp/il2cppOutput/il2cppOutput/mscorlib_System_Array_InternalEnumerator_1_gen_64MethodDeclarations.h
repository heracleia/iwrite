﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.SByte>
struct InternalEnumerator_1_t1738;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11705_gshared (InternalEnumerator_1_t1738 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11705(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1738 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11705_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11706_gshared (InternalEnumerator_1_t1738 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11706(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1738 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11706_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11707_gshared (InternalEnumerator_1_t1738 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11707(__this, method) (( void (*) (InternalEnumerator_1_t1738 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11707_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11708_gshared (InternalEnumerator_1_t1738 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11708(__this, method) (( bool (*) (InternalEnumerator_1_t1738 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11708_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C" int8_t InternalEnumerator_1_get_Current_m11709_gshared (InternalEnumerator_1_t1738 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11709(__this, method) (( int8_t (*) (InternalEnumerator_1_t1738 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11709_gshared)(__this, method)
