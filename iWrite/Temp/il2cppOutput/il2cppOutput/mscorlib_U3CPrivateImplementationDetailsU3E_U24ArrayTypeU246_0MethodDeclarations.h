﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$640
struct U24ArrayTypeU24640_t1384;
struct U24ArrayTypeU24640_t1384_marshaled;

void U24ArrayTypeU24640_t1384_marshal(const U24ArrayTypeU24640_t1384& unmarshaled, U24ArrayTypeU24640_t1384_marshaled& marshaled);
void U24ArrayTypeU24640_t1384_marshal_back(const U24ArrayTypeU24640_t1384_marshaled& marshaled, U24ArrayTypeU24640_t1384& unmarshaled);
void U24ArrayTypeU24640_t1384_marshal_cleanup(U24ArrayTypeU24640_t1384_marshaled& marshaled);
