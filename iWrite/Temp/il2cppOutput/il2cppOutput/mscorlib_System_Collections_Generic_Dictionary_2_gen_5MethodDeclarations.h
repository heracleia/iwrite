﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct Dictionary_2_t358;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t295;
// System.Collections.Generic.ICollection`1<SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct ICollection_1_t1891;
// System.Object
struct Object_t;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_t198;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct KeyCollection_t1620;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct ValueCollection_t1621;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1488;
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct IDictionary_2_t303;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t310;
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
struct KeyValuePair_2U5BU5D_t1892;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>
struct IEnumerator_1_t367;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t559;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__11.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_10MethodDeclarations.h"
#define Dictionary_2__ctor_m1295(__this, method) (( void (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2__ctor_m8769_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m10515(__this, ___comparer, method) (( void (*) (Dictionary_2_t358 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m8770_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m10516(__this, ___dictionary, method) (( void (*) (Dictionary_2_t358 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m8772_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Int32)
#define Dictionary_2__ctor_m10517(__this, ___capacity, method) (( void (*) (Dictionary_2_t358 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m8774_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m10518(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t358 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m8776_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m10519(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t358 *, SerializationInfo_t310 *, StreamingContext_t311 , const MethodInfo*))Dictionary_2__ctor_m8778_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m10520(__this, method) (( Object_t* (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m8780_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m10521(__this, method) (( Object_t* (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m8782_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m10522(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t358 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m8784_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m10523(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t358 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m8786_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m10524(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t358 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m8788_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m10525(__this, ___key, method) (( bool (*) (Dictionary_2_t358 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m8790_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m10526(__this, ___key, method) (( void (*) (Dictionary_2_t358 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m8792_gshared)(__this, ___key, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m10527(__this, method) (( Object_t * (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m8794_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m10528(__this, method) (( bool (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m8796_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m10529(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t358 *, KeyValuePair_2_t365 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m8798_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m10530(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t358 *, KeyValuePair_2_t365 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m8800_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m10531(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t358 *, KeyValuePair_2U5BU5D_t1892*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m8802_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m10532(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t358 *, KeyValuePair_2_t365 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m8804_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m10533(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t358 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m8806_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m10534(__this, method) (( Object_t * (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m8808_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m10535(__this, method) (( Object_t* (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m8810_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m10536(__this, method) (( Object_t * (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m8812_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Count()
#define Dictionary_2_get_Count_m10537(__this, method) (( int32_t (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_get_Count_m8814_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Item(TKey)
#define Dictionary_2_get_Item_m10538(__this, ___key, method) (( GetDelegate_t198 * (*) (Dictionary_2_t358 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m8816_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m10539(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t358 *, String_t*, GetDelegate_t198 *, const MethodInfo*))Dictionary_2_set_Item_m8818_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m10540(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t358 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m8820_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m10541(__this, ___size, method) (( void (*) (Dictionary_2_t358 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m8822_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m10542(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t358 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m8824_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m10543(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t365  (*) (Object_t * /* static, unused */, String_t*, GetDelegate_t198 *, const MethodInfo*))Dictionary_2_make_pair_m8826_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m10544(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, GetDelegate_t198 *, const MethodInfo*))Dictionary_2_pick_key_m8828_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m10545(__this /* static, unused */, ___key, ___value, method) (( GetDelegate_t198 * (*) (Object_t * /* static, unused */, String_t*, GetDelegate_t198 *, const MethodInfo*))Dictionary_2_pick_value_m8830_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m10546(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t358 *, KeyValuePair_2U5BU5D_t1892*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m8832_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Resize()
#define Dictionary_2_Resize_m10547(__this, method) (( void (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_Resize_m8834_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Add(TKey,TValue)
#define Dictionary_2_Add_m10548(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t358 *, String_t*, GetDelegate_t198 *, const MethodInfo*))Dictionary_2_Add_m8836_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Clear()
#define Dictionary_2_Clear_m10549(__this, method) (( void (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_Clear_m8838_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m10550(__this, ___key, method) (( bool (*) (Dictionary_2_t358 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m8840_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m10551(__this, ___value, method) (( bool (*) (Dictionary_2_t358 *, GetDelegate_t198 *, const MethodInfo*))Dictionary_2_ContainsValue_m8842_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m10552(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t358 *, SerializationInfo_t310 *, StreamingContext_t311 , const MethodInfo*))Dictionary_2_GetObjectData_m8844_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m10553(__this, ___sender, method) (( void (*) (Dictionary_2_t358 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m8846_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Remove(TKey)
#define Dictionary_2_Remove_m10554(__this, ___key, method) (( bool (*) (Dictionary_2_t358 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m8848_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m10555(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t358 *, String_t*, GetDelegate_t198 **, const MethodInfo*))Dictionary_2_TryGetValue_m8850_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Keys()
#define Dictionary_2_get_Keys_m10556(__this, method) (( KeyCollection_t1620 * (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_get_Keys_m8852_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Values()
#define Dictionary_2_get_Values_m10557(__this, method) (( ValueCollection_t1621 * (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_get_Values_m8853_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m10558(__this, ___key, method) (( String_t* (*) (Dictionary_2_t358 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m8855_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m10559(__this, ___value, method) (( GetDelegate_t198 * (*) (Dictionary_2_t358 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m8857_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m10560(__this, ___pair, method) (( bool (*) (Dictionary_2_t358 *, KeyValuePair_2_t365 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m8859_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m10561(__this, method) (( Enumerator_t1622  (*) (Dictionary_2_t358 *, const MethodInfo*))Dictionary_2_GetEnumerator_m8861_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m10562(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t560  (*) (Object_t * /* static, unused */, String_t*, GetDelegate_t198 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m8863_gshared)(__this /* static, unused */, ___key, ___value, method)
