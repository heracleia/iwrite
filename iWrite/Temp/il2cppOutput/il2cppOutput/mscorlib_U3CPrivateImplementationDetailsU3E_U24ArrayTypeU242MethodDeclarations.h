﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$24
struct U24ArrayTypeU2424_t1367;
struct U24ArrayTypeU2424_t1367_marshaled;

void U24ArrayTypeU2424_t1367_marshal(const U24ArrayTypeU2424_t1367& unmarshaled, U24ArrayTypeU2424_t1367_marshaled& marshaled);
void U24ArrayTypeU2424_t1367_marshal_back(const U24ArrayTypeU2424_t1367_marshaled& marshaled, U24ArrayTypeU2424_t1367& unmarshaled);
void U24ArrayTypeU2424_t1367_marshal_cleanup(U24ArrayTypeU2424_t1367_marshaled& marshaled);
