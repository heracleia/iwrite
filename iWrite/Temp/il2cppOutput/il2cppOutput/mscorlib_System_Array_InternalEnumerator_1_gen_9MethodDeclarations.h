﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>
struct InternalEnumerator_1_t1454;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m8451_gshared (InternalEnumerator_1_t1454 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m8451(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1454 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8451_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8452_gshared (InternalEnumerator_1_t1454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8452(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1454 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8452_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m8453_gshared (InternalEnumerator_1_t1454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m8453(__this, method) (( void (*) (InternalEnumerator_1_t1454 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8453_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m8454_gshared (InternalEnumerator_1_t1454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m8454(__this, method) (( bool (*) (InternalEnumerator_1_t1454 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8454_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern "C" GcAchievementData_t224  InternalEnumerator_1_get_Current_m8455_gshared (InternalEnumerator_1_t1454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m8455(__this, method) (( GcAchievementData_t224  (*) (InternalEnumerator_1_t1454 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8455_gshared)(__this, method)
