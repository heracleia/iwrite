﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// Metadata Definition UnityEngine.SocialPlatforms.IAchievementDescription
extern TypeInfo IAchievementDescription_t1804_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IAchievementDescription_t1804_0_0_0;
extern const Il2CppType IAchievementDescription_t1804_1_0_0;
struct IAchievementDescription_t1804;
const Il2CppTypeDefinitionMetadata IAchievementDescription_t1804_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IAchievementDescription_t1804_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &IAchievementDescription_t1804_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievementDescription_t1804_0_0_0/* byval_arg */
	, &IAchievementDescription_t1804_1_0_0/* this_arg */
	, &IAchievementDescription_t1804_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IScore
extern TypeInfo IScore_t236_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IScore_t236_0_0_0;
extern const Il2CppType IScore_t236_1_0_0;
struct IScore_t236;
const Il2CppTypeDefinitionMetadata IScore_t236_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IScore_t236_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IScore"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &IScore_t236_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IScore_t236_0_0_0/* byval_arg */
	, &IScore_t236_1_0_0/* this_arg */
	, &IScore_t236_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserScope
extern TypeInfo UserScope_t244_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScopeMethodDeclarations.h"
static const EncodedMethodIndex UserScope_t244_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
extern const Il2CppType IFormattable_t1406_0_0_0;
extern const Il2CppType IConvertible_t1409_0_0_0;
extern const Il2CppType IComparable_t1408_0_0_0;
static Il2CppInterfaceOffsetPair UserScope_t244_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserScope_t244_0_0_0;
extern const Il2CppType UserScope_t244_1_0_0;
extern const Il2CppType Enum_t305_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t320_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata UserScope_t244_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserScope_t244_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, UserScope_t244_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1021/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UserScope_t244_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserScope_t244_0_0_0/* byval_arg */
	, &UserScope_t244_1_0_0/* this_arg */
	, &UserScope_t244_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserScope_t244)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserScope_t244)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.TimeScope
extern TypeInfo TimeScope_t245_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScopeMethodDeclarations.h"
static const EncodedMethodIndex TimeScope_t245_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair TimeScope_t245_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TimeScope_t245_0_0_0;
extern const Il2CppType TimeScope_t245_1_0_0;
const Il2CppTypeDefinitionMetadata TimeScope_t245_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TimeScope_t245_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, TimeScope_t245_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1024/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TimeScope_t245_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TimeScope_t245_0_0_0/* byval_arg */
	, &TimeScope_t245_1_0_0/* this_arg */
	, &TimeScope_t245_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeScope_t245)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeScope_t245)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
// Metadata Definition UnityEngine.SocialPlatforms.Range
extern TypeInfo Range_t238_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_RangeMethodDeclarations.h"
static const EncodedMethodIndex Range_t238_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Range_t238_0_0_0;
extern const Il2CppType Range_t238_1_0_0;
extern const Il2CppType ValueType_t757_0_0_0;
const Il2CppTypeDefinitionMetadata Range_t238_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Range_t238_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1028/* fieldStart */
	, 1143/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Range_t238_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Range"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &Range_t238_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Range_t238_0_0_0/* byval_arg */
	, &Range_t238_1_0_0/* this_arg */
	, &Range_t238_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Range_t238)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Range_t238)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Range_t238 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILeaderboard
extern TypeInfo ILeaderboard_t276_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ILeaderboard_t276_0_0_0;
extern const Il2CppType ILeaderboard_t276_1_0_0;
struct ILeaderboard_t276;
const Il2CppTypeDefinitionMetadata ILeaderboard_t276_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1144/* methodStart */
	, -1/* eventStart */
	, 282/* propertyStart */

};
TypeInfo ILeaderboard_t276_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILeaderboard"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &ILeaderboard_t276_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILeaderboard_t276_0_0_0/* byval_arg */
	, &ILeaderboard_t276_1_0_0/* this_arg */
	, &ILeaderboard_t276_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderState.h"
// Metadata Definition UnityEngine.SliderState
extern TypeInfo SliderState_t246_il2cpp_TypeInfo;
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderStateMethodDeclarations.h"
static const EncodedMethodIndex SliderState_t246_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SliderState_t246_0_0_0;
extern const Il2CppType SliderState_t246_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SliderState_t246;
const Il2CppTypeDefinitionMetadata SliderState_t246_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SliderState_t246_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1030/* fieldStart */
	, 1148/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SliderState_t246_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderState"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SliderState_t246_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderState_t246_0_0_0/* byval_arg */
	, &SliderState_t246_1_0_0/* this_arg */
	, &SliderState_t246_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderState_t246)/* instance_size */
	, sizeof (SliderState_t246)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
// Metadata Definition UnityEngine.StackTraceUtility
extern TypeInfo StackTraceUtility_t247_il2cpp_TypeInfo;
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtilityMethodDeclarations.h"
static const EncodedMethodIndex StackTraceUtility_t247_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType StackTraceUtility_t247_0_0_0;
extern const Il2CppType StackTraceUtility_t247_1_0_0;
struct StackTraceUtility_t247;
const Il2CppTypeDefinitionMetadata StackTraceUtility_t247_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StackTraceUtility_t247_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1033/* fieldStart */
	, 1149/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StackTraceUtility_t247_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackTraceUtility"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &StackTraceUtility_t247_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StackTraceUtility_t247_0_0_0/* byval_arg */
	, &StackTraceUtility_t247_1_0_0/* this_arg */
	, &StackTraceUtility_t247_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackTraceUtility_t247)/* instance_size */
	, sizeof (StackTraceUtility_t247)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StackTraceUtility_t247_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityException.h"
// Metadata Definition UnityEngine.UnityException
extern TypeInfo UnityException_t248_il2cpp_TypeInfo;
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"
static const EncodedMethodIndex UnityException_t248_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
extern const Il2CppType ISerializable_t1426_0_0_0;
extern const Il2CppType _Exception_t2066_0_0_0;
static Il2CppInterfaceOffsetPair UnityException_t248_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityException_t248_0_0_0;
extern const Il2CppType UnityException_t248_1_0_0;
extern const Il2CppType Exception_t65_0_0_0;
struct UnityException_t248;
const Il2CppTypeDefinitionMetadata UnityException_t248_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityException_t248_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t65_0_0_0/* parent */
	, UnityException_t248_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1034/* fieldStart */
	, 1158/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityException_t248_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityException"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &UnityException_t248_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityException_t248_0_0_0/* byval_arg */
	, &UnityException_t248_1_0_0/* this_arg */
	, &UnityException_t248_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityException_t248)/* instance_size */
	, sizeof (UnityException_t248)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"
// Metadata Definition UnityEngine.SharedBetweenAnimatorsAttribute
extern TypeInfo SharedBetweenAnimatorsAttribute_t249_il2cpp_TypeInfo;
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttributeMethodDeclarations.h"
static const EncodedMethodIndex SharedBetweenAnimatorsAttribute_t249_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
extern const Il2CppType _Attribute_t2052_0_0_0;
static Il2CppInterfaceOffsetPair SharedBetweenAnimatorsAttribute_t249_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SharedBetweenAnimatorsAttribute_t249_0_0_0;
extern const Il2CppType SharedBetweenAnimatorsAttribute_t249_1_0_0;
extern const Il2CppType Attribute_t96_0_0_0;
struct SharedBetweenAnimatorsAttribute_t249;
const Il2CppTypeDefinitionMetadata SharedBetweenAnimatorsAttribute_t249_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SharedBetweenAnimatorsAttribute_t249_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, SharedBetweenAnimatorsAttribute_t249_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1162/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SharedBetweenAnimatorsAttribute_t249_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SharedBetweenAnimatorsAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SharedBetweenAnimatorsAttribute_t249_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 428/* custom_attributes_cache */
	, &SharedBetweenAnimatorsAttribute_t249_0_0_0/* byval_arg */
	, &SharedBetweenAnimatorsAttribute_t249_1_0_0/* this_arg */
	, &SharedBetweenAnimatorsAttribute_t249_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SharedBetweenAnimatorsAttribute_t249)/* instance_size */
	, sizeof (SharedBetweenAnimatorsAttribute_t249)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"
// Metadata Definition UnityEngine.StateMachineBehaviour
extern TypeInfo StateMachineBehaviour_t250_il2cpp_TypeInfo;
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviourMethodDeclarations.h"
static const EncodedMethodIndex StateMachineBehaviour_t250_VTable[11] = 
{
	124,
	125,
	126,
	127,
	427,
	428,
	429,
	430,
	431,
	432,
	433,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType StateMachineBehaviour_t250_0_0_0;
extern const Il2CppType StateMachineBehaviour_t250_1_0_0;
extern const Il2CppType ScriptableObject_t15_0_0_0;
struct StateMachineBehaviour_t250;
const Il2CppTypeDefinitionMetadata StateMachineBehaviour_t250_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ScriptableObject_t15_0_0_0/* parent */
	, StateMachineBehaviour_t250_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1163/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StateMachineBehaviour_t250_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StateMachineBehaviour"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &StateMachineBehaviour_t250_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StateMachineBehaviour_t250_0_0_0/* byval_arg */
	, &StateMachineBehaviour_t250_1_0_0/* this_arg */
	, &StateMachineBehaviour_t250_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StateMachineBehaviour_t250)/* instance_size */
	, sizeof (StateMachineBehaviour_t250)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditor.h"
// Metadata Definition UnityEngine.TextEditor
extern TypeInfo TextEditor_t254_il2cpp_TypeInfo;
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
extern const Il2CppType DblClickSnapping_t251_0_0_0;
extern const Il2CppType TextEditOp_t252_0_0_0;
static const Il2CppType* TextEditor_t254_il2cpp_TypeInfo__nestedTypes[2] =
{
	&DblClickSnapping_t251_0_0_0,
	&TextEditOp_t252_0_0_0,
};
static const EncodedMethodIndex TextEditor_t254_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextEditor_t254_0_0_0;
extern const Il2CppType TextEditor_t254_1_0_0;
struct TextEditor_t254;
const Il2CppTypeDefinitionMetadata TextEditor_t254_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextEditor_t254_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextEditor_t254_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1036/* fieldStart */
	, 1171/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextEditor_t254_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditor"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TextEditor_t254_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditor_t254_0_0_0/* byval_arg */
	, &TextEditor_t254_1_0_0/* this_arg */
	, &TextEditor_t254_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditor_t254)/* instance_size */
	, sizeof (TextEditor_t254)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextEditor_t254_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
// Metadata Definition UnityEngine.TextEditor/DblClickSnapping
extern TypeInfo DblClickSnapping_t251_il2cpp_TypeInfo;
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappingMethodDeclarations.h"
static const EncodedMethodIndex DblClickSnapping_t251_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair DblClickSnapping_t251_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DblClickSnapping_t251_1_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t326_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata DblClickSnapping_t251_DefinitionMetadata = 
{
	&TextEditor_t254_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DblClickSnapping_t251_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, DblClickSnapping_t251_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1060/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DblClickSnapping_t251_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DblClickSnapping"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Byte_t326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DblClickSnapping_t251_0_0_0/* byval_arg */
	, &DblClickSnapping_t251_1_0_0/* this_arg */
	, &DblClickSnapping_t251_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DblClickSnapping_t251)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DblClickSnapping_t251)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// Metadata Definition UnityEngine.TextEditor/TextEditOp
extern TypeInfo TextEditOp_t252_il2cpp_TypeInfo;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOpMethodDeclarations.h"
static const EncodedMethodIndex TextEditOp_t252_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair TextEditOp_t252_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextEditOp_t252_1_0_0;
const Il2CppTypeDefinitionMetadata TextEditOp_t252_DefinitionMetadata = 
{
	&TextEditor_t254_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextEditOp_t252_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, TextEditOp_t252_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1063/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextEditOp_t252_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditOp"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditOp_t252_0_0_0/* byval_arg */
	, &TextEditOp_t252_1_0_0/* this_arg */
	, &TextEditOp_t252_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditOp_t252)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextEditOp_t252)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 51/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// Metadata Definition UnityEngine.TextGenerationSettings
extern TypeInfo TextGenerationSettings_t155_il2cpp_TypeInfo;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
static const EncodedMethodIndex TextGenerationSettings_t155_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextGenerationSettings_t155_0_0_0;
extern const Il2CppType TextGenerationSettings_t155_1_0_0;
const Il2CppTypeDefinitionMetadata TextGenerationSettings_t155_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, TextGenerationSettings_t155_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1114/* fieldStart */
	, 1172/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextGenerationSettings_t155_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextGenerationSettings"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TextGenerationSettings_t155_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextGenerationSettings_t155_0_0_0/* byval_arg */
	, &TextGenerationSettings_t155_1_0_0/* this_arg */
	, &TextGenerationSettings_t155_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextGenerationSettings_t155)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextGenerationSettings_t155)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReference.h"
// Metadata Definition UnityEngine.TrackedReference
extern TypeInfo TrackedReference_t137_il2cpp_TypeInfo;
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReferenceMethodDeclarations.h"
static const EncodedMethodIndex TrackedReference_t137_VTable[4] = 
{
	280,
	125,
	281,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TrackedReference_t137_0_0_0;
extern const Il2CppType TrackedReference_t137_1_0_0;
struct TrackedReference_t137;
const Il2CppTypeDefinitionMetadata TrackedReference_t137_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackedReference_t137_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1131/* fieldStart */
	, 1175/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TrackedReference_t137_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackedReference"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TrackedReference_t137_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TrackedReference_t137_0_0_0/* byval_arg */
	, &TrackedReference_t137_1_0_0/* this_arg */
	, &TrackedReference_t137_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)TrackedReference_t137_marshal/* marshal_to_native_func */
	, (methodPointerType)TrackedReference_t137_marshal_back/* marshal_from_native_func */
	, (methodPointerType)TrackedReference_t137_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (TrackedReference_t137)/* instance_size */
	, sizeof (TrackedReference_t137)/* actualSize */
	, 0/* element_size */
	, sizeof(TrackedReference_t137_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
// Metadata Definition UnityEngine.Events.ArgumentCache
extern TypeInfo ArgumentCache_t255_il2cpp_TypeInfo;
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCacheMethodDeclarations.h"
static const EncodedMethodIndex ArgumentCache_t255_VTable[6] = 
{
	120,
	125,
	122,
	123,
	434,
	435,
};
extern const Il2CppType ISerializationCallbackReceiver_t2037_0_0_0;
static const Il2CppType* ArgumentCache_t255_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t2037_0_0_0,
};
static Il2CppInterfaceOffsetPair ArgumentCache_t255_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t2037_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ArgumentCache_t255_0_0_0;
extern const Il2CppType ArgumentCache_t255_1_0_0;
struct ArgumentCache_t255;
const Il2CppTypeDefinitionMetadata ArgumentCache_t255_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ArgumentCache_t255_InterfacesTypeInfos/* implementedInterfaces */
	, ArgumentCache_t255_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgumentCache_t255_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1132/* fieldStart */
	, 1178/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArgumentCache_t255_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentCache"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &ArgumentCache_t255_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgumentCache_t255_0_0_0/* byval_arg */
	, &ArgumentCache_t255_1_0_0/* this_arg */
	, &ArgumentCache_t255_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentCache_t255)/* instance_size */
	, sizeof (ArgumentCache_t255)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// Metadata Definition UnityEngine.Events.BaseInvokableCall
extern TypeInfo BaseInvokableCall_t256_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
static const EncodedMethodIndex BaseInvokableCall_t256_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType BaseInvokableCall_t256_0_0_0;
extern const Il2CppType BaseInvokableCall_t256_1_0_0;
struct BaseInvokableCall_t256;
const Il2CppTypeDefinitionMetadata BaseInvokableCall_t256_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseInvokableCall_t256_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BaseInvokableCall_t256_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseInvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &BaseInvokableCall_t256_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseInvokableCall_t256_0_0_0/* byval_arg */
	, &BaseInvokableCall_t256_1_0_0/* this_arg */
	, &BaseInvokableCall_t256_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseInvokableCall_t256)/* instance_size */
	, sizeof (BaseInvokableCall_t256)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
// Metadata Definition UnityEngine.Events.UnityEventCallState
extern TypeInfo UnityEventCallState_t257_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallStateMethodDeclarations.h"
static const EncodedMethodIndex UnityEventCallState_t257_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair UnityEventCallState_t257_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEventCallState_t257_0_0_0;
extern const Il2CppType UnityEventCallState_t257_1_0_0;
const Il2CppTypeDefinitionMetadata UnityEventCallState_t257_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEventCallState_t257_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, UnityEventCallState_t257_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1133/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEventCallState_t257_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventCallState"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventCallState_t257_0_0_0/* byval_arg */
	, &UnityEventCallState_t257_1_0_0/* this_arg */
	, &UnityEventCallState_t257_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventCallState_t257)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnityEventCallState_t257)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
// Metadata Definition UnityEngine.Events.PersistentCall
extern TypeInfo PersistentCall_t258_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCallMethodDeclarations.h"
static const EncodedMethodIndex PersistentCall_t258_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentCall_t258_0_0_0;
extern const Il2CppType PersistentCall_t258_1_0_0;
struct PersistentCall_t258;
const Il2CppTypeDefinitionMetadata PersistentCall_t258_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCall_t258_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1137/* fieldStart */
	, 1182/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PersistentCall_t258_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &PersistentCall_t258_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCall_t258_0_0_0/* byval_arg */
	, &PersistentCall_t258_1_0_0/* this_arg */
	, &PersistentCall_t258_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCall_t258)/* instance_size */
	, sizeof (PersistentCall_t258)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
// Metadata Definition UnityEngine.Events.PersistentCallGroup
extern TypeInfo PersistentCallGroup_t260_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroupMethodDeclarations.h"
static const EncodedMethodIndex PersistentCallGroup_t260_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentCallGroup_t260_0_0_0;
extern const Il2CppType PersistentCallGroup_t260_1_0_0;
struct PersistentCallGroup_t260;
const Il2CppTypeDefinitionMetadata PersistentCallGroup_t260_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCallGroup_t260_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1139/* fieldStart */
	, 1183/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PersistentCallGroup_t260_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCallGroup"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &PersistentCallGroup_t260_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCallGroup_t260_0_0_0/* byval_arg */
	, &PersistentCallGroup_t260_1_0_0/* this_arg */
	, &PersistentCallGroup_t260_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCallGroup_t260)/* instance_size */
	, sizeof (PersistentCallGroup_t260)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
// Metadata Definition UnityEngine.Events.InvokableCallList
extern TypeInfo InvokableCallList_t262_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallListMethodDeclarations.h"
static const EncodedMethodIndex InvokableCallList_t262_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCallList_t262_0_0_0;
extern const Il2CppType InvokableCallList_t262_1_0_0;
struct InvokableCallList_t262;
const Il2CppTypeDefinitionMetadata InvokableCallList_t262_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InvokableCallList_t262_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1140/* fieldStart */
	, 1184/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InvokableCallList_t262_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCallList"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &InvokableCallList_t262_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCallList_t262_0_0_0/* byval_arg */
	, &InvokableCallList_t262_1_0_0/* this_arg */
	, &InvokableCallList_t262_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCallList_t262)/* instance_size */
	, sizeof (InvokableCallList_t262)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// Metadata Definition UnityEngine.Events.UnityEventBase
extern TypeInfo UnityEventBase_t263_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
static const EncodedMethodIndex UnityEventBase_t263_VTable[6] = 
{
	120,
	125,
	122,
	436,
	437,
	438,
};
static const Il2CppType* UnityEventBase_t263_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t2037_0_0_0,
};
static Il2CppInterfaceOffsetPair UnityEventBase_t263_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t2037_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEventBase_t263_0_0_0;
extern const Il2CppType UnityEventBase_t263_1_0_0;
struct UnityEventBase_t263;
const Il2CppTypeDefinitionMetadata UnityEventBase_t263_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UnityEventBase_t263_InterfacesTypeInfos/* implementedInterfaces */
	, UnityEventBase_t263_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnityEventBase_t263_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1143/* fieldStart */
	, 1186/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEventBase_t263_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventBase"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityEventBase_t263_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventBase_t263_0_0_0/* byval_arg */
	, &UnityEventBase_t263_1_0_0/* this_arg */
	, &UnityEventBase_t263_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventBase_t263)/* instance_size */
	, sizeof (UnityEventBase_t263)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
// Metadata Definition UnityEngine.Events.UnityEvent
extern TypeInfo UnityEvent_t264_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"
static const EncodedMethodIndex UnityEvent_t264_VTable[6] = 
{
	120,
	125,
	122,
	436,
	437,
	438,
};
static Il2CppInterfaceOffsetPair UnityEvent_t264_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t2037_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_t264_0_0_0;
extern const Il2CppType UnityEvent_t264_1_0_0;
struct UnityEvent_t264;
const Il2CppTypeDefinitionMetadata UnityEvent_t264_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_t264_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t263_0_0_0/* parent */
	, UnityEvent_t264_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1147/* fieldStart */
	, 1191/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEvent_t264_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityEvent_t264_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_t264_0_0_0/* byval_arg */
	, &UnityEvent_t264_1_0_0/* this_arg */
	, &UnityEvent_t264_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_t264)/* instance_size */
	, sizeof (UnityEvent_t264)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`1
extern TypeInfo UnityEvent_1_t2045_il2cpp_TypeInfo;
static const EncodedMethodIndex UnityEvent_1_t2045_VTable[6] = 
{
	120,
	125,
	122,
	436,
	437,
	438,
};
static Il2CppInterfaceOffsetPair UnityEvent_1_t2045_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t2037_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_1_t2045_0_0_0;
extern const Il2CppType UnityEvent_1_t2045_1_0_0;
struct UnityEvent_1_t2045;
const Il2CppTypeDefinitionMetadata UnityEvent_1_t2045_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_1_t2045_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t263_0_0_0/* parent */
	, UnityEvent_1_t2045_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1148/* fieldStart */
	, 1192/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEvent_1_t2045_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityEvent_1_t2045_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_1_t2045_0_0_0/* byval_arg */
	, &UnityEvent_1_t2045_1_0_0/* this_arg */
	, &UnityEvent_1_t2045_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 9/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`2
extern TypeInfo UnityEvent_2_t2046_il2cpp_TypeInfo;
static const EncodedMethodIndex UnityEvent_2_t2046_VTable[6] = 
{
	120,
	125,
	122,
	436,
	437,
	438,
};
static Il2CppInterfaceOffsetPair UnityEvent_2_t2046_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t2037_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_2_t2046_0_0_0;
extern const Il2CppType UnityEvent_2_t2046_1_0_0;
struct UnityEvent_2_t2046;
const Il2CppTypeDefinitionMetadata UnityEvent_2_t2046_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_2_t2046_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t263_0_0_0/* parent */
	, UnityEvent_2_t2046_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1149/* fieldStart */
	, 1193/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEvent_2_t2046_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityEvent_2_t2046_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_2_t2046_0_0_0/* byval_arg */
	, &UnityEvent_2_t2046_1_0_0/* this_arg */
	, &UnityEvent_2_t2046_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 10/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`3
extern TypeInfo UnityEvent_3_t2047_il2cpp_TypeInfo;
static const EncodedMethodIndex UnityEvent_3_t2047_VTable[6] = 
{
	120,
	125,
	122,
	436,
	437,
	438,
};
static Il2CppInterfaceOffsetPair UnityEvent_3_t2047_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t2037_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_3_t2047_0_0_0;
extern const Il2CppType UnityEvent_3_t2047_1_0_0;
struct UnityEvent_3_t2047;
const Il2CppTypeDefinitionMetadata UnityEvent_3_t2047_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_3_t2047_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t263_0_0_0/* parent */
	, UnityEvent_3_t2047_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1150/* fieldStart */
	, 1194/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEvent_3_t2047_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityEvent_3_t2047_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_3_t2047_0_0_0/* byval_arg */
	, &UnityEvent_3_t2047_1_0_0/* this_arg */
	, &UnityEvent_3_t2047_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 11/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`4
extern TypeInfo UnityEvent_4_t2048_il2cpp_TypeInfo;
static const EncodedMethodIndex UnityEvent_4_t2048_VTable[6] = 
{
	120,
	125,
	122,
	436,
	437,
	438,
};
static Il2CppInterfaceOffsetPair UnityEvent_4_t2048_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t2037_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_4_t2048_0_0_0;
extern const Il2CppType UnityEvent_4_t2048_1_0_0;
struct UnityEvent_4_t2048;
const Il2CppTypeDefinitionMetadata UnityEvent_4_t2048_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_4_t2048_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t263_0_0_0/* parent */
	, UnityEvent_4_t2048_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1151/* fieldStart */
	, 1195/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEvent_4_t2048_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityEvent_4_t2048_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_4_t2048_0_0_0/* byval_arg */
	, &UnityEvent_4_t2048_1_0_0/* this_arg */
	, &UnityEvent_4_t2048_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 12/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialog.h"
// Metadata Definition UnityEngine.UserAuthorizationDialog
extern TypeInfo UserAuthorizationDialog_t265_il2cpp_TypeInfo;
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialogMethodDeclarations.h"
static const EncodedMethodIndex UserAuthorizationDialog_t265_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserAuthorizationDialog_t265_0_0_0;
extern const Il2CppType UserAuthorizationDialog_t265_1_0_0;
extern const Il2CppType MonoBehaviour_t116_0_0_0;
struct UserAuthorizationDialog_t265;
const Il2CppTypeDefinitionMetadata UserAuthorizationDialog_t265_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t116_0_0_0/* parent */
	, UserAuthorizationDialog_t265_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1152/* fieldStart */
	, 1196/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UserAuthorizationDialog_t265_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserAuthorizationDialog"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &UserAuthorizationDialog_t265_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 435/* custom_attributes_cache */
	, &UserAuthorizationDialog_t265_0_0_0/* byval_arg */
	, &UserAuthorizationDialog_t265_1_0_0/* this_arg */
	, &UserAuthorizationDialog_t265_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserAuthorizationDialog_t265)/* instance_size */
	, sizeof (UserAuthorizationDialog_t265)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// Metadata Definition UnityEngine.Internal.DefaultValueAttribute
extern TypeInfo DefaultValueAttribute_t266_il2cpp_TypeInfo;
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
static const EncodedMethodIndex DefaultValueAttribute_t266_VTable[4] = 
{
	439,
	125,
	440,
	123,
};
static Il2CppInterfaceOffsetPair DefaultValueAttribute_t266_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DefaultValueAttribute_t266_0_0_0;
extern const Il2CppType DefaultValueAttribute_t266_1_0_0;
struct DefaultValueAttribute_t266;
const Il2CppTypeDefinitionMetadata DefaultValueAttribute_t266_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultValueAttribute_t266_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, DefaultValueAttribute_t266_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1156/* fieldStart */
	, 1200/* methodStart */
	, -1/* eventStart */
	, 286/* propertyStart */

};
TypeInfo DefaultValueAttribute_t266_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultValueAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, NULL/* methods */
	, &DefaultValueAttribute_t266_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 436/* custom_attributes_cache */
	, &DefaultValueAttribute_t266_0_0_0/* byval_arg */
	, &DefaultValueAttribute_t266_1_0_0/* this_arg */
	, &DefaultValueAttribute_t266_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultValueAttribute_t266)/* instance_size */
	, sizeof (DefaultValueAttribute_t266)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// Metadata Definition UnityEngine.Internal.ExcludeFromDocsAttribute
extern TypeInfo ExcludeFromDocsAttribute_t267_il2cpp_TypeInfo;
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
static const EncodedMethodIndex ExcludeFromDocsAttribute_t267_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair ExcludeFromDocsAttribute_t267_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExcludeFromDocsAttribute_t267_0_0_0;
extern const Il2CppType ExcludeFromDocsAttribute_t267_1_0_0;
struct ExcludeFromDocsAttribute_t267;
const Il2CppTypeDefinitionMetadata ExcludeFromDocsAttribute_t267_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExcludeFromDocsAttribute_t267_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, ExcludeFromDocsAttribute_t267_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1204/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExcludeFromDocsAttribute_t267_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExcludeFromDocsAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, NULL/* methods */
	, &ExcludeFromDocsAttribute_t267_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 437/* custom_attributes_cache */
	, &ExcludeFromDocsAttribute_t267_0_0_0/* byval_arg */
	, &ExcludeFromDocsAttribute_t267_1_0_0/* this_arg */
	, &ExcludeFromDocsAttribute_t267_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExcludeFromDocsAttribute_t267)/* instance_size */
	, sizeof (ExcludeFromDocsAttribute_t267)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// Metadata Definition UnityEngine.Serialization.FormerlySerializedAsAttribute
extern TypeInfo FormerlySerializedAsAttribute_t268_il2cpp_TypeInfo;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
static const EncodedMethodIndex FormerlySerializedAsAttribute_t268_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair FormerlySerializedAsAttribute_t268_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FormerlySerializedAsAttribute_t268_0_0_0;
extern const Il2CppType FormerlySerializedAsAttribute_t268_1_0_0;
struct FormerlySerializedAsAttribute_t268;
const Il2CppTypeDefinitionMetadata FormerlySerializedAsAttribute_t268_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormerlySerializedAsAttribute_t268_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, FormerlySerializedAsAttribute_t268_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1157/* fieldStart */
	, 1205/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormerlySerializedAsAttribute_t268_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormerlySerializedAsAttribute"/* name */
	, "UnityEngine.Serialization"/* namespaze */
	, NULL/* methods */
	, &FormerlySerializedAsAttribute_t268_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 438/* custom_attributes_cache */
	, &FormerlySerializedAsAttribute_t268_0_0_0/* byval_arg */
	, &FormerlySerializedAsAttribute_t268_1_0_0/* this_arg */
	, &FormerlySerializedAsAttribute_t268_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormerlySerializedAsAttribute_t268)/* instance_size */
	, sizeof (FormerlySerializedAsAttribute_t268)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRules
extern TypeInfo TypeInferenceRules_t269_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRulesMethodDeclarations.h"
static const EncodedMethodIndex TypeInferenceRules_t269_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair TypeInferenceRules_t269_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TypeInferenceRules_t269_0_0_0;
extern const Il2CppType TypeInferenceRules_t269_1_0_0;
const Il2CppTypeDefinitionMetadata TypeInferenceRules_t269_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRules_t269_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, TypeInferenceRules_t269_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1158/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeInferenceRules_t269_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRules"/* name */
	, "UnityEngineInternal"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInferenceRules_t269_0_0_0/* byval_arg */
	, &TypeInferenceRules_t269_1_0_0/* this_arg */
	, &TypeInferenceRules_t269_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRules_t269)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeInferenceRules_t269)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRuleAttribute
extern TypeInfo TypeInferenceRuleAttribute_t270_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
static const EncodedMethodIndex TypeInferenceRuleAttribute_t270_VTable[4] = 
{
	253,
	125,
	254,
	441,
};
static Il2CppInterfaceOffsetPair TypeInferenceRuleAttribute_t270_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TypeInferenceRuleAttribute_t270_0_0_0;
extern const Il2CppType TypeInferenceRuleAttribute_t270_1_0_0;
struct TypeInferenceRuleAttribute_t270;
const Il2CppTypeDefinitionMetadata TypeInferenceRuleAttribute_t270_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRuleAttribute_t270_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, TypeInferenceRuleAttribute_t270_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1163/* fieldStart */
	, 1206/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeInferenceRuleAttribute_t270_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRuleAttribute"/* name */
	, "UnityEngineInternal"/* namespaze */
	, NULL/* methods */
	, &TypeInferenceRuleAttribute_t270_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 439/* custom_attributes_cache */
	, &TypeInferenceRuleAttribute_t270_0_0_0/* byval_arg */
	, &TypeInferenceRuleAttribute_t270_1_0_0/* this_arg */
	, &TypeInferenceRuleAttribute_t270_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRuleAttribute_t270)/* instance_size */
	, sizeof (TypeInferenceRuleAttribute_t270)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
// Metadata Definition UnityEngineInternal.GenericStack
extern TypeInfo GenericStack_t49_il2cpp_TypeInfo;
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
static const EncodedMethodIndex GenericStack_t49_VTable[16] = 
{
	120,
	125,
	122,
	123,
	442,
	443,
	444,
	445,
	443,
	444,
	446,
	445,
	442,
	447,
	448,
	449,
};
extern const Il2CppType IEnumerable_t302_0_0_0;
extern const Il2CppType ICloneable_t2056_0_0_0;
extern const Il2CppType ICollection_t569_0_0_0;
static Il2CppInterfaceOffsetPair GenericStack_t49_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 5},
	{ &ICollection_t569_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GenericStack_t49_0_0_0;
extern const Il2CppType GenericStack_t49_1_0_0;
extern const Il2CppType Stack_t271_0_0_0;
struct GenericStack_t49;
const Il2CppTypeDefinitionMetadata GenericStack_t49_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericStack_t49_InterfacesOffsets/* interfaceOffsets */
	, &Stack_t271_0_0_0/* parent */
	, GenericStack_t49_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1209/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GenericStack_t49_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericStack"/* name */
	, "UnityEngineInternal"/* namespaze */
	, NULL/* methods */
	, &GenericStack_t49_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericStack_t49_0_0_0/* byval_arg */
	, &GenericStack_t49_1_0_0/* this_arg */
	, &GenericStack_t49_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericStack_t49)/* instance_size */
	, sizeof (GenericStack_t49)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 16/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
