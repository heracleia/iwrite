﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Enumerator_t1633;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t1627;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m10753_gshared (Enumerator_t1633 * __this, Dictionary_2_t1627 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m10753(__this, ___host, method) (( void (*) (Enumerator_t1633 *, Dictionary_2_t1627 *, const MethodInfo*))Enumerator__ctor_m10753_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m10754_gshared (Enumerator_t1633 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m10754(__this, method) (( Object_t * (*) (Enumerator_t1633 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m10754_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void Enumerator_Dispose_m10755_gshared (Enumerator_t1633 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m10755(__this, method) (( void (*) (Enumerator_t1633 *, const MethodInfo*))Enumerator_Dispose_m10755_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool Enumerator_MoveNext_m10756_gshared (Enumerator_t1633 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m10756(__this, method) (( bool (*) (Enumerator_t1633 *, const MethodInfo*))Enumerator_MoveNext_m10756_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t1493  Enumerator_get_Current_m10757_gshared (Enumerator_t1633 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m10757(__this, method) (( KeyValuePair_2_t1493  (*) (Enumerator_t1633 *, const MethodInfo*))Enumerator_get_Current_m10757_gshared)(__this, method)
