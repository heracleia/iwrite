﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
struct KeyValuePair_2_t328;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.String>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m9270(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t328 *, String_t*, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m8869_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m1211(__this, method) (( String_t* (*) (KeyValuePair_2_t328 *, const MethodInfo*))KeyValuePair_2_get_Key_m8870_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m9271(__this, ___value, method) (( void (*) (KeyValuePair_2_t328 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m8871_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m1213(__this, method) (( String_t* (*) (KeyValuePair_2_t328 *, const MethodInfo*))KeyValuePair_2_get_Value_m8872_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m9272(__this, ___value, method) (( void (*) (KeyValuePair_2_t328 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m8873_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.String>::ToString()
#define KeyValuePair_2_ToString_m9273(__this, method) (( String_t* (*) (KeyValuePair_2_t328 *, const MethodInfo*))KeyValuePair_2_ToString_m8874_gshared)(__this, method)
