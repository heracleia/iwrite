﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t253;
// System.Collections.Generic.ICollection`1<UnityEngine.Event>
struct ICollection_1_t1907;
// System.Collections.Generic.ICollection`1<UnityEngine.TextEditor/TextEditOp>
struct ICollection_1_t1908;
// System.Object
struct Object_t;
// UnityEngine.Event
struct Event_t76;
struct Event_t76_marshaled;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct KeyCollection_t1677;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct ValueCollection_t1678;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Event>
struct IEqualityComparer_1_t1658;
// System.Collections.Generic.IDictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct IDictionary_2_t1909;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t310;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t1910;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>>
struct IEnumerator_1_t1911;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t559;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"
// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__15.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_19MethodDeclarations.h"
#define Dictionary_2__ctor_m10939(__this, method) (( void (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2__ctor_m10940_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m10941(__this, ___comparer, method) (( void (*) (Dictionary_2_t253 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m10942_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m10943(__this, ___dictionary, method) (( void (*) (Dictionary_2_t253 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m10944_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Int32)
#define Dictionary_2__ctor_m10945(__this, ___capacity, method) (( void (*) (Dictionary_2_t253 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m10946_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m10947(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t253 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m10948_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m10949(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t253 *, SerializationInfo_t310 *, StreamingContext_t311 , const MethodInfo*))Dictionary_2__ctor_m10950_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m10951(__this, method) (( Object_t* (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m10952_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m10953(__this, method) (( Object_t* (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m10954_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m10955(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t253 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m10956_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m10957(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t253 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m10958_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m10959(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t253 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m10960_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m10961(__this, ___key, method) (( bool (*) (Dictionary_2_t253 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m10962_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m10963(__this, ___key, method) (( void (*) (Dictionary_2_t253 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m10964_gshared)(__this, ___key, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m10965(__this, method) (( Object_t * (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m10966_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m10967(__this, method) (( bool (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m10968_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m10969(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t253 *, KeyValuePair_2_t1676 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m10970_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m10971(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t253 *, KeyValuePair_2_t1676 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m10972_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m10973(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t253 *, KeyValuePair_2U5BU5D_t1910*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m10974_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m10975(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t253 *, KeyValuePair_2_t1676 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m10976_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m10977(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t253 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m10978_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m10979(__this, method) (( Object_t * (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m10980_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m10981(__this, method) (( Object_t* (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m10982_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m10983(__this, method) (( Object_t * (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m10984_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Count()
#define Dictionary_2_get_Count_m10985(__this, method) (( int32_t (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_get_Count_m10986_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Item(TKey)
#define Dictionary_2_get_Item_m10987(__this, ___key, method) (( int32_t (*) (Dictionary_2_t253 *, Event_t76 *, const MethodInfo*))Dictionary_2_get_Item_m10988_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m10989(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t253 *, Event_t76 *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m10990_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m10991(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t253 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m10992_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m10993(__this, ___size, method) (( void (*) (Dictionary_2_t253 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m10994_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m10995(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t253 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m10996_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m10997(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1676  (*) (Object_t * /* static, unused */, Event_t76 *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m10998_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m10999(__this /* static, unused */, ___key, ___value, method) (( Event_t76 * (*) (Object_t * /* static, unused */, Event_t76 *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m11000_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m11001(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Event_t76 *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m11002_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m11003(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t253 *, KeyValuePair_2U5BU5D_t1910*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m11004_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Resize()
#define Dictionary_2_Resize_m11005(__this, method) (( void (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_Resize_m11006_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Add(TKey,TValue)
#define Dictionary_2_Add_m11007(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t253 *, Event_t76 *, int32_t, const MethodInfo*))Dictionary_2_Add_m11008_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Clear()
#define Dictionary_2_Clear_m11009(__this, method) (( void (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_Clear_m11010_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m11011(__this, ___key, method) (( bool (*) (Dictionary_2_t253 *, Event_t76 *, const MethodInfo*))Dictionary_2_ContainsKey_m11012_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m11013(__this, ___value, method) (( bool (*) (Dictionary_2_t253 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m11014_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m11015(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t253 *, SerializationInfo_t310 *, StreamingContext_t311 , const MethodInfo*))Dictionary_2_GetObjectData_m11016_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m11017(__this, ___sender, method) (( void (*) (Dictionary_2_t253 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m11018_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Remove(TKey)
#define Dictionary_2_Remove_m11019(__this, ___key, method) (( bool (*) (Dictionary_2_t253 *, Event_t76 *, const MethodInfo*))Dictionary_2_Remove_m11020_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m11021(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t253 *, Event_t76 *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m11022_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Keys()
#define Dictionary_2_get_Keys_m11023(__this, method) (( KeyCollection_t1677 * (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_get_Keys_m11024_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Values()
#define Dictionary_2_get_Values_m11025(__this, method) (( ValueCollection_t1678 * (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_get_Values_m11026_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m11027(__this, ___key, method) (( Event_t76 * (*) (Dictionary_2_t253 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m11028_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m11029(__this, ___value, method) (( int32_t (*) (Dictionary_2_t253 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m11030_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m11031(__this, ___pair, method) (( bool (*) (Dictionary_2_t253 *, KeyValuePair_2_t1676 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m11032_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m11033(__this, method) (( Enumerator_t1679  (*) (Dictionary_2_t253 *, const MethodInfo*))Dictionary_2_GetEnumerator_m11034_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m11035(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t560  (*) (Object_t * /* static, unused */, Event_t76 *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m11036_gshared)(__this /* static, unused */, ___key, ___value, method)
