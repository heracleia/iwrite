﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/PreviousInfo
struct PreviousInfo_t809;
struct PreviousInfo_t809_marshaled;

// System.Void Mono.Globalization.Unicode.SimpleCollator/PreviousInfo::.ctor(System.Boolean)
extern "C" void PreviousInfo__ctor_m4263 (PreviousInfo_t809 * __this, bool ___dummy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void PreviousInfo_t809_marshal(const PreviousInfo_t809& unmarshaled, PreviousInfo_t809_marshaled& marshaled);
void PreviousInfo_t809_marshal_back(const PreviousInfo_t809_marshaled& marshaled, PreviousInfo_t809& unmarshaled);
void PreviousInfo_t809_marshal_cleanup(PreviousInfo_t809_marshaled& marshaled);
