﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
struct Enumerator_t1542;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t152;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m9511_gshared (Enumerator_t1542 * __this, List_1_t152 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m9511(__this, ___l, method) (( void (*) (Enumerator_t1542 *, List_1_t152 *, const MethodInfo*))Enumerator__ctor_m9511_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m9512_gshared (Enumerator_t1542 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m9512(__this, method) (( Object_t * (*) (Enumerator_t1542 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m9512_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C" void Enumerator_Dispose_m9513_gshared (Enumerator_t1542 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m9513(__this, method) (( void (*) (Enumerator_t1542 *, const MethodInfo*))Enumerator_Dispose_m9513_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m9514_gshared (Enumerator_t1542 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m9514(__this, method) (( void (*) (Enumerator_t1542 *, const MethodInfo*))Enumerator_VerifyState_m9514_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m9515_gshared (Enumerator_t1542 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m9515(__this, method) (( bool (*) (Enumerator_t1542 *, const MethodInfo*))Enumerator_MoveNext_m9515_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t149  Enumerator_get_Current_m9516_gshared (Enumerator_t1542 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m9516(__this, method) (( UICharInfo_t149  (*) (Enumerator_t1542 *, const MethodInfo*))Enumerator_get_Current_m9516_gshared)(__this, method)
