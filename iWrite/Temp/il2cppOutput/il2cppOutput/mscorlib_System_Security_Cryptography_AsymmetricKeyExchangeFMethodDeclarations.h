﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
struct AsymmetricKeyExchangeFormatter_t1179;
// System.Byte[]
struct ByteU5BU5D_t102;

// System.Void System.Security.Cryptography.AsymmetricKeyExchangeFormatter::.ctor()
extern "C" void AsymmetricKeyExchangeFormatter__ctor_m6661 (AsymmetricKeyExchangeFormatter_t1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
