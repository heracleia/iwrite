﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
struct ShimEnumerator_t1592;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Dictionary_2_t1580;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m10074_gshared (ShimEnumerator_t1592 * __this, Dictionary_2_t1580 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m10074(__this, ___host, method) (( void (*) (ShimEnumerator_t1592 *, Dictionary_2_t1580 *, const MethodInfo*))ShimEnumerator__ctor_m10074_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m10075_gshared (ShimEnumerator_t1592 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m10075(__this, method) (( bool (*) (ShimEnumerator_t1592 *, const MethodInfo*))ShimEnumerator_MoveNext_m10075_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Entry()
extern "C" DictionaryEntry_t560  ShimEnumerator_get_Entry_m10076_gshared (ShimEnumerator_t1592 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m10076(__this, method) (( DictionaryEntry_t560  (*) (ShimEnumerator_t1592 *, const MethodInfo*))ShimEnumerator_get_Entry_m10076_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m10077_gshared (ShimEnumerator_t1592 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m10077(__this, method) (( Object_t * (*) (ShimEnumerator_t1592 *, const MethodInfo*))ShimEnumerator_get_Key_m10077_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m10078_gshared (ShimEnumerator_t1592 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m10078(__this, method) (( Object_t * (*) (ShimEnumerator_t1592 *, const MethodInfo*))ShimEnumerator_get_Value_m10078_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m10079_gshared (ShimEnumerator_t1592 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m10079(__this, method) (( Object_t * (*) (ShimEnumerator_t1592 *, const MethodInfo*))ShimEnumerator_get_Current_m10079_gshared)(__this, method)
