﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>
struct Enumerator_t1723;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t495;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m11633_gshared (Enumerator_t1723 * __this, Dictionary_2_t495 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m11633(__this, ___host, method) (( void (*) (Enumerator_t1723 *, Dictionary_2_t495 *, const MethodInfo*))Enumerator__ctor_m11633_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m11634_gshared (Enumerator_t1723 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m11634(__this, method) (( Object_t * (*) (Enumerator_t1723 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11634_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m11635_gshared (Enumerator_t1723 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m11635(__this, method) (( void (*) (Enumerator_t1723 *, const MethodInfo*))Enumerator_Dispose_m11635_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m11636_gshared (Enumerator_t1723 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m11636(__this, method) (( bool (*) (Enumerator_t1723 *, const MethodInfo*))Enumerator_MoveNext_m11636_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m11637_gshared (Enumerator_t1723 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m11637(__this, method) (( int32_t (*) (Enumerator_t1723 *, const MethodInfo*))Enumerator_get_Current_m11637_gshared)(__this, method)
