﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t395;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.CollectionBase
struct  CollectionBase_t453  : public Object_t
{
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t395 * ___list_0;
};
