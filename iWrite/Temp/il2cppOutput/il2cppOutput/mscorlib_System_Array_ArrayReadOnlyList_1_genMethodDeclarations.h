﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t1748;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t200;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1807;
// System.Exception
struct Exception_t65;

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m11791_gshared (ArrayReadOnlyList_1_t1748 * __this, ObjectU5BU5D_t200* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m11791(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t1748 *, ObjectU5BU5D_t200*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m11791_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m11792_gshared (ArrayReadOnlyList_1_t1748 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m11792(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1748 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m11792_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m11793_gshared (ArrayReadOnlyList_1_t1748 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m11793(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1748 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m11793_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m11794_gshared (ArrayReadOnlyList_1_t1748 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m11794(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t1748 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m11794_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m11795_gshared (ArrayReadOnlyList_1_t1748 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m11795(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t1748 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m11795_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m11796_gshared (ArrayReadOnlyList_1_t1748 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m11796(__this, method) (( bool (*) (ArrayReadOnlyList_1_t1748 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m11796_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m11797_gshared (ArrayReadOnlyList_1_t1748 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m11797(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1748 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m11797_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m11798_gshared (ArrayReadOnlyList_1_t1748 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m11798(__this, method) (( void (*) (ArrayReadOnlyList_1_t1748 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m11798_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m11799_gshared (ArrayReadOnlyList_1_t1748 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m11799(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1748 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m11799_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m11800_gshared (ArrayReadOnlyList_1_t1748 * __this, ObjectU5BU5D_t200* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m11800(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1748 *, ObjectU5BU5D_t200*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m11800_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m11801_gshared (ArrayReadOnlyList_1_t1748 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m11801(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t1748 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m11801_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m11802_gshared (ArrayReadOnlyList_1_t1748 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m11802(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t1748 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m11802_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m11803_gshared (ArrayReadOnlyList_1_t1748 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m11803(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1748 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m11803_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m11804_gshared (ArrayReadOnlyList_1_t1748 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m11804(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1748 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m11804_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m11805_gshared (ArrayReadOnlyList_1_t1748 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m11805(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1748 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m11805_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t65 * ArrayReadOnlyList_1_ReadOnlyError_m11806_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m11806(__this /* static, unused */, method) (( Exception_t65 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m11806_gshared)(__this /* static, unused */, method)
