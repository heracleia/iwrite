﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
struct ReceiveRecordAsyncResult_t693;
// System.IO.Stream
struct Stream_t692;
// System.Byte[]
struct ByteU5BU5D_t102;
// System.Object
struct Object_t;
// System.Exception
struct Exception_t65;
// System.Threading.WaitHandle
struct WaitHandle_t738;
// System.AsyncCallback
struct AsyncCallback_t45;

// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::.ctor(System.AsyncCallback,System.Object,System.Byte[],System.IO.Stream)
extern "C" void ReceiveRecordAsyncResult__ctor_m2958 (ReceiveRecordAsyncResult_t693 * __this, AsyncCallback_t45 * ___userCallback, Object_t * ___userState, ByteU5BU5D_t102* ___initialBuffer, Stream_t692 * ___record, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_Record()
extern "C" Stream_t692 * ReceiveRecordAsyncResult_get_Record_m2959 (ReceiveRecordAsyncResult_t693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_ResultingBuffer()
extern "C" ByteU5BU5D_t102* ReceiveRecordAsyncResult_get_ResultingBuffer_m2960 (ReceiveRecordAsyncResult_t693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_InitialBuffer()
extern "C" ByteU5BU5D_t102* ReceiveRecordAsyncResult_get_InitialBuffer_m2961 (ReceiveRecordAsyncResult_t693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncState()
extern "C" Object_t * ReceiveRecordAsyncResult_get_AsyncState_m2962 (ReceiveRecordAsyncResult_t693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncException()
extern "C" Exception_t65 * ReceiveRecordAsyncResult_get_AsyncException_m2963 (ReceiveRecordAsyncResult_t693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_CompletedWithError()
extern "C" bool ReceiveRecordAsyncResult_get_CompletedWithError_m2964 (ReceiveRecordAsyncResult_t693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncWaitHandle()
extern "C" WaitHandle_t738 * ReceiveRecordAsyncResult_get_AsyncWaitHandle_m2965 (ReceiveRecordAsyncResult_t693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_IsCompleted()
extern "C" bool ReceiveRecordAsyncResult_get_IsCompleted_m2966 (ReceiveRecordAsyncResult_t693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception,System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2967 (ReceiveRecordAsyncResult_t693 * __this, Exception_t65 * ___ex, ByteU5BU5D_t102* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception)
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2968 (ReceiveRecordAsyncResult_t693 * __this, Exception_t65 * ___ex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2969 (ReceiveRecordAsyncResult_t693 * __this, ByteU5BU5D_t102* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
