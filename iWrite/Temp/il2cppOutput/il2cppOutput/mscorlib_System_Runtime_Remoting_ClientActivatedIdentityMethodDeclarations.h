﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ClientActivatedIdentity
struct ClientActivatedIdentity_t1134;
// System.MarshalByRefObject
struct MarshalByRefObject_t434;

// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
extern "C" MarshalByRefObject_t434 * ClientActivatedIdentity_GetServerObject_m6476 (ClientActivatedIdentity_t1134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
