﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern "C" void AssetBundleCreateRequest__ctor_m0 ();
extern "C" void AssetBundleCreateRequest_get_assetBundle_m1 ();
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m2 ();
extern "C" void AssetBundleRequest__ctor_m3 ();
extern "C" void AssetBundleRequest_get_asset_m4 ();
extern "C" void AssetBundleRequest_get_allAssets_m5 ();
extern "C" void AssetBundle_LoadAsset_m6 ();
extern "C" void AssetBundle_LoadAsset_Internal_m7 ();
extern "C" void AssetBundle_LoadAssetWithSubAssets_Internal_m8 ();
extern "C" void LayerMask_get_value_m9 ();
extern "C" void LayerMask_set_value_m10 ();
extern "C" void LayerMask_LayerToName_m11 ();
extern "C" void LayerMask_NameToLayer_m12 ();
extern "C" void LayerMask_GetMask_m13 ();
extern "C" void LayerMask_op_Implicit_m14 ();
extern "C" void LayerMask_op_Implicit_m15 ();
extern "C" void SystemInfo_get_deviceUniqueIdentifier_m16 ();
extern "C" void WaitForSeconds__ctor_m17 ();
extern "C" void WaitForFixedUpdate__ctor_m18 ();
extern "C" void WaitForEndOfFrame__ctor_m19 ();
extern "C" void Coroutine__ctor_m20 ();
extern "C" void Coroutine_ReleaseCoroutine_m21 ();
extern "C" void Coroutine_Finalize_m22 ();
extern "C" void ScriptableObject__ctor_m23 ();
extern "C" void ScriptableObject_Internal_CreateScriptableObject_m24 ();
extern "C" void ScriptableObject_CreateInstance_m25 ();
extern "C" void ScriptableObject_CreateInstance_m26 ();
extern "C" void ScriptableObject_CreateInstanceFromType_m27 ();
extern "C" void GameCenterPlatform__ctor_m28 ();
extern "C" void GameCenterPlatform__cctor_m29 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m30 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m31 ();
extern "C" void GameCenterPlatform_Internal_Authenticate_m32 ();
extern "C" void GameCenterPlatform_Internal_Authenticated_m33 ();
extern "C" void GameCenterPlatform_Internal_UserName_m34 ();
extern "C" void GameCenterPlatform_Internal_UserID_m35 ();
extern "C" void GameCenterPlatform_Internal_Underage_m36 ();
extern "C" void GameCenterPlatform_Internal_UserImage_m37 ();
extern "C" void GameCenterPlatform_Internal_LoadFriends_m38 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievementDescriptions_m39 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievements_m40 ();
extern "C" void GameCenterPlatform_Internal_ReportProgress_m41 ();
extern "C" void GameCenterPlatform_Internal_ReportScore_m42 ();
extern "C" void GameCenterPlatform_Internal_LoadScores_m43 ();
extern "C" void GameCenterPlatform_Internal_ShowAchievementsUI_m44 ();
extern "C" void GameCenterPlatform_Internal_ShowLeaderboardUI_m45 ();
extern "C" void GameCenterPlatform_Internal_LoadUsers_m46 ();
extern "C" void GameCenterPlatform_Internal_ResetAllAchievements_m47 ();
extern "C" void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m48 ();
extern "C" void GameCenterPlatform_ResetAllAchievements_m49 ();
extern "C" void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m50 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m51 ();
extern "C" void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m52 ();
extern "C" void GameCenterPlatform_ClearAchievementDescriptions_m53 ();
extern "C" void GameCenterPlatform_SetAchievementDescription_m54 ();
extern "C" void GameCenterPlatform_SetAchievementDescriptionImage_m55 ();
extern "C" void GameCenterPlatform_TriggerAchievementDescriptionCallback_m56 ();
extern "C" void GameCenterPlatform_AuthenticateCallbackWrapper_m57 ();
extern "C" void GameCenterPlatform_ClearFriends_m58 ();
extern "C" void GameCenterPlatform_SetFriends_m59 ();
extern "C" void GameCenterPlatform_SetFriendImage_m60 ();
extern "C" void GameCenterPlatform_TriggerFriendsCallbackWrapper_m61 ();
extern "C" void GameCenterPlatform_AchievementCallbackWrapper_m62 ();
extern "C" void GameCenterPlatform_ProgressCallbackWrapper_m63 ();
extern "C" void GameCenterPlatform_ScoreCallbackWrapper_m64 ();
extern "C" void GameCenterPlatform_ScoreLoaderCallbackWrapper_m65 ();
extern "C" void GameCenterPlatform_get_localUser_m66 ();
extern "C" void GameCenterPlatform_PopulateLocalUser_m67 ();
extern "C" void GameCenterPlatform_LoadAchievementDescriptions_m68 ();
extern "C" void GameCenterPlatform_ReportProgress_m69 ();
extern "C" void GameCenterPlatform_LoadAchievements_m70 ();
extern "C" void GameCenterPlatform_ReportScore_m71 ();
extern "C" void GameCenterPlatform_LoadScores_m72 ();
extern "C" void GameCenterPlatform_LoadScores_m73 ();
extern "C" void GameCenterPlatform_LeaderboardCallbackWrapper_m74 ();
extern "C" void GameCenterPlatform_GetLoading_m75 ();
extern "C" void GameCenterPlatform_VerifyAuthentication_m76 ();
extern "C" void GameCenterPlatform_ShowAchievementsUI_m77 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m78 ();
extern "C" void GameCenterPlatform_ClearUsers_m79 ();
extern "C" void GameCenterPlatform_SetUser_m80 ();
extern "C" void GameCenterPlatform_SetUserImage_m81 ();
extern "C" void GameCenterPlatform_TriggerUsersCallbackWrapper_m82 ();
extern "C" void GameCenterPlatform_LoadUsers_m83 ();
extern "C" void GameCenterPlatform_SafeSetUserImage_m84 ();
extern "C" void GameCenterPlatform_SafeClearArray_m85 ();
extern "C" void GameCenterPlatform_CreateLeaderboard_m86 ();
extern "C" void GameCenterPlatform_CreateAchievement_m87 ();
extern "C" void GameCenterPlatform_TriggerResetAchievementCallback_m88 ();
extern "C" void GcLeaderboard__ctor_m89 ();
extern "C" void GcLeaderboard_Finalize_m90 ();
extern "C" void GcLeaderboard_Contains_m91 ();
extern "C" void GcLeaderboard_SetScores_m92 ();
extern "C" void GcLeaderboard_SetLocalScore_m93 ();
extern "C" void GcLeaderboard_SetMaxRange_m94 ();
extern "C" void GcLeaderboard_SetTitle_m95 ();
extern "C" void GcLeaderboard_Internal_LoadScores_m96 ();
extern "C" void GcLeaderboard_Internal_LoadScoresWithUsers_m97 ();
extern "C" void GcLeaderboard_Loading_m98 ();
extern "C" void GcLeaderboard_Dispose_m99 ();
extern "C" void BoneWeight_get_weight0_m100 ();
extern "C" void BoneWeight_set_weight0_m101 ();
extern "C" void BoneWeight_get_weight1_m102 ();
extern "C" void BoneWeight_set_weight1_m103 ();
extern "C" void BoneWeight_get_weight2_m104 ();
extern "C" void BoneWeight_set_weight2_m105 ();
extern "C" void BoneWeight_get_weight3_m106 ();
extern "C" void BoneWeight_set_weight3_m107 ();
extern "C" void BoneWeight_get_boneIndex0_m108 ();
extern "C" void BoneWeight_set_boneIndex0_m109 ();
extern "C" void BoneWeight_get_boneIndex1_m110 ();
extern "C" void BoneWeight_set_boneIndex1_m111 ();
extern "C" void BoneWeight_get_boneIndex2_m112 ();
extern "C" void BoneWeight_set_boneIndex2_m113 ();
extern "C" void BoneWeight_get_boneIndex3_m114 ();
extern "C" void BoneWeight_set_boneIndex3_m115 ();
extern "C" void BoneWeight_GetHashCode_m116 ();
extern "C" void BoneWeight_Equals_m117 ();
extern "C" void BoneWeight_op_Equality_m118 ();
extern "C" void BoneWeight_op_Inequality_m119 ();
extern "C" void Screen_get_width_m120 ();
extern "C" void Screen_get_height_m121 ();
extern "C" void Texture__ctor_m122 ();
extern "C" void Texture2D__ctor_m123 ();
extern "C" void Texture2D_Internal_Create_m124 ();
extern "C" void GUILayer_HitTest_m125 ();
extern "C" void GUILayer_INTERNAL_CALL_HitTest_m126 ();
extern "C" void GradientColorKey__ctor_m127 ();
extern "C" void GradientAlphaKey__ctor_m128 ();
extern "C" void Gradient__ctor_m129 ();
extern "C" void Gradient_Init_m130 ();
extern "C" void Gradient_Cleanup_m131 ();
extern "C" void Gradient_Finalize_m132 ();
extern "C" void ScrollViewState__ctor_m133 ();
extern "C" void WindowFunction__ctor_m134 ();
extern "C" void WindowFunction_Invoke_m135 ();
extern "C" void WindowFunction_BeginInvoke_m136 ();
extern "C" void WindowFunction_EndInvoke_m137 ();
extern "C" void GUI__cctor_m138 ();
extern "C" void GUI_set_nextScrollStepTime_m139 ();
extern "C" void GUI_set_skin_m140 ();
extern "C" void GUI_get_skin_m141 ();
extern "C" void GUI_set_changed_m142 ();
extern "C" void GUI_CallWindowDelegate_m143 ();
extern "C" void GUILayout_Width_m144 ();
extern "C" void GUILayout_Height_m145 ();
extern "C" void LayoutCache__ctor_m146 ();
extern "C" void GUILayoutUtility__cctor_m147 ();
extern "C" void GUILayoutUtility_SelectIDList_m148 ();
extern "C" void GUILayoutUtility_Begin_m149 ();
extern "C" void GUILayoutUtility_BeginWindow_m150 ();
extern "C" void GUILayoutUtility_Layout_m151 ();
extern "C" void GUILayoutUtility_LayoutFromEditorWindow_m152 ();
extern "C" void GUILayoutUtility_LayoutFreeGroup_m153 ();
extern "C" void GUILayoutUtility_LayoutSingleGroup_m154 ();
extern "C" void GUILayoutUtility_Internal_GetWindowRect_m155 ();
extern "C" void GUILayoutUtility_Internal_MoveWindow_m156 ();
extern "C" void GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m157 ();
extern "C" void GUILayoutUtility_get_spaceStyle_m158 ();
extern "C" void GUILayoutEntry__ctor_m159 ();
extern "C" void GUILayoutEntry__cctor_m160 ();
extern "C" void GUILayoutEntry_get_style_m161 ();
extern "C" void GUILayoutEntry_set_style_m162 ();
extern "C" void GUILayoutEntry_get_margin_m163 ();
extern "C" void GUILayoutEntry_CalcWidth_m164 ();
extern "C" void GUILayoutEntry_CalcHeight_m165 ();
extern "C" void GUILayoutEntry_SetHorizontal_m166 ();
extern "C" void GUILayoutEntry_SetVertical_m167 ();
extern "C" void GUILayoutEntry_ApplyStyleSettings_m168 ();
extern "C" void GUILayoutEntry_ApplyOptions_m169 ();
extern "C" void GUILayoutEntry_ToString_m170 ();
extern "C" void GUILayoutGroup__ctor_m171 ();
extern "C" void GUILayoutGroup_get_margin_m172 ();
extern "C" void GUILayoutGroup_ApplyOptions_m173 ();
extern "C" void GUILayoutGroup_ApplyStyleSettings_m174 ();
extern "C" void GUILayoutGroup_ResetCursor_m175 ();
extern "C" void GUILayoutGroup_CalcWidth_m176 ();
extern "C" void GUILayoutGroup_SetHorizontal_m177 ();
extern "C" void GUILayoutGroup_CalcHeight_m178 ();
extern "C" void GUILayoutGroup_SetVertical_m179 ();
extern "C" void GUILayoutGroup_ToString_m180 ();
extern "C" void GUIScrollGroup__ctor_m181 ();
extern "C" void GUIScrollGroup_CalcWidth_m182 ();
extern "C" void GUIScrollGroup_SetHorizontal_m183 ();
extern "C" void GUIScrollGroup_CalcHeight_m184 ();
extern "C" void GUIScrollGroup_SetVertical_m185 ();
extern "C" void GUILayoutOption__ctor_m186 ();
extern "C" void GUIUtility__cctor_m187 ();
extern "C" void GUIUtility_GetDefaultSkin_m188 ();
extern "C" void GUIUtility_Internal_GetDefaultSkin_m189 ();
extern "C" void GUIUtility_BeginGUI_m190 ();
extern "C" void GUIUtility_Internal_ExitGUI_m191 ();
extern "C" void GUIUtility_EndGUI_m192 ();
extern "C" void GUIUtility_EndGUIFromException_m193 ();
extern "C" void GUIUtility_CheckOnGUI_m194 ();
extern "C" void GUIUtility_Internal_GetGUIDepth_m195 ();
extern "C" void GUISettings__ctor_m196 ();
extern "C" void SkinChangedDelegate__ctor_m197 ();
extern "C" void SkinChangedDelegate_Invoke_m198 ();
extern "C" void SkinChangedDelegate_BeginInvoke_m199 ();
extern "C" void SkinChangedDelegate_EndInvoke_m200 ();
extern "C" void GUISkin__ctor_m201 ();
extern "C" void GUISkin_OnEnable_m202 ();
extern "C" void GUISkin_get_font_m203 ();
extern "C" void GUISkin_set_font_m204 ();
extern "C" void GUISkin_get_box_m205 ();
extern "C" void GUISkin_set_box_m206 ();
extern "C" void GUISkin_get_label_m207 ();
extern "C" void GUISkin_set_label_m208 ();
extern "C" void GUISkin_get_textField_m209 ();
extern "C" void GUISkin_set_textField_m210 ();
extern "C" void GUISkin_get_textArea_m211 ();
extern "C" void GUISkin_set_textArea_m212 ();
extern "C" void GUISkin_get_button_m213 ();
extern "C" void GUISkin_set_button_m214 ();
extern "C" void GUISkin_get_toggle_m215 ();
extern "C" void GUISkin_set_toggle_m216 ();
extern "C" void GUISkin_get_window_m217 ();
extern "C" void GUISkin_set_window_m218 ();
extern "C" void GUISkin_get_horizontalSlider_m219 ();
extern "C" void GUISkin_set_horizontalSlider_m220 ();
extern "C" void GUISkin_get_horizontalSliderThumb_m221 ();
extern "C" void GUISkin_set_horizontalSliderThumb_m222 ();
extern "C" void GUISkin_get_verticalSlider_m223 ();
extern "C" void GUISkin_set_verticalSlider_m224 ();
extern "C" void GUISkin_get_verticalSliderThumb_m225 ();
extern "C" void GUISkin_set_verticalSliderThumb_m226 ();
extern "C" void GUISkin_get_horizontalScrollbar_m227 ();
extern "C" void GUISkin_set_horizontalScrollbar_m228 ();
extern "C" void GUISkin_get_horizontalScrollbarThumb_m229 ();
extern "C" void GUISkin_set_horizontalScrollbarThumb_m230 ();
extern "C" void GUISkin_get_horizontalScrollbarLeftButton_m231 ();
extern "C" void GUISkin_set_horizontalScrollbarLeftButton_m232 ();
extern "C" void GUISkin_get_horizontalScrollbarRightButton_m233 ();
extern "C" void GUISkin_set_horizontalScrollbarRightButton_m234 ();
extern "C" void GUISkin_get_verticalScrollbar_m235 ();
extern "C" void GUISkin_set_verticalScrollbar_m236 ();
extern "C" void GUISkin_get_verticalScrollbarThumb_m237 ();
extern "C" void GUISkin_set_verticalScrollbarThumb_m238 ();
extern "C" void GUISkin_get_verticalScrollbarUpButton_m239 ();
extern "C" void GUISkin_set_verticalScrollbarUpButton_m240 ();
extern "C" void GUISkin_get_verticalScrollbarDownButton_m241 ();
extern "C" void GUISkin_set_verticalScrollbarDownButton_m242 ();
extern "C" void GUISkin_get_scrollView_m243 ();
extern "C" void GUISkin_set_scrollView_m244 ();
extern "C" void GUISkin_get_customStyles_m245 ();
extern "C" void GUISkin_set_customStyles_m246 ();
extern "C" void GUISkin_get_settings_m247 ();
extern "C" void GUISkin_get_error_m248 ();
extern "C" void GUISkin_Apply_m249 ();
extern "C" void GUISkin_BuildStyleCache_m250 ();
extern "C" void GUISkin_GetStyle_m251 ();
extern "C" void GUISkin_FindStyle_m252 ();
extern "C" void GUISkin_MakeCurrent_m253 ();
extern "C" void GUISkin_GetEnumerator_m254 ();
extern "C" void GUIContent__ctor_m255 ();
extern "C" void GUIContent__ctor_m256 ();
extern "C" void GUIContent__cctor_m257 ();
extern "C" void GUIContent_ClearStaticCache_m258 ();
extern "C" void GUIStyleState__ctor_m259 ();
extern "C" void GUIStyleState__ctor_m260 ();
extern "C" void GUIStyleState_Finalize_m261 ();
extern "C" void GUIStyleState_Init_m262 ();
extern "C" void GUIStyleState_Cleanup_m263 ();
extern "C" void GUIStyleState_GetBackgroundInternal_m264 ();
extern "C" void GUIStyleState_set_textColor_m265 ();
extern "C" void GUIStyleState_INTERNAL_set_textColor_m266 ();
extern "C" void RectOffset__ctor_m267 ();
extern "C" void RectOffset__ctor_m268 ();
extern "C" void RectOffset__ctor_m269 ();
extern "C" void RectOffset_Finalize_m270 ();
extern "C" void RectOffset_Init_m271 ();
extern "C" void RectOffset_Cleanup_m272 ();
extern "C" void RectOffset_get_left_m273 ();
extern "C" void RectOffset_set_left_m274 ();
extern "C" void RectOffset_get_right_m275 ();
extern "C" void RectOffset_set_right_m276 ();
extern "C" void RectOffset_get_top_m277 ();
extern "C" void RectOffset_set_top_m278 ();
extern "C" void RectOffset_get_bottom_m279 ();
extern "C" void RectOffset_set_bottom_m280 ();
extern "C" void RectOffset_get_horizontal_m281 ();
extern "C" void RectOffset_get_vertical_m282 ();
extern "C" void RectOffset_ToString_m283 ();
extern "C" void GUIStyle__ctor_m284 ();
extern "C" void GUIStyle__cctor_m285 ();
extern "C" void GUIStyle_Finalize_m286 ();
extern "C" void GUIStyle_Init_m287 ();
extern "C" void GUIStyle_Cleanup_m288 ();
extern "C" void GUIStyle_get_name_m289 ();
extern "C" void GUIStyle_set_name_m290 ();
extern "C" void GUIStyle_get_normal_m291 ();
extern "C" void GUIStyle_GetStyleStatePtr_m292 ();
extern "C" void GUIStyle_get_margin_m293 ();
extern "C" void GUIStyle_get_padding_m294 ();
extern "C" void GUIStyle_GetRectOffsetPtr_m295 ();
extern "C" void GUIStyle_get_fixedWidth_m296 ();
extern "C" void GUIStyle_get_fixedHeight_m297 ();
extern "C" void GUIStyle_get_stretchWidth_m298 ();
extern "C" void GUIStyle_set_stretchWidth_m299 ();
extern "C" void GUIStyle_get_stretchHeight_m300 ();
extern "C" void GUIStyle_set_stretchHeight_m301 ();
extern "C" void GUIStyle_SetDefaultFont_m302 ();
extern "C" void GUIStyle_get_none_m303 ();
extern "C" void GUIStyle_ToString_m304 ();
extern "C" void TouchScreenKeyboard_Destroy_m305 ();
extern "C" void TouchScreenKeyboard_Finalize_m306 ();
extern "C" void Event__ctor_m307 ();
extern "C" void Event_Init_m308 ();
extern "C" void Event_Finalize_m309 ();
extern "C" void Event_Cleanup_m310 ();
extern "C" void Event_get_type_m311 ();
extern "C" void Event_get_mousePosition_m312 ();
extern "C" void Event_Internal_GetMousePosition_m313 ();
extern "C" void Event_get_modifiers_m314 ();
extern "C" void Event_get_character_m315 ();
extern "C" void Event_get_commandName_m316 ();
extern "C" void Event_get_keyCode_m317 ();
extern "C" void Event_get_current_m318 ();
extern "C" void Event_Internal_SetNativeEvent_m319 ();
extern "C" void Event_Internal_MakeMasterEventCurrent_m320 ();
extern "C" void Event_get_isKey_m321 ();
extern "C" void Event_get_isMouse_m322 ();
extern "C" void Event_GetHashCode_m323 ();
extern "C" void Event_Equals_m324 ();
extern "C" void Event_ToString_m325 ();
extern "C" void Vector2__ctor_m326 ();
extern "C" void Vector2_ToString_m327 ();
extern "C" void Vector2_GetHashCode_m328 ();
extern "C" void Vector2_Equals_m329 ();
extern "C" void Vector2_SqrMagnitude_m330 ();
extern "C" void Vector2_get_zero_m331 ();
extern "C" void Vector2_op_Subtraction_m332 ();
extern "C" void Vector2_op_Equality_m333 ();
extern "C" void Vector3__ctor_m334 ();
extern "C" void Vector3_GetHashCode_m335 ();
extern "C" void Vector3_Equals_m336 ();
extern "C" void Vector3_ToString_m337 ();
extern "C" void Vector3_ToString_m338 ();
extern "C" void Vector3_SqrMagnitude_m339 ();
extern "C" void Vector3_Min_m340 ();
extern "C" void Vector3_Max_m341 ();
extern "C" void Vector3_get_zero_m342 ();
extern "C" void Vector3_get_back_m343 ();
extern "C" void Vector3_op_Addition_m344 ();
extern "C" void Vector3_op_Subtraction_m345 ();
extern "C" void Vector3_op_Multiply_m346 ();
extern "C" void Vector3_op_Equality_m347 ();
extern "C" void Color__ctor_m348 ();
extern "C" void Color__ctor_m349 ();
extern "C" void Color_ToString_m350 ();
extern "C" void Color_GetHashCode_m351 ();
extern "C" void Color_Equals_m352 ();
extern "C" void Color_get_red_m353 ();
extern "C" void Color_get_white_m354 ();
extern "C" void Color_op_Multiply_m355 ();
extern "C" void Color_op_Implicit_m356 ();
extern "C" void Color32__ctor_m357 ();
extern "C" void Color32_ToString_m358 ();
extern "C" void Color32_op_Implicit_m359 ();
extern "C" void Quaternion_ToString_m360 ();
extern "C" void Quaternion_GetHashCode_m361 ();
extern "C" void Quaternion_Equals_m362 ();
extern "C" void Rect__ctor_m363 ();
extern "C" void Rect_get_x_m364 ();
extern "C" void Rect_set_x_m365 ();
extern "C" void Rect_get_y_m366 ();
extern "C" void Rect_set_y_m367 ();
extern "C" void Rect_get_width_m368 ();
extern "C" void Rect_set_width_m369 ();
extern "C" void Rect_get_height_m370 ();
extern "C" void Rect_set_height_m371 ();
extern "C" void Rect_get_xMin_m372 ();
extern "C" void Rect_get_yMin_m373 ();
extern "C" void Rect_get_xMax_m374 ();
extern "C" void Rect_get_yMax_m375 ();
extern "C" void Rect_ToString_m376 ();
extern "C" void Rect_Contains_m377 ();
extern "C" void Rect_GetHashCode_m378 ();
extern "C" void Rect_Equals_m379 ();
extern "C" void Matrix4x4_get_Item_m380 ();
extern "C" void Matrix4x4_set_Item_m381 ();
extern "C" void Matrix4x4_get_Item_m382 ();
extern "C" void Matrix4x4_set_Item_m383 ();
extern "C" void Matrix4x4_GetHashCode_m384 ();
extern "C" void Matrix4x4_Equals_m385 ();
extern "C" void Matrix4x4_Inverse_m386 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Inverse_m387 ();
extern "C" void Matrix4x4_Transpose_m388 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Transpose_m389 ();
extern "C" void Matrix4x4_Invert_m390 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Invert_m391 ();
extern "C" void Matrix4x4_get_inverse_m392 ();
extern "C" void Matrix4x4_get_transpose_m393 ();
extern "C" void Matrix4x4_get_isIdentity_m394 ();
extern "C" void Matrix4x4_GetColumn_m395 ();
extern "C" void Matrix4x4_GetRow_m396 ();
extern "C" void Matrix4x4_SetColumn_m397 ();
extern "C" void Matrix4x4_SetRow_m398 ();
extern "C" void Matrix4x4_MultiplyPoint_m399 ();
extern "C" void Matrix4x4_MultiplyPoint3x4_m400 ();
extern "C" void Matrix4x4_MultiplyVector_m401 ();
extern "C" void Matrix4x4_Scale_m402 ();
extern "C" void Matrix4x4_get_zero_m403 ();
extern "C" void Matrix4x4_get_identity_m404 ();
extern "C" void Matrix4x4_SetTRS_m405 ();
extern "C" void Matrix4x4_TRS_m406 ();
extern "C" void Matrix4x4_INTERNAL_CALL_TRS_m407 ();
extern "C" void Matrix4x4_ToString_m408 ();
extern "C" void Matrix4x4_ToString_m409 ();
extern "C" void Matrix4x4_Ortho_m410 ();
extern "C" void Matrix4x4_Perspective_m411 ();
extern "C" void Matrix4x4_op_Multiply_m412 ();
extern "C" void Matrix4x4_op_Multiply_m413 ();
extern "C" void Matrix4x4_op_Equality_m414 ();
extern "C" void Matrix4x4_op_Inequality_m415 ();
extern "C" void Bounds__ctor_m416 ();
extern "C" void Bounds_GetHashCode_m417 ();
extern "C" void Bounds_Equals_m418 ();
extern "C" void Bounds_get_center_m419 ();
extern "C" void Bounds_set_center_m420 ();
extern "C" void Bounds_get_size_m421 ();
extern "C" void Bounds_set_size_m422 ();
extern "C" void Bounds_get_extents_m423 ();
extern "C" void Bounds_set_extents_m424 ();
extern "C" void Bounds_get_min_m425 ();
extern "C" void Bounds_set_min_m426 ();
extern "C" void Bounds_get_max_m427 ();
extern "C" void Bounds_set_max_m428 ();
extern "C" void Bounds_SetMinMax_m429 ();
extern "C" void Bounds_Encapsulate_m430 ();
extern "C" void Bounds_Encapsulate_m431 ();
extern "C" void Bounds_Expand_m432 ();
extern "C" void Bounds_Expand_m433 ();
extern "C" void Bounds_Intersects_m434 ();
extern "C" void Bounds_Internal_Contains_m435 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_Contains_m436 ();
extern "C" void Bounds_Contains_m437 ();
extern "C" void Bounds_Internal_SqrDistance_m438 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_SqrDistance_m439 ();
extern "C" void Bounds_SqrDistance_m440 ();
extern "C" void Bounds_Internal_IntersectRay_m441 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_IntersectRay_m442 ();
extern "C" void Bounds_IntersectRay_m443 ();
extern "C" void Bounds_IntersectRay_m444 ();
extern "C" void Bounds_Internal_GetClosestPoint_m445 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m446 ();
extern "C" void Bounds_ClosestPoint_m447 ();
extern "C" void Bounds_ToString_m448 ();
extern "C" void Bounds_ToString_m449 ();
extern "C" void Bounds_op_Equality_m450 ();
extern "C" void Bounds_op_Inequality_m451 ();
extern "C" void Vector4__ctor_m452 ();
extern "C" void Vector4_GetHashCode_m453 ();
extern "C" void Vector4_Equals_m454 ();
extern "C" void Vector4_ToString_m455 ();
extern "C" void Vector4_Dot_m456 ();
extern "C" void Vector4_SqrMagnitude_m457 ();
extern "C" void Vector4_op_Subtraction_m458 ();
extern "C" void Vector4_op_Equality_m459 ();
extern "C" void Ray_get_direction_m460 ();
extern "C" void Ray_ToString_m461 ();
extern "C" void MathfInternal__cctor_m462 ();
extern "C" void Mathf__cctor_m463 ();
extern "C" void Mathf_Abs_m464 ();
extern "C" void Mathf_Min_m465 ();
extern "C" void Mathf_Min_m466 ();
extern "C" void Mathf_Max_m467 ();
extern "C" void Mathf_Max_m468 ();
extern "C" void Mathf_Round_m469 ();
extern "C" void Mathf_Clamp_m470 ();
extern "C" void Mathf_Clamp01_m471 ();
extern "C" void Mathf_Lerp_m472 ();
extern "C" void Mathf_Approximately_m473 ();
extern "C" void ReapplyDrivenProperties__ctor_m474 ();
extern "C" void ReapplyDrivenProperties_Invoke_m475 ();
extern "C" void ReapplyDrivenProperties_BeginInvoke_m476 ();
extern "C" void ReapplyDrivenProperties_EndInvoke_m477 ();
extern "C" void RectTransform_SendReapplyDrivenProperties_m478 ();
extern "C" void ResourceRequest__ctor_m479 ();
extern "C" void ResourceRequest_get_asset_m480 ();
extern "C" void Resources_Load_m481 ();
extern "C" void SerializePrivateVariables__ctor_m482 ();
extern "C" void SerializeField__ctor_m483 ();
extern "C" void SphericalHarmonicsL2_Clear_m484 ();
extern "C" void SphericalHarmonicsL2_ClearInternal_m485 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m486 ();
extern "C" void SphericalHarmonicsL2_AddAmbientLight_m487 ();
extern "C" void SphericalHarmonicsL2_AddAmbientLightInternal_m488 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m489 ();
extern "C" void SphericalHarmonicsL2_AddDirectionalLight_m490 ();
extern "C" void SphericalHarmonicsL2_AddDirectionalLightInternal_m491 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m492 ();
extern "C" void SphericalHarmonicsL2_get_Item_m493 ();
extern "C" void SphericalHarmonicsL2_set_Item_m494 ();
extern "C" void SphericalHarmonicsL2_GetHashCode_m495 ();
extern "C" void SphericalHarmonicsL2_Equals_m496 ();
extern "C" void SphericalHarmonicsL2_op_Multiply_m497 ();
extern "C" void SphericalHarmonicsL2_op_Multiply_m498 ();
extern "C" void SphericalHarmonicsL2_op_Addition_m499 ();
extern "C" void SphericalHarmonicsL2_op_Equality_m500 ();
extern "C" void SphericalHarmonicsL2_op_Inequality_m501 ();
extern "C" void WWW__ctor_m502 ();
extern "C" void WWW_Dispose_m503 ();
extern "C" void WWW_Finalize_m504 ();
extern "C" void WWW_DestroyWWW_m505 ();
extern "C" void WWW_InitWWW_m506 ();
extern "C" void WWW_get_responseHeaders_m507 ();
extern "C" void WWW_get_responseHeadersString_m508 ();
extern "C" void WWW_get_text_m509 ();
extern "C" void WWW_get_DefaultEncoding_m510 ();
extern "C" void WWW_GetTextEncoder_m511 ();
extern "C" void WWW_get_bytes_m512 ();
extern "C" void WWW_get_error_m513 ();
extern "C" void WWW_get_isDone_m514 ();
extern "C" void WWW_FlattenedHeadersFrom_m515 ();
extern "C" void WWW_ParseHTTPHeaderString_m516 ();
extern "C" void WWWForm__ctor_m517 ();
extern "C" void WWWForm_AddField_m518 ();
extern "C" void WWWForm_AddField_m519 ();
extern "C" void WWWForm_AddField_m520 ();
extern "C" void WWWForm_get_headers_m521 ();
extern "C" void WWWForm_get_data_m522 ();
extern "C" void WWWTranscoder__cctor_m523 ();
extern "C" void WWWTranscoder_Byte2Hex_m524 ();
extern "C" void WWWTranscoder_URLEncode_m525 ();
extern "C" void WWWTranscoder_QPEncode_m526 ();
extern "C" void WWWTranscoder_Encode_m527 ();
extern "C" void WWWTranscoder_ByteArrayContains_m528 ();
extern "C" void WWWTranscoder_SevenBitClean_m529 ();
extern "C" void WWWTranscoder_SevenBitClean_m530 ();
extern "C" void UnityString_Format_m531 ();
extern "C" void AsyncOperation__ctor_m532 ();
extern "C" void AsyncOperation_InternalDestroy_m533 ();
extern "C" void AsyncOperation_Finalize_m534 ();
extern "C" void LogCallback__ctor_m535 ();
extern "C" void LogCallback_Invoke_m536 ();
extern "C" void LogCallback_BeginInvoke_m537 ();
extern "C" void LogCallback_EndInvoke_m538 ();
extern "C" void Application_get_cloudProjectId_m539 ();
extern "C" void Application_CallLogCallback_m540 ();
extern "C" void Behaviour__ctor_m541 ();
extern "C" void CameraCallback__ctor_m542 ();
extern "C" void CameraCallback_Invoke_m543 ();
extern "C" void CameraCallback_BeginInvoke_m544 ();
extern "C" void CameraCallback_EndInvoke_m545 ();
extern "C" void Camera_get_nearClipPlane_m546 ();
extern "C" void Camera_get_farClipPlane_m547 ();
extern "C" void Camera_get_cullingMask_m548 ();
extern "C" void Camera_get_eventMask_m549 ();
extern "C" void Camera_get_pixelRect_m550 ();
extern "C" void Camera_INTERNAL_get_pixelRect_m551 ();
extern "C" void Camera_get_targetTexture_m552 ();
extern "C" void Camera_get_clearFlags_m553 ();
extern "C" void Camera_ScreenPointToRay_m554 ();
extern "C" void Camera_INTERNAL_CALL_ScreenPointToRay_m555 ();
extern "C" void Camera_get_allCamerasCount_m556 ();
extern "C" void Camera_GetAllCameras_m557 ();
extern "C" void Camera_FireOnPreCull_m558 ();
extern "C" void Camera_FireOnPreRender_m559 ();
extern "C" void Camera_FireOnPostRender_m560 ();
extern "C" void Camera_RaycastTry_m561 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry_m562 ();
extern "C" void Camera_RaycastTry2D_m563 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry2D_m564 ();
extern "C" void Debug_Internal_Log_m565 ();
extern "C" void Debug_Log_m566 ();
extern "C" void Debug_LogError_m567 ();
extern "C" void Debug_LogWarning_m568 ();
extern "C" void DisplaysUpdatedDelegate__ctor_m569 ();
extern "C" void DisplaysUpdatedDelegate_Invoke_m570 ();
extern "C" void DisplaysUpdatedDelegate_BeginInvoke_m571 ();
extern "C" void DisplaysUpdatedDelegate_EndInvoke_m572 ();
extern "C" void Display__ctor_m573 ();
extern "C" void Display__ctor_m574 ();
extern "C" void Display__cctor_m575 ();
extern "C" void Display_add_onDisplaysUpdated_m576 ();
extern "C" void Display_remove_onDisplaysUpdated_m577 ();
extern "C" void Display_get_renderingWidth_m578 ();
extern "C" void Display_get_renderingHeight_m579 ();
extern "C" void Display_get_systemWidth_m580 ();
extern "C" void Display_get_systemHeight_m581 ();
extern "C" void Display_get_colorBuffer_m582 ();
extern "C" void Display_get_depthBuffer_m583 ();
extern "C" void Display_Activate_m584 ();
extern "C" void Display_Activate_m585 ();
extern "C" void Display_SetParams_m586 ();
extern "C" void Display_SetRenderingResolution_m587 ();
extern "C" void Display_MultiDisplayLicense_m588 ();
extern "C" void Display_RelativeMouseAt_m589 ();
extern "C" void Display_get_main_m590 ();
extern "C" void Display_RecreateDisplayList_m591 ();
extern "C" void Display_FireDisplaysUpdated_m592 ();
extern "C" void Display_GetSystemExtImpl_m593 ();
extern "C" void Display_GetRenderingExtImpl_m594 ();
extern "C" void Display_GetRenderingBuffersImpl_m595 ();
extern "C" void Display_SetRenderingResolutionImpl_m596 ();
extern "C" void Display_ActivateDisplayImpl_m597 ();
extern "C" void Display_SetParamsImpl_m598 ();
extern "C" void Display_MultiDisplayLicenseImpl_m599 ();
extern "C" void Display_RelativeMouseAtImpl_m600 ();
extern "C" void MonoBehaviour__ctor_m601 ();
extern "C" void MonoBehaviour_StartCoroutine_m602 ();
extern "C" void MonoBehaviour_StartCoroutine_Auto_m603 ();
extern "C" void Input__cctor_m604 ();
extern "C" void Input_GetMouseButton_m605 ();
extern "C" void Input_GetMouseButtonDown_m606 ();
extern "C" void Input_get_mousePosition_m607 ();
extern "C" void Input_INTERNAL_get_mousePosition_m608 ();
extern "C" void Object__ctor_m609 ();
extern "C" void Object_get_name_m610 ();
extern "C" void Object_ToString_m611 ();
extern "C" void Object_Equals_m612 ();
extern "C" void Object_GetHashCode_m613 ();
extern "C" void Object_CompareBaseObjects_m614 ();
extern "C" void Object_IsNativeObjectAlive_m615 ();
extern "C" void Object_GetInstanceID_m616 ();
extern "C" void Object_GetCachedPtr_m617 ();
extern "C" void Object_op_Implicit_m618 ();
extern "C" void Object_op_Equality_m619 ();
extern "C" void Object_op_Inequality_m620 ();
extern "C" void Component__ctor_m621 ();
extern "C" void Component_get_gameObject_m622 ();
extern "C" void Component_GetComponentFastPath_m623 ();
extern "C" void GameObject_SendMessage_m624 ();
extern "C" void Enumerator__ctor_m625 ();
extern "C" void Enumerator_get_Current_m626 ();
extern "C" void Enumerator_MoveNext_m627 ();
extern "C" void Transform_get_childCount_m628 ();
extern "C" void Transform_GetEnumerator_m629 ();
extern "C" void Transform_GetChild_m630 ();
extern "C" void Random_Range_m631 ();
extern "C" void Random_RandomRangeInt_m632 ();
extern "C" void YieldInstruction__ctor_m633 ();
extern "C" void PlayerPrefs_GetString_m634 ();
extern "C" void PlayerPrefs_GetString_m635 ();
extern "C" void Particle_get_position_m636 ();
extern "C" void Particle_set_position_m637 ();
extern "C" void Particle_get_velocity_m638 ();
extern "C" void Particle_set_velocity_m639 ();
extern "C" void Particle_get_energy_m640 ();
extern "C" void Particle_set_energy_m641 ();
extern "C" void Particle_get_startEnergy_m642 ();
extern "C" void Particle_set_startEnergy_m643 ();
extern "C" void Particle_get_size_m644 ();
extern "C" void Particle_set_size_m645 ();
extern "C" void Particle_get_rotation_m646 ();
extern "C" void Particle_set_rotation_m647 ();
extern "C" void Particle_get_angularVelocity_m648 ();
extern "C" void Particle_set_angularVelocity_m649 ();
extern "C" void Particle_get_color_m650 ();
extern "C" void Particle_set_color_m651 ();
extern "C" void AudioConfigurationChangeHandler__ctor_m652 ();
extern "C" void AudioConfigurationChangeHandler_Invoke_m653 ();
extern "C" void AudioConfigurationChangeHandler_BeginInvoke_m654 ();
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m655 ();
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m656 ();
extern "C" void PCMReaderCallback__ctor_m657 ();
extern "C" void PCMReaderCallback_Invoke_m658 ();
extern "C" void PCMReaderCallback_BeginInvoke_m659 ();
extern "C" void PCMReaderCallback_EndInvoke_m660 ();
extern "C" void PCMSetPositionCallback__ctor_m661 ();
extern "C" void PCMSetPositionCallback_Invoke_m662 ();
extern "C" void PCMSetPositionCallback_BeginInvoke_m663 ();
extern "C" void PCMSetPositionCallback_EndInvoke_m664 ();
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m665 ();
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m666 ();
extern "C" void WebCamDevice_get_name_m667 ();
extern "C" void WebCamDevice_get_isFrontFacing_m668 ();
extern "C" void AnimationEvent__ctor_m669 ();
extern "C" void AnimationEvent_get_data_m670 ();
extern "C" void AnimationEvent_set_data_m671 ();
extern "C" void AnimationEvent_get_stringParameter_m672 ();
extern "C" void AnimationEvent_set_stringParameter_m673 ();
extern "C" void AnimationEvent_get_floatParameter_m674 ();
extern "C" void AnimationEvent_set_floatParameter_m675 ();
extern "C" void AnimationEvent_get_intParameter_m676 ();
extern "C" void AnimationEvent_set_intParameter_m677 ();
extern "C" void AnimationEvent_get_objectReferenceParameter_m678 ();
extern "C" void AnimationEvent_set_objectReferenceParameter_m679 ();
extern "C" void AnimationEvent_get_functionName_m680 ();
extern "C" void AnimationEvent_set_functionName_m681 ();
extern "C" void AnimationEvent_get_time_m682 ();
extern "C" void AnimationEvent_set_time_m683 ();
extern "C" void AnimationEvent_get_messageOptions_m684 ();
extern "C" void AnimationEvent_set_messageOptions_m685 ();
extern "C" void AnimationEvent_get_isFiredByLegacy_m686 ();
extern "C" void AnimationEvent_get_isFiredByAnimator_m687 ();
extern "C" void AnimationEvent_get_animationState_m688 ();
extern "C" void AnimationEvent_get_animatorStateInfo_m689 ();
extern "C" void AnimationEvent_get_animatorClipInfo_m690 ();
extern "C" void AnimationEvent_GetHash_m691 ();
extern "C" void AnimationCurve__ctor_m692 ();
extern "C" void AnimationCurve__ctor_m693 ();
extern "C" void AnimationCurve_Cleanup_m694 ();
extern "C" void AnimationCurve_Finalize_m695 ();
extern "C" void AnimationCurve_Init_m696 ();
extern "C" void AnimatorStateInfo_IsName_m697 ();
extern "C" void AnimatorStateInfo_get_fullPathHash_m698 ();
extern "C" void AnimatorStateInfo_get_nameHash_m699 ();
extern "C" void AnimatorStateInfo_get_shortNameHash_m700 ();
extern "C" void AnimatorStateInfo_get_normalizedTime_m701 ();
extern "C" void AnimatorStateInfo_get_length_m702 ();
extern "C" void AnimatorStateInfo_get_tagHash_m703 ();
extern "C" void AnimatorStateInfo_IsTag_m704 ();
extern "C" void AnimatorStateInfo_get_loop_m705 ();
extern "C" void AnimatorTransitionInfo_IsName_m706 ();
extern "C" void AnimatorTransitionInfo_IsUserName_m707 ();
extern "C" void AnimatorTransitionInfo_get_fullPathHash_m708 ();
extern "C" void AnimatorTransitionInfo_get_nameHash_m709 ();
extern "C" void AnimatorTransitionInfo_get_userNameHash_m710 ();
extern "C" void AnimatorTransitionInfo_get_normalizedTime_m711 ();
extern "C" void AnimatorTransitionInfo_get_anyState_m712 ();
extern "C" void AnimatorTransitionInfo_get_entry_m713 ();
extern "C" void AnimatorTransitionInfo_get_exit_m714 ();
extern "C" void Animator_StringToHash_m715 ();
extern "C" void HumanBone_get_boneName_m716 ();
extern "C" void HumanBone_set_boneName_m717 ();
extern "C" void HumanBone_get_humanName_m718 ();
extern "C" void HumanBone_set_humanName_m719 ();
extern "C" void CharacterInfo_get_advance_m720 ();
extern "C" void CharacterInfo_get_glyphWidth_m721 ();
extern "C" void CharacterInfo_get_glyphHeight_m722 ();
extern "C" void CharacterInfo_get_bearing_m723 ();
extern "C" void CharacterInfo_get_minY_m724 ();
extern "C" void CharacterInfo_get_maxY_m725 ();
extern "C" void CharacterInfo_get_minX_m726 ();
extern "C" void CharacterInfo_get_maxX_m727 ();
extern "C" void CharacterInfo_get_uvBottomLeftUnFlipped_m728 ();
extern "C" void CharacterInfo_get_uvBottomRightUnFlipped_m729 ();
extern "C" void CharacterInfo_get_uvTopRightUnFlipped_m730 ();
extern "C" void CharacterInfo_get_uvTopLeftUnFlipped_m731 ();
extern "C" void CharacterInfo_get_uvBottomLeft_m732 ();
extern "C" void CharacterInfo_get_uvBottomRight_m733 ();
extern "C" void CharacterInfo_get_uvTopRight_m734 ();
extern "C" void CharacterInfo_get_uvTopLeft_m735 ();
extern "C" void FontTextureRebuildCallback__ctor_m736 ();
extern "C" void FontTextureRebuildCallback_Invoke_m737 ();
extern "C" void FontTextureRebuildCallback_BeginInvoke_m738 ();
extern "C" void FontTextureRebuildCallback_EndInvoke_m739 ();
extern "C" void Font_InvokeTextureRebuilt_Internal_m740 ();
extern "C" void Font_get_dynamic_m741 ();
extern "C" void TextGenerator__ctor_m742 ();
extern "C" void TextGenerator__ctor_m743 ();
extern "C" void TextGenerator_System_IDisposable_Dispose_m744 ();
extern "C" void TextGenerator_Init_m745 ();
extern "C" void TextGenerator_Dispose_cpp_m746 ();
extern "C" void TextGenerator_Populate_Internal_m747 ();
extern "C" void TextGenerator_Populate_Internal_cpp_m748 ();
extern "C" void TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m749 ();
extern "C" void TextGenerator_get_rectExtents_m750 ();
extern "C" void TextGenerator_INTERNAL_get_rectExtents_m751 ();
extern "C" void TextGenerator_get_vertexCount_m752 ();
extern "C" void TextGenerator_GetVerticesInternal_m753 ();
extern "C" void TextGenerator_GetVerticesArray_m754 ();
extern "C" void TextGenerator_get_characterCount_m755 ();
extern "C" void TextGenerator_get_characterCountVisible_m756 ();
extern "C" void TextGenerator_GetCharactersInternal_m757 ();
extern "C" void TextGenerator_GetCharactersArray_m758 ();
extern "C" void TextGenerator_get_lineCount_m759 ();
extern "C" void TextGenerator_GetLinesInternal_m760 ();
extern "C" void TextGenerator_GetLinesArray_m761 ();
extern "C" void TextGenerator_get_fontSizeUsedForBestFit_m762 ();
extern "C" void TextGenerator_Finalize_m763 ();
extern "C" void TextGenerator_ValidatedSettings_m764 ();
extern "C" void TextGenerator_Invalidate_m765 ();
extern "C" void TextGenerator_GetCharacters_m766 ();
extern "C" void TextGenerator_GetLines_m767 ();
extern "C" void TextGenerator_GetVertices_m768 ();
extern "C" void TextGenerator_GetPreferredWidth_m769 ();
extern "C" void TextGenerator_GetPreferredHeight_m770 ();
extern "C" void TextGenerator_Populate_m771 ();
extern "C" void TextGenerator_PopulateAlways_m772 ();
extern "C" void TextGenerator_get_verts_m773 ();
extern "C" void TextGenerator_get_characters_m774 ();
extern "C" void TextGenerator_get_lines_m775 ();
extern "C" void WillRenderCanvases__ctor_m776 ();
extern "C" void WillRenderCanvases_Invoke_m777 ();
extern "C" void WillRenderCanvases_BeginInvoke_m778 ();
extern "C" void WillRenderCanvases_EndInvoke_m779 ();
extern "C" void Canvas_SendWillRenderCanvases_m780 ();
extern "C" void UIVertex__cctor_m781 ();
extern "C" void Request__ctor_m782 ();
extern "C" void Request_get_sourceId_m783 ();
extern "C" void Request_get_appId_m784 ();
extern "C" void Request_get_domain_m785 ();
extern "C" void Request_ToString_m786 ();
extern "C" void ResponseBase__ctor_m787 ();
extern "C" void ResponseBase_ParseJSONString_m788 ();
extern "C" void ResponseBase_ParseJSONInt32_m789 ();
extern "C" void ResponseBase_ParseJSONUInt16_m790 ();
extern "C" void ResponseBase_ParseJSONUInt64_m791 ();
extern "C" void ResponseBase_ParseJSONBool_m792 ();
extern "C" void Response__ctor_m793 ();
extern "C" void Response_get_success_m794 ();
extern "C" void Response_set_success_m795 ();
extern "C" void Response_get_extendedInfo_m796 ();
extern "C" void Response_set_extendedInfo_m797 ();
extern "C" void Response_ToString_m798 ();
extern "C" void Response_Parse_m799 ();
extern "C" void BasicResponse__ctor_m800 ();
extern "C" void CreateMatchRequest__ctor_m801 ();
extern "C" void CreateMatchRequest_get_name_m802 ();
extern "C" void CreateMatchRequest_set_name_m803 ();
extern "C" void CreateMatchRequest_get_size_m804 ();
extern "C" void CreateMatchRequest_set_size_m805 ();
extern "C" void CreateMatchRequest_get_advertise_m806 ();
extern "C" void CreateMatchRequest_set_advertise_m807 ();
extern "C" void CreateMatchRequest_get_password_m808 ();
extern "C" void CreateMatchRequest_set_password_m809 ();
extern "C" void CreateMatchRequest_get_matchAttributes_m810 ();
extern "C" void CreateMatchRequest_ToString_m811 ();
extern "C" void CreateMatchResponse__ctor_m812 ();
extern "C" void CreateMatchResponse_get_address_m813 ();
extern "C" void CreateMatchResponse_set_address_m814 ();
extern "C" void CreateMatchResponse_get_port_m815 ();
extern "C" void CreateMatchResponse_set_port_m816 ();
extern "C" void CreateMatchResponse_get_networkId_m817 ();
extern "C" void CreateMatchResponse_set_networkId_m818 ();
extern "C" void CreateMatchResponse_get_accessTokenString_m819 ();
extern "C" void CreateMatchResponse_set_accessTokenString_m820 ();
extern "C" void CreateMatchResponse_get_nodeId_m821 ();
extern "C" void CreateMatchResponse_set_nodeId_m822 ();
extern "C" void CreateMatchResponse_get_usingRelay_m823 ();
extern "C" void CreateMatchResponse_set_usingRelay_m824 ();
extern "C" void CreateMatchResponse_ToString_m825 ();
extern "C" void CreateMatchResponse_Parse_m826 ();
extern "C" void JoinMatchRequest__ctor_m827 ();
extern "C" void JoinMatchRequest_get_networkId_m828 ();
extern "C" void JoinMatchRequest_set_networkId_m829 ();
extern "C" void JoinMatchRequest_get_password_m830 ();
extern "C" void JoinMatchRequest_set_password_m831 ();
extern "C" void JoinMatchRequest_ToString_m832 ();
extern "C" void JoinMatchResponse__ctor_m833 ();
extern "C" void JoinMatchResponse_get_address_m834 ();
extern "C" void JoinMatchResponse_set_address_m835 ();
extern "C" void JoinMatchResponse_get_port_m836 ();
extern "C" void JoinMatchResponse_set_port_m837 ();
extern "C" void JoinMatchResponse_get_networkId_m838 ();
extern "C" void JoinMatchResponse_set_networkId_m839 ();
extern "C" void JoinMatchResponse_get_accessTokenString_m840 ();
extern "C" void JoinMatchResponse_set_accessTokenString_m841 ();
extern "C" void JoinMatchResponse_get_nodeId_m842 ();
extern "C" void JoinMatchResponse_set_nodeId_m843 ();
extern "C" void JoinMatchResponse_get_usingRelay_m844 ();
extern "C" void JoinMatchResponse_set_usingRelay_m845 ();
extern "C" void JoinMatchResponse_ToString_m846 ();
extern "C" void JoinMatchResponse_Parse_m847 ();
extern "C" void DestroyMatchRequest__ctor_m848 ();
extern "C" void DestroyMatchRequest_get_networkId_m849 ();
extern "C" void DestroyMatchRequest_set_networkId_m850 ();
extern "C" void DestroyMatchRequest_ToString_m851 ();
extern "C" void DropConnectionRequest__ctor_m852 ();
extern "C" void DropConnectionRequest_get_networkId_m853 ();
extern "C" void DropConnectionRequest_set_networkId_m854 ();
extern "C" void DropConnectionRequest_get_nodeId_m855 ();
extern "C" void DropConnectionRequest_set_nodeId_m856 ();
extern "C" void DropConnectionRequest_ToString_m857 ();
extern "C" void ListMatchRequest__ctor_m858 ();
extern "C" void ListMatchRequest_get_pageSize_m859 ();
extern "C" void ListMatchRequest_set_pageSize_m860 ();
extern "C" void ListMatchRequest_get_pageNum_m861 ();
extern "C" void ListMatchRequest_set_pageNum_m862 ();
extern "C" void ListMatchRequest_get_nameFilter_m863 ();
extern "C" void ListMatchRequest_set_nameFilter_m864 ();
extern "C" void ListMatchRequest_get_includePasswordMatches_m865 ();
extern "C" void ListMatchRequest_get_matchAttributeFilterLessThan_m866 ();
extern "C" void ListMatchRequest_get_matchAttributeFilterGreaterThan_m867 ();
extern "C" void ListMatchRequest_ToString_m868 ();
extern "C" void MatchDirectConnectInfo__ctor_m869 ();
extern "C" void MatchDirectConnectInfo_get_nodeId_m870 ();
extern "C" void MatchDirectConnectInfo_set_nodeId_m871 ();
extern "C" void MatchDirectConnectInfo_get_publicAddress_m872 ();
extern "C" void MatchDirectConnectInfo_set_publicAddress_m873 ();
extern "C" void MatchDirectConnectInfo_get_privateAddress_m874 ();
extern "C" void MatchDirectConnectInfo_set_privateAddress_m875 ();
extern "C" void MatchDirectConnectInfo_ToString_m876 ();
extern "C" void MatchDirectConnectInfo_Parse_m877 ();
extern "C" void MatchDesc__ctor_m878 ();
extern "C" void MatchDesc_get_networkId_m879 ();
extern "C" void MatchDesc_set_networkId_m880 ();
extern "C" void MatchDesc_get_name_m881 ();
extern "C" void MatchDesc_set_name_m882 ();
extern "C" void MatchDesc_get_averageEloScore_m883 ();
extern "C" void MatchDesc_get_maxSize_m884 ();
extern "C" void MatchDesc_set_maxSize_m885 ();
extern "C" void MatchDesc_get_currentSize_m886 ();
extern "C" void MatchDesc_set_currentSize_m887 ();
extern "C" void MatchDesc_get_isPrivate_m888 ();
extern "C" void MatchDesc_set_isPrivate_m889 ();
extern "C" void MatchDesc_get_matchAttributes_m890 ();
extern "C" void MatchDesc_get_hostNodeId_m891 ();
extern "C" void MatchDesc_get_directConnectInfos_m892 ();
extern "C" void MatchDesc_set_directConnectInfos_m893 ();
extern "C" void MatchDesc_ToString_m894 ();
extern "C" void MatchDesc_Parse_m895 ();
extern "C" void ListMatchResponse__ctor_m896 ();
extern "C" void ListMatchResponse_get_matches_m897 ();
extern "C" void ListMatchResponse_set_matches_m898 ();
extern "C" void ListMatchResponse_ToString_m899 ();
extern "C" void ListMatchResponse_Parse_m900 ();
extern "C" void NetworkAccessToken__ctor_m901 ();
extern "C" void NetworkAccessToken_GetByteString_m902 ();
extern "C" void Utility__cctor_m903 ();
extern "C" void Utility_GetSourceID_m904 ();
extern "C" void Utility_SetAppID_m905 ();
extern "C" void Utility_GetAppID_m906 ();
extern "C" void Utility_GetAccessTokenForNetwork_m907 ();
extern "C" void NetworkMatch__ctor_m908 ();
extern "C" void NetworkMatch_get_baseUri_m909 ();
extern "C" void NetworkMatch_set_baseUri_m910 ();
extern "C" void NetworkMatch_SetProgramAppID_m911 ();
extern "C" void NetworkMatch_CreateMatch_m912 ();
extern "C" void NetworkMatch_CreateMatch_m913 ();
extern "C" void NetworkMatch_JoinMatch_m914 ();
extern "C" void NetworkMatch_JoinMatch_m915 ();
extern "C" void NetworkMatch_DestroyMatch_m916 ();
extern "C" void NetworkMatch_DestroyMatch_m917 ();
extern "C" void NetworkMatch_DropConnection_m918 ();
extern "C" void NetworkMatch_DropConnection_m919 ();
extern "C" void NetworkMatch_ListMatches_m920 ();
extern "C" void NetworkMatch_ListMatches_m921 ();
extern "C" void JsonArray__ctor_m922 ();
extern "C" void JsonArray_ToString_m923 ();
extern "C" void JsonObject__ctor_m924 ();
extern "C" void JsonObject_System_Collections_IEnumerable_GetEnumerator_m925 ();
extern "C" void JsonObject_Add_m926 ();
extern "C" void JsonObject_get_Keys_m927 ();
extern "C" void JsonObject_TryGetValue_m928 ();
extern "C" void JsonObject_get_Values_m929 ();
extern "C" void JsonObject_get_Item_m930 ();
extern "C" void JsonObject_set_Item_m931 ();
extern "C" void JsonObject_Add_m932 ();
extern "C" void JsonObject_Clear_m933 ();
extern "C" void JsonObject_Contains_m934 ();
extern "C" void JsonObject_CopyTo_m935 ();
extern "C" void JsonObject_get_Count_m936 ();
extern "C" void JsonObject_get_IsReadOnly_m937 ();
extern "C" void JsonObject_Remove_m938 ();
extern "C" void JsonObject_GetEnumerator_m939 ();
extern "C" void JsonObject_ToString_m940 ();
extern "C" void SimpleJson_TryDeserializeObject_m941 ();
extern "C" void SimpleJson_SerializeObject_m942 ();
extern "C" void SimpleJson_SerializeObject_m943 ();
extern "C" void SimpleJson_ParseObject_m944 ();
extern "C" void SimpleJson_ParseArray_m945 ();
extern "C" void SimpleJson_ParseValue_m946 ();
extern "C" void SimpleJson_ParseString_m947 ();
extern "C" void SimpleJson_ConvertFromUtf32_m948 ();
extern "C" void SimpleJson_ParseNumber_m949 ();
extern "C" void SimpleJson_GetLastIndexOfNumber_m950 ();
extern "C" void SimpleJson_EatWhitespace_m951 ();
extern "C" void SimpleJson_LookAhead_m952 ();
extern "C" void SimpleJson_NextToken_m953 ();
extern "C" void SimpleJson_SerializeValue_m954 ();
extern "C" void SimpleJson_SerializeObject_m955 ();
extern "C" void SimpleJson_SerializeArray_m956 ();
extern "C" void SimpleJson_SerializeString_m957 ();
extern "C" void SimpleJson_SerializeNumber_m958 ();
extern "C" void SimpleJson_IsNumeric_m959 ();
extern "C" void SimpleJson_get_CurrentJsonSerializerStrategy_m960 ();
extern "C" void SimpleJson_get_PocoJsonSerializerStrategy_m961 ();
extern "C" void PocoJsonSerializerStrategy__ctor_m962 ();
extern "C" void PocoJsonSerializerStrategy__cctor_m963 ();
extern "C" void PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m964 ();
extern "C" void PocoJsonSerializerStrategy_ContructorDelegateFactory_m965 ();
extern "C" void PocoJsonSerializerStrategy_GetterValueFactory_m966 ();
extern "C" void PocoJsonSerializerStrategy_SetterValueFactory_m967 ();
extern "C" void PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m968 ();
extern "C" void PocoJsonSerializerStrategy_SerializeEnum_m969 ();
extern "C" void PocoJsonSerializerStrategy_TrySerializeKnownTypes_m970 ();
extern "C" void PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m971 ();
extern "C" void GetDelegate__ctor_m972 ();
extern "C" void GetDelegate_Invoke_m973 ();
extern "C" void GetDelegate_BeginInvoke_m974 ();
extern "C" void GetDelegate_EndInvoke_m975 ();
extern "C" void SetDelegate__ctor_m976 ();
extern "C" void SetDelegate_Invoke_m977 ();
extern "C" void SetDelegate_BeginInvoke_m978 ();
extern "C" void SetDelegate_EndInvoke_m979 ();
extern "C" void ConstructorDelegate__ctor_m980 ();
extern "C" void ConstructorDelegate_Invoke_m981 ();
extern "C" void ConstructorDelegate_BeginInvoke_m982 ();
extern "C" void ConstructorDelegate_EndInvoke_m983 ();
extern "C" void U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m984 ();
extern "C" void U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m985 ();
extern "C" void U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m986 ();
extern "C" void U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m987 ();
extern "C" void U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m988 ();
extern "C" void U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m989 ();
extern "C" void U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m990 ();
extern "C" void U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m991 ();
extern "C" void U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m992 ();
extern "C" void U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m993 ();
extern "C" void ReflectionUtils__cctor_m994 ();
extern "C" void ReflectionUtils_GetConstructors_m995 ();
extern "C" void ReflectionUtils_GetConstructorInfo_m996 ();
extern "C" void ReflectionUtils_GetProperties_m997 ();
extern "C" void ReflectionUtils_GetFields_m998 ();
extern "C" void ReflectionUtils_GetGetterMethodInfo_m999 ();
extern "C" void ReflectionUtils_GetSetterMethodInfo_m1000 ();
extern "C" void ReflectionUtils_GetContructor_m1001 ();
extern "C" void ReflectionUtils_GetConstructorByReflection_m1002 ();
extern "C" void ReflectionUtils_GetConstructorByReflection_m1003 ();
extern "C" void ReflectionUtils_GetGetMethod_m1004 ();
extern "C" void ReflectionUtils_GetGetMethod_m1005 ();
extern "C" void ReflectionUtils_GetGetMethodByReflection_m1006 ();
extern "C" void ReflectionUtils_GetGetMethodByReflection_m1007 ();
extern "C" void ReflectionUtils_GetSetMethod_m1008 ();
extern "C" void ReflectionUtils_GetSetMethod_m1009 ();
extern "C" void ReflectionUtils_GetSetMethodByReflection_m1010 ();
extern "C" void ReflectionUtils_GetSetMethodByReflection_m1011 ();
extern "C" void WrapperlessIcall__ctor_m1012 ();
extern "C" void IL2CPPStructAlignmentAttribute__ctor_m1013 ();
extern "C" void AttributeHelperEngine__cctor_m1014 ();
extern "C" void AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m1015 ();
extern "C" void AttributeHelperEngine_GetRequiredComponents_m1016 ();
extern "C" void AttributeHelperEngine_CheckIsEditorScript_m1017 ();
extern "C" void AddComponentMenu__ctor_m1018 ();
extern "C" void ExecuteInEditMode__ctor_m1019 ();
extern "C" void SetupCoroutine__ctor_m1020 ();
extern "C" void SetupCoroutine_InvokeMember_m1021 ();
extern "C" void SetupCoroutine_InvokeStatic_m1022 ();
extern "C" void WritableAttribute__ctor_m1023 ();
extern "C" void AssemblyIsEditorAssembly__ctor_m1024 ();
extern "C" void GcUserProfileData_ToUserProfile_m1025 ();
extern "C" void GcUserProfileData_AddToArray_m1026 ();
extern "C" void GcAchievementDescriptionData_ToAchievementDescription_m1027 ();
extern "C" void GcAchievementData_ToAchievement_m1028 ();
extern "C" void GcScoreData_ToScore_m1029 ();
extern "C" void Resolution_get_width_m1030 ();
extern "C" void Resolution_set_width_m1031 ();
extern "C" void Resolution_get_height_m1032 ();
extern "C" void Resolution_set_height_m1033 ();
extern "C" void Resolution_get_refreshRate_m1034 ();
extern "C" void Resolution_set_refreshRate_m1035 ();
extern "C" void Resolution_ToString_m1036 ();
extern "C" void LocalUser__ctor_m1037 ();
extern "C" void LocalUser_SetFriends_m1038 ();
extern "C" void LocalUser_SetAuthenticated_m1039 ();
extern "C" void LocalUser_SetUnderage_m1040 ();
extern "C" void LocalUser_get_authenticated_m1041 ();
extern "C" void UserProfile__ctor_m1042 ();
extern "C" void UserProfile__ctor_m1043 ();
extern "C" void UserProfile_ToString_m1044 ();
extern "C" void UserProfile_SetUserName_m1045 ();
extern "C" void UserProfile_SetUserID_m1046 ();
extern "C" void UserProfile_SetImage_m1047 ();
extern "C" void UserProfile_get_userName_m1048 ();
extern "C" void UserProfile_get_id_m1049 ();
extern "C" void UserProfile_get_isFriend_m1050 ();
extern "C" void UserProfile_get_state_m1051 ();
extern "C" void Achievement__ctor_m1052 ();
extern "C" void Achievement__ctor_m1053 ();
extern "C" void Achievement__ctor_m1054 ();
extern "C" void Achievement_ToString_m1055 ();
extern "C" void Achievement_get_id_m1056 ();
extern "C" void Achievement_set_id_m1057 ();
extern "C" void Achievement_get_percentCompleted_m1058 ();
extern "C" void Achievement_set_percentCompleted_m1059 ();
extern "C" void Achievement_get_completed_m1060 ();
extern "C" void Achievement_get_hidden_m1061 ();
extern "C" void Achievement_get_lastReportedDate_m1062 ();
extern "C" void AchievementDescription__ctor_m1063 ();
extern "C" void AchievementDescription_ToString_m1064 ();
extern "C" void AchievementDescription_SetImage_m1065 ();
extern "C" void AchievementDescription_get_id_m1066 ();
extern "C" void AchievementDescription_set_id_m1067 ();
extern "C" void AchievementDescription_get_title_m1068 ();
extern "C" void AchievementDescription_get_achievedDescription_m1069 ();
extern "C" void AchievementDescription_get_unachievedDescription_m1070 ();
extern "C" void AchievementDescription_get_hidden_m1071 ();
extern "C" void AchievementDescription_get_points_m1072 ();
extern "C" void Score__ctor_m1073 ();
extern "C" void Score__ctor_m1074 ();
extern "C" void Score_ToString_m1075 ();
extern "C" void Score_get_leaderboardID_m1076 ();
extern "C" void Score_set_leaderboardID_m1077 ();
extern "C" void Score_get_value_m1078 ();
extern "C" void Score_set_value_m1079 ();
extern "C" void Leaderboard__ctor_m1080 ();
extern "C" void Leaderboard_ToString_m1081 ();
extern "C" void Leaderboard_SetLocalUserScore_m1082 ();
extern "C" void Leaderboard_SetMaxRange_m1083 ();
extern "C" void Leaderboard_SetScores_m1084 ();
extern "C" void Leaderboard_SetTitle_m1085 ();
extern "C" void Leaderboard_GetUserFilter_m1086 ();
extern "C" void Leaderboard_get_id_m1087 ();
extern "C" void Leaderboard_set_id_m1088 ();
extern "C" void Leaderboard_get_userScope_m1089 ();
extern "C" void Leaderboard_set_userScope_m1090 ();
extern "C" void Leaderboard_get_range_m1091 ();
extern "C" void Leaderboard_set_range_m1092 ();
extern "C" void Leaderboard_get_timeScope_m1093 ();
extern "C" void Leaderboard_set_timeScope_m1094 ();
extern "C" void HitInfo_SendMessage_m1095 ();
extern "C" void HitInfo_Compare_m1096 ();
extern "C" void HitInfo_op_Implicit_m1097 ();
extern "C" void SendMouseEvents__cctor_m1098 ();
extern "C" void SendMouseEvents_DoSendMouseEvents_m1099 ();
extern "C" void SendMouseEvents_SendEvents_m1100 ();
extern "C" void Range__ctor_m1101 ();
extern "C" void SliderState__ctor_m1102 ();
extern "C" void StackTraceUtility__ctor_m1103 ();
extern "C" void StackTraceUtility__cctor_m1104 ();
extern "C" void StackTraceUtility_SetProjectFolder_m1105 ();
extern "C" void StackTraceUtility_ExtractStackTrace_m1106 ();
extern "C" void StackTraceUtility_IsSystemStacktraceType_m1107 ();
extern "C" void StackTraceUtility_ExtractStringFromException_m1108 ();
extern "C" void StackTraceUtility_ExtractStringFromExceptionInternal_m1109 ();
extern "C" void StackTraceUtility_PostprocessStacktrace_m1110 ();
extern "C" void StackTraceUtility_ExtractFormattedStackTrace_m1111 ();
extern "C" void UnityException__ctor_m1112 ();
extern "C" void UnityException__ctor_m1113 ();
extern "C" void UnityException__ctor_m1114 ();
extern "C" void UnityException__ctor_m1115 ();
extern "C" void SharedBetweenAnimatorsAttribute__ctor_m1116 ();
extern "C" void StateMachineBehaviour__ctor_m1117 ();
extern "C" void StateMachineBehaviour_OnStateEnter_m1118 ();
extern "C" void StateMachineBehaviour_OnStateUpdate_m1119 ();
extern "C" void StateMachineBehaviour_OnStateExit_m1120 ();
extern "C" void StateMachineBehaviour_OnStateMove_m1121 ();
extern "C" void StateMachineBehaviour_OnStateIK_m1122 ();
extern "C" void StateMachineBehaviour_OnStateMachineEnter_m1123 ();
extern "C" void StateMachineBehaviour_OnStateMachineExit_m1124 ();
extern "C" void TextEditor__ctor_m1125 ();
extern "C" void TextGenerationSettings_CompareColors_m1126 ();
extern "C" void TextGenerationSettings_CompareVector2_m1127 ();
extern "C" void TextGenerationSettings_Equals_m1128 ();
extern "C" void TrackedReference_Equals_m1129 ();
extern "C" void TrackedReference_GetHashCode_m1130 ();
extern "C" void TrackedReference_op_Equality_m1131 ();
extern "C" void ArgumentCache__ctor_m1132 ();
extern "C" void ArgumentCache_TidyAssemblyTypeName_m1133 ();
extern "C" void ArgumentCache_OnBeforeSerialize_m1134 ();
extern "C" void ArgumentCache_OnAfterDeserialize_m1135 ();
extern "C" void PersistentCall__ctor_m1136 ();
extern "C" void PersistentCallGroup__ctor_m1137 ();
extern "C" void InvokableCallList__ctor_m1138 ();
extern "C" void InvokableCallList_ClearPersistent_m1139 ();
extern "C" void UnityEventBase__ctor_m1140 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1141 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1142 ();
extern "C" void UnityEventBase_DirtyPersistentCalls_m1143 ();
extern "C" void UnityEventBase_ToString_m1144 ();
extern "C" void UnityEvent__ctor_m1145 ();
extern "C" void UserAuthorizationDialog__ctor_m1146 ();
extern "C" void UserAuthorizationDialog_Start_m1147 ();
extern "C" void UserAuthorizationDialog_OnGUI_m1148 ();
extern "C" void UserAuthorizationDialog_DoUserAuthorizationDialog_m1149 ();
extern "C" void DefaultValueAttribute__ctor_m1150 ();
extern "C" void DefaultValueAttribute_get_Value_m1151 ();
extern "C" void DefaultValueAttribute_Equals_m1152 ();
extern "C" void DefaultValueAttribute_GetHashCode_m1153 ();
extern "C" void ExcludeFromDocsAttribute__ctor_m1154 ();
extern "C" void FormerlySerializedAsAttribute__ctor_m1155 ();
extern "C" void TypeInferenceRuleAttribute__ctor_m1156 ();
extern "C" void TypeInferenceRuleAttribute__ctor_m1157 ();
extern "C" void TypeInferenceRuleAttribute_ToString_m1158 ();
extern "C" void GenericStack__ctor_m1159 ();
extern "C" void Locale_GetText_m1337 ();
extern "C" void Locale_GetText_m1338 ();
extern "C" void MonoTODOAttribute__ctor_m1339 ();
extern "C" void MonoTODOAttribute__ctor_m1340 ();
extern "C" void GeneratedCodeAttribute__ctor_m1341 ();
extern "C" void HybridDictionary__ctor_m1342 ();
extern "C" void HybridDictionary__ctor_m1343 ();
extern "C" void HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m1344 ();
extern "C" void HybridDictionary_get_inner_m1345 ();
extern "C" void HybridDictionary_get_Count_m1346 ();
extern "C" void HybridDictionary_get_Item_m1347 ();
extern "C" void HybridDictionary_set_Item_m1348 ();
extern "C" void HybridDictionary_get_SyncRoot_m1349 ();
extern "C" void HybridDictionary_Add_m1350 ();
extern "C" void HybridDictionary_Contains_m1351 ();
extern "C" void HybridDictionary_CopyTo_m1352 ();
extern "C" void HybridDictionary_GetEnumerator_m1353 ();
extern "C" void HybridDictionary_Remove_m1354 ();
extern "C" void HybridDictionary_Switch_m1355 ();
extern "C" void DictionaryNode__ctor_m1356 ();
extern "C" void DictionaryNodeEnumerator__ctor_m1357 ();
extern "C" void DictionaryNodeEnumerator_FailFast_m1358 ();
extern "C" void DictionaryNodeEnumerator_MoveNext_m1359 ();
extern "C" void DictionaryNodeEnumerator_Reset_m1360 ();
extern "C" void DictionaryNodeEnumerator_get_Current_m1361 ();
extern "C" void DictionaryNodeEnumerator_get_DictionaryNode_m1362 ();
extern "C" void DictionaryNodeEnumerator_get_Entry_m1363 ();
extern "C" void DictionaryNodeEnumerator_get_Key_m1364 ();
extern "C" void DictionaryNodeEnumerator_get_Value_m1365 ();
extern "C" void ListDictionary__ctor_m1366 ();
extern "C" void ListDictionary__ctor_m1367 ();
extern "C" void ListDictionary_System_Collections_IEnumerable_GetEnumerator_m1368 ();
extern "C" void ListDictionary_FindEntry_m1369 ();
extern "C" void ListDictionary_FindEntry_m1370 ();
extern "C" void ListDictionary_AddImpl_m1371 ();
extern "C" void ListDictionary_get_Count_m1372 ();
extern "C" void ListDictionary_get_SyncRoot_m1373 ();
extern "C" void ListDictionary_CopyTo_m1374 ();
extern "C" void ListDictionary_get_Item_m1375 ();
extern "C" void ListDictionary_set_Item_m1376 ();
extern "C" void ListDictionary_Add_m1377 ();
extern "C" void ListDictionary_Clear_m1378 ();
extern "C" void ListDictionary_Contains_m1379 ();
extern "C" void ListDictionary_GetEnumerator_m1380 ();
extern "C" void ListDictionary_Remove_m1381 ();
extern "C" void _Item__ctor_m1382 ();
extern "C" void _KeysEnumerator__ctor_m1383 ();
extern "C" void _KeysEnumerator_get_Current_m1384 ();
extern "C" void _KeysEnumerator_MoveNext_m1385 ();
extern "C" void _KeysEnumerator_Reset_m1386 ();
extern "C" void KeysCollection__ctor_m1387 ();
extern "C" void KeysCollection_System_Collections_ICollection_CopyTo_m1388 ();
extern "C" void KeysCollection_System_Collections_ICollection_get_SyncRoot_m1389 ();
extern "C" void KeysCollection_get_Count_m1390 ();
extern "C" void KeysCollection_GetEnumerator_m1391 ();
extern "C" void NameObjectCollectionBase__ctor_m1392 ();
extern "C" void NameObjectCollectionBase__ctor_m1393 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m1394 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m1395 ();
extern "C" void NameObjectCollectionBase_Init_m1396 ();
extern "C" void NameObjectCollectionBase_get_Keys_m1397 ();
extern "C" void NameObjectCollectionBase_GetEnumerator_m1398 ();
extern "C" void NameObjectCollectionBase_GetObjectData_m1399 ();
extern "C" void NameObjectCollectionBase_get_Count_m1400 ();
extern "C" void NameObjectCollectionBase_OnDeserialization_m1401 ();
extern "C" void NameObjectCollectionBase_get_IsReadOnly_m1402 ();
extern "C" void NameObjectCollectionBase_BaseAdd_m1403 ();
extern "C" void NameObjectCollectionBase_BaseGet_m1404 ();
extern "C" void NameObjectCollectionBase_BaseGet_m1405 ();
extern "C" void NameObjectCollectionBase_BaseGetKey_m1406 ();
extern "C" void NameObjectCollectionBase_FindFirstMatchedItem_m1407 ();
extern "C" void NameValueCollection__ctor_m1408 ();
extern "C" void NameValueCollection__ctor_m1409 ();
extern "C" void NameValueCollection_Add_m1410 ();
extern "C" void NameValueCollection_Get_m1411 ();
extern "C" void NameValueCollection_AsSingleString_m1412 ();
extern "C" void NameValueCollection_GetKey_m1413 ();
extern "C" void NameValueCollection_InvalidateCachedArrays_m1414 ();
extern "C" void DefaultValueAttribute__ctor_m1415 ();
extern "C" void DefaultValueAttribute_get_Value_m1416 ();
extern "C" void DefaultValueAttribute_Equals_m1417 ();
extern "C" void DefaultValueAttribute_GetHashCode_m1418 ();
extern "C" void EditorBrowsableAttribute__ctor_m1419 ();
extern "C" void EditorBrowsableAttribute_get_State_m1420 ();
extern "C" void EditorBrowsableAttribute_Equals_m1421 ();
extern "C" void EditorBrowsableAttribute_GetHashCode_m1422 ();
extern "C" void TypeConverterAttribute__ctor_m1423 ();
extern "C" void TypeConverterAttribute__ctor_m1424 ();
extern "C" void TypeConverterAttribute__cctor_m1425 ();
extern "C" void TypeConverterAttribute_Equals_m1426 ();
extern "C" void TypeConverterAttribute_GetHashCode_m1427 ();
extern "C" void TypeConverterAttribute_get_ConverterTypeName_m1428 ();
extern "C" void DefaultCertificatePolicy__ctor_m1429 ();
extern "C" void DefaultCertificatePolicy_CheckValidationResult_m1430 ();
extern "C" void FileWebRequest__ctor_m1431 ();
extern "C" void FileWebRequest__ctor_m1432 ();
extern "C" void FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m1433 ();
extern "C" void FileWebRequest_GetObjectData_m1434 ();
extern "C" void FileWebRequestCreator__ctor_m1435 ();
extern "C" void FileWebRequestCreator_Create_m1436 ();
extern "C" void FtpRequestCreator__ctor_m1437 ();
extern "C" void FtpRequestCreator_Create_m1438 ();
extern "C" void FtpWebRequest__ctor_m1439 ();
extern "C" void FtpWebRequest__cctor_m1440 ();
extern "C" void FtpWebRequest_U3CcallbackU3Em__B_m1441 ();
extern "C" void GlobalProxySelection_get_Select_m1442 ();
extern "C" void HttpRequestCreator__ctor_m1443 ();
extern "C" void HttpRequestCreator_Create_m1444 ();
extern "C" void HttpVersion__cctor_m1445 ();
extern "C" void HttpWebRequest__ctor_m1446 ();
extern "C" void HttpWebRequest__ctor_m1447 ();
extern "C" void HttpWebRequest__cctor_m1448 ();
extern "C" void HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m1449 ();
extern "C" void HttpWebRequest_get_Address_m1450 ();
extern "C" void HttpWebRequest_get_ServicePoint_m1451 ();
extern "C" void HttpWebRequest_GetServicePoint_m1452 ();
extern "C" void HttpWebRequest_GetObjectData_m1453 ();
extern "C" void IPAddress__ctor_m1454 ();
extern "C" void IPAddress__ctor_m1455 ();
extern "C" void IPAddress__cctor_m1456 ();
extern "C" void IPAddress_SwapShort_m1457 ();
extern "C" void IPAddress_HostToNetworkOrder_m1458 ();
extern "C" void IPAddress_NetworkToHostOrder_m1459 ();
extern "C" void IPAddress_Parse_m1460 ();
extern "C" void IPAddress_TryParse_m1461 ();
extern "C" void IPAddress_ParseIPV4_m1462 ();
extern "C" void IPAddress_ParseIPV6_m1463 ();
extern "C" void IPAddress_get_InternalIPv4Address_m1464 ();
extern "C" void IPAddress_get_ScopeId_m1465 ();
extern "C" void IPAddress_get_AddressFamily_m1466 ();
extern "C" void IPAddress_IsLoopback_m1467 ();
extern "C" void IPAddress_ToString_m1468 ();
extern "C" void IPAddress_ToString_m1469 ();
extern "C" void IPAddress_Equals_m1470 ();
extern "C" void IPAddress_GetHashCode_m1471 ();
extern "C" void IPAddress_Hash_m1472 ();
extern "C" void IPv6Address__ctor_m1473 ();
extern "C" void IPv6Address__ctor_m1474 ();
extern "C" void IPv6Address__ctor_m1475 ();
extern "C" void IPv6Address__cctor_m1476 ();
extern "C" void IPv6Address_Parse_m1477 ();
extern "C" void IPv6Address_Fill_m1478 ();
extern "C" void IPv6Address_TryParse_m1479 ();
extern "C" void IPv6Address_TryParse_m1480 ();
extern "C" void IPv6Address_get_Address_m1481 ();
extern "C" void IPv6Address_get_ScopeId_m1482 ();
extern "C" void IPv6Address_set_ScopeId_m1483 ();
extern "C" void IPv6Address_IsLoopback_m1484 ();
extern "C" void IPv6Address_SwapUShort_m1485 ();
extern "C" void IPv6Address_AsIPv4Int_m1486 ();
extern "C" void IPv6Address_IsIPv4Compatible_m1487 ();
extern "C" void IPv6Address_IsIPv4Mapped_m1488 ();
extern "C" void IPv6Address_ToString_m1489 ();
extern "C" void IPv6Address_ToString_m1490 ();
extern "C" void IPv6Address_Equals_m1491 ();
extern "C" void IPv6Address_GetHashCode_m1492 ();
extern "C" void IPv6Address_Hash_m1493 ();
extern "C" void ServicePoint__ctor_m1494 ();
extern "C" void ServicePoint_get_Address_m1495 ();
extern "C" void ServicePoint_get_CurrentConnections_m1496 ();
extern "C" void ServicePoint_get_IdleSince_m1497 ();
extern "C" void ServicePoint_set_IdleSince_m1498 ();
extern "C" void ServicePoint_set_Expect100Continue_m1499 ();
extern "C" void ServicePoint_set_UseNagleAlgorithm_m1500 ();
extern "C" void ServicePoint_set_SendContinue_m1501 ();
extern "C" void ServicePoint_set_UsesProxy_m1502 ();
extern "C" void ServicePoint_set_UseConnect_m1503 ();
extern "C" void ServicePoint_get_AvailableForRecycling_m1504 ();
extern "C" void SPKey__ctor_m1505 ();
extern "C" void SPKey_GetHashCode_m1506 ();
extern "C" void SPKey_Equals_m1507 ();
extern "C" void ServicePointManager__cctor_m1508 ();
extern "C" void ServicePointManager_get_CertificatePolicy_m1509 ();
extern "C" void ServicePointManager_get_CheckCertificateRevocationList_m1510 ();
extern "C" void ServicePointManager_get_SecurityProtocol_m1511 ();
extern "C" void ServicePointManager_get_ServerCertificateValidationCallback_m1512 ();
extern "C" void ServicePointManager_FindServicePoint_m1513 ();
extern "C" void ServicePointManager_RecycleServicePoints_m1514 ();
extern "C" void WebHeaderCollection__ctor_m1515 ();
extern "C" void WebHeaderCollection__ctor_m1516 ();
extern "C" void WebHeaderCollection__ctor_m1517 ();
extern "C" void WebHeaderCollection__cctor_m1518 ();
extern "C" void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m1519 ();
extern "C" void WebHeaderCollection_Add_m1520 ();
extern "C" void WebHeaderCollection_AddWithoutValidate_m1521 ();
extern "C" void WebHeaderCollection_IsRestricted_m1522 ();
extern "C" void WebHeaderCollection_OnDeserialization_m1523 ();
extern "C" void WebHeaderCollection_ToString_m1524 ();
extern "C" void WebHeaderCollection_GetObjectData_m1525 ();
extern "C" void WebHeaderCollection_get_Count_m1526 ();
extern "C" void WebHeaderCollection_get_Keys_m1527 ();
extern "C" void WebHeaderCollection_Get_m1528 ();
extern "C" void WebHeaderCollection_GetKey_m1529 ();
extern "C" void WebHeaderCollection_GetEnumerator_m1530 ();
extern "C" void WebHeaderCollection_IsHeaderValue_m1531 ();
extern "C" void WebHeaderCollection_IsHeaderName_m1532 ();
extern "C" void WebProxy__ctor_m1533 ();
extern "C" void WebProxy__ctor_m1534 ();
extern "C" void WebProxy__ctor_m1535 ();
extern "C" void WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m1536 ();
extern "C" void WebProxy_get_UseDefaultCredentials_m1537 ();
extern "C" void WebProxy_GetProxy_m1538 ();
extern "C" void WebProxy_IsBypassed_m1539 ();
extern "C" void WebProxy_GetObjectData_m1540 ();
extern "C" void WebProxy_CheckBypassList_m1541 ();
extern "C" void WebRequest__ctor_m1542 ();
extern "C" void WebRequest__ctor_m1543 ();
extern "C" void WebRequest__cctor_m1544 ();
extern "C" void WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m1545 ();
extern "C" void WebRequest_AddDynamicPrefix_m1546 ();
extern "C" void WebRequest_GetMustImplement_m1547 ();
extern "C" void WebRequest_get_DefaultWebProxy_m1548 ();
extern "C" void WebRequest_GetDefaultWebProxy_m1549 ();
extern "C" void WebRequest_GetObjectData_m1550 ();
extern "C" void WebRequest_AddPrefix_m1551 ();
extern "C" void PublicKey__ctor_m1552 ();
extern "C" void PublicKey_get_EncodedKeyValue_m1553 ();
extern "C" void PublicKey_get_EncodedParameters_m1554 ();
extern "C" void PublicKey_get_Key_m1555 ();
extern "C" void PublicKey_get_Oid_m1556 ();
extern "C" void PublicKey_GetUnsignedBigInteger_m1557 ();
extern "C" void PublicKey_DecodeDSA_m1558 ();
extern "C" void PublicKey_DecodeRSA_m1559 ();
extern "C" void X500DistinguishedName__ctor_m1560 ();
extern "C" void X500DistinguishedName_Decode_m1561 ();
extern "C" void X500DistinguishedName_GetSeparator_m1562 ();
extern "C" void X500DistinguishedName_DecodeRawData_m1563 ();
extern "C" void X500DistinguishedName_Canonize_m1564 ();
extern "C" void X500DistinguishedName_AreEqual_m1565 ();
extern "C" void X509BasicConstraintsExtension__ctor_m1566 ();
extern "C" void X509BasicConstraintsExtension__ctor_m1567 ();
extern "C" void X509BasicConstraintsExtension__ctor_m1568 ();
extern "C" void X509BasicConstraintsExtension_get_CertificateAuthority_m1569 ();
extern "C" void X509BasicConstraintsExtension_get_HasPathLengthConstraint_m1570 ();
extern "C" void X509BasicConstraintsExtension_get_PathLengthConstraint_m1571 ();
extern "C" void X509BasicConstraintsExtension_CopyFrom_m1572 ();
extern "C" void X509BasicConstraintsExtension_Decode_m1573 ();
extern "C" void X509BasicConstraintsExtension_Encode_m1574 ();
extern "C" void X509BasicConstraintsExtension_ToString_m1575 ();
extern "C" void X509Certificate2__ctor_m1576 ();
extern "C" void X509Certificate2__cctor_m1577 ();
extern "C" void X509Certificate2_get_Extensions_m1578 ();
extern "C" void X509Certificate2_get_IssuerName_m1579 ();
extern "C" void X509Certificate2_get_NotAfter_m1580 ();
extern "C" void X509Certificate2_get_NotBefore_m1581 ();
extern "C" void X509Certificate2_get_PrivateKey_m1582 ();
extern "C" void X509Certificate2_get_PublicKey_m1583 ();
extern "C" void X509Certificate2_get_SerialNumber_m1584 ();
extern "C" void X509Certificate2_get_SignatureAlgorithm_m1585 ();
extern "C" void X509Certificate2_get_SubjectName_m1586 ();
extern "C" void X509Certificate2_get_Thumbprint_m1587 ();
extern "C" void X509Certificate2_get_Version_m1588 ();
extern "C" void X509Certificate2_GetNameInfo_m1589 ();
extern "C" void X509Certificate2_Find_m1590 ();
extern "C" void X509Certificate2_GetValueAsString_m1591 ();
extern "C" void X509Certificate2_ImportPkcs12_m1592 ();
extern "C" void X509Certificate2_Import_m1593 ();
extern "C" void X509Certificate2_Reset_m1594 ();
extern "C" void X509Certificate2_ToString_m1595 ();
extern "C" void X509Certificate2_ToString_m1596 ();
extern "C" void X509Certificate2_AppendBuffer_m1597 ();
extern "C" void X509Certificate2_Verify_m1598 ();
extern "C" void X509Certificate2_get_MonoCertificate_m1599 ();
extern "C" void X509Certificate2Collection__ctor_m1600 ();
extern "C" void X509Certificate2Collection__ctor_m1601 ();
extern "C" void X509Certificate2Collection_get_Item_m1602 ();
extern "C" void X509Certificate2Collection_Add_m1603 ();
extern "C" void X509Certificate2Collection_AddRange_m1604 ();
extern "C" void X509Certificate2Collection_Contains_m1605 ();
extern "C" void X509Certificate2Collection_Find_m1606 ();
extern "C" void X509Certificate2Collection_GetEnumerator_m1607 ();
extern "C" void X509Certificate2Enumerator__ctor_m1608 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m1609 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m1610 ();
extern "C" void X509Certificate2Enumerator_get_Current_m1611 ();
extern "C" void X509Certificate2Enumerator_MoveNext_m1612 ();
extern "C" void X509CertificateEnumerator__ctor_m1613 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m1614 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m1615 ();
extern "C" void X509CertificateEnumerator_get_Current_m1616 ();
extern "C" void X509CertificateEnumerator_MoveNext_m1617 ();
extern "C" void X509CertificateCollection__ctor_m1618 ();
extern "C" void X509CertificateCollection__ctor_m1619 ();
extern "C" void X509CertificateCollection_get_Item_m1620 ();
extern "C" void X509CertificateCollection_AddRange_m1621 ();
extern "C" void X509CertificateCollection_GetEnumerator_m1622 ();
extern "C" void X509CertificateCollection_GetHashCode_m1623 ();
extern "C" void X509Chain__ctor_m1624 ();
extern "C" void X509Chain__ctor_m1625 ();
extern "C" void X509Chain__cctor_m1626 ();
extern "C" void X509Chain_get_ChainPolicy_m1627 ();
extern "C" void X509Chain_Build_m1628 ();
extern "C" void X509Chain_Reset_m1629 ();
extern "C" void X509Chain_get_Roots_m1630 ();
extern "C" void X509Chain_get_CertificateAuthorities_m1631 ();
extern "C" void X509Chain_get_CertificateCollection_m1632 ();
extern "C" void X509Chain_BuildChainFrom_m1633 ();
extern "C" void X509Chain_SelectBestFromCollection_m1634 ();
extern "C" void X509Chain_FindParent_m1635 ();
extern "C" void X509Chain_IsChainComplete_m1636 ();
extern "C" void X509Chain_IsSelfIssued_m1637 ();
extern "C" void X509Chain_ValidateChain_m1638 ();
extern "C" void X509Chain_Process_m1639 ();
extern "C" void X509Chain_PrepareForNextCertificate_m1640 ();
extern "C" void X509Chain_WrapUp_m1641 ();
extern "C" void X509Chain_ProcessCertificateExtensions_m1642 ();
extern "C" void X509Chain_IsSignedWith_m1643 ();
extern "C" void X509Chain_GetSubjectKeyIdentifier_m1644 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m1645 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m1646 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m1647 ();
extern "C" void X509Chain_CheckRevocationOnChain_m1648 ();
extern "C" void X509Chain_CheckRevocation_m1649 ();
extern "C" void X509Chain_CheckRevocation_m1650 ();
extern "C" void X509Chain_FindCrl_m1651 ();
extern "C" void X509Chain_ProcessCrlExtensions_m1652 ();
extern "C" void X509Chain_ProcessCrlEntryExtensions_m1653 ();
extern "C" void X509ChainElement__ctor_m1654 ();
extern "C" void X509ChainElement_get_Certificate_m1655 ();
extern "C" void X509ChainElement_get_ChainElementStatus_m1656 ();
extern "C" void X509ChainElement_get_StatusFlags_m1657 ();
extern "C" void X509ChainElement_set_StatusFlags_m1658 ();
extern "C" void X509ChainElement_Count_m1659 ();
extern "C" void X509ChainElement_Set_m1660 ();
extern "C" void X509ChainElement_UncompressFlags_m1661 ();
extern "C" void X509ChainElementCollection__ctor_m1662 ();
extern "C" void X509ChainElementCollection_System_Collections_ICollection_CopyTo_m1663 ();
extern "C" void X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m1664 ();
extern "C" void X509ChainElementCollection_get_Count_m1665 ();
extern "C" void X509ChainElementCollection_get_Item_m1666 ();
extern "C" void X509ChainElementCollection_get_SyncRoot_m1667 ();
extern "C" void X509ChainElementCollection_GetEnumerator_m1668 ();
extern "C" void X509ChainElementCollection_Add_m1669 ();
extern "C" void X509ChainElementCollection_Clear_m1670 ();
extern "C" void X509ChainElementCollection_Contains_m1671 ();
extern "C" void X509ChainElementEnumerator__ctor_m1672 ();
extern "C" void X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m1673 ();
extern "C" void X509ChainElementEnumerator_get_Current_m1674 ();
extern "C" void X509ChainElementEnumerator_MoveNext_m1675 ();
extern "C" void X509ChainPolicy__ctor_m1676 ();
extern "C" void X509ChainPolicy_get_ExtraStore_m1677 ();
extern "C" void X509ChainPolicy_get_RevocationFlag_m1678 ();
extern "C" void X509ChainPolicy_get_RevocationMode_m1679 ();
extern "C" void X509ChainPolicy_get_VerificationFlags_m1680 ();
extern "C" void X509ChainPolicy_get_VerificationTime_m1681 ();
extern "C" void X509ChainPolicy_Reset_m1682 ();
extern "C" void X509ChainStatus__ctor_m1683 ();
extern "C" void X509ChainStatus_get_Status_m1684 ();
extern "C" void X509ChainStatus_set_Status_m1685 ();
extern "C" void X509ChainStatus_set_StatusInformation_m1686 ();
extern "C" void X509ChainStatus_GetInformation_m1687 ();
extern "C" void X509EnhancedKeyUsageExtension__ctor_m1688 ();
extern "C" void X509EnhancedKeyUsageExtension_CopyFrom_m1689 ();
extern "C" void X509EnhancedKeyUsageExtension_Decode_m1690 ();
extern "C" void X509EnhancedKeyUsageExtension_ToString_m1691 ();
extern "C" void X509Extension__ctor_m1692 ();
extern "C" void X509Extension__ctor_m1693 ();
extern "C" void X509Extension_get_Critical_m1694 ();
extern "C" void X509Extension_set_Critical_m1695 ();
extern "C" void X509Extension_CopyFrom_m1696 ();
extern "C" void X509Extension_FormatUnkownData_m1697 ();
extern "C" void X509ExtensionCollection__ctor_m1698 ();
extern "C" void X509ExtensionCollection_System_Collections_ICollection_CopyTo_m1699 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m1700 ();
extern "C" void X509ExtensionCollection_get_Count_m1701 ();
extern "C" void X509ExtensionCollection_get_SyncRoot_m1702 ();
extern "C" void X509ExtensionCollection_get_Item_m1703 ();
extern "C" void X509ExtensionCollection_GetEnumerator_m1704 ();
extern "C" void X509ExtensionEnumerator__ctor_m1705 ();
extern "C" void X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m1706 ();
extern "C" void X509ExtensionEnumerator_get_Current_m1707 ();
extern "C" void X509ExtensionEnumerator_MoveNext_m1708 ();
extern "C" void X509KeyUsageExtension__ctor_m1709 ();
extern "C" void X509KeyUsageExtension__ctor_m1710 ();
extern "C" void X509KeyUsageExtension__ctor_m1711 ();
extern "C" void X509KeyUsageExtension_get_KeyUsages_m1712 ();
extern "C" void X509KeyUsageExtension_CopyFrom_m1713 ();
extern "C" void X509KeyUsageExtension_GetValidFlags_m1714 ();
extern "C" void X509KeyUsageExtension_Decode_m1715 ();
extern "C" void X509KeyUsageExtension_Encode_m1716 ();
extern "C" void X509KeyUsageExtension_ToString_m1717 ();
extern "C" void X509Store__ctor_m1718 ();
extern "C" void X509Store_get_Certificates_m1719 ();
extern "C" void X509Store_get_Factory_m1720 ();
extern "C" void X509Store_get_Store_m1721 ();
extern "C" void X509Store_Close_m1722 ();
extern "C" void X509Store_Open_m1723 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1724 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1725 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1726 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1727 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1728 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1729 ();
extern "C" void X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m1730 ();
extern "C" void X509SubjectKeyIdentifierExtension_CopyFrom_m1731 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChar_m1732 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChars_m1733 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHex_m1734 ();
extern "C" void X509SubjectKeyIdentifierExtension_Decode_m1735 ();
extern "C" void X509SubjectKeyIdentifierExtension_Encode_m1736 ();
extern "C" void X509SubjectKeyIdentifierExtension_ToString_m1737 ();
extern "C" void AsnEncodedData__ctor_m1738 ();
extern "C" void AsnEncodedData__ctor_m1739 ();
extern "C" void AsnEncodedData__ctor_m1740 ();
extern "C" void AsnEncodedData_get_Oid_m1741 ();
extern "C" void AsnEncodedData_set_Oid_m1742 ();
extern "C" void AsnEncodedData_get_RawData_m1743 ();
extern "C" void AsnEncodedData_set_RawData_m1744 ();
extern "C" void AsnEncodedData_CopyFrom_m1745 ();
extern "C" void AsnEncodedData_ToString_m1746 ();
extern "C" void AsnEncodedData_Default_m1747 ();
extern "C" void AsnEncodedData_BasicConstraintsExtension_m1748 ();
extern "C" void AsnEncodedData_EnhancedKeyUsageExtension_m1749 ();
extern "C" void AsnEncodedData_KeyUsageExtension_m1750 ();
extern "C" void AsnEncodedData_SubjectKeyIdentifierExtension_m1751 ();
extern "C" void AsnEncodedData_SubjectAltName_m1752 ();
extern "C" void AsnEncodedData_NetscapeCertType_m1753 ();
extern "C" void Oid__ctor_m1754 ();
extern "C" void Oid__ctor_m1755 ();
extern "C" void Oid__ctor_m1756 ();
extern "C" void Oid__ctor_m1757 ();
extern "C" void Oid_get_FriendlyName_m1758 ();
extern "C" void Oid_get_Value_m1759 ();
extern "C" void Oid_GetName_m1760 ();
extern "C" void OidCollection__ctor_m1761 ();
extern "C" void OidCollection_System_Collections_ICollection_CopyTo_m1762 ();
extern "C" void OidCollection_System_Collections_IEnumerable_GetEnumerator_m1763 ();
extern "C" void OidCollection_get_Count_m1764 ();
extern "C" void OidCollection_get_Item_m1765 ();
extern "C" void OidCollection_get_SyncRoot_m1766 ();
extern "C" void OidCollection_Add_m1767 ();
extern "C" void OidEnumerator__ctor_m1768 ();
extern "C" void OidEnumerator_System_Collections_IEnumerator_get_Current_m1769 ();
extern "C" void OidEnumerator_MoveNext_m1770 ();
extern "C" void MatchAppendEvaluator__ctor_m1771 ();
extern "C" void MatchAppendEvaluator_Invoke_m1772 ();
extern "C" void MatchAppendEvaluator_BeginInvoke_m1773 ();
extern "C" void MatchAppendEvaluator_EndInvoke_m1774 ();
extern "C" void BaseMachine__ctor_m1775 ();
extern "C" void BaseMachine_Replace_m1776 ();
extern "C" void BaseMachine_Scan_m1777 ();
extern "C" void BaseMachine_LTRReplace_m1778 ();
extern "C" void BaseMachine_RTLReplace_m1779 ();
extern "C" void Capture__ctor_m1780 ();
extern "C" void Capture__ctor_m1781 ();
extern "C" void Capture_get_Index_m1782 ();
extern "C" void Capture_get_Length_m1783 ();
extern "C" void Capture_get_Value_m1784 ();
extern "C" void Capture_ToString_m1785 ();
extern "C" void Capture_get_Text_m1786 ();
extern "C" void CaptureCollection__ctor_m1787 ();
extern "C" void CaptureCollection_get_Count_m1788 ();
extern "C" void CaptureCollection_SetValue_m1789 ();
extern "C" void CaptureCollection_get_SyncRoot_m1790 ();
extern "C" void CaptureCollection_CopyTo_m1791 ();
extern "C" void CaptureCollection_GetEnumerator_m1792 ();
extern "C" void Group__ctor_m1793 ();
extern "C" void Group__ctor_m1794 ();
extern "C" void Group__ctor_m1795 ();
extern "C" void Group__cctor_m1796 ();
extern "C" void Group_get_Captures_m1797 ();
extern "C" void Group_get_Success_m1798 ();
extern "C" void GroupCollection__ctor_m1799 ();
extern "C" void GroupCollection_get_Count_m1800 ();
extern "C" void GroupCollection_get_Item_m1801 ();
extern "C" void GroupCollection_SetValue_m1802 ();
extern "C" void GroupCollection_get_SyncRoot_m1803 ();
extern "C" void GroupCollection_CopyTo_m1804 ();
extern "C" void GroupCollection_GetEnumerator_m1805 ();
extern "C" void Match__ctor_m1806 ();
extern "C" void Match__ctor_m1807 ();
extern "C" void Match__ctor_m1808 ();
extern "C" void Match__cctor_m1809 ();
extern "C" void Match_get_Empty_m1810 ();
extern "C" void Match_get_Groups_m1811 ();
extern "C" void Match_NextMatch_m1812 ();
extern "C" void Match_get_Regex_m1813 ();
extern "C" void Enumerator__ctor_m1814 ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m1815 ();
extern "C" void Enumerator_System_Collections_IEnumerator_MoveNext_m1816 ();
extern "C" void MatchCollection__ctor_m1817 ();
extern "C" void MatchCollection_get_Count_m1818 ();
extern "C" void MatchCollection_get_Item_m1819 ();
extern "C" void MatchCollection_get_SyncRoot_m1820 ();
extern "C" void MatchCollection_CopyTo_m1821 ();
extern "C" void MatchCollection_GetEnumerator_m1822 ();
extern "C" void MatchCollection_TryToGet_m1823 ();
extern "C" void MatchCollection_get_FullList_m1824 ();
extern "C" void Regex__ctor_m1825 ();
extern "C" void Regex__ctor_m1826 ();
extern "C" void Regex__ctor_m1827 ();
extern "C" void Regex__ctor_m1828 ();
extern "C" void Regex__cctor_m1829 ();
extern "C" void Regex_System_Runtime_Serialization_ISerializable_GetObjectData_m1830 ();
extern "C" void Regex_Replace_m1332 ();
extern "C" void Regex_Replace_m1831 ();
extern "C" void Regex_validate_options_m1832 ();
extern "C" void Regex_Init_m1833 ();
extern "C" void Regex_InitNewRegex_m1834 ();
extern "C" void Regex_CreateMachineFactory_m1835 ();
extern "C" void Regex_get_Options_m1836 ();
extern "C" void Regex_get_RightToLeft_m1837 ();
extern "C" void Regex_GroupNumberFromName_m1838 ();
extern "C" void Regex_GetGroupIndex_m1839 ();
extern "C" void Regex_default_startat_m1840 ();
extern "C" void Regex_IsMatch_m1841 ();
extern "C" void Regex_IsMatch_m1842 ();
extern "C" void Regex_Match_m1843 ();
extern "C" void Regex_Matches_m1844 ();
extern "C" void Regex_Matches_m1845 ();
extern "C" void Regex_Replace_m1846 ();
extern "C" void Regex_Replace_m1847 ();
extern "C" void Regex_ToString_m1848 ();
extern "C" void Regex_get_GroupCount_m1849 ();
extern "C" void Regex_get_Gap_m1850 ();
extern "C" void Regex_CreateMachine_m1851 ();
extern "C" void Regex_GetGroupNamesArray_m1852 ();
extern "C" void Regex_get_GroupNumbers_m1853 ();
extern "C" void Key__ctor_m1854 ();
extern "C" void Key_GetHashCode_m1855 ();
extern "C" void Key_Equals_m1856 ();
extern "C" void Key_ToString_m1857 ();
extern "C" void FactoryCache__ctor_m1858 ();
extern "C" void FactoryCache_Add_m1859 ();
extern "C" void FactoryCache_Cleanup_m1860 ();
extern "C" void FactoryCache_Lookup_m1861 ();
extern "C" void Node__ctor_m1862 ();
extern "C" void MRUList__ctor_m1863 ();
extern "C" void MRUList_Use_m1864 ();
extern "C" void MRUList_Evict_m1865 ();
extern "C" void CategoryUtils_CategoryFromName_m1866 ();
extern "C" void CategoryUtils_IsCategory_m1867 ();
extern "C" void CategoryUtils_IsCategory_m1868 ();
extern "C" void LinkRef__ctor_m1869 ();
extern "C" void InterpreterFactory__ctor_m1870 ();
extern "C" void InterpreterFactory_NewInstance_m1871 ();
extern "C" void InterpreterFactory_get_GroupCount_m1872 ();
extern "C" void InterpreterFactory_get_Gap_m1873 ();
extern "C" void InterpreterFactory_set_Gap_m1874 ();
extern "C" void InterpreterFactory_get_Mapping_m1875 ();
extern "C" void InterpreterFactory_set_Mapping_m1876 ();
extern "C" void InterpreterFactory_get_NamesMapping_m1877 ();
extern "C" void InterpreterFactory_set_NamesMapping_m1878 ();
extern "C" void PatternLinkStack__ctor_m1879 ();
extern "C" void PatternLinkStack_set_BaseAddress_m1880 ();
extern "C" void PatternLinkStack_get_OffsetAddress_m1881 ();
extern "C" void PatternLinkStack_set_OffsetAddress_m1882 ();
extern "C" void PatternLinkStack_GetOffset_m1883 ();
extern "C" void PatternLinkStack_GetCurrent_m1884 ();
extern "C" void PatternLinkStack_SetCurrent_m1885 ();
extern "C" void PatternCompiler__ctor_m1886 ();
extern "C" void PatternCompiler_EncodeOp_m1887 ();
extern "C" void PatternCompiler_GetMachineFactory_m1888 ();
extern "C" void PatternCompiler_EmitFalse_m1889 ();
extern "C" void PatternCompiler_EmitTrue_m1890 ();
extern "C" void PatternCompiler_EmitCount_m1891 ();
extern "C" void PatternCompiler_EmitCharacter_m1892 ();
extern "C" void PatternCompiler_EmitCategory_m1893 ();
extern "C" void PatternCompiler_EmitNotCategory_m1894 ();
extern "C" void PatternCompiler_EmitRange_m1895 ();
extern "C" void PatternCompiler_EmitSet_m1896 ();
extern "C" void PatternCompiler_EmitString_m1897 ();
extern "C" void PatternCompiler_EmitPosition_m1898 ();
extern "C" void PatternCompiler_EmitOpen_m1899 ();
extern "C" void PatternCompiler_EmitClose_m1900 ();
extern "C" void PatternCompiler_EmitBalanceStart_m1901 ();
extern "C" void PatternCompiler_EmitBalance_m1902 ();
extern "C" void PatternCompiler_EmitReference_m1903 ();
extern "C" void PatternCompiler_EmitIfDefined_m1904 ();
extern "C" void PatternCompiler_EmitSub_m1905 ();
extern "C" void PatternCompiler_EmitTest_m1906 ();
extern "C" void PatternCompiler_EmitBranch_m1907 ();
extern "C" void PatternCompiler_EmitJump_m1908 ();
extern "C" void PatternCompiler_EmitRepeat_m1909 ();
extern "C" void PatternCompiler_EmitUntil_m1910 ();
extern "C" void PatternCompiler_EmitFastRepeat_m1911 ();
extern "C" void PatternCompiler_EmitIn_m1912 ();
extern "C" void PatternCompiler_EmitAnchor_m1913 ();
extern "C" void PatternCompiler_EmitInfo_m1914 ();
extern "C" void PatternCompiler_NewLink_m1915 ();
extern "C" void PatternCompiler_ResolveLink_m1916 ();
extern "C" void PatternCompiler_EmitBranchEnd_m1917 ();
extern "C" void PatternCompiler_EmitAlternationEnd_m1918 ();
extern "C" void PatternCompiler_MakeFlags_m1919 ();
extern "C" void PatternCompiler_Emit_m1920 ();
extern "C" void PatternCompiler_Emit_m1921 ();
extern "C" void PatternCompiler_Emit_m1922 ();
extern "C" void PatternCompiler_get_CurrentAddress_m1923 ();
extern "C" void PatternCompiler_BeginLink_m1924 ();
extern "C" void PatternCompiler_EmitLink_m1925 ();
extern "C" void LinkStack__ctor_m1926 ();
extern "C" void LinkStack_Push_m1927 ();
extern "C" void LinkStack_Pop_m1928 ();
extern "C" void Mark_get_IsDefined_m1929 ();
extern "C" void Mark_get_Index_m1930 ();
extern "C" void Mark_get_Length_m1931 ();
extern "C" void IntStack_Pop_m1932 ();
extern "C" void IntStack_Push_m1933 ();
extern "C" void IntStack_get_Count_m1934 ();
extern "C" void IntStack_set_Count_m1935 ();
extern "C" void RepeatContext__ctor_m1936 ();
extern "C" void RepeatContext_get_Count_m1937 ();
extern "C" void RepeatContext_set_Count_m1938 ();
extern "C" void RepeatContext_get_Start_m1939 ();
extern "C" void RepeatContext_set_Start_m1940 ();
extern "C" void RepeatContext_get_IsMinimum_m1941 ();
extern "C" void RepeatContext_get_IsMaximum_m1942 ();
extern "C" void RepeatContext_get_IsLazy_m1943 ();
extern "C" void RepeatContext_get_Expression_m1944 ();
extern "C" void RepeatContext_get_Previous_m1945 ();
extern "C" void Interpreter__ctor_m1946 ();
extern "C" void Interpreter_ReadProgramCount_m1947 ();
extern "C" void Interpreter_Scan_m1948 ();
extern "C" void Interpreter_Reset_m1949 ();
extern "C" void Interpreter_Eval_m1950 ();
extern "C" void Interpreter_EvalChar_m1951 ();
extern "C" void Interpreter_TryMatch_m1952 ();
extern "C" void Interpreter_IsPosition_m1953 ();
extern "C" void Interpreter_IsWordChar_m1954 ();
extern "C" void Interpreter_GetString_m1955 ();
extern "C" void Interpreter_Open_m1956 ();
extern "C" void Interpreter_Close_m1957 ();
extern "C" void Interpreter_Balance_m1958 ();
extern "C" void Interpreter_Checkpoint_m1959 ();
extern "C" void Interpreter_Backtrack_m1960 ();
extern "C" void Interpreter_ResetGroups_m1961 ();
extern "C" void Interpreter_GetLastDefined_m1962 ();
extern "C" void Interpreter_CreateMark_m1963 ();
extern "C" void Interpreter_GetGroupInfo_m1964 ();
extern "C" void Interpreter_PopulateGroup_m1965 ();
extern "C" void Interpreter_GenerateMatch_m1966 ();
extern "C" void Interval__ctor_m1967 ();
extern "C" void Interval_get_Empty_m1968 ();
extern "C" void Interval_get_IsDiscontiguous_m1969 ();
extern "C" void Interval_get_IsSingleton_m1970 ();
extern "C" void Interval_get_IsEmpty_m1971 ();
extern "C" void Interval_get_Size_m1972 ();
extern "C" void Interval_IsDisjoint_m1973 ();
extern "C" void Interval_IsAdjacent_m1974 ();
extern "C" void Interval_Contains_m1975 ();
extern "C" void Interval_Contains_m1976 ();
extern "C" void Interval_Intersects_m1977 ();
extern "C" void Interval_Merge_m1978 ();
extern "C" void Interval_CompareTo_m1979 ();
extern "C" void Enumerator__ctor_m1980 ();
extern "C" void Enumerator_get_Current_m1981 ();
extern "C" void Enumerator_MoveNext_m1982 ();
extern "C" void Enumerator_Reset_m1983 ();
extern "C" void CostDelegate__ctor_m1984 ();
extern "C" void CostDelegate_Invoke_m1985 ();
extern "C" void CostDelegate_BeginInvoke_m1986 ();
extern "C" void CostDelegate_EndInvoke_m1987 ();
extern "C" void IntervalCollection__ctor_m1988 ();
extern "C" void IntervalCollection_get_Item_m1989 ();
extern "C" void IntervalCollection_Add_m1990 ();
extern "C" void IntervalCollection_Normalize_m1991 ();
extern "C" void IntervalCollection_GetMetaCollection_m1992 ();
extern "C" void IntervalCollection_Optimize_m1993 ();
extern "C" void IntervalCollection_get_Count_m1994 ();
extern "C" void IntervalCollection_get_SyncRoot_m1995 ();
extern "C" void IntervalCollection_CopyTo_m1996 ();
extern "C" void IntervalCollection_GetEnumerator_m1997 ();
extern "C" void Parser__ctor_m1998 ();
extern "C" void Parser_ParseDecimal_m1999 ();
extern "C" void Parser_ParseOctal_m2000 ();
extern "C" void Parser_ParseHex_m2001 ();
extern "C" void Parser_ParseNumber_m2002 ();
extern "C" void Parser_ParseName_m2003 ();
extern "C" void Parser_ParseRegularExpression_m2004 ();
extern "C" void Parser_GetMapping_m2005 ();
extern "C" void Parser_ParseGroup_m2006 ();
extern "C" void Parser_ParseGroupingConstruct_m2007 ();
extern "C" void Parser_ParseAssertionType_m2008 ();
extern "C" void Parser_ParseOptions_m2009 ();
extern "C" void Parser_ParseCharacterClass_m2010 ();
extern "C" void Parser_ParseRepetitionBounds_m2011 ();
extern "C" void Parser_ParseUnicodeCategory_m2012 ();
extern "C" void Parser_ParseSpecial_m2013 ();
extern "C" void Parser_ParseEscape_m2014 ();
extern "C" void Parser_ParseName_m2015 ();
extern "C" void Parser_IsNameChar_m2016 ();
extern "C" void Parser_ParseNumber_m2017 ();
extern "C" void Parser_ParseDigit_m2018 ();
extern "C" void Parser_ConsumeWhitespace_m2019 ();
extern "C" void Parser_ResolveReferences_m2020 ();
extern "C" void Parser_HandleExplicitNumericGroups_m2021 ();
extern "C" void Parser_IsIgnoreCase_m2022 ();
extern "C" void Parser_IsMultiline_m2023 ();
extern "C" void Parser_IsExplicitCapture_m2024 ();
extern "C" void Parser_IsSingleline_m2025 ();
extern "C" void Parser_IsIgnorePatternWhitespace_m2026 ();
extern "C" void Parser_IsECMAScript_m2027 ();
extern "C" void Parser_NewParseException_m2028 ();
extern "C" void QuickSearch__ctor_m2029 ();
extern "C" void QuickSearch__cctor_m2030 ();
extern "C" void QuickSearch_get_Length_m2031 ();
extern "C" void QuickSearch_Search_m2032 ();
extern "C" void QuickSearch_SetupShiftTable_m2033 ();
extern "C" void QuickSearch_GetShiftDistance_m2034 ();
extern "C" void QuickSearch_GetChar_m2035 ();
extern "C" void ReplacementEvaluator__ctor_m2036 ();
extern "C" void ReplacementEvaluator_Evaluate_m2037 ();
extern "C" void ReplacementEvaluator_EvaluateAppend_m2038 ();
extern "C" void ReplacementEvaluator_get_NeedsGroupsOrCaptures_m2039 ();
extern "C" void ReplacementEvaluator_Ensure_m2040 ();
extern "C" void ReplacementEvaluator_AddFromReplacement_m2041 ();
extern "C" void ReplacementEvaluator_AddInt_m2042 ();
extern "C" void ReplacementEvaluator_Compile_m2043 ();
extern "C" void ReplacementEvaluator_CompileTerm_m2044 ();
extern "C" void ExpressionCollection__ctor_m2045 ();
extern "C" void ExpressionCollection_Add_m2046 ();
extern "C" void ExpressionCollection_get_Item_m2047 ();
extern "C" void ExpressionCollection_set_Item_m2048 ();
extern "C" void ExpressionCollection_OnValidate_m2049 ();
extern "C" void Expression__ctor_m2050 ();
extern "C" void Expression_GetFixedWidth_m2051 ();
extern "C" void Expression_GetAnchorInfo_m2052 ();
extern "C" void CompositeExpression__ctor_m2053 ();
extern "C" void CompositeExpression_get_Expressions_m2054 ();
extern "C" void CompositeExpression_GetWidth_m2055 ();
extern "C" void CompositeExpression_IsComplex_m2056 ();
extern "C" void Group__ctor_m2057 ();
extern "C" void Group_AppendExpression_m2058 ();
extern "C" void Group_Compile_m2059 ();
extern "C" void Group_GetWidth_m2060 ();
extern "C" void Group_GetAnchorInfo_m2061 ();
extern "C" void RegularExpression__ctor_m2062 ();
extern "C" void RegularExpression_set_GroupCount_m2063 ();
extern "C" void RegularExpression_Compile_m2064 ();
extern "C" void CapturingGroup__ctor_m2065 ();
extern "C" void CapturingGroup_get_Index_m2066 ();
extern "C" void CapturingGroup_set_Index_m2067 ();
extern "C" void CapturingGroup_get_Name_m2068 ();
extern "C" void CapturingGroup_set_Name_m2069 ();
extern "C" void CapturingGroup_get_IsNamed_m2070 ();
extern "C" void CapturingGroup_Compile_m2071 ();
extern "C" void CapturingGroup_IsComplex_m2072 ();
extern "C" void CapturingGroup_CompareTo_m2073 ();
extern "C" void BalancingGroup__ctor_m2074 ();
extern "C" void BalancingGroup_set_Balance_m2075 ();
extern "C" void BalancingGroup_Compile_m2076 ();
extern "C" void NonBacktrackingGroup__ctor_m2077 ();
extern "C" void NonBacktrackingGroup_Compile_m2078 ();
extern "C" void NonBacktrackingGroup_IsComplex_m2079 ();
extern "C" void Repetition__ctor_m2080 ();
extern "C" void Repetition_get_Expression_m2081 ();
extern "C" void Repetition_set_Expression_m2082 ();
extern "C" void Repetition_get_Minimum_m2083 ();
extern "C" void Repetition_Compile_m2084 ();
extern "C" void Repetition_GetWidth_m2085 ();
extern "C" void Repetition_GetAnchorInfo_m2086 ();
extern "C" void Assertion__ctor_m2087 ();
extern "C" void Assertion_get_TrueExpression_m2088 ();
extern "C" void Assertion_set_TrueExpression_m2089 ();
extern "C" void Assertion_get_FalseExpression_m2090 ();
extern "C" void Assertion_set_FalseExpression_m2091 ();
extern "C" void Assertion_GetWidth_m2092 ();
extern "C" void CaptureAssertion__ctor_m2093 ();
extern "C" void CaptureAssertion_set_CapturingGroup_m2094 ();
extern "C" void CaptureAssertion_Compile_m2095 ();
extern "C" void CaptureAssertion_IsComplex_m2096 ();
extern "C" void CaptureAssertion_get_Alternate_m2097 ();
extern "C" void ExpressionAssertion__ctor_m2098 ();
extern "C" void ExpressionAssertion_set_Reverse_m2099 ();
extern "C" void ExpressionAssertion_set_Negate_m2100 ();
extern "C" void ExpressionAssertion_get_TestExpression_m2101 ();
extern "C" void ExpressionAssertion_set_TestExpression_m2102 ();
extern "C" void ExpressionAssertion_Compile_m2103 ();
extern "C" void ExpressionAssertion_IsComplex_m2104 ();
extern "C" void Alternation__ctor_m2105 ();
extern "C" void Alternation_get_Alternatives_m2106 ();
extern "C" void Alternation_AddAlternative_m2107 ();
extern "C" void Alternation_Compile_m2108 ();
extern "C" void Alternation_GetWidth_m2109 ();
extern "C" void Literal__ctor_m2110 ();
extern "C" void Literal_CompileLiteral_m2111 ();
extern "C" void Literal_Compile_m2112 ();
extern "C" void Literal_GetWidth_m2113 ();
extern "C" void Literal_GetAnchorInfo_m2114 ();
extern "C" void Literal_IsComplex_m2115 ();
extern "C" void PositionAssertion__ctor_m2116 ();
extern "C" void PositionAssertion_Compile_m2117 ();
extern "C" void PositionAssertion_GetWidth_m2118 ();
extern "C" void PositionAssertion_IsComplex_m2119 ();
extern "C" void PositionAssertion_GetAnchorInfo_m2120 ();
extern "C" void Reference__ctor_m2121 ();
extern "C" void Reference_get_CapturingGroup_m2122 ();
extern "C" void Reference_set_CapturingGroup_m2123 ();
extern "C" void Reference_get_IgnoreCase_m2124 ();
extern "C" void Reference_Compile_m2125 ();
extern "C" void Reference_GetWidth_m2126 ();
extern "C" void Reference_IsComplex_m2127 ();
extern "C" void BackslashNumber__ctor_m2128 ();
extern "C" void BackslashNumber_ResolveReference_m2129 ();
extern "C" void BackslashNumber_Compile_m2130 ();
extern "C" void CharacterClass__ctor_m2131 ();
extern "C" void CharacterClass__ctor_m2132 ();
extern "C" void CharacterClass__cctor_m2133 ();
extern "C" void CharacterClass_AddCategory_m2134 ();
extern "C" void CharacterClass_AddCharacter_m2135 ();
extern "C" void CharacterClass_AddRange_m2136 ();
extern "C" void CharacterClass_Compile_m2137 ();
extern "C" void CharacterClass_GetWidth_m2138 ();
extern "C" void CharacterClass_IsComplex_m2139 ();
extern "C" void CharacterClass_GetIntervalCost_m2140 ();
extern "C" void AnchorInfo__ctor_m2141 ();
extern "C" void AnchorInfo__ctor_m2142 ();
extern "C" void AnchorInfo__ctor_m2143 ();
extern "C" void AnchorInfo_get_Offset_m2144 ();
extern "C" void AnchorInfo_get_Width_m2145 ();
extern "C" void AnchorInfo_get_Length_m2146 ();
extern "C" void AnchorInfo_get_IsUnknownWidth_m2147 ();
extern "C" void AnchorInfo_get_IsComplete_m2148 ();
extern "C" void AnchorInfo_get_Substring_m2149 ();
extern "C" void AnchorInfo_get_IgnoreCase_m2150 ();
extern "C" void AnchorInfo_get_Position_m2151 ();
extern "C" void AnchorInfo_get_IsSubstring_m2152 ();
extern "C" void AnchorInfo_get_IsPosition_m2153 ();
extern "C" void AnchorInfo_GetInterval_m2154 ();
extern "C" void DefaultUriParser__ctor_m2155 ();
extern "C" void DefaultUriParser__ctor_m2156 ();
extern "C" void UriScheme__ctor_m2157 ();
extern "C" void Uri__ctor_m1254 ();
extern "C" void Uri__ctor_m2158 ();
extern "C" void Uri__ctor_m2159 ();
extern "C" void Uri__ctor_m1256 ();
extern "C" void Uri__cctor_m2160 ();
extern "C" void Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m2161 ();
extern "C" void Uri_Merge_m2162 ();
extern "C" void Uri_get_AbsoluteUri_m2163 ();
extern "C" void Uri_get_Authority_m2164 ();
extern "C" void Uri_get_Host_m2165 ();
extern "C" void Uri_get_IsFile_m2166 ();
extern "C" void Uri_get_IsLoopback_m2167 ();
extern "C" void Uri_get_IsUnc_m2168 ();
extern "C" void Uri_get_Scheme_m2169 ();
extern "C" void Uri_get_IsAbsoluteUri_m2170 ();
extern "C" void Uri_CheckHostName_m2171 ();
extern "C" void Uri_IsIPv4Address_m2172 ();
extern "C" void Uri_IsDomainAddress_m2173 ();
extern "C" void Uri_CheckSchemeName_m2174 ();
extern "C" void Uri_IsAlpha_m2175 ();
extern "C" void Uri_Equals_m2176 ();
extern "C" void Uri_InternalEquals_m2177 ();
extern "C" void Uri_GetHashCode_m2178 ();
extern "C" void Uri_GetLeftPart_m2179 ();
extern "C" void Uri_FromHex_m2180 ();
extern "C" void Uri_HexEscape_m2181 ();
extern "C" void Uri_IsHexDigit_m2182 ();
extern "C" void Uri_IsHexEncoding_m2183 ();
extern "C" void Uri_AppendQueryAndFragment_m2184 ();
extern "C" void Uri_ToString_m2185 ();
extern "C" void Uri_EscapeString_m2186 ();
extern "C" void Uri_EscapeString_m2187 ();
extern "C" void Uri_ParseUri_m2188 ();
extern "C" void Uri_Unescape_m2189 ();
extern "C" void Uri_Unescape_m2190 ();
extern "C" void Uri_ParseAsWindowsUNC_m2191 ();
extern "C" void Uri_ParseAsWindowsAbsoluteFilePath_m2192 ();
extern "C" void Uri_ParseAsUnixAbsoluteFilePath_m2193 ();
extern "C" void Uri_Parse_m2194 ();
extern "C" void Uri_ParseNoExceptions_m2195 ();
extern "C" void Uri_CompactEscaped_m2196 ();
extern "C" void Uri_Reduce_m2197 ();
extern "C" void Uri_HexUnescapeMultiByte_m2198 ();
extern "C" void Uri_GetSchemeDelimiter_m2199 ();
extern "C" void Uri_GetDefaultPort_m2200 ();
extern "C" void Uri_GetOpaqueWiseSchemeDelimiter_m2201 ();
extern "C" void Uri_IsPredefinedScheme_m2202 ();
extern "C" void Uri_get_Parser_m2203 ();
extern "C" void Uri_EnsureAbsoluteUri_m2204 ();
extern "C" void Uri_op_Equality_m2205 ();
extern "C" void UriFormatException__ctor_m2206 ();
extern "C" void UriFormatException__ctor_m2207 ();
extern "C" void UriFormatException__ctor_m2208 ();
extern "C" void UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m2209 ();
extern "C" void UriParser__ctor_m2210 ();
extern "C" void UriParser__cctor_m2211 ();
extern "C" void UriParser_InitializeAndValidate_m2212 ();
extern "C" void UriParser_OnRegister_m2213 ();
extern "C" void UriParser_set_SchemeName_m2214 ();
extern "C" void UriParser_get_DefaultPort_m2215 ();
extern "C" void UriParser_set_DefaultPort_m2216 ();
extern "C" void UriParser_CreateDefaults_m2217 ();
extern "C" void UriParser_InternalRegister_m2218 ();
extern "C" void UriParser_GetParser_m2219 ();
extern "C" void RemoteCertificateValidationCallback__ctor_m2220 ();
extern "C" void RemoteCertificateValidationCallback_Invoke_m2221 ();
extern "C" void RemoteCertificateValidationCallback_BeginInvoke_m2222 ();
extern "C" void RemoteCertificateValidationCallback_EndInvoke_m2223 ();
extern "C" void MatchEvaluator__ctor_m2224 ();
extern "C" void MatchEvaluator_Invoke_m2225 ();
extern "C" void MatchEvaluator_BeginInvoke_m2226 ();
extern "C" void MatchEvaluator_EndInvoke_m2227 ();
extern "C" void ExtensionAttribute__ctor_m2409 ();
extern "C" void Locale_GetText_m2410 ();
extern "C" void Locale_GetText_m2411 ();
extern "C" void KeyBuilder_get_Rng_m2412 ();
extern "C" void KeyBuilder_Key_m2413 ();
extern "C" void KeyBuilder_IV_m2414 ();
extern "C" void SymmetricTransform__ctor_m2415 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m2416 ();
extern "C" void SymmetricTransform_Finalize_m2417 ();
extern "C" void SymmetricTransform_Dispose_m2418 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m2419 ();
extern "C" void SymmetricTransform_Transform_m2420 ();
extern "C" void SymmetricTransform_CBC_m2421 ();
extern "C" void SymmetricTransform_CFB_m2422 ();
extern "C" void SymmetricTransform_OFB_m2423 ();
extern "C" void SymmetricTransform_CTS_m2424 ();
extern "C" void SymmetricTransform_CheckInput_m2425 ();
extern "C" void SymmetricTransform_TransformBlock_m2426 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m2427 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m2428 ();
extern "C" void SymmetricTransform_Random_m2429 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m2430 ();
extern "C" void SymmetricTransform_FinalEncrypt_m2431 ();
extern "C" void SymmetricTransform_FinalDecrypt_m2432 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m2433 ();
extern "C" void Aes__ctor_m2434 ();
extern "C" void AesManaged__ctor_m2435 ();
extern "C" void AesManaged_GenerateIV_m2436 ();
extern "C" void AesManaged_GenerateKey_m2437 ();
extern "C" void AesManaged_CreateDecryptor_m2438 ();
extern "C" void AesManaged_CreateEncryptor_m2439 ();
extern "C" void AesManaged_get_IV_m2440 ();
extern "C" void AesManaged_set_IV_m2441 ();
extern "C" void AesManaged_get_Key_m2442 ();
extern "C" void AesManaged_set_Key_m2443 ();
extern "C" void AesManaged_get_KeySize_m2444 ();
extern "C" void AesManaged_set_KeySize_m2445 ();
extern "C" void AesManaged_CreateDecryptor_m2446 ();
extern "C" void AesManaged_CreateEncryptor_m2447 ();
extern "C" void AesManaged_Dispose_m2448 ();
extern "C" void AesTransform__ctor_m2449 ();
extern "C" void AesTransform__cctor_m2450 ();
extern "C" void AesTransform_ECB_m2451 ();
extern "C" void AesTransform_SubByte_m2452 ();
extern "C" void AesTransform_Encrypt128_m2453 ();
extern "C" void AesTransform_Decrypt128_m2454 ();
extern "C" void Locale_GetText_m2470 ();
extern "C" void ModulusRing__ctor_m2471 ();
extern "C" void ModulusRing_BarrettReduction_m2472 ();
extern "C" void ModulusRing_Multiply_m2473 ();
extern "C" void ModulusRing_Difference_m2474 ();
extern "C" void ModulusRing_Pow_m2475 ();
extern "C" void ModulusRing_Pow_m2476 ();
extern "C" void Kernel_AddSameSign_m2477 ();
extern "C" void Kernel_Subtract_m2478 ();
extern "C" void Kernel_MinusEq_m2479 ();
extern "C" void Kernel_PlusEq_m2480 ();
extern "C" void Kernel_Compare_m2481 ();
extern "C" void Kernel_SingleByteDivideInPlace_m2482 ();
extern "C" void Kernel_DwordMod_m2483 ();
extern "C" void Kernel_DwordDivMod_m2484 ();
extern "C" void Kernel_multiByteDivide_m2485 ();
extern "C" void Kernel_LeftShift_m2486 ();
extern "C" void Kernel_RightShift_m2487 ();
extern "C" void Kernel_Multiply_m2488 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m2489 ();
extern "C" void Kernel_modInverse_m2490 ();
extern "C" void Kernel_modInverse_m2491 ();
extern "C" void BigInteger__ctor_m2492 ();
extern "C" void BigInteger__ctor_m2493 ();
extern "C" void BigInteger__ctor_m2494 ();
extern "C" void BigInteger__ctor_m2495 ();
extern "C" void BigInteger__ctor_m2496 ();
extern "C" void BigInteger__cctor_m2497 ();
extern "C" void BigInteger_get_Rng_m2498 ();
extern "C" void BigInteger_GenerateRandom_m2499 ();
extern "C" void BigInteger_GenerateRandom_m2500 ();
extern "C" void BigInteger_BitCount_m2501 ();
extern "C" void BigInteger_TestBit_m2502 ();
extern "C" void BigInteger_SetBit_m2503 ();
extern "C" void BigInteger_SetBit_m2504 ();
extern "C" void BigInteger_LowestSetBit_m2505 ();
extern "C" void BigInteger_GetBytes_m2506 ();
extern "C" void BigInteger_ToString_m2507 ();
extern "C" void BigInteger_ToString_m2508 ();
extern "C" void BigInteger_Normalize_m2509 ();
extern "C" void BigInteger_Clear_m2510 ();
extern "C" void BigInteger_GetHashCode_m2511 ();
extern "C" void BigInteger_ToString_m2512 ();
extern "C" void BigInteger_Equals_m2513 ();
extern "C" void BigInteger_ModInverse_m2514 ();
extern "C" void BigInteger_ModPow_m2515 ();
extern "C" void BigInteger_GeneratePseudoPrime_m2516 ();
extern "C" void BigInteger_Incr2_m2517 ();
extern "C" void BigInteger_op_Implicit_m2518 ();
extern "C" void BigInteger_op_Implicit_m2519 ();
extern "C" void BigInteger_op_Addition_m2520 ();
extern "C" void BigInteger_op_Subtraction_m2521 ();
extern "C" void BigInteger_op_Modulus_m2522 ();
extern "C" void BigInteger_op_Modulus_m2523 ();
extern "C" void BigInteger_op_Division_m2524 ();
extern "C" void BigInteger_op_Multiply_m2525 ();
extern "C" void BigInteger_op_LeftShift_m2526 ();
extern "C" void BigInteger_op_RightShift_m2527 ();
extern "C" void BigInteger_op_Equality_m2528 ();
extern "C" void BigInteger_op_Inequality_m2529 ();
extern "C" void BigInteger_op_Equality_m2530 ();
extern "C" void BigInteger_op_Inequality_m2531 ();
extern "C" void BigInteger_op_GreaterThan_m2532 ();
extern "C" void BigInteger_op_LessThan_m2533 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m2534 ();
extern "C" void BigInteger_op_LessThanOrEqual_m2535 ();
extern "C" void PrimalityTests_GetSPPRounds_m2536 ();
extern "C" void PrimalityTests_RabinMillerTest_m2537 ();
extern "C" void PrimeGeneratorBase__ctor_m2538 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m2539 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m2540 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m2541 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m2542 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m2543 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m2544 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m2545 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m2546 ();
extern "C" void ASN1__ctor_m2306 ();
extern "C" void ASN1__ctor_m2307 ();
extern "C" void ASN1__ctor_m2289 ();
extern "C" void ASN1_get_Count_m2293 ();
extern "C" void ASN1_get_Tag_m2290 ();
extern "C" void ASN1_get_Length_m2320 ();
extern "C" void ASN1_get_Value_m2292 ();
extern "C" void ASN1_set_Value_m2547 ();
extern "C" void ASN1_CompareArray_m2548 ();
extern "C" void ASN1_CompareValue_m2319 ();
extern "C" void ASN1_Add_m2308 ();
extern "C" void ASN1_GetBytes_m2549 ();
extern "C" void ASN1_Decode_m2550 ();
extern "C" void ASN1_DecodeTLV_m2551 ();
extern "C" void ASN1_get_Item_m2294 ();
extern "C" void ASN1_Element_m2552 ();
extern "C" void ASN1_ToString_m2553 ();
extern "C" void ASN1Convert_FromInt32_m2309 ();
extern "C" void ASN1Convert_FromOid_m2554 ();
extern "C" void ASN1Convert_ToInt32_m2305 ();
extern "C" void ASN1Convert_ToOid_m2358 ();
extern "C" void ASN1Convert_ToDateTime_m2555 ();
extern "C" void BitConverterLE_GetUIntBytes_m2556 ();
extern "C" void BitConverterLE_GetBytes_m2557 ();
extern "C" void ContentInfo__ctor_m2558 ();
extern "C" void ContentInfo__ctor_m2559 ();
extern "C" void ContentInfo__ctor_m2560 ();
extern "C" void ContentInfo__ctor_m2561 ();
extern "C" void ContentInfo_get_Content_m2562 ();
extern "C" void ContentInfo_set_Content_m2563 ();
extern "C" void ContentInfo_get_ContentType_m2564 ();
extern "C" void EncryptedData__ctor_m2565 ();
extern "C" void EncryptedData__ctor_m2566 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m2567 ();
extern "C" void EncryptedData_get_EncryptedContent_m2568 ();
extern "C" void ARC4Managed__ctor_m2569 ();
extern "C" void ARC4Managed_Finalize_m2570 ();
extern "C" void ARC4Managed_Dispose_m2571 ();
extern "C" void ARC4Managed_get_Key_m2572 ();
extern "C" void ARC4Managed_set_Key_m2573 ();
extern "C" void ARC4Managed_get_CanReuseTransform_m2574 ();
extern "C" void ARC4Managed_CreateEncryptor_m2575 ();
extern "C" void ARC4Managed_CreateDecryptor_m2576 ();
extern "C" void ARC4Managed_GenerateIV_m2577 ();
extern "C" void ARC4Managed_GenerateKey_m2578 ();
extern "C" void ARC4Managed_KeySetup_m2579 ();
extern "C" void ARC4Managed_CheckInput_m2580 ();
extern "C" void ARC4Managed_TransformBlock_m2581 ();
extern "C" void ARC4Managed_InternalTransformBlock_m2582 ();
extern "C" void ARC4Managed_TransformFinalBlock_m2583 ();
extern "C" void CryptoConvert_ToHex_m2372 ();
extern "C" void KeyBuilder_get_Rng_m2584 ();
extern "C" void KeyBuilder_Key_m2585 ();
extern "C" void MD2__ctor_m2586 ();
extern "C" void MD2_Create_m2587 ();
extern "C" void MD2_Create_m2588 ();
extern "C" void MD2Managed__ctor_m2589 ();
extern "C" void MD2Managed__cctor_m2590 ();
extern "C" void MD2Managed_Padding_m2591 ();
extern "C" void MD2Managed_Initialize_m2592 ();
extern "C" void MD2Managed_HashCore_m2593 ();
extern "C" void MD2Managed_HashFinal_m2594 ();
extern "C" void MD2Managed_MD2Transform_m2595 ();
extern "C" void PKCS1__cctor_m2596 ();
extern "C" void PKCS1_Compare_m2597 ();
extern "C" void PKCS1_I2OSP_m2598 ();
extern "C" void PKCS1_OS2IP_m2599 ();
extern "C" void PKCS1_RSASP1_m2600 ();
extern "C" void PKCS1_RSAVP1_m2601 ();
extern "C" void PKCS1_Sign_v15_m2602 ();
extern "C" void PKCS1_Verify_v15_m2603 ();
extern "C" void PKCS1_Verify_v15_m2604 ();
extern "C" void PKCS1_Encode_v15_m2605 ();
extern "C" void PrivateKeyInfo__ctor_m2606 ();
extern "C" void PrivateKeyInfo__ctor_m2607 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m2608 ();
extern "C" void PrivateKeyInfo_Decode_m2609 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m2610 ();
extern "C" void PrivateKeyInfo_Normalize_m2611 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m2612 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m2613 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m2614 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m2615 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m2616 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m2617 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m2618 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m2619 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m2620 ();
extern "C" void RC4__ctor_m2621 ();
extern "C" void RC4__cctor_m2622 ();
extern "C" void RC4_get_IV_m2623 ();
extern "C" void RC4_set_IV_m2624 ();
extern "C" void KeyGeneratedEventHandler__ctor_m2625 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m2626 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m2627 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m2628 ();
extern "C" void RSAManaged__ctor_m2629 ();
extern "C" void RSAManaged__ctor_m2630 ();
extern "C" void RSAManaged_Finalize_m2631 ();
extern "C" void RSAManaged_GenerateKeyPair_m2632 ();
extern "C" void RSAManaged_get_KeySize_m2633 ();
extern "C" void RSAManaged_get_PublicOnly_m2282 ();
extern "C" void RSAManaged_DecryptValue_m2634 ();
extern "C" void RSAManaged_EncryptValue_m2635 ();
extern "C" void RSAManaged_ExportParameters_m2636 ();
extern "C" void RSAManaged_ImportParameters_m2637 ();
extern "C" void RSAManaged_Dispose_m2638 ();
extern "C" void RSAManaged_ToXmlString_m2639 ();
extern "C" void RSAManaged_GetPaddedValue_m2640 ();
extern "C" void SafeBag__ctor_m2641 ();
extern "C" void SafeBag_get_BagOID_m2642 ();
extern "C" void SafeBag_get_ASN1_m2643 ();
extern "C" void DeriveBytes__ctor_m2644 ();
extern "C" void DeriveBytes__cctor_m2645 ();
extern "C" void DeriveBytes_set_HashName_m2646 ();
extern "C" void DeriveBytes_set_IterationCount_m2647 ();
extern "C" void DeriveBytes_set_Password_m2648 ();
extern "C" void DeriveBytes_set_Salt_m2649 ();
extern "C" void DeriveBytes_Adjust_m2650 ();
extern "C" void DeriveBytes_Derive_m2651 ();
extern "C" void DeriveBytes_DeriveKey_m2652 ();
extern "C" void DeriveBytes_DeriveIV_m2653 ();
extern "C" void DeriveBytes_DeriveMAC_m2654 ();
extern "C" void PKCS12__ctor_m2655 ();
extern "C" void PKCS12__ctor_m2321 ();
extern "C" void PKCS12__ctor_m2322 ();
extern "C" void PKCS12__cctor_m2656 ();
extern "C" void PKCS12_Decode_m2657 ();
extern "C" void PKCS12_Finalize_m2658 ();
extern "C" void PKCS12_set_Password_m2659 ();
extern "C" void PKCS12_get_Keys_m2325 ();
extern "C" void PKCS12_get_Certificates_m2323 ();
extern "C" void PKCS12_Compare_m2660 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m2661 ();
extern "C" void PKCS12_Decrypt_m2662 ();
extern "C" void PKCS12_Decrypt_m2663 ();
extern "C" void PKCS12_GetExistingParameters_m2664 ();
extern "C" void PKCS12_AddPrivateKey_m2665 ();
extern "C" void PKCS12_ReadSafeBag_m2666 ();
extern "C" void PKCS12_MAC_m2667 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m2668 ();
extern "C" void X501__cctor_m2669 ();
extern "C" void X501_ToString_m2670 ();
extern "C" void X501_ToString_m2298 ();
extern "C" void X501_AppendEntry_m2671 ();
extern "C" void X509Certificate__ctor_m2328 ();
extern "C" void X509Certificate__cctor_m2672 ();
extern "C" void X509Certificate_Parse_m2673 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m2674 ();
extern "C" void X509Certificate_get_DSA_m2284 ();
extern "C" void X509Certificate_set_DSA_m2326 ();
extern "C" void X509Certificate_get_Extensions_m2344 ();
extern "C" void X509Certificate_get_Hash_m2675 ();
extern "C" void X509Certificate_get_IssuerName_m2676 ();
extern "C" void X509Certificate_get_KeyAlgorithm_m2677 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m2678 ();
extern "C" void X509Certificate_set_KeyAlgorithmParameters_m2679 ();
extern "C" void X509Certificate_get_PublicKey_m2680 ();
extern "C" void X509Certificate_get_RSA_m2681 ();
extern "C" void X509Certificate_set_RSA_m2682 ();
extern "C" void X509Certificate_get_RawData_m2683 ();
extern "C" void X509Certificate_get_SerialNumber_m2684 ();
extern "C" void X509Certificate_get_Signature_m2685 ();
extern "C" void X509Certificate_get_SignatureAlgorithm_m2686 ();
extern "C" void X509Certificate_get_SubjectName_m2687 ();
extern "C" void X509Certificate_get_ValidFrom_m2688 ();
extern "C" void X509Certificate_get_ValidUntil_m2689 ();
extern "C" void X509Certificate_get_Version_m2318 ();
extern "C" void X509Certificate_get_IsCurrent_m2690 ();
extern "C" void X509Certificate_WasCurrent_m2691 ();
extern "C" void X509Certificate_VerifySignature_m2692 ();
extern "C" void X509Certificate_VerifySignature_m2693 ();
extern "C" void X509Certificate_VerifySignature_m2343 ();
extern "C" void X509Certificate_get_IsSelfSigned_m2694 ();
extern "C" void X509Certificate_GetIssuerName_m2313 ();
extern "C" void X509Certificate_GetSubjectName_m2316 ();
extern "C" void X509Certificate_GetObjectData_m2695 ();
extern "C" void X509Certificate_PEM_m2696 ();
extern "C" void X509CertificateEnumerator__ctor_m2697 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m2698 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m2699 ();
extern "C" void X509CertificateEnumerator_get_Current_m2369 ();
extern "C" void X509CertificateEnumerator_MoveNext_m2700 ();
extern "C" void X509CertificateCollection__ctor_m2701 ();
extern "C" void X509CertificateCollection__ctor_m2702 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m2703 ();
extern "C" void X509CertificateCollection_get_Item_m2324 ();
extern "C" void X509CertificateCollection_Add_m2704 ();
extern "C" void X509CertificateCollection_AddRange_m2705 ();
extern "C" void X509CertificateCollection_Contains_m2706 ();
extern "C" void X509CertificateCollection_GetEnumerator_m2368 ();
extern "C" void X509CertificateCollection_GetHashCode_m2707 ();
extern "C" void X509CertificateCollection_IndexOf_m2708 ();
extern "C" void X509CertificateCollection_Remove_m2709 ();
extern "C" void X509CertificateCollection_Compare_m2710 ();
extern "C" void X509Chain__ctor_m2711 ();
extern "C" void X509Chain__ctor_m2712 ();
extern "C" void X509Chain_get_Status_m2713 ();
extern "C" void X509Chain_get_TrustAnchors_m2714 ();
extern "C" void X509Chain_Build_m2715 ();
extern "C" void X509Chain_IsValid_m2716 ();
extern "C" void X509Chain_FindCertificateParent_m2717 ();
extern "C" void X509Chain_FindCertificateRoot_m2718 ();
extern "C" void X509Chain_IsTrusted_m2719 ();
extern "C" void X509Chain_IsParent_m2720 ();
extern "C" void X509CrlEntry__ctor_m2721 ();
extern "C" void X509CrlEntry_get_SerialNumber_m2722 ();
extern "C" void X509CrlEntry_get_RevocationDate_m2351 ();
extern "C" void X509CrlEntry_get_Extensions_m2357 ();
extern "C" void X509Crl__ctor_m2723 ();
extern "C" void X509Crl_Parse_m2724 ();
extern "C" void X509Crl_get_Extensions_m2346 ();
extern "C" void X509Crl_get_Hash_m2725 ();
extern "C" void X509Crl_get_IssuerName_m2354 ();
extern "C" void X509Crl_get_NextUpdate_m2352 ();
extern "C" void X509Crl_Compare_m2726 ();
extern "C" void X509Crl_GetCrlEntry_m2350 ();
extern "C" void X509Crl_GetCrlEntry_m2727 ();
extern "C" void X509Crl_GetHashName_m2728 ();
extern "C" void X509Crl_VerifySignature_m2729 ();
extern "C" void X509Crl_VerifySignature_m2730 ();
extern "C" void X509Crl_VerifySignature_m2349 ();
extern "C" void X509Extension__ctor_m2731 ();
extern "C" void X509Extension__ctor_m2732 ();
extern "C" void X509Extension_Decode_m2733 ();
extern "C" void X509Extension_Encode_m2734 ();
extern "C" void X509Extension_get_Oid_m2356 ();
extern "C" void X509Extension_get_Critical_m2355 ();
extern "C" void X509Extension_get_Value_m2360 ();
extern "C" void X509Extension_Equals_m2735 ();
extern "C" void X509Extension_GetHashCode_m2736 ();
extern "C" void X509Extension_WriteLine_m2737 ();
extern "C" void X509Extension_ToString_m2738 ();
extern "C" void X509ExtensionCollection__ctor_m2739 ();
extern "C" void X509ExtensionCollection__ctor_m2740 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m2741 ();
extern "C" void X509ExtensionCollection_IndexOf_m2742 ();
extern "C" void X509ExtensionCollection_get_Item_m2345 ();
extern "C" void X509Store__ctor_m2743 ();
extern "C" void X509Store_get_Certificates_m2367 ();
extern "C" void X509Store_get_Crls_m2353 ();
extern "C" void X509Store_Load_m2744 ();
extern "C" void X509Store_LoadCertificate_m2745 ();
extern "C" void X509Store_LoadCrl_m2746 ();
extern "C" void X509Store_CheckStore_m2747 ();
extern "C" void X509Store_BuildCertificatesCollection_m2748 ();
extern "C" void X509Store_BuildCrlsCollection_m2749 ();
extern "C" void X509StoreManager_get_CurrentUser_m2364 ();
extern "C" void X509StoreManager_get_LocalMachine_m2365 ();
extern "C" void X509StoreManager_get_TrustedRootCertificates_m2750 ();
extern "C" void X509Stores__ctor_m2751 ();
extern "C" void X509Stores_get_TrustedRoot_m2752 ();
extern "C" void X509Stores_Open_m2366 ();
extern "C" void AuthorityKeyIdentifierExtension__ctor_m2347 ();
extern "C" void AuthorityKeyIdentifierExtension_Decode_m2753 ();
extern "C" void AuthorityKeyIdentifierExtension_get_Identifier_m2348 ();
extern "C" void AuthorityKeyIdentifierExtension_ToString_m2754 ();
extern "C" void BasicConstraintsExtension__ctor_m2755 ();
extern "C" void BasicConstraintsExtension_Decode_m2756 ();
extern "C" void BasicConstraintsExtension_Encode_m2757 ();
extern "C" void BasicConstraintsExtension_get_CertificateAuthority_m2758 ();
extern "C" void BasicConstraintsExtension_ToString_m2759 ();
extern "C" void ExtendedKeyUsageExtension__ctor_m2760 ();
extern "C" void ExtendedKeyUsageExtension_Decode_m2761 ();
extern "C" void ExtendedKeyUsageExtension_Encode_m2762 ();
extern "C" void ExtendedKeyUsageExtension_get_KeyPurpose_m2763 ();
extern "C" void ExtendedKeyUsageExtension_ToString_m2764 ();
extern "C" void GeneralNames__ctor_m2765 ();
extern "C" void GeneralNames_get_DNSNames_m2766 ();
extern "C" void GeneralNames_get_IPAddresses_m2767 ();
extern "C" void GeneralNames_ToString_m2768 ();
extern "C" void KeyUsageExtension__ctor_m2769 ();
extern "C" void KeyUsageExtension_Decode_m2770 ();
extern "C" void KeyUsageExtension_Encode_m2771 ();
extern "C" void KeyUsageExtension_Support_m2772 ();
extern "C" void KeyUsageExtension_ToString_m2773 ();
extern "C" void NetscapeCertTypeExtension__ctor_m2774 ();
extern "C" void NetscapeCertTypeExtension_Decode_m2775 ();
extern "C" void NetscapeCertTypeExtension_Support_m2776 ();
extern "C" void NetscapeCertTypeExtension_ToString_m2777 ();
extern "C" void SubjectAltNameExtension__ctor_m2778 ();
extern "C" void SubjectAltNameExtension_Decode_m2779 ();
extern "C" void SubjectAltNameExtension_get_DNSNames_m2780 ();
extern "C" void SubjectAltNameExtension_get_IPAddresses_m2781 ();
extern "C" void SubjectAltNameExtension_ToString_m2782 ();
extern "C" void HMAC__ctor_m2783 ();
extern "C" void HMAC_get_Key_m2784 ();
extern "C" void HMAC_set_Key_m2785 ();
extern "C" void HMAC_Initialize_m2786 ();
extern "C" void HMAC_HashFinal_m2787 ();
extern "C" void HMAC_HashCore_m2788 ();
extern "C" void HMAC_initializePad_m2789 ();
extern "C" void MD5SHA1__ctor_m2790 ();
extern "C" void MD5SHA1_Initialize_m2791 ();
extern "C" void MD5SHA1_HashFinal_m2792 ();
extern "C" void MD5SHA1_HashCore_m2793 ();
extern "C" void MD5SHA1_CreateSignature_m2794 ();
extern "C" void MD5SHA1_VerifySignature_m2795 ();
extern "C" void Alert__ctor_m2796 ();
extern "C" void Alert__ctor_m2797 ();
extern "C" void Alert_get_Level_m2798 ();
extern "C" void Alert_get_Description_m2799 ();
extern "C" void Alert_get_IsWarning_m2800 ();
extern "C" void Alert_get_IsCloseNotify_m2801 ();
extern "C" void Alert_inferAlertLevel_m2802 ();
extern "C" void Alert_GetAlertMessage_m2803 ();
extern "C" void CipherSuite__ctor_m2804 ();
extern "C" void CipherSuite__cctor_m2805 ();
extern "C" void CipherSuite_get_EncryptionCipher_m2806 ();
extern "C" void CipherSuite_get_DecryptionCipher_m2807 ();
extern "C" void CipherSuite_get_ClientHMAC_m2808 ();
extern "C" void CipherSuite_get_ServerHMAC_m2809 ();
extern "C" void CipherSuite_get_CipherAlgorithmType_m2810 ();
extern "C" void CipherSuite_get_HashAlgorithmName_m2811 ();
extern "C" void CipherSuite_get_HashAlgorithmType_m2812 ();
extern "C" void CipherSuite_get_HashSize_m2813 ();
extern "C" void CipherSuite_get_ExchangeAlgorithmType_m2814 ();
extern "C" void CipherSuite_get_CipherMode_m2815 ();
extern "C" void CipherSuite_get_Code_m2816 ();
extern "C" void CipherSuite_get_Name_m2817 ();
extern "C" void CipherSuite_get_IsExportable_m2818 ();
extern "C" void CipherSuite_get_KeyMaterialSize_m2819 ();
extern "C" void CipherSuite_get_KeyBlockSize_m2820 ();
extern "C" void CipherSuite_get_ExpandedKeyMaterialSize_m2821 ();
extern "C" void CipherSuite_get_EffectiveKeyBits_m2822 ();
extern "C" void CipherSuite_get_IvSize_m2823 ();
extern "C" void CipherSuite_get_Context_m2824 ();
extern "C" void CipherSuite_set_Context_m2825 ();
extern "C" void CipherSuite_Write_m2826 ();
extern "C" void CipherSuite_Write_m2827 ();
extern "C" void CipherSuite_InitializeCipher_m2828 ();
extern "C" void CipherSuite_EncryptRecord_m2829 ();
extern "C" void CipherSuite_DecryptRecord_m2830 ();
extern "C" void CipherSuite_CreatePremasterSecret_m2831 ();
extern "C" void CipherSuite_PRF_m2832 ();
extern "C" void CipherSuite_Expand_m2833 ();
extern "C" void CipherSuite_createEncryptionCipher_m2834 ();
extern "C" void CipherSuite_createDecryptionCipher_m2835 ();
extern "C" void CipherSuiteCollection__ctor_m2836 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_get_Item_m2837 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_set_Item_m2838 ();
extern "C" void CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m2839 ();
extern "C" void CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m2840 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Contains_m2841 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_IndexOf_m2842 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Insert_m2843 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Remove_m2844 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_RemoveAt_m2845 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Add_m2846 ();
extern "C" void CipherSuiteCollection_get_Item_m2847 ();
extern "C" void CipherSuiteCollection_get_Item_m2848 ();
extern "C" void CipherSuiteCollection_set_Item_m2849 ();
extern "C" void CipherSuiteCollection_get_Item_m2850 ();
extern "C" void CipherSuiteCollection_get_Count_m2851 ();
extern "C" void CipherSuiteCollection_CopyTo_m2852 ();
extern "C" void CipherSuiteCollection_Clear_m2853 ();
extern "C" void CipherSuiteCollection_IndexOf_m2854 ();
extern "C" void CipherSuiteCollection_IndexOf_m2855 ();
extern "C" void CipherSuiteCollection_Add_m2856 ();
extern "C" void CipherSuiteCollection_add_m2857 ();
extern "C" void CipherSuiteCollection_add_m2858 ();
extern "C" void CipherSuiteCollection_cultureAwareCompare_m2859 ();
extern "C" void CipherSuiteFactory_GetSupportedCiphers_m2860 ();
extern "C" void CipherSuiteFactory_GetTls1SupportedCiphers_m2861 ();
extern "C" void CipherSuiteFactory_GetSsl3SupportedCiphers_m2862 ();
extern "C" void ClientContext__ctor_m2863 ();
extern "C" void ClientContext_get_SslStream_m2864 ();
extern "C" void ClientContext_get_ClientHelloProtocol_m2865 ();
extern "C" void ClientContext_set_ClientHelloProtocol_m2866 ();
extern "C" void ClientContext_Clear_m2867 ();
extern "C" void ClientRecordProtocol__ctor_m2868 ();
extern "C" void ClientRecordProtocol_GetMessage_m2869 ();
extern "C" void ClientRecordProtocol_ProcessHandshakeMessage_m2870 ();
extern "C" void ClientRecordProtocol_createClientHandshakeMessage_m2871 ();
extern "C" void ClientRecordProtocol_createServerHandshakeMessage_m2872 ();
extern "C" void ClientSessionInfo__ctor_m2873 ();
extern "C" void ClientSessionInfo__cctor_m2874 ();
extern "C" void ClientSessionInfo_Finalize_m2875 ();
extern "C" void ClientSessionInfo_get_HostName_m2876 ();
extern "C" void ClientSessionInfo_get_Id_m2877 ();
extern "C" void ClientSessionInfo_get_Valid_m2878 ();
extern "C" void ClientSessionInfo_GetContext_m2879 ();
extern "C" void ClientSessionInfo_SetContext_m2880 ();
extern "C" void ClientSessionInfo_KeepAlive_m2881 ();
extern "C" void ClientSessionInfo_Dispose_m2882 ();
extern "C" void ClientSessionInfo_Dispose_m2883 ();
extern "C" void ClientSessionInfo_CheckDisposed_m2884 ();
extern "C" void ClientSessionCache__cctor_m2885 ();
extern "C" void ClientSessionCache_Add_m2886 ();
extern "C" void ClientSessionCache_FromHost_m2887 ();
extern "C" void ClientSessionCache_FromContext_m2888 ();
extern "C" void ClientSessionCache_SetContextInCache_m2889 ();
extern "C" void ClientSessionCache_SetContextFromCache_m2890 ();
extern "C" void Context__ctor_m2891 ();
extern "C" void Context_get_AbbreviatedHandshake_m2892 ();
extern "C" void Context_set_AbbreviatedHandshake_m2893 ();
extern "C" void Context_get_ProtocolNegotiated_m2894 ();
extern "C" void Context_set_ProtocolNegotiated_m2895 ();
extern "C" void Context_get_SecurityProtocol_m2896 ();
extern "C" void Context_set_SecurityProtocol_m2897 ();
extern "C" void Context_get_SecurityProtocolFlags_m2898 ();
extern "C" void Context_get_Protocol_m2899 ();
extern "C" void Context_get_SessionId_m2900 ();
extern "C" void Context_set_SessionId_m2901 ();
extern "C" void Context_get_CompressionMethod_m2902 ();
extern "C" void Context_set_CompressionMethod_m2903 ();
extern "C" void Context_get_ServerSettings_m2904 ();
extern "C" void Context_get_ClientSettings_m2905 ();
extern "C" void Context_get_LastHandshakeMsg_m2906 ();
extern "C" void Context_set_LastHandshakeMsg_m2907 ();
extern "C" void Context_get_HandshakeState_m2908 ();
extern "C" void Context_set_HandshakeState_m2909 ();
extern "C" void Context_get_ReceivedConnectionEnd_m2910 ();
extern "C" void Context_set_ReceivedConnectionEnd_m2911 ();
extern "C" void Context_get_SentConnectionEnd_m2912 ();
extern "C" void Context_set_SentConnectionEnd_m2913 ();
extern "C" void Context_get_SupportedCiphers_m2914 ();
extern "C" void Context_set_SupportedCiphers_m2915 ();
extern "C" void Context_get_HandshakeMessages_m2916 ();
extern "C" void Context_get_WriteSequenceNumber_m2917 ();
extern "C" void Context_set_WriteSequenceNumber_m2918 ();
extern "C" void Context_get_ReadSequenceNumber_m2919 ();
extern "C" void Context_set_ReadSequenceNumber_m2920 ();
extern "C" void Context_get_ClientRandom_m2921 ();
extern "C" void Context_set_ClientRandom_m2922 ();
extern "C" void Context_get_ServerRandom_m2923 ();
extern "C" void Context_set_ServerRandom_m2924 ();
extern "C" void Context_get_RandomCS_m2925 ();
extern "C" void Context_set_RandomCS_m2926 ();
extern "C" void Context_get_RandomSC_m2927 ();
extern "C" void Context_set_RandomSC_m2928 ();
extern "C" void Context_get_MasterSecret_m2929 ();
extern "C" void Context_set_MasterSecret_m2930 ();
extern "C" void Context_get_ClientWriteKey_m2931 ();
extern "C" void Context_set_ClientWriteKey_m2932 ();
extern "C" void Context_get_ServerWriteKey_m2933 ();
extern "C" void Context_set_ServerWriteKey_m2934 ();
extern "C" void Context_get_ClientWriteIV_m2935 ();
extern "C" void Context_set_ClientWriteIV_m2936 ();
extern "C" void Context_get_ServerWriteIV_m2937 ();
extern "C" void Context_set_ServerWriteIV_m2938 ();
extern "C" void Context_get_RecordProtocol_m2939 ();
extern "C" void Context_set_RecordProtocol_m2940 ();
extern "C" void Context_GetUnixTime_m2941 ();
extern "C" void Context_GetSecureRandomBytes_m2942 ();
extern "C" void Context_Clear_m2943 ();
extern "C" void Context_ClearKeyInfo_m2944 ();
extern "C" void Context_DecodeProtocolCode_m2945 ();
extern "C" void Context_ChangeProtocol_m2946 ();
extern "C" void Context_get_Current_m2947 ();
extern "C" void Context_get_Negotiating_m2948 ();
extern "C" void Context_get_Read_m2949 ();
extern "C" void Context_get_Write_m2950 ();
extern "C" void Context_StartSwitchingSecurityParameters_m2951 ();
extern "C" void Context_EndSwitchingSecurityParameters_m2952 ();
extern "C" void HttpsClientStream__ctor_m2953 ();
extern "C" void HttpsClientStream_get_TrustFailure_m2954 ();
extern "C" void HttpsClientStream_RaiseServerCertificateValidation_m2955 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__0_m2956 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__1_m2957 ();
extern "C" void ReceiveRecordAsyncResult__ctor_m2958 ();
extern "C" void ReceiveRecordAsyncResult_get_Record_m2959 ();
extern "C" void ReceiveRecordAsyncResult_get_ResultingBuffer_m2960 ();
extern "C" void ReceiveRecordAsyncResult_get_InitialBuffer_m2961 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncState_m2962 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncException_m2963 ();
extern "C" void ReceiveRecordAsyncResult_get_CompletedWithError_m2964 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncWaitHandle_m2965 ();
extern "C" void ReceiveRecordAsyncResult_get_IsCompleted_m2966 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2967 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2968 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2969 ();
extern "C" void SendRecordAsyncResult__ctor_m2970 ();
extern "C" void SendRecordAsyncResult_get_Message_m2971 ();
extern "C" void SendRecordAsyncResult_get_AsyncState_m2972 ();
extern "C" void SendRecordAsyncResult_get_AsyncException_m2973 ();
extern "C" void SendRecordAsyncResult_get_CompletedWithError_m2974 ();
extern "C" void SendRecordAsyncResult_get_AsyncWaitHandle_m2975 ();
extern "C" void SendRecordAsyncResult_get_IsCompleted_m2976 ();
extern "C" void SendRecordAsyncResult_SetComplete_m2977 ();
extern "C" void SendRecordAsyncResult_SetComplete_m2978 ();
extern "C" void RecordProtocol__ctor_m2979 ();
extern "C" void RecordProtocol__cctor_m2980 ();
extern "C" void RecordProtocol_get_Context_m2981 ();
extern "C" void RecordProtocol_SendRecord_m2982 ();
extern "C" void RecordProtocol_ProcessChangeCipherSpec_m2983 ();
extern "C" void RecordProtocol_GetMessage_m2984 ();
extern "C" void RecordProtocol_BeginReceiveRecord_m2985 ();
extern "C" void RecordProtocol_InternalReceiveRecordCallback_m2986 ();
extern "C" void RecordProtocol_EndReceiveRecord_m2987 ();
extern "C" void RecordProtocol_ReceiveRecord_m2988 ();
extern "C" void RecordProtocol_ReadRecordBuffer_m2989 ();
extern "C" void RecordProtocol_ReadClientHelloV2_m2990 ();
extern "C" void RecordProtocol_ReadStandardRecordBuffer_m2991 ();
extern "C" void RecordProtocol_ProcessAlert_m2992 ();
extern "C" void RecordProtocol_SendAlert_m2993 ();
extern "C" void RecordProtocol_SendAlert_m2994 ();
extern "C" void RecordProtocol_SendAlert_m2995 ();
extern "C" void RecordProtocol_SendChangeCipherSpec_m2996 ();
extern "C" void RecordProtocol_BeginSendRecord_m2997 ();
extern "C" void RecordProtocol_InternalSendRecordCallback_m2998 ();
extern "C" void RecordProtocol_BeginSendRecord_m2999 ();
extern "C" void RecordProtocol_EndSendRecord_m3000 ();
extern "C" void RecordProtocol_SendRecord_m3001 ();
extern "C" void RecordProtocol_EncodeRecord_m3002 ();
extern "C" void RecordProtocol_EncodeRecord_m3003 ();
extern "C" void RecordProtocol_encryptRecordFragment_m3004 ();
extern "C" void RecordProtocol_decryptRecordFragment_m3005 ();
extern "C" void RecordProtocol_Compare_m3006 ();
extern "C" void RecordProtocol_ProcessCipherSpecV2Buffer_m3007 ();
extern "C" void RecordProtocol_MapV2CipherCode_m3008 ();
extern "C" void RSASslSignatureDeformatter__ctor_m3009 ();
extern "C" void RSASslSignatureDeformatter_VerifySignature_m3010 ();
extern "C" void RSASslSignatureDeformatter_SetHashAlgorithm_m3011 ();
extern "C" void RSASslSignatureDeformatter_SetKey_m3012 ();
extern "C" void RSASslSignatureFormatter__ctor_m3013 ();
extern "C" void RSASslSignatureFormatter_CreateSignature_m3014 ();
extern "C" void RSASslSignatureFormatter_SetHashAlgorithm_m3015 ();
extern "C" void RSASslSignatureFormatter_SetKey_m3016 ();
extern "C" void SecurityParameters__ctor_m3017 ();
extern "C" void SecurityParameters_get_Cipher_m3018 ();
extern "C" void SecurityParameters_set_Cipher_m3019 ();
extern "C" void SecurityParameters_get_ClientWriteMAC_m3020 ();
extern "C" void SecurityParameters_set_ClientWriteMAC_m3021 ();
extern "C" void SecurityParameters_get_ServerWriteMAC_m3022 ();
extern "C" void SecurityParameters_set_ServerWriteMAC_m3023 ();
extern "C" void SecurityParameters_Clear_m3024 ();
extern "C" void ValidationResult_get_Trusted_m3025 ();
extern "C" void ValidationResult_get_ErrorCode_m3026 ();
extern "C" void SslClientStream__ctor_m3027 ();
extern "C" void SslClientStream__ctor_m3028 ();
extern "C" void SslClientStream__ctor_m3029 ();
extern "C" void SslClientStream__ctor_m3030 ();
extern "C" void SslClientStream__ctor_m3031 ();
extern "C" void SslClientStream_add_ServerCertValidation_m3032 ();
extern "C" void SslClientStream_remove_ServerCertValidation_m3033 ();
extern "C" void SslClientStream_add_ClientCertSelection_m3034 ();
extern "C" void SslClientStream_remove_ClientCertSelection_m3035 ();
extern "C" void SslClientStream_add_PrivateKeySelection_m3036 ();
extern "C" void SslClientStream_remove_PrivateKeySelection_m3037 ();
extern "C" void SslClientStream_add_ServerCertValidation2_m3038 ();
extern "C" void SslClientStream_remove_ServerCertValidation2_m3039 ();
extern "C" void SslClientStream_get_InputBuffer_m3040 ();
extern "C" void SslClientStream_get_ClientCertificates_m3041 ();
extern "C" void SslClientStream_get_SelectedClientCertificate_m3042 ();
extern "C" void SslClientStream_get_ServerCertValidationDelegate_m3043 ();
extern "C" void SslClientStream_set_ServerCertValidationDelegate_m3044 ();
extern "C" void SslClientStream_get_ClientCertSelectionDelegate_m3045 ();
extern "C" void SslClientStream_set_ClientCertSelectionDelegate_m3046 ();
extern "C" void SslClientStream_get_PrivateKeyCertSelectionDelegate_m3047 ();
extern "C" void SslClientStream_set_PrivateKeyCertSelectionDelegate_m3048 ();
extern "C" void SslClientStream_Finalize_m3049 ();
extern "C" void SslClientStream_Dispose_m3050 ();
extern "C" void SslClientStream_OnBeginNegotiateHandshake_m3051 ();
extern "C" void SslClientStream_SafeReceiveRecord_m3052 ();
extern "C" void SslClientStream_OnNegotiateHandshakeCallback_m3053 ();
extern "C" void SslClientStream_OnLocalCertificateSelection_m3054 ();
extern "C" void SslClientStream_get_HaveRemoteValidation2Callback_m3055 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation2_m3056 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation_m3057 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation_m3058 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation2_m3059 ();
extern "C" void SslClientStream_RaiseClientCertificateSelection_m3060 ();
extern "C" void SslClientStream_OnLocalPrivateKeySelection_m3061 ();
extern "C" void SslClientStream_RaisePrivateKeySelection_m3062 ();
extern "C" void SslCipherSuite__ctor_m3063 ();
extern "C" void SslCipherSuite_ComputeServerRecordMAC_m3064 ();
extern "C" void SslCipherSuite_ComputeClientRecordMAC_m3065 ();
extern "C" void SslCipherSuite_ComputeMasterSecret_m3066 ();
extern "C" void SslCipherSuite_ComputeKeys_m3067 ();
extern "C" void SslCipherSuite_prf_m3068 ();
extern "C" void SslHandshakeHash__ctor_m3069 ();
extern "C" void SslHandshakeHash_Initialize_m3070 ();
extern "C" void SslHandshakeHash_HashFinal_m3071 ();
extern "C" void SslHandshakeHash_HashCore_m3072 ();
extern "C" void SslHandshakeHash_CreateSignature_m3073 ();
extern "C" void SslHandshakeHash_initializePad_m3074 ();
extern "C" void InternalAsyncResult__ctor_m3075 ();
extern "C" void InternalAsyncResult_get_ProceedAfterHandshake_m3076 ();
extern "C" void InternalAsyncResult_get_FromWrite_m3077 ();
extern "C" void InternalAsyncResult_get_Buffer_m3078 ();
extern "C" void InternalAsyncResult_get_Offset_m3079 ();
extern "C" void InternalAsyncResult_get_Count_m3080 ();
extern "C" void InternalAsyncResult_get_BytesRead_m3081 ();
extern "C" void InternalAsyncResult_get_AsyncState_m3082 ();
extern "C" void InternalAsyncResult_get_AsyncException_m3083 ();
extern "C" void InternalAsyncResult_get_CompletedWithError_m3084 ();
extern "C" void InternalAsyncResult_get_AsyncWaitHandle_m3085 ();
extern "C" void InternalAsyncResult_get_IsCompleted_m3086 ();
extern "C" void InternalAsyncResult_SetComplete_m3087 ();
extern "C" void InternalAsyncResult_SetComplete_m3088 ();
extern "C" void InternalAsyncResult_SetComplete_m3089 ();
extern "C" void InternalAsyncResult_SetComplete_m3090 ();
extern "C" void SslStreamBase__ctor_m3091 ();
extern "C" void SslStreamBase__cctor_m3092 ();
extern "C" void SslStreamBase_AsyncHandshakeCallback_m3093 ();
extern "C" void SslStreamBase_get_MightNeedHandshake_m3094 ();
extern "C" void SslStreamBase_NegotiateHandshake_m3095 ();
extern "C" void SslStreamBase_RaiseLocalCertificateSelection_m3096 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation_m3097 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation2_m3098 ();
extern "C" void SslStreamBase_RaiseLocalPrivateKeySelection_m3099 ();
extern "C" void SslStreamBase_get_CheckCertRevocationStatus_m3100 ();
extern "C" void SslStreamBase_set_CheckCertRevocationStatus_m3101 ();
extern "C" void SslStreamBase_get_CipherAlgorithm_m3102 ();
extern "C" void SslStreamBase_get_CipherStrength_m3103 ();
extern "C" void SslStreamBase_get_HashAlgorithm_m3104 ();
extern "C" void SslStreamBase_get_HashStrength_m3105 ();
extern "C" void SslStreamBase_get_KeyExchangeStrength_m3106 ();
extern "C" void SslStreamBase_get_KeyExchangeAlgorithm_m3107 ();
extern "C" void SslStreamBase_get_SecurityProtocol_m3108 ();
extern "C" void SslStreamBase_get_ServerCertificate_m3109 ();
extern "C" void SslStreamBase_get_ServerCertificates_m3110 ();
extern "C" void SslStreamBase_BeginNegotiateHandshake_m3111 ();
extern "C" void SslStreamBase_EndNegotiateHandshake_m3112 ();
extern "C" void SslStreamBase_BeginRead_m3113 ();
extern "C" void SslStreamBase_InternalBeginRead_m3114 ();
extern "C" void SslStreamBase_InternalReadCallback_m3115 ();
extern "C" void SslStreamBase_InternalBeginWrite_m3116 ();
extern "C" void SslStreamBase_InternalWriteCallback_m3117 ();
extern "C" void SslStreamBase_BeginWrite_m3118 ();
extern "C" void SslStreamBase_EndRead_m3119 ();
extern "C" void SslStreamBase_EndWrite_m3120 ();
extern "C" void SslStreamBase_Close_m3121 ();
extern "C" void SslStreamBase_Flush_m3122 ();
extern "C" void SslStreamBase_Read_m3123 ();
extern "C" void SslStreamBase_Read_m3124 ();
extern "C" void SslStreamBase_Seek_m3125 ();
extern "C" void SslStreamBase_SetLength_m3126 ();
extern "C" void SslStreamBase_Write_m3127 ();
extern "C" void SslStreamBase_Write_m3128 ();
extern "C" void SslStreamBase_get_CanRead_m3129 ();
extern "C" void SslStreamBase_get_CanSeek_m3130 ();
extern "C" void SslStreamBase_get_CanWrite_m3131 ();
extern "C" void SslStreamBase_get_Length_m3132 ();
extern "C" void SslStreamBase_get_Position_m3133 ();
extern "C" void SslStreamBase_set_Position_m3134 ();
extern "C" void SslStreamBase_Finalize_m3135 ();
extern "C" void SslStreamBase_Dispose_m3136 ();
extern "C" void SslStreamBase_resetBuffer_m3137 ();
extern "C" void SslStreamBase_checkDisposed_m3138 ();
extern "C" void TlsCipherSuite__ctor_m3139 ();
extern "C" void TlsCipherSuite_ComputeServerRecordMAC_m3140 ();
extern "C" void TlsCipherSuite_ComputeClientRecordMAC_m3141 ();
extern "C" void TlsCipherSuite_ComputeMasterSecret_m3142 ();
extern "C" void TlsCipherSuite_ComputeKeys_m3143 ();
extern "C" void TlsClientSettings__ctor_m3144 ();
extern "C" void TlsClientSettings_get_TargetHost_m3145 ();
extern "C" void TlsClientSettings_set_TargetHost_m3146 ();
extern "C" void TlsClientSettings_get_Certificates_m3147 ();
extern "C" void TlsClientSettings_set_Certificates_m3148 ();
extern "C" void TlsClientSettings_get_ClientCertificate_m3149 ();
extern "C" void TlsClientSettings_set_ClientCertificate_m3150 ();
extern "C" void TlsClientSettings_UpdateCertificateRSA_m3151 ();
extern "C" void TlsException__ctor_m3152 ();
extern "C" void TlsException__ctor_m3153 ();
extern "C" void TlsException__ctor_m3154 ();
extern "C" void TlsException__ctor_m3155 ();
extern "C" void TlsException__ctor_m3156 ();
extern "C" void TlsException__ctor_m3157 ();
extern "C" void TlsException_get_Alert_m3158 ();
extern "C" void TlsServerSettings__ctor_m3159 ();
extern "C" void TlsServerSettings_get_ServerKeyExchange_m3160 ();
extern "C" void TlsServerSettings_set_ServerKeyExchange_m3161 ();
extern "C" void TlsServerSettings_get_Certificates_m3162 ();
extern "C" void TlsServerSettings_set_Certificates_m3163 ();
extern "C" void TlsServerSettings_get_CertificateRSA_m3164 ();
extern "C" void TlsServerSettings_get_RsaParameters_m3165 ();
extern "C" void TlsServerSettings_set_RsaParameters_m3166 ();
extern "C" void TlsServerSettings_set_SignedParams_m3167 ();
extern "C" void TlsServerSettings_get_CertificateRequest_m3168 ();
extern "C" void TlsServerSettings_set_CertificateRequest_m3169 ();
extern "C" void TlsServerSettings_set_CertificateTypes_m3170 ();
extern "C" void TlsServerSettings_set_DistinguisedNames_m3171 ();
extern "C" void TlsServerSettings_UpdateCertificateRSA_m3172 ();
extern "C" void TlsStream__ctor_m3173 ();
extern "C" void TlsStream__ctor_m3174 ();
extern "C" void TlsStream_get_EOF_m3175 ();
extern "C" void TlsStream_get_CanWrite_m3176 ();
extern "C" void TlsStream_get_CanRead_m3177 ();
extern "C" void TlsStream_get_CanSeek_m3178 ();
extern "C" void TlsStream_get_Position_m3179 ();
extern "C" void TlsStream_set_Position_m3180 ();
extern "C" void TlsStream_get_Length_m3181 ();
extern "C" void TlsStream_ReadSmallValue_m3182 ();
extern "C" void TlsStream_ReadByte_m3183 ();
extern "C" void TlsStream_ReadInt16_m3184 ();
extern "C" void TlsStream_ReadInt24_m3185 ();
extern "C" void TlsStream_ReadBytes_m3186 ();
extern "C" void TlsStream_Write_m3187 ();
extern "C" void TlsStream_Write_m3188 ();
extern "C" void TlsStream_WriteInt24_m3189 ();
extern "C" void TlsStream_Write_m3190 ();
extern "C" void TlsStream_Write_m3191 ();
extern "C" void TlsStream_Reset_m3192 ();
extern "C" void TlsStream_ToArray_m3193 ();
extern "C" void TlsStream_Flush_m3194 ();
extern "C" void TlsStream_SetLength_m3195 ();
extern "C" void TlsStream_Seek_m3196 ();
extern "C" void TlsStream_Read_m3197 ();
extern "C" void TlsStream_Write_m3198 ();
extern "C" void HandshakeMessage__ctor_m3199 ();
extern "C" void HandshakeMessage__ctor_m3200 ();
extern "C" void HandshakeMessage__ctor_m3201 ();
extern "C" void HandshakeMessage_get_Context_m3202 ();
extern "C" void HandshakeMessage_get_HandshakeType_m3203 ();
extern "C" void HandshakeMessage_get_ContentType_m3204 ();
extern "C" void HandshakeMessage_Process_m3205 ();
extern "C" void HandshakeMessage_Update_m3206 ();
extern "C" void HandshakeMessage_EncodeMessage_m3207 ();
extern "C" void HandshakeMessage_Compare_m3208 ();
extern "C" void TlsClientCertificate__ctor_m3209 ();
extern "C" void TlsClientCertificate_get_ClientCertificate_m3210 ();
extern "C" void TlsClientCertificate_Update_m3211 ();
extern "C" void TlsClientCertificate_GetClientCertificate_m3212 ();
extern "C" void TlsClientCertificate_SendCertificates_m3213 ();
extern "C" void TlsClientCertificate_ProcessAsSsl3_m3214 ();
extern "C" void TlsClientCertificate_ProcessAsTls1_m3215 ();
extern "C" void TlsClientCertificate_FindParentCertificate_m3216 ();
extern "C" void TlsClientCertificateVerify__ctor_m3217 ();
extern "C" void TlsClientCertificateVerify_Update_m3218 ();
extern "C" void TlsClientCertificateVerify_ProcessAsSsl3_m3219 ();
extern "C" void TlsClientCertificateVerify_ProcessAsTls1_m3220 ();
extern "C" void TlsClientCertificateVerify_getClientCertRSA_m3221 ();
extern "C" void TlsClientCertificateVerify_getUnsignedBigInteger_m3222 ();
extern "C" void TlsClientFinished__ctor_m3223 ();
extern "C" void TlsClientFinished__cctor_m3224 ();
extern "C" void TlsClientFinished_Update_m3225 ();
extern "C" void TlsClientFinished_ProcessAsSsl3_m3226 ();
extern "C" void TlsClientFinished_ProcessAsTls1_m3227 ();
extern "C" void TlsClientHello__ctor_m3228 ();
extern "C" void TlsClientHello_Update_m3229 ();
extern "C" void TlsClientHello_ProcessAsSsl3_m3230 ();
extern "C" void TlsClientHello_ProcessAsTls1_m3231 ();
extern "C" void TlsClientKeyExchange__ctor_m3232 ();
extern "C" void TlsClientKeyExchange_ProcessAsSsl3_m3233 ();
extern "C" void TlsClientKeyExchange_ProcessAsTls1_m3234 ();
extern "C" void TlsClientKeyExchange_ProcessCommon_m3235 ();
extern "C" void TlsServerCertificate__ctor_m3236 ();
extern "C" void TlsServerCertificate_Update_m3237 ();
extern "C" void TlsServerCertificate_ProcessAsSsl3_m3238 ();
extern "C" void TlsServerCertificate_ProcessAsTls1_m3239 ();
extern "C" void TlsServerCertificate_checkCertificateUsage_m3240 ();
extern "C" void TlsServerCertificate_validateCertificates_m3241 ();
extern "C" void TlsServerCertificate_checkServerIdentity_m3242 ();
extern "C" void TlsServerCertificate_checkDomainName_m3243 ();
extern "C" void TlsServerCertificate_Match_m3244 ();
extern "C" void TlsServerCertificateRequest__ctor_m3245 ();
extern "C" void TlsServerCertificateRequest_Update_m3246 ();
extern "C" void TlsServerCertificateRequest_ProcessAsSsl3_m3247 ();
extern "C" void TlsServerCertificateRequest_ProcessAsTls1_m3248 ();
extern "C" void TlsServerFinished__ctor_m3249 ();
extern "C" void TlsServerFinished__cctor_m3250 ();
extern "C" void TlsServerFinished_Update_m3251 ();
extern "C" void TlsServerFinished_ProcessAsSsl3_m3252 ();
extern "C" void TlsServerFinished_ProcessAsTls1_m3253 ();
extern "C" void TlsServerHello__ctor_m3254 ();
extern "C" void TlsServerHello_Update_m3255 ();
extern "C" void TlsServerHello_ProcessAsSsl3_m3256 ();
extern "C" void TlsServerHello_ProcessAsTls1_m3257 ();
extern "C" void TlsServerHello_processProtocol_m3258 ();
extern "C" void TlsServerHelloDone__ctor_m3259 ();
extern "C" void TlsServerHelloDone_ProcessAsSsl3_m3260 ();
extern "C" void TlsServerHelloDone_ProcessAsTls1_m3261 ();
extern "C" void TlsServerKeyExchange__ctor_m3262 ();
extern "C" void TlsServerKeyExchange_Update_m3263 ();
extern "C" void TlsServerKeyExchange_ProcessAsSsl3_m3264 ();
extern "C" void TlsServerKeyExchange_ProcessAsTls1_m3265 ();
extern "C" void TlsServerKeyExchange_verifySignature_m3266 ();
extern "C" void PrimalityTest__ctor_m3267 ();
extern "C" void PrimalityTest_Invoke_m3268 ();
extern "C" void PrimalityTest_BeginInvoke_m3269 ();
extern "C" void PrimalityTest_EndInvoke_m3270 ();
extern "C" void CertificateValidationCallback__ctor_m3271 ();
extern "C" void CertificateValidationCallback_Invoke_m3272 ();
extern "C" void CertificateValidationCallback_BeginInvoke_m3273 ();
extern "C" void CertificateValidationCallback_EndInvoke_m3274 ();
extern "C" void CertificateValidationCallback2__ctor_m3275 ();
extern "C" void CertificateValidationCallback2_Invoke_m3276 ();
extern "C" void CertificateValidationCallback2_BeginInvoke_m3277 ();
extern "C" void CertificateValidationCallback2_EndInvoke_m3278 ();
extern "C" void CertificateSelectionCallback__ctor_m3279 ();
extern "C" void CertificateSelectionCallback_Invoke_m3280 ();
extern "C" void CertificateSelectionCallback_BeginInvoke_m3281 ();
extern "C" void CertificateSelectionCallback_EndInvoke_m3282 ();
extern "C" void PrivateKeySelectionCallback__ctor_m3283 ();
extern "C" void PrivateKeySelectionCallback_Invoke_m3284 ();
extern "C" void PrivateKeySelectionCallback_BeginInvoke_m3285 ();
extern "C" void PrivateKeySelectionCallback_EndInvoke_m3286 ();
extern "C" void Object__ctor_m1164 ();
extern "C" void Object_Equals_m3361 ();
extern "C" void Object_Equals_m2406 ();
extern "C" void Object_Finalize_m1163 ();
extern "C" void Object_GetHashCode_m3362 ();
extern "C" void Object_GetType_m1185 ();
extern "C" void Object_MemberwiseClone_m3363 ();
extern "C" void Object_ToString_m1235 ();
extern "C" void Object_ReferenceEquals_m1193 ();
extern "C" void Object_InternalGetHashCode_m3364 ();
extern "C" void ValueType__ctor_m3365 ();
extern "C" void ValueType_InternalEquals_m3366 ();
extern "C" void ValueType_DefaultEquals_m3367 ();
extern "C" void ValueType_Equals_m3368 ();
extern "C" void ValueType_InternalGetHashCode_m3369 ();
extern "C" void ValueType_GetHashCode_m3370 ();
extern "C" void ValueType_ToString_m3371 ();
extern "C" void Attribute__ctor_m1198 ();
extern "C" void Attribute_CheckParameters_m3372 ();
extern "C" void Attribute_GetCustomAttribute_m3373 ();
extern "C" void Attribute_GetCustomAttribute_m3374 ();
extern "C" void Attribute_GetHashCode_m1335 ();
extern "C" void Attribute_IsDefined_m3375 ();
extern "C" void Attribute_IsDefined_m3376 ();
extern "C" void Attribute_IsDefined_m3377 ();
extern "C" void Attribute_IsDefined_m3378 ();
extern "C" void Attribute_Equals_m3379 ();
extern "C" void Int32_System_IConvertible_ToBoolean_m3380 ();
extern "C" void Int32_System_IConvertible_ToByte_m3381 ();
extern "C" void Int32_System_IConvertible_ToChar_m3382 ();
extern "C" void Int32_System_IConvertible_ToDateTime_m3383 ();
extern "C" void Int32_System_IConvertible_ToDecimal_m3384 ();
extern "C" void Int32_System_IConvertible_ToDouble_m3385 ();
extern "C" void Int32_System_IConvertible_ToInt16_m3386 ();
extern "C" void Int32_System_IConvertible_ToInt32_m3387 ();
extern "C" void Int32_System_IConvertible_ToInt64_m3388 ();
extern "C" void Int32_System_IConvertible_ToSByte_m3389 ();
extern "C" void Int32_System_IConvertible_ToSingle_m3390 ();
extern "C" void Int32_System_IConvertible_ToType_m3391 ();
extern "C" void Int32_System_IConvertible_ToUInt16_m3392 ();
extern "C" void Int32_System_IConvertible_ToUInt32_m3393 ();
extern "C" void Int32_System_IConvertible_ToUInt64_m3394 ();
extern "C" void Int32_CompareTo_m3395 ();
extern "C" void Int32_Equals_m3396 ();
extern "C" void Int32_GetHashCode_m1175 ();
extern "C" void Int32_CompareTo_m3397 ();
extern "C" void Int32_Equals_m1177 ();
extern "C" void Int32_ProcessTrailingWhitespace_m3398 ();
extern "C" void Int32_Parse_m3399 ();
extern "C" void Int32_Parse_m3400 ();
extern "C" void Int32_CheckStyle_m3401 ();
extern "C" void Int32_JumpOverWhite_m3402 ();
extern "C" void Int32_FindSign_m3403 ();
extern "C" void Int32_FindCurrency_m3404 ();
extern "C" void Int32_FindExponent_m3405 ();
extern "C" void Int32_FindOther_m3406 ();
extern "C" void Int32_ValidDigit_m3407 ();
extern "C" void Int32_GetFormatException_m3408 ();
extern "C" void Int32_Parse_m3409 ();
extern "C" void Int32_Parse_m2379 ();
extern "C" void Int32_Parse_m3410 ();
extern "C" void Int32_TryParse_m3411 ();
extern "C" void Int32_TryParse_m2262 ();
extern "C" void Int32_ToString_m1222 ();
extern "C" void Int32_ToString_m1282 ();
extern "C" void Int32_ToString_m2373 ();
extern "C" void Int32_ToString_m3330 ();
extern "C" void SerializableAttribute__ctor_m3412 ();
extern "C" void AttributeUsageAttribute__ctor_m3413 ();
extern "C" void AttributeUsageAttribute_get_AllowMultiple_m3414 ();
extern "C" void AttributeUsageAttribute_set_AllowMultiple_m3415 ();
extern "C" void AttributeUsageAttribute_get_Inherited_m3416 ();
extern "C" void AttributeUsageAttribute_set_Inherited_m3417 ();
extern "C" void ComVisibleAttribute__ctor_m3418 ();
extern "C" void Int64_System_IConvertible_ToBoolean_m3419 ();
extern "C" void Int64_System_IConvertible_ToByte_m3420 ();
extern "C" void Int64_System_IConvertible_ToChar_m3421 ();
extern "C" void Int64_System_IConvertible_ToDateTime_m3422 ();
extern "C" void Int64_System_IConvertible_ToDecimal_m3423 ();
extern "C" void Int64_System_IConvertible_ToDouble_m3424 ();
extern "C" void Int64_System_IConvertible_ToInt16_m3425 ();
extern "C" void Int64_System_IConvertible_ToInt32_m3426 ();
extern "C" void Int64_System_IConvertible_ToInt64_m3427 ();
extern "C" void Int64_System_IConvertible_ToSByte_m3428 ();
extern "C" void Int64_System_IConvertible_ToSingle_m3429 ();
extern "C" void Int64_System_IConvertible_ToType_m3430 ();
extern "C" void Int64_System_IConvertible_ToUInt16_m3431 ();
extern "C" void Int64_System_IConvertible_ToUInt32_m3432 ();
extern "C" void Int64_System_IConvertible_ToUInt64_m3433 ();
extern "C" void Int64_CompareTo_m3434 ();
extern "C" void Int64_Equals_m3435 ();
extern "C" void Int64_GetHashCode_m3436 ();
extern "C" void Int64_CompareTo_m3437 ();
extern "C" void Int64_Equals_m3438 ();
extern "C" void Int64_Parse_m3439 ();
extern "C" void Int64_Parse_m3440 ();
extern "C" void Int64_Parse_m3441 ();
extern "C" void Int64_Parse_m3442 ();
extern "C" void Int64_Parse_m3443 ();
extern "C" void Int64_TryParse_m3444 ();
extern "C" void Int64_TryParse_m1279 ();
extern "C" void Int64_ToString_m2261 ();
extern "C" void Int64_ToString_m1280 ();
extern "C" void Int64_ToString_m3445 ();
extern "C" void Int64_ToString_m3446 ();
extern "C" void UInt32_System_IConvertible_ToBoolean_m3447 ();
extern "C" void UInt32_System_IConvertible_ToByte_m3448 ();
extern "C" void UInt32_System_IConvertible_ToChar_m3449 ();
extern "C" void UInt32_System_IConvertible_ToDateTime_m3450 ();
extern "C" void UInt32_System_IConvertible_ToDecimal_m3451 ();
extern "C" void UInt32_System_IConvertible_ToDouble_m3452 ();
extern "C" void UInt32_System_IConvertible_ToInt16_m3453 ();
extern "C" void UInt32_System_IConvertible_ToInt32_m3454 ();
extern "C" void UInt32_System_IConvertible_ToInt64_m3455 ();
extern "C" void UInt32_System_IConvertible_ToSByte_m3456 ();
extern "C" void UInt32_System_IConvertible_ToSingle_m3457 ();
extern "C" void UInt32_System_IConvertible_ToType_m3458 ();
extern "C" void UInt32_System_IConvertible_ToUInt16_m3459 ();
extern "C" void UInt32_System_IConvertible_ToUInt32_m3460 ();
extern "C" void UInt32_System_IConvertible_ToUInt64_m3461 ();
extern "C" void UInt32_CompareTo_m3462 ();
extern "C" void UInt32_Equals_m3463 ();
extern "C" void UInt32_GetHashCode_m3464 ();
extern "C" void UInt32_CompareTo_m3465 ();
extern "C" void UInt32_Equals_m3466 ();
extern "C" void UInt32_Parse_m3467 ();
extern "C" void UInt32_Parse_m3468 ();
extern "C" void UInt32_Parse_m3469 ();
extern "C" void UInt32_Parse_m3470 ();
extern "C" void UInt32_TryParse_m2400 ();
extern "C" void UInt32_TryParse_m1273 ();
extern "C" void UInt32_ToString_m1257 ();
extern "C" void UInt32_ToString_m1283 ();
extern "C" void UInt32_ToString_m3471 ();
extern "C" void UInt32_ToString_m3472 ();
extern "C" void CLSCompliantAttribute__ctor_m3473 ();
extern "C" void UInt64_System_IConvertible_ToBoolean_m3474 ();
extern "C" void UInt64_System_IConvertible_ToByte_m3475 ();
extern "C" void UInt64_System_IConvertible_ToChar_m3476 ();
extern "C" void UInt64_System_IConvertible_ToDateTime_m3477 ();
extern "C" void UInt64_System_IConvertible_ToDecimal_m3478 ();
extern "C" void UInt64_System_IConvertible_ToDouble_m3479 ();
extern "C" void UInt64_System_IConvertible_ToInt16_m3480 ();
extern "C" void UInt64_System_IConvertible_ToInt32_m3481 ();
extern "C" void UInt64_System_IConvertible_ToInt64_m3482 ();
extern "C" void UInt64_System_IConvertible_ToSByte_m3483 ();
extern "C" void UInt64_System_IConvertible_ToSingle_m3484 ();
extern "C" void UInt64_System_IConvertible_ToType_m3485 ();
extern "C" void UInt64_System_IConvertible_ToUInt16_m3486 ();
extern "C" void UInt64_System_IConvertible_ToUInt32_m3487 ();
extern "C" void UInt64_System_IConvertible_ToUInt64_m3488 ();
extern "C" void UInt64_CompareTo_m3489 ();
extern "C" void UInt64_Equals_m3490 ();
extern "C" void UInt64_GetHashCode_m3491 ();
extern "C" void UInt64_CompareTo_m3492 ();
extern "C" void UInt64_Equals_m3493 ();
extern "C" void UInt64_Parse_m3494 ();
extern "C" void UInt64_Parse_m3495 ();
extern "C" void UInt64_Parse_m3496 ();
extern "C" void UInt64_TryParse_m1255 ();
extern "C" void UInt64_ToString_m3497 ();
extern "C" void UInt64_ToString_m1281 ();
extern "C" void UInt64_ToString_m3498 ();
extern "C" void UInt64_ToString_m3499 ();
extern "C" void Byte_System_IConvertible_ToType_m3500 ();
extern "C" void Byte_System_IConvertible_ToBoolean_m3501 ();
extern "C" void Byte_System_IConvertible_ToByte_m3502 ();
extern "C" void Byte_System_IConvertible_ToChar_m3503 ();
extern "C" void Byte_System_IConvertible_ToDateTime_m3504 ();
extern "C" void Byte_System_IConvertible_ToDecimal_m3505 ();
extern "C" void Byte_System_IConvertible_ToDouble_m3506 ();
extern "C" void Byte_System_IConvertible_ToInt16_m3507 ();
extern "C" void Byte_System_IConvertible_ToInt32_m3508 ();
extern "C" void Byte_System_IConvertible_ToInt64_m3509 ();
extern "C" void Byte_System_IConvertible_ToSByte_m3510 ();
extern "C" void Byte_System_IConvertible_ToSingle_m3511 ();
extern "C" void Byte_System_IConvertible_ToUInt16_m3512 ();
extern "C" void Byte_System_IConvertible_ToUInt32_m3513 ();
extern "C" void Byte_System_IConvertible_ToUInt64_m3514 ();
extern "C" void Byte_CompareTo_m3515 ();
extern "C" void Byte_Equals_m3516 ();
extern "C" void Byte_GetHashCode_m3517 ();
extern "C" void Byte_CompareTo_m3518 ();
extern "C" void Byte_Equals_m3519 ();
extern "C" void Byte_Parse_m3520 ();
extern "C" void Byte_Parse_m3521 ();
extern "C" void Byte_Parse_m3522 ();
extern "C" void Byte_TryParse_m3523 ();
extern "C" void Byte_TryParse_m3524 ();
extern "C" void Byte_ToString_m3328 ();
extern "C" void Byte_ToString_m2315 ();
extern "C" void Byte_ToString_m3291 ();
extern "C" void Byte_ToString_m3295 ();
extern "C" void SByte_System_IConvertible_ToBoolean_m3525 ();
extern "C" void SByte_System_IConvertible_ToByte_m3526 ();
extern "C" void SByte_System_IConvertible_ToChar_m3527 ();
extern "C" void SByte_System_IConvertible_ToDateTime_m3528 ();
extern "C" void SByte_System_IConvertible_ToDecimal_m3529 ();
extern "C" void SByte_System_IConvertible_ToDouble_m3530 ();
extern "C" void SByte_System_IConvertible_ToInt16_m3531 ();
extern "C" void SByte_System_IConvertible_ToInt32_m3532 ();
extern "C" void SByte_System_IConvertible_ToInt64_m3533 ();
extern "C" void SByte_System_IConvertible_ToSByte_m3534 ();
extern "C" void SByte_System_IConvertible_ToSingle_m3535 ();
extern "C" void SByte_System_IConvertible_ToType_m3536 ();
extern "C" void SByte_System_IConvertible_ToUInt16_m3537 ();
extern "C" void SByte_System_IConvertible_ToUInt32_m3538 ();
extern "C" void SByte_System_IConvertible_ToUInt64_m3539 ();
extern "C" void SByte_CompareTo_m3540 ();
extern "C" void SByte_Equals_m3541 ();
extern "C" void SByte_GetHashCode_m3542 ();
extern "C" void SByte_CompareTo_m3543 ();
extern "C" void SByte_Equals_m3544 ();
extern "C" void SByte_Parse_m3545 ();
extern "C" void SByte_Parse_m3546 ();
extern "C" void SByte_Parse_m3547 ();
extern "C" void SByte_TryParse_m3548 ();
extern "C" void SByte_ToString_m3549 ();
extern "C" void SByte_ToString_m3550 ();
extern "C" void SByte_ToString_m3551 ();
extern "C" void SByte_ToString_m3552 ();
extern "C" void Int16_System_IConvertible_ToBoolean_m3553 ();
extern "C" void Int16_System_IConvertible_ToByte_m3554 ();
extern "C" void Int16_System_IConvertible_ToChar_m3555 ();
extern "C" void Int16_System_IConvertible_ToDateTime_m3556 ();
extern "C" void Int16_System_IConvertible_ToDecimal_m3557 ();
extern "C" void Int16_System_IConvertible_ToDouble_m3558 ();
extern "C" void Int16_System_IConvertible_ToInt16_m3559 ();
extern "C" void Int16_System_IConvertible_ToInt32_m3560 ();
extern "C" void Int16_System_IConvertible_ToInt64_m3561 ();
extern "C" void Int16_System_IConvertible_ToSByte_m3562 ();
extern "C" void Int16_System_IConvertible_ToSingle_m3563 ();
extern "C" void Int16_System_IConvertible_ToType_m3564 ();
extern "C" void Int16_System_IConvertible_ToUInt16_m3565 ();
extern "C" void Int16_System_IConvertible_ToUInt32_m3566 ();
extern "C" void Int16_System_IConvertible_ToUInt64_m3567 ();
extern "C" void Int16_CompareTo_m3568 ();
extern "C" void Int16_Equals_m3569 ();
extern "C" void Int16_GetHashCode_m3570 ();
extern "C" void Int16_CompareTo_m3571 ();
extern "C" void Int16_Equals_m3572 ();
extern "C" void Int16_Parse_m3573 ();
extern "C" void Int16_Parse_m3574 ();
extern "C" void Int16_Parse_m3575 ();
extern "C" void Int16_TryParse_m3576 ();
extern "C" void Int16_ToString_m3577 ();
extern "C" void Int16_ToString_m3578 ();
extern "C" void Int16_ToString_m3579 ();
extern "C" void Int16_ToString_m3580 ();
extern "C" void UInt16_System_IConvertible_ToBoolean_m3581 ();
extern "C" void UInt16_System_IConvertible_ToByte_m3582 ();
extern "C" void UInt16_System_IConvertible_ToChar_m3583 ();
extern "C" void UInt16_System_IConvertible_ToDateTime_m3584 ();
extern "C" void UInt16_System_IConvertible_ToDecimal_m3585 ();
extern "C" void UInt16_System_IConvertible_ToDouble_m3586 ();
extern "C" void UInt16_System_IConvertible_ToInt16_m3587 ();
extern "C" void UInt16_System_IConvertible_ToInt32_m3588 ();
extern "C" void UInt16_System_IConvertible_ToInt64_m3589 ();
extern "C" void UInt16_System_IConvertible_ToSByte_m3590 ();
extern "C" void UInt16_System_IConvertible_ToSingle_m3591 ();
extern "C" void UInt16_System_IConvertible_ToType_m3592 ();
extern "C" void UInt16_System_IConvertible_ToUInt16_m3593 ();
extern "C" void UInt16_System_IConvertible_ToUInt32_m3594 ();
extern "C" void UInt16_System_IConvertible_ToUInt64_m3595 ();
extern "C" void UInt16_CompareTo_m3596 ();
extern "C" void UInt16_Equals_m3597 ();
extern "C" void UInt16_GetHashCode_m3598 ();
extern "C" void UInt16_CompareTo_m3599 ();
extern "C" void UInt16_Equals_m3600 ();
extern "C" void UInt16_Parse_m3601 ();
extern "C" void UInt16_Parse_m3602 ();
extern "C" void UInt16_TryParse_m3603 ();
extern "C" void UInt16_TryParse_m3604 ();
extern "C" void UInt16_ToString_m3605 ();
extern "C" void UInt16_ToString_m3606 ();
extern "C" void UInt16_ToString_m3607 ();
extern "C" void UInt16_ToString_m3608 ();
extern "C" void Char__cctor_m3609 ();
extern "C" void Char_System_IConvertible_ToType_m3610 ();
extern "C" void Char_System_IConvertible_ToBoolean_m3611 ();
extern "C" void Char_System_IConvertible_ToByte_m3612 ();
extern "C" void Char_System_IConvertible_ToChar_m3613 ();
extern "C" void Char_System_IConvertible_ToDateTime_m3614 ();
extern "C" void Char_System_IConvertible_ToDecimal_m3615 ();
extern "C" void Char_System_IConvertible_ToDouble_m3616 ();
extern "C" void Char_System_IConvertible_ToInt16_m3617 ();
extern "C" void Char_System_IConvertible_ToInt32_m3618 ();
extern "C" void Char_System_IConvertible_ToInt64_m3619 ();
extern "C" void Char_System_IConvertible_ToSByte_m3620 ();
extern "C" void Char_System_IConvertible_ToSingle_m3621 ();
extern "C" void Char_System_IConvertible_ToUInt16_m3622 ();
extern "C" void Char_System_IConvertible_ToUInt32_m3623 ();
extern "C" void Char_System_IConvertible_ToUInt64_m3624 ();
extern "C" void Char_GetDataTablePointers_m3625 ();
extern "C" void Char_CompareTo_m3626 ();
extern "C" void Char_Equals_m3627 ();
extern "C" void Char_CompareTo_m3628 ();
extern "C" void Char_Equals_m3629 ();
extern "C" void Char_GetHashCode_m3630 ();
extern "C" void Char_GetUnicodeCategory_m2385 ();
extern "C" void Char_IsDigit_m2383 ();
extern "C" void Char_IsLetter_m3631 ();
extern "C" void Char_IsLetterOrDigit_m2382 ();
extern "C" void Char_IsLower_m3632 ();
extern "C" void Char_IsSurrogate_m3633 ();
extern "C" void Char_IsWhiteSpace_m2384 ();
extern "C" void Char_IsWhiteSpace_m2301 ();
extern "C" void Char_CheckParameter_m3634 ();
extern "C" void Char_Parse_m3635 ();
extern "C" void Char_ToLower_m2386 ();
extern "C" void Char_ToLowerInvariant_m3636 ();
extern "C" void Char_ToLower_m3637 ();
extern "C" void Char_ToUpper_m2390 ();
extern "C" void Char_ToUpperInvariant_m2303 ();
extern "C" void Char_ToString_m2391 ();
extern "C" void Char_ToString_m3638 ();
extern "C" void String__ctor_m3639 ();
extern "C" void String__ctor_m3640 ();
extern "C" void String__ctor_m3641 ();
extern "C" void String__ctor_m3642 ();
extern "C" void String__cctor_m3643 ();
extern "C" void String_System_IConvertible_ToBoolean_m3644 ();
extern "C" void String_System_IConvertible_ToByte_m3645 ();
extern "C" void String_System_IConvertible_ToChar_m3646 ();
extern "C" void String_System_IConvertible_ToDateTime_m3647 ();
extern "C" void String_System_IConvertible_ToDecimal_m3648 ();
extern "C" void String_System_IConvertible_ToDouble_m3649 ();
extern "C" void String_System_IConvertible_ToInt16_m3650 ();
extern "C" void String_System_IConvertible_ToInt32_m3651 ();
extern "C" void String_System_IConvertible_ToInt64_m3652 ();
extern "C" void String_System_IConvertible_ToSByte_m3653 ();
extern "C" void String_System_IConvertible_ToSingle_m3654 ();
extern "C" void String_System_IConvertible_ToType_m3655 ();
extern "C" void String_System_IConvertible_ToUInt16_m3656 ();
extern "C" void String_System_IConvertible_ToUInt32_m3657 ();
extern "C" void String_System_IConvertible_ToUInt64_m3658 ();
extern "C" void String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m3659 ();
extern "C" void String_System_Collections_IEnumerable_GetEnumerator_m3660 ();
extern "C" void String_Equals_m3661 ();
extern "C" void String_Equals_m3662 ();
extern "C" void String_Equals_m2362 ();
extern "C" void String_get_Chars_m1318 ();
extern "C" void String_CopyTo_m3663 ();
extern "C" void String_ToCharArray_m1267 ();
extern "C" void String_ToCharArray_m3664 ();
extern "C" void String_Split_m1317 ();
extern "C" void String_Split_m3665 ();
extern "C" void String_Split_m3666 ();
extern "C" void String_Split_m3667 ();
extern "C" void String_Split_m2304 ();
extern "C" void String_Substring_m1202 ();
extern "C" void String_Substring_m1206 ();
extern "C" void String_SubstringUnchecked_m3668 ();
extern "C" void String_Trim_m1203 ();
extern "C" void String_Trim_m1204 ();
extern "C" void String_TrimStart_m2402 ();
extern "C" void String_TrimEnd_m2302 ();
extern "C" void String_FindNotWhiteSpace_m3669 ();
extern "C" void String_FindNotInTable_m3670 ();
extern "C" void String_Compare_m3671 ();
extern "C" void String_Compare_m3672 ();
extern "C" void String_Compare_m2276 ();
extern "C" void String_Compare_m3360 ();
extern "C" void String_CompareTo_m3673 ();
extern "C" void String_CompareTo_m3674 ();
extern "C" void String_CompareOrdinal_m2398 ();
extern "C" void String_CompareOrdinalUnchecked_m3675 ();
extern "C" void String_CompareOrdinalCaseInsensitiveUnchecked_m3676 ();
extern "C" void String_EndsWith_m1319 ();
extern "C" void String_IndexOfAny_m2397 ();
extern "C" void String_IndexOfAny_m3677 ();
extern "C" void String_IndexOfAny_m3313 ();
extern "C" void String_IndexOfAnyUnchecked_m3678 ();
extern "C" void String_IndexOf_m1200 ();
extern "C" void String_IndexOf_m3679 ();
extern "C" void String_IndexOfOrdinal_m3680 ();
extern "C" void String_IndexOfOrdinalUnchecked_m3681 ();
extern "C" void String_IndexOfOrdinalIgnoreCaseUnchecked_m3682 ();
extern "C" void String_IndexOf_m1205 ();
extern "C" void String_IndexOf_m1201 ();
extern "C" void String_IndexOf_m2403 ();
extern "C" void String_IndexOfUnchecked_m3683 ();
extern "C" void String_IndexOf_m1218 ();
extern "C" void String_IndexOf_m1321 ();
extern "C" void String_IndexOf_m3684 ();
extern "C" void String_LastIndexOfAny_m3685 ();
extern "C" void String_LastIndexOfAnyUnchecked_m3686 ();
extern "C" void String_LastIndexOf_m2263 ();
extern "C" void String_LastIndexOf_m2399 ();
extern "C" void String_LastIndexOf_m2404 ();
extern "C" void String_LastIndexOfUnchecked_m3687 ();
extern "C" void String_LastIndexOf_m1324 ();
extern "C" void String_LastIndexOf_m3688 ();
extern "C" void String_IsNullOrEmpty_m1234 ();
extern "C" void String_PadRight_m3689 ();
extern "C" void String_StartsWith_m1217 ();
extern "C" void String_Replace_m1323 ();
extern "C" void String_Replace_m1322 ();
extern "C" void String_ReplaceUnchecked_m3690 ();
extern "C" void String_ReplaceFallback_m3691 ();
extern "C" void String_Remove_m1320 ();
extern "C" void String_ToLower_m2389 ();
extern "C" void String_ToLower_m2401 ();
extern "C" void String_ToLowerInvariant_m3692 ();
extern "C" void String_ToUpper_m1219 ();
extern "C" void String_ToUpper_m3693 ();
extern "C" void String_ToUpperInvariant_m3694 ();
extern "C" void String_ToString_m1212 ();
extern "C" void String_ToString_m3695 ();
extern "C" void String_Format_m2310 ();
extern "C" void String_Format_m3696 ();
extern "C" void String_Format_m3697 ();
extern "C" void String_Format_m1225 ();
extern "C" void String_Format_m3339 ();
extern "C" void String_FormatHelper_m3698 ();
extern "C" void String_Concat_m1194 ();
extern "C" void String_Concat_m2250 ();
extern "C" void String_Concat_m1184 ();
extern "C" void String_Concat_m1188 ();
extern "C" void String_Concat_m1316 ();
extern "C" void String_Concat_m1186 ();
extern "C" void String_Concat_m1224 ();
extern "C" void String_ConcatInternal_m3699 ();
extern "C" void String_Insert_m1325 ();
extern "C" void String_Join_m3700 ();
extern "C" void String_Join_m3701 ();
extern "C" void String_JoinUnchecked_m3702 ();
extern "C" void String_get_Length_m1161 ();
extern "C" void String_ParseFormatSpecifier_m3703 ();
extern "C" void String_ParseDecimal_m3704 ();
extern "C" void String_InternalSetChar_m3705 ();
extern "C" void String_InternalSetLength_m3706 ();
extern "C" void String_GetHashCode_m1178 ();
extern "C" void String_GetCaseInsensitiveHashCode_m3707 ();
extern "C" void String_CreateString_m3708 ();
extern "C" void String_CreateString_m3709 ();
extern "C" void String_CreateString_m3710 ();
extern "C" void String_CreateString_m3711 ();
extern "C" void String_CreateString_m3712 ();
extern "C" void String_CreateString_m1271 ();
extern "C" void String_CreateString_m1277 ();
extern "C" void String_CreateString_m1276 ();
extern "C" void String_memcpy4_m3713 ();
extern "C" void String_memcpy2_m3714 ();
extern "C" void String_memcpy1_m3715 ();
extern "C" void String_memcpy_m3716 ();
extern "C" void String_CharCopy_m3717 ();
extern "C" void String_CharCopyReverse_m3718 ();
extern "C" void String_CharCopy_m3719 ();
extern "C" void String_CharCopy_m3720 ();
extern "C" void String_CharCopyReverse_m3721 ();
extern "C" void String_InternalSplit_m3722 ();
extern "C" void String_InternalAllocateStr_m3723 ();
extern "C" void String_op_Equality_m1170 ();
extern "C" void String_op_Inequality_m2269 ();
extern "C" void Single_System_IConvertible_ToBoolean_m3724 ();
extern "C" void Single_System_IConvertible_ToByte_m3725 ();
extern "C" void Single_System_IConvertible_ToChar_m3726 ();
extern "C" void Single_System_IConvertible_ToDateTime_m3727 ();
extern "C" void Single_System_IConvertible_ToDecimal_m3728 ();
extern "C" void Single_System_IConvertible_ToDouble_m3729 ();
extern "C" void Single_System_IConvertible_ToInt16_m3730 ();
extern "C" void Single_System_IConvertible_ToInt32_m3731 ();
extern "C" void Single_System_IConvertible_ToInt64_m3732 ();
extern "C" void Single_System_IConvertible_ToSByte_m3733 ();
extern "C" void Single_System_IConvertible_ToSingle_m3734 ();
extern "C" void Single_System_IConvertible_ToType_m3735 ();
extern "C" void Single_System_IConvertible_ToUInt16_m3736 ();
extern "C" void Single_System_IConvertible_ToUInt32_m3737 ();
extern "C" void Single_System_IConvertible_ToUInt64_m3738 ();
extern "C" void Single_CompareTo_m3739 ();
extern "C" void Single_Equals_m3740 ();
extern "C" void Single_CompareTo_m3741 ();
extern "C" void Single_Equals_m1195 ();
extern "C" void Single_GetHashCode_m1176 ();
extern "C" void Single_IsInfinity_m3742 ();
extern "C" void Single_IsNaN_m3743 ();
extern "C" void Single_IsNegativeInfinity_m3744 ();
extern "C" void Single_IsPositiveInfinity_m3745 ();
extern "C" void Single_Parse_m3746 ();
extern "C" void Single_ToString_m3747 ();
extern "C" void Single_ToString_m1285 ();
extern "C" void Single_ToString_m1196 ();
extern "C" void Single_ToString_m3748 ();
extern "C" void Double_System_IConvertible_ToType_m3749 ();
extern "C" void Double_System_IConvertible_ToBoolean_m3750 ();
extern "C" void Double_System_IConvertible_ToByte_m3751 ();
extern "C" void Double_System_IConvertible_ToChar_m3752 ();
extern "C" void Double_System_IConvertible_ToDateTime_m3753 ();
extern "C" void Double_System_IConvertible_ToDecimal_m3754 ();
extern "C" void Double_System_IConvertible_ToDouble_m3755 ();
extern "C" void Double_System_IConvertible_ToInt16_m3756 ();
extern "C" void Double_System_IConvertible_ToInt32_m3757 ();
extern "C" void Double_System_IConvertible_ToInt64_m3758 ();
extern "C" void Double_System_IConvertible_ToSByte_m3759 ();
extern "C" void Double_System_IConvertible_ToSingle_m3760 ();
extern "C" void Double_System_IConvertible_ToUInt16_m3761 ();
extern "C" void Double_System_IConvertible_ToUInt32_m3762 ();
extern "C" void Double_System_IConvertible_ToUInt64_m3763 ();
extern "C" void Double_CompareTo_m3764 ();
extern "C" void Double_Equals_m3765 ();
extern "C" void Double_CompareTo_m3766 ();
extern "C" void Double_Equals_m3767 ();
extern "C" void Double_GetHashCode_m3768 ();
extern "C" void Double_IsInfinity_m3769 ();
extern "C" void Double_IsNaN_m3770 ();
extern "C" void Double_IsNegativeInfinity_m3771 ();
extern "C" void Double_IsPositiveInfinity_m3772 ();
extern "C" void Double_Parse_m3773 ();
extern "C" void Double_Parse_m3774 ();
extern "C" void Double_Parse_m3775 ();
extern "C" void Double_Parse_m3776 ();
extern "C" void Double_TryParseStringConstant_m3777 ();
extern "C" void Double_ParseImpl_m3778 ();
extern "C" void Double_TryParse_m1278 ();
extern "C" void Double_ToString_m3779 ();
extern "C" void Double_ToString_m3780 ();
extern "C" void Double_ToString_m1287 ();
extern "C" void Decimal__ctor_m3781 ();
extern "C" void Decimal__ctor_m3782 ();
extern "C" void Decimal__ctor_m3783 ();
extern "C" void Decimal__ctor_m3784 ();
extern "C" void Decimal__ctor_m3785 ();
extern "C" void Decimal__ctor_m3786 ();
extern "C" void Decimal__ctor_m3787 ();
extern "C" void Decimal__cctor_m3788 ();
extern "C" void Decimal_System_IConvertible_ToType_m3789 ();
extern "C" void Decimal_System_IConvertible_ToBoolean_m3790 ();
extern "C" void Decimal_System_IConvertible_ToByte_m3791 ();
extern "C" void Decimal_System_IConvertible_ToChar_m3792 ();
extern "C" void Decimal_System_IConvertible_ToDateTime_m3793 ();
extern "C" void Decimal_System_IConvertible_ToDecimal_m3794 ();
extern "C" void Decimal_System_IConvertible_ToDouble_m3795 ();
extern "C" void Decimal_System_IConvertible_ToInt16_m3796 ();
extern "C" void Decimal_System_IConvertible_ToInt32_m3797 ();
extern "C" void Decimal_System_IConvertible_ToInt64_m3798 ();
extern "C" void Decimal_System_IConvertible_ToSByte_m3799 ();
extern "C" void Decimal_System_IConvertible_ToSingle_m3800 ();
extern "C" void Decimal_System_IConvertible_ToUInt16_m3801 ();
extern "C" void Decimal_System_IConvertible_ToUInt32_m3802 ();
extern "C" void Decimal_System_IConvertible_ToUInt64_m3803 ();
extern "C" void Decimal_GetBits_m3804 ();
extern "C" void Decimal_Add_m3805 ();
extern "C" void Decimal_Subtract_m3806 ();
extern "C" void Decimal_GetHashCode_m3807 ();
extern "C" void Decimal_u64_m3808 ();
extern "C" void Decimal_s64_m3809 ();
extern "C" void Decimal_Equals_m3810 ();
extern "C" void Decimal_Equals_m3811 ();
extern "C" void Decimal_IsZero_m3812 ();
extern "C" void Decimal_Floor_m3813 ();
extern "C" void Decimal_Multiply_m3814 ();
extern "C" void Decimal_Divide_m3815 ();
extern "C" void Decimal_Compare_m3816 ();
extern "C" void Decimal_CompareTo_m3817 ();
extern "C" void Decimal_CompareTo_m3818 ();
extern "C" void Decimal_Equals_m3819 ();
extern "C" void Decimal_Parse_m3820 ();
extern "C" void Decimal_ThrowAtPos_m3821 ();
extern "C" void Decimal_ThrowInvalidExp_m3822 ();
extern "C" void Decimal_stripStyles_m3823 ();
extern "C" void Decimal_Parse_m3824 ();
extern "C" void Decimal_PerformParse_m3825 ();
extern "C" void Decimal_ToString_m3826 ();
extern "C" void Decimal_ToString_m3827 ();
extern "C" void Decimal_ToString_m1284 ();
extern "C" void Decimal_decimal2UInt64_m3828 ();
extern "C" void Decimal_decimal2Int64_m3829 ();
extern "C" void Decimal_decimalIncr_m3830 ();
extern "C" void Decimal_string2decimal_m3831 ();
extern "C" void Decimal_decimalSetExponent_m3832 ();
extern "C" void Decimal_decimal2double_m3833 ();
extern "C" void Decimal_decimalFloorAndTrunc_m3834 ();
extern "C" void Decimal_decimalMult_m3835 ();
extern "C" void Decimal_decimalDiv_m3836 ();
extern "C" void Decimal_decimalCompare_m3837 ();
extern "C" void Decimal_op_Increment_m3838 ();
extern "C" void Decimal_op_Subtraction_m3839 ();
extern "C" void Decimal_op_Multiply_m3840 ();
extern "C" void Decimal_op_Division_m3841 ();
extern "C" void Decimal_op_Explicit_m3842 ();
extern "C" void Decimal_op_Explicit_m3843 ();
extern "C" void Decimal_op_Explicit_m3844 ();
extern "C" void Decimal_op_Explicit_m3845 ();
extern "C" void Decimal_op_Explicit_m3846 ();
extern "C" void Decimal_op_Explicit_m3847 ();
extern "C" void Decimal_op_Explicit_m3848 ();
extern "C" void Decimal_op_Explicit_m3849 ();
extern "C" void Decimal_op_Implicit_m3850 ();
extern "C" void Decimal_op_Implicit_m3851 ();
extern "C" void Decimal_op_Implicit_m3852 ();
extern "C" void Decimal_op_Implicit_m3853 ();
extern "C" void Decimal_op_Implicit_m3854 ();
extern "C" void Decimal_op_Implicit_m3855 ();
extern "C" void Decimal_op_Implicit_m3856 ();
extern "C" void Decimal_op_Implicit_m3857 ();
extern "C" void Decimal_op_Explicit_m3858 ();
extern "C" void Decimal_op_Explicit_m3859 ();
extern "C" void Decimal_op_Explicit_m3860 ();
extern "C" void Decimal_op_Explicit_m3861 ();
extern "C" void Decimal_op_Inequality_m3862 ();
extern "C" void Decimal_op_Equality_m3863 ();
extern "C" void Decimal_op_GreaterThan_m3864 ();
extern "C" void Decimal_op_LessThan_m3865 ();
extern "C" void Boolean__cctor_m3866 ();
extern "C" void Boolean_System_IConvertible_ToType_m3867 ();
extern "C" void Boolean_System_IConvertible_ToBoolean_m3868 ();
extern "C" void Boolean_System_IConvertible_ToByte_m3869 ();
extern "C" void Boolean_System_IConvertible_ToChar_m3870 ();
extern "C" void Boolean_System_IConvertible_ToDateTime_m3871 ();
extern "C" void Boolean_System_IConvertible_ToDecimal_m3872 ();
extern "C" void Boolean_System_IConvertible_ToDouble_m3873 ();
extern "C" void Boolean_System_IConvertible_ToInt16_m3874 ();
extern "C" void Boolean_System_IConvertible_ToInt32_m3875 ();
extern "C" void Boolean_System_IConvertible_ToInt64_m3876 ();
extern "C" void Boolean_System_IConvertible_ToSByte_m3877 ();
extern "C" void Boolean_System_IConvertible_ToSingle_m3878 ();
extern "C" void Boolean_System_IConvertible_ToUInt16_m3879 ();
extern "C" void Boolean_System_IConvertible_ToUInt32_m3880 ();
extern "C" void Boolean_System_IConvertible_ToUInt64_m3881 ();
extern "C" void Boolean_CompareTo_m3882 ();
extern "C" void Boolean_Equals_m3883 ();
extern "C" void Boolean_CompareTo_m3884 ();
extern "C" void Boolean_Equals_m3885 ();
extern "C" void Boolean_GetHashCode_m3886 ();
extern "C" void Boolean_Parse_m3887 ();
extern "C" void Boolean_ToString_m1258 ();
extern "C" void Boolean_ToString_m3888 ();
extern "C" void IntPtr__ctor_m1226 ();
extern "C" void IntPtr__ctor_m3889 ();
extern "C" void IntPtr__ctor_m3890 ();
extern "C" void IntPtr__ctor_m3891 ();
extern "C" void IntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m3892 ();
extern "C" void IntPtr_get_Size_m3893 ();
extern "C" void IntPtr_Equals_m3894 ();
extern "C" void IntPtr_GetHashCode_m3895 ();
extern "C" void IntPtr_ToInt64_m3896 ();
extern "C" void IntPtr_ToString_m3897 ();
extern "C" void IntPtr_ToString_m3898 ();
extern "C" void IntPtr_op_Equality_m1331 ();
extern "C" void IntPtr_op_Inequality_m1229 ();
extern "C" void IntPtr_op_Explicit_m3899 ();
extern "C" void IntPtr_op_Explicit_m3900 ();
extern "C" void IntPtr_op_Explicit_m3901 ();
extern "C" void IntPtr_op_Explicit_m1330 ();
extern "C" void IntPtr_op_Explicit_m3902 ();
extern "C" void UIntPtr__ctor_m3903 ();
extern "C" void UIntPtr__ctor_m3904 ();
extern "C" void UIntPtr__ctor_m3905 ();
extern "C" void UIntPtr__cctor_m3906 ();
extern "C" void UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m3907 ();
extern "C" void UIntPtr_Equals_m3908 ();
extern "C" void UIntPtr_GetHashCode_m3909 ();
extern "C" void UIntPtr_ToUInt32_m3910 ();
extern "C" void UIntPtr_ToUInt64_m3911 ();
extern "C" void UIntPtr_ToPointer_m3912 ();
extern "C" void UIntPtr_ToString_m3913 ();
extern "C" void UIntPtr_get_Size_m3914 ();
extern "C" void UIntPtr_op_Equality_m3915 ();
extern "C" void UIntPtr_op_Inequality_m3916 ();
extern "C" void UIntPtr_op_Explicit_m3917 ();
extern "C" void UIntPtr_op_Explicit_m3918 ();
extern "C" void UIntPtr_op_Explicit_m3919 ();
extern "C" void UIntPtr_op_Explicit_m3920 ();
extern "C" void UIntPtr_op_Explicit_m3921 ();
extern "C" void UIntPtr_op_Explicit_m3922 ();
extern "C" void MulticastDelegate_GetObjectData_m3923 ();
extern "C" void MulticastDelegate_Equals_m3924 ();
extern "C" void MulticastDelegate_GetHashCode_m3925 ();
extern "C" void MulticastDelegate_GetInvocationList_m3926 ();
extern "C" void MulticastDelegate_CombineImpl_m3927 ();
extern "C" void MulticastDelegate_BaseEquals_m3928 ();
extern "C" void MulticastDelegate_KPM_m3929 ();
extern "C" void MulticastDelegate_RemoveImpl_m3930 ();
extern "C" void Delegate_get_Method_m3931 ();
extern "C" void Delegate_get_Target_m3932 ();
extern "C" void Delegate_CreateDelegate_internal_m3933 ();
extern "C" void Delegate_SetMulticastInvoke_m3934 ();
extern "C" void Delegate_arg_type_match_m3935 ();
extern "C" void Delegate_return_type_match_m3936 ();
extern "C" void Delegate_CreateDelegate_m3937 ();
extern "C" void Delegate_CreateDelegate_m3938 ();
extern "C" void Delegate_CreateDelegate_m3939 ();
extern "C" void Delegate_CreateDelegate_m3940 ();
extern "C" void Delegate_GetCandidateMethod_m3941 ();
extern "C" void Delegate_CreateDelegate_m3942 ();
extern "C" void Delegate_CreateDelegate_m3943 ();
extern "C" void Delegate_CreateDelegate_m3944 ();
extern "C" void Delegate_CreateDelegate_m3945 ();
extern "C" void Delegate_Clone_m3946 ();
extern "C" void Delegate_Equals_m3947 ();
extern "C" void Delegate_GetHashCode_m3948 ();
extern "C" void Delegate_GetObjectData_m3949 ();
extern "C" void Delegate_GetInvocationList_m3950 ();
extern "C" void Delegate_Combine_m1227 ();
extern "C" void Delegate_Combine_m3951 ();
extern "C" void Delegate_CombineImpl_m3952 ();
extern "C" void Delegate_Remove_m1228 ();
extern "C" void Delegate_RemoveImpl_m3953 ();
extern "C" void Enum__ctor_m3954 ();
extern "C" void Enum__cctor_m3955 ();
extern "C" void Enum_System_IConvertible_ToBoolean_m3956 ();
extern "C" void Enum_System_IConvertible_ToByte_m3957 ();
extern "C" void Enum_System_IConvertible_ToChar_m3958 ();
extern "C" void Enum_System_IConvertible_ToDateTime_m3959 ();
extern "C" void Enum_System_IConvertible_ToDecimal_m3960 ();
extern "C" void Enum_System_IConvertible_ToDouble_m3961 ();
extern "C" void Enum_System_IConvertible_ToInt16_m3962 ();
extern "C" void Enum_System_IConvertible_ToInt32_m3963 ();
extern "C" void Enum_System_IConvertible_ToInt64_m3964 ();
extern "C" void Enum_System_IConvertible_ToSByte_m3965 ();
extern "C" void Enum_System_IConvertible_ToSingle_m3966 ();
extern "C" void Enum_System_IConvertible_ToType_m3967 ();
extern "C" void Enum_System_IConvertible_ToUInt16_m3968 ();
extern "C" void Enum_System_IConvertible_ToUInt32_m3969 ();
extern "C" void Enum_System_IConvertible_ToUInt64_m3970 ();
extern "C" void Enum_GetTypeCode_m3971 ();
extern "C" void Enum_get_value_m3972 ();
extern "C" void Enum_get_Value_m3973 ();
extern "C" void Enum_FindPosition_m3974 ();
extern "C" void Enum_GetName_m3975 ();
extern "C" void Enum_IsDefined_m3348 ();
extern "C" void Enum_get_underlying_type_m3976 ();
extern "C" void Enum_GetUnderlyingType_m3977 ();
extern "C" void Enum_FindName_m3978 ();
extern "C" void Enum_GetValue_m3979 ();
extern "C" void Enum_Parse_m2381 ();
extern "C" void Enum_compare_value_to_m3980 ();
extern "C" void Enum_CompareTo_m3981 ();
extern "C" void Enum_ToString_m3982 ();
extern "C" void Enum_ToString_m3983 ();
extern "C" void Enum_ToString_m1236 ();
extern "C" void Enum_ToString_m3984 ();
extern "C" void Enum_ToObject_m3985 ();
extern "C" void Enum_ToObject_m3986 ();
extern "C" void Enum_ToObject_m3987 ();
extern "C" void Enum_ToObject_m3988 ();
extern "C" void Enum_ToObject_m3989 ();
extern "C" void Enum_ToObject_m3990 ();
extern "C" void Enum_ToObject_m3991 ();
extern "C" void Enum_ToObject_m3992 ();
extern "C" void Enum_ToObject_m3993 ();
extern "C" void Enum_Equals_m3994 ();
extern "C" void Enum_get_hashcode_m3995 ();
extern "C" void Enum_GetHashCode_m3996 ();
extern "C" void Enum_FormatSpecifier_X_m3997 ();
extern "C" void Enum_FormatFlags_m3998 ();
extern "C" void Enum_Format_m3999 ();
extern "C" void SimpleEnumerator__ctor_m4000 ();
extern "C" void SimpleEnumerator_get_Current_m4001 ();
extern "C" void SimpleEnumerator_MoveNext_m4002 ();
extern "C" void Swapper__ctor_m4003 ();
extern "C" void Swapper_Invoke_m4004 ();
extern "C" void Swapper_BeginInvoke_m4005 ();
extern "C" void Swapper_EndInvoke_m4006 ();
extern "C" void Array__ctor_m4007 ();
extern "C" void Array_System_Collections_IList_get_Item_m4008 ();
extern "C" void Array_System_Collections_IList_set_Item_m4009 ();
extern "C" void Array_System_Collections_IList_Add_m4010 ();
extern "C" void Array_System_Collections_IList_Clear_m4011 ();
extern "C" void Array_System_Collections_IList_Contains_m4012 ();
extern "C" void Array_System_Collections_IList_IndexOf_m4013 ();
extern "C" void Array_System_Collections_IList_Insert_m4014 ();
extern "C" void Array_System_Collections_IList_Remove_m4015 ();
extern "C" void Array_System_Collections_IList_RemoveAt_m4016 ();
extern "C" void Array_System_Collections_ICollection_get_Count_m4017 ();
extern "C" void Array_InternalArray__ICollection_get_Count_m4018 ();
extern "C" void Array_InternalArray__ICollection_get_IsReadOnly_m4019 ();
extern "C" void Array_InternalArray__ICollection_Clear_m4020 ();
extern "C" void Array_InternalArray__RemoveAt_m4021 ();
extern "C" void Array_get_Length_m2235 ();
extern "C" void Array_get_LongLength_m4022 ();
extern "C" void Array_get_Rank_m2240 ();
extern "C" void Array_GetRank_m4023 ();
extern "C" void Array_GetLength_m4024 ();
extern "C" void Array_GetLongLength_m4025 ();
extern "C" void Array_GetLowerBound_m4026 ();
extern "C" void Array_GetValue_m4027 ();
extern "C" void Array_SetValue_m4028 ();
extern "C" void Array_GetValueImpl_m4029 ();
extern "C" void Array_SetValueImpl_m4030 ();
extern "C" void Array_FastCopy_m4031 ();
extern "C" void Array_CreateInstanceImpl_m4032 ();
extern "C" void Array_get_IsSynchronized_m4033 ();
extern "C" void Array_get_SyncRoot_m4034 ();
extern "C" void Array_get_IsFixedSize_m4035 ();
extern "C" void Array_get_IsReadOnly_m4036 ();
extern "C" void Array_GetEnumerator_m4037 ();
extern "C" void Array_GetUpperBound_m4038 ();
extern "C" void Array_GetValue_m4039 ();
extern "C" void Array_GetValue_m4040 ();
extern "C" void Array_GetValue_m4041 ();
extern "C" void Array_GetValue_m4042 ();
extern "C" void Array_GetValue_m4043 ();
extern "C" void Array_GetValue_m4044 ();
extern "C" void Array_SetValue_m4045 ();
extern "C" void Array_SetValue_m4046 ();
extern "C" void Array_SetValue_m4047 ();
extern "C" void Array_SetValue_m2236 ();
extern "C" void Array_SetValue_m4048 ();
extern "C" void Array_SetValue_m4049 ();
extern "C" void Array_CreateInstance_m4050 ();
extern "C" void Array_CreateInstance_m4051 ();
extern "C" void Array_CreateInstance_m4052 ();
extern "C" void Array_CreateInstance_m4053 ();
extern "C" void Array_CreateInstance_m4054 ();
extern "C" void Array_GetIntArray_m4055 ();
extern "C" void Array_CreateInstance_m4056 ();
extern "C" void Array_GetValue_m4057 ();
extern "C" void Array_SetValue_m4058 ();
extern "C" void Array_BinarySearch_m4059 ();
extern "C" void Array_BinarySearch_m4060 ();
extern "C" void Array_BinarySearch_m4061 ();
extern "C" void Array_BinarySearch_m4062 ();
extern "C" void Array_DoBinarySearch_m4063 ();
extern "C" void Array_Clear_m2458 ();
extern "C" void Array_ClearInternal_m4064 ();
extern "C" void Array_Clone_m4065 ();
extern "C" void Array_Copy_m2392 ();
extern "C" void Array_Copy_m4066 ();
extern "C" void Array_Copy_m4067 ();
extern "C" void Array_Copy_m4068 ();
extern "C" void Array_IndexOf_m4069 ();
extern "C" void Array_IndexOf_m4070 ();
extern "C" void Array_IndexOf_m4071 ();
extern "C" void Array_Initialize_m4072 ();
extern "C" void Array_LastIndexOf_m4073 ();
extern "C" void Array_LastIndexOf_m4074 ();
extern "C" void Array_LastIndexOf_m4075 ();
extern "C" void Array_get_swapper_m4076 ();
extern "C" void Array_Reverse_m3290 ();
extern "C" void Array_Reverse_m3314 ();
extern "C" void Array_Sort_m4077 ();
extern "C" void Array_Sort_m4078 ();
extern "C" void Array_Sort_m4079 ();
extern "C" void Array_Sort_m4080 ();
extern "C" void Array_Sort_m4081 ();
extern "C" void Array_Sort_m4082 ();
extern "C" void Array_Sort_m4083 ();
extern "C" void Array_Sort_m4084 ();
extern "C" void Array_int_swapper_m4085 ();
extern "C" void Array_obj_swapper_m4086 ();
extern "C" void Array_slow_swapper_m4087 ();
extern "C" void Array_double_swapper_m4088 ();
extern "C" void Array_new_gap_m4089 ();
extern "C" void Array_combsort_m4090 ();
extern "C" void Array_combsort_m4091 ();
extern "C" void Array_combsort_m4092 ();
extern "C" void Array_qsort_m4093 ();
extern "C" void Array_swap_m4094 ();
extern "C" void Array_compare_m4095 ();
extern "C" void Array_CopyTo_m4096 ();
extern "C" void Array_CopyTo_m4097 ();
extern "C" void Array_ConstrainedCopy_m4098 ();
extern "C" void Type__ctor_m4099 ();
extern "C" void Type__cctor_m4100 ();
extern "C" void Type_FilterName_impl_m4101 ();
extern "C" void Type_FilterNameIgnoreCase_impl_m4102 ();
extern "C" void Type_FilterAttribute_impl_m4103 ();
extern "C" void Type_get_Attributes_m4104 ();
extern "C" void Type_get_DeclaringType_m4105 ();
extern "C" void Type_get_HasElementType_m4106 ();
extern "C" void Type_get_IsAbstract_m4107 ();
extern "C" void Type_get_IsArray_m4108 ();
extern "C" void Type_get_IsByRef_m4109 ();
extern "C" void Type_get_IsClass_m4110 ();
extern "C" void Type_get_IsContextful_m4111 ();
extern "C" void Type_get_IsEnum_m4112 ();
extern "C" void Type_get_IsExplicitLayout_m4113 ();
extern "C" void Type_get_IsInterface_m4114 ();
extern "C" void Type_get_IsMarshalByRef_m4115 ();
extern "C" void Type_get_IsPointer_m4116 ();
extern "C" void Type_get_IsPrimitive_m4117 ();
extern "C" void Type_get_IsSealed_m4118 ();
extern "C" void Type_get_IsSerializable_m4119 ();
extern "C" void Type_get_IsValueType_m4120 ();
extern "C" void Type_get_MemberType_m4121 ();
extern "C" void Type_get_ReflectedType_m4122 ();
extern "C" void Type_get_TypeHandle_m4123 ();
extern "C" void Type_Equals_m4124 ();
extern "C" void Type_Equals_m4125 ();
extern "C" void Type_EqualsInternal_m4126 ();
extern "C" void Type_internal_from_handle_m4127 ();
extern "C" void Type_internal_from_name_m4128 ();
extern "C" void Type_GetType_m4129 ();
extern "C" void Type_GetType_m4130 ();
extern "C" void Type_GetTypeCodeInternal_m4131 ();
extern "C" void Type_GetTypeCode_m4132 ();
extern "C" void Type_GetTypeFromHandle_m1294 ();
extern "C" void Type_GetTypeHandle_m4133 ();
extern "C" void Type_type_is_subtype_of_m4134 ();
extern "C" void Type_type_is_assignable_from_m4135 ();
extern "C" void Type_IsSubclassOf_m4136 ();
extern "C" void Type_IsAssignableFrom_m4137 ();
extern "C" void Type_IsInstanceOfType_m4138 ();
extern "C" void Type_GetHashCode_m4139 ();
extern "C" void Type_GetMethod_m4140 ();
extern "C" void Type_GetMethod_m4141 ();
extern "C" void Type_GetMethod_m4142 ();
extern "C" void Type_GetMethod_m4143 ();
extern "C" void Type_GetProperty_m4144 ();
extern "C" void Type_GetProperty_m4145 ();
extern "C" void Type_GetProperty_m4146 ();
extern "C" void Type_GetProperty_m4147 ();
extern "C" void Type_IsArrayImpl_m4148 ();
extern "C" void Type_IsValueTypeImpl_m4149 ();
extern "C" void Type_IsContextfulImpl_m4150 ();
extern "C" void Type_IsMarshalByRefImpl_m4151 ();
extern "C" void Type_GetConstructor_m4152 ();
extern "C" void Type_GetConstructor_m4153 ();
extern "C" void Type_GetConstructor_m4154 ();
extern "C" void Type_GetConstructors_m4155 ();
extern "C" void Type_ToString_m4156 ();
extern "C" void Type_get_IsSystemType_m4157 ();
extern "C" void Type_GetGenericArguments_m4158 ();
extern "C" void Type_get_ContainsGenericParameters_m4159 ();
extern "C" void Type_get_IsGenericTypeDefinition_m4160 ();
extern "C" void Type_GetGenericTypeDefinition_impl_m4161 ();
extern "C" void Type_GetGenericTypeDefinition_m4162 ();
extern "C" void Type_get_IsGenericType_m4163 ();
extern "C" void Type_MakeGenericType_m4164 ();
extern "C" void Type_MakeGenericType_m4165 ();
extern "C" void Type_get_IsGenericParameter_m4166 ();
extern "C" void Type_get_IsNested_m4167 ();
extern "C" void Type_GetPseudoCustomAttributes_m4168 ();
extern "C" void MemberInfo__ctor_m4169 ();
extern "C" void MemberInfo_get_Module_m4170 ();
extern "C" void Exception__ctor_m3287 ();
extern "C" void Exception__ctor_m1326 ();
extern "C" void Exception__ctor_m1329 ();
extern "C" void Exception__ctor_m1328 ();
extern "C" void Exception_get_InnerException_m4171 ();
extern "C" void Exception_set_HResult_m1327 ();
extern "C" void Exception_get_ClassName_m4172 ();
extern "C" void Exception_get_Message_m4173 ();
extern "C" void Exception_get_Source_m4174 ();
extern "C" void Exception_get_StackTrace_m4175 ();
extern "C" void Exception_GetObjectData_m2408 ();
extern "C" void Exception_ToString_m4176 ();
extern "C" void Exception_GetFullNameForStackTrace_m4177 ();
extern "C" void Exception_GetType_m4178 ();
extern "C" void RuntimeFieldHandle__ctor_m4179 ();
extern "C" void RuntimeFieldHandle_get_Value_m4180 ();
extern "C" void RuntimeFieldHandle_GetObjectData_m4181 ();
extern "C" void RuntimeFieldHandle_Equals_m4182 ();
extern "C" void RuntimeFieldHandle_GetHashCode_m4183 ();
extern "C" void RuntimeTypeHandle__ctor_m4184 ();
extern "C" void RuntimeTypeHandle_get_Value_m4185 ();
extern "C" void RuntimeTypeHandle_GetObjectData_m4186 ();
extern "C" void RuntimeTypeHandle_Equals_m4187 ();
extern "C" void RuntimeTypeHandle_GetHashCode_m4188 ();
extern "C" void ParamArrayAttribute__ctor_m4189 ();
extern "C" void OutAttribute__ctor_m4190 ();
extern "C" void ObsoleteAttribute__ctor_m4191 ();
extern "C" void ObsoleteAttribute__ctor_m4192 ();
extern "C" void ObsoleteAttribute__ctor_m4193 ();
extern "C" void DllImportAttribute__ctor_m4194 ();
extern "C" void DllImportAttribute_get_Value_m4195 ();
extern "C" void MarshalAsAttribute__ctor_m4196 ();
extern "C" void InAttribute__ctor_m4197 ();
extern "C" void ConditionalAttribute__ctor_m4198 ();
extern "C" void GuidAttribute__ctor_m4199 ();
extern "C" void ComImportAttribute__ctor_m4200 ();
extern "C" void OptionalAttribute__ctor_m4201 ();
extern "C" void CompilerGeneratedAttribute__ctor_m4202 ();
extern "C" void InternalsVisibleToAttribute__ctor_m4203 ();
extern "C" void RuntimeCompatibilityAttribute__ctor_m4204 ();
extern "C" void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4205 ();
extern "C" void DebuggerHiddenAttribute__ctor_m4206 ();
extern "C" void DefaultMemberAttribute__ctor_m4207 ();
extern "C" void DefaultMemberAttribute_get_MemberName_m4208 ();
extern "C" void DecimalConstantAttribute__ctor_m4209 ();
extern "C" void FieldOffsetAttribute__ctor_m4210 ();
extern "C" void AsyncCallback__ctor_m3347 ();
extern "C" void AsyncCallback_Invoke_m4211 ();
extern "C" void AsyncCallback_BeginInvoke_m3345 ();
extern "C" void AsyncCallback_EndInvoke_m4212 ();
extern "C" void TypedReference_Equals_m4213 ();
extern "C" void TypedReference_GetHashCode_m4214 ();
extern "C" void ArgIterator_Equals_m4215 ();
extern "C" void ArgIterator_GetHashCode_m4216 ();
extern "C" void MarshalByRefObject__ctor_m2277 ();
extern "C" void RuntimeHelpers_InitializeArray_m4217 ();
extern "C" void RuntimeHelpers_InitializeArray_m2271 ();
extern "C" void RuntimeHelpers_get_OffsetToStringData_m4218 ();
extern "C" void Locale_GetText_m4219 ();
extern "C" void Locale_GetText_m4220 ();
extern "C" void MonoTODOAttribute__ctor_m4221 ();
extern "C" void MonoTODOAttribute__ctor_m4222 ();
extern "C" void MonoDocumentationNoteAttribute__ctor_m4223 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid__ctor_m4224 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m4225 ();
extern "C" void SafeWaitHandle__ctor_m4226 ();
extern "C" void SafeWaitHandle_ReleaseHandle_m4227 ();
extern "C" void TableRange__ctor_m4228 ();
extern "C" void CodePointIndexer__ctor_m4229 ();
extern "C" void CodePointIndexer_ToIndex_m4230 ();
extern "C" void TailoringInfo__ctor_m4231 ();
extern "C" void Contraction__ctor_m4232 ();
extern "C" void ContractionComparer__ctor_m4233 ();
extern "C" void ContractionComparer__cctor_m4234 ();
extern "C" void ContractionComparer_Compare_m4235 ();
extern "C" void Level2Map__ctor_m4236 ();
extern "C" void Level2MapComparer__ctor_m4237 ();
extern "C" void Level2MapComparer__cctor_m4238 ();
extern "C" void Level2MapComparer_Compare_m4239 ();
extern "C" void MSCompatUnicodeTable__cctor_m4240 ();
extern "C" void MSCompatUnicodeTable_GetTailoringInfo_m4241 ();
extern "C" void MSCompatUnicodeTable_BuildTailoringTables_m4242 ();
extern "C" void MSCompatUnicodeTable_SetCJKReferences_m4243 ();
extern "C" void MSCompatUnicodeTable_Category_m4244 ();
extern "C" void MSCompatUnicodeTable_Level1_m4245 ();
extern "C" void MSCompatUnicodeTable_Level2_m4246 ();
extern "C" void MSCompatUnicodeTable_Level3_m4247 ();
extern "C" void MSCompatUnicodeTable_IsIgnorable_m4248 ();
extern "C" void MSCompatUnicodeTable_IsIgnorableNonSpacing_m4249 ();
extern "C" void MSCompatUnicodeTable_ToKanaTypeInsensitive_m4250 ();
extern "C" void MSCompatUnicodeTable_ToWidthCompat_m4251 ();
extern "C" void MSCompatUnicodeTable_HasSpecialWeight_m4252 ();
extern "C" void MSCompatUnicodeTable_IsHalfWidthKana_m4253 ();
extern "C" void MSCompatUnicodeTable_IsHiragana_m4254 ();
extern "C" void MSCompatUnicodeTable_IsJapaneseSmallLetter_m4255 ();
extern "C" void MSCompatUnicodeTable_get_IsReady_m4256 ();
extern "C" void MSCompatUnicodeTable_GetResource_m4257 ();
extern "C" void MSCompatUnicodeTable_UInt32FromBytePtr_m4258 ();
extern "C" void MSCompatUnicodeTable_FillCJK_m4259 ();
extern "C" void MSCompatUnicodeTable_FillCJKCore_m4260 ();
extern "C" void MSCompatUnicodeTableUtil__cctor_m4261 ();
extern "C" void Context__ctor_m4262 ();
extern "C" void PreviousInfo__ctor_m4263 ();
extern "C" void SimpleCollator__ctor_m4264 ();
extern "C" void SimpleCollator__cctor_m4265 ();
extern "C" void SimpleCollator_SetCJKTable_m4266 ();
extern "C" void SimpleCollator_GetNeutralCulture_m4267 ();
extern "C" void SimpleCollator_Category_m4268 ();
extern "C" void SimpleCollator_Level1_m4269 ();
extern "C" void SimpleCollator_Level2_m4270 ();
extern "C" void SimpleCollator_IsHalfKana_m4271 ();
extern "C" void SimpleCollator_GetContraction_m4272 ();
extern "C" void SimpleCollator_GetContraction_m4273 ();
extern "C" void SimpleCollator_GetTailContraction_m4274 ();
extern "C" void SimpleCollator_GetTailContraction_m4275 ();
extern "C" void SimpleCollator_FilterOptions_m4276 ();
extern "C" void SimpleCollator_GetExtenderType_m4277 ();
extern "C" void SimpleCollator_ToDashTypeValue_m4278 ();
extern "C" void SimpleCollator_FilterExtender_m4279 ();
extern "C" void SimpleCollator_IsIgnorable_m4280 ();
extern "C" void SimpleCollator_IsSafe_m4281 ();
extern "C" void SimpleCollator_GetSortKey_m4282 ();
extern "C" void SimpleCollator_GetSortKey_m4283 ();
extern "C" void SimpleCollator_GetSortKey_m4284 ();
extern "C" void SimpleCollator_FillSortKeyRaw_m4285 ();
extern "C" void SimpleCollator_FillSurrogateSortKeyRaw_m4286 ();
extern "C" void SimpleCollator_CompareOrdinal_m4287 ();
extern "C" void SimpleCollator_CompareQuick_m4288 ();
extern "C" void SimpleCollator_CompareOrdinalIgnoreCase_m4289 ();
extern "C" void SimpleCollator_Compare_m4290 ();
extern "C" void SimpleCollator_ClearBuffer_m4291 ();
extern "C" void SimpleCollator_QuickCheckPossible_m4292 ();
extern "C" void SimpleCollator_CompareInternal_m4293 ();
extern "C" void SimpleCollator_CompareFlagPair_m4294 ();
extern "C" void SimpleCollator_IsPrefix_m4295 ();
extern "C" void SimpleCollator_IsPrefix_m4296 ();
extern "C" void SimpleCollator_IsPrefix_m4297 ();
extern "C" void SimpleCollator_IsSuffix_m4298 ();
extern "C" void SimpleCollator_IsSuffix_m4299 ();
extern "C" void SimpleCollator_QuickIndexOf_m4300 ();
extern "C" void SimpleCollator_IndexOf_m4301 ();
extern "C" void SimpleCollator_IndexOfOrdinal_m4302 ();
extern "C" void SimpleCollator_IndexOfOrdinalIgnoreCase_m4303 ();
extern "C" void SimpleCollator_IndexOfSortKey_m4304 ();
extern "C" void SimpleCollator_IndexOf_m4305 ();
extern "C" void SimpleCollator_LastIndexOf_m4306 ();
extern "C" void SimpleCollator_LastIndexOfOrdinal_m4307 ();
extern "C" void SimpleCollator_LastIndexOfOrdinalIgnoreCase_m4308 ();
extern "C" void SimpleCollator_LastIndexOfSortKey_m4309 ();
extern "C" void SimpleCollator_LastIndexOf_m4310 ();
extern "C" void SimpleCollator_MatchesForward_m4311 ();
extern "C" void SimpleCollator_MatchesForwardCore_m4312 ();
extern "C" void SimpleCollator_MatchesPrimitive_m4313 ();
extern "C" void SimpleCollator_MatchesBackward_m4314 ();
extern "C" void SimpleCollator_MatchesBackwardCore_m4315 ();
extern "C" void SortKey__ctor_m4316 ();
extern "C" void SortKey__ctor_m4317 ();
extern "C" void SortKey_Compare_m4318 ();
extern "C" void SortKey_get_OriginalString_m4319 ();
extern "C" void SortKey_get_KeyData_m4320 ();
extern "C" void SortKey_Equals_m4321 ();
extern "C" void SortKey_GetHashCode_m4322 ();
extern "C" void SortKey_ToString_m4323 ();
extern "C" void SortKeyBuffer__ctor_m4324 ();
extern "C" void SortKeyBuffer_Reset_m4325 ();
extern "C" void SortKeyBuffer_Initialize_m4326 ();
extern "C" void SortKeyBuffer_AppendCJKExtension_m4327 ();
extern "C" void SortKeyBuffer_AppendKana_m4328 ();
extern "C" void SortKeyBuffer_AppendNormal_m4329 ();
extern "C" void SortKeyBuffer_AppendLevel5_m4330 ();
extern "C" void SortKeyBuffer_AppendBufferPrimitive_m4331 ();
extern "C" void SortKeyBuffer_GetResultAndReset_m4332 ();
extern "C" void SortKeyBuffer_GetOptimizedLength_m4333 ();
extern "C" void SortKeyBuffer_GetResult_m4334 ();
extern "C" void PrimeGeneratorBase__ctor_m4335 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m4336 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m4337 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m4338 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m4339 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m4340 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m4341 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m4342 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m4343 ();
extern "C" void PrimalityTests_GetSPPRounds_m4344 ();
extern "C" void PrimalityTests_Test_m4345 ();
extern "C" void PrimalityTests_RabinMillerTest_m4346 ();
extern "C" void PrimalityTests_SmallPrimeSppTest_m4347 ();
extern "C" void ModulusRing__ctor_m4348 ();
extern "C" void ModulusRing_BarrettReduction_m4349 ();
extern "C" void ModulusRing_Multiply_m4350 ();
extern "C" void ModulusRing_Difference_m4351 ();
extern "C" void ModulusRing_Pow_m4352 ();
extern "C" void ModulusRing_Pow_m4353 ();
extern "C" void Kernel_AddSameSign_m4354 ();
extern "C" void Kernel_Subtract_m4355 ();
extern "C" void Kernel_MinusEq_m4356 ();
extern "C" void Kernel_PlusEq_m4357 ();
extern "C" void Kernel_Compare_m4358 ();
extern "C" void Kernel_SingleByteDivideInPlace_m4359 ();
extern "C" void Kernel_DwordMod_m4360 ();
extern "C" void Kernel_DwordDivMod_m4361 ();
extern "C" void Kernel_multiByteDivide_m4362 ();
extern "C" void Kernel_LeftShift_m4363 ();
extern "C" void Kernel_RightShift_m4364 ();
extern "C" void Kernel_MultiplyByDword_m4365 ();
extern "C" void Kernel_Multiply_m4366 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m4367 ();
extern "C" void Kernel_modInverse_m4368 ();
extern "C" void Kernel_modInverse_m4369 ();
extern "C" void BigInteger__ctor_m4370 ();
extern "C" void BigInteger__ctor_m4371 ();
extern "C" void BigInteger__ctor_m4372 ();
extern "C" void BigInteger__ctor_m4373 ();
extern "C" void BigInteger__ctor_m4374 ();
extern "C" void BigInteger__cctor_m4375 ();
extern "C" void BigInteger_get_Rng_m4376 ();
extern "C" void BigInteger_GenerateRandom_m4377 ();
extern "C" void BigInteger_GenerateRandom_m4378 ();
extern "C" void BigInteger_Randomize_m4379 ();
extern "C" void BigInteger_Randomize_m4380 ();
extern "C" void BigInteger_BitCount_m4381 ();
extern "C" void BigInteger_TestBit_m4382 ();
extern "C" void BigInteger_TestBit_m4383 ();
extern "C" void BigInteger_SetBit_m4384 ();
extern "C" void BigInteger_SetBit_m4385 ();
extern "C" void BigInteger_LowestSetBit_m4386 ();
extern "C" void BigInteger_GetBytes_m4387 ();
extern "C" void BigInteger_ToString_m4388 ();
extern "C" void BigInteger_ToString_m4389 ();
extern "C" void BigInteger_Normalize_m4390 ();
extern "C" void BigInteger_Clear_m4391 ();
extern "C" void BigInteger_GetHashCode_m4392 ();
extern "C" void BigInteger_ToString_m4393 ();
extern "C" void BigInteger_Equals_m4394 ();
extern "C" void BigInteger_ModInverse_m4395 ();
extern "C" void BigInteger_ModPow_m4396 ();
extern "C" void BigInteger_IsProbablePrime_m4397 ();
extern "C" void BigInteger_GeneratePseudoPrime_m4398 ();
extern "C" void BigInteger_Incr2_m4399 ();
extern "C" void BigInteger_op_Implicit_m4400 ();
extern "C" void BigInteger_op_Implicit_m4401 ();
extern "C" void BigInteger_op_Addition_m4402 ();
extern "C" void BigInteger_op_Subtraction_m4403 ();
extern "C" void BigInteger_op_Modulus_m4404 ();
extern "C" void BigInteger_op_Modulus_m4405 ();
extern "C" void BigInteger_op_Division_m4406 ();
extern "C" void BigInteger_op_Multiply_m4407 ();
extern "C" void BigInteger_op_Multiply_m4408 ();
extern "C" void BigInteger_op_LeftShift_m4409 ();
extern "C" void BigInteger_op_RightShift_m4410 ();
extern "C" void BigInteger_op_Equality_m4411 ();
extern "C" void BigInteger_op_Inequality_m4412 ();
extern "C" void BigInteger_op_Equality_m4413 ();
extern "C" void BigInteger_op_Inequality_m4414 ();
extern "C" void BigInteger_op_GreaterThan_m4415 ();
extern "C" void BigInteger_op_LessThan_m4416 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m4417 ();
extern "C" void BigInteger_op_LessThanOrEqual_m4418 ();
extern "C" void CryptoConvert_ToInt32LE_m4419 ();
extern "C" void CryptoConvert_ToUInt32LE_m4420 ();
extern "C" void CryptoConvert_GetBytesLE_m4421 ();
extern "C" void CryptoConvert_ToCapiPrivateKeyBlob_m4422 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m4423 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m4424 ();
extern "C" void CryptoConvert_ToCapiPublicKeyBlob_m4425 ();
extern "C" void CryptoConvert_ToCapiKeyBlob_m4426 ();
extern "C" void KeyBuilder_get_Rng_m4427 ();
extern "C" void KeyBuilder_Key_m4428 ();
extern "C" void KeyBuilder_IV_m4429 ();
extern "C" void BlockProcessor__ctor_m4430 ();
extern "C" void BlockProcessor_Finalize_m4431 ();
extern "C" void BlockProcessor_Initialize_m4432 ();
extern "C" void BlockProcessor_Core_m4433 ();
extern "C" void BlockProcessor_Core_m4434 ();
extern "C" void BlockProcessor_Final_m4435 ();
extern "C" void KeyGeneratedEventHandler__ctor_m4436 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m4437 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m4438 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m4439 ();
extern "C" void DSAManaged__ctor_m4440 ();
extern "C" void DSAManaged_add_KeyGenerated_m4441 ();
extern "C" void DSAManaged_remove_KeyGenerated_m4442 ();
extern "C" void DSAManaged_Finalize_m4443 ();
extern "C" void DSAManaged_Generate_m4444 ();
extern "C" void DSAManaged_GenerateKeyPair_m4445 ();
extern "C" void DSAManaged_add_m4446 ();
extern "C" void DSAManaged_GenerateParams_m4447 ();
extern "C" void DSAManaged_get_Random_m4448 ();
extern "C" void DSAManaged_get_KeySize_m4449 ();
extern "C" void DSAManaged_get_PublicOnly_m4450 ();
extern "C" void DSAManaged_NormalizeArray_m4451 ();
extern "C" void DSAManaged_ExportParameters_m4452 ();
extern "C" void DSAManaged_ImportParameters_m4453 ();
extern "C" void DSAManaged_CreateSignature_m4454 ();
extern "C" void DSAManaged_VerifySignature_m4455 ();
extern "C" void DSAManaged_Dispose_m4456 ();
extern "C" void KeyPairPersistence__ctor_m4457 ();
extern "C" void KeyPairPersistence__ctor_m4458 ();
extern "C" void KeyPairPersistence__cctor_m4459 ();
extern "C" void KeyPairPersistence_get_Filename_m4460 ();
extern "C" void KeyPairPersistence_get_KeyValue_m4461 ();
extern "C" void KeyPairPersistence_set_KeyValue_m4462 ();
extern "C" void KeyPairPersistence_Load_m4463 ();
extern "C" void KeyPairPersistence_Save_m4464 ();
extern "C" void KeyPairPersistence_Remove_m4465 ();
extern "C" void KeyPairPersistence_get_UserPath_m4466 ();
extern "C" void KeyPairPersistence_get_MachinePath_m4467 ();
extern "C" void KeyPairPersistence__CanSecure_m4468 ();
extern "C" void KeyPairPersistence__ProtectUser_m4469 ();
extern "C" void KeyPairPersistence__ProtectMachine_m4470 ();
extern "C" void KeyPairPersistence__IsUserProtected_m4471 ();
extern "C" void KeyPairPersistence__IsMachineProtected_m4472 ();
extern "C" void KeyPairPersistence_CanSecure_m4473 ();
extern "C" void KeyPairPersistence_ProtectUser_m4474 ();
extern "C" void KeyPairPersistence_ProtectMachine_m4475 ();
extern "C" void KeyPairPersistence_IsUserProtected_m4476 ();
extern "C" void KeyPairPersistence_IsMachineProtected_m4477 ();
extern "C" void KeyPairPersistence_get_CanChange_m4478 ();
extern "C" void KeyPairPersistence_get_UseDefaultKeyContainer_m4479 ();
extern "C" void KeyPairPersistence_get_UseMachineKeyStore_m4480 ();
extern "C" void KeyPairPersistence_get_ContainerName_m4481 ();
extern "C" void KeyPairPersistence_Copy_m4482 ();
extern "C" void KeyPairPersistence_FromXml_m4483 ();
extern "C" void KeyPairPersistence_ToXml_m4484 ();
extern "C" void MACAlgorithm__ctor_m4485 ();
extern "C" void MACAlgorithm_Initialize_m4486 ();
extern "C" void MACAlgorithm_Core_m4487 ();
extern "C" void MACAlgorithm_Final_m4488 ();
extern "C" void PKCS1__cctor_m4489 ();
extern "C" void PKCS1_Compare_m4490 ();
extern "C" void PKCS1_I2OSP_m4491 ();
extern "C" void PKCS1_OS2IP_m4492 ();
extern "C" void PKCS1_RSAEP_m4493 ();
extern "C" void PKCS1_RSASP1_m4494 ();
extern "C" void PKCS1_RSAVP1_m4495 ();
extern "C" void PKCS1_Encrypt_v15_m4496 ();
extern "C" void PKCS1_Sign_v15_m4497 ();
extern "C" void PKCS1_Verify_v15_m4498 ();
extern "C" void PKCS1_Verify_v15_m4499 ();
extern "C" void PKCS1_Encode_v15_m4500 ();
extern "C" void PrivateKeyInfo__ctor_m4501 ();
extern "C" void PrivateKeyInfo__ctor_m4502 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m4503 ();
extern "C" void PrivateKeyInfo_Decode_m4504 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m4505 ();
extern "C" void PrivateKeyInfo_Normalize_m4506 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m4507 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m4508 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m4509 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m4510 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m4511 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m4512 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m4513 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m4514 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m4515 ();
extern "C" void KeyGeneratedEventHandler__ctor_m4516 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m4517 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m4518 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m4519 ();
extern "C" void RSAManaged__ctor_m4520 ();
extern "C" void RSAManaged_add_KeyGenerated_m4521 ();
extern "C" void RSAManaged_remove_KeyGenerated_m4522 ();
extern "C" void RSAManaged_Finalize_m4523 ();
extern "C" void RSAManaged_GenerateKeyPair_m4524 ();
extern "C" void RSAManaged_get_KeySize_m4525 ();
extern "C" void RSAManaged_get_PublicOnly_m4526 ();
extern "C" void RSAManaged_DecryptValue_m4527 ();
extern "C" void RSAManaged_EncryptValue_m4528 ();
extern "C" void RSAManaged_ExportParameters_m4529 ();
extern "C" void RSAManaged_ImportParameters_m4530 ();
extern "C" void RSAManaged_Dispose_m4531 ();
extern "C" void RSAManaged_ToXmlString_m4532 ();
extern "C" void RSAManaged_get_IsCrtPossible_m4533 ();
extern "C" void RSAManaged_GetPaddedValue_m4534 ();
extern "C" void SymmetricTransform__ctor_m4535 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m4536 ();
extern "C" void SymmetricTransform_Finalize_m4537 ();
extern "C" void SymmetricTransform_Dispose_m4538 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m4539 ();
extern "C" void SymmetricTransform_Transform_m4540 ();
extern "C" void SymmetricTransform_CBC_m4541 ();
extern "C" void SymmetricTransform_CFB_m4542 ();
extern "C" void SymmetricTransform_OFB_m4543 ();
extern "C" void SymmetricTransform_CTS_m4544 ();
extern "C" void SymmetricTransform_CheckInput_m4545 ();
extern "C" void SymmetricTransform_TransformBlock_m4546 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m4547 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m4548 ();
extern "C" void SymmetricTransform_Random_m4549 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m4550 ();
extern "C" void SymmetricTransform_FinalEncrypt_m4551 ();
extern "C" void SymmetricTransform_FinalDecrypt_m4552 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m4553 ();
extern "C" void SafeBag__ctor_m4554 ();
extern "C" void SafeBag_get_BagOID_m4555 ();
extern "C" void SafeBag_get_ASN1_m4556 ();
extern "C" void DeriveBytes__ctor_m4557 ();
extern "C" void DeriveBytes__cctor_m4558 ();
extern "C" void DeriveBytes_set_HashName_m4559 ();
extern "C" void DeriveBytes_set_IterationCount_m4560 ();
extern "C" void DeriveBytes_set_Password_m4561 ();
extern "C" void DeriveBytes_set_Salt_m4562 ();
extern "C" void DeriveBytes_Adjust_m4563 ();
extern "C" void DeriveBytes_Derive_m4564 ();
extern "C" void DeriveBytes_DeriveKey_m4565 ();
extern "C" void DeriveBytes_DeriveIV_m4566 ();
extern "C" void DeriveBytes_DeriveMAC_m4567 ();
extern "C" void PKCS12__ctor_m4568 ();
extern "C" void PKCS12__ctor_m4569 ();
extern "C" void PKCS12__ctor_m4570 ();
extern "C" void PKCS12__cctor_m4571 ();
extern "C" void PKCS12_Decode_m4572 ();
extern "C" void PKCS12_Finalize_m4573 ();
extern "C" void PKCS12_set_Password_m4574 ();
extern "C" void PKCS12_get_Certificates_m4575 ();
extern "C" void PKCS12_Compare_m4576 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m4577 ();
extern "C" void PKCS12_Decrypt_m4578 ();
extern "C" void PKCS12_Decrypt_m4579 ();
extern "C" void PKCS12_GetExistingParameters_m4580 ();
extern "C" void PKCS12_AddPrivateKey_m4581 ();
extern "C" void PKCS12_ReadSafeBag_m4582 ();
extern "C" void PKCS12_MAC_m4583 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m4584 ();
extern "C" void X501__cctor_m4585 ();
extern "C" void X501_ToString_m4586 ();
extern "C" void X501_ToString_m4587 ();
extern "C" void X501_AppendEntry_m4588 ();
extern "C" void X509Certificate__ctor_m4589 ();
extern "C" void X509Certificate__cctor_m4590 ();
extern "C" void X509Certificate_Parse_m4591 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m4592 ();
extern "C" void X509Certificate_get_DSA_m4593 ();
extern "C" void X509Certificate_get_IssuerName_m4594 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m4595 ();
extern "C" void X509Certificate_get_PublicKey_m4596 ();
extern "C" void X509Certificate_get_RawData_m4597 ();
extern "C" void X509Certificate_get_SubjectName_m4598 ();
extern "C" void X509Certificate_get_ValidFrom_m4599 ();
extern "C" void X509Certificate_get_ValidUntil_m4600 ();
extern "C" void X509Certificate_GetIssuerName_m4601 ();
extern "C" void X509Certificate_GetSubjectName_m4602 ();
extern "C" void X509Certificate_GetObjectData_m4603 ();
extern "C" void X509Certificate_PEM_m4604 ();
extern "C" void X509CertificateEnumerator__ctor_m4605 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m4606 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m4607 ();
extern "C" void X509CertificateEnumerator_get_Current_m4608 ();
extern "C" void X509CertificateEnumerator_MoveNext_m4609 ();
extern "C" void X509CertificateCollection__ctor_m4610 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m4611 ();
extern "C" void X509CertificateCollection_get_Item_m4612 ();
extern "C" void X509CertificateCollection_Add_m4613 ();
extern "C" void X509CertificateCollection_GetEnumerator_m4614 ();
extern "C" void X509CertificateCollection_GetHashCode_m4615 ();
extern "C" void X509Extension__ctor_m4616 ();
extern "C" void X509Extension_Decode_m4617 ();
extern "C" void X509Extension_Equals_m4618 ();
extern "C" void X509Extension_GetHashCode_m4619 ();
extern "C" void X509Extension_WriteLine_m4620 ();
extern "C" void X509Extension_ToString_m4621 ();
extern "C" void X509ExtensionCollection__ctor_m4622 ();
extern "C" void X509ExtensionCollection__ctor_m4623 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m4624 ();
extern "C" void ASN1__ctor_m4625 ();
extern "C" void ASN1__ctor_m4626 ();
extern "C" void ASN1__ctor_m4627 ();
extern "C" void ASN1_get_Count_m4628 ();
extern "C" void ASN1_get_Tag_m4629 ();
extern "C" void ASN1_get_Length_m4630 ();
extern "C" void ASN1_get_Value_m4631 ();
extern "C" void ASN1_set_Value_m4632 ();
extern "C" void ASN1_CompareArray_m4633 ();
extern "C" void ASN1_CompareValue_m4634 ();
extern "C" void ASN1_Add_m4635 ();
extern "C" void ASN1_GetBytes_m4636 ();
extern "C" void ASN1_Decode_m4637 ();
extern "C" void ASN1_DecodeTLV_m4638 ();
extern "C" void ASN1_get_Item_m4639 ();
extern "C" void ASN1_Element_m4640 ();
extern "C" void ASN1_ToString_m4641 ();
extern "C" void ASN1Convert_ToInt32_m4642 ();
extern "C" void ASN1Convert_ToOid_m4643 ();
extern "C" void ASN1Convert_ToDateTime_m4644 ();
extern "C" void BitConverterLE_GetUIntBytes_m4645 ();
extern "C" void BitConverterLE_GetBytes_m4646 ();
extern "C" void BitConverterLE_UShortFromBytes_m4647 ();
extern "C" void BitConverterLE_UIntFromBytes_m4648 ();
extern "C" void BitConverterLE_ULongFromBytes_m4649 ();
extern "C" void BitConverterLE_ToInt16_m4650 ();
extern "C" void BitConverterLE_ToInt32_m4651 ();
extern "C" void BitConverterLE_ToSingle_m4652 ();
extern "C" void BitConverterLE_ToDouble_m4653 ();
extern "C" void ContentInfo__ctor_m4654 ();
extern "C" void ContentInfo__ctor_m4655 ();
extern "C" void ContentInfo__ctor_m4656 ();
extern "C" void ContentInfo__ctor_m4657 ();
extern "C" void ContentInfo_get_Content_m4658 ();
extern "C" void ContentInfo_set_Content_m4659 ();
extern "C" void ContentInfo_get_ContentType_m4660 ();
extern "C" void EncryptedData__ctor_m4661 ();
extern "C" void EncryptedData__ctor_m4662 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m4663 ();
extern "C" void EncryptedData_get_EncryptedContent_m4664 ();
extern "C" void StrongName__cctor_m4665 ();
extern "C" void StrongName_get_PublicKey_m4666 ();
extern "C" void StrongName_get_PublicKeyToken_m4667 ();
extern "C" void StrongName_get_TokenAlgorithm_m4668 ();
extern "C" void SecurityParser__ctor_m4669 ();
extern "C" void SecurityParser_LoadXml_m4670 ();
extern "C" void SecurityParser_ToXml_m4671 ();
extern "C" void SecurityParser_OnStartParsing_m4672 ();
extern "C" void SecurityParser_OnProcessingInstruction_m4673 ();
extern "C" void SecurityParser_OnIgnorableWhitespace_m4674 ();
extern "C" void SecurityParser_OnStartElement_m4675 ();
extern "C" void SecurityParser_OnEndElement_m4676 ();
extern "C" void SecurityParser_OnChars_m4677 ();
extern "C" void SecurityParser_OnEndParsing_m4678 ();
extern "C" void AttrListImpl__ctor_m4679 ();
extern "C" void AttrListImpl_get_Length_m4680 ();
extern "C" void AttrListImpl_GetName_m4681 ();
extern "C" void AttrListImpl_GetValue_m4682 ();
extern "C" void AttrListImpl_GetValue_m4683 ();
extern "C" void AttrListImpl_get_Names_m4684 ();
extern "C" void AttrListImpl_get_Values_m4685 ();
extern "C" void AttrListImpl_Clear_m4686 ();
extern "C" void AttrListImpl_Add_m4687 ();
extern "C" void SmallXmlParser__ctor_m4688 ();
extern "C" void SmallXmlParser_Error_m4689 ();
extern "C" void SmallXmlParser_UnexpectedEndError_m4690 ();
extern "C" void SmallXmlParser_IsNameChar_m4691 ();
extern "C" void SmallXmlParser_IsWhitespace_m4692 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m4693 ();
extern "C" void SmallXmlParser_HandleWhitespaces_m4694 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m4695 ();
extern "C" void SmallXmlParser_Peek_m4696 ();
extern "C" void SmallXmlParser_Read_m4697 ();
extern "C" void SmallXmlParser_Expect_m4698 ();
extern "C" void SmallXmlParser_ReadUntil_m4699 ();
extern "C" void SmallXmlParser_ReadName_m4700 ();
extern "C" void SmallXmlParser_Parse_m4701 ();
extern "C" void SmallXmlParser_Cleanup_m4702 ();
extern "C" void SmallXmlParser_ReadContent_m4703 ();
extern "C" void SmallXmlParser_HandleBufferedContent_m4704 ();
extern "C" void SmallXmlParser_ReadCharacters_m4705 ();
extern "C" void SmallXmlParser_ReadReference_m4706 ();
extern "C" void SmallXmlParser_ReadCharacterReference_m4707 ();
extern "C" void SmallXmlParser_ReadAttribute_m4708 ();
extern "C" void SmallXmlParser_ReadCDATASection_m4709 ();
extern "C" void SmallXmlParser_ReadComment_m4710 ();
extern "C" void SmallXmlParserException__ctor_m4711 ();
extern "C" void Runtime_GetDisplayName_m4712 ();
extern "C" void KeyNotFoundException__ctor_m4713 ();
extern "C" void KeyNotFoundException__ctor_m4714 ();
extern "C" void SimpleEnumerator__ctor_m4715 ();
extern "C" void SimpleEnumerator__cctor_m4716 ();
extern "C" void SimpleEnumerator_MoveNext_m4717 ();
extern "C" void SimpleEnumerator_get_Current_m4718 ();
extern "C" void ArrayListWrapper__ctor_m4719 ();
extern "C" void ArrayListWrapper_get_Item_m4720 ();
extern "C" void ArrayListWrapper_set_Item_m4721 ();
extern "C" void ArrayListWrapper_get_Count_m4722 ();
extern "C" void ArrayListWrapper_get_Capacity_m4723 ();
extern "C" void ArrayListWrapper_set_Capacity_m4724 ();
extern "C" void ArrayListWrapper_get_IsReadOnly_m4725 ();
extern "C" void ArrayListWrapper_get_IsSynchronized_m4726 ();
extern "C" void ArrayListWrapper_get_SyncRoot_m4727 ();
extern "C" void ArrayListWrapper_Add_m4728 ();
extern "C" void ArrayListWrapper_Clear_m4729 ();
extern "C" void ArrayListWrapper_Contains_m4730 ();
extern "C" void ArrayListWrapper_IndexOf_m4731 ();
extern "C" void ArrayListWrapper_IndexOf_m4732 ();
extern "C" void ArrayListWrapper_IndexOf_m4733 ();
extern "C" void ArrayListWrapper_Insert_m4734 ();
extern "C" void ArrayListWrapper_InsertRange_m4735 ();
extern "C" void ArrayListWrapper_Remove_m4736 ();
extern "C" void ArrayListWrapper_RemoveAt_m4737 ();
extern "C" void ArrayListWrapper_CopyTo_m4738 ();
extern "C" void ArrayListWrapper_CopyTo_m4739 ();
extern "C" void ArrayListWrapper_CopyTo_m4740 ();
extern "C" void ArrayListWrapper_GetEnumerator_m4741 ();
extern "C" void ArrayListWrapper_AddRange_m4742 ();
extern "C" void ArrayListWrapper_Clone_m4743 ();
extern "C" void ArrayListWrapper_Sort_m4744 ();
extern "C" void ArrayListWrapper_Sort_m4745 ();
extern "C" void ArrayListWrapper_ToArray_m4746 ();
extern "C" void ArrayListWrapper_ToArray_m4747 ();
extern "C" void SynchronizedArrayListWrapper__ctor_m4748 ();
extern "C" void SynchronizedArrayListWrapper_get_Item_m4749 ();
extern "C" void SynchronizedArrayListWrapper_set_Item_m4750 ();
extern "C" void SynchronizedArrayListWrapper_get_Count_m4751 ();
extern "C" void SynchronizedArrayListWrapper_get_Capacity_m4752 ();
extern "C" void SynchronizedArrayListWrapper_set_Capacity_m4753 ();
extern "C" void SynchronizedArrayListWrapper_get_IsReadOnly_m4754 ();
extern "C" void SynchronizedArrayListWrapper_get_IsSynchronized_m4755 ();
extern "C" void SynchronizedArrayListWrapper_get_SyncRoot_m4756 ();
extern "C" void SynchronizedArrayListWrapper_Add_m4757 ();
extern "C" void SynchronizedArrayListWrapper_Clear_m4758 ();
extern "C" void SynchronizedArrayListWrapper_Contains_m4759 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m4760 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m4761 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m4762 ();
extern "C" void SynchronizedArrayListWrapper_Insert_m4763 ();
extern "C" void SynchronizedArrayListWrapper_InsertRange_m4764 ();
extern "C" void SynchronizedArrayListWrapper_Remove_m4765 ();
extern "C" void SynchronizedArrayListWrapper_RemoveAt_m4766 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m4767 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m4768 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m4769 ();
extern "C" void SynchronizedArrayListWrapper_GetEnumerator_m4770 ();
extern "C" void SynchronizedArrayListWrapper_AddRange_m4771 ();
extern "C" void SynchronizedArrayListWrapper_Clone_m4772 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m4773 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m4774 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m4775 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m4776 ();
extern "C" void FixedSizeArrayListWrapper__ctor_m4777 ();
extern "C" void FixedSizeArrayListWrapper_get_ErrorMessage_m4778 ();
extern "C" void FixedSizeArrayListWrapper_get_Capacity_m4779 ();
extern "C" void FixedSizeArrayListWrapper_set_Capacity_m4780 ();
extern "C" void FixedSizeArrayListWrapper_Add_m4781 ();
extern "C" void FixedSizeArrayListWrapper_AddRange_m4782 ();
extern "C" void FixedSizeArrayListWrapper_Clear_m4783 ();
extern "C" void FixedSizeArrayListWrapper_Insert_m4784 ();
extern "C" void FixedSizeArrayListWrapper_InsertRange_m4785 ();
extern "C" void FixedSizeArrayListWrapper_Remove_m4786 ();
extern "C" void FixedSizeArrayListWrapper_RemoveAt_m4787 ();
extern "C" void ReadOnlyArrayListWrapper__ctor_m4788 ();
extern "C" void ReadOnlyArrayListWrapper_get_ErrorMessage_m4789 ();
extern "C" void ReadOnlyArrayListWrapper_get_IsReadOnly_m4790 ();
extern "C" void ReadOnlyArrayListWrapper_get_Item_m4791 ();
extern "C" void ReadOnlyArrayListWrapper_set_Item_m4792 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m4793 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m4794 ();
extern "C" void ArrayList__ctor_m2242 ();
extern "C" void ArrayList__ctor_m2275 ();
extern "C" void ArrayList__ctor_m2359 ();
extern "C" void ArrayList__ctor_m4795 ();
extern "C" void ArrayList__cctor_m4796 ();
extern "C" void ArrayList_get_Item_m4797 ();
extern "C" void ArrayList_set_Item_m4798 ();
extern "C" void ArrayList_get_Count_m4799 ();
extern "C" void ArrayList_get_Capacity_m4800 ();
extern "C" void ArrayList_set_Capacity_m4801 ();
extern "C" void ArrayList_get_IsReadOnly_m4802 ();
extern "C" void ArrayList_get_IsSynchronized_m4803 ();
extern "C" void ArrayList_get_SyncRoot_m4804 ();
extern "C" void ArrayList_EnsureCapacity_m4805 ();
extern "C" void ArrayList_Shift_m4806 ();
extern "C" void ArrayList_Add_m4807 ();
extern "C" void ArrayList_Clear_m4808 ();
extern "C" void ArrayList_Contains_m4809 ();
extern "C" void ArrayList_IndexOf_m4810 ();
extern "C" void ArrayList_IndexOf_m4811 ();
extern "C" void ArrayList_IndexOf_m4812 ();
extern "C" void ArrayList_Insert_m4813 ();
extern "C" void ArrayList_InsertRange_m4814 ();
extern "C" void ArrayList_Remove_m4815 ();
extern "C" void ArrayList_RemoveAt_m4816 ();
extern "C" void ArrayList_CopyTo_m4817 ();
extern "C" void ArrayList_CopyTo_m4818 ();
extern "C" void ArrayList_CopyTo_m4819 ();
extern "C" void ArrayList_GetEnumerator_m4820 ();
extern "C" void ArrayList_AddRange_m4821 ();
extern "C" void ArrayList_Sort_m4822 ();
extern "C" void ArrayList_Sort_m4823 ();
extern "C" void ArrayList_ToArray_m4824 ();
extern "C" void ArrayList_ToArray_m4825 ();
extern "C" void ArrayList_Clone_m4826 ();
extern "C" void ArrayList_ThrowNewArgumentOutOfRangeException_m4827 ();
extern "C" void ArrayList_Synchronized_m4828 ();
extern "C" void ArrayList_ReadOnly_m3308 ();
extern "C" void BitArrayEnumerator__ctor_m4829 ();
extern "C" void BitArrayEnumerator_get_Current_m4830 ();
extern "C" void BitArrayEnumerator_MoveNext_m4831 ();
extern "C" void BitArrayEnumerator_checkVersion_m4832 ();
extern "C" void BitArray__ctor_m2395 ();
extern "C" void BitArray_getByte_m4833 ();
extern "C" void BitArray_get_Count_m4834 ();
extern "C" void BitArray_get_Item_m2388 ();
extern "C" void BitArray_set_Item_m2396 ();
extern "C" void BitArray_get_Length_m2387 ();
extern "C" void BitArray_get_SyncRoot_m4835 ();
extern "C" void BitArray_CopyTo_m4836 ();
extern "C" void BitArray_Get_m4837 ();
extern "C" void BitArray_Set_m4838 ();
extern "C" void BitArray_GetEnumerator_m4839 ();
extern "C" void CaseInsensitiveComparer__ctor_m4840 ();
extern "C" void CaseInsensitiveComparer__ctor_m4841 ();
extern "C" void CaseInsensitiveComparer__cctor_m4842 ();
extern "C" void CaseInsensitiveComparer_get_DefaultInvariant_m2228 ();
extern "C" void CaseInsensitiveComparer_Compare_m4843 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m4844 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m4845 ();
extern "C" void CaseInsensitiveHashCodeProvider__cctor_m4846 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m4847 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m4848 ();
extern "C" void CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m2229 ();
extern "C" void CaseInsensitiveHashCodeProvider_GetHashCode_m4849 ();
extern "C" void CollectionBase__ctor_m2341 ();
extern "C" void CollectionBase_System_Collections_ICollection_CopyTo_m4850 ();
extern "C" void CollectionBase_System_Collections_ICollection_get_SyncRoot_m4851 ();
extern "C" void CollectionBase_System_Collections_IList_Add_m4852 ();
extern "C" void CollectionBase_System_Collections_IList_Contains_m4853 ();
extern "C" void CollectionBase_System_Collections_IList_IndexOf_m4854 ();
extern "C" void CollectionBase_System_Collections_IList_Insert_m4855 ();
extern "C" void CollectionBase_System_Collections_IList_Remove_m4856 ();
extern "C" void CollectionBase_System_Collections_IList_get_Item_m4857 ();
extern "C" void CollectionBase_System_Collections_IList_set_Item_m4858 ();
extern "C" void CollectionBase_get_Count_m4859 ();
extern "C" void CollectionBase_GetEnumerator_m4860 ();
extern "C" void CollectionBase_Clear_m4861 ();
extern "C" void CollectionBase_RemoveAt_m4862 ();
extern "C" void CollectionBase_get_InnerList_m2336 ();
extern "C" void CollectionBase_get_List_m2393 ();
extern "C" void CollectionBase_OnClear_m4863 ();
extern "C" void CollectionBase_OnClearComplete_m4864 ();
extern "C" void CollectionBase_OnInsert_m4865 ();
extern "C" void CollectionBase_OnInsertComplete_m4866 ();
extern "C" void CollectionBase_OnRemove_m4867 ();
extern "C" void CollectionBase_OnRemoveComplete_m4868 ();
extern "C" void CollectionBase_OnSet_m4869 ();
extern "C" void CollectionBase_OnSetComplete_m4870 ();
extern "C" void CollectionBase_OnValidate_m4871 ();
extern "C" void Comparer__ctor_m4872 ();
extern "C" void Comparer__ctor_m4873 ();
extern "C" void Comparer__cctor_m4874 ();
extern "C" void Comparer_Compare_m4875 ();
extern "C" void Comparer_GetObjectData_m4876 ();
extern "C" void DictionaryEntry__ctor_m2233 ();
extern "C" void DictionaryEntry_get_Key_m4877 ();
extern "C" void DictionaryEntry_get_Value_m4878 ();
extern "C" void KeyMarker__ctor_m4879 ();
extern "C" void KeyMarker__cctor_m4880 ();
extern "C" void Enumerator__ctor_m4881 ();
extern "C" void Enumerator__cctor_m4882 ();
extern "C" void Enumerator_FailFast_m4883 ();
extern "C" void Enumerator_Reset_m4884 ();
extern "C" void Enumerator_MoveNext_m4885 ();
extern "C" void Enumerator_get_Entry_m4886 ();
extern "C" void Enumerator_get_Key_m4887 ();
extern "C" void Enumerator_get_Value_m4888 ();
extern "C" void Enumerator_get_Current_m4889 ();
extern "C" void HashKeys__ctor_m4890 ();
extern "C" void HashKeys_get_Count_m4891 ();
extern "C" void HashKeys_get_SyncRoot_m4892 ();
extern "C" void HashKeys_CopyTo_m4893 ();
extern "C" void HashKeys_GetEnumerator_m4894 ();
extern "C" void HashValues__ctor_m4895 ();
extern "C" void HashValues_get_Count_m4896 ();
extern "C" void HashValues_get_SyncRoot_m4897 ();
extern "C" void HashValues_CopyTo_m4898 ();
extern "C" void HashValues_GetEnumerator_m4899 ();
extern "C" void Hashtable__ctor_m2378 ();
extern "C" void Hashtable__ctor_m4900 ();
extern "C" void Hashtable__ctor_m4901 ();
extern "C" void Hashtable__ctor_m2380 ();
extern "C" void Hashtable__ctor_m4902 ();
extern "C" void Hashtable__ctor_m2230 ();
extern "C" void Hashtable__ctor_m4903 ();
extern "C" void Hashtable__ctor_m2231 ();
extern "C" void Hashtable__ctor_m2272 ();
extern "C" void Hashtable__ctor_m4904 ();
extern "C" void Hashtable__ctor_m2241 ();
extern "C" void Hashtable__ctor_m4905 ();
extern "C" void Hashtable__cctor_m4906 ();
extern "C" void Hashtable_System_Collections_IEnumerable_GetEnumerator_m4907 ();
extern "C" void Hashtable_set_comparer_m4908 ();
extern "C" void Hashtable_set_hcp_m4909 ();
extern "C" void Hashtable_get_Count_m4910 ();
extern "C" void Hashtable_get_SyncRoot_m4911 ();
extern "C" void Hashtable_get_Keys_m4912 ();
extern "C" void Hashtable_get_Values_m4913 ();
extern "C" void Hashtable_get_Item_m4914 ();
extern "C" void Hashtable_set_Item_m4915 ();
extern "C" void Hashtable_CopyTo_m4916 ();
extern "C" void Hashtable_Add_m4917 ();
extern "C" void Hashtable_Clear_m4918 ();
extern "C" void Hashtable_Contains_m4919 ();
extern "C" void Hashtable_GetEnumerator_m4920 ();
extern "C" void Hashtable_Remove_m4921 ();
extern "C" void Hashtable_ContainsKey_m4922 ();
extern "C" void Hashtable_Clone_m4923 ();
extern "C" void Hashtable_GetObjectData_m4924 ();
extern "C" void Hashtable_OnDeserialization_m4925 ();
extern "C" void Hashtable_GetHash_m4926 ();
extern "C" void Hashtable_KeyEquals_m4927 ();
extern "C" void Hashtable_AdjustThreshold_m4928 ();
extern "C" void Hashtable_SetTable_m4929 ();
extern "C" void Hashtable_Find_m4930 ();
extern "C" void Hashtable_Rehash_m4931 ();
extern "C" void Hashtable_PutImpl_m4932 ();
extern "C" void Hashtable_CopyToArray_m4933 ();
extern "C" void Hashtable_TestPrime_m4934 ();
extern "C" void Hashtable_CalcPrime_m4935 ();
extern "C" void Hashtable_ToPrime_m4936 ();
extern "C" void Enumerator__ctor_m4937 ();
extern "C" void Enumerator__cctor_m4938 ();
extern "C" void Enumerator_Reset_m4939 ();
extern "C" void Enumerator_MoveNext_m4940 ();
extern "C" void Enumerator_get_Entry_m4941 ();
extern "C" void Enumerator_get_Key_m4942 ();
extern "C" void Enumerator_get_Value_m4943 ();
extern "C" void Enumerator_get_Current_m4944 ();
extern "C" void SortedList__ctor_m4945 ();
extern "C" void SortedList__ctor_m2270 ();
extern "C" void SortedList__ctor_m4946 ();
extern "C" void SortedList__cctor_m4947 ();
extern "C" void SortedList_System_Collections_IEnumerable_GetEnumerator_m4948 ();
extern "C" void SortedList_get_Count_m4949 ();
extern "C" void SortedList_get_SyncRoot_m4950 ();
extern "C" void SortedList_get_IsFixedSize_m4951 ();
extern "C" void SortedList_get_IsReadOnly_m4952 ();
extern "C" void SortedList_get_Item_m4953 ();
extern "C" void SortedList_set_Item_m4954 ();
extern "C" void SortedList_get_Capacity_m4955 ();
extern "C" void SortedList_set_Capacity_m4956 ();
extern "C" void SortedList_Add_m4957 ();
extern "C" void SortedList_Contains_m4958 ();
extern "C" void SortedList_GetEnumerator_m4959 ();
extern "C" void SortedList_Remove_m4960 ();
extern "C" void SortedList_CopyTo_m4961 ();
extern "C" void SortedList_RemoveAt_m4962 ();
extern "C" void SortedList_IndexOfKey_m4963 ();
extern "C" void SortedList_ContainsKey_m4964 ();
extern "C" void SortedList_GetByIndex_m4965 ();
extern "C" void SortedList_EnsureCapacity_m4966 ();
extern "C" void SortedList_PutImpl_m4967 ();
extern "C" void SortedList_GetImpl_m4968 ();
extern "C" void SortedList_InitTable_m4969 ();
extern "C" void SortedList_Find_m4970 ();
extern "C" void Enumerator__ctor_m4971 ();
extern "C" void Enumerator_get_Current_m4972 ();
extern "C" void Enumerator_MoveNext_m4973 ();
extern "C" void Stack__ctor_m1336 ();
extern "C" void Stack_Resize_m4974 ();
extern "C" void Stack_get_Count_m4975 ();
extern "C" void Stack_get_SyncRoot_m4976 ();
extern "C" void Stack_Clear_m4977 ();
extern "C" void Stack_CopyTo_m4978 ();
extern "C" void Stack_GetEnumerator_m4979 ();
extern "C" void Stack_Peek_m4980 ();
extern "C" void Stack_Pop_m4981 ();
extern "C" void Stack_Push_m4982 ();
extern "C" void SuppressMessageAttribute__ctor_m4983 ();
extern "C" void SuppressMessageAttribute_set_Justification_m4984 ();
extern "C" void DebuggableAttribute__ctor_m4985 ();
extern "C" void DebuggerDisplayAttribute__ctor_m4986 ();
extern "C" void DebuggerDisplayAttribute_set_Name_m4987 ();
extern "C" void DebuggerStepThroughAttribute__ctor_m4988 ();
extern "C" void DebuggerTypeProxyAttribute__ctor_m4989 ();
extern "C" void StackFrame__ctor_m4990 ();
extern "C" void StackFrame__ctor_m4991 ();
extern "C" void StackFrame_get_frame_info_m4992 ();
extern "C" void StackFrame_GetFileLineNumber_m4993 ();
extern "C" void StackFrame_GetFileName_m4994 ();
extern "C" void StackFrame_GetSecureFileName_m4995 ();
extern "C" void StackFrame_GetILOffset_m4996 ();
extern "C" void StackFrame_GetMethod_m4997 ();
extern "C" void StackFrame_GetNativeOffset_m4998 ();
extern "C" void StackFrame_GetInternalMethodName_m4999 ();
extern "C" void StackFrame_ToString_m5000 ();
extern "C" void StackTrace__ctor_m5001 ();
extern "C" void StackTrace__ctor_m1315 ();
extern "C" void StackTrace__ctor_m5002 ();
extern "C" void StackTrace__ctor_m5003 ();
extern "C" void StackTrace__ctor_m5004 ();
extern "C" void StackTrace_init_frames_m5005 ();
extern "C" void StackTrace_get_trace_m5006 ();
extern "C" void StackTrace_get_FrameCount_m5007 ();
extern "C" void StackTrace_GetFrame_m5008 ();
extern "C" void StackTrace_ToString_m5009 ();
extern "C" void Calendar__ctor_m5010 ();
extern "C" void Calendar_CheckReadOnly_m5011 ();
extern "C" void Calendar_get_EraNames_m5012 ();
extern "C" void CCMath_div_m5013 ();
extern "C" void CCMath_mod_m5014 ();
extern "C" void CCMath_div_mod_m5015 ();
extern "C" void CCFixed_FromDateTime_m5016 ();
extern "C" void CCFixed_day_of_week_m5017 ();
extern "C" void CCGregorianCalendar_is_leap_year_m5018 ();
extern "C" void CCGregorianCalendar_fixed_from_dmy_m5019 ();
extern "C" void CCGregorianCalendar_year_from_fixed_m5020 ();
extern "C" void CCGregorianCalendar_my_from_fixed_m5021 ();
extern "C" void CCGregorianCalendar_dmy_from_fixed_m5022 ();
extern "C" void CCGregorianCalendar_month_from_fixed_m5023 ();
extern "C" void CCGregorianCalendar_day_from_fixed_m5024 ();
extern "C" void CCGregorianCalendar_GetDayOfMonth_m5025 ();
extern "C" void CCGregorianCalendar_GetMonth_m5026 ();
extern "C" void CCGregorianCalendar_GetYear_m5027 ();
extern "C" void CompareInfo__ctor_m5028 ();
extern "C" void CompareInfo__ctor_m5029 ();
extern "C" void CompareInfo__cctor_m5030 ();
extern "C" void CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m5031 ();
extern "C" void CompareInfo_get_UseManagedCollation_m5032 ();
extern "C" void CompareInfo_construct_compareinfo_m5033 ();
extern "C" void CompareInfo_free_internal_collator_m5034 ();
extern "C" void CompareInfo_internal_compare_m5035 ();
extern "C" void CompareInfo_assign_sortkey_m5036 ();
extern "C" void CompareInfo_internal_index_m5037 ();
extern "C" void CompareInfo_Finalize_m5038 ();
extern "C" void CompareInfo_internal_compare_managed_m5039 ();
extern "C" void CompareInfo_internal_compare_switch_m5040 ();
extern "C" void CompareInfo_Compare_m5041 ();
extern "C" void CompareInfo_Compare_m5042 ();
extern "C" void CompareInfo_Compare_m5043 ();
extern "C" void CompareInfo_Equals_m5044 ();
extern "C" void CompareInfo_GetHashCode_m5045 ();
extern "C" void CompareInfo_GetSortKey_m5046 ();
extern "C" void CompareInfo_IndexOf_m5047 ();
extern "C" void CompareInfo_internal_index_managed_m5048 ();
extern "C" void CompareInfo_internal_index_switch_m5049 ();
extern "C" void CompareInfo_IndexOf_m5050 ();
extern "C" void CompareInfo_IsPrefix_m5051 ();
extern "C" void CompareInfo_IsSuffix_m5052 ();
extern "C" void CompareInfo_LastIndexOf_m5053 ();
extern "C" void CompareInfo_LastIndexOf_m5054 ();
extern "C" void CompareInfo_ToString_m5055 ();
extern "C" void CompareInfo_get_LCID_m5056 ();
extern "C" void CultureInfo__ctor_m5057 ();
extern "C" void CultureInfo__ctor_m5058 ();
extern "C" void CultureInfo__ctor_m5059 ();
extern "C" void CultureInfo__ctor_m5060 ();
extern "C" void CultureInfo__ctor_m5061 ();
extern "C" void CultureInfo__cctor_m5062 ();
extern "C" void CultureInfo_get_InvariantCulture_m1272 ();
extern "C" void CultureInfo_get_CurrentCulture_m3337 ();
extern "C" void CultureInfo_get_CurrentUICulture_m3338 ();
extern "C" void CultureInfo_ConstructCurrentCulture_m5063 ();
extern "C" void CultureInfo_ConstructCurrentUICulture_m5064 ();
extern "C" void CultureInfo_get_LCID_m5065 ();
extern "C" void CultureInfo_get_Name_m5066 ();
extern "C" void CultureInfo_get_Parent_m5067 ();
extern "C" void CultureInfo_get_TextInfo_m5068 ();
extern "C" void CultureInfo_get_IcuName_m5069 ();
extern "C" void CultureInfo_Equals_m5070 ();
extern "C" void CultureInfo_GetHashCode_m5071 ();
extern "C" void CultureInfo_ToString_m5072 ();
extern "C" void CultureInfo_get_CompareInfo_m5073 ();
extern "C" void CultureInfo_get_IsNeutralCulture_m5074 ();
extern "C" void CultureInfo_CheckNeutral_m5075 ();
extern "C" void CultureInfo_get_NumberFormat_m5076 ();
extern "C" void CultureInfo_get_DateTimeFormat_m5077 ();
extern "C" void CultureInfo_get_IsReadOnly_m5078 ();
extern "C" void CultureInfo_GetFormat_m5079 ();
extern "C" void CultureInfo_Construct_m5080 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromName_m5081 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromLcid_m5082 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromCurrentLocale_m5083 ();
extern "C" void CultureInfo_construct_internal_locale_from_lcid_m5084 ();
extern "C" void CultureInfo_construct_internal_locale_from_name_m5085 ();
extern "C" void CultureInfo_construct_internal_locale_from_current_locale_m5086 ();
extern "C" void CultureInfo_construct_datetime_format_m5087 ();
extern "C" void CultureInfo_construct_number_format_m5088 ();
extern "C" void CultureInfo_ConstructInvariant_m5089 ();
extern "C" void CultureInfo_CreateTextInfo_m5090 ();
extern "C" void CultureInfo_CreateCulture_m5091 ();
extern "C" void DateTimeFormatInfo__ctor_m5092 ();
extern "C" void DateTimeFormatInfo__ctor_m5093 ();
extern "C" void DateTimeFormatInfo__cctor_m5094 ();
extern "C" void DateTimeFormatInfo_GetInstance_m5095 ();
extern "C" void DateTimeFormatInfo_get_IsReadOnly_m5096 ();
extern "C" void DateTimeFormatInfo_ReadOnly_m5097 ();
extern "C" void DateTimeFormatInfo_Clone_m5098 ();
extern "C" void DateTimeFormatInfo_GetFormat_m5099 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedMonthName_m5100 ();
extern "C" void DateTimeFormatInfo_GetEraName_m5101 ();
extern "C" void DateTimeFormatInfo_GetMonthName_m5102 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedDayNames_m5103 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m5104 ();
extern "C" void DateTimeFormatInfo_get_RawDayNames_m5105 ();
extern "C" void DateTimeFormatInfo_get_RawMonthNames_m5106 ();
extern "C" void DateTimeFormatInfo_get_AMDesignator_m5107 ();
extern "C" void DateTimeFormatInfo_get_PMDesignator_m5108 ();
extern "C" void DateTimeFormatInfo_get_DateSeparator_m5109 ();
extern "C" void DateTimeFormatInfo_get_TimeSeparator_m5110 ();
extern "C" void DateTimeFormatInfo_get_LongDatePattern_m5111 ();
extern "C" void DateTimeFormatInfo_get_ShortDatePattern_m5112 ();
extern "C" void DateTimeFormatInfo_get_ShortTimePattern_m5113 ();
extern "C" void DateTimeFormatInfo_get_LongTimePattern_m5114 ();
extern "C" void DateTimeFormatInfo_get_MonthDayPattern_m5115 ();
extern "C" void DateTimeFormatInfo_get_YearMonthPattern_m5116 ();
extern "C" void DateTimeFormatInfo_get_FullDateTimePattern_m5117 ();
extern "C" void DateTimeFormatInfo_get_CurrentInfo_m5118 ();
extern "C" void DateTimeFormatInfo_get_InvariantInfo_m5119 ();
extern "C" void DateTimeFormatInfo_get_Calendar_m5120 ();
extern "C" void DateTimeFormatInfo_set_Calendar_m5121 ();
extern "C" void DateTimeFormatInfo_get_RFC1123Pattern_m5122 ();
extern "C" void DateTimeFormatInfo_get_RoundtripPattern_m5123 ();
extern "C" void DateTimeFormatInfo_get_SortableDateTimePattern_m5124 ();
extern "C" void DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m5125 ();
extern "C" void DateTimeFormatInfo_GetAllDateTimePatternsInternal_m5126 ();
extern "C" void DateTimeFormatInfo_FillAllDateTimePatterns_m5127 ();
extern "C" void DateTimeFormatInfo_GetAllRawDateTimePatterns_m5128 ();
extern "C" void DateTimeFormatInfo_GetDayName_m5129 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedDayName_m5130 ();
extern "C" void DateTimeFormatInfo_FillInvariantPatterns_m5131 ();
extern "C" void DateTimeFormatInfo_PopulateCombinedList_m5132 ();
extern "C" void DaylightTime__ctor_m5133 ();
extern "C" void DaylightTime_get_Start_m5134 ();
extern "C" void DaylightTime_get_End_m5135 ();
extern "C" void DaylightTime_get_Delta_m5136 ();
extern "C" void GregorianCalendar__ctor_m5137 ();
extern "C" void GregorianCalendar__ctor_m5138 ();
extern "C" void GregorianCalendar_get_Eras_m5139 ();
extern "C" void GregorianCalendar_set_CalendarType_m5140 ();
extern "C" void GregorianCalendar_GetDayOfMonth_m5141 ();
extern "C" void GregorianCalendar_GetDayOfWeek_m5142 ();
extern "C" void GregorianCalendar_GetEra_m5143 ();
extern "C" void GregorianCalendar_GetMonth_m5144 ();
extern "C" void GregorianCalendar_GetYear_m5145 ();
extern "C" void NumberFormatInfo__ctor_m5146 ();
extern "C" void NumberFormatInfo__ctor_m5147 ();
extern "C" void NumberFormatInfo__ctor_m5148 ();
extern "C" void NumberFormatInfo__cctor_m5149 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalDigits_m5150 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalSeparator_m5151 ();
extern "C" void NumberFormatInfo_get_CurrencyGroupSeparator_m5152 ();
extern "C" void NumberFormatInfo_get_RawCurrencyGroupSizes_m5153 ();
extern "C" void NumberFormatInfo_get_CurrencyNegativePattern_m5154 ();
extern "C" void NumberFormatInfo_get_CurrencyPositivePattern_m5155 ();
extern "C" void NumberFormatInfo_get_CurrencySymbol_m5156 ();
extern "C" void NumberFormatInfo_get_CurrentInfo_m5157 ();
extern "C" void NumberFormatInfo_get_InvariantInfo_m5158 ();
extern "C" void NumberFormatInfo_get_NaNSymbol_m5159 ();
extern "C" void NumberFormatInfo_get_NegativeInfinitySymbol_m5160 ();
extern "C" void NumberFormatInfo_get_NegativeSign_m5161 ();
extern "C" void NumberFormatInfo_get_NumberDecimalDigits_m5162 ();
extern "C" void NumberFormatInfo_get_NumberDecimalSeparator_m5163 ();
extern "C" void NumberFormatInfo_get_NumberGroupSeparator_m5164 ();
extern "C" void NumberFormatInfo_get_RawNumberGroupSizes_m5165 ();
extern "C" void NumberFormatInfo_get_NumberNegativePattern_m5166 ();
extern "C" void NumberFormatInfo_set_NumberNegativePattern_m5167 ();
extern "C" void NumberFormatInfo_get_PercentDecimalDigits_m5168 ();
extern "C" void NumberFormatInfo_get_PercentDecimalSeparator_m5169 ();
extern "C" void NumberFormatInfo_get_PercentGroupSeparator_m5170 ();
extern "C" void NumberFormatInfo_get_RawPercentGroupSizes_m5171 ();
extern "C" void NumberFormatInfo_get_PercentNegativePattern_m5172 ();
extern "C" void NumberFormatInfo_get_PercentPositivePattern_m5173 ();
extern "C" void NumberFormatInfo_get_PercentSymbol_m5174 ();
extern "C" void NumberFormatInfo_get_PerMilleSymbol_m5175 ();
extern "C" void NumberFormatInfo_get_PositiveInfinitySymbol_m5176 ();
extern "C" void NumberFormatInfo_get_PositiveSign_m5177 ();
extern "C" void NumberFormatInfo_GetFormat_m5178 ();
extern "C" void NumberFormatInfo_Clone_m5179 ();
extern "C" void NumberFormatInfo_GetInstance_m5180 ();
extern "C" void TextInfo__ctor_m5181 ();
extern "C" void TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m5182 ();
extern "C" void TextInfo_get_CultureName_m5183 ();
extern "C" void TextInfo_Equals_m5184 ();
extern "C" void TextInfo_GetHashCode_m5185 ();
extern "C" void TextInfo_ToString_m5186 ();
extern "C" void TextInfo_ToLower_m5187 ();
extern "C" void TextInfo_ToUpper_m5188 ();
extern "C" void TextInfo_ToLower_m5189 ();
extern "C" void TextInfo_ToUpper_m5190 ();
extern "C" void IsolatedStorageException__ctor_m5191 ();
extern "C" void IsolatedStorageException__ctor_m5192 ();
extern "C" void IsolatedStorageException__ctor_m5193 ();
extern "C" void BinaryReader__ctor_m5194 ();
extern "C" void BinaryReader__ctor_m5195 ();
extern "C" void BinaryReader_System_IDisposable_Dispose_m5196 ();
extern "C" void BinaryReader_Dispose_m5197 ();
extern "C" void BinaryReader_FillBuffer_m5198 ();
extern "C" void BinaryReader_Read_m5199 ();
extern "C" void BinaryReader_Read_m5200 ();
extern "C" void BinaryReader_Read_m5201 ();
extern "C" void BinaryReader_ReadCharBytes_m5202 ();
extern "C" void BinaryReader_Read7BitEncodedInt_m5203 ();
extern "C" void BinaryReader_ReadBoolean_m5204 ();
extern "C" void BinaryReader_ReadByte_m5205 ();
extern "C" void BinaryReader_ReadChar_m5206 ();
extern "C" void BinaryReader_ReadDecimal_m5207 ();
extern "C" void BinaryReader_ReadDouble_m5208 ();
extern "C" void BinaryReader_ReadInt16_m5209 ();
extern "C" void BinaryReader_ReadInt32_m5210 ();
extern "C" void BinaryReader_ReadInt64_m5211 ();
extern "C" void BinaryReader_ReadSByte_m5212 ();
extern "C" void BinaryReader_ReadString_m5213 ();
extern "C" void BinaryReader_ReadSingle_m5214 ();
extern "C" void BinaryReader_ReadUInt16_m5215 ();
extern "C" void BinaryReader_ReadUInt32_m5216 ();
extern "C" void BinaryReader_ReadUInt64_m5217 ();
extern "C" void BinaryReader_CheckBuffer_m5218 ();
extern "C" void Directory_CreateDirectory_m3324 ();
extern "C" void Directory_CreateDirectoriesInternal_m5219 ();
extern "C" void Directory_Exists_m3323 ();
extern "C" void Directory_GetCurrentDirectory_m5220 ();
extern "C" void Directory_GetFiles_m3326 ();
extern "C" void Directory_GetFileSystemEntries_m5221 ();
extern "C" void DirectoryInfo__ctor_m5222 ();
extern "C" void DirectoryInfo__ctor_m5223 ();
extern "C" void DirectoryInfo__ctor_m5224 ();
extern "C" void DirectoryInfo_Initialize_m5225 ();
extern "C" void DirectoryInfo_get_Exists_m5226 ();
extern "C" void DirectoryInfo_get_Parent_m5227 ();
extern "C" void DirectoryInfo_Create_m5228 ();
extern "C" void DirectoryInfo_ToString_m5229 ();
extern "C" void DirectoryNotFoundException__ctor_m5230 ();
extern "C" void DirectoryNotFoundException__ctor_m5231 ();
extern "C" void DirectoryNotFoundException__ctor_m5232 ();
extern "C" void EndOfStreamException__ctor_m5233 ();
extern "C" void EndOfStreamException__ctor_m5234 ();
extern "C" void File_Delete_m5235 ();
extern "C" void File_Exists_m5236 ();
extern "C" void File_Open_m5237 ();
extern "C" void File_OpenRead_m3322 ();
extern "C" void File_OpenText_m5238 ();
extern "C" void FileNotFoundException__ctor_m5239 ();
extern "C" void FileNotFoundException__ctor_m5240 ();
extern "C" void FileNotFoundException__ctor_m5241 ();
extern "C" void FileNotFoundException_get_Message_m5242 ();
extern "C" void FileNotFoundException_GetObjectData_m5243 ();
extern "C" void FileNotFoundException_ToString_m5244 ();
extern "C" void ReadDelegate__ctor_m5245 ();
extern "C" void ReadDelegate_Invoke_m5246 ();
extern "C" void ReadDelegate_BeginInvoke_m5247 ();
extern "C" void ReadDelegate_EndInvoke_m5248 ();
extern "C" void WriteDelegate__ctor_m5249 ();
extern "C" void WriteDelegate_Invoke_m5250 ();
extern "C" void WriteDelegate_BeginInvoke_m5251 ();
extern "C" void WriteDelegate_EndInvoke_m5252 ();
extern "C" void FileStream__ctor_m5253 ();
extern "C" void FileStream__ctor_m5254 ();
extern "C" void FileStream__ctor_m5255 ();
extern "C" void FileStream__ctor_m5256 ();
extern "C" void FileStream__ctor_m5257 ();
extern "C" void FileStream_get_CanRead_m5258 ();
extern "C" void FileStream_get_CanWrite_m5259 ();
extern "C" void FileStream_get_CanSeek_m5260 ();
extern "C" void FileStream_get_Length_m5261 ();
extern "C" void FileStream_get_Position_m5262 ();
extern "C" void FileStream_set_Position_m5263 ();
extern "C" void FileStream_ReadByte_m5264 ();
extern "C" void FileStream_WriteByte_m5265 ();
extern "C" void FileStream_Read_m5266 ();
extern "C" void FileStream_ReadInternal_m5267 ();
extern "C" void FileStream_BeginRead_m5268 ();
extern "C" void FileStream_EndRead_m5269 ();
extern "C" void FileStream_Write_m5270 ();
extern "C" void FileStream_WriteInternal_m5271 ();
extern "C" void FileStream_BeginWrite_m5272 ();
extern "C" void FileStream_EndWrite_m5273 ();
extern "C" void FileStream_Seek_m5274 ();
extern "C" void FileStream_SetLength_m5275 ();
extern "C" void FileStream_Flush_m5276 ();
extern "C" void FileStream_Finalize_m5277 ();
extern "C" void FileStream_Dispose_m5278 ();
extern "C" void FileStream_ReadSegment_m5279 ();
extern "C" void FileStream_WriteSegment_m5280 ();
extern "C" void FileStream_FlushBuffer_m5281 ();
extern "C" void FileStream_FlushBuffer_m5282 ();
extern "C" void FileStream_FlushBufferIfDirty_m5283 ();
extern "C" void FileStream_RefillBuffer_m5284 ();
extern "C" void FileStream_ReadData_m5285 ();
extern "C" void FileStream_InitBuffer_m5286 ();
extern "C" void FileStream_GetSecureFileName_m5287 ();
extern "C" void FileStream_GetSecureFileName_m5288 ();
extern "C" void FileStreamAsyncResult__ctor_m5289 ();
extern "C" void FileStreamAsyncResult_CBWrapper_m5290 ();
extern "C" void FileStreamAsyncResult_get_AsyncState_m5291 ();
extern "C" void FileStreamAsyncResult_get_AsyncWaitHandle_m5292 ();
extern "C" void FileStreamAsyncResult_get_IsCompleted_m5293 ();
extern "C" void FileSystemInfo__ctor_m5294 ();
extern "C" void FileSystemInfo__ctor_m5295 ();
extern "C" void FileSystemInfo_GetObjectData_m5296 ();
extern "C" void FileSystemInfo_get_FullName_m5297 ();
extern "C" void FileSystemInfo_Refresh_m5298 ();
extern "C" void FileSystemInfo_InternalRefresh_m5299 ();
extern "C" void FileSystemInfo_CheckPath_m5300 ();
extern "C" void IOException__ctor_m5301 ();
extern "C" void IOException__ctor_m5302 ();
extern "C" void IOException__ctor_m3351 ();
extern "C" void IOException__ctor_m5303 ();
extern "C" void IOException__ctor_m5304 ();
extern "C" void MemoryStream__ctor_m3352 ();
extern "C" void MemoryStream__ctor_m1223 ();
extern "C" void MemoryStream__ctor_m3357 ();
extern "C" void MemoryStream_InternalConstructor_m5305 ();
extern "C" void MemoryStream_CheckIfClosedThrowDisposed_m5306 ();
extern "C" void MemoryStream_get_CanRead_m5307 ();
extern "C" void MemoryStream_get_CanSeek_m5308 ();
extern "C" void MemoryStream_get_CanWrite_m5309 ();
extern "C" void MemoryStream_set_Capacity_m5310 ();
extern "C" void MemoryStream_get_Length_m5311 ();
extern "C" void MemoryStream_get_Position_m5312 ();
extern "C" void MemoryStream_set_Position_m5313 ();
extern "C" void MemoryStream_Dispose_m5314 ();
extern "C" void MemoryStream_Flush_m5315 ();
extern "C" void MemoryStream_Read_m5316 ();
extern "C" void MemoryStream_ReadByte_m5317 ();
extern "C" void MemoryStream_Seek_m5318 ();
extern "C" void MemoryStream_CalculateNewCapacity_m5319 ();
extern "C" void MemoryStream_Expand_m5320 ();
extern "C" void MemoryStream_SetLength_m5321 ();
extern "C" void MemoryStream_ToArray_m5322 ();
extern "C" void MemoryStream_Write_m5323 ();
extern "C" void MemoryStream_WriteByte_m5324 ();
extern "C" void MonoIO__cctor_m5325 ();
extern "C" void MonoIO_GetException_m5326 ();
extern "C" void MonoIO_GetException_m5327 ();
extern "C" void MonoIO_CreateDirectory_m5328 ();
extern "C" void MonoIO_GetFileSystemEntries_m5329 ();
extern "C" void MonoIO_GetCurrentDirectory_m5330 ();
extern "C" void MonoIO_DeleteFile_m5331 ();
extern "C" void MonoIO_GetFileAttributes_m5332 ();
extern "C" void MonoIO_GetFileType_m5333 ();
extern "C" void MonoIO_ExistsFile_m5334 ();
extern "C" void MonoIO_ExistsDirectory_m5335 ();
extern "C" void MonoIO_GetFileStat_m5336 ();
extern "C" void MonoIO_Open_m5337 ();
extern "C" void MonoIO_Close_m5338 ();
extern "C" void MonoIO_Read_m5339 ();
extern "C" void MonoIO_Write_m5340 ();
extern "C" void MonoIO_Seek_m5341 ();
extern "C" void MonoIO_GetLength_m5342 ();
extern "C" void MonoIO_SetLength_m5343 ();
extern "C" void MonoIO_get_ConsoleOutput_m5344 ();
extern "C" void MonoIO_get_ConsoleInput_m5345 ();
extern "C" void MonoIO_get_ConsoleError_m5346 ();
extern "C" void MonoIO_get_VolumeSeparatorChar_m5347 ();
extern "C" void MonoIO_get_DirectorySeparatorChar_m5348 ();
extern "C" void MonoIO_get_AltDirectorySeparatorChar_m5349 ();
extern "C" void MonoIO_get_PathSeparator_m5350 ();
extern "C" void Path__cctor_m5351 ();
extern "C" void Path_Combine_m3325 ();
extern "C" void Path_CleanPath_m5352 ();
extern "C" void Path_GetDirectoryName_m5353 ();
extern "C" void Path_GetFileName_m5354 ();
extern "C" void Path_GetFullPath_m5355 ();
extern "C" void Path_WindowsDriveAdjustment_m5356 ();
extern "C" void Path_InsecureGetFullPath_m5357 ();
extern "C" void Path_IsDsc_m5358 ();
extern "C" void Path_GetPathRoot_m5359 ();
extern "C" void Path_IsPathRooted_m5360 ();
extern "C" void Path_GetInvalidPathChars_m5361 ();
extern "C" void Path_GetServerAndShare_m5362 ();
extern "C" void Path_SameRoot_m5363 ();
extern "C" void Path_CanonicalizePath_m5364 ();
extern "C" void PathTooLongException__ctor_m5365 ();
extern "C" void PathTooLongException__ctor_m5366 ();
extern "C" void PathTooLongException__ctor_m5367 ();
extern "C" void SearchPattern__cctor_m5368 ();
extern "C" void Stream__ctor_m3353 ();
extern "C" void Stream__cctor_m5369 ();
extern "C" void Stream_Dispose_m5370 ();
extern "C" void Stream_Dispose_m3356 ();
extern "C" void Stream_Close_m3355 ();
extern "C" void Stream_ReadByte_m5371 ();
extern "C" void Stream_WriteByte_m5372 ();
extern "C" void Stream_BeginRead_m5373 ();
extern "C" void Stream_BeginWrite_m5374 ();
extern "C" void Stream_EndRead_m5375 ();
extern "C" void Stream_EndWrite_m5376 ();
extern "C" void NullStream__ctor_m5377 ();
extern "C" void NullStream_get_CanRead_m5378 ();
extern "C" void NullStream_get_CanSeek_m5379 ();
extern "C" void NullStream_get_CanWrite_m5380 ();
extern "C" void NullStream_get_Length_m5381 ();
extern "C" void NullStream_get_Position_m5382 ();
extern "C" void NullStream_set_Position_m5383 ();
extern "C" void NullStream_Flush_m5384 ();
extern "C" void NullStream_Read_m5385 ();
extern "C" void NullStream_ReadByte_m5386 ();
extern "C" void NullStream_Seek_m5387 ();
extern "C" void NullStream_SetLength_m5388 ();
extern "C" void NullStream_Write_m5389 ();
extern "C" void NullStream_WriteByte_m5390 ();
extern "C" void StreamAsyncResult__ctor_m5391 ();
extern "C" void StreamAsyncResult_SetComplete_m5392 ();
extern "C" void StreamAsyncResult_SetComplete_m5393 ();
extern "C" void StreamAsyncResult_get_AsyncState_m5394 ();
extern "C" void StreamAsyncResult_get_AsyncWaitHandle_m5395 ();
extern "C" void StreamAsyncResult_get_IsCompleted_m5396 ();
extern "C" void StreamAsyncResult_get_Exception_m5397 ();
extern "C" void StreamAsyncResult_get_NBytes_m5398 ();
extern "C" void StreamAsyncResult_get_Done_m5399 ();
extern "C" void StreamAsyncResult_set_Done_m5400 ();
extern "C" void NullStreamReader__ctor_m5401 ();
extern "C" void NullStreamReader_Peek_m5402 ();
extern "C" void NullStreamReader_Read_m5403 ();
extern "C" void NullStreamReader_Read_m5404 ();
extern "C" void NullStreamReader_ReadLine_m5405 ();
extern "C" void NullStreamReader_ReadToEnd_m5406 ();
extern "C" void StreamReader__ctor_m5407 ();
extern "C" void StreamReader__ctor_m5408 ();
extern "C" void StreamReader__ctor_m5409 ();
extern "C" void StreamReader__ctor_m5410 ();
extern "C" void StreamReader__ctor_m5411 ();
extern "C" void StreamReader__cctor_m5412 ();
extern "C" void StreamReader_Initialize_m5413 ();
extern "C" void StreamReader_Dispose_m5414 ();
extern "C" void StreamReader_DoChecks_m5415 ();
extern "C" void StreamReader_ReadBuffer_m5416 ();
extern "C" void StreamReader_Peek_m5417 ();
extern "C" void StreamReader_Read_m5418 ();
extern "C" void StreamReader_Read_m5419 ();
extern "C" void StreamReader_FindNextEOL_m5420 ();
extern "C" void StreamReader_ReadLine_m5421 ();
extern "C" void StreamReader_ReadToEnd_m5422 ();
extern "C" void StreamWriter__ctor_m5423 ();
extern "C" void StreamWriter__ctor_m5424 ();
extern "C" void StreamWriter__cctor_m5425 ();
extern "C" void StreamWriter_Initialize_m5426 ();
extern "C" void StreamWriter_set_AutoFlush_m5427 ();
extern "C" void StreamWriter_Dispose_m5428 ();
extern "C" void StreamWriter_Flush_m5429 ();
extern "C" void StreamWriter_FlushBytes_m5430 ();
extern "C" void StreamWriter_Decode_m5431 ();
extern "C" void StreamWriter_Write_m5432 ();
extern "C" void StreamWriter_LowLevelWrite_m5433 ();
extern "C" void StreamWriter_LowLevelWrite_m5434 ();
extern "C" void StreamWriter_Write_m5435 ();
extern "C" void StreamWriter_Write_m5436 ();
extern "C" void StreamWriter_Write_m5437 ();
extern "C" void StreamWriter_Close_m5438 ();
extern "C" void StreamWriter_Finalize_m5439 ();
extern "C" void StringReader__ctor_m1216 ();
extern "C" void StringReader_Dispose_m5440 ();
extern "C" void StringReader_Peek_m5441 ();
extern "C" void StringReader_Read_m5442 ();
extern "C" void StringReader_Read_m5443 ();
extern "C" void StringReader_ReadLine_m5444 ();
extern "C" void StringReader_ReadToEnd_m5445 ();
extern "C" void StringReader_CheckObjectDisposedException_m5446 ();
extern "C" void NullTextReader__ctor_m5447 ();
extern "C" void NullTextReader_ReadLine_m5448 ();
extern "C" void TextReader__ctor_m5449 ();
extern "C" void TextReader__cctor_m5450 ();
extern "C" void TextReader_Dispose_m5451 ();
extern "C" void TextReader_Dispose_m5452 ();
extern "C" void TextReader_Peek_m5453 ();
extern "C" void TextReader_Read_m5454 ();
extern "C" void TextReader_Read_m5455 ();
extern "C" void TextReader_ReadLine_m5456 ();
extern "C" void TextReader_ReadToEnd_m5457 ();
extern "C" void TextReader_Synchronized_m5458 ();
extern "C" void SynchronizedReader__ctor_m5459 ();
extern "C" void SynchronizedReader_Peek_m5460 ();
extern "C" void SynchronizedReader_ReadLine_m5461 ();
extern "C" void SynchronizedReader_ReadToEnd_m5462 ();
extern "C" void SynchronizedReader_Read_m5463 ();
extern "C" void SynchronizedReader_Read_m5464 ();
extern "C" void NullTextWriter__ctor_m5465 ();
extern "C" void NullTextWriter_Write_m5466 ();
extern "C" void NullTextWriter_Write_m5467 ();
extern "C" void NullTextWriter_Write_m5468 ();
extern "C" void TextWriter__ctor_m5469 ();
extern "C" void TextWriter__cctor_m5470 ();
extern "C" void TextWriter_Close_m5471 ();
extern "C" void TextWriter_Dispose_m5472 ();
extern "C" void TextWriter_Dispose_m5473 ();
extern "C" void TextWriter_Flush_m5474 ();
extern "C" void TextWriter_Synchronized_m5475 ();
extern "C" void TextWriter_Write_m5476 ();
extern "C" void TextWriter_Write_m5477 ();
extern "C" void TextWriter_Write_m5478 ();
extern "C" void TextWriter_Write_m5479 ();
extern "C" void TextWriter_WriteLine_m5480 ();
extern "C" void TextWriter_WriteLine_m5481 ();
extern "C" void SynchronizedWriter__ctor_m5482 ();
extern "C" void SynchronizedWriter_Close_m5483 ();
extern "C" void SynchronizedWriter_Flush_m5484 ();
extern "C" void SynchronizedWriter_Write_m5485 ();
extern "C" void SynchronizedWriter_Write_m5486 ();
extern "C" void SynchronizedWriter_Write_m5487 ();
extern "C" void SynchronizedWriter_Write_m5488 ();
extern "C" void SynchronizedWriter_WriteLine_m5489 ();
extern "C" void SynchronizedWriter_WriteLine_m5490 ();
extern "C" void UnexceptionalStreamReader__ctor_m5491 ();
extern "C" void UnexceptionalStreamReader__cctor_m5492 ();
extern "C" void UnexceptionalStreamReader_Peek_m5493 ();
extern "C" void UnexceptionalStreamReader_Read_m5494 ();
extern "C" void UnexceptionalStreamReader_Read_m5495 ();
extern "C" void UnexceptionalStreamReader_CheckEOL_m5496 ();
extern "C" void UnexceptionalStreamReader_ReadLine_m5497 ();
extern "C" void UnexceptionalStreamReader_ReadToEnd_m5498 ();
extern "C" void UnexceptionalStreamWriter__ctor_m5499 ();
extern "C" void UnexceptionalStreamWriter_Flush_m5500 ();
extern "C" void UnexceptionalStreamWriter_Write_m5501 ();
extern "C" void UnexceptionalStreamWriter_Write_m5502 ();
extern "C" void UnexceptionalStreamWriter_Write_m5503 ();
extern "C" void UnexceptionalStreamWriter_Write_m5504 ();
extern "C" void AssemblyBuilder_get_Location_m5505 ();
extern "C" void AssemblyBuilder_GetModulesInternal_m5506 ();
extern "C" void AssemblyBuilder_GetTypes_m5507 ();
extern "C" void AssemblyBuilder_get_IsCompilerContext_m5508 ();
extern "C" void AssemblyBuilder_not_supported_m5509 ();
extern "C" void AssemblyBuilder_UnprotectedGetName_m5510 ();
extern "C" void ConstructorBuilder__ctor_m5511 ();
extern "C" void ConstructorBuilder_get_CallingConvention_m5512 ();
extern "C" void ConstructorBuilder_get_TypeBuilder_m5513 ();
extern "C" void ConstructorBuilder_GetParameters_m5514 ();
extern "C" void ConstructorBuilder_GetParametersInternal_m5515 ();
extern "C" void ConstructorBuilder_GetParameterCount_m5516 ();
extern "C" void ConstructorBuilder_Invoke_m5517 ();
extern "C" void ConstructorBuilder_Invoke_m5518 ();
extern "C" void ConstructorBuilder_get_MethodHandle_m5519 ();
extern "C" void ConstructorBuilder_get_Attributes_m5520 ();
extern "C" void ConstructorBuilder_get_ReflectedType_m5521 ();
extern "C" void ConstructorBuilder_get_DeclaringType_m5522 ();
extern "C" void ConstructorBuilder_get_Name_m5523 ();
extern "C" void ConstructorBuilder_IsDefined_m5524 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m5525 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m5526 ();
extern "C" void ConstructorBuilder_GetILGenerator_m5527 ();
extern "C" void ConstructorBuilder_GetILGenerator_m5528 ();
extern "C" void ConstructorBuilder_GetToken_m5529 ();
extern "C" void ConstructorBuilder_get_Module_m5530 ();
extern "C" void ConstructorBuilder_ToString_m5531 ();
extern "C" void ConstructorBuilder_fixup_m5532 ();
extern "C" void ConstructorBuilder_get_next_table_index_m5533 ();
extern "C" void ConstructorBuilder_get_IsCompilerContext_m5534 ();
extern "C" void ConstructorBuilder_not_supported_m5535 ();
extern "C" void ConstructorBuilder_not_created_m5536 ();
extern "C" void EnumBuilder_get_Assembly_m5537 ();
extern "C" void EnumBuilder_get_AssemblyQualifiedName_m5538 ();
extern "C" void EnumBuilder_get_BaseType_m5539 ();
extern "C" void EnumBuilder_get_DeclaringType_m5540 ();
extern "C" void EnumBuilder_get_FullName_m5541 ();
extern "C" void EnumBuilder_get_Module_m5542 ();
extern "C" void EnumBuilder_get_Name_m5543 ();
extern "C" void EnumBuilder_get_Namespace_m5544 ();
extern "C" void EnumBuilder_get_ReflectedType_m5545 ();
extern "C" void EnumBuilder_get_TypeHandle_m5546 ();
extern "C" void EnumBuilder_get_UnderlyingSystemType_m5547 ();
extern "C" void EnumBuilder_GetAttributeFlagsImpl_m5548 ();
extern "C" void EnumBuilder_GetConstructorImpl_m5549 ();
extern "C" void EnumBuilder_GetConstructors_m5550 ();
extern "C" void EnumBuilder_GetCustomAttributes_m5551 ();
extern "C" void EnumBuilder_GetCustomAttributes_m5552 ();
extern "C" void EnumBuilder_GetElementType_m5553 ();
extern "C" void EnumBuilder_GetEvent_m5554 ();
extern "C" void EnumBuilder_GetField_m5555 ();
extern "C" void EnumBuilder_GetFields_m5556 ();
extern "C" void EnumBuilder_GetInterfaces_m5557 ();
extern "C" void EnumBuilder_GetMethodImpl_m5558 ();
extern "C" void EnumBuilder_GetMethods_m5559 ();
extern "C" void EnumBuilder_GetProperties_m5560 ();
extern "C" void EnumBuilder_GetPropertyImpl_m5561 ();
extern "C" void EnumBuilder_HasElementTypeImpl_m5562 ();
extern "C" void EnumBuilder_InvokeMember_m5563 ();
extern "C" void EnumBuilder_IsArrayImpl_m5564 ();
extern "C" void EnumBuilder_IsByRefImpl_m5565 ();
extern "C" void EnumBuilder_IsPointerImpl_m5566 ();
extern "C" void EnumBuilder_IsPrimitiveImpl_m5567 ();
extern "C" void EnumBuilder_IsValueTypeImpl_m5568 ();
extern "C" void EnumBuilder_IsDefined_m5569 ();
extern "C" void EnumBuilder_CreateNotSupportedException_m5570 ();
extern "C" void FieldBuilder_get_Attributes_m5571 ();
extern "C" void FieldBuilder_get_DeclaringType_m5572 ();
extern "C" void FieldBuilder_get_FieldHandle_m5573 ();
extern "C" void FieldBuilder_get_FieldType_m5574 ();
extern "C" void FieldBuilder_get_Name_m5575 ();
extern "C" void FieldBuilder_get_ReflectedType_m5576 ();
extern "C" void FieldBuilder_GetCustomAttributes_m5577 ();
extern "C" void FieldBuilder_GetCustomAttributes_m5578 ();
extern "C" void FieldBuilder_GetValue_m5579 ();
extern "C" void FieldBuilder_IsDefined_m5580 ();
extern "C" void FieldBuilder_GetFieldOffset_m5581 ();
extern "C" void FieldBuilder_SetValue_m5582 ();
extern "C" void FieldBuilder_get_UMarshal_m5583 ();
extern "C" void FieldBuilder_CreateNotSupportedException_m5584 ();
extern "C" void FieldBuilder_get_Module_m5585 ();
extern "C" void GenericTypeParameterBuilder_IsSubclassOf_m5586 ();
extern "C" void GenericTypeParameterBuilder_GetAttributeFlagsImpl_m5587 ();
extern "C" void GenericTypeParameterBuilder_GetConstructorImpl_m5588 ();
extern "C" void GenericTypeParameterBuilder_GetConstructors_m5589 ();
extern "C" void GenericTypeParameterBuilder_GetEvent_m5590 ();
extern "C" void GenericTypeParameterBuilder_GetField_m5591 ();
extern "C" void GenericTypeParameterBuilder_GetFields_m5592 ();
extern "C" void GenericTypeParameterBuilder_GetInterfaces_m5593 ();
extern "C" void GenericTypeParameterBuilder_GetMethods_m5594 ();
extern "C" void GenericTypeParameterBuilder_GetMethodImpl_m5595 ();
extern "C" void GenericTypeParameterBuilder_GetProperties_m5596 ();
extern "C" void GenericTypeParameterBuilder_GetPropertyImpl_m5597 ();
extern "C" void GenericTypeParameterBuilder_HasElementTypeImpl_m5598 ();
extern "C" void GenericTypeParameterBuilder_IsAssignableFrom_m5599 ();
extern "C" void GenericTypeParameterBuilder_IsInstanceOfType_m5600 ();
extern "C" void GenericTypeParameterBuilder_IsArrayImpl_m5601 ();
extern "C" void GenericTypeParameterBuilder_IsByRefImpl_m5602 ();
extern "C" void GenericTypeParameterBuilder_IsPointerImpl_m5603 ();
extern "C" void GenericTypeParameterBuilder_IsPrimitiveImpl_m5604 ();
extern "C" void GenericTypeParameterBuilder_IsValueTypeImpl_m5605 ();
extern "C" void GenericTypeParameterBuilder_InvokeMember_m5606 ();
extern "C" void GenericTypeParameterBuilder_GetElementType_m5607 ();
extern "C" void GenericTypeParameterBuilder_get_UnderlyingSystemType_m5608 ();
extern "C" void GenericTypeParameterBuilder_get_Assembly_m5609 ();
extern "C" void GenericTypeParameterBuilder_get_AssemblyQualifiedName_m5610 ();
extern "C" void GenericTypeParameterBuilder_get_BaseType_m5611 ();
extern "C" void GenericTypeParameterBuilder_get_FullName_m5612 ();
extern "C" void GenericTypeParameterBuilder_IsDefined_m5613 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m5614 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m5615 ();
extern "C" void GenericTypeParameterBuilder_get_Name_m5616 ();
extern "C" void GenericTypeParameterBuilder_get_Namespace_m5617 ();
extern "C" void GenericTypeParameterBuilder_get_Module_m5618 ();
extern "C" void GenericTypeParameterBuilder_get_DeclaringType_m5619 ();
extern "C" void GenericTypeParameterBuilder_get_ReflectedType_m5620 ();
extern "C" void GenericTypeParameterBuilder_get_TypeHandle_m5621 ();
extern "C" void GenericTypeParameterBuilder_GetGenericArguments_m5622 ();
extern "C" void GenericTypeParameterBuilder_GetGenericTypeDefinition_m5623 ();
extern "C" void GenericTypeParameterBuilder_get_ContainsGenericParameters_m5624 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericParameter_m5625 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericType_m5626 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m5627 ();
extern "C" void GenericTypeParameterBuilder_not_supported_m5628 ();
extern "C" void GenericTypeParameterBuilder_ToString_m5629 ();
extern "C" void GenericTypeParameterBuilder_Equals_m5630 ();
extern "C" void GenericTypeParameterBuilder_GetHashCode_m5631 ();
extern "C" void GenericTypeParameterBuilder_MakeGenericType_m5632 ();
extern "C" void ILGenerator__ctor_m5633 ();
extern "C" void ILGenerator__cctor_m5634 ();
extern "C" void ILGenerator_add_token_fixup_m5635 ();
extern "C" void ILGenerator_make_room_m5636 ();
extern "C" void ILGenerator_emit_int_m5637 ();
extern "C" void ILGenerator_ll_emit_m5638 ();
extern "C" void ILGenerator_Emit_m5639 ();
extern "C" void ILGenerator_Emit_m5640 ();
extern "C" void ILGenerator_label_fixup_m5641 ();
extern "C" void ILGenerator_Mono_GetCurrentOffset_m5642 ();
extern "C" void MethodBuilder_get_ContainsGenericParameters_m5643 ();
extern "C" void MethodBuilder_get_MethodHandle_m5644 ();
extern "C" void MethodBuilder_get_ReturnType_m5645 ();
extern "C" void MethodBuilder_get_ReflectedType_m5646 ();
extern "C" void MethodBuilder_get_DeclaringType_m5647 ();
extern "C" void MethodBuilder_get_Name_m5648 ();
extern "C" void MethodBuilder_get_Attributes_m5649 ();
extern "C" void MethodBuilder_get_CallingConvention_m5650 ();
extern "C" void MethodBuilder_GetBaseDefinition_m5651 ();
extern "C" void MethodBuilder_GetParameters_m5652 ();
extern "C" void MethodBuilder_GetParameterCount_m5653 ();
extern "C" void MethodBuilder_Invoke_m5654 ();
extern "C" void MethodBuilder_IsDefined_m5655 ();
extern "C" void MethodBuilder_GetCustomAttributes_m5656 ();
extern "C" void MethodBuilder_GetCustomAttributes_m5657 ();
extern "C" void MethodBuilder_check_override_m5658 ();
extern "C" void MethodBuilder_fixup_m5659 ();
extern "C" void MethodBuilder_ToString_m5660 ();
extern "C" void MethodBuilder_Equals_m5661 ();
extern "C" void MethodBuilder_GetHashCode_m5662 ();
extern "C" void MethodBuilder_get_next_table_index_m5663 ();
extern "C" void MethodBuilder_NotSupported_m5664 ();
extern "C" void MethodBuilder_MakeGenericMethod_m5665 ();
extern "C" void MethodBuilder_get_IsGenericMethodDefinition_m5666 ();
extern "C" void MethodBuilder_get_IsGenericMethod_m5667 ();
extern "C" void MethodBuilder_GetGenericArguments_m5668 ();
extern "C" void MethodBuilder_get_Module_m5669 ();
extern "C" void MethodToken__ctor_m5670 ();
extern "C" void MethodToken__cctor_m5671 ();
extern "C" void MethodToken_Equals_m5672 ();
extern "C" void MethodToken_GetHashCode_m5673 ();
extern "C" void MethodToken_get_Token_m5674 ();
extern "C" void ModuleBuilder__cctor_m5675 ();
extern "C" void ModuleBuilder_get_next_table_index_m5676 ();
extern "C" void ModuleBuilder_GetTypes_m5677 ();
extern "C" void ModuleBuilder_getToken_m5678 ();
extern "C" void ModuleBuilder_GetToken_m5679 ();
extern "C" void ModuleBuilder_RegisterToken_m5680 ();
extern "C" void ModuleBuilder_GetTokenGenerator_m5681 ();
extern "C" void ModuleBuilderTokenGenerator__ctor_m5682 ();
extern "C" void ModuleBuilderTokenGenerator_GetToken_m5683 ();
extern "C" void OpCode__ctor_m5684 ();
extern "C" void OpCode_GetHashCode_m5685 ();
extern "C" void OpCode_Equals_m5686 ();
extern "C" void OpCode_ToString_m5687 ();
extern "C" void OpCode_get_Name_m5688 ();
extern "C" void OpCode_get_Size_m5689 ();
extern "C" void OpCode_get_StackBehaviourPop_m5690 ();
extern "C" void OpCode_get_StackBehaviourPush_m5691 ();
extern "C" void OpCodeNames__cctor_m5692 ();
extern "C" void OpCodes__cctor_m5693 ();
extern "C" void ParameterBuilder_get_Attributes_m5694 ();
extern "C" void ParameterBuilder_get_Name_m5695 ();
extern "C" void ParameterBuilder_get_Position_m5696 ();
extern "C" void PropertyBuilder_get_Attributes_m5697 ();
extern "C" void PropertyBuilder_get_CanRead_m5698 ();
extern "C" void PropertyBuilder_get_CanWrite_m5699 ();
extern "C" void PropertyBuilder_get_DeclaringType_m5700 ();
extern "C" void PropertyBuilder_get_Name_m5701 ();
extern "C" void PropertyBuilder_get_PropertyType_m5702 ();
extern "C" void PropertyBuilder_get_ReflectedType_m5703 ();
extern "C" void PropertyBuilder_GetAccessors_m5704 ();
extern "C" void PropertyBuilder_GetCustomAttributes_m5705 ();
extern "C" void PropertyBuilder_GetCustomAttributes_m5706 ();
extern "C" void PropertyBuilder_GetGetMethod_m5707 ();
extern "C" void PropertyBuilder_GetIndexParameters_m5708 ();
extern "C" void PropertyBuilder_GetSetMethod_m5709 ();
extern "C" void PropertyBuilder_GetValue_m5710 ();
extern "C" void PropertyBuilder_GetValue_m5711 ();
extern "C" void PropertyBuilder_IsDefined_m5712 ();
extern "C" void PropertyBuilder_SetValue_m5713 ();
extern "C" void PropertyBuilder_SetValue_m5714 ();
extern "C" void PropertyBuilder_get_Module_m5715 ();
extern "C" void PropertyBuilder_not_supported_m5716 ();
extern "C" void TypeBuilder_GetAttributeFlagsImpl_m5717 ();
extern "C" void TypeBuilder_setup_internal_class_m5718 ();
extern "C" void TypeBuilder_create_generic_class_m5719 ();
extern "C" void TypeBuilder_get_Assembly_m5720 ();
extern "C" void TypeBuilder_get_AssemblyQualifiedName_m5721 ();
extern "C" void TypeBuilder_get_BaseType_m5722 ();
extern "C" void TypeBuilder_get_DeclaringType_m5723 ();
extern "C" void TypeBuilder_get_UnderlyingSystemType_m5724 ();
extern "C" void TypeBuilder_get_FullName_m5725 ();
extern "C" void TypeBuilder_get_Module_m5726 ();
extern "C" void TypeBuilder_get_Name_m5727 ();
extern "C" void TypeBuilder_get_Namespace_m5728 ();
extern "C" void TypeBuilder_get_ReflectedType_m5729 ();
extern "C" void TypeBuilder_GetConstructorImpl_m5730 ();
extern "C" void TypeBuilder_IsDefined_m5731 ();
extern "C" void TypeBuilder_GetCustomAttributes_m5732 ();
extern "C" void TypeBuilder_GetCustomAttributes_m5733 ();
extern "C" void TypeBuilder_DefineConstructor_m5734 ();
extern "C" void TypeBuilder_DefineConstructor_m5735 ();
extern "C" void TypeBuilder_DefineDefaultConstructor_m5736 ();
extern "C" void TypeBuilder_create_runtime_class_m5737 ();
extern "C" void TypeBuilder_is_nested_in_m5738 ();
extern "C" void TypeBuilder_has_ctor_method_m5739 ();
extern "C" void TypeBuilder_CreateType_m5740 ();
extern "C" void TypeBuilder_GetConstructors_m5741 ();
extern "C" void TypeBuilder_GetConstructorsInternal_m5742 ();
extern "C" void TypeBuilder_GetElementType_m5743 ();
extern "C" void TypeBuilder_GetEvent_m5744 ();
extern "C" void TypeBuilder_GetField_m5745 ();
extern "C" void TypeBuilder_GetFields_m5746 ();
extern "C" void TypeBuilder_GetInterfaces_m5747 ();
extern "C" void TypeBuilder_GetMethodsByName_m5748 ();
extern "C" void TypeBuilder_GetMethods_m5749 ();
extern "C" void TypeBuilder_GetMethodImpl_m5750 ();
extern "C" void TypeBuilder_GetProperties_m5751 ();
extern "C" void TypeBuilder_GetPropertyImpl_m5752 ();
extern "C" void TypeBuilder_HasElementTypeImpl_m5753 ();
extern "C" void TypeBuilder_InvokeMember_m5754 ();
extern "C" void TypeBuilder_IsArrayImpl_m5755 ();
extern "C" void TypeBuilder_IsByRefImpl_m5756 ();
extern "C" void TypeBuilder_IsPointerImpl_m5757 ();
extern "C" void TypeBuilder_IsPrimitiveImpl_m5758 ();
extern "C" void TypeBuilder_IsValueTypeImpl_m5759 ();
extern "C" void TypeBuilder_MakeGenericType_m5760 ();
extern "C" void TypeBuilder_get_TypeHandle_m5761 ();
extern "C" void TypeBuilder_SetParent_m5762 ();
extern "C" void TypeBuilder_get_next_table_index_m5763 ();
extern "C" void TypeBuilder_get_IsCompilerContext_m5764 ();
extern "C" void TypeBuilder_get_is_created_m5765 ();
extern "C" void TypeBuilder_not_supported_m5766 ();
extern "C" void TypeBuilder_check_not_created_m5767 ();
extern "C" void TypeBuilder_check_created_m5768 ();
extern "C" void TypeBuilder_ToString_m5769 ();
extern "C" void TypeBuilder_IsAssignableFrom_m5770 ();
extern "C" void TypeBuilder_IsSubclassOf_m5771 ();
extern "C" void TypeBuilder_IsAssignableTo_m5772 ();
extern "C" void TypeBuilder_GetGenericArguments_m5773 ();
extern "C" void TypeBuilder_GetGenericTypeDefinition_m5774 ();
extern "C" void TypeBuilder_get_ContainsGenericParameters_m5775 ();
extern "C" void TypeBuilder_get_IsGenericParameter_m5776 ();
extern "C" void TypeBuilder_get_IsGenericTypeDefinition_m5777 ();
extern "C" void TypeBuilder_get_IsGenericType_m5778 ();
extern "C" void UnmanagedMarshal_ToMarshalAsAttribute_m5779 ();
extern "C" void AmbiguousMatchException__ctor_m5780 ();
extern "C" void AmbiguousMatchException__ctor_m5781 ();
extern "C" void AmbiguousMatchException__ctor_m5782 ();
extern "C" void ResolveEventHolder__ctor_m5783 ();
extern "C" void Assembly__ctor_m5784 ();
extern "C" void Assembly_get_code_base_m5785 ();
extern "C" void Assembly_get_fullname_m5786 ();
extern "C" void Assembly_get_location_m5787 ();
extern "C" void Assembly_GetCodeBase_m5788 ();
extern "C" void Assembly_get_FullName_m5789 ();
extern "C" void Assembly_get_Location_m5790 ();
extern "C" void Assembly_IsDefined_m5791 ();
extern "C" void Assembly_GetCustomAttributes_m5792 ();
extern "C" void Assembly_GetManifestResourceInternal_m5793 ();
extern "C" void Assembly_GetTypes_m5794 ();
extern "C" void Assembly_GetTypes_m5795 ();
extern "C" void Assembly_GetType_m5796 ();
extern "C" void Assembly_GetType_m5797 ();
extern "C" void Assembly_InternalGetType_m5798 ();
extern "C" void Assembly_GetType_m5799 ();
extern "C" void Assembly_FillName_m5800 ();
extern "C" void Assembly_GetName_m5801 ();
extern "C" void Assembly_GetName_m5802 ();
extern "C" void Assembly_UnprotectedGetName_m5803 ();
extern "C" void Assembly_ToString_m5804 ();
extern "C" void Assembly_Load_m5805 ();
extern "C" void Assembly_GetModule_m5806 ();
extern "C" void Assembly_GetModulesInternal_m5807 ();
extern "C" void Assembly_GetModules_m5808 ();
extern "C" void Assembly_GetExecutingAssembly_m5809 ();
extern "C" void AssemblyCompanyAttribute__ctor_m5810 ();
extern "C" void AssemblyCopyrightAttribute__ctor_m5811 ();
extern "C" void AssemblyDefaultAliasAttribute__ctor_m5812 ();
extern "C" void AssemblyDelaySignAttribute__ctor_m5813 ();
extern "C" void AssemblyDescriptionAttribute__ctor_m5814 ();
extern "C" void AssemblyFileVersionAttribute__ctor_m5815 ();
extern "C" void AssemblyInformationalVersionAttribute__ctor_m5816 ();
extern "C" void AssemblyKeyFileAttribute__ctor_m5817 ();
extern "C" void AssemblyName__ctor_m5818 ();
extern "C" void AssemblyName__ctor_m5819 ();
extern "C" void AssemblyName_get_Name_m5820 ();
extern "C" void AssemblyName_get_Flags_m5821 ();
extern "C" void AssemblyName_get_FullName_m5822 ();
extern "C" void AssemblyName_get_Version_m5823 ();
extern "C" void AssemblyName_set_Version_m5824 ();
extern "C" void AssemblyName_ToString_m5825 ();
extern "C" void AssemblyName_get_IsPublicKeyValid_m5826 ();
extern "C" void AssemblyName_InternalGetPublicKeyToken_m5827 ();
extern "C" void AssemblyName_ComputePublicKeyToken_m5828 ();
extern "C" void AssemblyName_SetPublicKey_m5829 ();
extern "C" void AssemblyName_SetPublicKeyToken_m5830 ();
extern "C" void AssemblyName_GetObjectData_m5831 ();
extern "C" void AssemblyName_OnDeserialization_m5832 ();
extern "C" void AssemblyProductAttribute__ctor_m5833 ();
extern "C" void AssemblyTitleAttribute__ctor_m5834 ();
extern "C" void Default__ctor_m5835 ();
extern "C" void Default_BindToMethod_m5836 ();
extern "C" void Default_ReorderParameters_m5837 ();
extern "C" void Default_IsArrayAssignable_m5838 ();
extern "C" void Default_ChangeType_m5839 ();
extern "C" void Default_ReorderArgumentArray_m5840 ();
extern "C" void Default_check_type_m5841 ();
extern "C" void Default_check_arguments_m5842 ();
extern "C" void Default_SelectMethod_m5843 ();
extern "C" void Default_SelectMethod_m5844 ();
extern "C" void Default_GetBetterMethod_m5845 ();
extern "C" void Default_CompareCloserType_m5846 ();
extern "C" void Default_SelectProperty_m5847 ();
extern "C" void Default_check_arguments_with_score_m5848 ();
extern "C" void Default_check_type_with_score_m5849 ();
extern "C" void Binder__ctor_m5850 ();
extern "C" void Binder__cctor_m5851 ();
extern "C" void Binder_get_DefaultBinder_m5852 ();
extern "C" void Binder_ConvertArgs_m5853 ();
extern "C" void Binder_GetDerivedLevel_m5854 ();
extern "C" void Binder_FindMostDerivedMatch_m5855 ();
extern "C" void ConstructorInfo__ctor_m5856 ();
extern "C" void ConstructorInfo__cctor_m5857 ();
extern "C" void ConstructorInfo_get_MemberType_m5858 ();
extern "C" void ConstructorInfo_Invoke_m1305 ();
extern "C" void AddEventAdapter__ctor_m5859 ();
extern "C" void AddEventAdapter_Invoke_m5860 ();
extern "C" void AddEventAdapter_BeginInvoke_m5861 ();
extern "C" void AddEventAdapter_EndInvoke_m5862 ();
extern "C" void EventInfo__ctor_m5863 ();
extern "C" void EventInfo_get_EventHandlerType_m5864 ();
extern "C" void EventInfo_get_MemberType_m5865 ();
extern "C" void FieldInfo__ctor_m5866 ();
extern "C" void FieldInfo_get_MemberType_m5867 ();
extern "C" void FieldInfo_get_IsLiteral_m5868 ();
extern "C" void FieldInfo_get_IsStatic_m5869 ();
extern "C" void FieldInfo_get_IsInitOnly_m5870 ();
extern "C" void FieldInfo_get_IsPublic_m5871 ();
extern "C" void FieldInfo_get_IsNotSerialized_m5872 ();
extern "C" void FieldInfo_SetValue_m5873 ();
extern "C" void FieldInfo_internal_from_handle_type_m5874 ();
extern "C" void FieldInfo_GetFieldFromHandle_m5875 ();
extern "C" void FieldInfo_GetFieldOffset_m5876 ();
extern "C" void FieldInfo_GetUnmanagedMarshal_m5877 ();
extern "C" void FieldInfo_get_UMarshal_m5878 ();
extern "C" void FieldInfo_GetPseudoCustomAttributes_m5879 ();
extern "C" void MemberInfoSerializationHolder__ctor_m5880 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m5881 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m5882 ();
extern "C" void MemberInfoSerializationHolder_GetObjectData_m5883 ();
extern "C" void MemberInfoSerializationHolder_GetRealObject_m5884 ();
extern "C" void MethodBase__ctor_m5885 ();
extern "C" void MethodBase_GetMethodFromHandleNoGenericCheck_m5886 ();
extern "C" void MethodBase_GetMethodFromIntPtr_m5887 ();
extern "C" void MethodBase_GetMethodFromHandle_m5888 ();
extern "C" void MethodBase_GetMethodFromHandleInternalType_m5889 ();
extern "C" void MethodBase_GetParameterCount_m5890 ();
extern "C" void MethodBase_Invoke_m5891 ();
extern "C" void MethodBase_get_CallingConvention_m5892 ();
extern "C" void MethodBase_get_IsPublic_m5893 ();
extern "C" void MethodBase_get_IsStatic_m5894 ();
extern "C" void MethodBase_get_IsVirtual_m5895 ();
extern "C" void MethodBase_get_IsAbstract_m5896 ();
extern "C" void MethodBase_get_next_table_index_m5897 ();
extern "C" void MethodBase_GetGenericArguments_m5898 ();
extern "C" void MethodBase_get_ContainsGenericParameters_m5899 ();
extern "C" void MethodBase_get_IsGenericMethodDefinition_m5900 ();
extern "C" void MethodBase_get_IsGenericMethod_m5901 ();
extern "C" void MethodInfo__ctor_m5902 ();
extern "C" void MethodInfo_get_MemberType_m5903 ();
extern "C" void MethodInfo_get_ReturnType_m5904 ();
extern "C" void MethodInfo_MakeGenericMethod_m5905 ();
extern "C" void MethodInfo_GetGenericArguments_m5906 ();
extern "C" void MethodInfo_get_IsGenericMethod_m5907 ();
extern "C" void MethodInfo_get_IsGenericMethodDefinition_m5908 ();
extern "C" void MethodInfo_get_ContainsGenericParameters_m5909 ();
extern "C" void Missing__ctor_m5910 ();
extern "C" void Missing__cctor_m5911 ();
extern "C" void Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m5912 ();
extern "C" void Module__ctor_m5913 ();
extern "C" void Module__cctor_m5914 ();
extern "C" void Module_get_Assembly_m5915 ();
extern "C" void Module_get_ScopeName_m5916 ();
extern "C" void Module_GetCustomAttributes_m5917 ();
extern "C" void Module_GetObjectData_m5918 ();
extern "C" void Module_InternalGetTypes_m5919 ();
extern "C" void Module_GetTypes_m5920 ();
extern "C" void Module_IsDefined_m5921 ();
extern "C" void Module_IsResource_m5922 ();
extern "C" void Module_ToString_m5923 ();
extern "C" void Module_filter_by_type_name_m5924 ();
extern "C" void Module_filter_by_type_name_ignore_case_m5925 ();
extern "C" void MonoEventInfo_get_event_info_m5926 ();
extern "C" void MonoEventInfo_GetEventInfo_m5927 ();
extern "C" void MonoEvent__ctor_m5928 ();
extern "C" void MonoEvent_get_Attributes_m5929 ();
extern "C" void MonoEvent_GetAddMethod_m5930 ();
extern "C" void MonoEvent_get_DeclaringType_m5931 ();
extern "C" void MonoEvent_get_ReflectedType_m5932 ();
extern "C" void MonoEvent_get_Name_m5933 ();
extern "C" void MonoEvent_ToString_m5934 ();
extern "C" void MonoEvent_IsDefined_m5935 ();
extern "C" void MonoEvent_GetCustomAttributes_m5936 ();
extern "C" void MonoEvent_GetCustomAttributes_m5937 ();
extern "C" void MonoEvent_GetObjectData_m5938 ();
extern "C" void MonoField__ctor_m5939 ();
extern "C" void MonoField_get_Attributes_m5940 ();
extern "C" void MonoField_get_FieldHandle_m5941 ();
extern "C" void MonoField_get_FieldType_m5942 ();
extern "C" void MonoField_GetParentType_m5943 ();
extern "C" void MonoField_get_ReflectedType_m5944 ();
extern "C" void MonoField_get_DeclaringType_m5945 ();
extern "C" void MonoField_get_Name_m5946 ();
extern "C" void MonoField_IsDefined_m5947 ();
extern "C" void MonoField_GetCustomAttributes_m5948 ();
extern "C" void MonoField_GetCustomAttributes_m5949 ();
extern "C" void MonoField_GetFieldOffset_m5950 ();
extern "C" void MonoField_GetValueInternal_m5951 ();
extern "C" void MonoField_GetValue_m5952 ();
extern "C" void MonoField_ToString_m5953 ();
extern "C" void MonoField_SetValueInternal_m5954 ();
extern "C" void MonoField_SetValue_m5955 ();
extern "C" void MonoField_GetObjectData_m5956 ();
extern "C" void MonoField_CheckGeneric_m5957 ();
extern "C" void MonoGenericMethod__ctor_m5958 ();
extern "C" void MonoGenericMethod_get_ReflectedType_m5959 ();
extern "C" void MonoGenericCMethod__ctor_m5960 ();
extern "C" void MonoGenericCMethod_get_ReflectedType_m5961 ();
extern "C" void MonoMethodInfo_get_method_info_m5962 ();
extern "C" void MonoMethodInfo_GetMethodInfo_m5963 ();
extern "C" void MonoMethodInfo_GetDeclaringType_m5964 ();
extern "C" void MonoMethodInfo_GetReturnType_m5965 ();
extern "C" void MonoMethodInfo_GetAttributes_m5966 ();
extern "C" void MonoMethodInfo_GetCallingConvention_m5967 ();
extern "C" void MonoMethodInfo_get_parameter_info_m5968 ();
extern "C" void MonoMethodInfo_GetParametersInfo_m5969 ();
extern "C" void MonoMethod__ctor_m5970 ();
extern "C" void MonoMethod_get_name_m5971 ();
extern "C" void MonoMethod_get_base_definition_m5972 ();
extern "C" void MonoMethod_GetBaseDefinition_m5973 ();
extern "C" void MonoMethod_get_ReturnType_m5974 ();
extern "C" void MonoMethod_GetParameters_m5975 ();
extern "C" void MonoMethod_InternalInvoke_m5976 ();
extern "C" void MonoMethod_Invoke_m5977 ();
extern "C" void MonoMethod_get_MethodHandle_m5978 ();
extern "C" void MonoMethod_get_Attributes_m5979 ();
extern "C" void MonoMethod_get_CallingConvention_m5980 ();
extern "C" void MonoMethod_get_ReflectedType_m5981 ();
extern "C" void MonoMethod_get_DeclaringType_m5982 ();
extern "C" void MonoMethod_get_Name_m5983 ();
extern "C" void MonoMethod_IsDefined_m5984 ();
extern "C" void MonoMethod_GetCustomAttributes_m5985 ();
extern "C" void MonoMethod_GetCustomAttributes_m5986 ();
extern "C" void MonoMethod_GetDllImportAttribute_m5987 ();
extern "C" void MonoMethod_GetPseudoCustomAttributes_m5988 ();
extern "C" void MonoMethod_ShouldPrintFullName_m5989 ();
extern "C" void MonoMethod_ToString_m5990 ();
extern "C" void MonoMethod_GetObjectData_m5991 ();
extern "C" void MonoMethod_MakeGenericMethod_m5992 ();
extern "C" void MonoMethod_MakeGenericMethod_impl_m5993 ();
extern "C" void MonoMethod_GetGenericArguments_m5994 ();
extern "C" void MonoMethod_get_IsGenericMethodDefinition_m5995 ();
extern "C" void MonoMethod_get_IsGenericMethod_m5996 ();
extern "C" void MonoMethod_get_ContainsGenericParameters_m5997 ();
extern "C" void MonoCMethod__ctor_m5998 ();
extern "C" void MonoCMethod_GetParameters_m5999 ();
extern "C" void MonoCMethod_InternalInvoke_m6000 ();
extern "C" void MonoCMethod_Invoke_m6001 ();
extern "C" void MonoCMethod_Invoke_m6002 ();
extern "C" void MonoCMethod_get_MethodHandle_m6003 ();
extern "C" void MonoCMethod_get_Attributes_m6004 ();
extern "C" void MonoCMethod_get_CallingConvention_m6005 ();
extern "C" void MonoCMethod_get_ReflectedType_m6006 ();
extern "C" void MonoCMethod_get_DeclaringType_m6007 ();
extern "C" void MonoCMethod_get_Name_m6008 ();
extern "C" void MonoCMethod_IsDefined_m6009 ();
extern "C" void MonoCMethod_GetCustomAttributes_m6010 ();
extern "C" void MonoCMethod_GetCustomAttributes_m6011 ();
extern "C" void MonoCMethod_ToString_m6012 ();
extern "C" void MonoCMethod_GetObjectData_m6013 ();
extern "C" void MonoPropertyInfo_get_property_info_m6014 ();
extern "C" void MonoPropertyInfo_GetTypeModifiers_m6015 ();
extern "C" void GetterAdapter__ctor_m6016 ();
extern "C" void GetterAdapter_Invoke_m6017 ();
extern "C" void GetterAdapter_BeginInvoke_m6018 ();
extern "C" void GetterAdapter_EndInvoke_m6019 ();
extern "C" void MonoProperty__ctor_m6020 ();
extern "C" void MonoProperty_CachePropertyInfo_m6021 ();
extern "C" void MonoProperty_get_Attributes_m6022 ();
extern "C" void MonoProperty_get_CanRead_m6023 ();
extern "C" void MonoProperty_get_CanWrite_m6024 ();
extern "C" void MonoProperty_get_PropertyType_m6025 ();
extern "C" void MonoProperty_get_ReflectedType_m6026 ();
extern "C" void MonoProperty_get_DeclaringType_m6027 ();
extern "C" void MonoProperty_get_Name_m6028 ();
extern "C" void MonoProperty_GetAccessors_m6029 ();
extern "C" void MonoProperty_GetGetMethod_m6030 ();
extern "C" void MonoProperty_GetIndexParameters_m6031 ();
extern "C" void MonoProperty_GetSetMethod_m6032 ();
extern "C" void MonoProperty_IsDefined_m6033 ();
extern "C" void MonoProperty_GetCustomAttributes_m6034 ();
extern "C" void MonoProperty_GetCustomAttributes_m6035 ();
extern "C" void MonoProperty_CreateGetterDelegate_m6036 ();
extern "C" void MonoProperty_GetValue_m6037 ();
extern "C" void MonoProperty_GetValue_m6038 ();
extern "C" void MonoProperty_SetValue_m6039 ();
extern "C" void MonoProperty_ToString_m6040 ();
extern "C" void MonoProperty_GetOptionalCustomModifiers_m6041 ();
extern "C" void MonoProperty_GetRequiredCustomModifiers_m6042 ();
extern "C" void MonoProperty_GetObjectData_m6043 ();
extern "C" void ParameterInfo__ctor_m6044 ();
extern "C" void ParameterInfo__ctor_m6045 ();
extern "C" void ParameterInfo__ctor_m6046 ();
extern "C" void ParameterInfo_ToString_m6047 ();
extern "C" void ParameterInfo_get_ParameterType_m6048 ();
extern "C" void ParameterInfo_get_Attributes_m6049 ();
extern "C" void ParameterInfo_get_IsIn_m6050 ();
extern "C" void ParameterInfo_get_IsOptional_m6051 ();
extern "C" void ParameterInfo_get_IsOut_m6052 ();
extern "C" void ParameterInfo_get_IsRetval_m6053 ();
extern "C" void ParameterInfo_get_Member_m6054 ();
extern "C" void ParameterInfo_get_Name_m6055 ();
extern "C" void ParameterInfo_get_Position_m6056 ();
extern "C" void ParameterInfo_GetCustomAttributes_m6057 ();
extern "C" void ParameterInfo_IsDefined_m6058 ();
extern "C" void ParameterInfo_GetPseudoCustomAttributes_m6059 ();
extern "C" void Pointer__ctor_m6060 ();
extern "C" void Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m6061 ();
extern "C" void PropertyInfo__ctor_m6062 ();
extern "C" void PropertyInfo_get_MemberType_m6063 ();
extern "C" void PropertyInfo_GetValue_m6064 ();
extern "C" void PropertyInfo_SetValue_m6065 ();
extern "C" void PropertyInfo_GetOptionalCustomModifiers_m6066 ();
extern "C" void PropertyInfo_GetRequiredCustomModifiers_m6067 ();
extern "C" void StrongNameKeyPair__ctor_m6068 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m6069 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6070 ();
extern "C" void TargetException__ctor_m6071 ();
extern "C" void TargetException__ctor_m6072 ();
extern "C" void TargetException__ctor_m6073 ();
extern "C" void TargetInvocationException__ctor_m6074 ();
extern "C" void TargetInvocationException__ctor_m6075 ();
extern "C" void TargetParameterCountException__ctor_m6076 ();
extern "C" void TargetParameterCountException__ctor_m6077 ();
extern "C" void TargetParameterCountException__ctor_m6078 ();
extern "C" void NeutralResourcesLanguageAttribute__ctor_m6079 ();
extern "C" void SatelliteContractVersionAttribute__ctor_m6080 ();
extern "C" void CompilationRelaxationsAttribute__ctor_m6081 ();
extern "C" void DefaultDependencyAttribute__ctor_m6082 ();
extern "C" void StringFreezingAttribute__ctor_m6083 ();
extern "C" void CriticalFinalizerObject__ctor_m6084 ();
extern "C" void CriticalFinalizerObject_Finalize_m6085 ();
extern "C" void ReliabilityContractAttribute__ctor_m6086 ();
extern "C" void ClassInterfaceAttribute__ctor_m6087 ();
extern "C" void ComDefaultInterfaceAttribute__ctor_m6088 ();
extern "C" void DispIdAttribute__ctor_m6089 ();
extern "C" void GCHandle__ctor_m6090 ();
extern "C" void GCHandle_get_IsAllocated_m6091 ();
extern "C" void GCHandle_get_Target_m6092 ();
extern "C" void GCHandle_Alloc_m6093 ();
extern "C" void GCHandle_Free_m6094 ();
extern "C" void GCHandle_GetTarget_m6095 ();
extern "C" void GCHandle_GetTargetHandle_m6096 ();
extern "C" void GCHandle_FreeHandle_m6097 ();
extern "C" void GCHandle_Equals_m6098 ();
extern "C" void GCHandle_GetHashCode_m6099 ();
extern "C" void InterfaceTypeAttribute__ctor_m6100 ();
extern "C" void Marshal__cctor_m6101 ();
extern "C" void Marshal_copy_from_unmanaged_m6102 ();
extern "C" void Marshal_Copy_m6103 ();
extern "C" void Marshal_Copy_m6104 ();
extern "C" void MarshalDirectiveException__ctor_m6105 ();
extern "C" void MarshalDirectiveException__ctor_m6106 ();
extern "C" void PreserveSigAttribute__ctor_m6107 ();
extern "C" void SafeHandle__ctor_m6108 ();
extern "C" void SafeHandle_Close_m6109 ();
extern "C" void SafeHandle_DangerousAddRef_m6110 ();
extern "C" void SafeHandle_DangerousGetHandle_m6111 ();
extern "C" void SafeHandle_DangerousRelease_m6112 ();
extern "C" void SafeHandle_Dispose_m6113 ();
extern "C" void SafeHandle_Dispose_m6114 ();
extern "C" void SafeHandle_SetHandle_m6115 ();
extern "C" void SafeHandle_Finalize_m6116 ();
extern "C" void TypeLibImportClassAttribute__ctor_m6117 ();
extern "C" void TypeLibVersionAttribute__ctor_m6118 ();
extern "C" void ActivationServices_get_ConstructionActivator_m6119 ();
extern "C" void ActivationServices_CreateProxyFromAttributes_m6120 ();
extern "C" void ActivationServices_CreateConstructionCall_m6121 ();
extern "C" void ActivationServices_AllocateUninitializedClassInstance_m6122 ();
extern "C" void ActivationServices_EnableProxyActivation_m6123 ();
extern "C" void AppDomainLevelActivator__ctor_m6124 ();
extern "C" void ConstructionLevelActivator__ctor_m6125 ();
extern "C" void ContextLevelActivator__ctor_m6126 ();
extern "C" void UrlAttribute_get_UrlValue_m6127 ();
extern "C" void UrlAttribute_Equals_m6128 ();
extern "C" void UrlAttribute_GetHashCode_m6129 ();
extern "C" void UrlAttribute_GetPropertiesForNewContext_m6130 ();
extern "C" void UrlAttribute_IsContextOK_m6131 ();
extern "C" void ChannelInfo__ctor_m6132 ();
extern "C" void ChannelInfo_get_ChannelData_m6133 ();
extern "C" void ChannelServices__cctor_m6134 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m6135 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m6136 ();
extern "C" void ChannelServices_RegisterChannel_m6137 ();
extern "C" void ChannelServices_RegisterChannel_m6138 ();
extern "C" void ChannelServices_RegisterChannelConfig_m6139 ();
extern "C" void ChannelServices_CreateProvider_m6140 ();
extern "C" void ChannelServices_GetCurrentChannelInfo_m6141 ();
extern "C" void CrossAppDomainData__ctor_m6142 ();
extern "C" void CrossAppDomainData_get_DomainID_m6143 ();
extern "C" void CrossAppDomainData_get_ProcessID_m6144 ();
extern "C" void CrossAppDomainChannel__ctor_m6145 ();
extern "C" void CrossAppDomainChannel__cctor_m6146 ();
extern "C" void CrossAppDomainChannel_RegisterCrossAppDomainChannel_m6147 ();
extern "C" void CrossAppDomainChannel_get_ChannelName_m6148 ();
extern "C" void CrossAppDomainChannel_get_ChannelPriority_m6149 ();
extern "C" void CrossAppDomainChannel_get_ChannelData_m6150 ();
extern "C" void CrossAppDomainChannel_StartListening_m6151 ();
extern "C" void CrossAppDomainChannel_CreateMessageSink_m6152 ();
extern "C" void CrossAppDomainSink__ctor_m6153 ();
extern "C" void CrossAppDomainSink__cctor_m6154 ();
extern "C" void CrossAppDomainSink_GetSink_m6155 ();
extern "C" void CrossAppDomainSink_get_TargetDomainId_m6156 ();
extern "C" void SinkProviderData__ctor_m6157 ();
extern "C" void SinkProviderData_get_Children_m6158 ();
extern "C" void SinkProviderData_get_Properties_m6159 ();
extern "C" void Context__cctor_m6160 ();
extern "C" void Context_Finalize_m6161 ();
extern "C" void Context_get_DefaultContext_m6162 ();
extern "C" void Context_get_IsDefaultContext_m6163 ();
extern "C" void Context_GetProperty_m6164 ();
extern "C" void Context_ToString_m6165 ();
extern "C" void ContextAttribute__ctor_m6166 ();
extern "C" void ContextAttribute_get_Name_m6167 ();
extern "C" void ContextAttribute_Equals_m6168 ();
extern "C" void ContextAttribute_GetHashCode_m6169 ();
extern "C" void ContextAttribute_GetPropertiesForNewContext_m6170 ();
extern "C" void ContextAttribute_IsContextOK_m6171 ();
extern "C" void CrossContextChannel__ctor_m6172 ();
extern "C" void SynchronizationAttribute__ctor_m6173 ();
extern "C" void SynchronizationAttribute__ctor_m6174 ();
extern "C" void SynchronizationAttribute_set_Locked_m6175 ();
extern "C" void SynchronizationAttribute_ReleaseLock_m6176 ();
extern "C" void SynchronizationAttribute_GetPropertiesForNewContext_m6177 ();
extern "C" void SynchronizationAttribute_IsContextOK_m6178 ();
extern "C" void SynchronizationAttribute_ExitContext_m6179 ();
extern "C" void SynchronizationAttribute_EnterContext_m6180 ();
extern "C" void LeaseManager__ctor_m6181 ();
extern "C" void LeaseManager_SetPollTime_m6182 ();
extern "C" void LifetimeServices__cctor_m6183 ();
extern "C" void LifetimeServices_set_LeaseManagerPollTime_m6184 ();
extern "C" void LifetimeServices_set_LeaseTime_m6185 ();
extern "C" void LifetimeServices_set_RenewOnCallTime_m6186 ();
extern "C" void LifetimeServices_set_SponsorshipTimeout_m6187 ();
extern "C" void ArgInfo__ctor_m6188 ();
extern "C" void ArgInfo_GetInOutArgs_m6189 ();
extern "C" void AsyncResult__ctor_m6190 ();
extern "C" void AsyncResult_get_AsyncState_m6191 ();
extern "C" void AsyncResult_get_AsyncWaitHandle_m6192 ();
extern "C" void AsyncResult_get_CompletedSynchronously_m6193 ();
extern "C" void AsyncResult_get_IsCompleted_m6194 ();
extern "C" void AsyncResult_get_EndInvokeCalled_m6195 ();
extern "C" void AsyncResult_set_EndInvokeCalled_m6196 ();
extern "C" void AsyncResult_get_AsyncDelegate_m6197 ();
extern "C" void AsyncResult_get_NextSink_m6198 ();
extern "C" void AsyncResult_AsyncProcessMessage_m6199 ();
extern "C" void AsyncResult_GetReplyMessage_m6200 ();
extern "C" void AsyncResult_SetMessageCtrl_m6201 ();
extern "C" void AsyncResult_SetCompletedSynchronously_m6202 ();
extern "C" void AsyncResult_EndInvoke_m6203 ();
extern "C" void AsyncResult_SyncProcessMessage_m6204 ();
extern "C" void AsyncResult_get_CallMessage_m6205 ();
extern "C" void AsyncResult_set_CallMessage_m6206 ();
extern "C" void ConstructionCall__ctor_m6207 ();
extern "C" void ConstructionCall__ctor_m6208 ();
extern "C" void ConstructionCall_InitDictionary_m6209 ();
extern "C" void ConstructionCall_set_IsContextOk_m6210 ();
extern "C" void ConstructionCall_get_ActivationType_m6211 ();
extern "C" void ConstructionCall_get_ActivationTypeName_m6212 ();
extern "C" void ConstructionCall_get_Activator_m6213 ();
extern "C" void ConstructionCall_set_Activator_m6214 ();
extern "C" void ConstructionCall_get_CallSiteActivationAttributes_m6215 ();
extern "C" void ConstructionCall_SetActivationAttributes_m6216 ();
extern "C" void ConstructionCall_get_ContextProperties_m6217 ();
extern "C" void ConstructionCall_InitMethodProperty_m6218 ();
extern "C" void ConstructionCall_GetObjectData_m6219 ();
extern "C" void ConstructionCall_get_Properties_m6220 ();
extern "C" void ConstructionCallDictionary__ctor_m6221 ();
extern "C" void ConstructionCallDictionary__cctor_m6222 ();
extern "C" void ConstructionCallDictionary_GetMethodProperty_m6223 ();
extern "C" void ConstructionCallDictionary_SetMethodProperty_m6224 ();
extern "C" void EnvoyTerminatorSink__ctor_m6225 ();
extern "C" void EnvoyTerminatorSink__cctor_m6226 ();
extern "C" void Header__ctor_m6227 ();
extern "C" void Header__ctor_m6228 ();
extern "C" void Header__ctor_m6229 ();
extern "C" void LogicalCallContext__ctor_m6230 ();
extern "C" void LogicalCallContext__ctor_m6231 ();
extern "C" void LogicalCallContext_GetObjectData_m6232 ();
extern "C" void LogicalCallContext_SetData_m6233 ();
extern "C" void CallContextRemotingData__ctor_m6234 ();
extern "C" void MethodCall__ctor_m6235 ();
extern "C" void MethodCall__ctor_m6236 ();
extern "C" void MethodCall__ctor_m6237 ();
extern "C" void MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m6238 ();
extern "C" void MethodCall_InitMethodProperty_m6239 ();
extern "C" void MethodCall_GetObjectData_m6240 ();
extern "C" void MethodCall_get_Args_m6241 ();
extern "C" void MethodCall_get_LogicalCallContext_m6242 ();
extern "C" void MethodCall_get_MethodBase_m6243 ();
extern "C" void MethodCall_get_MethodName_m6244 ();
extern "C" void MethodCall_get_MethodSignature_m6245 ();
extern "C" void MethodCall_get_Properties_m6246 ();
extern "C" void MethodCall_InitDictionary_m6247 ();
extern "C" void MethodCall_get_TypeName_m6248 ();
extern "C" void MethodCall_get_Uri_m6249 ();
extern "C" void MethodCall_set_Uri_m6250 ();
extern "C" void MethodCall_Init_m6251 ();
extern "C" void MethodCall_ResolveMethod_m6252 ();
extern "C" void MethodCall_CastTo_m6253 ();
extern "C" void MethodCall_GetTypeNameFromAssemblyQualifiedName_m6254 ();
extern "C" void MethodCall_get_GenericArguments_m6255 ();
extern "C" void MethodCallDictionary__ctor_m6256 ();
extern "C" void MethodCallDictionary__cctor_m6257 ();
extern "C" void DictionaryEnumerator__ctor_m6258 ();
extern "C" void DictionaryEnumerator_get_Current_m6259 ();
extern "C" void DictionaryEnumerator_MoveNext_m6260 ();
extern "C" void DictionaryEnumerator_get_Entry_m6261 ();
extern "C" void DictionaryEnumerator_get_Key_m6262 ();
extern "C" void DictionaryEnumerator_get_Value_m6263 ();
extern "C" void MethodDictionary__ctor_m6264 ();
extern "C" void MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m6265 ();
extern "C" void MethodDictionary_set_MethodKeys_m6266 ();
extern "C" void MethodDictionary_AllocInternalProperties_m6267 ();
extern "C" void MethodDictionary_GetInternalProperties_m6268 ();
extern "C" void MethodDictionary_IsOverridenKey_m6269 ();
extern "C" void MethodDictionary_get_Item_m6270 ();
extern "C" void MethodDictionary_set_Item_m6271 ();
extern "C" void MethodDictionary_GetMethodProperty_m6272 ();
extern "C" void MethodDictionary_SetMethodProperty_m6273 ();
extern "C" void MethodDictionary_get_Values_m6274 ();
extern "C" void MethodDictionary_Add_m6275 ();
extern "C" void MethodDictionary_Contains_m6276 ();
extern "C" void MethodDictionary_Remove_m6277 ();
extern "C" void MethodDictionary_get_Count_m6278 ();
extern "C" void MethodDictionary_get_SyncRoot_m6279 ();
extern "C" void MethodDictionary_CopyTo_m6280 ();
extern "C" void MethodDictionary_GetEnumerator_m6281 ();
extern "C" void MethodReturnDictionary__ctor_m6282 ();
extern "C" void MethodReturnDictionary__cctor_m6283 ();
extern "C" void MonoMethodMessage_get_Args_m6284 ();
extern "C" void MonoMethodMessage_get_LogicalCallContext_m6285 ();
extern "C" void MonoMethodMessage_get_MethodBase_m6286 ();
extern "C" void MonoMethodMessage_get_MethodName_m6287 ();
extern "C" void MonoMethodMessage_get_MethodSignature_m6288 ();
extern "C" void MonoMethodMessage_get_TypeName_m6289 ();
extern "C" void MonoMethodMessage_get_Uri_m6290 ();
extern "C" void MonoMethodMessage_set_Uri_m6291 ();
extern "C" void MonoMethodMessage_get_Exception_m6292 ();
extern "C" void MonoMethodMessage_get_OutArgCount_m6293 ();
extern "C" void MonoMethodMessage_get_OutArgs_m6294 ();
extern "C" void MonoMethodMessage_get_ReturnValue_m6295 ();
extern "C" void RemotingSurrogate__ctor_m6296 ();
extern "C" void RemotingSurrogate_SetObjectData_m6297 ();
extern "C" void ObjRefSurrogate__ctor_m6298 ();
extern "C" void ObjRefSurrogate_SetObjectData_m6299 ();
extern "C" void RemotingSurrogateSelector__ctor_m6300 ();
extern "C" void RemotingSurrogateSelector__cctor_m6301 ();
extern "C" void RemotingSurrogateSelector_GetSurrogate_m6302 ();
extern "C" void ReturnMessage__ctor_m6303 ();
extern "C" void ReturnMessage__ctor_m6304 ();
extern "C" void ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m6305 ();
extern "C" void ReturnMessage_get_Args_m6306 ();
extern "C" void ReturnMessage_get_LogicalCallContext_m6307 ();
extern "C" void ReturnMessage_get_MethodBase_m6308 ();
extern "C" void ReturnMessage_get_MethodName_m6309 ();
extern "C" void ReturnMessage_get_MethodSignature_m6310 ();
extern "C" void ReturnMessage_get_Properties_m6311 ();
extern "C" void ReturnMessage_get_TypeName_m6312 ();
extern "C" void ReturnMessage_get_Uri_m6313 ();
extern "C" void ReturnMessage_set_Uri_m6314 ();
extern "C" void ReturnMessage_get_Exception_m6315 ();
extern "C" void ReturnMessage_get_OutArgs_m6316 ();
extern "C" void ReturnMessage_get_ReturnValue_m6317 ();
extern "C" void SoapAttribute__ctor_m6318 ();
extern "C" void SoapAttribute_get_UseAttribute_m6319 ();
extern "C" void SoapAttribute_get_XmlNamespace_m6320 ();
extern "C" void SoapAttribute_SetReflectionObject_m6321 ();
extern "C" void SoapFieldAttribute__ctor_m6322 ();
extern "C" void SoapFieldAttribute_get_XmlElementName_m6323 ();
extern "C" void SoapFieldAttribute_IsInteropXmlElement_m6324 ();
extern "C" void SoapFieldAttribute_SetReflectionObject_m6325 ();
extern "C" void SoapMethodAttribute__ctor_m6326 ();
extern "C" void SoapMethodAttribute_get_UseAttribute_m6327 ();
extern "C" void SoapMethodAttribute_get_XmlNamespace_m6328 ();
extern "C" void SoapMethodAttribute_SetReflectionObject_m6329 ();
extern "C" void SoapParameterAttribute__ctor_m6330 ();
extern "C" void SoapTypeAttribute__ctor_m6331 ();
extern "C" void SoapTypeAttribute_get_UseAttribute_m6332 ();
extern "C" void SoapTypeAttribute_get_XmlElementName_m6333 ();
extern "C" void SoapTypeAttribute_get_XmlNamespace_m6334 ();
extern "C" void SoapTypeAttribute_get_XmlTypeName_m6335 ();
extern "C" void SoapTypeAttribute_get_XmlTypeNamespace_m6336 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlElement_m6337 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlType_m6338 ();
extern "C" void SoapTypeAttribute_SetReflectionObject_m6339 ();
extern "C" void ProxyAttribute_CreateInstance_m6340 ();
extern "C" void ProxyAttribute_CreateProxy_m6341 ();
extern "C" void ProxyAttribute_GetPropertiesForNewContext_m6342 ();
extern "C" void ProxyAttribute_IsContextOK_m6343 ();
extern "C" void RealProxy__ctor_m6344 ();
extern "C" void RealProxy__ctor_m6345 ();
extern "C" void RealProxy__ctor_m6346 ();
extern "C" void RealProxy_InternalGetProxyType_m6347 ();
extern "C" void RealProxy_GetProxiedType_m6348 ();
extern "C" void RealProxy_InternalGetTransparentProxy_m6349 ();
extern "C" void RealProxy_GetTransparentProxy_m6350 ();
extern "C" void RealProxy_SetTargetDomain_m6351 ();
extern "C" void RemotingProxy__ctor_m6352 ();
extern "C" void RemotingProxy__ctor_m6353 ();
extern "C" void RemotingProxy__cctor_m6354 ();
extern "C" void RemotingProxy_get_TypeName_m6355 ();
extern "C" void RemotingProxy_Finalize_m6356 ();
extern "C" void TrackingServices__cctor_m6357 ();
extern "C" void TrackingServices_NotifyUnmarshaledObject_m6358 ();
extern "C" void ActivatedClientTypeEntry__ctor_m6359 ();
extern "C" void ActivatedClientTypeEntry_get_ApplicationUrl_m6360 ();
extern "C" void ActivatedClientTypeEntry_get_ContextAttributes_m6361 ();
extern "C" void ActivatedClientTypeEntry_get_ObjectType_m6362 ();
extern "C" void ActivatedClientTypeEntry_ToString_m6363 ();
extern "C" void ActivatedServiceTypeEntry__ctor_m6364 ();
extern "C" void ActivatedServiceTypeEntry_get_ObjectType_m6365 ();
extern "C" void ActivatedServiceTypeEntry_ToString_m6366 ();
extern "C" void EnvoyInfo__ctor_m6367 ();
extern "C" void EnvoyInfo_get_EnvoySinks_m6368 ();
extern "C" void Identity__ctor_m6369 ();
extern "C" void Identity_get_ChannelSink_m6370 ();
extern "C" void Identity_set_ChannelSink_m6371 ();
extern "C" void Identity_get_ObjectUri_m6372 ();
extern "C" void Identity_get_Disposed_m6373 ();
extern "C" void Identity_set_Disposed_m6374 ();
extern "C" void ClientIdentity__ctor_m6375 ();
extern "C" void ClientIdentity_get_ClientProxy_m6376 ();
extern "C" void ClientIdentity_set_ClientProxy_m6377 ();
extern "C" void ClientIdentity_CreateObjRef_m6378 ();
extern "C" void ClientIdentity_get_TargetUri_m6379 ();
extern "C" void InternalRemotingServices__cctor_m6380 ();
extern "C" void InternalRemotingServices_GetCachedSoapAttribute_m6381 ();
extern "C" void ObjRef__ctor_m6382 ();
extern "C" void ObjRef__ctor_m6383 ();
extern "C" void ObjRef__cctor_m6384 ();
extern "C" void ObjRef_get_IsReferenceToWellKnow_m6385 ();
extern "C" void ObjRef_get_ChannelInfo_m6386 ();
extern "C" void ObjRef_get_EnvoyInfo_m6387 ();
extern "C" void ObjRef_set_EnvoyInfo_m6388 ();
extern "C" void ObjRef_get_TypeInfo_m6389 ();
extern "C" void ObjRef_set_TypeInfo_m6390 ();
extern "C" void ObjRef_get_URI_m6391 ();
extern "C" void ObjRef_set_URI_m6392 ();
extern "C" void ObjRef_GetObjectData_m6393 ();
extern "C" void ObjRef_GetRealObject_m6394 ();
extern "C" void ObjRef_UpdateChannelInfo_m6395 ();
extern "C" void ObjRef_get_ServerType_m6396 ();
extern "C" void RemotingConfiguration__cctor_m6397 ();
extern "C" void RemotingConfiguration_get_ApplicationName_m6398 ();
extern "C" void RemotingConfiguration_set_ApplicationName_m6399 ();
extern "C" void RemotingConfiguration_get_ProcessId_m6400 ();
extern "C" void RemotingConfiguration_LoadDefaultDelayedChannels_m6401 ();
extern "C" void RemotingConfiguration_IsRemotelyActivatedClientType_m6402 ();
extern "C" void RemotingConfiguration_RegisterActivatedClientType_m6403 ();
extern "C" void RemotingConfiguration_RegisterActivatedServiceType_m6404 ();
extern "C" void RemotingConfiguration_RegisterWellKnownClientType_m6405 ();
extern "C" void RemotingConfiguration_RegisterWellKnownServiceType_m6406 ();
extern "C" void RemotingConfiguration_RegisterChannelTemplate_m6407 ();
extern "C" void RemotingConfiguration_RegisterClientProviderTemplate_m6408 ();
extern "C" void RemotingConfiguration_RegisterServerProviderTemplate_m6409 ();
extern "C" void RemotingConfiguration_RegisterChannels_m6410 ();
extern "C" void RemotingConfiguration_RegisterTypes_m6411 ();
extern "C" void RemotingConfiguration_SetCustomErrorsMode_m6412 ();
extern "C" void ConfigHandler__ctor_m6413 ();
extern "C" void ConfigHandler_ValidatePath_m6414 ();
extern "C" void ConfigHandler_CheckPath_m6415 ();
extern "C" void ConfigHandler_OnStartParsing_m6416 ();
extern "C" void ConfigHandler_OnProcessingInstruction_m6417 ();
extern "C" void ConfigHandler_OnIgnorableWhitespace_m6418 ();
extern "C" void ConfigHandler_OnStartElement_m6419 ();
extern "C" void ConfigHandler_ParseElement_m6420 ();
extern "C" void ConfigHandler_OnEndElement_m6421 ();
extern "C" void ConfigHandler_ReadCustomProviderData_m6422 ();
extern "C" void ConfigHandler_ReadLifetine_m6423 ();
extern "C" void ConfigHandler_ParseTime_m6424 ();
extern "C" void ConfigHandler_ReadChannel_m6425 ();
extern "C" void ConfigHandler_ReadProvider_m6426 ();
extern "C" void ConfigHandler_ReadClientActivated_m6427 ();
extern "C" void ConfigHandler_ReadServiceActivated_m6428 ();
extern "C" void ConfigHandler_ReadClientWellKnown_m6429 ();
extern "C" void ConfigHandler_ReadServiceWellKnown_m6430 ();
extern "C" void ConfigHandler_ReadInteropXml_m6431 ();
extern "C" void ConfigHandler_ReadPreload_m6432 ();
extern "C" void ConfigHandler_GetNotNull_m6433 ();
extern "C" void ConfigHandler_ExtractAssembly_m6434 ();
extern "C" void ConfigHandler_OnChars_m6435 ();
extern "C" void ConfigHandler_OnEndParsing_m6436 ();
extern "C" void ChannelData__ctor_m6437 ();
extern "C" void ChannelData_get_ServerProviders_m6438 ();
extern "C" void ChannelData_get_ClientProviders_m6439 ();
extern "C" void ChannelData_get_CustomProperties_m6440 ();
extern "C" void ChannelData_CopyFrom_m6441 ();
extern "C" void ProviderData__ctor_m6442 ();
extern "C" void ProviderData_CopyFrom_m6443 ();
extern "C" void FormatterData__ctor_m6444 ();
extern "C" void RemotingException__ctor_m6445 ();
extern "C" void RemotingException__ctor_m6446 ();
extern "C" void RemotingException__ctor_m6447 ();
extern "C" void RemotingException__ctor_m6448 ();
extern "C" void RemotingServices__cctor_m6449 ();
extern "C" void RemotingServices_GetVirtualMethod_m6450 ();
extern "C" void RemotingServices_IsTransparentProxy_m6451 ();
extern "C" void RemotingServices_GetServerTypeForUri_m6452 ();
extern "C" void RemotingServices_Unmarshal_m6453 ();
extern "C" void RemotingServices_Unmarshal_m6454 ();
extern "C" void RemotingServices_GetRealProxy_m6455 ();
extern "C" void RemotingServices_GetMethodBaseFromMethodMessage_m6456 ();
extern "C" void RemotingServices_GetMethodBaseFromName_m6457 ();
extern "C" void RemotingServices_FindInterfaceMethod_m6458 ();
extern "C" void RemotingServices_CreateClientProxy_m6459 ();
extern "C" void RemotingServices_CreateClientProxy_m6460 ();
extern "C" void RemotingServices_CreateClientProxyForContextBound_m6461 ();
extern "C" void RemotingServices_GetIdentityForUri_m6462 ();
extern "C" void RemotingServices_RemoveAppNameFromUri_m6463 ();
extern "C" void RemotingServices_GetOrCreateClientIdentity_m6464 ();
extern "C" void RemotingServices_GetClientChannelSinkChain_m6465 ();
extern "C" void RemotingServices_CreateWellKnownServerIdentity_m6466 ();
extern "C" void RemotingServices_RegisterServerIdentity_m6467 ();
extern "C" void RemotingServices_GetProxyForRemoteObject_m6468 ();
extern "C" void RemotingServices_GetRemoteObject_m6469 ();
extern "C" void RemotingServices_RegisterInternalChannels_m6470 ();
extern "C" void RemotingServices_DisposeIdentity_m6471 ();
extern "C" void RemotingServices_GetNormalizedUri_m6472 ();
extern "C" void ServerIdentity__ctor_m6473 ();
extern "C" void ServerIdentity_get_ObjectType_m6474 ();
extern "C" void ServerIdentity_CreateObjRef_m6475 ();
extern "C" void ClientActivatedIdentity_GetServerObject_m6476 ();
extern "C" void SingletonIdentity__ctor_m6477 ();
extern "C" void SingleCallIdentity__ctor_m6478 ();
extern "C" void TypeInfo__ctor_m6479 ();
extern "C" void SoapServices__cctor_m6480 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithAssembly_m6481 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNs_m6482 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m6483 ();
extern "C" void SoapServices_CodeXmlNamespaceForClrTypeNamespace_m6484 ();
extern "C" void SoapServices_GetNameKey_m6485 ();
extern "C" void SoapServices_GetAssemblyName_m6486 ();
extern "C" void SoapServices_GetXmlElementForInteropType_m6487 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodCall_m6488 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodResponse_m6489 ();
extern "C" void SoapServices_GetXmlTypeForInteropType_m6490 ();
extern "C" void SoapServices_PreLoad_m6491 ();
extern "C" void SoapServices_PreLoad_m6492 ();
extern "C" void SoapServices_RegisterInteropXmlElement_m6493 ();
extern "C" void SoapServices_RegisterInteropXmlType_m6494 ();
extern "C" void SoapServices_EncodeNs_m6495 ();
extern "C" void TypeEntry__ctor_m6496 ();
extern "C" void TypeEntry_get_AssemblyName_m6497 ();
extern "C" void TypeEntry_set_AssemblyName_m6498 ();
extern "C" void TypeEntry_get_TypeName_m6499 ();
extern "C" void TypeEntry_set_TypeName_m6500 ();
extern "C" void TypeInfo__ctor_m6501 ();
extern "C" void TypeInfo_get_TypeName_m6502 ();
extern "C" void WellKnownClientTypeEntry__ctor_m6503 ();
extern "C" void WellKnownClientTypeEntry_get_ApplicationUrl_m6504 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectType_m6505 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectUrl_m6506 ();
extern "C" void WellKnownClientTypeEntry_ToString_m6507 ();
extern "C" void WellKnownServiceTypeEntry__ctor_m6508 ();
extern "C" void WellKnownServiceTypeEntry_get_Mode_m6509 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectType_m6510 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectUri_m6511 ();
extern "C" void WellKnownServiceTypeEntry_ToString_m6512 ();
extern "C" void BinaryCommon__cctor_m6513 ();
extern "C" void BinaryCommon_IsPrimitive_m6514 ();
extern "C" void BinaryCommon_GetTypeFromCode_m6515 ();
extern "C" void BinaryCommon_SwapBytes_m6516 ();
extern "C" void BinaryFormatter__ctor_m6517 ();
extern "C" void BinaryFormatter__ctor_m6518 ();
extern "C" void BinaryFormatter_get_DefaultSurrogateSelector_m6519 ();
extern "C" void BinaryFormatter_set_AssemblyFormat_m6520 ();
extern "C" void BinaryFormatter_get_Binder_m6521 ();
extern "C" void BinaryFormatter_get_Context_m6522 ();
extern "C" void BinaryFormatter_get_SurrogateSelector_m6523 ();
extern "C" void BinaryFormatter_get_FilterLevel_m6524 ();
extern "C" void BinaryFormatter_Deserialize_m6525 ();
extern "C" void BinaryFormatter_NoCheckDeserialize_m6526 ();
extern "C" void BinaryFormatter_ReadBinaryHeader_m6527 ();
extern "C" void MessageFormatter_ReadMethodCall_m6528 ();
extern "C" void MessageFormatter_ReadMethodResponse_m6529 ();
extern "C" void TypeMetadata__ctor_m6530 ();
extern "C" void ArrayNullFiller__ctor_m6531 ();
extern "C" void ObjectReader__ctor_m6532 ();
extern "C" void ObjectReader_ReadObjectGraph_m6533 ();
extern "C" void ObjectReader_ReadObjectGraph_m6534 ();
extern "C" void ObjectReader_ReadNextObject_m6535 ();
extern "C" void ObjectReader_ReadNextObject_m6536 ();
extern "C" void ObjectReader_get_CurrentObject_m6537 ();
extern "C" void ObjectReader_ReadObject_m6538 ();
extern "C" void ObjectReader_ReadAssembly_m6539 ();
extern "C" void ObjectReader_ReadObjectInstance_m6540 ();
extern "C" void ObjectReader_ReadRefTypeObjectInstance_m6541 ();
extern "C" void ObjectReader_ReadObjectContent_m6542 ();
extern "C" void ObjectReader_RegisterObject_m6543 ();
extern "C" void ObjectReader_ReadStringIntance_m6544 ();
extern "C" void ObjectReader_ReadGenericArray_m6545 ();
extern "C" void ObjectReader_ReadBoxedPrimitiveTypeValue_m6546 ();
extern "C" void ObjectReader_ReadArrayOfPrimitiveType_m6547 ();
extern "C" void ObjectReader_BlockRead_m6548 ();
extern "C" void ObjectReader_ReadArrayOfObject_m6549 ();
extern "C" void ObjectReader_ReadArrayOfString_m6550 ();
extern "C" void ObjectReader_ReadSimpleArray_m6551 ();
extern "C" void ObjectReader_ReadTypeMetadata_m6552 ();
extern "C" void ObjectReader_ReadValue_m6553 ();
extern "C" void ObjectReader_SetObjectValue_m6554 ();
extern "C" void ObjectReader_RecordFixup_m6555 ();
extern "C" void ObjectReader_GetDeserializationType_m6556 ();
extern "C" void ObjectReader_ReadType_m6557 ();
extern "C" void ObjectReader_ReadPrimitiveTypeValue_m6558 ();
extern "C" void FormatterConverter__ctor_m6559 ();
extern "C" void FormatterConverter_Convert_m6560 ();
extern "C" void FormatterConverter_ToBoolean_m6561 ();
extern "C" void FormatterConverter_ToInt16_m6562 ();
extern "C" void FormatterConverter_ToInt32_m6563 ();
extern "C" void FormatterConverter_ToInt64_m6564 ();
extern "C" void FormatterConverter_ToString_m6565 ();
extern "C" void FormatterServices_GetUninitializedObject_m6566 ();
extern "C" void FormatterServices_GetSafeUninitializedObject_m6567 ();
extern "C" void ObjectManager__ctor_m6568 ();
extern "C" void ObjectManager_DoFixups_m6569 ();
extern "C" void ObjectManager_GetObjectRecord_m6570 ();
extern "C" void ObjectManager_GetObject_m6571 ();
extern "C" void ObjectManager_RaiseDeserializationEvent_m6572 ();
extern "C" void ObjectManager_RaiseOnDeserializingEvent_m6573 ();
extern "C" void ObjectManager_RaiseOnDeserializedEvent_m6574 ();
extern "C" void ObjectManager_AddFixup_m6575 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m6576 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m6577 ();
extern "C" void ObjectManager_RecordDelayedFixup_m6578 ();
extern "C" void ObjectManager_RecordFixup_m6579 ();
extern "C" void ObjectManager_RegisterObjectInternal_m6580 ();
extern "C" void ObjectManager_RegisterObject_m6581 ();
extern "C" void BaseFixupRecord__ctor_m6582 ();
extern "C" void BaseFixupRecord_DoFixup_m6583 ();
extern "C" void ArrayFixupRecord__ctor_m6584 ();
extern "C" void ArrayFixupRecord_FixupImpl_m6585 ();
extern "C" void MultiArrayFixupRecord__ctor_m6586 ();
extern "C" void MultiArrayFixupRecord_FixupImpl_m6587 ();
extern "C" void FixupRecord__ctor_m6588 ();
extern "C" void FixupRecord_FixupImpl_m6589 ();
extern "C" void DelayedFixupRecord__ctor_m6590 ();
extern "C" void DelayedFixupRecord_FixupImpl_m6591 ();
extern "C" void ObjectRecord__ctor_m6592 ();
extern "C" void ObjectRecord_SetMemberValue_m6593 ();
extern "C" void ObjectRecord_SetArrayValue_m6594 ();
extern "C" void ObjectRecord_SetMemberValue_m6595 ();
extern "C" void ObjectRecord_get_IsInstanceReady_m6596 ();
extern "C" void ObjectRecord_get_IsUnsolvedObjectReference_m6597 ();
extern "C" void ObjectRecord_get_IsRegistered_m6598 ();
extern "C" void ObjectRecord_DoFixups_m6599 ();
extern "C" void ObjectRecord_RemoveFixup_m6600 ();
extern "C" void ObjectRecord_UnchainFixup_m6601 ();
extern "C" void ObjectRecord_ChainFixup_m6602 ();
extern "C" void ObjectRecord_LoadData_m6603 ();
extern "C" void ObjectRecord_get_HasPendingFixups_m6604 ();
extern "C" void SerializationBinder__ctor_m6605 ();
extern "C" void CallbackHandler__ctor_m6606 ();
extern "C" void CallbackHandler_Invoke_m6607 ();
extern "C" void CallbackHandler_BeginInvoke_m6608 ();
extern "C" void CallbackHandler_EndInvoke_m6609 ();
extern "C" void SerializationCallbacks__ctor_m6610 ();
extern "C" void SerializationCallbacks__cctor_m6611 ();
extern "C" void SerializationCallbacks_get_HasDeserializedCallbacks_m6612 ();
extern "C" void SerializationCallbacks_GetMethodsByAttribute_m6613 ();
extern "C" void SerializationCallbacks_Invoke_m6614 ();
extern "C" void SerializationCallbacks_RaiseOnDeserializing_m6615 ();
extern "C" void SerializationCallbacks_RaiseOnDeserialized_m6616 ();
extern "C" void SerializationCallbacks_GetSerializationCallbacks_m6617 ();
extern "C" void SerializationEntry__ctor_m6618 ();
extern "C" void SerializationEntry_get_Name_m6619 ();
extern "C" void SerializationEntry_get_Value_m6620 ();
extern "C" void SerializationException__ctor_m6621 ();
extern "C" void SerializationException__ctor_m2247 ();
extern "C" void SerializationException__ctor_m6622 ();
extern "C" void SerializationInfo__ctor_m6623 ();
extern "C" void SerializationInfo_AddValue_m2243 ();
extern "C" void SerializationInfo_GetValue_m2246 ();
extern "C" void SerializationInfo_SetType_m6624 ();
extern "C" void SerializationInfo_GetEnumerator_m6625 ();
extern "C" void SerializationInfo_AddValue_m6626 ();
extern "C" void SerializationInfo_AddValue_m2245 ();
extern "C" void SerializationInfo_AddValue_m2244 ();
extern "C" void SerializationInfo_AddValue_m6627 ();
extern "C" void SerializationInfo_AddValue_m6628 ();
extern "C" void SerializationInfo_AddValue_m2257 ();
extern "C" void SerializationInfo_AddValue_m6629 ();
extern "C" void SerializationInfo_AddValue_m2256 ();
extern "C" void SerializationInfo_GetBoolean_m2248 ();
extern "C" void SerializationInfo_GetInt16_m6630 ();
extern "C" void SerializationInfo_GetInt32_m2255 ();
extern "C" void SerializationInfo_GetInt64_m2254 ();
extern "C" void SerializationInfo_GetString_m2253 ();
extern "C" void SerializationInfoEnumerator__ctor_m6631 ();
extern "C" void SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m6632 ();
extern "C" void SerializationInfoEnumerator_get_Current_m6633 ();
extern "C" void SerializationInfoEnumerator_get_Name_m6634 ();
extern "C" void SerializationInfoEnumerator_get_Value_m6635 ();
extern "C" void SerializationInfoEnumerator_MoveNext_m6636 ();
extern "C" void StreamingContext__ctor_m6637 ();
extern "C" void StreamingContext__ctor_m6638 ();
extern "C" void StreamingContext_get_State_m6639 ();
extern "C" void StreamingContext_Equals_m6640 ();
extern "C" void StreamingContext_GetHashCode_m6641 ();
extern "C" void X509Certificate__ctor_m6642 ();
extern "C" void X509Certificate__ctor_m3354 ();
extern "C" void X509Certificate__ctor_m2312 ();
extern "C" void X509Certificate__ctor_m6643 ();
extern "C" void X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6644 ();
extern "C" void X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m6645 ();
extern "C" void X509Certificate_tostr_m6646 ();
extern "C" void X509Certificate_Equals_m6647 ();
extern "C" void X509Certificate_GetCertHash_m6648 ();
extern "C" void X509Certificate_GetCertHashString_m2317 ();
extern "C" void X509Certificate_GetEffectiveDateString_m6649 ();
extern "C" void X509Certificate_GetExpirationDateString_m6650 ();
extern "C" void X509Certificate_GetHashCode_m6651 ();
extern "C" void X509Certificate_GetIssuerName_m6652 ();
extern "C" void X509Certificate_GetName_m6653 ();
extern "C" void X509Certificate_GetPublicKey_m6654 ();
extern "C" void X509Certificate_GetRawCertData_m6655 ();
extern "C" void X509Certificate_ToString_m6656 ();
extern "C" void X509Certificate_ToString_m2330 ();
extern "C" void X509Certificate_get_Issuer_m2333 ();
extern "C" void X509Certificate_get_Subject_m2332 ();
extern "C" void X509Certificate_Equals_m6657 ();
extern "C" void X509Certificate_Import_m2327 ();
extern "C" void X509Certificate_Reset_m2329 ();
extern "C" void AsymmetricAlgorithm__ctor_m6658 ();
extern "C" void AsymmetricAlgorithm_System_IDisposable_Dispose_m6659 ();
extern "C" void AsymmetricAlgorithm_get_KeySize_m3304 ();
extern "C" void AsymmetricAlgorithm_set_KeySize_m3303 ();
extern "C" void AsymmetricAlgorithm_Clear_m3359 ();
extern "C" void AsymmetricAlgorithm_GetNamedParam_m6660 ();
extern "C" void AsymmetricKeyExchangeFormatter__ctor_m6661 ();
extern "C" void AsymmetricSignatureDeformatter__ctor_m3349 ();
extern "C" void AsymmetricSignatureFormatter__ctor_m3350 ();
extern "C" void Base64Constants__cctor_m6662 ();
extern "C" void CryptoConfig__cctor_m6663 ();
extern "C" void CryptoConfig_Initialize_m6664 ();
extern "C" void CryptoConfig_CreateFromName_m2335 ();
extern "C" void CryptoConfig_CreateFromName_m2361 ();
extern "C" void CryptoConfig_MapNameToOID_m3297 ();
extern "C" void CryptoConfig_EncodeOID_m2337 ();
extern "C" void CryptoConfig_EncodeLongNumber_m6665 ();
extern "C" void CryptographicException__ctor_m6666 ();
extern "C" void CryptographicException__ctor_m2291 ();
extern "C" void CryptographicException__ctor_m2295 ();
extern "C" void CryptographicException__ctor_m2460 ();
extern "C" void CryptographicException__ctor_m6667 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m6668 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m3332 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m6669 ();
extern "C" void CspParameters__ctor_m3299 ();
extern "C" void CspParameters__ctor_m6670 ();
extern "C" void CspParameters__ctor_m6671 ();
extern "C" void CspParameters__ctor_m6672 ();
extern "C" void CspParameters_get_Flags_m6673 ();
extern "C" void CspParameters_set_Flags_m3300 ();
extern "C" void DES__ctor_m6674 ();
extern "C" void DES__cctor_m6675 ();
extern "C" void DES_Create_m3333 ();
extern "C" void DES_Create_m6676 ();
extern "C" void DES_IsWeakKey_m6677 ();
extern "C" void DES_IsSemiWeakKey_m6678 ();
extern "C" void DES_get_Key_m6679 ();
extern "C" void DES_set_Key_m6680 ();
extern "C" void DESTransform__ctor_m6681 ();
extern "C" void DESTransform__cctor_m6682 ();
extern "C" void DESTransform_CipherFunct_m6683 ();
extern "C" void DESTransform_Permutation_m6684 ();
extern "C" void DESTransform_BSwap_m6685 ();
extern "C" void DESTransform_SetKey_m6686 ();
extern "C" void DESTransform_ProcessBlock_m6687 ();
extern "C" void DESTransform_ECB_m6688 ();
extern "C" void DESTransform_GetStrongKey_m6689 ();
extern "C" void DESCryptoServiceProvider__ctor_m6690 ();
extern "C" void DESCryptoServiceProvider_CreateDecryptor_m6691 ();
extern "C" void DESCryptoServiceProvider_CreateEncryptor_m6692 ();
extern "C" void DESCryptoServiceProvider_GenerateIV_m6693 ();
extern "C" void DESCryptoServiceProvider_GenerateKey_m6694 ();
extern "C" void DSA__ctor_m6695 ();
extern "C" void DSA_Create_m2286 ();
extern "C" void DSA_Create_m6696 ();
extern "C" void DSA_ZeroizePrivateKey_m6697 ();
extern "C" void DSA_FromXmlString_m6698 ();
extern "C" void DSA_ToXmlString_m6699 ();
extern "C" void DSACryptoServiceProvider__ctor_m6700 ();
extern "C" void DSACryptoServiceProvider__ctor_m2296 ();
extern "C" void DSACryptoServiceProvider__ctor_m6701 ();
extern "C" void DSACryptoServiceProvider__cctor_m6702 ();
extern "C" void DSACryptoServiceProvider_Finalize_m6703 ();
extern "C" void DSACryptoServiceProvider_get_KeySize_m6704 ();
extern "C" void DSACryptoServiceProvider_get_PublicOnly_m2285 ();
extern "C" void DSACryptoServiceProvider_ExportParameters_m6705 ();
extern "C" void DSACryptoServiceProvider_ImportParameters_m6706 ();
extern "C" void DSACryptoServiceProvider_CreateSignature_m6707 ();
extern "C" void DSACryptoServiceProvider_VerifySignature_m6708 ();
extern "C" void DSACryptoServiceProvider_Dispose_m6709 ();
extern "C" void DSACryptoServiceProvider_OnKeyGenerated_m6710 ();
extern "C" void DSASignatureDeformatter__ctor_m6711 ();
extern "C" void DSASignatureDeformatter__ctor_m3318 ();
extern "C" void DSASignatureDeformatter_SetHashAlgorithm_m6712 ();
extern "C" void DSASignatureDeformatter_SetKey_m6713 ();
extern "C" void DSASignatureDeformatter_VerifySignature_m6714 ();
extern "C" void DSASignatureFormatter__ctor_m6715 ();
extern "C" void DSASignatureFormatter_CreateSignature_m6716 ();
extern "C" void DSASignatureFormatter_SetHashAlgorithm_m6717 ();
extern "C" void DSASignatureFormatter_SetKey_m6718 ();
extern "C" void HMAC__ctor_m6719 ();
extern "C" void HMAC_get_BlockSizeValue_m6720 ();
extern "C" void HMAC_set_BlockSizeValue_m6721 ();
extern "C" void HMAC_set_HashName_m6722 ();
extern "C" void HMAC_get_Key_m6723 ();
extern "C" void HMAC_set_Key_m6724 ();
extern "C" void HMAC_get_Block_m6725 ();
extern "C" void HMAC_KeySetup_m6726 ();
extern "C" void HMAC_Dispose_m6727 ();
extern "C" void HMAC_HashCore_m6728 ();
extern "C" void HMAC_HashFinal_m6729 ();
extern "C" void HMAC_Initialize_m6730 ();
extern "C" void HMAC_Create_m3311 ();
extern "C" void HMAC_Create_m6731 ();
extern "C" void HMACMD5__ctor_m6732 ();
extern "C" void HMACMD5__ctor_m6733 ();
extern "C" void HMACRIPEMD160__ctor_m6734 ();
extern "C" void HMACRIPEMD160__ctor_m6735 ();
extern "C" void HMACSHA1__ctor_m6736 ();
extern "C" void HMACSHA1__ctor_m6737 ();
extern "C" void HMACSHA256__ctor_m6738 ();
extern "C" void HMACSHA256__ctor_m6739 ();
extern "C" void HMACSHA384__ctor_m6740 ();
extern "C" void HMACSHA384__ctor_m6741 ();
extern "C" void HMACSHA384__cctor_m6742 ();
extern "C" void HMACSHA384_set_ProduceLegacyHmacValues_m6743 ();
extern "C" void HMACSHA512__ctor_m6744 ();
extern "C" void HMACSHA512__ctor_m6745 ();
extern "C" void HMACSHA512__cctor_m6746 ();
extern "C" void HMACSHA512_set_ProduceLegacyHmacValues_m6747 ();
extern "C" void HashAlgorithm__ctor_m3296 ();
extern "C" void HashAlgorithm_System_IDisposable_Dispose_m6748 ();
extern "C" void HashAlgorithm_get_CanReuseTransform_m6749 ();
extern "C" void HashAlgorithm_ComputeHash_m2371 ();
extern "C" void HashAlgorithm_ComputeHash_m3306 ();
extern "C" void HashAlgorithm_Create_m3305 ();
extern "C" void HashAlgorithm_get_Hash_m6750 ();
extern "C" void HashAlgorithm_get_HashSize_m6751 ();
extern "C" void HashAlgorithm_Dispose_m6752 ();
extern "C" void HashAlgorithm_TransformBlock_m6753 ();
extern "C" void HashAlgorithm_TransformFinalBlock_m6754 ();
extern "C" void KeySizes__ctor_m2462 ();
extern "C" void KeySizes_get_MaxSize_m6755 ();
extern "C" void KeySizes_get_MinSize_m6756 ();
extern "C" void KeySizes_get_SkipSize_m6757 ();
extern "C" void KeySizes_IsLegal_m6758 ();
extern "C" void KeySizes_IsLegalKeySize_m6759 ();
extern "C" void KeyedHashAlgorithm__ctor_m3331 ();
extern "C" void KeyedHashAlgorithm_Finalize_m6760 ();
extern "C" void KeyedHashAlgorithm_get_Key_m6761 ();
extern "C" void KeyedHashAlgorithm_set_Key_m6762 ();
extern "C" void KeyedHashAlgorithm_Dispose_m6763 ();
extern "C" void KeyedHashAlgorithm_ZeroizeKey_m6764 ();
extern "C" void MACTripleDES__ctor_m6765 ();
extern "C" void MACTripleDES_Setup_m6766 ();
extern "C" void MACTripleDES_Finalize_m6767 ();
extern "C" void MACTripleDES_Dispose_m6768 ();
extern "C" void MACTripleDES_Initialize_m6769 ();
extern "C" void MACTripleDES_HashCore_m6770 ();
extern "C" void MACTripleDES_HashFinal_m6771 ();
extern "C" void MD5__ctor_m6772 ();
extern "C" void MD5_Create_m3315 ();
extern "C" void MD5_Create_m6773 ();
extern "C" void MD5CryptoServiceProvider__ctor_m6774 ();
extern "C" void MD5CryptoServiceProvider__cctor_m6775 ();
extern "C" void MD5CryptoServiceProvider_Finalize_m6776 ();
extern "C" void MD5CryptoServiceProvider_Dispose_m6777 ();
extern "C" void MD5CryptoServiceProvider_HashCore_m6778 ();
extern "C" void MD5CryptoServiceProvider_HashFinal_m6779 ();
extern "C" void MD5CryptoServiceProvider_Initialize_m6780 ();
extern "C" void MD5CryptoServiceProvider_ProcessBlock_m6781 ();
extern "C" void MD5CryptoServiceProvider_ProcessFinalBlock_m6782 ();
extern "C" void MD5CryptoServiceProvider_AddLength_m6783 ();
extern "C" void RC2__ctor_m6784 ();
extern "C" void RC2_Create_m3334 ();
extern "C" void RC2_Create_m6785 ();
extern "C" void RC2_get_EffectiveKeySize_m6786 ();
extern "C" void RC2_get_KeySize_m6787 ();
extern "C" void RC2_set_KeySize_m6788 ();
extern "C" void RC2CryptoServiceProvider__ctor_m6789 ();
extern "C" void RC2CryptoServiceProvider_get_EffectiveKeySize_m6790 ();
extern "C" void RC2CryptoServiceProvider_CreateDecryptor_m6791 ();
extern "C" void RC2CryptoServiceProvider_CreateEncryptor_m6792 ();
extern "C" void RC2CryptoServiceProvider_GenerateIV_m6793 ();
extern "C" void RC2CryptoServiceProvider_GenerateKey_m6794 ();
extern "C" void RC2Transform__ctor_m6795 ();
extern "C" void RC2Transform__cctor_m6796 ();
extern "C" void RC2Transform_ECB_m6797 ();
extern "C" void RIPEMD160__ctor_m6798 ();
extern "C" void RIPEMD160Managed__ctor_m6799 ();
extern "C" void RIPEMD160Managed_Initialize_m6800 ();
extern "C" void RIPEMD160Managed_HashCore_m6801 ();
extern "C" void RIPEMD160Managed_HashFinal_m6802 ();
extern "C" void RIPEMD160Managed_Finalize_m6803 ();
extern "C" void RIPEMD160Managed_ProcessBlock_m6804 ();
extern "C" void RIPEMD160Managed_Compress_m6805 ();
extern "C" void RIPEMD160Managed_CompressFinal_m6806 ();
extern "C" void RIPEMD160Managed_ROL_m6807 ();
extern "C" void RIPEMD160Managed_F_m6808 ();
extern "C" void RIPEMD160Managed_G_m6809 ();
extern "C" void RIPEMD160Managed_H_m6810 ();
extern "C" void RIPEMD160Managed_I_m6811 ();
extern "C" void RIPEMD160Managed_J_m6812 ();
extern "C" void RIPEMD160Managed_FF_m6813 ();
extern "C" void RIPEMD160Managed_GG_m6814 ();
extern "C" void RIPEMD160Managed_HH_m6815 ();
extern "C" void RIPEMD160Managed_II_m6816 ();
extern "C" void RIPEMD160Managed_JJ_m6817 ();
extern "C" void RIPEMD160Managed_FFF_m6818 ();
extern "C" void RIPEMD160Managed_GGG_m6819 ();
extern "C" void RIPEMD160Managed_HHH_m6820 ();
extern "C" void RIPEMD160Managed_III_m6821 ();
extern "C" void RIPEMD160Managed_JJJ_m6822 ();
extern "C" void RNGCryptoServiceProvider__ctor_m6823 ();
extern "C" void RNGCryptoServiceProvider__cctor_m6824 ();
extern "C" void RNGCryptoServiceProvider_Check_m6825 ();
extern "C" void RNGCryptoServiceProvider_RngOpen_m6826 ();
extern "C" void RNGCryptoServiceProvider_RngInitialize_m6827 ();
extern "C" void RNGCryptoServiceProvider_RngGetBytes_m6828 ();
extern "C" void RNGCryptoServiceProvider_RngClose_m6829 ();
extern "C" void RNGCryptoServiceProvider_GetBytes_m6830 ();
extern "C" void RNGCryptoServiceProvider_GetNonZeroBytes_m6831 ();
extern "C" void RNGCryptoServiceProvider_Finalize_m6832 ();
extern "C" void RSA__ctor_m3302 ();
extern "C" void RSA_Create_m2283 ();
extern "C" void RSA_Create_m6833 ();
extern "C" void RSA_ZeroizePrivateKey_m6834 ();
extern "C" void RSA_FromXmlString_m6835 ();
extern "C" void RSA_ToXmlString_m6836 ();
extern "C" void RSACryptoServiceProvider__ctor_m6837 ();
extern "C" void RSACryptoServiceProvider__ctor_m3301 ();
extern "C" void RSACryptoServiceProvider__ctor_m2297 ();
extern "C" void RSACryptoServiceProvider__cctor_m6838 ();
extern "C" void RSACryptoServiceProvider_Common_m6839 ();
extern "C" void RSACryptoServiceProvider_Finalize_m6840 ();
extern "C" void RSACryptoServiceProvider_get_KeySize_m6841 ();
extern "C" void RSACryptoServiceProvider_get_PublicOnly_m2281 ();
extern "C" void RSACryptoServiceProvider_DecryptValue_m6842 ();
extern "C" void RSACryptoServiceProvider_EncryptValue_m6843 ();
extern "C" void RSACryptoServiceProvider_ExportParameters_m6844 ();
extern "C" void RSACryptoServiceProvider_ImportParameters_m6845 ();
extern "C" void RSACryptoServiceProvider_Dispose_m6846 ();
extern "C" void RSACryptoServiceProvider_OnKeyGenerated_m6847 ();
extern "C" void RSAPKCS1KeyExchangeFormatter__ctor_m3358 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m6848 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_SetRSAKey_m6849 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m6850 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m3319 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m6851 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetKey_m6852 ();
extern "C" void RSAPKCS1SignatureDeformatter_VerifySignature_m6853 ();
extern "C" void RSAPKCS1SignatureFormatter__ctor_m6854 ();
extern "C" void RSAPKCS1SignatureFormatter_CreateSignature_m6855 ();
extern "C" void RSAPKCS1SignatureFormatter_SetHashAlgorithm_m6856 ();
extern "C" void RSAPKCS1SignatureFormatter_SetKey_m6857 ();
extern "C" void RandomNumberGenerator__ctor_m6858 ();
extern "C" void RandomNumberGenerator_Create_m2455 ();
extern "C" void RandomNumberGenerator_Create_m6859 ();
extern "C" void Rijndael__ctor_m6860 ();
extern "C" void Rijndael_Create_m3336 ();
extern "C" void Rijndael_Create_m6861 ();
extern "C" void RijndaelManaged__ctor_m6862 ();
extern "C" void RijndaelManaged_GenerateIV_m6863 ();
extern "C" void RijndaelManaged_GenerateKey_m6864 ();
extern "C" void RijndaelManaged_CreateDecryptor_m6865 ();
extern "C" void RijndaelManaged_CreateEncryptor_m6866 ();
extern "C" void RijndaelTransform__ctor_m6867 ();
extern "C" void RijndaelTransform__cctor_m6868 ();
extern "C" void RijndaelTransform_Clear_m6869 ();
extern "C" void RijndaelTransform_ECB_m6870 ();
extern "C" void RijndaelTransform_SubByte_m6871 ();
extern "C" void RijndaelTransform_Encrypt128_m6872 ();
extern "C" void RijndaelTransform_Encrypt192_m6873 ();
extern "C" void RijndaelTransform_Encrypt256_m6874 ();
extern "C" void RijndaelTransform_Decrypt128_m6875 ();
extern "C" void RijndaelTransform_Decrypt192_m6876 ();
extern "C" void RijndaelTransform_Decrypt256_m6877 ();
extern "C" void RijndaelManagedTransform__ctor_m6878 ();
extern "C" void RijndaelManagedTransform_System_IDisposable_Dispose_m6879 ();
extern "C" void RijndaelManagedTransform_get_CanReuseTransform_m6880 ();
extern "C" void RijndaelManagedTransform_TransformBlock_m6881 ();
extern "C" void RijndaelManagedTransform_TransformFinalBlock_m6882 ();
extern "C" void SHA1__ctor_m6883 ();
extern "C" void SHA1_Create_m2370 ();
extern "C" void SHA1_Create_m6884 ();
extern "C" void SHA1Internal__ctor_m6885 ();
extern "C" void SHA1Internal_HashCore_m6886 ();
extern "C" void SHA1Internal_HashFinal_m6887 ();
extern "C" void SHA1Internal_Initialize_m6888 ();
extern "C" void SHA1Internal_ProcessBlock_m6889 ();
extern "C" void SHA1Internal_InitialiseBuff_m6890 ();
extern "C" void SHA1Internal_FillBuff_m6891 ();
extern "C" void SHA1Internal_ProcessFinalBlock_m6892 ();
extern "C" void SHA1Internal_AddLength_m6893 ();
extern "C" void SHA1CryptoServiceProvider__ctor_m6894 ();
extern "C" void SHA1CryptoServiceProvider_Finalize_m6895 ();
extern "C" void SHA1CryptoServiceProvider_Dispose_m6896 ();
extern "C" void SHA1CryptoServiceProvider_HashCore_m6897 ();
extern "C" void SHA1CryptoServiceProvider_HashFinal_m6898 ();
extern "C" void SHA1CryptoServiceProvider_Initialize_m6899 ();
extern "C" void SHA1Managed__ctor_m6900 ();
extern "C" void SHA1Managed_HashCore_m6901 ();
extern "C" void SHA1Managed_HashFinal_m6902 ();
extern "C" void SHA1Managed_Initialize_m6903 ();
extern "C" void SHA256__ctor_m6904 ();
extern "C" void SHA256_Create_m3316 ();
extern "C" void SHA256_Create_m6905 ();
extern "C" void SHA256Managed__ctor_m6906 ();
extern "C" void SHA256Managed_HashCore_m6907 ();
extern "C" void SHA256Managed_HashFinal_m6908 ();
extern "C" void SHA256Managed_Initialize_m6909 ();
extern "C" void SHA256Managed_ProcessBlock_m6910 ();
extern "C" void SHA256Managed_ProcessFinalBlock_m6911 ();
extern "C" void SHA256Managed_AddLength_m6912 ();
extern "C" void SHA384__ctor_m6913 ();
extern "C" void SHA384Managed__ctor_m6914 ();
extern "C" void SHA384Managed_Initialize_m6915 ();
extern "C" void SHA384Managed_Initialize_m6916 ();
extern "C" void SHA384Managed_HashCore_m6917 ();
extern "C" void SHA384Managed_HashFinal_m6918 ();
extern "C" void SHA384Managed_update_m6919 ();
extern "C" void SHA384Managed_processWord_m6920 ();
extern "C" void SHA384Managed_unpackWord_m6921 ();
extern "C" void SHA384Managed_adjustByteCounts_m6922 ();
extern "C" void SHA384Managed_processLength_m6923 ();
extern "C" void SHA384Managed_processBlock_m6924 ();
extern "C" void SHA512__ctor_m6925 ();
extern "C" void SHA512Managed__ctor_m6926 ();
extern "C" void SHA512Managed_Initialize_m6927 ();
extern "C" void SHA512Managed_Initialize_m6928 ();
extern "C" void SHA512Managed_HashCore_m6929 ();
extern "C" void SHA512Managed_HashFinal_m6930 ();
extern "C" void SHA512Managed_update_m6931 ();
extern "C" void SHA512Managed_processWord_m6932 ();
extern "C" void SHA512Managed_unpackWord_m6933 ();
extern "C" void SHA512Managed_adjustByteCounts_m6934 ();
extern "C" void SHA512Managed_processLength_m6935 ();
extern "C" void SHA512Managed_processBlock_m6936 ();
extern "C" void SHA512Managed_rotateRight_m6937 ();
extern "C" void SHA512Managed_Ch_m6938 ();
extern "C" void SHA512Managed_Maj_m6939 ();
extern "C" void SHA512Managed_Sum0_m6940 ();
extern "C" void SHA512Managed_Sum1_m6941 ();
extern "C" void SHA512Managed_Sigma0_m6942 ();
extern "C" void SHA512Managed_Sigma1_m6943 ();
extern "C" void SHAConstants__cctor_m6944 ();
extern "C" void SignatureDescription__ctor_m6945 ();
extern "C" void SignatureDescription_set_DeformatterAlgorithm_m6946 ();
extern "C" void SignatureDescription_set_DigestAlgorithm_m6947 ();
extern "C" void SignatureDescription_set_FormatterAlgorithm_m6948 ();
extern "C" void SignatureDescription_set_KeyAlgorithm_m6949 ();
extern "C" void DSASignatureDescription__ctor_m6950 ();
extern "C" void RSAPKCS1SHA1SignatureDescription__ctor_m6951 ();
extern "C" void SymmetricAlgorithm__ctor_m2461 ();
extern "C" void SymmetricAlgorithm_System_IDisposable_Dispose_m6952 ();
extern "C" void SymmetricAlgorithm_Finalize_m3294 ();
extern "C" void SymmetricAlgorithm_Clear_m3310 ();
extern "C" void SymmetricAlgorithm_Dispose_m2469 ();
extern "C" void SymmetricAlgorithm_get_BlockSize_m6953 ();
extern "C" void SymmetricAlgorithm_set_BlockSize_m6954 ();
extern "C" void SymmetricAlgorithm_get_FeedbackSize_m6955 ();
extern "C" void SymmetricAlgorithm_get_IV_m2463 ();
extern "C" void SymmetricAlgorithm_set_IV_m2464 ();
extern "C" void SymmetricAlgorithm_get_Key_m2465 ();
extern "C" void SymmetricAlgorithm_set_Key_m2466 ();
extern "C" void SymmetricAlgorithm_get_KeySize_m2467 ();
extern "C" void SymmetricAlgorithm_set_KeySize_m2468 ();
extern "C" void SymmetricAlgorithm_get_LegalKeySizes_m6956 ();
extern "C" void SymmetricAlgorithm_get_Mode_m6957 ();
extern "C" void SymmetricAlgorithm_set_Mode_m6958 ();
extern "C" void SymmetricAlgorithm_get_Padding_m6959 ();
extern "C" void SymmetricAlgorithm_set_Padding_m6960 ();
extern "C" void SymmetricAlgorithm_CreateDecryptor_m6961 ();
extern "C" void SymmetricAlgorithm_CreateEncryptor_m6962 ();
extern "C" void SymmetricAlgorithm_Create_m3309 ();
extern "C" void ToBase64Transform_System_IDisposable_Dispose_m6963 ();
extern "C" void ToBase64Transform_Finalize_m6964 ();
extern "C" void ToBase64Transform_get_CanReuseTransform_m6965 ();
extern "C" void ToBase64Transform_get_InputBlockSize_m6966 ();
extern "C" void ToBase64Transform_get_OutputBlockSize_m6967 ();
extern "C" void ToBase64Transform_Dispose_m6968 ();
extern "C" void ToBase64Transform_TransformBlock_m6969 ();
extern "C" void ToBase64Transform_InternalTransformBlock_m6970 ();
extern "C" void ToBase64Transform_TransformFinalBlock_m6971 ();
extern "C" void ToBase64Transform_InternalTransformFinalBlock_m6972 ();
extern "C" void TripleDES__ctor_m6973 ();
extern "C" void TripleDES_get_Key_m6974 ();
extern "C" void TripleDES_set_Key_m6975 ();
extern "C" void TripleDES_IsWeakKey_m6976 ();
extern "C" void TripleDES_Create_m3335 ();
extern "C" void TripleDES_Create_m6977 ();
extern "C" void TripleDESCryptoServiceProvider__ctor_m6978 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateIV_m6979 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateKey_m6980 ();
extern "C" void TripleDESCryptoServiceProvider_CreateDecryptor_m6981 ();
extern "C" void TripleDESCryptoServiceProvider_CreateEncryptor_m6982 ();
extern "C" void TripleDESTransform__ctor_m6983 ();
extern "C" void TripleDESTransform_ECB_m6984 ();
extern "C" void TripleDESTransform_GetStrongKey_m6985 ();
extern "C" void SecurityPermission__ctor_m6986 ();
extern "C" void SecurityPermission_set_Flags_m6987 ();
extern "C" void SecurityPermission_IsUnrestricted_m6988 ();
extern "C" void SecurityPermission_IsSubsetOf_m6989 ();
extern "C" void SecurityPermission_ToXml_m6990 ();
extern "C" void SecurityPermission_IsEmpty_m6991 ();
extern "C" void SecurityPermission_Cast_m6992 ();
extern "C" void StrongNamePublicKeyBlob_Equals_m6993 ();
extern "C" void StrongNamePublicKeyBlob_GetHashCode_m6994 ();
extern "C" void StrongNamePublicKeyBlob_ToString_m6995 ();
extern "C" void ApplicationTrust__ctor_m6996 ();
extern "C" void EvidenceEnumerator__ctor_m6997 ();
extern "C" void EvidenceEnumerator_MoveNext_m6998 ();
extern "C" void EvidenceEnumerator_get_Current_m6999 ();
extern "C" void Evidence__ctor_m7000 ();
extern "C" void Evidence_get_Count_m7001 ();
extern "C" void Evidence_get_SyncRoot_m7002 ();
extern "C" void Evidence_get_HostEvidenceList_m7003 ();
extern "C" void Evidence_get_AssemblyEvidenceList_m7004 ();
extern "C" void Evidence_CopyTo_m7005 ();
extern "C" void Evidence_Equals_m7006 ();
extern "C" void Evidence_GetEnumerator_m7007 ();
extern "C" void Evidence_GetHashCode_m7008 ();
extern "C" void Hash__ctor_m7009 ();
extern "C" void Hash__ctor_m7010 ();
extern "C" void Hash_GetObjectData_m7011 ();
extern "C" void Hash_ToString_m7012 ();
extern "C" void Hash_GetData_m7013 ();
extern "C" void StrongName_get_Name_m7014 ();
extern "C" void StrongName_get_PublicKey_m7015 ();
extern "C" void StrongName_get_Version_m7016 ();
extern "C" void StrongName_Equals_m7017 ();
extern "C" void StrongName_GetHashCode_m7018 ();
extern "C" void StrongName_ToString_m7019 ();
extern "C" void WindowsIdentity__ctor_m7020 ();
extern "C" void WindowsIdentity__cctor_m7021 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7022 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m7023 ();
extern "C" void WindowsIdentity_Dispose_m7024 ();
extern "C" void WindowsIdentity_GetCurrentToken_m7025 ();
extern "C" void WindowsIdentity_GetTokenName_m7026 ();
extern "C" void CodeAccessPermission__ctor_m7027 ();
extern "C" void CodeAccessPermission_Equals_m7028 ();
extern "C" void CodeAccessPermission_GetHashCode_m7029 ();
extern "C" void CodeAccessPermission_ToString_m7030 ();
extern "C" void CodeAccessPermission_Element_m7031 ();
extern "C" void CodeAccessPermission_ThrowInvalidPermission_m7032 ();
extern "C" void PermissionSet__ctor_m7033 ();
extern "C" void PermissionSet__ctor_m7034 ();
extern "C" void PermissionSet_set_DeclarativeSecurity_m7035 ();
extern "C" void PermissionSet_CreateFromBinaryFormat_m7036 ();
extern "C" void SecurityContext__ctor_m7037 ();
extern "C" void SecurityContext__ctor_m7038 ();
extern "C" void SecurityContext_Capture_m7039 ();
extern "C" void SecurityContext_get_FlowSuppressed_m7040 ();
extern "C" void SecurityContext_get_CompressedStack_m7041 ();
extern "C" void SecurityAttribute__ctor_m7042 ();
extern "C" void SecurityAttribute_get_Name_m7043 ();
extern "C" void SecurityAttribute_get_Value_m7044 ();
extern "C" void SecurityElement__ctor_m7045 ();
extern "C" void SecurityElement__ctor_m7046 ();
extern "C" void SecurityElement__cctor_m7047 ();
extern "C" void SecurityElement_get_Children_m7048 ();
extern "C" void SecurityElement_get_Tag_m7049 ();
extern "C" void SecurityElement_set_Text_m7050 ();
extern "C" void SecurityElement_AddAttribute_m7051 ();
extern "C" void SecurityElement_AddChild_m7052 ();
extern "C" void SecurityElement_Escape_m7053 ();
extern "C" void SecurityElement_Unescape_m7054 ();
extern "C" void SecurityElement_IsValidAttributeName_m7055 ();
extern "C" void SecurityElement_IsValidAttributeValue_m7056 ();
extern "C" void SecurityElement_IsValidTag_m7057 ();
extern "C" void SecurityElement_IsValidText_m7058 ();
extern "C" void SecurityElement_SearchForChildByTag_m7059 ();
extern "C" void SecurityElement_ToString_m7060 ();
extern "C" void SecurityElement_ToXml_m7061 ();
extern "C" void SecurityElement_GetAttribute_m7062 ();
extern "C" void SecurityException__ctor_m7063 ();
extern "C" void SecurityException__ctor_m7064 ();
extern "C" void SecurityException__ctor_m7065 ();
extern "C" void SecurityException_get_Demanded_m7066 ();
extern "C" void SecurityException_get_FirstPermissionThatFailed_m7067 ();
extern "C" void SecurityException_get_PermissionState_m7068 ();
extern "C" void SecurityException_get_PermissionType_m7069 ();
extern "C" void SecurityException_get_GrantedSet_m7070 ();
extern "C" void SecurityException_get_RefusedSet_m7071 ();
extern "C" void SecurityException_GetObjectData_m7072 ();
extern "C" void SecurityException_ToString_m7073 ();
extern "C" void SecurityFrame__ctor_m7074 ();
extern "C" void SecurityFrame__GetSecurityStack_m7075 ();
extern "C" void SecurityFrame_InitFromRuntimeFrame_m7076 ();
extern "C" void SecurityFrame_get_Assembly_m7077 ();
extern "C" void SecurityFrame_get_Domain_m7078 ();
extern "C" void SecurityFrame_ToString_m7079 ();
extern "C" void SecurityFrame_GetStack_m7080 ();
extern "C" void SecurityManager__cctor_m7081 ();
extern "C" void SecurityManager_get_SecurityEnabled_m7082 ();
extern "C" void SecurityManager_Decode_m7083 ();
extern "C" void SecurityManager_Decode_m7084 ();
extern "C" void SecuritySafeCriticalAttribute__ctor_m7085 ();
extern "C" void SuppressUnmanagedCodeSecurityAttribute__ctor_m7086 ();
extern "C" void UnverifiableCodeAttribute__ctor_m7087 ();
extern "C" void ASCIIEncoding__ctor_m7088 ();
extern "C" void ASCIIEncoding_GetByteCount_m7089 ();
extern "C" void ASCIIEncoding_GetByteCount_m7090 ();
extern "C" void ASCIIEncoding_GetBytes_m7091 ();
extern "C" void ASCIIEncoding_GetBytes_m7092 ();
extern "C" void ASCIIEncoding_GetBytes_m7093 ();
extern "C" void ASCIIEncoding_GetBytes_m7094 ();
extern "C" void ASCIIEncoding_GetCharCount_m7095 ();
extern "C" void ASCIIEncoding_GetChars_m7096 ();
extern "C" void ASCIIEncoding_GetChars_m7097 ();
extern "C" void ASCIIEncoding_GetMaxByteCount_m7098 ();
extern "C" void ASCIIEncoding_GetMaxCharCount_m7099 ();
extern "C" void ASCIIEncoding_GetString_m7100 ();
extern "C" void ASCIIEncoding_GetBytes_m7101 ();
extern "C" void ASCIIEncoding_GetByteCount_m7102 ();
extern "C" void ASCIIEncoding_GetDecoder_m7103 ();
extern "C" void Decoder__ctor_m7104 ();
extern "C" void Decoder_set_Fallback_m7105 ();
extern "C" void Decoder_get_FallbackBuffer_m7106 ();
extern "C" void DecoderExceptionFallback__ctor_m7107 ();
extern "C" void DecoderExceptionFallback_CreateFallbackBuffer_m7108 ();
extern "C" void DecoderExceptionFallback_Equals_m7109 ();
extern "C" void DecoderExceptionFallback_GetHashCode_m7110 ();
extern "C" void DecoderExceptionFallbackBuffer__ctor_m7111 ();
extern "C" void DecoderExceptionFallbackBuffer_get_Remaining_m7112 ();
extern "C" void DecoderExceptionFallbackBuffer_Fallback_m7113 ();
extern "C" void DecoderExceptionFallbackBuffer_GetNextChar_m7114 ();
extern "C" void DecoderFallback__ctor_m7115 ();
extern "C" void DecoderFallback__cctor_m7116 ();
extern "C" void DecoderFallback_get_ExceptionFallback_m7117 ();
extern "C" void DecoderFallback_get_ReplacementFallback_m7118 ();
extern "C" void DecoderFallback_get_StandardSafeFallback_m7119 ();
extern "C" void DecoderFallbackBuffer__ctor_m7120 ();
extern "C" void DecoderFallbackBuffer_Reset_m7121 ();
extern "C" void DecoderFallbackException__ctor_m7122 ();
extern "C" void DecoderFallbackException__ctor_m7123 ();
extern "C" void DecoderFallbackException__ctor_m7124 ();
extern "C" void DecoderReplacementFallback__ctor_m7125 ();
extern "C" void DecoderReplacementFallback__ctor_m7126 ();
extern "C" void DecoderReplacementFallback_get_DefaultString_m7127 ();
extern "C" void DecoderReplacementFallback_CreateFallbackBuffer_m7128 ();
extern "C" void DecoderReplacementFallback_Equals_m7129 ();
extern "C" void DecoderReplacementFallback_GetHashCode_m7130 ();
extern "C" void DecoderReplacementFallbackBuffer__ctor_m7131 ();
extern "C" void DecoderReplacementFallbackBuffer_get_Remaining_m7132 ();
extern "C" void DecoderReplacementFallbackBuffer_Fallback_m7133 ();
extern "C" void DecoderReplacementFallbackBuffer_GetNextChar_m7134 ();
extern "C" void DecoderReplacementFallbackBuffer_Reset_m7135 ();
extern "C" void EncoderExceptionFallback__ctor_m7136 ();
extern "C" void EncoderExceptionFallback_CreateFallbackBuffer_m7137 ();
extern "C" void EncoderExceptionFallback_Equals_m7138 ();
extern "C" void EncoderExceptionFallback_GetHashCode_m7139 ();
extern "C" void EncoderExceptionFallbackBuffer__ctor_m7140 ();
extern "C" void EncoderExceptionFallbackBuffer_get_Remaining_m7141 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m7142 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m7143 ();
extern "C" void EncoderExceptionFallbackBuffer_GetNextChar_m7144 ();
extern "C" void EncoderFallback__ctor_m7145 ();
extern "C" void EncoderFallback__cctor_m7146 ();
extern "C" void EncoderFallback_get_ExceptionFallback_m7147 ();
extern "C" void EncoderFallback_get_ReplacementFallback_m7148 ();
extern "C" void EncoderFallback_get_StandardSafeFallback_m7149 ();
extern "C" void EncoderFallbackBuffer__ctor_m7150 ();
extern "C" void EncoderFallbackException__ctor_m7151 ();
extern "C" void EncoderFallbackException__ctor_m7152 ();
extern "C" void EncoderFallbackException__ctor_m7153 ();
extern "C" void EncoderFallbackException__ctor_m7154 ();
extern "C" void EncoderReplacementFallback__ctor_m7155 ();
extern "C" void EncoderReplacementFallback__ctor_m7156 ();
extern "C" void EncoderReplacementFallback_get_DefaultString_m7157 ();
extern "C" void EncoderReplacementFallback_CreateFallbackBuffer_m7158 ();
extern "C" void EncoderReplacementFallback_Equals_m7159 ();
extern "C" void EncoderReplacementFallback_GetHashCode_m7160 ();
extern "C" void EncoderReplacementFallbackBuffer__ctor_m7161 ();
extern "C" void EncoderReplacementFallbackBuffer_get_Remaining_m7162 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m7163 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m7164 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m7165 ();
extern "C" void EncoderReplacementFallbackBuffer_GetNextChar_m7166 ();
extern "C" void ForwardingDecoder__ctor_m7167 ();
extern "C" void ForwardingDecoder_GetChars_m7168 ();
extern "C" void Encoding__ctor_m7169 ();
extern "C" void Encoding__ctor_m7170 ();
extern "C" void Encoding__cctor_m7171 ();
extern "C" void Encoding___m7172 ();
extern "C" void Encoding_get_IsReadOnly_m7173 ();
extern "C" void Encoding_get_DecoderFallback_m7174 ();
extern "C" void Encoding_set_DecoderFallback_m7175 ();
extern "C" void Encoding_get_EncoderFallback_m7176 ();
extern "C" void Encoding_SetFallbackInternal_m7177 ();
extern "C" void Encoding_Equals_m7178 ();
extern "C" void Encoding_GetByteCount_m7179 ();
extern "C" void Encoding_GetByteCount_m7180 ();
extern "C" void Encoding_GetBytes_m7181 ();
extern "C" void Encoding_GetBytes_m7182 ();
extern "C" void Encoding_GetBytes_m7183 ();
extern "C" void Encoding_GetBytes_m7184 ();
extern "C" void Encoding_GetChars_m7185 ();
extern "C" void Encoding_GetDecoder_m7186 ();
extern "C" void Encoding_InvokeI18N_m7187 ();
extern "C" void Encoding_GetEncoding_m7188 ();
extern "C" void Encoding_GetEncoding_m1207 ();
extern "C" void Encoding_GetHashCode_m7189 ();
extern "C" void Encoding_GetPreamble_m7190 ();
extern "C" void Encoding_GetString_m7191 ();
extern "C" void Encoding_GetString_m7192 ();
extern "C" void Encoding_get_HeaderName_m7193 ();
extern "C" void Encoding_get_WebName_m7194 ();
extern "C" void Encoding_get_ASCII_m1199 ();
extern "C" void Encoding_get_BigEndianUnicode_m3307 ();
extern "C" void Encoding_InternalCodePage_m7195 ();
extern "C" void Encoding_get_Default_m7196 ();
extern "C" void Encoding_get_ISOLatin1_m7197 ();
extern "C" void Encoding_get_UTF7_m3312 ();
extern "C" void Encoding_get_UTF8_m1208 ();
extern "C" void Encoding_get_UTF8Unmarked_m7198 ();
extern "C" void Encoding_get_UTF8UnmarkedUnsafe_m7199 ();
extern "C" void Encoding_get_Unicode_m7200 ();
extern "C" void Encoding_get_UTF32_m7201 ();
extern "C" void Encoding_get_BigEndianUTF32_m7202 ();
extern "C" void Encoding_GetByteCount_m7203 ();
extern "C" void Encoding_GetBytes_m7204 ();
extern "C" void Latin1Encoding__ctor_m7205 ();
extern "C" void Latin1Encoding_GetByteCount_m7206 ();
extern "C" void Latin1Encoding_GetByteCount_m7207 ();
extern "C" void Latin1Encoding_GetBytes_m7208 ();
extern "C" void Latin1Encoding_GetBytes_m7209 ();
extern "C" void Latin1Encoding_GetBytes_m7210 ();
extern "C" void Latin1Encoding_GetBytes_m7211 ();
extern "C" void Latin1Encoding_GetCharCount_m7212 ();
extern "C" void Latin1Encoding_GetChars_m7213 ();
extern "C" void Latin1Encoding_GetMaxByteCount_m7214 ();
extern "C" void Latin1Encoding_GetMaxCharCount_m7215 ();
extern "C" void Latin1Encoding_GetString_m7216 ();
extern "C" void Latin1Encoding_GetString_m7217 ();
extern "C" void Latin1Encoding_get_HeaderName_m7218 ();
extern "C" void Latin1Encoding_get_WebName_m7219 ();
extern "C" void StringBuilder__ctor_m7220 ();
extern "C" void StringBuilder__ctor_m7221 ();
extern "C" void StringBuilder__ctor_m2264 ();
extern "C" void StringBuilder__ctor_m1268 ();
extern "C" void StringBuilder__ctor_m2300 ();
extern "C" void StringBuilder__ctor_m2251 ();
extern "C" void StringBuilder__ctor_m7222 ();
extern "C" void StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m7223 ();
extern "C" void StringBuilder_get_Capacity_m7224 ();
extern "C" void StringBuilder_set_Capacity_m7225 ();
extern "C" void StringBuilder_get_Length_m2363 ();
extern "C" void StringBuilder_set_Length_m2405 ();
extern "C" void StringBuilder_get_Chars_m7226 ();
extern "C" void StringBuilder_set_Chars_m7227 ();
extern "C" void StringBuilder_ToString_m1269 ();
extern "C" void StringBuilder_ToString_m7228 ();
extern "C" void StringBuilder_Remove_m7229 ();
extern "C" void StringBuilder_Replace_m7230 ();
extern "C" void StringBuilder_Replace_m7231 ();
extern "C" void StringBuilder_Append_m1274 ();
extern "C" void StringBuilder_Append_m2311 ();
extern "C" void StringBuilder_Append_m2266 ();
extern "C" void StringBuilder_Append_m2252 ();
extern "C" void StringBuilder_Append_m1270 ();
extern "C" void StringBuilder_Append_m7232 ();
extern "C" void StringBuilder_Append_m7233 ();
extern "C" void StringBuilder_Append_m2375 ();
extern "C" void StringBuilder_AppendFormat_m3289 ();
extern "C" void StringBuilder_AppendFormat_m7234 ();
extern "C" void StringBuilder_AppendFormat_m2265 ();
extern "C" void StringBuilder_AppendFormat_m2331 ();
extern "C" void StringBuilder_AppendFormat_m2334 ();
extern "C" void StringBuilder_Insert_m7235 ();
extern "C" void StringBuilder_Insert_m7236 ();
extern "C" void StringBuilder_Insert_m7237 ();
extern "C" void StringBuilder_InternalEnsureCapacity_m7238 ();
extern "C" void UTF32Decoder__ctor_m7239 ();
extern "C" void UTF32Decoder_GetChars_m7240 ();
extern "C" void UTF32Encoding__ctor_m7241 ();
extern "C" void UTF32Encoding__ctor_m7242 ();
extern "C" void UTF32Encoding__ctor_m7243 ();
extern "C" void UTF32Encoding_GetByteCount_m7244 ();
extern "C" void UTF32Encoding_GetBytes_m7245 ();
extern "C" void UTF32Encoding_GetCharCount_m7246 ();
extern "C" void UTF32Encoding_GetChars_m7247 ();
extern "C" void UTF32Encoding_GetMaxByteCount_m7248 ();
extern "C" void UTF32Encoding_GetMaxCharCount_m7249 ();
extern "C" void UTF32Encoding_GetDecoder_m7250 ();
extern "C" void UTF32Encoding_GetPreamble_m7251 ();
extern "C" void UTF32Encoding_Equals_m7252 ();
extern "C" void UTF32Encoding_GetHashCode_m7253 ();
extern "C" void UTF32Encoding_GetByteCount_m7254 ();
extern "C" void UTF32Encoding_GetByteCount_m7255 ();
extern "C" void UTF32Encoding_GetBytes_m7256 ();
extern "C" void UTF32Encoding_GetBytes_m7257 ();
extern "C" void UTF32Encoding_GetString_m7258 ();
extern "C" void UTF7Decoder__ctor_m7259 ();
extern "C" void UTF7Decoder_GetChars_m7260 ();
extern "C" void UTF7Encoding__ctor_m7261 ();
extern "C" void UTF7Encoding__ctor_m7262 ();
extern "C" void UTF7Encoding__cctor_m7263 ();
extern "C" void UTF7Encoding_GetHashCode_m7264 ();
extern "C" void UTF7Encoding_Equals_m7265 ();
extern "C" void UTF7Encoding_InternalGetByteCount_m7266 ();
extern "C" void UTF7Encoding_GetByteCount_m7267 ();
extern "C" void UTF7Encoding_InternalGetBytes_m7268 ();
extern "C" void UTF7Encoding_GetBytes_m7269 ();
extern "C" void UTF7Encoding_InternalGetCharCount_m7270 ();
extern "C" void UTF7Encoding_GetCharCount_m7271 ();
extern "C" void UTF7Encoding_InternalGetChars_m7272 ();
extern "C" void UTF7Encoding_GetChars_m7273 ();
extern "C" void UTF7Encoding_GetMaxByteCount_m7274 ();
extern "C" void UTF7Encoding_GetMaxCharCount_m7275 ();
extern "C" void UTF7Encoding_GetDecoder_m7276 ();
extern "C" void UTF7Encoding_GetByteCount_m7277 ();
extern "C" void UTF7Encoding_GetByteCount_m7278 ();
extern "C" void UTF7Encoding_GetBytes_m7279 ();
extern "C" void UTF7Encoding_GetBytes_m7280 ();
extern "C" void UTF7Encoding_GetString_m7281 ();
extern "C" void UTF8Decoder__ctor_m7282 ();
extern "C" void UTF8Decoder_GetChars_m7283 ();
extern "C" void UTF8Encoding__ctor_m7284 ();
extern "C" void UTF8Encoding__ctor_m7285 ();
extern "C" void UTF8Encoding__ctor_m7286 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m7287 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m7288 ();
extern "C" void UTF8Encoding_GetByteCount_m7289 ();
extern "C" void UTF8Encoding_GetByteCount_m7290 ();
extern "C" void UTF8Encoding_InternalGetBytes_m7291 ();
extern "C" void UTF8Encoding_InternalGetBytes_m7292 ();
extern "C" void UTF8Encoding_GetBytes_m7293 ();
extern "C" void UTF8Encoding_GetBytes_m7294 ();
extern "C" void UTF8Encoding_GetBytes_m7295 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m7296 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m7297 ();
extern "C" void UTF8Encoding_Fallback_m7298 ();
extern "C" void UTF8Encoding_Fallback_m7299 ();
extern "C" void UTF8Encoding_GetCharCount_m7300 ();
extern "C" void UTF8Encoding_InternalGetChars_m7301 ();
extern "C" void UTF8Encoding_InternalGetChars_m7302 ();
extern "C" void UTF8Encoding_GetChars_m7303 ();
extern "C" void UTF8Encoding_GetMaxByteCount_m7304 ();
extern "C" void UTF8Encoding_GetMaxCharCount_m7305 ();
extern "C" void UTF8Encoding_GetDecoder_m7306 ();
extern "C" void UTF8Encoding_GetPreamble_m7307 ();
extern "C" void UTF8Encoding_Equals_m7308 ();
extern "C" void UTF8Encoding_GetHashCode_m7309 ();
extern "C" void UTF8Encoding_GetByteCount_m7310 ();
extern "C" void UTF8Encoding_GetString_m7311 ();
extern "C" void UnicodeDecoder__ctor_m7312 ();
extern "C" void UnicodeDecoder_GetChars_m7313 ();
extern "C" void UnicodeEncoding__ctor_m7314 ();
extern "C" void UnicodeEncoding__ctor_m7315 ();
extern "C" void UnicodeEncoding__ctor_m7316 ();
extern "C" void UnicodeEncoding_GetByteCount_m7317 ();
extern "C" void UnicodeEncoding_GetByteCount_m7318 ();
extern "C" void UnicodeEncoding_GetByteCount_m7319 ();
extern "C" void UnicodeEncoding_GetBytes_m7320 ();
extern "C" void UnicodeEncoding_GetBytes_m7321 ();
extern "C" void UnicodeEncoding_GetBytes_m7322 ();
extern "C" void UnicodeEncoding_GetBytesInternal_m7323 ();
extern "C" void UnicodeEncoding_GetCharCount_m7324 ();
extern "C" void UnicodeEncoding_GetChars_m7325 ();
extern "C" void UnicodeEncoding_GetString_m7326 ();
extern "C" void UnicodeEncoding_GetCharsInternal_m7327 ();
extern "C" void UnicodeEncoding_GetMaxByteCount_m7328 ();
extern "C" void UnicodeEncoding_GetMaxCharCount_m7329 ();
extern "C" void UnicodeEncoding_GetDecoder_m7330 ();
extern "C" void UnicodeEncoding_GetPreamble_m7331 ();
extern "C" void UnicodeEncoding_Equals_m7332 ();
extern "C" void UnicodeEncoding_GetHashCode_m7333 ();
extern "C" void UnicodeEncoding_CopyChars_m7334 ();
extern "C" void CompressedStack__ctor_m7335 ();
extern "C" void CompressedStack__ctor_m7336 ();
extern "C" void CompressedStack_CreateCopy_m7337 ();
extern "C" void CompressedStack_Capture_m7338 ();
extern "C" void CompressedStack_GetObjectData_m7339 ();
extern "C" void CompressedStack_IsEmpty_m7340 ();
extern "C" void EventWaitHandle__ctor_m7341 ();
extern "C" void EventWaitHandle_IsManualReset_m7342 ();
extern "C" void EventWaitHandle_Reset_m3346 ();
extern "C" void EventWaitHandle_Set_m3344 ();
extern "C" void ExecutionContext__ctor_m7343 ();
extern "C" void ExecutionContext__ctor_m7344 ();
extern "C" void ExecutionContext__ctor_m7345 ();
extern "C" void ExecutionContext_Capture_m7346 ();
extern "C" void ExecutionContext_GetObjectData_m7347 ();
extern "C" void ExecutionContext_get_SecurityContext_m7348 ();
extern "C" void ExecutionContext_set_SecurityContext_m7349 ();
extern "C" void ExecutionContext_get_FlowSuppressed_m7350 ();
extern "C" void ExecutionContext_IsFlowSuppressed_m7351 ();
extern "C" void Interlocked_CompareExchange_m7352 ();
extern "C" void ManualResetEvent__ctor_m3343 ();
extern "C" void Monitor_Enter_m2259 ();
extern "C" void Monitor_Exit_m2260 ();
extern "C" void Monitor_Monitor_pulse_m7353 ();
extern "C" void Monitor_Monitor_test_synchronised_m7354 ();
extern "C" void Monitor_Pulse_m7355 ();
extern "C" void Monitor_Monitor_wait_m7356 ();
extern "C" void Monitor_Wait_m7357 ();
extern "C" void Mutex__ctor_m7358 ();
extern "C" void Mutex_CreateMutex_internal_m7359 ();
extern "C" void Mutex_ReleaseMutex_internal_m7360 ();
extern "C" void Mutex_ReleaseMutex_m7361 ();
extern "C" void NativeEventCalls_CreateEvent_internal_m7362 ();
extern "C" void NativeEventCalls_SetEvent_internal_m7363 ();
extern "C" void NativeEventCalls_ResetEvent_internal_m7364 ();
extern "C" void NativeEventCalls_CloseEvent_internal_m7365 ();
extern "C" void SynchronizationLockException__ctor_m7366 ();
extern "C" void SynchronizationLockException__ctor_m7367 ();
extern "C" void SynchronizationLockException__ctor_m7368 ();
extern "C" void Thread__ctor_m7369 ();
extern "C" void Thread__cctor_m7370 ();
extern "C" void Thread_get_CurrentContext_m7371 ();
extern "C" void Thread_CurrentThread_internal_m7372 ();
extern "C" void Thread_get_CurrentThread_m7373 ();
extern "C" void Thread_GetDomainID_m7374 ();
extern "C" void Thread_Thread_internal_m7375 ();
extern "C" void Thread_Thread_init_m7376 ();
extern "C" void Thread_GetCachedCurrentCulture_m7377 ();
extern "C" void Thread_GetSerializedCurrentCulture_m7378 ();
extern "C" void Thread_SetCachedCurrentCulture_m7379 ();
extern "C" void Thread_GetCachedCurrentUICulture_m7380 ();
extern "C" void Thread_GetSerializedCurrentUICulture_m7381 ();
extern "C" void Thread_SetCachedCurrentUICulture_m7382 ();
extern "C" void Thread_get_CurrentCulture_m7383 ();
extern "C" void Thread_get_CurrentUICulture_m7384 ();
extern "C" void Thread_set_IsBackground_m7385 ();
extern "C" void Thread_SetName_internal_m7386 ();
extern "C" void Thread_set_Name_m7387 ();
extern "C" void Thread_Start_m7388 ();
extern "C" void Thread_Thread_free_internal_m7389 ();
extern "C" void Thread_Finalize_m7390 ();
extern "C" void Thread_SetState_m7391 ();
extern "C" void Thread_ClrState_m7392 ();
extern "C" void Thread_GetNewManagedId_m7393 ();
extern "C" void Thread_GetNewManagedId_internal_m7394 ();
extern "C" void Thread_get_ExecutionContext_m7395 ();
extern "C" void Thread_get_ManagedThreadId_m7396 ();
extern "C" void Thread_GetHashCode_m7397 ();
extern "C" void Thread_GetCompressedStack_m7398 ();
extern "C" void ThreadAbortException__ctor_m7399 ();
extern "C" void ThreadAbortException__ctor_m7400 ();
extern "C" void ThreadInterruptedException__ctor_m7401 ();
extern "C" void ThreadInterruptedException__ctor_m7402 ();
extern "C" void ThreadPool_QueueUserWorkItem_m7403 ();
extern "C" void ThreadStateException__ctor_m7404 ();
extern "C" void ThreadStateException__ctor_m7405 ();
extern "C" void TimerComparer__ctor_m7406 ();
extern "C" void TimerComparer_Compare_m7407 ();
extern "C" void Scheduler__ctor_m7408 ();
extern "C" void Scheduler__cctor_m7409 ();
extern "C" void Scheduler_get_Instance_m7410 ();
extern "C" void Scheduler_Remove_m7411 ();
extern "C" void Scheduler_Change_m7412 ();
extern "C" void Scheduler_Add_m7413 ();
extern "C" void Scheduler_InternalRemove_m7414 ();
extern "C" void Scheduler_SchedulerThread_m7415 ();
extern "C" void Scheduler_ShrinkIfNeeded_m7416 ();
extern "C" void Timer__cctor_m7417 ();
extern "C" void Timer_Change_m7418 ();
extern "C" void Timer_Dispose_m7419 ();
extern "C" void Timer_Change_m7420 ();
extern "C" void WaitHandle__ctor_m7421 ();
extern "C" void WaitHandle__cctor_m7422 ();
extern "C" void WaitHandle_System_IDisposable_Dispose_m7423 ();
extern "C" void WaitHandle_get_Handle_m7424 ();
extern "C" void WaitHandle_set_Handle_m7425 ();
extern "C" void WaitHandle_WaitOne_internal_m7426 ();
extern "C" void WaitHandle_Dispose_m7427 ();
extern "C" void WaitHandle_WaitOne_m7428 ();
extern "C" void WaitHandle_WaitOne_m7429 ();
extern "C" void WaitHandle_CheckDisposed_m7430 ();
extern "C" void WaitHandle_Finalize_m7431 ();
extern "C" void AccessViolationException__ctor_m7432 ();
extern "C" void AccessViolationException__ctor_m7433 ();
extern "C" void ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m7434 ();
extern "C" void ActivationContext_Finalize_m7435 ();
extern "C" void ActivationContext_Dispose_m7436 ();
extern "C" void ActivationContext_Dispose_m7437 ();
extern "C" void Activator_CreateInstance_m7438 ();
extern "C" void Activator_CreateInstance_m7439 ();
extern "C" void Activator_CreateInstance_m7440 ();
extern "C" void Activator_CreateInstance_m7441 ();
extern "C" void Activator_CreateInstance_m2280 ();
extern "C" void Activator_CheckType_m7442 ();
extern "C" void Activator_CheckAbstractType_m7443 ();
extern "C" void Activator_CreateInstanceInternal_m7444 ();
extern "C" void AppDomain_getFriendlyName_m7445 ();
extern "C" void AppDomain_getCurDomain_m7446 ();
extern "C" void AppDomain_get_CurrentDomain_m7447 ();
extern "C" void AppDomain_LoadAssembly_m7448 ();
extern "C" void AppDomain_Load_m7449 ();
extern "C" void AppDomain_Load_m7450 ();
extern "C" void AppDomain_InternalGetContext_m7451 ();
extern "C" void AppDomain_InternalGetDefaultContext_m7452 ();
extern "C" void AppDomain_InternalGetProcessGuid_m7453 ();
extern "C" void AppDomain_GetProcessGuid_m7454 ();
extern "C" void AppDomain_ToString_m7455 ();
extern "C" void AppDomain_DoTypeResolve_m7456 ();
extern "C" void AppDomainSetup__ctor_m7457 ();
extern "C" void ApplicationException__ctor_m7458 ();
extern "C" void ApplicationException__ctor_m7459 ();
extern "C" void ApplicationException__ctor_m7460 ();
extern "C" void ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m7461 ();
extern "C" void ApplicationIdentity_ToString_m7462 ();
extern "C" void ArgumentException__ctor_m7463 ();
extern "C" void ArgumentException__ctor_m1162 ();
extern "C" void ArgumentException__ctor_m2342 ();
extern "C" void ArgumentException__ctor_m2237 ();
extern "C" void ArgumentException__ctor_m7464 ();
extern "C" void ArgumentException__ctor_m7465 ();
extern "C" void ArgumentException_get_ParamName_m7466 ();
extern "C" void ArgumentException_get_Message_m7467 ();
extern "C" void ArgumentException_GetObjectData_m7468 ();
extern "C" void ArgumentNullException__ctor_m7469 ();
extern "C" void ArgumentNullException__ctor_m1266 ();
extern "C" void ArgumentNullException__ctor_m2234 ();
extern "C" void ArgumentNullException__ctor_m7470 ();
extern "C" void ArgumentOutOfRangeException__ctor_m2374 ();
extern "C" void ArgumentOutOfRangeException__ctor_m2239 ();
extern "C" void ArgumentOutOfRangeException__ctor_m1275 ();
extern "C" void ArgumentOutOfRangeException__ctor_m7471 ();
extern "C" void ArgumentOutOfRangeException__ctor_m7472 ();
extern "C" void ArgumentOutOfRangeException_get_Message_m7473 ();
extern "C" void ArgumentOutOfRangeException_GetObjectData_m7474 ();
extern "C" void ArithmeticException__ctor_m7475 ();
extern "C" void ArithmeticException__ctor_m3288 ();
extern "C" void ArithmeticException__ctor_m7476 ();
extern "C" void ArrayTypeMismatchException__ctor_m7477 ();
extern "C" void ArrayTypeMismatchException__ctor_m7478 ();
extern "C" void ArrayTypeMismatchException__ctor_m7479 ();
extern "C" void BitConverter__cctor_m7480 ();
extern "C" void BitConverter_AmILittleEndian_m7481 ();
extern "C" void BitConverter_DoubleWordsAreSwapped_m7482 ();
extern "C" void BitConverter_DoubleToInt64Bits_m7483 ();
extern "C" void BitConverter_GetBytes_m7484 ();
extern "C" void BitConverter_GetBytes_m7485 ();
extern "C" void BitConverter_PutBytes_m7486 ();
extern "C" void BitConverter_ToInt64_m7487 ();
extern "C" void BitConverter_ToString_m3341 ();
extern "C" void BitConverter_ToString_m7488 ();
extern "C" void Buffer_ByteLength_m7489 ();
extern "C" void Buffer_BlockCopy_m2288 ();
extern "C" void Buffer_ByteLengthInternal_m7490 ();
extern "C" void Buffer_BlockCopyInternal_m7491 ();
extern "C" void CharEnumerator__ctor_m7492 ();
extern "C" void CharEnumerator_System_Collections_IEnumerator_get_Current_m7493 ();
extern "C" void CharEnumerator_System_IDisposable_Dispose_m7494 ();
extern "C" void CharEnumerator_get_Current_m7495 ();
extern "C" void CharEnumerator_MoveNext_m7496 ();
extern "C" void Console__cctor_m7497 ();
extern "C" void Console_SetEncodings_m7498 ();
extern "C" void Console_get_Error_m2394 ();
extern "C" void Console_Open_m7499 ();
extern "C" void Console_OpenStandardError_m7500 ();
extern "C" void Console_OpenStandardInput_m7501 ();
extern "C" void Console_OpenStandardOutput_m7502 ();
extern "C" void ContextBoundObject__ctor_m7503 ();
extern "C" void Convert__cctor_m7504 ();
extern "C" void Convert_InternalFromBase64String_m7505 ();
extern "C" void Convert_FromBase64String_m3320 ();
extern "C" void Convert_ToBase64String_m1245 ();
extern "C" void Convert_ToBase64String_m7506 ();
extern "C" void Convert_ToBoolean_m7507 ();
extern "C" void Convert_ToBoolean_m7508 ();
extern "C" void Convert_ToBoolean_m7509 ();
extern "C" void Convert_ToBoolean_m7510 ();
extern "C" void Convert_ToBoolean_m7511 ();
extern "C" void Convert_ToBoolean_m7512 ();
extern "C" void Convert_ToBoolean_m7513 ();
extern "C" void Convert_ToBoolean_m7514 ();
extern "C" void Convert_ToBoolean_m7515 ();
extern "C" void Convert_ToBoolean_m7516 ();
extern "C" void Convert_ToBoolean_m7517 ();
extern "C" void Convert_ToBoolean_m7518 ();
extern "C" void Convert_ToBoolean_m1241 ();
extern "C" void Convert_ToBoolean_m7519 ();
extern "C" void Convert_ToByte_m7520 ();
extern "C" void Convert_ToByte_m7521 ();
extern "C" void Convert_ToByte_m7522 ();
extern "C" void Convert_ToByte_m7523 ();
extern "C" void Convert_ToByte_m7524 ();
extern "C" void Convert_ToByte_m7525 ();
extern "C" void Convert_ToByte_m7526 ();
extern "C" void Convert_ToByte_m7527 ();
extern "C" void Convert_ToByte_m7528 ();
extern "C" void Convert_ToByte_m7529 ();
extern "C" void Convert_ToByte_m7530 ();
extern "C" void Convert_ToByte_m7531 ();
extern "C" void Convert_ToByte_m7532 ();
extern "C" void Convert_ToByte_m7533 ();
extern "C" void Convert_ToByte_m7534 ();
extern "C" void Convert_ToChar_m3321 ();
extern "C" void Convert_ToChar_m7535 ();
extern "C" void Convert_ToChar_m7536 ();
extern "C" void Convert_ToChar_m7537 ();
extern "C" void Convert_ToChar_m7538 ();
extern "C" void Convert_ToChar_m7539 ();
extern "C" void Convert_ToChar_m7540 ();
extern "C" void Convert_ToChar_m7541 ();
extern "C" void Convert_ToChar_m7542 ();
extern "C" void Convert_ToChar_m7543 ();
extern "C" void Convert_ToChar_m7544 ();
extern "C" void Convert_ToDateTime_m7545 ();
extern "C" void Convert_ToDateTime_m7546 ();
extern "C" void Convert_ToDateTime_m7547 ();
extern "C" void Convert_ToDateTime_m7548 ();
extern "C" void Convert_ToDateTime_m7549 ();
extern "C" void Convert_ToDateTime_m7550 ();
extern "C" void Convert_ToDateTime_m7551 ();
extern "C" void Convert_ToDateTime_m7552 ();
extern "C" void Convert_ToDateTime_m7553 ();
extern "C" void Convert_ToDateTime_m7554 ();
extern "C" void Convert_ToDecimal_m7555 ();
extern "C" void Convert_ToDecimal_m7556 ();
extern "C" void Convert_ToDecimal_m7557 ();
extern "C" void Convert_ToDecimal_m7558 ();
extern "C" void Convert_ToDecimal_m7559 ();
extern "C" void Convert_ToDecimal_m7560 ();
extern "C" void Convert_ToDecimal_m7561 ();
extern "C" void Convert_ToDecimal_m7562 ();
extern "C" void Convert_ToDecimal_m7563 ();
extern "C" void Convert_ToDecimal_m7564 ();
extern "C" void Convert_ToDecimal_m7565 ();
extern "C" void Convert_ToDecimal_m7566 ();
extern "C" void Convert_ToDecimal_m7567 ();
extern "C" void Convert_ToDouble_m7568 ();
extern "C" void Convert_ToDouble_m7569 ();
extern "C" void Convert_ToDouble_m7570 ();
extern "C" void Convert_ToDouble_m7571 ();
extern "C" void Convert_ToDouble_m7572 ();
extern "C" void Convert_ToDouble_m7573 ();
extern "C" void Convert_ToDouble_m7574 ();
extern "C" void Convert_ToDouble_m7575 ();
extern "C" void Convert_ToDouble_m7576 ();
extern "C" void Convert_ToDouble_m7577 ();
extern "C" void Convert_ToDouble_m7578 ();
extern "C" void Convert_ToDouble_m7579 ();
extern "C" void Convert_ToDouble_m7580 ();
extern "C" void Convert_ToDouble_m1286 ();
extern "C" void Convert_ToInt16_m7581 ();
extern "C" void Convert_ToInt16_m7582 ();
extern "C" void Convert_ToInt16_m7583 ();
extern "C" void Convert_ToInt16_m7584 ();
extern "C" void Convert_ToInt16_m7585 ();
extern "C" void Convert_ToInt16_m7586 ();
extern "C" void Convert_ToInt16_m7587 ();
extern "C" void Convert_ToInt16_m7588 ();
extern "C" void Convert_ToInt16_m7589 ();
extern "C" void Convert_ToInt16_m7590 ();
extern "C" void Convert_ToInt16_m3292 ();
extern "C" void Convert_ToInt16_m7591 ();
extern "C" void Convert_ToInt16_m7592 ();
extern "C" void Convert_ToInt16_m7593 ();
extern "C" void Convert_ToInt16_m7594 ();
extern "C" void Convert_ToInt16_m7595 ();
extern "C" void Convert_ToInt32_m7596 ();
extern "C" void Convert_ToInt32_m7597 ();
extern "C" void Convert_ToInt32_m7598 ();
extern "C" void Convert_ToInt32_m7599 ();
extern "C" void Convert_ToInt32_m7600 ();
extern "C" void Convert_ToInt32_m7601 ();
extern "C" void Convert_ToInt32_m7602 ();
extern "C" void Convert_ToInt32_m7603 ();
extern "C" void Convert_ToInt32_m7604 ();
extern "C" void Convert_ToInt32_m7605 ();
extern "C" void Convert_ToInt32_m7606 ();
extern "C" void Convert_ToInt32_m7607 ();
extern "C" void Convert_ToInt32_m7608 ();
extern "C" void Convert_ToInt32_m1238 ();
extern "C" void Convert_ToInt32_m3329 ();
extern "C" void Convert_ToInt64_m7609 ();
extern "C" void Convert_ToInt64_m7610 ();
extern "C" void Convert_ToInt64_m7611 ();
extern "C" void Convert_ToInt64_m7612 ();
extern "C" void Convert_ToInt64_m7613 ();
extern "C" void Convert_ToInt64_m7614 ();
extern "C" void Convert_ToInt64_m7615 ();
extern "C" void Convert_ToInt64_m7616 ();
extern "C" void Convert_ToInt64_m7617 ();
extern "C" void Convert_ToInt64_m7618 ();
extern "C" void Convert_ToInt64_m7619 ();
extern "C" void Convert_ToInt64_m7620 ();
extern "C" void Convert_ToInt64_m7621 ();
extern "C" void Convert_ToInt64_m7622 ();
extern "C" void Convert_ToInt64_m7623 ();
extern "C" void Convert_ToInt64_m7624 ();
extern "C" void Convert_ToInt64_m7625 ();
extern "C" void Convert_ToSByte_m7626 ();
extern "C" void Convert_ToSByte_m7627 ();
extern "C" void Convert_ToSByte_m7628 ();
extern "C" void Convert_ToSByte_m7629 ();
extern "C" void Convert_ToSByte_m7630 ();
extern "C" void Convert_ToSByte_m7631 ();
extern "C" void Convert_ToSByte_m7632 ();
extern "C" void Convert_ToSByte_m7633 ();
extern "C" void Convert_ToSByte_m7634 ();
extern "C" void Convert_ToSByte_m7635 ();
extern "C" void Convert_ToSByte_m7636 ();
extern "C" void Convert_ToSByte_m7637 ();
extern "C" void Convert_ToSByte_m7638 ();
extern "C" void Convert_ToSByte_m7639 ();
extern "C" void Convert_ToSingle_m7640 ();
extern "C" void Convert_ToSingle_m7641 ();
extern "C" void Convert_ToSingle_m7642 ();
extern "C" void Convert_ToSingle_m7643 ();
extern "C" void Convert_ToSingle_m7644 ();
extern "C" void Convert_ToSingle_m7645 ();
extern "C" void Convert_ToSingle_m7646 ();
extern "C" void Convert_ToSingle_m7647 ();
extern "C" void Convert_ToSingle_m7648 ();
extern "C" void Convert_ToSingle_m7649 ();
extern "C" void Convert_ToSingle_m7650 ();
extern "C" void Convert_ToSingle_m7651 ();
extern "C" void Convert_ToSingle_m7652 ();
extern "C" void Convert_ToSingle_m7653 ();
extern "C" void Convert_ToString_m7654 ();
extern "C" void Convert_ToString_m7655 ();
extern "C" void Convert_ToUInt16_m7656 ();
extern "C" void Convert_ToUInt16_m7657 ();
extern "C" void Convert_ToUInt16_m7658 ();
extern "C" void Convert_ToUInt16_m7659 ();
extern "C" void Convert_ToUInt16_m7660 ();
extern "C" void Convert_ToUInt16_m7661 ();
extern "C" void Convert_ToUInt16_m7662 ();
extern "C" void Convert_ToUInt16_m7663 ();
extern "C" void Convert_ToUInt16_m7664 ();
extern "C" void Convert_ToUInt16_m7665 ();
extern "C" void Convert_ToUInt16_m7666 ();
extern "C" void Convert_ToUInt16_m7667 ();
extern "C" void Convert_ToUInt16_m7668 ();
extern "C" void Convert_ToUInt16_m1239 ();
extern "C" void Convert_ToUInt16_m7669 ();
extern "C" void Convert_ToUInt32_m7670 ();
extern "C" void Convert_ToUInt32_m7671 ();
extern "C" void Convert_ToUInt32_m7672 ();
extern "C" void Convert_ToUInt32_m7673 ();
extern "C" void Convert_ToUInt32_m7674 ();
extern "C" void Convert_ToUInt32_m7675 ();
extern "C" void Convert_ToUInt32_m7676 ();
extern "C" void Convert_ToUInt32_m7677 ();
extern "C" void Convert_ToUInt32_m7678 ();
extern "C" void Convert_ToUInt32_m7679 ();
extern "C" void Convert_ToUInt32_m7680 ();
extern "C" void Convert_ToUInt32_m7681 ();
extern "C" void Convert_ToUInt32_m7682 ();
extern "C" void Convert_ToUInt32_m7683 ();
extern "C" void Convert_ToUInt64_m7684 ();
extern "C" void Convert_ToUInt64_m7685 ();
extern "C" void Convert_ToUInt64_m7686 ();
extern "C" void Convert_ToUInt64_m7687 ();
extern "C" void Convert_ToUInt64_m7688 ();
extern "C" void Convert_ToUInt64_m7689 ();
extern "C" void Convert_ToUInt64_m7690 ();
extern "C" void Convert_ToUInt64_m7691 ();
extern "C" void Convert_ToUInt64_m7692 ();
extern "C" void Convert_ToUInt64_m7693 ();
extern "C" void Convert_ToUInt64_m7694 ();
extern "C" void Convert_ToUInt64_m7695 ();
extern "C" void Convert_ToUInt64_m7696 ();
extern "C" void Convert_ToUInt64_m1240 ();
extern "C" void Convert_ToUInt64_m7697 ();
extern "C" void Convert_ChangeType_m7698 ();
extern "C" void Convert_ToType_m7699 ();
extern "C" void DBNull__ctor_m7700 ();
extern "C" void DBNull__ctor_m7701 ();
extern "C" void DBNull__cctor_m7702 ();
extern "C" void DBNull_System_IConvertible_ToBoolean_m7703 ();
extern "C" void DBNull_System_IConvertible_ToByte_m7704 ();
extern "C" void DBNull_System_IConvertible_ToChar_m7705 ();
extern "C" void DBNull_System_IConvertible_ToDateTime_m7706 ();
extern "C" void DBNull_System_IConvertible_ToDecimal_m7707 ();
extern "C" void DBNull_System_IConvertible_ToDouble_m7708 ();
extern "C" void DBNull_System_IConvertible_ToInt16_m7709 ();
extern "C" void DBNull_System_IConvertible_ToInt32_m7710 ();
extern "C" void DBNull_System_IConvertible_ToInt64_m7711 ();
extern "C" void DBNull_System_IConvertible_ToSByte_m7712 ();
extern "C" void DBNull_System_IConvertible_ToSingle_m7713 ();
extern "C" void DBNull_System_IConvertible_ToType_m7714 ();
extern "C" void DBNull_System_IConvertible_ToUInt16_m7715 ();
extern "C" void DBNull_System_IConvertible_ToUInt32_m7716 ();
extern "C" void DBNull_System_IConvertible_ToUInt64_m7717 ();
extern "C" void DBNull_GetObjectData_m7718 ();
extern "C" void DBNull_ToString_m7719 ();
extern "C" void DBNull_ToString_m7720 ();
extern "C" void DateTime__ctor_m7721 ();
extern "C" void DateTime__ctor_m7722 ();
extern "C" void DateTime__ctor_m1311 ();
extern "C" void DateTime__ctor_m7723 ();
extern "C" void DateTime__ctor_m7724 ();
extern "C" void DateTime__cctor_m7725 ();
extern "C" void DateTime_System_IConvertible_ToBoolean_m7726 ();
extern "C" void DateTime_System_IConvertible_ToByte_m7727 ();
extern "C" void DateTime_System_IConvertible_ToChar_m7728 ();
extern "C" void DateTime_System_IConvertible_ToDateTime_m7729 ();
extern "C" void DateTime_System_IConvertible_ToDecimal_m7730 ();
extern "C" void DateTime_System_IConvertible_ToDouble_m7731 ();
extern "C" void DateTime_System_IConvertible_ToInt16_m7732 ();
extern "C" void DateTime_System_IConvertible_ToInt32_m7733 ();
extern "C" void DateTime_System_IConvertible_ToInt64_m7734 ();
extern "C" void DateTime_System_IConvertible_ToSByte_m7735 ();
extern "C" void DateTime_System_IConvertible_ToSingle_m7736 ();
extern "C" void DateTime_System_IConvertible_ToType_m7737 ();
extern "C" void DateTime_System_IConvertible_ToUInt16_m7738 ();
extern "C" void DateTime_System_IConvertible_ToUInt32_m7739 ();
extern "C" void DateTime_System_IConvertible_ToUInt64_m7740 ();
extern "C" void DateTime_AbsoluteDays_m7741 ();
extern "C" void DateTime_FromTicks_m7742 ();
extern "C" void DateTime_get_Month_m7743 ();
extern "C" void DateTime_get_Day_m7744 ();
extern "C" void DateTime_get_DayOfWeek_m7745 ();
extern "C" void DateTime_get_Hour_m7746 ();
extern "C" void DateTime_get_Minute_m7747 ();
extern "C" void DateTime_get_Second_m7748 ();
extern "C" void DateTime_GetTimeMonotonic_m7749 ();
extern "C" void DateTime_GetNow_m7750 ();
extern "C" void DateTime_get_Now_m1179 ();
extern "C" void DateTime_get_Ticks_m3342 ();
extern "C" void DateTime_get_Today_m7751 ();
extern "C" void DateTime_get_UtcNow_m3317 ();
extern "C" void DateTime_get_Year_m7752 ();
extern "C" void DateTime_get_Kind_m7753 ();
extern "C" void DateTime_Add_m7754 ();
extern "C" void DateTime_AddTicks_m7755 ();
extern "C" void DateTime_AddMilliseconds_m2267 ();
extern "C" void DateTime_AddSeconds_m1312 ();
extern "C" void DateTime_Compare_m7756 ();
extern "C" void DateTime_CompareTo_m7757 ();
extern "C" void DateTime_CompareTo_m7758 ();
extern "C" void DateTime_Equals_m7759 ();
extern "C" void DateTime_FromBinary_m7760 ();
extern "C" void DateTime_SpecifyKind_m7761 ();
extern "C" void DateTime_DaysInMonth_m7762 ();
extern "C" void DateTime_Equals_m7763 ();
extern "C" void DateTime_CheckDateTimeKind_m7764 ();
extern "C" void DateTime_GetHashCode_m7765 ();
extern "C" void DateTime_IsLeapYear_m7766 ();
extern "C" void DateTime_Parse_m7767 ();
extern "C" void DateTime_Parse_m7768 ();
extern "C" void DateTime_CoreParse_m7769 ();
extern "C" void DateTime_YearMonthDayFormats_m7770 ();
extern "C" void DateTime__ParseNumber_m7771 ();
extern "C" void DateTime__ParseEnum_m7772 ();
extern "C" void DateTime__ParseString_m7773 ();
extern "C" void DateTime__ParseAmPm_m7774 ();
extern "C" void DateTime__ParseTimeSeparator_m7775 ();
extern "C" void DateTime__ParseDateSeparator_m7776 ();
extern "C" void DateTime_IsLetter_m7777 ();
extern "C" void DateTime__DoParse_m7778 ();
extern "C" void DateTime_ParseExact_m3293 ();
extern "C" void DateTime_ParseExact_m7779 ();
extern "C" void DateTime_CheckStyle_m7780 ();
extern "C" void DateTime_ParseExact_m7781 ();
extern "C" void DateTime_Subtract_m7782 ();
extern "C" void DateTime_ToString_m7783 ();
extern "C" void DateTime_ToString_m7784 ();
extern "C" void DateTime_ToString_m1299 ();
extern "C" void DateTime_ToLocalTime_m2314 ();
extern "C" void DateTime_ToUniversalTime_m1298 ();
extern "C" void DateTime_op_Addition_m7785 ();
extern "C" void DateTime_op_Equality_m7786 ();
extern "C" void DateTime_op_GreaterThan_m2340 ();
extern "C" void DateTime_op_GreaterThanOrEqual_m2268 ();
extern "C" void DateTime_op_Inequality_m7787 ();
extern "C" void DateTime_op_LessThan_m2339 ();
extern "C" void DateTime_op_LessThanOrEqual_m2338 ();
extern "C" void DateTime_op_Subtraction_m7788 ();
extern "C" void DateTimeOffset__ctor_m7789 ();
extern "C" void DateTimeOffset__ctor_m7790 ();
extern "C" void DateTimeOffset__ctor_m7791 ();
extern "C" void DateTimeOffset__ctor_m7792 ();
extern "C" void DateTimeOffset__cctor_m7793 ();
extern "C" void DateTimeOffset_System_IComparable_CompareTo_m7794 ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m7795 ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7796 ();
extern "C" void DateTimeOffset_CompareTo_m7797 ();
extern "C" void DateTimeOffset_Equals_m7798 ();
extern "C" void DateTimeOffset_Equals_m7799 ();
extern "C" void DateTimeOffset_GetHashCode_m7800 ();
extern "C" void DateTimeOffset_ToString_m7801 ();
extern "C" void DateTimeOffset_ToString_m1301 ();
extern "C" void DateTimeOffset_ToUniversalTime_m1300 ();
extern "C" void DateTimeOffset_get_DateTime_m7802 ();
extern "C" void DateTimeOffset_get_Offset_m7803 ();
extern "C" void DateTimeOffset_get_UtcDateTime_m7804 ();
extern "C" void DateTimeUtils_CountRepeat_m7805 ();
extern "C" void DateTimeUtils_ZeroPad_m7806 ();
extern "C" void DateTimeUtils_ParseQuotedString_m7807 ();
extern "C" void DateTimeUtils_GetStandardPattern_m7808 ();
extern "C" void DateTimeUtils_GetStandardPattern_m7809 ();
extern "C" void DateTimeUtils_ToString_m7810 ();
extern "C" void DateTimeUtils_ToString_m7811 ();
extern "C" void DelegateEntry__ctor_m7812 ();
extern "C" void DelegateEntry_DeserializeDelegate_m7813 ();
extern "C" void DelegateSerializationHolder__ctor_m7814 ();
extern "C" void DelegateSerializationHolder_GetDelegateData_m7815 ();
extern "C" void DelegateSerializationHolder_GetObjectData_m7816 ();
extern "C" void DelegateSerializationHolder_GetRealObject_m7817 ();
extern "C" void DivideByZeroException__ctor_m7818 ();
extern "C" void DivideByZeroException__ctor_m7819 ();
extern "C" void DllNotFoundException__ctor_m7820 ();
extern "C" void DllNotFoundException__ctor_m7821 ();
extern "C" void EntryPointNotFoundException__ctor_m7822 ();
extern "C" void EntryPointNotFoundException__ctor_m7823 ();
extern "C" void SByteComparer__ctor_m7824 ();
extern "C" void SByteComparer_Compare_m7825 ();
extern "C" void SByteComparer_Compare_m7826 ();
extern "C" void ShortComparer__ctor_m7827 ();
extern "C" void ShortComparer_Compare_m7828 ();
extern "C" void ShortComparer_Compare_m7829 ();
extern "C" void IntComparer__ctor_m7830 ();
extern "C" void IntComparer_Compare_m7831 ();
extern "C" void IntComparer_Compare_m7832 ();
extern "C" void LongComparer__ctor_m7833 ();
extern "C" void LongComparer_Compare_m7834 ();
extern "C" void LongComparer_Compare_m7835 ();
extern "C" void MonoEnumInfo__ctor_m7836 ();
extern "C" void MonoEnumInfo__cctor_m7837 ();
extern "C" void MonoEnumInfo_get_enum_info_m7838 ();
extern "C" void MonoEnumInfo_get_Cache_m7839 ();
extern "C" void MonoEnumInfo_GetInfo_m7840 ();
extern "C" void Environment_get_SocketSecurityEnabled_m7841 ();
extern "C" void Environment_get_NewLine_m2299 ();
extern "C" void Environment_get_Platform_m7842 ();
extern "C" void Environment_GetOSVersionString_m7843 ();
extern "C" void Environment_get_OSVersion_m7844 ();
extern "C" void Environment_get_TickCount_m1246 ();
extern "C" void Environment_internalGetEnvironmentVariable_m7845 ();
extern "C" void Environment_GetEnvironmentVariable_m3340 ();
extern "C" void Environment_GetWindowsFolderPath_m7846 ();
extern "C" void Environment_GetFolderPath_m3327 ();
extern "C" void Environment_ReadXdgUserDir_m7847 ();
extern "C" void Environment_InternalGetFolderPath_m7848 ();
extern "C" void Environment_get_IsRunningOnWindows_m7849 ();
extern "C" void Environment_GetMachineConfigPath_m7850 ();
extern "C" void Environment_internalGetHome_m7851 ();
extern "C" void EventArgs__ctor_m7852 ();
extern "C" void EventArgs__cctor_m7853 ();
extern "C" void ExecutionEngineException__ctor_m7854 ();
extern "C" void ExecutionEngineException__ctor_m7855 ();
extern "C" void FieldAccessException__ctor_m7856 ();
extern "C" void FieldAccessException__ctor_m7857 ();
extern "C" void FieldAccessException__ctor_m7858 ();
extern "C" void FlagsAttribute__ctor_m7859 ();
extern "C" void FormatException__ctor_m7860 ();
extern "C" void FormatException__ctor_m1237 ();
extern "C" void FormatException__ctor_m2407 ();
extern "C" void GC_SuppressFinalize_m2457 ();
extern "C" void Guid__ctor_m7861 ();
extern "C" void Guid__ctor_m7862 ();
extern "C" void Guid__cctor_m7863 ();
extern "C" void Guid_CheckNull_m7864 ();
extern "C" void Guid_CheckLength_m7865 ();
extern "C" void Guid_CheckArray_m7866 ();
extern "C" void Guid_Compare_m7867 ();
extern "C" void Guid_CompareTo_m7868 ();
extern "C" void Guid_Equals_m7869 ();
extern "C" void Guid_CompareTo_m7870 ();
extern "C" void Guid_Equals_m7871 ();
extern "C" void Guid_GetHashCode_m7872 ();
extern "C" void Guid_ToHex_m7873 ();
extern "C" void Guid_NewGuid_m7874 ();
extern "C" void Guid_AppendInt_m7875 ();
extern "C" void Guid_AppendShort_m7876 ();
extern "C" void Guid_AppendByte_m7877 ();
extern "C" void Guid_BaseToString_m7878 ();
extern "C" void Guid_ToString_m7879 ();
extern "C" void Guid_ToString_m1302 ();
extern "C" void Guid_ToString_m7880 ();
extern "C" void IndexOutOfRangeException__ctor_m7881 ();
extern "C" void IndexOutOfRangeException__ctor_m1197 ();
extern "C" void IndexOutOfRangeException__ctor_m7882 ();
extern "C" void InvalidCastException__ctor_m7883 ();
extern "C" void InvalidCastException__ctor_m7884 ();
extern "C" void InvalidCastException__ctor_m7885 ();
extern "C" void InvalidOperationException__ctor_m2238 ();
extern "C" void InvalidOperationException__ctor_m2232 ();
extern "C" void InvalidOperationException__ctor_m7886 ();
extern "C" void InvalidOperationException__ctor_m7887 ();
extern "C" void Math_Abs_m7888 ();
extern "C" void Math_Abs_m7889 ();
extern "C" void Math_Abs_m7890 ();
extern "C" void Math_Floor_m7891 ();
extern "C" void Math_Max_m3298 ();
extern "C" void Math_Min_m2456 ();
extern "C" void Math_Round_m7892 ();
extern "C" void Math_Round_m7893 ();
extern "C" void Math_Pow_m7894 ();
extern "C" void Math_Sqrt_m7895 ();
extern "C" void MemberAccessException__ctor_m7896 ();
extern "C" void MemberAccessException__ctor_m7897 ();
extern "C" void MemberAccessException__ctor_m7898 ();
extern "C" void MethodAccessException__ctor_m7899 ();
extern "C" void MethodAccessException__ctor_m7900 ();
extern "C" void MissingFieldException__ctor_m7901 ();
extern "C" void MissingFieldException__ctor_m7902 ();
extern "C" void MissingFieldException__ctor_m7903 ();
extern "C" void MissingFieldException_get_Message_m7904 ();
extern "C" void MissingMemberException__ctor_m7905 ();
extern "C" void MissingMemberException__ctor_m7906 ();
extern "C" void MissingMemberException__ctor_m7907 ();
extern "C" void MissingMemberException__ctor_m7908 ();
extern "C" void MissingMemberException_GetObjectData_m7909 ();
extern "C" void MissingMemberException_get_Message_m7910 ();
extern "C" void MissingMethodException__ctor_m7911 ();
extern "C" void MissingMethodException__ctor_m7912 ();
extern "C" void MissingMethodException__ctor_m7913 ();
extern "C" void MissingMethodException__ctor_m7914 ();
extern "C" void MissingMethodException_get_Message_m7915 ();
extern "C" void MonoAsyncCall__ctor_m7916 ();
extern "C" void AttributeInfo__ctor_m7917 ();
extern "C" void AttributeInfo_get_Usage_m7918 ();
extern "C" void AttributeInfo_get_InheritanceLevel_m7919 ();
extern "C" void MonoCustomAttrs__cctor_m7920 ();
extern "C" void MonoCustomAttrs_IsUserCattrProvider_m7921 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesInternal_m7922 ();
extern "C" void MonoCustomAttrs_GetPseudoCustomAttributes_m7923 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesBase_m7924 ();
extern "C" void MonoCustomAttrs_GetCustomAttribute_m7925 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m7926 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m7927 ();
extern "C" void MonoCustomAttrs_IsDefined_m7928 ();
extern "C" void MonoCustomAttrs_IsDefinedInternal_m7929 ();
extern "C" void MonoCustomAttrs_GetBasePropertyDefinition_m7930 ();
extern "C" void MonoCustomAttrs_GetBase_m7931 ();
extern "C" void MonoCustomAttrs_RetrieveAttributeUsage_m7932 ();
extern "C" void MonoTouchAOTHelper__cctor_m7933 ();
extern "C" void MonoTypeInfo__ctor_m7934 ();
extern "C" void MonoType_get_attributes_m7935 ();
extern "C" void MonoType_GetDefaultConstructor_m7936 ();
extern "C" void MonoType_GetAttributeFlagsImpl_m7937 ();
extern "C" void MonoType_GetConstructorImpl_m7938 ();
extern "C" void MonoType_GetConstructors_internal_m7939 ();
extern "C" void MonoType_GetConstructors_m7940 ();
extern "C" void MonoType_InternalGetEvent_m7941 ();
extern "C" void MonoType_GetEvent_m7942 ();
extern "C" void MonoType_GetField_m7943 ();
extern "C" void MonoType_GetFields_internal_m7944 ();
extern "C" void MonoType_GetFields_m7945 ();
extern "C" void MonoType_GetInterfaces_m7946 ();
extern "C" void MonoType_GetMethodsByName_m7947 ();
extern "C" void MonoType_GetMethods_m7948 ();
extern "C" void MonoType_GetMethodImpl_m7949 ();
extern "C" void MonoType_GetPropertiesByName_m7950 ();
extern "C" void MonoType_GetProperties_m7951 ();
extern "C" void MonoType_GetPropertyImpl_m7952 ();
extern "C" void MonoType_HasElementTypeImpl_m7953 ();
extern "C" void MonoType_IsArrayImpl_m7954 ();
extern "C" void MonoType_IsByRefImpl_m7955 ();
extern "C" void MonoType_IsPointerImpl_m7956 ();
extern "C" void MonoType_IsPrimitiveImpl_m7957 ();
extern "C" void MonoType_IsSubclassOf_m7958 ();
extern "C" void MonoType_InvokeMember_m7959 ();
extern "C" void MonoType_GetElementType_m7960 ();
extern "C" void MonoType_get_UnderlyingSystemType_m7961 ();
extern "C" void MonoType_get_Assembly_m7962 ();
extern "C" void MonoType_get_AssemblyQualifiedName_m7963 ();
extern "C" void MonoType_getFullName_m7964 ();
extern "C" void MonoType_get_BaseType_m7965 ();
extern "C" void MonoType_get_FullName_m7966 ();
extern "C" void MonoType_IsDefined_m7967 ();
extern "C" void MonoType_GetCustomAttributes_m7968 ();
extern "C" void MonoType_GetCustomAttributes_m7969 ();
extern "C" void MonoType_get_MemberType_m7970 ();
extern "C" void MonoType_get_Name_m7971 ();
extern "C" void MonoType_get_Namespace_m7972 ();
extern "C" void MonoType_get_Module_m7973 ();
extern "C" void MonoType_get_DeclaringType_m7974 ();
extern "C" void MonoType_get_ReflectedType_m7975 ();
extern "C" void MonoType_get_TypeHandle_m7976 ();
extern "C" void MonoType_GetObjectData_m7977 ();
extern "C" void MonoType_ToString_m7978 ();
extern "C" void MonoType_GetGenericArguments_m7979 ();
extern "C" void MonoType_get_ContainsGenericParameters_m7980 ();
extern "C" void MonoType_get_IsGenericParameter_m7981 ();
extern "C" void MonoType_GetGenericTypeDefinition_m7982 ();
extern "C" void MonoType_CheckMethodSecurity_m7983 ();
extern "C" void MonoType_ReorderParamArrayArguments_m7984 ();
extern "C" void MulticastNotSupportedException__ctor_m7985 ();
extern "C" void MulticastNotSupportedException__ctor_m7986 ();
extern "C" void MulticastNotSupportedException__ctor_m7987 ();
extern "C" void NonSerializedAttribute__ctor_m7988 ();
extern "C" void NotImplementedException__ctor_m7989 ();
extern "C" void NotImplementedException__ctor_m2279 ();
extern "C" void NotImplementedException__ctor_m7990 ();
extern "C" void NotSupportedException__ctor_m2278 ();
extern "C" void NotSupportedException__ctor_m2249 ();
extern "C" void NotSupportedException__ctor_m7991 ();
extern "C" void NullReferenceException__ctor_m7992 ();
extern "C" void NullReferenceException__ctor_m1160 ();
extern "C" void NullReferenceException__ctor_m7993 ();
extern "C" void CustomInfo__ctor_m7994 ();
extern "C" void CustomInfo_GetActiveSection_m7995 ();
extern "C" void CustomInfo_Parse_m7996 ();
extern "C" void CustomInfo_Format_m7997 ();
extern "C" void NumberFormatter__ctor_m7998 ();
extern "C" void NumberFormatter__cctor_m7999 ();
extern "C" void NumberFormatter_GetFormatterTables_m8000 ();
extern "C" void NumberFormatter_GetTenPowerOf_m8001 ();
extern "C" void NumberFormatter_InitDecHexDigits_m8002 ();
extern "C" void NumberFormatter_InitDecHexDigits_m8003 ();
extern "C" void NumberFormatter_InitDecHexDigits_m8004 ();
extern "C" void NumberFormatter_FastToDecHex_m8005 ();
extern "C" void NumberFormatter_ToDecHex_m8006 ();
extern "C" void NumberFormatter_FastDecHexLen_m8007 ();
extern "C" void NumberFormatter_DecHexLen_m8008 ();
extern "C" void NumberFormatter_DecHexLen_m8009 ();
extern "C" void NumberFormatter_ScaleOrder_m8010 ();
extern "C" void NumberFormatter_InitialFloatingPrecision_m8011 ();
extern "C" void NumberFormatter_ParsePrecision_m8012 ();
extern "C" void NumberFormatter_Init_m8013 ();
extern "C" void NumberFormatter_InitHex_m8014 ();
extern "C" void NumberFormatter_Init_m8015 ();
extern "C" void NumberFormatter_Init_m8016 ();
extern "C" void NumberFormatter_Init_m8017 ();
extern "C" void NumberFormatter_Init_m8018 ();
extern "C" void NumberFormatter_Init_m8019 ();
extern "C" void NumberFormatter_Init_m8020 ();
extern "C" void NumberFormatter_ResetCharBuf_m8021 ();
extern "C" void NumberFormatter_Resize_m8022 ();
extern "C" void NumberFormatter_Append_m8023 ();
extern "C" void NumberFormatter_Append_m8024 ();
extern "C" void NumberFormatter_Append_m8025 ();
extern "C" void NumberFormatter_GetNumberFormatInstance_m8026 ();
extern "C" void NumberFormatter_set_CurrentCulture_m8027 ();
extern "C" void NumberFormatter_get_IntegerDigits_m8028 ();
extern "C" void NumberFormatter_get_DecimalDigits_m8029 ();
extern "C" void NumberFormatter_get_IsFloatingSource_m8030 ();
extern "C" void NumberFormatter_get_IsZero_m8031 ();
extern "C" void NumberFormatter_get_IsZeroInteger_m8032 ();
extern "C" void NumberFormatter_RoundPos_m8033 ();
extern "C" void NumberFormatter_RoundDecimal_m8034 ();
extern "C" void NumberFormatter_RoundBits_m8035 ();
extern "C" void NumberFormatter_RemoveTrailingZeros_m8036 ();
extern "C" void NumberFormatter_AddOneToDecHex_m8037 ();
extern "C" void NumberFormatter_AddOneToDecHex_m8038 ();
extern "C" void NumberFormatter_CountTrailingZeros_m8039 ();
extern "C" void NumberFormatter_CountTrailingZeros_m8040 ();
extern "C" void NumberFormatter_GetInstance_m8041 ();
extern "C" void NumberFormatter_Release_m8042 ();
extern "C" void NumberFormatter_SetThreadCurrentCulture_m8043 ();
extern "C" void NumberFormatter_NumberToString_m8044 ();
extern "C" void NumberFormatter_NumberToString_m8045 ();
extern "C" void NumberFormatter_NumberToString_m8046 ();
extern "C" void NumberFormatter_NumberToString_m8047 ();
extern "C" void NumberFormatter_NumberToString_m8048 ();
extern "C" void NumberFormatter_NumberToString_m8049 ();
extern "C" void NumberFormatter_NumberToString_m8050 ();
extern "C" void NumberFormatter_NumberToString_m8051 ();
extern "C" void NumberFormatter_NumberToString_m8052 ();
extern "C" void NumberFormatter_NumberToString_m8053 ();
extern "C" void NumberFormatter_NumberToString_m8054 ();
extern "C" void NumberFormatter_NumberToString_m8055 ();
extern "C" void NumberFormatter_NumberToString_m8056 ();
extern "C" void NumberFormatter_NumberToString_m8057 ();
extern "C" void NumberFormatter_NumberToString_m8058 ();
extern "C" void NumberFormatter_NumberToString_m8059 ();
extern "C" void NumberFormatter_NumberToString_m8060 ();
extern "C" void NumberFormatter_FastIntegerToString_m8061 ();
extern "C" void NumberFormatter_IntegerToString_m8062 ();
extern "C" void NumberFormatter_NumberToString_m8063 ();
extern "C" void NumberFormatter_FormatCurrency_m8064 ();
extern "C" void NumberFormatter_FormatDecimal_m8065 ();
extern "C" void NumberFormatter_FormatHexadecimal_m8066 ();
extern "C" void NumberFormatter_FormatFixedPoint_m8067 ();
extern "C" void NumberFormatter_FormatRoundtrip_m8068 ();
extern "C" void NumberFormatter_FormatRoundtrip_m8069 ();
extern "C" void NumberFormatter_FormatGeneral_m8070 ();
extern "C" void NumberFormatter_FormatNumber_m8071 ();
extern "C" void NumberFormatter_FormatPercent_m8072 ();
extern "C" void NumberFormatter_FormatExponential_m8073 ();
extern "C" void NumberFormatter_FormatExponential_m8074 ();
extern "C" void NumberFormatter_FormatCustom_m8075 ();
extern "C" void NumberFormatter_ZeroTrimEnd_m8076 ();
extern "C" void NumberFormatter_IsZeroOnly_m8077 ();
extern "C" void NumberFormatter_AppendNonNegativeNumber_m8078 ();
extern "C" void NumberFormatter_AppendIntegerString_m8079 ();
extern "C" void NumberFormatter_AppendIntegerString_m8080 ();
extern "C" void NumberFormatter_AppendDecimalString_m8081 ();
extern "C" void NumberFormatter_AppendDecimalString_m8082 ();
extern "C" void NumberFormatter_AppendIntegerStringWithGroupSeparator_m8083 ();
extern "C" void NumberFormatter_AppendExponent_m8084 ();
extern "C" void NumberFormatter_AppendOneDigit_m8085 ();
extern "C" void NumberFormatter_FastAppendDigits_m8086 ();
extern "C" void NumberFormatter_AppendDigits_m8087 ();
extern "C" void NumberFormatter_AppendDigits_m8088 ();
extern "C" void NumberFormatter_Multiply10_m8089 ();
extern "C" void NumberFormatter_Divide10_m8090 ();
extern "C" void NumberFormatter_GetClone_m8091 ();
extern "C" void ObjectDisposedException__ctor_m2459 ();
extern "C" void ObjectDisposedException__ctor_m8092 ();
extern "C" void ObjectDisposedException__ctor_m8093 ();
extern "C" void ObjectDisposedException_get_Message_m8094 ();
extern "C" void ObjectDisposedException_GetObjectData_m8095 ();
extern "C" void OperatingSystem__ctor_m8096 ();
extern "C" void OperatingSystem_get_Platform_m8097 ();
extern "C" void OperatingSystem_GetObjectData_m8098 ();
extern "C" void OperatingSystem_ToString_m8099 ();
extern "C" void OutOfMemoryException__ctor_m8100 ();
extern "C" void OutOfMemoryException__ctor_m8101 ();
extern "C" void OverflowException__ctor_m8102 ();
extern "C" void OverflowException__ctor_m8103 ();
extern "C" void OverflowException__ctor_m8104 ();
extern "C" void Random__ctor_m8105 ();
extern "C" void Random__ctor_m1247 ();
extern "C" void RankException__ctor_m8106 ();
extern "C" void RankException__ctor_m8107 ();
extern "C" void RankException__ctor_m8108 ();
extern "C" void ResolveEventArgs__ctor_m8109 ();
extern "C" void RuntimeMethodHandle__ctor_m8110 ();
extern "C" void RuntimeMethodHandle__ctor_m8111 ();
extern "C" void RuntimeMethodHandle_get_Value_m8112 ();
extern "C" void RuntimeMethodHandle_GetObjectData_m8113 ();
extern "C" void RuntimeMethodHandle_Equals_m8114 ();
extern "C" void RuntimeMethodHandle_GetHashCode_m8115 ();
extern "C" void StringComparer__ctor_m8116 ();
extern "C" void StringComparer__cctor_m8117 ();
extern "C" void StringComparer_get_InvariantCultureIgnoreCase_m2273 ();
extern "C" void StringComparer_get_OrdinalIgnoreCase_m1189 ();
extern "C" void StringComparer_Compare_m8118 ();
extern "C" void StringComparer_Equals_m8119 ();
extern "C" void StringComparer_GetHashCode_m8120 ();
extern "C" void CultureAwareComparer__ctor_m8121 ();
extern "C" void CultureAwareComparer_Compare_m8122 ();
extern "C" void CultureAwareComparer_Equals_m8123 ();
extern "C" void CultureAwareComparer_GetHashCode_m8124 ();
extern "C" void OrdinalComparer__ctor_m8125 ();
extern "C" void OrdinalComparer_Compare_m8126 ();
extern "C" void OrdinalComparer_Equals_m8127 ();
extern "C" void OrdinalComparer_GetHashCode_m8128 ();
extern "C" void SystemException__ctor_m8129 ();
extern "C" void SystemException__ctor_m2376 ();
extern "C" void SystemException__ctor_m8130 ();
extern "C" void SystemException__ctor_m8131 ();
extern "C" void ThreadStaticAttribute__ctor_m8132 ();
extern "C" void TimeSpan__ctor_m8133 ();
extern "C" void TimeSpan__ctor_m8134 ();
extern "C" void TimeSpan__ctor_m8135 ();
extern "C" void TimeSpan__cctor_m8136 ();
extern "C" void TimeSpan_CalculateTicks_m8137 ();
extern "C" void TimeSpan_get_Days_m8138 ();
extern "C" void TimeSpan_get_Hours_m8139 ();
extern "C" void TimeSpan_get_Milliseconds_m8140 ();
extern "C" void TimeSpan_get_Minutes_m8141 ();
extern "C" void TimeSpan_get_Seconds_m8142 ();
extern "C" void TimeSpan_get_Ticks_m8143 ();
extern "C" void TimeSpan_get_TotalDays_m8144 ();
extern "C" void TimeSpan_get_TotalHours_m8145 ();
extern "C" void TimeSpan_get_TotalMilliseconds_m8146 ();
extern "C" void TimeSpan_get_TotalMinutes_m8147 ();
extern "C" void TimeSpan_get_TotalSeconds_m8148 ();
extern "C" void TimeSpan_Add_m8149 ();
extern "C" void TimeSpan_Compare_m8150 ();
extern "C" void TimeSpan_CompareTo_m8151 ();
extern "C" void TimeSpan_CompareTo_m8152 ();
extern "C" void TimeSpan_Equals_m8153 ();
extern "C" void TimeSpan_Duration_m8154 ();
extern "C" void TimeSpan_Equals_m8155 ();
extern "C" void TimeSpan_FromDays_m8156 ();
extern "C" void TimeSpan_FromHours_m8157 ();
extern "C" void TimeSpan_FromMinutes_m8158 ();
extern "C" void TimeSpan_FromSeconds_m8159 ();
extern "C" void TimeSpan_FromMilliseconds_m8160 ();
extern "C" void TimeSpan_From_m8161 ();
extern "C" void TimeSpan_GetHashCode_m8162 ();
extern "C" void TimeSpan_Negate_m8163 ();
extern "C" void TimeSpan_Subtract_m8164 ();
extern "C" void TimeSpan_ToString_m8165 ();
extern "C" void TimeSpan_op_Addition_m8166 ();
extern "C" void TimeSpan_op_Equality_m8167 ();
extern "C" void TimeSpan_op_GreaterThan_m8168 ();
extern "C" void TimeSpan_op_GreaterThanOrEqual_m8169 ();
extern "C" void TimeSpan_op_Inequality_m8170 ();
extern "C" void TimeSpan_op_LessThan_m8171 ();
extern "C" void TimeSpan_op_LessThanOrEqual_m8172 ();
extern "C" void TimeSpan_op_Subtraction_m8173 ();
extern "C" void TimeZone__ctor_m8174 ();
extern "C" void TimeZone__cctor_m8175 ();
extern "C" void TimeZone_get_CurrentTimeZone_m8176 ();
extern "C" void TimeZone_IsDaylightSavingTime_m8177 ();
extern "C" void TimeZone_IsDaylightSavingTime_m8178 ();
extern "C" void TimeZone_ToLocalTime_m8179 ();
extern "C" void TimeZone_ToUniversalTime_m8180 ();
extern "C" void TimeZone_GetLocalTimeDiff_m8181 ();
extern "C" void TimeZone_GetLocalTimeDiff_m8182 ();
extern "C" void CurrentSystemTimeZone__ctor_m8183 ();
extern "C" void CurrentSystemTimeZone__ctor_m8184 ();
extern "C" void CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8185 ();
extern "C" void CurrentSystemTimeZone_GetTimeZoneData_m8186 ();
extern "C" void CurrentSystemTimeZone_GetDaylightChanges_m8187 ();
extern "C" void CurrentSystemTimeZone_GetUtcOffset_m8188 ();
extern "C" void CurrentSystemTimeZone_OnDeserialization_m8189 ();
extern "C" void CurrentSystemTimeZone_GetDaylightTimeFromData_m8190 ();
extern "C" void TypeInitializationException__ctor_m8191 ();
extern "C" void TypeInitializationException_GetObjectData_m8192 ();
extern "C" void TypeLoadException__ctor_m8193 ();
extern "C" void TypeLoadException__ctor_m8194 ();
extern "C" void TypeLoadException__ctor_m8195 ();
extern "C" void TypeLoadException_get_Message_m8196 ();
extern "C" void TypeLoadException_GetObjectData_m8197 ();
extern "C" void UnauthorizedAccessException__ctor_m8198 ();
extern "C" void UnauthorizedAccessException__ctor_m8199 ();
extern "C" void UnauthorizedAccessException__ctor_m8200 ();
extern "C" void UnhandledExceptionEventArgs__ctor_m8201 ();
extern "C" void UnhandledExceptionEventArgs_get_ExceptionObject_m8202 ();
extern "C" void UnhandledExceptionEventArgs_get_IsTerminating_m8203 ();
extern "C" void UnitySerializationHolder__ctor_m8204 ();
extern "C" void UnitySerializationHolder_GetTypeData_m8205 ();
extern "C" void UnitySerializationHolder_GetDBNullData_m8206 ();
extern "C" void UnitySerializationHolder_GetModuleData_m8207 ();
extern "C" void UnitySerializationHolder_GetObjectData_m8208 ();
extern "C" void UnitySerializationHolder_GetRealObject_m8209 ();
extern "C" void Version__ctor_m8210 ();
extern "C" void Version__ctor_m8211 ();
extern "C" void Version__ctor_m2258 ();
extern "C" void Version__ctor_m8212 ();
extern "C" void Version_CheckedSet_m8213 ();
extern "C" void Version_get_Build_m8214 ();
extern "C" void Version_get_Major_m8215 ();
extern "C" void Version_get_Minor_m8216 ();
extern "C" void Version_get_Revision_m8217 ();
extern "C" void Version_CompareTo_m8218 ();
extern "C" void Version_Equals_m8219 ();
extern "C" void Version_CompareTo_m8220 ();
extern "C" void Version_Equals_m8221 ();
extern "C" void Version_GetHashCode_m8222 ();
extern "C" void Version_ToString_m8223 ();
extern "C" void Version_CreateFromString_m8224 ();
extern "C" void Version_op_Equality_m8225 ();
extern "C" void Version_op_Inequality_m8226 ();
extern "C" void WeakReference__ctor_m8227 ();
extern "C" void WeakReference__ctor_m8228 ();
extern "C" void WeakReference__ctor_m8229 ();
extern "C" void WeakReference__ctor_m8230 ();
extern "C" void WeakReference_AllocateHandle_m8231 ();
extern "C" void WeakReference_get_Target_m8232 ();
extern "C" void WeakReference_get_TrackResurrection_m8233 ();
extern "C" void WeakReference_Finalize_m8234 ();
extern "C" void WeakReference_GetObjectData_m8235 ();
extern "C" void PrimalityTest__ctor_m8236 ();
extern "C" void PrimalityTest_Invoke_m8237 ();
extern "C" void PrimalityTest_BeginInvoke_m8238 ();
extern "C" void PrimalityTest_EndInvoke_m8239 ();
extern "C" void MemberFilter__ctor_m8240 ();
extern "C" void MemberFilter_Invoke_m8241 ();
extern "C" void MemberFilter_BeginInvoke_m8242 ();
extern "C" void MemberFilter_EndInvoke_m8243 ();
extern "C" void TypeFilter__ctor_m8244 ();
extern "C" void TypeFilter_Invoke_m8245 ();
extern "C" void TypeFilter_BeginInvoke_m8246 ();
extern "C" void TypeFilter_EndInvoke_m8247 ();
extern "C" void HeaderHandler__ctor_m8248 ();
extern "C" void HeaderHandler_Invoke_m8249 ();
extern "C" void HeaderHandler_BeginInvoke_m8250 ();
extern "C" void HeaderHandler_EndInvoke_m8251 ();
extern "C" void ThreadStart__ctor_m8252 ();
extern "C" void ThreadStart_Invoke_m8253 ();
extern "C" void ThreadStart_BeginInvoke_m8254 ();
extern "C" void ThreadStart_EndInvoke_m8255 ();
extern "C" void TimerCallback__ctor_m8256 ();
extern "C" void TimerCallback_Invoke_m8257 ();
extern "C" void TimerCallback_BeginInvoke_m8258 ();
extern "C" void TimerCallback_EndInvoke_m8259 ();
extern "C" void WaitCallback__ctor_m8260 ();
extern "C" void WaitCallback_Invoke_m8261 ();
extern "C" void WaitCallback_BeginInvoke_m8262 ();
extern "C" void WaitCallback_EndInvoke_m8263 ();
extern "C" void AppDomainInitializer__ctor_m8264 ();
extern "C" void AppDomainInitializer_Invoke_m8265 ();
extern "C" void AppDomainInitializer_BeginInvoke_m8266 ();
extern "C" void AppDomainInitializer_EndInvoke_m8267 ();
extern "C" void AssemblyLoadEventHandler__ctor_m8268 ();
extern "C" void AssemblyLoadEventHandler_Invoke_m8269 ();
extern "C" void AssemblyLoadEventHandler_BeginInvoke_m8270 ();
extern "C" void AssemblyLoadEventHandler_EndInvoke_m8271 ();
extern "C" void EventHandler__ctor_m8272 ();
extern "C" void EventHandler_Invoke_m8273 ();
extern "C" void EventHandler_BeginInvoke_m8274 ();
extern "C" void EventHandler_EndInvoke_m8275 ();
extern "C" void ResolveEventHandler__ctor_m8276 ();
extern "C" void ResolveEventHandler_Invoke_m8277 ();
extern "C" void ResolveEventHandler_BeginInvoke_m8278 ();
extern "C" void ResolveEventHandler_EndInvoke_m8279 ();
extern "C" void UnhandledExceptionEventHandler__ctor_m8280 ();
extern "C" void UnhandledExceptionEventHandler_Invoke_m8281 ();
extern "C" void UnhandledExceptionEventHandler_BeginInvoke_m8282 ();
extern "C" void UnhandledExceptionEventHandler_EndInvoke_m8283 ();
extern const methodPointerType g_MethodPointers[8216] = 
{
	AssetBundleCreateRequest__ctor_m0,
	AssetBundleCreateRequest_get_assetBundle_m1,
	AssetBundleCreateRequest_DisableCompatibilityChecks_m2,
	AssetBundleRequest__ctor_m3,
	AssetBundleRequest_get_asset_m4,
	AssetBundleRequest_get_allAssets_m5,
	AssetBundle_LoadAsset_m6,
	AssetBundle_LoadAsset_Internal_m7,
	AssetBundle_LoadAssetWithSubAssets_Internal_m8,
	LayerMask_get_value_m9,
	LayerMask_set_value_m10,
	LayerMask_LayerToName_m11,
	LayerMask_NameToLayer_m12,
	LayerMask_GetMask_m13,
	LayerMask_op_Implicit_m14,
	LayerMask_op_Implicit_m15,
	SystemInfo_get_deviceUniqueIdentifier_m16,
	WaitForSeconds__ctor_m17,
	WaitForFixedUpdate__ctor_m18,
	WaitForEndOfFrame__ctor_m19,
	Coroutine__ctor_m20,
	Coroutine_ReleaseCoroutine_m21,
	Coroutine_Finalize_m22,
	ScriptableObject__ctor_m23,
	ScriptableObject_Internal_CreateScriptableObject_m24,
	ScriptableObject_CreateInstance_m25,
	ScriptableObject_CreateInstance_m26,
	ScriptableObject_CreateInstanceFromType_m27,
	GameCenterPlatform__ctor_m28,
	GameCenterPlatform__cctor_m29,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m30,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m31,
	GameCenterPlatform_Internal_Authenticate_m32,
	GameCenterPlatform_Internal_Authenticated_m33,
	GameCenterPlatform_Internal_UserName_m34,
	GameCenterPlatform_Internal_UserID_m35,
	GameCenterPlatform_Internal_Underage_m36,
	GameCenterPlatform_Internal_UserImage_m37,
	GameCenterPlatform_Internal_LoadFriends_m38,
	GameCenterPlatform_Internal_LoadAchievementDescriptions_m39,
	GameCenterPlatform_Internal_LoadAchievements_m40,
	GameCenterPlatform_Internal_ReportProgress_m41,
	GameCenterPlatform_Internal_ReportScore_m42,
	GameCenterPlatform_Internal_LoadScores_m43,
	GameCenterPlatform_Internal_ShowAchievementsUI_m44,
	GameCenterPlatform_Internal_ShowLeaderboardUI_m45,
	GameCenterPlatform_Internal_LoadUsers_m46,
	GameCenterPlatform_Internal_ResetAllAchievements_m47,
	GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m48,
	GameCenterPlatform_ResetAllAchievements_m49,
	GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m50,
	GameCenterPlatform_ShowLeaderboardUI_m51,
	GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m52,
	GameCenterPlatform_ClearAchievementDescriptions_m53,
	GameCenterPlatform_SetAchievementDescription_m54,
	GameCenterPlatform_SetAchievementDescriptionImage_m55,
	GameCenterPlatform_TriggerAchievementDescriptionCallback_m56,
	GameCenterPlatform_AuthenticateCallbackWrapper_m57,
	GameCenterPlatform_ClearFriends_m58,
	GameCenterPlatform_SetFriends_m59,
	GameCenterPlatform_SetFriendImage_m60,
	GameCenterPlatform_TriggerFriendsCallbackWrapper_m61,
	GameCenterPlatform_AchievementCallbackWrapper_m62,
	GameCenterPlatform_ProgressCallbackWrapper_m63,
	GameCenterPlatform_ScoreCallbackWrapper_m64,
	GameCenterPlatform_ScoreLoaderCallbackWrapper_m65,
	GameCenterPlatform_get_localUser_m66,
	GameCenterPlatform_PopulateLocalUser_m67,
	GameCenterPlatform_LoadAchievementDescriptions_m68,
	GameCenterPlatform_ReportProgress_m69,
	GameCenterPlatform_LoadAchievements_m70,
	GameCenterPlatform_ReportScore_m71,
	GameCenterPlatform_LoadScores_m72,
	GameCenterPlatform_LoadScores_m73,
	GameCenterPlatform_LeaderboardCallbackWrapper_m74,
	GameCenterPlatform_GetLoading_m75,
	GameCenterPlatform_VerifyAuthentication_m76,
	GameCenterPlatform_ShowAchievementsUI_m77,
	GameCenterPlatform_ShowLeaderboardUI_m78,
	GameCenterPlatform_ClearUsers_m79,
	GameCenterPlatform_SetUser_m80,
	GameCenterPlatform_SetUserImage_m81,
	GameCenterPlatform_TriggerUsersCallbackWrapper_m82,
	GameCenterPlatform_LoadUsers_m83,
	GameCenterPlatform_SafeSetUserImage_m84,
	GameCenterPlatform_SafeClearArray_m85,
	GameCenterPlatform_CreateLeaderboard_m86,
	GameCenterPlatform_CreateAchievement_m87,
	GameCenterPlatform_TriggerResetAchievementCallback_m88,
	GcLeaderboard__ctor_m89,
	GcLeaderboard_Finalize_m90,
	GcLeaderboard_Contains_m91,
	GcLeaderboard_SetScores_m92,
	GcLeaderboard_SetLocalScore_m93,
	GcLeaderboard_SetMaxRange_m94,
	GcLeaderboard_SetTitle_m95,
	GcLeaderboard_Internal_LoadScores_m96,
	GcLeaderboard_Internal_LoadScoresWithUsers_m97,
	GcLeaderboard_Loading_m98,
	GcLeaderboard_Dispose_m99,
	BoneWeight_get_weight0_m100,
	BoneWeight_set_weight0_m101,
	BoneWeight_get_weight1_m102,
	BoneWeight_set_weight1_m103,
	BoneWeight_get_weight2_m104,
	BoneWeight_set_weight2_m105,
	BoneWeight_get_weight3_m106,
	BoneWeight_set_weight3_m107,
	BoneWeight_get_boneIndex0_m108,
	BoneWeight_set_boneIndex0_m109,
	BoneWeight_get_boneIndex1_m110,
	BoneWeight_set_boneIndex1_m111,
	BoneWeight_get_boneIndex2_m112,
	BoneWeight_set_boneIndex2_m113,
	BoneWeight_get_boneIndex3_m114,
	BoneWeight_set_boneIndex3_m115,
	BoneWeight_GetHashCode_m116,
	BoneWeight_Equals_m117,
	BoneWeight_op_Equality_m118,
	BoneWeight_op_Inequality_m119,
	Screen_get_width_m120,
	Screen_get_height_m121,
	Texture__ctor_m122,
	Texture2D__ctor_m123,
	Texture2D_Internal_Create_m124,
	GUILayer_HitTest_m125,
	GUILayer_INTERNAL_CALL_HitTest_m126,
	GradientColorKey__ctor_m127,
	GradientAlphaKey__ctor_m128,
	Gradient__ctor_m129,
	Gradient_Init_m130,
	Gradient_Cleanup_m131,
	Gradient_Finalize_m132,
	ScrollViewState__ctor_m133,
	WindowFunction__ctor_m134,
	WindowFunction_Invoke_m135,
	WindowFunction_BeginInvoke_m136,
	WindowFunction_EndInvoke_m137,
	GUI__cctor_m138,
	GUI_set_nextScrollStepTime_m139,
	GUI_set_skin_m140,
	GUI_get_skin_m141,
	GUI_set_changed_m142,
	GUI_CallWindowDelegate_m143,
	GUILayout_Width_m144,
	GUILayout_Height_m145,
	LayoutCache__ctor_m146,
	GUILayoutUtility__cctor_m147,
	GUILayoutUtility_SelectIDList_m148,
	GUILayoutUtility_Begin_m149,
	GUILayoutUtility_BeginWindow_m150,
	GUILayoutUtility_Layout_m151,
	GUILayoutUtility_LayoutFromEditorWindow_m152,
	GUILayoutUtility_LayoutFreeGroup_m153,
	GUILayoutUtility_LayoutSingleGroup_m154,
	GUILayoutUtility_Internal_GetWindowRect_m155,
	GUILayoutUtility_Internal_MoveWindow_m156,
	GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m157,
	GUILayoutUtility_get_spaceStyle_m158,
	GUILayoutEntry__ctor_m159,
	GUILayoutEntry__cctor_m160,
	GUILayoutEntry_get_style_m161,
	GUILayoutEntry_set_style_m162,
	GUILayoutEntry_get_margin_m163,
	GUILayoutEntry_CalcWidth_m164,
	GUILayoutEntry_CalcHeight_m165,
	GUILayoutEntry_SetHorizontal_m166,
	GUILayoutEntry_SetVertical_m167,
	GUILayoutEntry_ApplyStyleSettings_m168,
	GUILayoutEntry_ApplyOptions_m169,
	GUILayoutEntry_ToString_m170,
	GUILayoutGroup__ctor_m171,
	GUILayoutGroup_get_margin_m172,
	GUILayoutGroup_ApplyOptions_m173,
	GUILayoutGroup_ApplyStyleSettings_m174,
	GUILayoutGroup_ResetCursor_m175,
	GUILayoutGroup_CalcWidth_m176,
	GUILayoutGroup_SetHorizontal_m177,
	GUILayoutGroup_CalcHeight_m178,
	GUILayoutGroup_SetVertical_m179,
	GUILayoutGroup_ToString_m180,
	GUIScrollGroup__ctor_m181,
	GUIScrollGroup_CalcWidth_m182,
	GUIScrollGroup_SetHorizontal_m183,
	GUIScrollGroup_CalcHeight_m184,
	GUIScrollGroup_SetVertical_m185,
	GUILayoutOption__ctor_m186,
	GUIUtility__cctor_m187,
	GUIUtility_GetDefaultSkin_m188,
	GUIUtility_Internal_GetDefaultSkin_m189,
	GUIUtility_BeginGUI_m190,
	GUIUtility_Internal_ExitGUI_m191,
	GUIUtility_EndGUI_m192,
	GUIUtility_EndGUIFromException_m193,
	GUIUtility_CheckOnGUI_m194,
	GUIUtility_Internal_GetGUIDepth_m195,
	GUISettings__ctor_m196,
	SkinChangedDelegate__ctor_m197,
	SkinChangedDelegate_Invoke_m198,
	SkinChangedDelegate_BeginInvoke_m199,
	SkinChangedDelegate_EndInvoke_m200,
	GUISkin__ctor_m201,
	GUISkin_OnEnable_m202,
	GUISkin_get_font_m203,
	GUISkin_set_font_m204,
	GUISkin_get_box_m205,
	GUISkin_set_box_m206,
	GUISkin_get_label_m207,
	GUISkin_set_label_m208,
	GUISkin_get_textField_m209,
	GUISkin_set_textField_m210,
	GUISkin_get_textArea_m211,
	GUISkin_set_textArea_m212,
	GUISkin_get_button_m213,
	GUISkin_set_button_m214,
	GUISkin_get_toggle_m215,
	GUISkin_set_toggle_m216,
	GUISkin_get_window_m217,
	GUISkin_set_window_m218,
	GUISkin_get_horizontalSlider_m219,
	GUISkin_set_horizontalSlider_m220,
	GUISkin_get_horizontalSliderThumb_m221,
	GUISkin_set_horizontalSliderThumb_m222,
	GUISkin_get_verticalSlider_m223,
	GUISkin_set_verticalSlider_m224,
	GUISkin_get_verticalSliderThumb_m225,
	GUISkin_set_verticalSliderThumb_m226,
	GUISkin_get_horizontalScrollbar_m227,
	GUISkin_set_horizontalScrollbar_m228,
	GUISkin_get_horizontalScrollbarThumb_m229,
	GUISkin_set_horizontalScrollbarThumb_m230,
	GUISkin_get_horizontalScrollbarLeftButton_m231,
	GUISkin_set_horizontalScrollbarLeftButton_m232,
	GUISkin_get_horizontalScrollbarRightButton_m233,
	GUISkin_set_horizontalScrollbarRightButton_m234,
	GUISkin_get_verticalScrollbar_m235,
	GUISkin_set_verticalScrollbar_m236,
	GUISkin_get_verticalScrollbarThumb_m237,
	GUISkin_set_verticalScrollbarThumb_m238,
	GUISkin_get_verticalScrollbarUpButton_m239,
	GUISkin_set_verticalScrollbarUpButton_m240,
	GUISkin_get_verticalScrollbarDownButton_m241,
	GUISkin_set_verticalScrollbarDownButton_m242,
	GUISkin_get_scrollView_m243,
	GUISkin_set_scrollView_m244,
	GUISkin_get_customStyles_m245,
	GUISkin_set_customStyles_m246,
	GUISkin_get_settings_m247,
	GUISkin_get_error_m248,
	GUISkin_Apply_m249,
	GUISkin_BuildStyleCache_m250,
	GUISkin_GetStyle_m251,
	GUISkin_FindStyle_m252,
	GUISkin_MakeCurrent_m253,
	GUISkin_GetEnumerator_m254,
	GUIContent__ctor_m255,
	GUIContent__ctor_m256,
	GUIContent__cctor_m257,
	GUIContent_ClearStaticCache_m258,
	GUIStyleState__ctor_m259,
	GUIStyleState__ctor_m260,
	GUIStyleState_Finalize_m261,
	GUIStyleState_Init_m262,
	GUIStyleState_Cleanup_m263,
	GUIStyleState_GetBackgroundInternal_m264,
	GUIStyleState_set_textColor_m265,
	GUIStyleState_INTERNAL_set_textColor_m266,
	RectOffset__ctor_m267,
	RectOffset__ctor_m268,
	RectOffset__ctor_m269,
	RectOffset_Finalize_m270,
	RectOffset_Init_m271,
	RectOffset_Cleanup_m272,
	RectOffset_get_left_m273,
	RectOffset_set_left_m274,
	RectOffset_get_right_m275,
	RectOffset_set_right_m276,
	RectOffset_get_top_m277,
	RectOffset_set_top_m278,
	RectOffset_get_bottom_m279,
	RectOffset_set_bottom_m280,
	RectOffset_get_horizontal_m281,
	RectOffset_get_vertical_m282,
	RectOffset_ToString_m283,
	GUIStyle__ctor_m284,
	GUIStyle__cctor_m285,
	GUIStyle_Finalize_m286,
	GUIStyle_Init_m287,
	GUIStyle_Cleanup_m288,
	GUIStyle_get_name_m289,
	GUIStyle_set_name_m290,
	GUIStyle_get_normal_m291,
	GUIStyle_GetStyleStatePtr_m292,
	GUIStyle_get_margin_m293,
	GUIStyle_get_padding_m294,
	GUIStyle_GetRectOffsetPtr_m295,
	GUIStyle_get_fixedWidth_m296,
	GUIStyle_get_fixedHeight_m297,
	GUIStyle_get_stretchWidth_m298,
	GUIStyle_set_stretchWidth_m299,
	GUIStyle_get_stretchHeight_m300,
	GUIStyle_set_stretchHeight_m301,
	GUIStyle_SetDefaultFont_m302,
	GUIStyle_get_none_m303,
	GUIStyle_ToString_m304,
	TouchScreenKeyboard_Destroy_m305,
	TouchScreenKeyboard_Finalize_m306,
	Event__ctor_m307,
	Event_Init_m308,
	Event_Finalize_m309,
	Event_Cleanup_m310,
	Event_get_type_m311,
	Event_get_mousePosition_m312,
	Event_Internal_GetMousePosition_m313,
	Event_get_modifiers_m314,
	Event_get_character_m315,
	Event_get_commandName_m316,
	Event_get_keyCode_m317,
	Event_get_current_m318,
	Event_Internal_SetNativeEvent_m319,
	Event_Internal_MakeMasterEventCurrent_m320,
	Event_get_isKey_m321,
	Event_get_isMouse_m322,
	Event_GetHashCode_m323,
	Event_Equals_m324,
	Event_ToString_m325,
	Vector2__ctor_m326,
	Vector2_ToString_m327,
	Vector2_GetHashCode_m328,
	Vector2_Equals_m329,
	Vector2_SqrMagnitude_m330,
	Vector2_get_zero_m331,
	Vector2_op_Subtraction_m332,
	Vector2_op_Equality_m333,
	Vector3__ctor_m334,
	Vector3_GetHashCode_m335,
	Vector3_Equals_m336,
	Vector3_ToString_m337,
	Vector3_ToString_m338,
	Vector3_SqrMagnitude_m339,
	Vector3_Min_m340,
	Vector3_Max_m341,
	Vector3_get_zero_m342,
	Vector3_get_back_m343,
	Vector3_op_Addition_m344,
	Vector3_op_Subtraction_m345,
	Vector3_op_Multiply_m346,
	Vector3_op_Equality_m347,
	Color__ctor_m348,
	Color__ctor_m349,
	Color_ToString_m350,
	Color_GetHashCode_m351,
	Color_Equals_m352,
	Color_get_red_m353,
	Color_get_white_m354,
	Color_op_Multiply_m355,
	Color_op_Implicit_m356,
	Color32__ctor_m357,
	Color32_ToString_m358,
	Color32_op_Implicit_m359,
	Quaternion_ToString_m360,
	Quaternion_GetHashCode_m361,
	Quaternion_Equals_m362,
	Rect__ctor_m363,
	Rect_get_x_m364,
	Rect_set_x_m365,
	Rect_get_y_m366,
	Rect_set_y_m367,
	Rect_get_width_m368,
	Rect_set_width_m369,
	Rect_get_height_m370,
	Rect_set_height_m371,
	Rect_get_xMin_m372,
	Rect_get_yMin_m373,
	Rect_get_xMax_m374,
	Rect_get_yMax_m375,
	Rect_ToString_m376,
	Rect_Contains_m377,
	Rect_GetHashCode_m378,
	Rect_Equals_m379,
	Matrix4x4_get_Item_m380,
	Matrix4x4_set_Item_m381,
	Matrix4x4_get_Item_m382,
	Matrix4x4_set_Item_m383,
	Matrix4x4_GetHashCode_m384,
	Matrix4x4_Equals_m385,
	Matrix4x4_Inverse_m386,
	Matrix4x4_INTERNAL_CALL_Inverse_m387,
	Matrix4x4_Transpose_m388,
	Matrix4x4_INTERNAL_CALL_Transpose_m389,
	Matrix4x4_Invert_m390,
	Matrix4x4_INTERNAL_CALL_Invert_m391,
	Matrix4x4_get_inverse_m392,
	Matrix4x4_get_transpose_m393,
	Matrix4x4_get_isIdentity_m394,
	Matrix4x4_GetColumn_m395,
	Matrix4x4_GetRow_m396,
	Matrix4x4_SetColumn_m397,
	Matrix4x4_SetRow_m398,
	Matrix4x4_MultiplyPoint_m399,
	Matrix4x4_MultiplyPoint3x4_m400,
	Matrix4x4_MultiplyVector_m401,
	Matrix4x4_Scale_m402,
	Matrix4x4_get_zero_m403,
	Matrix4x4_get_identity_m404,
	Matrix4x4_SetTRS_m405,
	Matrix4x4_TRS_m406,
	Matrix4x4_INTERNAL_CALL_TRS_m407,
	Matrix4x4_ToString_m408,
	Matrix4x4_ToString_m409,
	Matrix4x4_Ortho_m410,
	Matrix4x4_Perspective_m411,
	Matrix4x4_op_Multiply_m412,
	Matrix4x4_op_Multiply_m413,
	Matrix4x4_op_Equality_m414,
	Matrix4x4_op_Inequality_m415,
	Bounds__ctor_m416,
	Bounds_GetHashCode_m417,
	Bounds_Equals_m418,
	Bounds_get_center_m419,
	Bounds_set_center_m420,
	Bounds_get_size_m421,
	Bounds_set_size_m422,
	Bounds_get_extents_m423,
	Bounds_set_extents_m424,
	Bounds_get_min_m425,
	Bounds_set_min_m426,
	Bounds_get_max_m427,
	Bounds_set_max_m428,
	Bounds_SetMinMax_m429,
	Bounds_Encapsulate_m430,
	Bounds_Encapsulate_m431,
	Bounds_Expand_m432,
	Bounds_Expand_m433,
	Bounds_Intersects_m434,
	Bounds_Internal_Contains_m435,
	Bounds_INTERNAL_CALL_Internal_Contains_m436,
	Bounds_Contains_m437,
	Bounds_Internal_SqrDistance_m438,
	Bounds_INTERNAL_CALL_Internal_SqrDistance_m439,
	Bounds_SqrDistance_m440,
	Bounds_Internal_IntersectRay_m441,
	Bounds_INTERNAL_CALL_Internal_IntersectRay_m442,
	Bounds_IntersectRay_m443,
	Bounds_IntersectRay_m444,
	Bounds_Internal_GetClosestPoint_m445,
	Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m446,
	Bounds_ClosestPoint_m447,
	Bounds_ToString_m448,
	Bounds_ToString_m449,
	Bounds_op_Equality_m450,
	Bounds_op_Inequality_m451,
	Vector4__ctor_m452,
	Vector4_GetHashCode_m453,
	Vector4_Equals_m454,
	Vector4_ToString_m455,
	Vector4_Dot_m456,
	Vector4_SqrMagnitude_m457,
	Vector4_op_Subtraction_m458,
	Vector4_op_Equality_m459,
	Ray_get_direction_m460,
	Ray_ToString_m461,
	MathfInternal__cctor_m462,
	Mathf__cctor_m463,
	Mathf_Abs_m464,
	Mathf_Min_m465,
	Mathf_Min_m466,
	Mathf_Max_m467,
	Mathf_Max_m468,
	Mathf_Round_m469,
	Mathf_Clamp_m470,
	Mathf_Clamp01_m471,
	Mathf_Lerp_m472,
	Mathf_Approximately_m473,
	ReapplyDrivenProperties__ctor_m474,
	ReapplyDrivenProperties_Invoke_m475,
	ReapplyDrivenProperties_BeginInvoke_m476,
	ReapplyDrivenProperties_EndInvoke_m477,
	RectTransform_SendReapplyDrivenProperties_m478,
	ResourceRequest__ctor_m479,
	ResourceRequest_get_asset_m480,
	Resources_Load_m481,
	SerializePrivateVariables__ctor_m482,
	SerializeField__ctor_m483,
	SphericalHarmonicsL2_Clear_m484,
	SphericalHarmonicsL2_ClearInternal_m485,
	SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m486,
	SphericalHarmonicsL2_AddAmbientLight_m487,
	SphericalHarmonicsL2_AddAmbientLightInternal_m488,
	SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m489,
	SphericalHarmonicsL2_AddDirectionalLight_m490,
	SphericalHarmonicsL2_AddDirectionalLightInternal_m491,
	SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m492,
	SphericalHarmonicsL2_get_Item_m493,
	SphericalHarmonicsL2_set_Item_m494,
	SphericalHarmonicsL2_GetHashCode_m495,
	SphericalHarmonicsL2_Equals_m496,
	SphericalHarmonicsL2_op_Multiply_m497,
	SphericalHarmonicsL2_op_Multiply_m498,
	SphericalHarmonicsL2_op_Addition_m499,
	SphericalHarmonicsL2_op_Equality_m500,
	SphericalHarmonicsL2_op_Inequality_m501,
	WWW__ctor_m502,
	WWW_Dispose_m503,
	WWW_Finalize_m504,
	WWW_DestroyWWW_m505,
	WWW_InitWWW_m506,
	WWW_get_responseHeaders_m507,
	WWW_get_responseHeadersString_m508,
	WWW_get_text_m509,
	WWW_get_DefaultEncoding_m510,
	WWW_GetTextEncoder_m511,
	WWW_get_bytes_m512,
	WWW_get_error_m513,
	WWW_get_isDone_m514,
	WWW_FlattenedHeadersFrom_m515,
	WWW_ParseHTTPHeaderString_m516,
	WWWForm__ctor_m517,
	WWWForm_AddField_m518,
	WWWForm_AddField_m519,
	WWWForm_AddField_m520,
	WWWForm_get_headers_m521,
	WWWForm_get_data_m522,
	WWWTranscoder__cctor_m523,
	WWWTranscoder_Byte2Hex_m524,
	WWWTranscoder_URLEncode_m525,
	WWWTranscoder_QPEncode_m526,
	WWWTranscoder_Encode_m527,
	WWWTranscoder_ByteArrayContains_m528,
	WWWTranscoder_SevenBitClean_m529,
	WWWTranscoder_SevenBitClean_m530,
	UnityString_Format_m531,
	AsyncOperation__ctor_m532,
	AsyncOperation_InternalDestroy_m533,
	AsyncOperation_Finalize_m534,
	LogCallback__ctor_m535,
	LogCallback_Invoke_m536,
	LogCallback_BeginInvoke_m537,
	LogCallback_EndInvoke_m538,
	Application_get_cloudProjectId_m539,
	Application_CallLogCallback_m540,
	Behaviour__ctor_m541,
	CameraCallback__ctor_m542,
	CameraCallback_Invoke_m543,
	CameraCallback_BeginInvoke_m544,
	CameraCallback_EndInvoke_m545,
	Camera_get_nearClipPlane_m546,
	Camera_get_farClipPlane_m547,
	Camera_get_cullingMask_m548,
	Camera_get_eventMask_m549,
	Camera_get_pixelRect_m550,
	Camera_INTERNAL_get_pixelRect_m551,
	Camera_get_targetTexture_m552,
	Camera_get_clearFlags_m553,
	Camera_ScreenPointToRay_m554,
	Camera_INTERNAL_CALL_ScreenPointToRay_m555,
	Camera_get_allCamerasCount_m556,
	Camera_GetAllCameras_m557,
	Camera_FireOnPreCull_m558,
	Camera_FireOnPreRender_m559,
	Camera_FireOnPostRender_m560,
	Camera_RaycastTry_m561,
	Camera_INTERNAL_CALL_RaycastTry_m562,
	Camera_RaycastTry2D_m563,
	Camera_INTERNAL_CALL_RaycastTry2D_m564,
	Debug_Internal_Log_m565,
	Debug_Log_m566,
	Debug_LogError_m567,
	Debug_LogWarning_m568,
	DisplaysUpdatedDelegate__ctor_m569,
	DisplaysUpdatedDelegate_Invoke_m570,
	DisplaysUpdatedDelegate_BeginInvoke_m571,
	DisplaysUpdatedDelegate_EndInvoke_m572,
	Display__ctor_m573,
	Display__ctor_m574,
	Display__cctor_m575,
	Display_add_onDisplaysUpdated_m576,
	Display_remove_onDisplaysUpdated_m577,
	Display_get_renderingWidth_m578,
	Display_get_renderingHeight_m579,
	Display_get_systemWidth_m580,
	Display_get_systemHeight_m581,
	Display_get_colorBuffer_m582,
	Display_get_depthBuffer_m583,
	Display_Activate_m584,
	Display_Activate_m585,
	Display_SetParams_m586,
	Display_SetRenderingResolution_m587,
	Display_MultiDisplayLicense_m588,
	Display_RelativeMouseAt_m589,
	Display_get_main_m590,
	Display_RecreateDisplayList_m591,
	Display_FireDisplaysUpdated_m592,
	Display_GetSystemExtImpl_m593,
	Display_GetRenderingExtImpl_m594,
	Display_GetRenderingBuffersImpl_m595,
	Display_SetRenderingResolutionImpl_m596,
	Display_ActivateDisplayImpl_m597,
	Display_SetParamsImpl_m598,
	Display_MultiDisplayLicenseImpl_m599,
	Display_RelativeMouseAtImpl_m600,
	MonoBehaviour__ctor_m601,
	MonoBehaviour_StartCoroutine_m602,
	MonoBehaviour_StartCoroutine_Auto_m603,
	Input__cctor_m604,
	Input_GetMouseButton_m605,
	Input_GetMouseButtonDown_m606,
	Input_get_mousePosition_m607,
	Input_INTERNAL_get_mousePosition_m608,
	Object__ctor_m609,
	Object_get_name_m610,
	Object_ToString_m611,
	Object_Equals_m612,
	Object_GetHashCode_m613,
	Object_CompareBaseObjects_m614,
	Object_IsNativeObjectAlive_m615,
	Object_GetInstanceID_m616,
	Object_GetCachedPtr_m617,
	Object_op_Implicit_m618,
	Object_op_Equality_m619,
	Object_op_Inequality_m620,
	Component__ctor_m621,
	Component_get_gameObject_m622,
	Component_GetComponentFastPath_m623,
	GameObject_SendMessage_m624,
	Enumerator__ctor_m625,
	Enumerator_get_Current_m626,
	Enumerator_MoveNext_m627,
	Transform_get_childCount_m628,
	Transform_GetEnumerator_m629,
	Transform_GetChild_m630,
	Random_Range_m631,
	Random_RandomRangeInt_m632,
	YieldInstruction__ctor_m633,
	PlayerPrefs_GetString_m634,
	PlayerPrefs_GetString_m635,
	Particle_get_position_m636,
	Particle_set_position_m637,
	Particle_get_velocity_m638,
	Particle_set_velocity_m639,
	Particle_get_energy_m640,
	Particle_set_energy_m641,
	Particle_get_startEnergy_m642,
	Particle_set_startEnergy_m643,
	Particle_get_size_m644,
	Particle_set_size_m645,
	Particle_get_rotation_m646,
	Particle_set_rotation_m647,
	Particle_get_angularVelocity_m648,
	Particle_set_angularVelocity_m649,
	Particle_get_color_m650,
	Particle_set_color_m651,
	AudioConfigurationChangeHandler__ctor_m652,
	AudioConfigurationChangeHandler_Invoke_m653,
	AudioConfigurationChangeHandler_BeginInvoke_m654,
	AudioConfigurationChangeHandler_EndInvoke_m655,
	AudioSettings_InvokeOnAudioConfigurationChanged_m656,
	PCMReaderCallback__ctor_m657,
	PCMReaderCallback_Invoke_m658,
	PCMReaderCallback_BeginInvoke_m659,
	PCMReaderCallback_EndInvoke_m660,
	PCMSetPositionCallback__ctor_m661,
	PCMSetPositionCallback_Invoke_m662,
	PCMSetPositionCallback_BeginInvoke_m663,
	PCMSetPositionCallback_EndInvoke_m664,
	AudioClip_InvokePCMReaderCallback_Internal_m665,
	AudioClip_InvokePCMSetPositionCallback_Internal_m666,
	WebCamDevice_get_name_m667,
	WebCamDevice_get_isFrontFacing_m668,
	AnimationEvent__ctor_m669,
	AnimationEvent_get_data_m670,
	AnimationEvent_set_data_m671,
	AnimationEvent_get_stringParameter_m672,
	AnimationEvent_set_stringParameter_m673,
	AnimationEvent_get_floatParameter_m674,
	AnimationEvent_set_floatParameter_m675,
	AnimationEvent_get_intParameter_m676,
	AnimationEvent_set_intParameter_m677,
	AnimationEvent_get_objectReferenceParameter_m678,
	AnimationEvent_set_objectReferenceParameter_m679,
	AnimationEvent_get_functionName_m680,
	AnimationEvent_set_functionName_m681,
	AnimationEvent_get_time_m682,
	AnimationEvent_set_time_m683,
	AnimationEvent_get_messageOptions_m684,
	AnimationEvent_set_messageOptions_m685,
	AnimationEvent_get_isFiredByLegacy_m686,
	AnimationEvent_get_isFiredByAnimator_m687,
	AnimationEvent_get_animationState_m688,
	AnimationEvent_get_animatorStateInfo_m689,
	AnimationEvent_get_animatorClipInfo_m690,
	AnimationEvent_GetHash_m691,
	AnimationCurve__ctor_m692,
	AnimationCurve__ctor_m693,
	AnimationCurve_Cleanup_m694,
	AnimationCurve_Finalize_m695,
	AnimationCurve_Init_m696,
	AnimatorStateInfo_IsName_m697,
	AnimatorStateInfo_get_fullPathHash_m698,
	AnimatorStateInfo_get_nameHash_m699,
	AnimatorStateInfo_get_shortNameHash_m700,
	AnimatorStateInfo_get_normalizedTime_m701,
	AnimatorStateInfo_get_length_m702,
	AnimatorStateInfo_get_tagHash_m703,
	AnimatorStateInfo_IsTag_m704,
	AnimatorStateInfo_get_loop_m705,
	AnimatorTransitionInfo_IsName_m706,
	AnimatorTransitionInfo_IsUserName_m707,
	AnimatorTransitionInfo_get_fullPathHash_m708,
	AnimatorTransitionInfo_get_nameHash_m709,
	AnimatorTransitionInfo_get_userNameHash_m710,
	AnimatorTransitionInfo_get_normalizedTime_m711,
	AnimatorTransitionInfo_get_anyState_m712,
	AnimatorTransitionInfo_get_entry_m713,
	AnimatorTransitionInfo_get_exit_m714,
	Animator_StringToHash_m715,
	HumanBone_get_boneName_m716,
	HumanBone_set_boneName_m717,
	HumanBone_get_humanName_m718,
	HumanBone_set_humanName_m719,
	CharacterInfo_get_advance_m720,
	CharacterInfo_get_glyphWidth_m721,
	CharacterInfo_get_glyphHeight_m722,
	CharacterInfo_get_bearing_m723,
	CharacterInfo_get_minY_m724,
	CharacterInfo_get_maxY_m725,
	CharacterInfo_get_minX_m726,
	CharacterInfo_get_maxX_m727,
	CharacterInfo_get_uvBottomLeftUnFlipped_m728,
	CharacterInfo_get_uvBottomRightUnFlipped_m729,
	CharacterInfo_get_uvTopRightUnFlipped_m730,
	CharacterInfo_get_uvTopLeftUnFlipped_m731,
	CharacterInfo_get_uvBottomLeft_m732,
	CharacterInfo_get_uvBottomRight_m733,
	CharacterInfo_get_uvTopRight_m734,
	CharacterInfo_get_uvTopLeft_m735,
	FontTextureRebuildCallback__ctor_m736,
	FontTextureRebuildCallback_Invoke_m737,
	FontTextureRebuildCallback_BeginInvoke_m738,
	FontTextureRebuildCallback_EndInvoke_m739,
	Font_InvokeTextureRebuilt_Internal_m740,
	Font_get_dynamic_m741,
	TextGenerator__ctor_m742,
	TextGenerator__ctor_m743,
	TextGenerator_System_IDisposable_Dispose_m744,
	TextGenerator_Init_m745,
	TextGenerator_Dispose_cpp_m746,
	TextGenerator_Populate_Internal_m747,
	TextGenerator_Populate_Internal_cpp_m748,
	TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m749,
	TextGenerator_get_rectExtents_m750,
	TextGenerator_INTERNAL_get_rectExtents_m751,
	TextGenerator_get_vertexCount_m752,
	TextGenerator_GetVerticesInternal_m753,
	TextGenerator_GetVerticesArray_m754,
	TextGenerator_get_characterCount_m755,
	TextGenerator_get_characterCountVisible_m756,
	TextGenerator_GetCharactersInternal_m757,
	TextGenerator_GetCharactersArray_m758,
	TextGenerator_get_lineCount_m759,
	TextGenerator_GetLinesInternal_m760,
	TextGenerator_GetLinesArray_m761,
	TextGenerator_get_fontSizeUsedForBestFit_m762,
	TextGenerator_Finalize_m763,
	TextGenerator_ValidatedSettings_m764,
	TextGenerator_Invalidate_m765,
	TextGenerator_GetCharacters_m766,
	TextGenerator_GetLines_m767,
	TextGenerator_GetVertices_m768,
	TextGenerator_GetPreferredWidth_m769,
	TextGenerator_GetPreferredHeight_m770,
	TextGenerator_Populate_m771,
	TextGenerator_PopulateAlways_m772,
	TextGenerator_get_verts_m773,
	TextGenerator_get_characters_m774,
	TextGenerator_get_lines_m775,
	WillRenderCanvases__ctor_m776,
	WillRenderCanvases_Invoke_m777,
	WillRenderCanvases_BeginInvoke_m778,
	WillRenderCanvases_EndInvoke_m779,
	Canvas_SendWillRenderCanvases_m780,
	UIVertex__cctor_m781,
	Request__ctor_m782,
	Request_get_sourceId_m783,
	Request_get_appId_m784,
	Request_get_domain_m785,
	Request_ToString_m786,
	ResponseBase__ctor_m787,
	ResponseBase_ParseJSONString_m788,
	ResponseBase_ParseJSONInt32_m789,
	ResponseBase_ParseJSONUInt16_m790,
	ResponseBase_ParseJSONUInt64_m791,
	ResponseBase_ParseJSONBool_m792,
	Response__ctor_m793,
	Response_get_success_m794,
	Response_set_success_m795,
	Response_get_extendedInfo_m796,
	Response_set_extendedInfo_m797,
	Response_ToString_m798,
	Response_Parse_m799,
	BasicResponse__ctor_m800,
	CreateMatchRequest__ctor_m801,
	CreateMatchRequest_get_name_m802,
	CreateMatchRequest_set_name_m803,
	CreateMatchRequest_get_size_m804,
	CreateMatchRequest_set_size_m805,
	CreateMatchRequest_get_advertise_m806,
	CreateMatchRequest_set_advertise_m807,
	CreateMatchRequest_get_password_m808,
	CreateMatchRequest_set_password_m809,
	CreateMatchRequest_get_matchAttributes_m810,
	CreateMatchRequest_ToString_m811,
	CreateMatchResponse__ctor_m812,
	CreateMatchResponse_get_address_m813,
	CreateMatchResponse_set_address_m814,
	CreateMatchResponse_get_port_m815,
	CreateMatchResponse_set_port_m816,
	CreateMatchResponse_get_networkId_m817,
	CreateMatchResponse_set_networkId_m818,
	CreateMatchResponse_get_accessTokenString_m819,
	CreateMatchResponse_set_accessTokenString_m820,
	CreateMatchResponse_get_nodeId_m821,
	CreateMatchResponse_set_nodeId_m822,
	CreateMatchResponse_get_usingRelay_m823,
	CreateMatchResponse_set_usingRelay_m824,
	CreateMatchResponse_ToString_m825,
	CreateMatchResponse_Parse_m826,
	JoinMatchRequest__ctor_m827,
	JoinMatchRequest_get_networkId_m828,
	JoinMatchRequest_set_networkId_m829,
	JoinMatchRequest_get_password_m830,
	JoinMatchRequest_set_password_m831,
	JoinMatchRequest_ToString_m832,
	JoinMatchResponse__ctor_m833,
	JoinMatchResponse_get_address_m834,
	JoinMatchResponse_set_address_m835,
	JoinMatchResponse_get_port_m836,
	JoinMatchResponse_set_port_m837,
	JoinMatchResponse_get_networkId_m838,
	JoinMatchResponse_set_networkId_m839,
	JoinMatchResponse_get_accessTokenString_m840,
	JoinMatchResponse_set_accessTokenString_m841,
	JoinMatchResponse_get_nodeId_m842,
	JoinMatchResponse_set_nodeId_m843,
	JoinMatchResponse_get_usingRelay_m844,
	JoinMatchResponse_set_usingRelay_m845,
	JoinMatchResponse_ToString_m846,
	JoinMatchResponse_Parse_m847,
	DestroyMatchRequest__ctor_m848,
	DestroyMatchRequest_get_networkId_m849,
	DestroyMatchRequest_set_networkId_m850,
	DestroyMatchRequest_ToString_m851,
	DropConnectionRequest__ctor_m852,
	DropConnectionRequest_get_networkId_m853,
	DropConnectionRequest_set_networkId_m854,
	DropConnectionRequest_get_nodeId_m855,
	DropConnectionRequest_set_nodeId_m856,
	DropConnectionRequest_ToString_m857,
	ListMatchRequest__ctor_m858,
	ListMatchRequest_get_pageSize_m859,
	ListMatchRequest_set_pageSize_m860,
	ListMatchRequest_get_pageNum_m861,
	ListMatchRequest_set_pageNum_m862,
	ListMatchRequest_get_nameFilter_m863,
	ListMatchRequest_set_nameFilter_m864,
	ListMatchRequest_get_includePasswordMatches_m865,
	ListMatchRequest_get_matchAttributeFilterLessThan_m866,
	ListMatchRequest_get_matchAttributeFilterGreaterThan_m867,
	ListMatchRequest_ToString_m868,
	MatchDirectConnectInfo__ctor_m869,
	MatchDirectConnectInfo_get_nodeId_m870,
	MatchDirectConnectInfo_set_nodeId_m871,
	MatchDirectConnectInfo_get_publicAddress_m872,
	MatchDirectConnectInfo_set_publicAddress_m873,
	MatchDirectConnectInfo_get_privateAddress_m874,
	MatchDirectConnectInfo_set_privateAddress_m875,
	MatchDirectConnectInfo_ToString_m876,
	MatchDirectConnectInfo_Parse_m877,
	MatchDesc__ctor_m878,
	MatchDesc_get_networkId_m879,
	MatchDesc_set_networkId_m880,
	MatchDesc_get_name_m881,
	MatchDesc_set_name_m882,
	MatchDesc_get_averageEloScore_m883,
	MatchDesc_get_maxSize_m884,
	MatchDesc_set_maxSize_m885,
	MatchDesc_get_currentSize_m886,
	MatchDesc_set_currentSize_m887,
	MatchDesc_get_isPrivate_m888,
	MatchDesc_set_isPrivate_m889,
	MatchDesc_get_matchAttributes_m890,
	MatchDesc_get_hostNodeId_m891,
	MatchDesc_get_directConnectInfos_m892,
	MatchDesc_set_directConnectInfos_m893,
	MatchDesc_ToString_m894,
	MatchDesc_Parse_m895,
	ListMatchResponse__ctor_m896,
	ListMatchResponse_get_matches_m897,
	ListMatchResponse_set_matches_m898,
	ListMatchResponse_ToString_m899,
	ListMatchResponse_Parse_m900,
	NetworkAccessToken__ctor_m901,
	NetworkAccessToken_GetByteString_m902,
	Utility__cctor_m903,
	Utility_GetSourceID_m904,
	Utility_SetAppID_m905,
	Utility_GetAppID_m906,
	Utility_GetAccessTokenForNetwork_m907,
	NetworkMatch__ctor_m908,
	NetworkMatch_get_baseUri_m909,
	NetworkMatch_set_baseUri_m910,
	NetworkMatch_SetProgramAppID_m911,
	NetworkMatch_CreateMatch_m912,
	NetworkMatch_CreateMatch_m913,
	NetworkMatch_JoinMatch_m914,
	NetworkMatch_JoinMatch_m915,
	NetworkMatch_DestroyMatch_m916,
	NetworkMatch_DestroyMatch_m917,
	NetworkMatch_DropConnection_m918,
	NetworkMatch_DropConnection_m919,
	NetworkMatch_ListMatches_m920,
	NetworkMatch_ListMatches_m921,
	JsonArray__ctor_m922,
	JsonArray_ToString_m923,
	JsonObject__ctor_m924,
	JsonObject_System_Collections_IEnumerable_GetEnumerator_m925,
	JsonObject_Add_m926,
	JsonObject_get_Keys_m927,
	JsonObject_TryGetValue_m928,
	JsonObject_get_Values_m929,
	JsonObject_get_Item_m930,
	JsonObject_set_Item_m931,
	JsonObject_Add_m932,
	JsonObject_Clear_m933,
	JsonObject_Contains_m934,
	JsonObject_CopyTo_m935,
	JsonObject_get_Count_m936,
	JsonObject_get_IsReadOnly_m937,
	JsonObject_Remove_m938,
	JsonObject_GetEnumerator_m939,
	JsonObject_ToString_m940,
	SimpleJson_TryDeserializeObject_m941,
	SimpleJson_SerializeObject_m942,
	SimpleJson_SerializeObject_m943,
	SimpleJson_ParseObject_m944,
	SimpleJson_ParseArray_m945,
	SimpleJson_ParseValue_m946,
	SimpleJson_ParseString_m947,
	SimpleJson_ConvertFromUtf32_m948,
	SimpleJson_ParseNumber_m949,
	SimpleJson_GetLastIndexOfNumber_m950,
	SimpleJson_EatWhitespace_m951,
	SimpleJson_LookAhead_m952,
	SimpleJson_NextToken_m953,
	SimpleJson_SerializeValue_m954,
	SimpleJson_SerializeObject_m955,
	SimpleJson_SerializeArray_m956,
	SimpleJson_SerializeString_m957,
	SimpleJson_SerializeNumber_m958,
	SimpleJson_IsNumeric_m959,
	SimpleJson_get_CurrentJsonSerializerStrategy_m960,
	SimpleJson_get_PocoJsonSerializerStrategy_m961,
	PocoJsonSerializerStrategy__ctor_m962,
	PocoJsonSerializerStrategy__cctor_m963,
	PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m964,
	PocoJsonSerializerStrategy_ContructorDelegateFactory_m965,
	PocoJsonSerializerStrategy_GetterValueFactory_m966,
	PocoJsonSerializerStrategy_SetterValueFactory_m967,
	PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m968,
	PocoJsonSerializerStrategy_SerializeEnum_m969,
	PocoJsonSerializerStrategy_TrySerializeKnownTypes_m970,
	PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m971,
	GetDelegate__ctor_m972,
	GetDelegate_Invoke_m973,
	GetDelegate_BeginInvoke_m974,
	GetDelegate_EndInvoke_m975,
	SetDelegate__ctor_m976,
	SetDelegate_Invoke_m977,
	SetDelegate_BeginInvoke_m978,
	SetDelegate_EndInvoke_m979,
	ConstructorDelegate__ctor_m980,
	ConstructorDelegate_Invoke_m981,
	ConstructorDelegate_BeginInvoke_m982,
	ConstructorDelegate_EndInvoke_m983,
	U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m984,
	U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m985,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m986,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m987,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m988,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m989,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m990,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m991,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m992,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m993,
	ReflectionUtils__cctor_m994,
	ReflectionUtils_GetConstructors_m995,
	ReflectionUtils_GetConstructorInfo_m996,
	ReflectionUtils_GetProperties_m997,
	ReflectionUtils_GetFields_m998,
	ReflectionUtils_GetGetterMethodInfo_m999,
	ReflectionUtils_GetSetterMethodInfo_m1000,
	ReflectionUtils_GetContructor_m1001,
	ReflectionUtils_GetConstructorByReflection_m1002,
	ReflectionUtils_GetConstructorByReflection_m1003,
	ReflectionUtils_GetGetMethod_m1004,
	ReflectionUtils_GetGetMethod_m1005,
	ReflectionUtils_GetGetMethodByReflection_m1006,
	ReflectionUtils_GetGetMethodByReflection_m1007,
	ReflectionUtils_GetSetMethod_m1008,
	ReflectionUtils_GetSetMethod_m1009,
	ReflectionUtils_GetSetMethodByReflection_m1010,
	ReflectionUtils_GetSetMethodByReflection_m1011,
	WrapperlessIcall__ctor_m1012,
	IL2CPPStructAlignmentAttribute__ctor_m1013,
	AttributeHelperEngine__cctor_m1014,
	AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m1015,
	AttributeHelperEngine_GetRequiredComponents_m1016,
	AttributeHelperEngine_CheckIsEditorScript_m1017,
	AddComponentMenu__ctor_m1018,
	ExecuteInEditMode__ctor_m1019,
	SetupCoroutine__ctor_m1020,
	SetupCoroutine_InvokeMember_m1021,
	SetupCoroutine_InvokeStatic_m1022,
	WritableAttribute__ctor_m1023,
	AssemblyIsEditorAssembly__ctor_m1024,
	GcUserProfileData_ToUserProfile_m1025,
	GcUserProfileData_AddToArray_m1026,
	GcAchievementDescriptionData_ToAchievementDescription_m1027,
	GcAchievementData_ToAchievement_m1028,
	GcScoreData_ToScore_m1029,
	Resolution_get_width_m1030,
	Resolution_set_width_m1031,
	Resolution_get_height_m1032,
	Resolution_set_height_m1033,
	Resolution_get_refreshRate_m1034,
	Resolution_set_refreshRate_m1035,
	Resolution_ToString_m1036,
	LocalUser__ctor_m1037,
	LocalUser_SetFriends_m1038,
	LocalUser_SetAuthenticated_m1039,
	LocalUser_SetUnderage_m1040,
	LocalUser_get_authenticated_m1041,
	UserProfile__ctor_m1042,
	UserProfile__ctor_m1043,
	UserProfile_ToString_m1044,
	UserProfile_SetUserName_m1045,
	UserProfile_SetUserID_m1046,
	UserProfile_SetImage_m1047,
	UserProfile_get_userName_m1048,
	UserProfile_get_id_m1049,
	UserProfile_get_isFriend_m1050,
	UserProfile_get_state_m1051,
	Achievement__ctor_m1052,
	Achievement__ctor_m1053,
	Achievement__ctor_m1054,
	Achievement_ToString_m1055,
	Achievement_get_id_m1056,
	Achievement_set_id_m1057,
	Achievement_get_percentCompleted_m1058,
	Achievement_set_percentCompleted_m1059,
	Achievement_get_completed_m1060,
	Achievement_get_hidden_m1061,
	Achievement_get_lastReportedDate_m1062,
	AchievementDescription__ctor_m1063,
	AchievementDescription_ToString_m1064,
	AchievementDescription_SetImage_m1065,
	AchievementDescription_get_id_m1066,
	AchievementDescription_set_id_m1067,
	AchievementDescription_get_title_m1068,
	AchievementDescription_get_achievedDescription_m1069,
	AchievementDescription_get_unachievedDescription_m1070,
	AchievementDescription_get_hidden_m1071,
	AchievementDescription_get_points_m1072,
	Score__ctor_m1073,
	Score__ctor_m1074,
	Score_ToString_m1075,
	Score_get_leaderboardID_m1076,
	Score_set_leaderboardID_m1077,
	Score_get_value_m1078,
	Score_set_value_m1079,
	Leaderboard__ctor_m1080,
	Leaderboard_ToString_m1081,
	Leaderboard_SetLocalUserScore_m1082,
	Leaderboard_SetMaxRange_m1083,
	Leaderboard_SetScores_m1084,
	Leaderboard_SetTitle_m1085,
	Leaderboard_GetUserFilter_m1086,
	Leaderboard_get_id_m1087,
	Leaderboard_set_id_m1088,
	Leaderboard_get_userScope_m1089,
	Leaderboard_set_userScope_m1090,
	Leaderboard_get_range_m1091,
	Leaderboard_set_range_m1092,
	Leaderboard_get_timeScope_m1093,
	Leaderboard_set_timeScope_m1094,
	HitInfo_SendMessage_m1095,
	HitInfo_Compare_m1096,
	HitInfo_op_Implicit_m1097,
	SendMouseEvents__cctor_m1098,
	SendMouseEvents_DoSendMouseEvents_m1099,
	SendMouseEvents_SendEvents_m1100,
	Range__ctor_m1101,
	SliderState__ctor_m1102,
	StackTraceUtility__ctor_m1103,
	StackTraceUtility__cctor_m1104,
	StackTraceUtility_SetProjectFolder_m1105,
	StackTraceUtility_ExtractStackTrace_m1106,
	StackTraceUtility_IsSystemStacktraceType_m1107,
	StackTraceUtility_ExtractStringFromException_m1108,
	StackTraceUtility_ExtractStringFromExceptionInternal_m1109,
	StackTraceUtility_PostprocessStacktrace_m1110,
	StackTraceUtility_ExtractFormattedStackTrace_m1111,
	UnityException__ctor_m1112,
	UnityException__ctor_m1113,
	UnityException__ctor_m1114,
	UnityException__ctor_m1115,
	SharedBetweenAnimatorsAttribute__ctor_m1116,
	StateMachineBehaviour__ctor_m1117,
	StateMachineBehaviour_OnStateEnter_m1118,
	StateMachineBehaviour_OnStateUpdate_m1119,
	StateMachineBehaviour_OnStateExit_m1120,
	StateMachineBehaviour_OnStateMove_m1121,
	StateMachineBehaviour_OnStateIK_m1122,
	StateMachineBehaviour_OnStateMachineEnter_m1123,
	StateMachineBehaviour_OnStateMachineExit_m1124,
	TextEditor__ctor_m1125,
	TextGenerationSettings_CompareColors_m1126,
	TextGenerationSettings_CompareVector2_m1127,
	TextGenerationSettings_Equals_m1128,
	TrackedReference_Equals_m1129,
	TrackedReference_GetHashCode_m1130,
	TrackedReference_op_Equality_m1131,
	ArgumentCache__ctor_m1132,
	ArgumentCache_TidyAssemblyTypeName_m1133,
	ArgumentCache_OnBeforeSerialize_m1134,
	ArgumentCache_OnAfterDeserialize_m1135,
	PersistentCall__ctor_m1136,
	PersistentCallGroup__ctor_m1137,
	InvokableCallList__ctor_m1138,
	InvokableCallList_ClearPersistent_m1139,
	UnityEventBase__ctor_m1140,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1141,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1142,
	UnityEventBase_DirtyPersistentCalls_m1143,
	UnityEventBase_ToString_m1144,
	UnityEvent__ctor_m1145,
	UserAuthorizationDialog__ctor_m1146,
	UserAuthorizationDialog_Start_m1147,
	UserAuthorizationDialog_OnGUI_m1148,
	UserAuthorizationDialog_DoUserAuthorizationDialog_m1149,
	DefaultValueAttribute__ctor_m1150,
	DefaultValueAttribute_get_Value_m1151,
	DefaultValueAttribute_Equals_m1152,
	DefaultValueAttribute_GetHashCode_m1153,
	ExcludeFromDocsAttribute__ctor_m1154,
	FormerlySerializedAsAttribute__ctor_m1155,
	TypeInferenceRuleAttribute__ctor_m1156,
	TypeInferenceRuleAttribute__ctor_m1157,
	TypeInferenceRuleAttribute_ToString_m1158,
	GenericStack__ctor_m1159,
	Locale_GetText_m1337,
	Locale_GetText_m1338,
	MonoTODOAttribute__ctor_m1339,
	MonoTODOAttribute__ctor_m1340,
	GeneratedCodeAttribute__ctor_m1341,
	HybridDictionary__ctor_m1342,
	HybridDictionary__ctor_m1343,
	HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m1344,
	HybridDictionary_get_inner_m1345,
	HybridDictionary_get_Count_m1346,
	HybridDictionary_get_Item_m1347,
	HybridDictionary_set_Item_m1348,
	HybridDictionary_get_SyncRoot_m1349,
	HybridDictionary_Add_m1350,
	HybridDictionary_Contains_m1351,
	HybridDictionary_CopyTo_m1352,
	HybridDictionary_GetEnumerator_m1353,
	HybridDictionary_Remove_m1354,
	HybridDictionary_Switch_m1355,
	DictionaryNode__ctor_m1356,
	DictionaryNodeEnumerator__ctor_m1357,
	DictionaryNodeEnumerator_FailFast_m1358,
	DictionaryNodeEnumerator_MoveNext_m1359,
	DictionaryNodeEnumerator_Reset_m1360,
	DictionaryNodeEnumerator_get_Current_m1361,
	DictionaryNodeEnumerator_get_DictionaryNode_m1362,
	DictionaryNodeEnumerator_get_Entry_m1363,
	DictionaryNodeEnumerator_get_Key_m1364,
	DictionaryNodeEnumerator_get_Value_m1365,
	ListDictionary__ctor_m1366,
	ListDictionary__ctor_m1367,
	ListDictionary_System_Collections_IEnumerable_GetEnumerator_m1368,
	ListDictionary_FindEntry_m1369,
	ListDictionary_FindEntry_m1370,
	ListDictionary_AddImpl_m1371,
	ListDictionary_get_Count_m1372,
	ListDictionary_get_SyncRoot_m1373,
	ListDictionary_CopyTo_m1374,
	ListDictionary_get_Item_m1375,
	ListDictionary_set_Item_m1376,
	ListDictionary_Add_m1377,
	ListDictionary_Clear_m1378,
	ListDictionary_Contains_m1379,
	ListDictionary_GetEnumerator_m1380,
	ListDictionary_Remove_m1381,
	_Item__ctor_m1382,
	_KeysEnumerator__ctor_m1383,
	_KeysEnumerator_get_Current_m1384,
	_KeysEnumerator_MoveNext_m1385,
	_KeysEnumerator_Reset_m1386,
	KeysCollection__ctor_m1387,
	KeysCollection_System_Collections_ICollection_CopyTo_m1388,
	KeysCollection_System_Collections_ICollection_get_SyncRoot_m1389,
	KeysCollection_get_Count_m1390,
	KeysCollection_GetEnumerator_m1391,
	NameObjectCollectionBase__ctor_m1392,
	NameObjectCollectionBase__ctor_m1393,
	NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m1394,
	NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m1395,
	NameObjectCollectionBase_Init_m1396,
	NameObjectCollectionBase_get_Keys_m1397,
	NameObjectCollectionBase_GetEnumerator_m1398,
	NameObjectCollectionBase_GetObjectData_m1399,
	NameObjectCollectionBase_get_Count_m1400,
	NameObjectCollectionBase_OnDeserialization_m1401,
	NameObjectCollectionBase_get_IsReadOnly_m1402,
	NameObjectCollectionBase_BaseAdd_m1403,
	NameObjectCollectionBase_BaseGet_m1404,
	NameObjectCollectionBase_BaseGet_m1405,
	NameObjectCollectionBase_BaseGetKey_m1406,
	NameObjectCollectionBase_FindFirstMatchedItem_m1407,
	NameValueCollection__ctor_m1408,
	NameValueCollection__ctor_m1409,
	NameValueCollection_Add_m1410,
	NameValueCollection_Get_m1411,
	NameValueCollection_AsSingleString_m1412,
	NameValueCollection_GetKey_m1413,
	NameValueCollection_InvalidateCachedArrays_m1414,
	DefaultValueAttribute__ctor_m1415,
	DefaultValueAttribute_get_Value_m1416,
	DefaultValueAttribute_Equals_m1417,
	DefaultValueAttribute_GetHashCode_m1418,
	EditorBrowsableAttribute__ctor_m1419,
	EditorBrowsableAttribute_get_State_m1420,
	EditorBrowsableAttribute_Equals_m1421,
	EditorBrowsableAttribute_GetHashCode_m1422,
	TypeConverterAttribute__ctor_m1423,
	TypeConverterAttribute__ctor_m1424,
	TypeConverterAttribute__cctor_m1425,
	TypeConverterAttribute_Equals_m1426,
	TypeConverterAttribute_GetHashCode_m1427,
	TypeConverterAttribute_get_ConverterTypeName_m1428,
	DefaultCertificatePolicy__ctor_m1429,
	DefaultCertificatePolicy_CheckValidationResult_m1430,
	FileWebRequest__ctor_m1431,
	FileWebRequest__ctor_m1432,
	FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m1433,
	FileWebRequest_GetObjectData_m1434,
	FileWebRequestCreator__ctor_m1435,
	FileWebRequestCreator_Create_m1436,
	FtpRequestCreator__ctor_m1437,
	FtpRequestCreator_Create_m1438,
	FtpWebRequest__ctor_m1439,
	FtpWebRequest__cctor_m1440,
	FtpWebRequest_U3CcallbackU3Em__B_m1441,
	GlobalProxySelection_get_Select_m1442,
	HttpRequestCreator__ctor_m1443,
	HttpRequestCreator_Create_m1444,
	HttpVersion__cctor_m1445,
	HttpWebRequest__ctor_m1446,
	HttpWebRequest__ctor_m1447,
	HttpWebRequest__cctor_m1448,
	HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m1449,
	HttpWebRequest_get_Address_m1450,
	HttpWebRequest_get_ServicePoint_m1451,
	HttpWebRequest_GetServicePoint_m1452,
	HttpWebRequest_GetObjectData_m1453,
	IPAddress__ctor_m1454,
	IPAddress__ctor_m1455,
	IPAddress__cctor_m1456,
	IPAddress_SwapShort_m1457,
	IPAddress_HostToNetworkOrder_m1458,
	IPAddress_NetworkToHostOrder_m1459,
	IPAddress_Parse_m1460,
	IPAddress_TryParse_m1461,
	IPAddress_ParseIPV4_m1462,
	IPAddress_ParseIPV6_m1463,
	IPAddress_get_InternalIPv4Address_m1464,
	IPAddress_get_ScopeId_m1465,
	IPAddress_get_AddressFamily_m1466,
	IPAddress_IsLoopback_m1467,
	IPAddress_ToString_m1468,
	IPAddress_ToString_m1469,
	IPAddress_Equals_m1470,
	IPAddress_GetHashCode_m1471,
	IPAddress_Hash_m1472,
	IPv6Address__ctor_m1473,
	IPv6Address__ctor_m1474,
	IPv6Address__ctor_m1475,
	IPv6Address__cctor_m1476,
	IPv6Address_Parse_m1477,
	IPv6Address_Fill_m1478,
	IPv6Address_TryParse_m1479,
	IPv6Address_TryParse_m1480,
	IPv6Address_get_Address_m1481,
	IPv6Address_get_ScopeId_m1482,
	IPv6Address_set_ScopeId_m1483,
	IPv6Address_IsLoopback_m1484,
	IPv6Address_SwapUShort_m1485,
	IPv6Address_AsIPv4Int_m1486,
	IPv6Address_IsIPv4Compatible_m1487,
	IPv6Address_IsIPv4Mapped_m1488,
	IPv6Address_ToString_m1489,
	IPv6Address_ToString_m1490,
	IPv6Address_Equals_m1491,
	IPv6Address_GetHashCode_m1492,
	IPv6Address_Hash_m1493,
	ServicePoint__ctor_m1494,
	ServicePoint_get_Address_m1495,
	ServicePoint_get_CurrentConnections_m1496,
	ServicePoint_get_IdleSince_m1497,
	ServicePoint_set_IdleSince_m1498,
	ServicePoint_set_Expect100Continue_m1499,
	ServicePoint_set_UseNagleAlgorithm_m1500,
	ServicePoint_set_SendContinue_m1501,
	ServicePoint_set_UsesProxy_m1502,
	ServicePoint_set_UseConnect_m1503,
	ServicePoint_get_AvailableForRecycling_m1504,
	SPKey__ctor_m1505,
	SPKey_GetHashCode_m1506,
	SPKey_Equals_m1507,
	ServicePointManager__cctor_m1508,
	ServicePointManager_get_CertificatePolicy_m1509,
	ServicePointManager_get_CheckCertificateRevocationList_m1510,
	ServicePointManager_get_SecurityProtocol_m1511,
	ServicePointManager_get_ServerCertificateValidationCallback_m1512,
	ServicePointManager_FindServicePoint_m1513,
	ServicePointManager_RecycleServicePoints_m1514,
	WebHeaderCollection__ctor_m1515,
	WebHeaderCollection__ctor_m1516,
	WebHeaderCollection__ctor_m1517,
	WebHeaderCollection__cctor_m1518,
	WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m1519,
	WebHeaderCollection_Add_m1520,
	WebHeaderCollection_AddWithoutValidate_m1521,
	WebHeaderCollection_IsRestricted_m1522,
	WebHeaderCollection_OnDeserialization_m1523,
	WebHeaderCollection_ToString_m1524,
	WebHeaderCollection_GetObjectData_m1525,
	WebHeaderCollection_get_Count_m1526,
	WebHeaderCollection_get_Keys_m1527,
	WebHeaderCollection_Get_m1528,
	WebHeaderCollection_GetKey_m1529,
	WebHeaderCollection_GetEnumerator_m1530,
	WebHeaderCollection_IsHeaderValue_m1531,
	WebHeaderCollection_IsHeaderName_m1532,
	WebProxy__ctor_m1533,
	WebProxy__ctor_m1534,
	WebProxy__ctor_m1535,
	WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m1536,
	WebProxy_get_UseDefaultCredentials_m1537,
	WebProxy_GetProxy_m1538,
	WebProxy_IsBypassed_m1539,
	WebProxy_GetObjectData_m1540,
	WebProxy_CheckBypassList_m1541,
	WebRequest__ctor_m1542,
	WebRequest__ctor_m1543,
	WebRequest__cctor_m1544,
	WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m1545,
	WebRequest_AddDynamicPrefix_m1546,
	WebRequest_GetMustImplement_m1547,
	WebRequest_get_DefaultWebProxy_m1548,
	WebRequest_GetDefaultWebProxy_m1549,
	WebRequest_GetObjectData_m1550,
	WebRequest_AddPrefix_m1551,
	PublicKey__ctor_m1552,
	PublicKey_get_EncodedKeyValue_m1553,
	PublicKey_get_EncodedParameters_m1554,
	PublicKey_get_Key_m1555,
	PublicKey_get_Oid_m1556,
	PublicKey_GetUnsignedBigInteger_m1557,
	PublicKey_DecodeDSA_m1558,
	PublicKey_DecodeRSA_m1559,
	X500DistinguishedName__ctor_m1560,
	X500DistinguishedName_Decode_m1561,
	X500DistinguishedName_GetSeparator_m1562,
	X500DistinguishedName_DecodeRawData_m1563,
	X500DistinguishedName_Canonize_m1564,
	X500DistinguishedName_AreEqual_m1565,
	X509BasicConstraintsExtension__ctor_m1566,
	X509BasicConstraintsExtension__ctor_m1567,
	X509BasicConstraintsExtension__ctor_m1568,
	X509BasicConstraintsExtension_get_CertificateAuthority_m1569,
	X509BasicConstraintsExtension_get_HasPathLengthConstraint_m1570,
	X509BasicConstraintsExtension_get_PathLengthConstraint_m1571,
	X509BasicConstraintsExtension_CopyFrom_m1572,
	X509BasicConstraintsExtension_Decode_m1573,
	X509BasicConstraintsExtension_Encode_m1574,
	X509BasicConstraintsExtension_ToString_m1575,
	X509Certificate2__ctor_m1576,
	X509Certificate2__cctor_m1577,
	X509Certificate2_get_Extensions_m1578,
	X509Certificate2_get_IssuerName_m1579,
	X509Certificate2_get_NotAfter_m1580,
	X509Certificate2_get_NotBefore_m1581,
	X509Certificate2_get_PrivateKey_m1582,
	X509Certificate2_get_PublicKey_m1583,
	X509Certificate2_get_SerialNumber_m1584,
	X509Certificate2_get_SignatureAlgorithm_m1585,
	X509Certificate2_get_SubjectName_m1586,
	X509Certificate2_get_Thumbprint_m1587,
	X509Certificate2_get_Version_m1588,
	X509Certificate2_GetNameInfo_m1589,
	X509Certificate2_Find_m1590,
	X509Certificate2_GetValueAsString_m1591,
	X509Certificate2_ImportPkcs12_m1592,
	X509Certificate2_Import_m1593,
	X509Certificate2_Reset_m1594,
	X509Certificate2_ToString_m1595,
	X509Certificate2_ToString_m1596,
	X509Certificate2_AppendBuffer_m1597,
	X509Certificate2_Verify_m1598,
	X509Certificate2_get_MonoCertificate_m1599,
	X509Certificate2Collection__ctor_m1600,
	X509Certificate2Collection__ctor_m1601,
	X509Certificate2Collection_get_Item_m1602,
	X509Certificate2Collection_Add_m1603,
	X509Certificate2Collection_AddRange_m1604,
	X509Certificate2Collection_Contains_m1605,
	X509Certificate2Collection_Find_m1606,
	X509Certificate2Collection_GetEnumerator_m1607,
	X509Certificate2Enumerator__ctor_m1608,
	X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m1609,
	X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m1610,
	X509Certificate2Enumerator_get_Current_m1611,
	X509Certificate2Enumerator_MoveNext_m1612,
	X509CertificateEnumerator__ctor_m1613,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m1614,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m1615,
	X509CertificateEnumerator_get_Current_m1616,
	X509CertificateEnumerator_MoveNext_m1617,
	X509CertificateCollection__ctor_m1618,
	X509CertificateCollection__ctor_m1619,
	X509CertificateCollection_get_Item_m1620,
	X509CertificateCollection_AddRange_m1621,
	X509CertificateCollection_GetEnumerator_m1622,
	X509CertificateCollection_GetHashCode_m1623,
	X509Chain__ctor_m1624,
	X509Chain__ctor_m1625,
	X509Chain__cctor_m1626,
	X509Chain_get_ChainPolicy_m1627,
	X509Chain_Build_m1628,
	X509Chain_Reset_m1629,
	X509Chain_get_Roots_m1630,
	X509Chain_get_CertificateAuthorities_m1631,
	X509Chain_get_CertificateCollection_m1632,
	X509Chain_BuildChainFrom_m1633,
	X509Chain_SelectBestFromCollection_m1634,
	X509Chain_FindParent_m1635,
	X509Chain_IsChainComplete_m1636,
	X509Chain_IsSelfIssued_m1637,
	X509Chain_ValidateChain_m1638,
	X509Chain_Process_m1639,
	X509Chain_PrepareForNextCertificate_m1640,
	X509Chain_WrapUp_m1641,
	X509Chain_ProcessCertificateExtensions_m1642,
	X509Chain_IsSignedWith_m1643,
	X509Chain_GetSubjectKeyIdentifier_m1644,
	X509Chain_GetAuthorityKeyIdentifier_m1645,
	X509Chain_GetAuthorityKeyIdentifier_m1646,
	X509Chain_GetAuthorityKeyIdentifier_m1647,
	X509Chain_CheckRevocationOnChain_m1648,
	X509Chain_CheckRevocation_m1649,
	X509Chain_CheckRevocation_m1650,
	X509Chain_FindCrl_m1651,
	X509Chain_ProcessCrlExtensions_m1652,
	X509Chain_ProcessCrlEntryExtensions_m1653,
	X509ChainElement__ctor_m1654,
	X509ChainElement_get_Certificate_m1655,
	X509ChainElement_get_ChainElementStatus_m1656,
	X509ChainElement_get_StatusFlags_m1657,
	X509ChainElement_set_StatusFlags_m1658,
	X509ChainElement_Count_m1659,
	X509ChainElement_Set_m1660,
	X509ChainElement_UncompressFlags_m1661,
	X509ChainElementCollection__ctor_m1662,
	X509ChainElementCollection_System_Collections_ICollection_CopyTo_m1663,
	X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m1664,
	X509ChainElementCollection_get_Count_m1665,
	X509ChainElementCollection_get_Item_m1666,
	X509ChainElementCollection_get_SyncRoot_m1667,
	X509ChainElementCollection_GetEnumerator_m1668,
	X509ChainElementCollection_Add_m1669,
	X509ChainElementCollection_Clear_m1670,
	X509ChainElementCollection_Contains_m1671,
	X509ChainElementEnumerator__ctor_m1672,
	X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m1673,
	X509ChainElementEnumerator_get_Current_m1674,
	X509ChainElementEnumerator_MoveNext_m1675,
	X509ChainPolicy__ctor_m1676,
	X509ChainPolicy_get_ExtraStore_m1677,
	X509ChainPolicy_get_RevocationFlag_m1678,
	X509ChainPolicy_get_RevocationMode_m1679,
	X509ChainPolicy_get_VerificationFlags_m1680,
	X509ChainPolicy_get_VerificationTime_m1681,
	X509ChainPolicy_Reset_m1682,
	X509ChainStatus__ctor_m1683,
	X509ChainStatus_get_Status_m1684,
	X509ChainStatus_set_Status_m1685,
	X509ChainStatus_set_StatusInformation_m1686,
	X509ChainStatus_GetInformation_m1687,
	X509EnhancedKeyUsageExtension__ctor_m1688,
	X509EnhancedKeyUsageExtension_CopyFrom_m1689,
	X509EnhancedKeyUsageExtension_Decode_m1690,
	X509EnhancedKeyUsageExtension_ToString_m1691,
	X509Extension__ctor_m1692,
	X509Extension__ctor_m1693,
	X509Extension_get_Critical_m1694,
	X509Extension_set_Critical_m1695,
	X509Extension_CopyFrom_m1696,
	X509Extension_FormatUnkownData_m1697,
	X509ExtensionCollection__ctor_m1698,
	X509ExtensionCollection_System_Collections_ICollection_CopyTo_m1699,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m1700,
	X509ExtensionCollection_get_Count_m1701,
	X509ExtensionCollection_get_SyncRoot_m1702,
	X509ExtensionCollection_get_Item_m1703,
	X509ExtensionCollection_GetEnumerator_m1704,
	X509ExtensionEnumerator__ctor_m1705,
	X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m1706,
	X509ExtensionEnumerator_get_Current_m1707,
	X509ExtensionEnumerator_MoveNext_m1708,
	X509KeyUsageExtension__ctor_m1709,
	X509KeyUsageExtension__ctor_m1710,
	X509KeyUsageExtension__ctor_m1711,
	X509KeyUsageExtension_get_KeyUsages_m1712,
	X509KeyUsageExtension_CopyFrom_m1713,
	X509KeyUsageExtension_GetValidFlags_m1714,
	X509KeyUsageExtension_Decode_m1715,
	X509KeyUsageExtension_Encode_m1716,
	X509KeyUsageExtension_ToString_m1717,
	X509Store__ctor_m1718,
	X509Store_get_Certificates_m1719,
	X509Store_get_Factory_m1720,
	X509Store_get_Store_m1721,
	X509Store_Close_m1722,
	X509Store_Open_m1723,
	X509SubjectKeyIdentifierExtension__ctor_m1724,
	X509SubjectKeyIdentifierExtension__ctor_m1725,
	X509SubjectKeyIdentifierExtension__ctor_m1726,
	X509SubjectKeyIdentifierExtension__ctor_m1727,
	X509SubjectKeyIdentifierExtension__ctor_m1728,
	X509SubjectKeyIdentifierExtension__ctor_m1729,
	X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m1730,
	X509SubjectKeyIdentifierExtension_CopyFrom_m1731,
	X509SubjectKeyIdentifierExtension_FromHexChar_m1732,
	X509SubjectKeyIdentifierExtension_FromHexChars_m1733,
	X509SubjectKeyIdentifierExtension_FromHex_m1734,
	X509SubjectKeyIdentifierExtension_Decode_m1735,
	X509SubjectKeyIdentifierExtension_Encode_m1736,
	X509SubjectKeyIdentifierExtension_ToString_m1737,
	AsnEncodedData__ctor_m1738,
	AsnEncodedData__ctor_m1739,
	AsnEncodedData__ctor_m1740,
	AsnEncodedData_get_Oid_m1741,
	AsnEncodedData_set_Oid_m1742,
	AsnEncodedData_get_RawData_m1743,
	AsnEncodedData_set_RawData_m1744,
	AsnEncodedData_CopyFrom_m1745,
	AsnEncodedData_ToString_m1746,
	AsnEncodedData_Default_m1747,
	AsnEncodedData_BasicConstraintsExtension_m1748,
	AsnEncodedData_EnhancedKeyUsageExtension_m1749,
	AsnEncodedData_KeyUsageExtension_m1750,
	AsnEncodedData_SubjectKeyIdentifierExtension_m1751,
	AsnEncodedData_SubjectAltName_m1752,
	AsnEncodedData_NetscapeCertType_m1753,
	Oid__ctor_m1754,
	Oid__ctor_m1755,
	Oid__ctor_m1756,
	Oid__ctor_m1757,
	Oid_get_FriendlyName_m1758,
	Oid_get_Value_m1759,
	Oid_GetName_m1760,
	OidCollection__ctor_m1761,
	OidCollection_System_Collections_ICollection_CopyTo_m1762,
	OidCollection_System_Collections_IEnumerable_GetEnumerator_m1763,
	OidCollection_get_Count_m1764,
	OidCollection_get_Item_m1765,
	OidCollection_get_SyncRoot_m1766,
	OidCollection_Add_m1767,
	OidEnumerator__ctor_m1768,
	OidEnumerator_System_Collections_IEnumerator_get_Current_m1769,
	OidEnumerator_MoveNext_m1770,
	MatchAppendEvaluator__ctor_m1771,
	MatchAppendEvaluator_Invoke_m1772,
	MatchAppendEvaluator_BeginInvoke_m1773,
	MatchAppendEvaluator_EndInvoke_m1774,
	BaseMachine__ctor_m1775,
	BaseMachine_Replace_m1776,
	BaseMachine_Scan_m1777,
	BaseMachine_LTRReplace_m1778,
	BaseMachine_RTLReplace_m1779,
	Capture__ctor_m1780,
	Capture__ctor_m1781,
	Capture_get_Index_m1782,
	Capture_get_Length_m1783,
	Capture_get_Value_m1784,
	Capture_ToString_m1785,
	Capture_get_Text_m1786,
	CaptureCollection__ctor_m1787,
	CaptureCollection_get_Count_m1788,
	CaptureCollection_SetValue_m1789,
	CaptureCollection_get_SyncRoot_m1790,
	CaptureCollection_CopyTo_m1791,
	CaptureCollection_GetEnumerator_m1792,
	Group__ctor_m1793,
	Group__ctor_m1794,
	Group__ctor_m1795,
	Group__cctor_m1796,
	Group_get_Captures_m1797,
	Group_get_Success_m1798,
	GroupCollection__ctor_m1799,
	GroupCollection_get_Count_m1800,
	GroupCollection_get_Item_m1801,
	GroupCollection_SetValue_m1802,
	GroupCollection_get_SyncRoot_m1803,
	GroupCollection_CopyTo_m1804,
	GroupCollection_GetEnumerator_m1805,
	Match__ctor_m1806,
	Match__ctor_m1807,
	Match__ctor_m1808,
	Match__cctor_m1809,
	Match_get_Empty_m1810,
	Match_get_Groups_m1811,
	Match_NextMatch_m1812,
	Match_get_Regex_m1813,
	Enumerator__ctor_m1814,
	Enumerator_System_Collections_IEnumerator_get_Current_m1815,
	Enumerator_System_Collections_IEnumerator_MoveNext_m1816,
	MatchCollection__ctor_m1817,
	MatchCollection_get_Count_m1818,
	MatchCollection_get_Item_m1819,
	MatchCollection_get_SyncRoot_m1820,
	MatchCollection_CopyTo_m1821,
	MatchCollection_GetEnumerator_m1822,
	MatchCollection_TryToGet_m1823,
	MatchCollection_get_FullList_m1824,
	Regex__ctor_m1825,
	Regex__ctor_m1826,
	Regex__ctor_m1827,
	Regex__ctor_m1828,
	Regex__cctor_m1829,
	Regex_System_Runtime_Serialization_ISerializable_GetObjectData_m1830,
	Regex_Replace_m1332,
	Regex_Replace_m1831,
	Regex_validate_options_m1832,
	Regex_Init_m1833,
	Regex_InitNewRegex_m1834,
	Regex_CreateMachineFactory_m1835,
	Regex_get_Options_m1836,
	Regex_get_RightToLeft_m1837,
	Regex_GroupNumberFromName_m1838,
	Regex_GetGroupIndex_m1839,
	Regex_default_startat_m1840,
	Regex_IsMatch_m1841,
	Regex_IsMatch_m1842,
	Regex_Match_m1843,
	Regex_Matches_m1844,
	Regex_Matches_m1845,
	Regex_Replace_m1846,
	Regex_Replace_m1847,
	Regex_ToString_m1848,
	Regex_get_GroupCount_m1849,
	Regex_get_Gap_m1850,
	Regex_CreateMachine_m1851,
	Regex_GetGroupNamesArray_m1852,
	Regex_get_GroupNumbers_m1853,
	Key__ctor_m1854,
	Key_GetHashCode_m1855,
	Key_Equals_m1856,
	Key_ToString_m1857,
	FactoryCache__ctor_m1858,
	FactoryCache_Add_m1859,
	FactoryCache_Cleanup_m1860,
	FactoryCache_Lookup_m1861,
	Node__ctor_m1862,
	MRUList__ctor_m1863,
	MRUList_Use_m1864,
	MRUList_Evict_m1865,
	CategoryUtils_CategoryFromName_m1866,
	CategoryUtils_IsCategory_m1867,
	CategoryUtils_IsCategory_m1868,
	LinkRef__ctor_m1869,
	InterpreterFactory__ctor_m1870,
	InterpreterFactory_NewInstance_m1871,
	InterpreterFactory_get_GroupCount_m1872,
	InterpreterFactory_get_Gap_m1873,
	InterpreterFactory_set_Gap_m1874,
	InterpreterFactory_get_Mapping_m1875,
	InterpreterFactory_set_Mapping_m1876,
	InterpreterFactory_get_NamesMapping_m1877,
	InterpreterFactory_set_NamesMapping_m1878,
	PatternLinkStack__ctor_m1879,
	PatternLinkStack_set_BaseAddress_m1880,
	PatternLinkStack_get_OffsetAddress_m1881,
	PatternLinkStack_set_OffsetAddress_m1882,
	PatternLinkStack_GetOffset_m1883,
	PatternLinkStack_GetCurrent_m1884,
	PatternLinkStack_SetCurrent_m1885,
	PatternCompiler__ctor_m1886,
	PatternCompiler_EncodeOp_m1887,
	PatternCompiler_GetMachineFactory_m1888,
	PatternCompiler_EmitFalse_m1889,
	PatternCompiler_EmitTrue_m1890,
	PatternCompiler_EmitCount_m1891,
	PatternCompiler_EmitCharacter_m1892,
	PatternCompiler_EmitCategory_m1893,
	PatternCompiler_EmitNotCategory_m1894,
	PatternCompiler_EmitRange_m1895,
	PatternCompiler_EmitSet_m1896,
	PatternCompiler_EmitString_m1897,
	PatternCompiler_EmitPosition_m1898,
	PatternCompiler_EmitOpen_m1899,
	PatternCompiler_EmitClose_m1900,
	PatternCompiler_EmitBalanceStart_m1901,
	PatternCompiler_EmitBalance_m1902,
	PatternCompiler_EmitReference_m1903,
	PatternCompiler_EmitIfDefined_m1904,
	PatternCompiler_EmitSub_m1905,
	PatternCompiler_EmitTest_m1906,
	PatternCompiler_EmitBranch_m1907,
	PatternCompiler_EmitJump_m1908,
	PatternCompiler_EmitRepeat_m1909,
	PatternCompiler_EmitUntil_m1910,
	PatternCompiler_EmitFastRepeat_m1911,
	PatternCompiler_EmitIn_m1912,
	PatternCompiler_EmitAnchor_m1913,
	PatternCompiler_EmitInfo_m1914,
	PatternCompiler_NewLink_m1915,
	PatternCompiler_ResolveLink_m1916,
	PatternCompiler_EmitBranchEnd_m1917,
	PatternCompiler_EmitAlternationEnd_m1918,
	PatternCompiler_MakeFlags_m1919,
	PatternCompiler_Emit_m1920,
	PatternCompiler_Emit_m1921,
	PatternCompiler_Emit_m1922,
	PatternCompiler_get_CurrentAddress_m1923,
	PatternCompiler_BeginLink_m1924,
	PatternCompiler_EmitLink_m1925,
	LinkStack__ctor_m1926,
	LinkStack_Push_m1927,
	LinkStack_Pop_m1928,
	Mark_get_IsDefined_m1929,
	Mark_get_Index_m1930,
	Mark_get_Length_m1931,
	IntStack_Pop_m1932,
	IntStack_Push_m1933,
	IntStack_get_Count_m1934,
	IntStack_set_Count_m1935,
	RepeatContext__ctor_m1936,
	RepeatContext_get_Count_m1937,
	RepeatContext_set_Count_m1938,
	RepeatContext_get_Start_m1939,
	RepeatContext_set_Start_m1940,
	RepeatContext_get_IsMinimum_m1941,
	RepeatContext_get_IsMaximum_m1942,
	RepeatContext_get_IsLazy_m1943,
	RepeatContext_get_Expression_m1944,
	RepeatContext_get_Previous_m1945,
	Interpreter__ctor_m1946,
	Interpreter_ReadProgramCount_m1947,
	Interpreter_Scan_m1948,
	Interpreter_Reset_m1949,
	Interpreter_Eval_m1950,
	Interpreter_EvalChar_m1951,
	Interpreter_TryMatch_m1952,
	Interpreter_IsPosition_m1953,
	Interpreter_IsWordChar_m1954,
	Interpreter_GetString_m1955,
	Interpreter_Open_m1956,
	Interpreter_Close_m1957,
	Interpreter_Balance_m1958,
	Interpreter_Checkpoint_m1959,
	Interpreter_Backtrack_m1960,
	Interpreter_ResetGroups_m1961,
	Interpreter_GetLastDefined_m1962,
	Interpreter_CreateMark_m1963,
	Interpreter_GetGroupInfo_m1964,
	Interpreter_PopulateGroup_m1965,
	Interpreter_GenerateMatch_m1966,
	Interval__ctor_m1967,
	Interval_get_Empty_m1968,
	Interval_get_IsDiscontiguous_m1969,
	Interval_get_IsSingleton_m1970,
	Interval_get_IsEmpty_m1971,
	Interval_get_Size_m1972,
	Interval_IsDisjoint_m1973,
	Interval_IsAdjacent_m1974,
	Interval_Contains_m1975,
	Interval_Contains_m1976,
	Interval_Intersects_m1977,
	Interval_Merge_m1978,
	Interval_CompareTo_m1979,
	Enumerator__ctor_m1980,
	Enumerator_get_Current_m1981,
	Enumerator_MoveNext_m1982,
	Enumerator_Reset_m1983,
	CostDelegate__ctor_m1984,
	CostDelegate_Invoke_m1985,
	CostDelegate_BeginInvoke_m1986,
	CostDelegate_EndInvoke_m1987,
	IntervalCollection__ctor_m1988,
	IntervalCollection_get_Item_m1989,
	IntervalCollection_Add_m1990,
	IntervalCollection_Normalize_m1991,
	IntervalCollection_GetMetaCollection_m1992,
	IntervalCollection_Optimize_m1993,
	IntervalCollection_get_Count_m1994,
	IntervalCollection_get_SyncRoot_m1995,
	IntervalCollection_CopyTo_m1996,
	IntervalCollection_GetEnumerator_m1997,
	Parser__ctor_m1998,
	Parser_ParseDecimal_m1999,
	Parser_ParseOctal_m2000,
	Parser_ParseHex_m2001,
	Parser_ParseNumber_m2002,
	Parser_ParseName_m2003,
	Parser_ParseRegularExpression_m2004,
	Parser_GetMapping_m2005,
	Parser_ParseGroup_m2006,
	Parser_ParseGroupingConstruct_m2007,
	Parser_ParseAssertionType_m2008,
	Parser_ParseOptions_m2009,
	Parser_ParseCharacterClass_m2010,
	Parser_ParseRepetitionBounds_m2011,
	Parser_ParseUnicodeCategory_m2012,
	Parser_ParseSpecial_m2013,
	Parser_ParseEscape_m2014,
	Parser_ParseName_m2015,
	Parser_IsNameChar_m2016,
	Parser_ParseNumber_m2017,
	Parser_ParseDigit_m2018,
	Parser_ConsumeWhitespace_m2019,
	Parser_ResolveReferences_m2020,
	Parser_HandleExplicitNumericGroups_m2021,
	Parser_IsIgnoreCase_m2022,
	Parser_IsMultiline_m2023,
	Parser_IsExplicitCapture_m2024,
	Parser_IsSingleline_m2025,
	Parser_IsIgnorePatternWhitespace_m2026,
	Parser_IsECMAScript_m2027,
	Parser_NewParseException_m2028,
	QuickSearch__ctor_m2029,
	QuickSearch__cctor_m2030,
	QuickSearch_get_Length_m2031,
	QuickSearch_Search_m2032,
	QuickSearch_SetupShiftTable_m2033,
	QuickSearch_GetShiftDistance_m2034,
	QuickSearch_GetChar_m2035,
	ReplacementEvaluator__ctor_m2036,
	ReplacementEvaluator_Evaluate_m2037,
	ReplacementEvaluator_EvaluateAppend_m2038,
	ReplacementEvaluator_get_NeedsGroupsOrCaptures_m2039,
	ReplacementEvaluator_Ensure_m2040,
	ReplacementEvaluator_AddFromReplacement_m2041,
	ReplacementEvaluator_AddInt_m2042,
	ReplacementEvaluator_Compile_m2043,
	ReplacementEvaluator_CompileTerm_m2044,
	ExpressionCollection__ctor_m2045,
	ExpressionCollection_Add_m2046,
	ExpressionCollection_get_Item_m2047,
	ExpressionCollection_set_Item_m2048,
	ExpressionCollection_OnValidate_m2049,
	Expression__ctor_m2050,
	Expression_GetFixedWidth_m2051,
	Expression_GetAnchorInfo_m2052,
	CompositeExpression__ctor_m2053,
	CompositeExpression_get_Expressions_m2054,
	CompositeExpression_GetWidth_m2055,
	CompositeExpression_IsComplex_m2056,
	Group__ctor_m2057,
	Group_AppendExpression_m2058,
	Group_Compile_m2059,
	Group_GetWidth_m2060,
	Group_GetAnchorInfo_m2061,
	RegularExpression__ctor_m2062,
	RegularExpression_set_GroupCount_m2063,
	RegularExpression_Compile_m2064,
	CapturingGroup__ctor_m2065,
	CapturingGroup_get_Index_m2066,
	CapturingGroup_set_Index_m2067,
	CapturingGroup_get_Name_m2068,
	CapturingGroup_set_Name_m2069,
	CapturingGroup_get_IsNamed_m2070,
	CapturingGroup_Compile_m2071,
	CapturingGroup_IsComplex_m2072,
	CapturingGroup_CompareTo_m2073,
	BalancingGroup__ctor_m2074,
	BalancingGroup_set_Balance_m2075,
	BalancingGroup_Compile_m2076,
	NonBacktrackingGroup__ctor_m2077,
	NonBacktrackingGroup_Compile_m2078,
	NonBacktrackingGroup_IsComplex_m2079,
	Repetition__ctor_m2080,
	Repetition_get_Expression_m2081,
	Repetition_set_Expression_m2082,
	Repetition_get_Minimum_m2083,
	Repetition_Compile_m2084,
	Repetition_GetWidth_m2085,
	Repetition_GetAnchorInfo_m2086,
	Assertion__ctor_m2087,
	Assertion_get_TrueExpression_m2088,
	Assertion_set_TrueExpression_m2089,
	Assertion_get_FalseExpression_m2090,
	Assertion_set_FalseExpression_m2091,
	Assertion_GetWidth_m2092,
	CaptureAssertion__ctor_m2093,
	CaptureAssertion_set_CapturingGroup_m2094,
	CaptureAssertion_Compile_m2095,
	CaptureAssertion_IsComplex_m2096,
	CaptureAssertion_get_Alternate_m2097,
	ExpressionAssertion__ctor_m2098,
	ExpressionAssertion_set_Reverse_m2099,
	ExpressionAssertion_set_Negate_m2100,
	ExpressionAssertion_get_TestExpression_m2101,
	ExpressionAssertion_set_TestExpression_m2102,
	ExpressionAssertion_Compile_m2103,
	ExpressionAssertion_IsComplex_m2104,
	Alternation__ctor_m2105,
	Alternation_get_Alternatives_m2106,
	Alternation_AddAlternative_m2107,
	Alternation_Compile_m2108,
	Alternation_GetWidth_m2109,
	Literal__ctor_m2110,
	Literal_CompileLiteral_m2111,
	Literal_Compile_m2112,
	Literal_GetWidth_m2113,
	Literal_GetAnchorInfo_m2114,
	Literal_IsComplex_m2115,
	PositionAssertion__ctor_m2116,
	PositionAssertion_Compile_m2117,
	PositionAssertion_GetWidth_m2118,
	PositionAssertion_IsComplex_m2119,
	PositionAssertion_GetAnchorInfo_m2120,
	Reference__ctor_m2121,
	Reference_get_CapturingGroup_m2122,
	Reference_set_CapturingGroup_m2123,
	Reference_get_IgnoreCase_m2124,
	Reference_Compile_m2125,
	Reference_GetWidth_m2126,
	Reference_IsComplex_m2127,
	BackslashNumber__ctor_m2128,
	BackslashNumber_ResolveReference_m2129,
	BackslashNumber_Compile_m2130,
	CharacterClass__ctor_m2131,
	CharacterClass__ctor_m2132,
	CharacterClass__cctor_m2133,
	CharacterClass_AddCategory_m2134,
	CharacterClass_AddCharacter_m2135,
	CharacterClass_AddRange_m2136,
	CharacterClass_Compile_m2137,
	CharacterClass_GetWidth_m2138,
	CharacterClass_IsComplex_m2139,
	CharacterClass_GetIntervalCost_m2140,
	AnchorInfo__ctor_m2141,
	AnchorInfo__ctor_m2142,
	AnchorInfo__ctor_m2143,
	AnchorInfo_get_Offset_m2144,
	AnchorInfo_get_Width_m2145,
	AnchorInfo_get_Length_m2146,
	AnchorInfo_get_IsUnknownWidth_m2147,
	AnchorInfo_get_IsComplete_m2148,
	AnchorInfo_get_Substring_m2149,
	AnchorInfo_get_IgnoreCase_m2150,
	AnchorInfo_get_Position_m2151,
	AnchorInfo_get_IsSubstring_m2152,
	AnchorInfo_get_IsPosition_m2153,
	AnchorInfo_GetInterval_m2154,
	DefaultUriParser__ctor_m2155,
	DefaultUriParser__ctor_m2156,
	UriScheme__ctor_m2157,
	Uri__ctor_m1254,
	Uri__ctor_m2158,
	Uri__ctor_m2159,
	Uri__ctor_m1256,
	Uri__cctor_m2160,
	Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m2161,
	Uri_Merge_m2162,
	Uri_get_AbsoluteUri_m2163,
	Uri_get_Authority_m2164,
	Uri_get_Host_m2165,
	Uri_get_IsFile_m2166,
	Uri_get_IsLoopback_m2167,
	Uri_get_IsUnc_m2168,
	Uri_get_Scheme_m2169,
	Uri_get_IsAbsoluteUri_m2170,
	Uri_CheckHostName_m2171,
	Uri_IsIPv4Address_m2172,
	Uri_IsDomainAddress_m2173,
	Uri_CheckSchemeName_m2174,
	Uri_IsAlpha_m2175,
	Uri_Equals_m2176,
	Uri_InternalEquals_m2177,
	Uri_GetHashCode_m2178,
	Uri_GetLeftPart_m2179,
	Uri_FromHex_m2180,
	Uri_HexEscape_m2181,
	Uri_IsHexDigit_m2182,
	Uri_IsHexEncoding_m2183,
	Uri_AppendQueryAndFragment_m2184,
	Uri_ToString_m2185,
	Uri_EscapeString_m2186,
	Uri_EscapeString_m2187,
	Uri_ParseUri_m2188,
	Uri_Unescape_m2189,
	Uri_Unescape_m2190,
	Uri_ParseAsWindowsUNC_m2191,
	Uri_ParseAsWindowsAbsoluteFilePath_m2192,
	Uri_ParseAsUnixAbsoluteFilePath_m2193,
	Uri_Parse_m2194,
	Uri_ParseNoExceptions_m2195,
	Uri_CompactEscaped_m2196,
	Uri_Reduce_m2197,
	Uri_HexUnescapeMultiByte_m2198,
	Uri_GetSchemeDelimiter_m2199,
	Uri_GetDefaultPort_m2200,
	Uri_GetOpaqueWiseSchemeDelimiter_m2201,
	Uri_IsPredefinedScheme_m2202,
	Uri_get_Parser_m2203,
	Uri_EnsureAbsoluteUri_m2204,
	Uri_op_Equality_m2205,
	UriFormatException__ctor_m2206,
	UriFormatException__ctor_m2207,
	UriFormatException__ctor_m2208,
	UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m2209,
	UriParser__ctor_m2210,
	UriParser__cctor_m2211,
	UriParser_InitializeAndValidate_m2212,
	UriParser_OnRegister_m2213,
	UriParser_set_SchemeName_m2214,
	UriParser_get_DefaultPort_m2215,
	UriParser_set_DefaultPort_m2216,
	UriParser_CreateDefaults_m2217,
	UriParser_InternalRegister_m2218,
	UriParser_GetParser_m2219,
	RemoteCertificateValidationCallback__ctor_m2220,
	RemoteCertificateValidationCallback_Invoke_m2221,
	RemoteCertificateValidationCallback_BeginInvoke_m2222,
	RemoteCertificateValidationCallback_EndInvoke_m2223,
	MatchEvaluator__ctor_m2224,
	MatchEvaluator_Invoke_m2225,
	MatchEvaluator_BeginInvoke_m2226,
	MatchEvaluator_EndInvoke_m2227,
	ExtensionAttribute__ctor_m2409,
	Locale_GetText_m2410,
	Locale_GetText_m2411,
	KeyBuilder_get_Rng_m2412,
	KeyBuilder_Key_m2413,
	KeyBuilder_IV_m2414,
	SymmetricTransform__ctor_m2415,
	SymmetricTransform_System_IDisposable_Dispose_m2416,
	SymmetricTransform_Finalize_m2417,
	SymmetricTransform_Dispose_m2418,
	SymmetricTransform_get_CanReuseTransform_m2419,
	SymmetricTransform_Transform_m2420,
	SymmetricTransform_CBC_m2421,
	SymmetricTransform_CFB_m2422,
	SymmetricTransform_OFB_m2423,
	SymmetricTransform_CTS_m2424,
	SymmetricTransform_CheckInput_m2425,
	SymmetricTransform_TransformBlock_m2426,
	SymmetricTransform_get_KeepLastBlock_m2427,
	SymmetricTransform_InternalTransformBlock_m2428,
	SymmetricTransform_Random_m2429,
	SymmetricTransform_ThrowBadPaddingException_m2430,
	SymmetricTransform_FinalEncrypt_m2431,
	SymmetricTransform_FinalDecrypt_m2432,
	SymmetricTransform_TransformFinalBlock_m2433,
	Aes__ctor_m2434,
	AesManaged__ctor_m2435,
	AesManaged_GenerateIV_m2436,
	AesManaged_GenerateKey_m2437,
	AesManaged_CreateDecryptor_m2438,
	AesManaged_CreateEncryptor_m2439,
	AesManaged_get_IV_m2440,
	AesManaged_set_IV_m2441,
	AesManaged_get_Key_m2442,
	AesManaged_set_Key_m2443,
	AesManaged_get_KeySize_m2444,
	AesManaged_set_KeySize_m2445,
	AesManaged_CreateDecryptor_m2446,
	AesManaged_CreateEncryptor_m2447,
	AesManaged_Dispose_m2448,
	AesTransform__ctor_m2449,
	AesTransform__cctor_m2450,
	AesTransform_ECB_m2451,
	AesTransform_SubByte_m2452,
	AesTransform_Encrypt128_m2453,
	AesTransform_Decrypt128_m2454,
	Locale_GetText_m2470,
	ModulusRing__ctor_m2471,
	ModulusRing_BarrettReduction_m2472,
	ModulusRing_Multiply_m2473,
	ModulusRing_Difference_m2474,
	ModulusRing_Pow_m2475,
	ModulusRing_Pow_m2476,
	Kernel_AddSameSign_m2477,
	Kernel_Subtract_m2478,
	Kernel_MinusEq_m2479,
	Kernel_PlusEq_m2480,
	Kernel_Compare_m2481,
	Kernel_SingleByteDivideInPlace_m2482,
	Kernel_DwordMod_m2483,
	Kernel_DwordDivMod_m2484,
	Kernel_multiByteDivide_m2485,
	Kernel_LeftShift_m2486,
	Kernel_RightShift_m2487,
	Kernel_Multiply_m2488,
	Kernel_MultiplyMod2p32pmod_m2489,
	Kernel_modInverse_m2490,
	Kernel_modInverse_m2491,
	BigInteger__ctor_m2492,
	BigInteger__ctor_m2493,
	BigInteger__ctor_m2494,
	BigInteger__ctor_m2495,
	BigInteger__ctor_m2496,
	BigInteger__cctor_m2497,
	BigInteger_get_Rng_m2498,
	BigInteger_GenerateRandom_m2499,
	BigInteger_GenerateRandom_m2500,
	BigInteger_BitCount_m2501,
	BigInteger_TestBit_m2502,
	BigInteger_SetBit_m2503,
	BigInteger_SetBit_m2504,
	BigInteger_LowestSetBit_m2505,
	BigInteger_GetBytes_m2506,
	BigInteger_ToString_m2507,
	BigInteger_ToString_m2508,
	BigInteger_Normalize_m2509,
	BigInteger_Clear_m2510,
	BigInteger_GetHashCode_m2511,
	BigInteger_ToString_m2512,
	BigInteger_Equals_m2513,
	BigInteger_ModInverse_m2514,
	BigInteger_ModPow_m2515,
	BigInteger_GeneratePseudoPrime_m2516,
	BigInteger_Incr2_m2517,
	BigInteger_op_Implicit_m2518,
	BigInteger_op_Implicit_m2519,
	BigInteger_op_Addition_m2520,
	BigInteger_op_Subtraction_m2521,
	BigInteger_op_Modulus_m2522,
	BigInteger_op_Modulus_m2523,
	BigInteger_op_Division_m2524,
	BigInteger_op_Multiply_m2525,
	BigInteger_op_LeftShift_m2526,
	BigInteger_op_RightShift_m2527,
	BigInteger_op_Equality_m2528,
	BigInteger_op_Inequality_m2529,
	BigInteger_op_Equality_m2530,
	BigInteger_op_Inequality_m2531,
	BigInteger_op_GreaterThan_m2532,
	BigInteger_op_LessThan_m2533,
	BigInteger_op_GreaterThanOrEqual_m2534,
	BigInteger_op_LessThanOrEqual_m2535,
	PrimalityTests_GetSPPRounds_m2536,
	PrimalityTests_RabinMillerTest_m2537,
	PrimeGeneratorBase__ctor_m2538,
	PrimeGeneratorBase_get_Confidence_m2539,
	PrimeGeneratorBase_get_PrimalityTest_m2540,
	PrimeGeneratorBase_get_TrialDivisionBounds_m2541,
	SequentialSearchPrimeGeneratorBase__ctor_m2542,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m2543,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m2544,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m2545,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m2546,
	ASN1__ctor_m2306,
	ASN1__ctor_m2307,
	ASN1__ctor_m2289,
	ASN1_get_Count_m2293,
	ASN1_get_Tag_m2290,
	ASN1_get_Length_m2320,
	ASN1_get_Value_m2292,
	ASN1_set_Value_m2547,
	ASN1_CompareArray_m2548,
	ASN1_CompareValue_m2319,
	ASN1_Add_m2308,
	ASN1_GetBytes_m2549,
	ASN1_Decode_m2550,
	ASN1_DecodeTLV_m2551,
	ASN1_get_Item_m2294,
	ASN1_Element_m2552,
	ASN1_ToString_m2553,
	ASN1Convert_FromInt32_m2309,
	ASN1Convert_FromOid_m2554,
	ASN1Convert_ToInt32_m2305,
	ASN1Convert_ToOid_m2358,
	ASN1Convert_ToDateTime_m2555,
	BitConverterLE_GetUIntBytes_m2556,
	BitConverterLE_GetBytes_m2557,
	ContentInfo__ctor_m2558,
	ContentInfo__ctor_m2559,
	ContentInfo__ctor_m2560,
	ContentInfo__ctor_m2561,
	ContentInfo_get_Content_m2562,
	ContentInfo_set_Content_m2563,
	ContentInfo_get_ContentType_m2564,
	EncryptedData__ctor_m2565,
	EncryptedData__ctor_m2566,
	EncryptedData_get_EncryptionAlgorithm_m2567,
	EncryptedData_get_EncryptedContent_m2568,
	ARC4Managed__ctor_m2569,
	ARC4Managed_Finalize_m2570,
	ARC4Managed_Dispose_m2571,
	ARC4Managed_get_Key_m2572,
	ARC4Managed_set_Key_m2573,
	ARC4Managed_get_CanReuseTransform_m2574,
	ARC4Managed_CreateEncryptor_m2575,
	ARC4Managed_CreateDecryptor_m2576,
	ARC4Managed_GenerateIV_m2577,
	ARC4Managed_GenerateKey_m2578,
	ARC4Managed_KeySetup_m2579,
	ARC4Managed_CheckInput_m2580,
	ARC4Managed_TransformBlock_m2581,
	ARC4Managed_InternalTransformBlock_m2582,
	ARC4Managed_TransformFinalBlock_m2583,
	CryptoConvert_ToHex_m2372,
	KeyBuilder_get_Rng_m2584,
	KeyBuilder_Key_m2585,
	MD2__ctor_m2586,
	MD2_Create_m2587,
	MD2_Create_m2588,
	MD2Managed__ctor_m2589,
	MD2Managed__cctor_m2590,
	MD2Managed_Padding_m2591,
	MD2Managed_Initialize_m2592,
	MD2Managed_HashCore_m2593,
	MD2Managed_HashFinal_m2594,
	MD2Managed_MD2Transform_m2595,
	PKCS1__cctor_m2596,
	PKCS1_Compare_m2597,
	PKCS1_I2OSP_m2598,
	PKCS1_OS2IP_m2599,
	PKCS1_RSASP1_m2600,
	PKCS1_RSAVP1_m2601,
	PKCS1_Sign_v15_m2602,
	PKCS1_Verify_v15_m2603,
	PKCS1_Verify_v15_m2604,
	PKCS1_Encode_v15_m2605,
	PrivateKeyInfo__ctor_m2606,
	PrivateKeyInfo__ctor_m2607,
	PrivateKeyInfo_get_PrivateKey_m2608,
	PrivateKeyInfo_Decode_m2609,
	PrivateKeyInfo_RemoveLeadingZero_m2610,
	PrivateKeyInfo_Normalize_m2611,
	PrivateKeyInfo_DecodeRSA_m2612,
	PrivateKeyInfo_DecodeDSA_m2613,
	EncryptedPrivateKeyInfo__ctor_m2614,
	EncryptedPrivateKeyInfo__ctor_m2615,
	EncryptedPrivateKeyInfo_get_Algorithm_m2616,
	EncryptedPrivateKeyInfo_get_EncryptedData_m2617,
	EncryptedPrivateKeyInfo_get_Salt_m2618,
	EncryptedPrivateKeyInfo_get_IterationCount_m2619,
	EncryptedPrivateKeyInfo_Decode_m2620,
	RC4__ctor_m2621,
	RC4__cctor_m2622,
	RC4_get_IV_m2623,
	RC4_set_IV_m2624,
	KeyGeneratedEventHandler__ctor_m2625,
	KeyGeneratedEventHandler_Invoke_m2626,
	KeyGeneratedEventHandler_BeginInvoke_m2627,
	KeyGeneratedEventHandler_EndInvoke_m2628,
	RSAManaged__ctor_m2629,
	RSAManaged__ctor_m2630,
	RSAManaged_Finalize_m2631,
	RSAManaged_GenerateKeyPair_m2632,
	RSAManaged_get_KeySize_m2633,
	RSAManaged_get_PublicOnly_m2282,
	RSAManaged_DecryptValue_m2634,
	RSAManaged_EncryptValue_m2635,
	RSAManaged_ExportParameters_m2636,
	RSAManaged_ImportParameters_m2637,
	RSAManaged_Dispose_m2638,
	RSAManaged_ToXmlString_m2639,
	RSAManaged_GetPaddedValue_m2640,
	SafeBag__ctor_m2641,
	SafeBag_get_BagOID_m2642,
	SafeBag_get_ASN1_m2643,
	DeriveBytes__ctor_m2644,
	DeriveBytes__cctor_m2645,
	DeriveBytes_set_HashName_m2646,
	DeriveBytes_set_IterationCount_m2647,
	DeriveBytes_set_Password_m2648,
	DeriveBytes_set_Salt_m2649,
	DeriveBytes_Adjust_m2650,
	DeriveBytes_Derive_m2651,
	DeriveBytes_DeriveKey_m2652,
	DeriveBytes_DeriveIV_m2653,
	DeriveBytes_DeriveMAC_m2654,
	PKCS12__ctor_m2655,
	PKCS12__ctor_m2321,
	PKCS12__ctor_m2322,
	PKCS12__cctor_m2656,
	PKCS12_Decode_m2657,
	PKCS12_Finalize_m2658,
	PKCS12_set_Password_m2659,
	PKCS12_get_Keys_m2325,
	PKCS12_get_Certificates_m2323,
	PKCS12_Compare_m2660,
	PKCS12_GetSymmetricAlgorithm_m2661,
	PKCS12_Decrypt_m2662,
	PKCS12_Decrypt_m2663,
	PKCS12_GetExistingParameters_m2664,
	PKCS12_AddPrivateKey_m2665,
	PKCS12_ReadSafeBag_m2666,
	PKCS12_MAC_m2667,
	PKCS12_get_MaximumPasswordLength_m2668,
	X501__cctor_m2669,
	X501_ToString_m2670,
	X501_ToString_m2298,
	X501_AppendEntry_m2671,
	X509Certificate__ctor_m2328,
	X509Certificate__cctor_m2672,
	X509Certificate_Parse_m2673,
	X509Certificate_GetUnsignedBigInteger_m2674,
	X509Certificate_get_DSA_m2284,
	X509Certificate_set_DSA_m2326,
	X509Certificate_get_Extensions_m2344,
	X509Certificate_get_Hash_m2675,
	X509Certificate_get_IssuerName_m2676,
	X509Certificate_get_KeyAlgorithm_m2677,
	X509Certificate_get_KeyAlgorithmParameters_m2678,
	X509Certificate_set_KeyAlgorithmParameters_m2679,
	X509Certificate_get_PublicKey_m2680,
	X509Certificate_get_RSA_m2681,
	X509Certificate_set_RSA_m2682,
	X509Certificate_get_RawData_m2683,
	X509Certificate_get_SerialNumber_m2684,
	X509Certificate_get_Signature_m2685,
	X509Certificate_get_SignatureAlgorithm_m2686,
	X509Certificate_get_SubjectName_m2687,
	X509Certificate_get_ValidFrom_m2688,
	X509Certificate_get_ValidUntil_m2689,
	X509Certificate_get_Version_m2318,
	X509Certificate_get_IsCurrent_m2690,
	X509Certificate_WasCurrent_m2691,
	X509Certificate_VerifySignature_m2692,
	X509Certificate_VerifySignature_m2693,
	X509Certificate_VerifySignature_m2343,
	X509Certificate_get_IsSelfSigned_m2694,
	X509Certificate_GetIssuerName_m2313,
	X509Certificate_GetSubjectName_m2316,
	X509Certificate_GetObjectData_m2695,
	X509Certificate_PEM_m2696,
	X509CertificateEnumerator__ctor_m2697,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m2698,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m2699,
	X509CertificateEnumerator_get_Current_m2369,
	X509CertificateEnumerator_MoveNext_m2700,
	X509CertificateCollection__ctor_m2701,
	X509CertificateCollection__ctor_m2702,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m2703,
	X509CertificateCollection_get_Item_m2324,
	X509CertificateCollection_Add_m2704,
	X509CertificateCollection_AddRange_m2705,
	X509CertificateCollection_Contains_m2706,
	X509CertificateCollection_GetEnumerator_m2368,
	X509CertificateCollection_GetHashCode_m2707,
	X509CertificateCollection_IndexOf_m2708,
	X509CertificateCollection_Remove_m2709,
	X509CertificateCollection_Compare_m2710,
	X509Chain__ctor_m2711,
	X509Chain__ctor_m2712,
	X509Chain_get_Status_m2713,
	X509Chain_get_TrustAnchors_m2714,
	X509Chain_Build_m2715,
	X509Chain_IsValid_m2716,
	X509Chain_FindCertificateParent_m2717,
	X509Chain_FindCertificateRoot_m2718,
	X509Chain_IsTrusted_m2719,
	X509Chain_IsParent_m2720,
	X509CrlEntry__ctor_m2721,
	X509CrlEntry_get_SerialNumber_m2722,
	X509CrlEntry_get_RevocationDate_m2351,
	X509CrlEntry_get_Extensions_m2357,
	X509Crl__ctor_m2723,
	X509Crl_Parse_m2724,
	X509Crl_get_Extensions_m2346,
	X509Crl_get_Hash_m2725,
	X509Crl_get_IssuerName_m2354,
	X509Crl_get_NextUpdate_m2352,
	X509Crl_Compare_m2726,
	X509Crl_GetCrlEntry_m2350,
	X509Crl_GetCrlEntry_m2727,
	X509Crl_GetHashName_m2728,
	X509Crl_VerifySignature_m2729,
	X509Crl_VerifySignature_m2730,
	X509Crl_VerifySignature_m2349,
	X509Extension__ctor_m2731,
	X509Extension__ctor_m2732,
	X509Extension_Decode_m2733,
	X509Extension_Encode_m2734,
	X509Extension_get_Oid_m2356,
	X509Extension_get_Critical_m2355,
	X509Extension_get_Value_m2360,
	X509Extension_Equals_m2735,
	X509Extension_GetHashCode_m2736,
	X509Extension_WriteLine_m2737,
	X509Extension_ToString_m2738,
	X509ExtensionCollection__ctor_m2739,
	X509ExtensionCollection__ctor_m2740,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m2741,
	X509ExtensionCollection_IndexOf_m2742,
	X509ExtensionCollection_get_Item_m2345,
	X509Store__ctor_m2743,
	X509Store_get_Certificates_m2367,
	X509Store_get_Crls_m2353,
	X509Store_Load_m2744,
	X509Store_LoadCertificate_m2745,
	X509Store_LoadCrl_m2746,
	X509Store_CheckStore_m2747,
	X509Store_BuildCertificatesCollection_m2748,
	X509Store_BuildCrlsCollection_m2749,
	X509StoreManager_get_CurrentUser_m2364,
	X509StoreManager_get_LocalMachine_m2365,
	X509StoreManager_get_TrustedRootCertificates_m2750,
	X509Stores__ctor_m2751,
	X509Stores_get_TrustedRoot_m2752,
	X509Stores_Open_m2366,
	AuthorityKeyIdentifierExtension__ctor_m2347,
	AuthorityKeyIdentifierExtension_Decode_m2753,
	AuthorityKeyIdentifierExtension_get_Identifier_m2348,
	AuthorityKeyIdentifierExtension_ToString_m2754,
	BasicConstraintsExtension__ctor_m2755,
	BasicConstraintsExtension_Decode_m2756,
	BasicConstraintsExtension_Encode_m2757,
	BasicConstraintsExtension_get_CertificateAuthority_m2758,
	BasicConstraintsExtension_ToString_m2759,
	ExtendedKeyUsageExtension__ctor_m2760,
	ExtendedKeyUsageExtension_Decode_m2761,
	ExtendedKeyUsageExtension_Encode_m2762,
	ExtendedKeyUsageExtension_get_KeyPurpose_m2763,
	ExtendedKeyUsageExtension_ToString_m2764,
	GeneralNames__ctor_m2765,
	GeneralNames_get_DNSNames_m2766,
	GeneralNames_get_IPAddresses_m2767,
	GeneralNames_ToString_m2768,
	KeyUsageExtension__ctor_m2769,
	KeyUsageExtension_Decode_m2770,
	KeyUsageExtension_Encode_m2771,
	KeyUsageExtension_Support_m2772,
	KeyUsageExtension_ToString_m2773,
	NetscapeCertTypeExtension__ctor_m2774,
	NetscapeCertTypeExtension_Decode_m2775,
	NetscapeCertTypeExtension_Support_m2776,
	NetscapeCertTypeExtension_ToString_m2777,
	SubjectAltNameExtension__ctor_m2778,
	SubjectAltNameExtension_Decode_m2779,
	SubjectAltNameExtension_get_DNSNames_m2780,
	SubjectAltNameExtension_get_IPAddresses_m2781,
	SubjectAltNameExtension_ToString_m2782,
	HMAC__ctor_m2783,
	HMAC_get_Key_m2784,
	HMAC_set_Key_m2785,
	HMAC_Initialize_m2786,
	HMAC_HashFinal_m2787,
	HMAC_HashCore_m2788,
	HMAC_initializePad_m2789,
	MD5SHA1__ctor_m2790,
	MD5SHA1_Initialize_m2791,
	MD5SHA1_HashFinal_m2792,
	MD5SHA1_HashCore_m2793,
	MD5SHA1_CreateSignature_m2794,
	MD5SHA1_VerifySignature_m2795,
	Alert__ctor_m2796,
	Alert__ctor_m2797,
	Alert_get_Level_m2798,
	Alert_get_Description_m2799,
	Alert_get_IsWarning_m2800,
	Alert_get_IsCloseNotify_m2801,
	Alert_inferAlertLevel_m2802,
	Alert_GetAlertMessage_m2803,
	CipherSuite__ctor_m2804,
	CipherSuite__cctor_m2805,
	CipherSuite_get_EncryptionCipher_m2806,
	CipherSuite_get_DecryptionCipher_m2807,
	CipherSuite_get_ClientHMAC_m2808,
	CipherSuite_get_ServerHMAC_m2809,
	CipherSuite_get_CipherAlgorithmType_m2810,
	CipherSuite_get_HashAlgorithmName_m2811,
	CipherSuite_get_HashAlgorithmType_m2812,
	CipherSuite_get_HashSize_m2813,
	CipherSuite_get_ExchangeAlgorithmType_m2814,
	CipherSuite_get_CipherMode_m2815,
	CipherSuite_get_Code_m2816,
	CipherSuite_get_Name_m2817,
	CipherSuite_get_IsExportable_m2818,
	CipherSuite_get_KeyMaterialSize_m2819,
	CipherSuite_get_KeyBlockSize_m2820,
	CipherSuite_get_ExpandedKeyMaterialSize_m2821,
	CipherSuite_get_EffectiveKeyBits_m2822,
	CipherSuite_get_IvSize_m2823,
	CipherSuite_get_Context_m2824,
	CipherSuite_set_Context_m2825,
	CipherSuite_Write_m2826,
	CipherSuite_Write_m2827,
	CipherSuite_InitializeCipher_m2828,
	CipherSuite_EncryptRecord_m2829,
	CipherSuite_DecryptRecord_m2830,
	CipherSuite_CreatePremasterSecret_m2831,
	CipherSuite_PRF_m2832,
	CipherSuite_Expand_m2833,
	CipherSuite_createEncryptionCipher_m2834,
	CipherSuite_createDecryptionCipher_m2835,
	CipherSuiteCollection__ctor_m2836,
	CipherSuiteCollection_System_Collections_IList_get_Item_m2837,
	CipherSuiteCollection_System_Collections_IList_set_Item_m2838,
	CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m2839,
	CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m2840,
	CipherSuiteCollection_System_Collections_IList_Contains_m2841,
	CipherSuiteCollection_System_Collections_IList_IndexOf_m2842,
	CipherSuiteCollection_System_Collections_IList_Insert_m2843,
	CipherSuiteCollection_System_Collections_IList_Remove_m2844,
	CipherSuiteCollection_System_Collections_IList_RemoveAt_m2845,
	CipherSuiteCollection_System_Collections_IList_Add_m2846,
	CipherSuiteCollection_get_Item_m2847,
	CipherSuiteCollection_get_Item_m2848,
	CipherSuiteCollection_set_Item_m2849,
	CipherSuiteCollection_get_Item_m2850,
	CipherSuiteCollection_get_Count_m2851,
	CipherSuiteCollection_CopyTo_m2852,
	CipherSuiteCollection_Clear_m2853,
	CipherSuiteCollection_IndexOf_m2854,
	CipherSuiteCollection_IndexOf_m2855,
	CipherSuiteCollection_Add_m2856,
	CipherSuiteCollection_add_m2857,
	CipherSuiteCollection_add_m2858,
	CipherSuiteCollection_cultureAwareCompare_m2859,
	CipherSuiteFactory_GetSupportedCiphers_m2860,
	CipherSuiteFactory_GetTls1SupportedCiphers_m2861,
	CipherSuiteFactory_GetSsl3SupportedCiphers_m2862,
	ClientContext__ctor_m2863,
	ClientContext_get_SslStream_m2864,
	ClientContext_get_ClientHelloProtocol_m2865,
	ClientContext_set_ClientHelloProtocol_m2866,
	ClientContext_Clear_m2867,
	ClientRecordProtocol__ctor_m2868,
	ClientRecordProtocol_GetMessage_m2869,
	ClientRecordProtocol_ProcessHandshakeMessage_m2870,
	ClientRecordProtocol_createClientHandshakeMessage_m2871,
	ClientRecordProtocol_createServerHandshakeMessage_m2872,
	ClientSessionInfo__ctor_m2873,
	ClientSessionInfo__cctor_m2874,
	ClientSessionInfo_Finalize_m2875,
	ClientSessionInfo_get_HostName_m2876,
	ClientSessionInfo_get_Id_m2877,
	ClientSessionInfo_get_Valid_m2878,
	ClientSessionInfo_GetContext_m2879,
	ClientSessionInfo_SetContext_m2880,
	ClientSessionInfo_KeepAlive_m2881,
	ClientSessionInfo_Dispose_m2882,
	ClientSessionInfo_Dispose_m2883,
	ClientSessionInfo_CheckDisposed_m2884,
	ClientSessionCache__cctor_m2885,
	ClientSessionCache_Add_m2886,
	ClientSessionCache_FromHost_m2887,
	ClientSessionCache_FromContext_m2888,
	ClientSessionCache_SetContextInCache_m2889,
	ClientSessionCache_SetContextFromCache_m2890,
	Context__ctor_m2891,
	Context_get_AbbreviatedHandshake_m2892,
	Context_set_AbbreviatedHandshake_m2893,
	Context_get_ProtocolNegotiated_m2894,
	Context_set_ProtocolNegotiated_m2895,
	Context_get_SecurityProtocol_m2896,
	Context_set_SecurityProtocol_m2897,
	Context_get_SecurityProtocolFlags_m2898,
	Context_get_Protocol_m2899,
	Context_get_SessionId_m2900,
	Context_set_SessionId_m2901,
	Context_get_CompressionMethod_m2902,
	Context_set_CompressionMethod_m2903,
	Context_get_ServerSettings_m2904,
	Context_get_ClientSettings_m2905,
	Context_get_LastHandshakeMsg_m2906,
	Context_set_LastHandshakeMsg_m2907,
	Context_get_HandshakeState_m2908,
	Context_set_HandshakeState_m2909,
	Context_get_ReceivedConnectionEnd_m2910,
	Context_set_ReceivedConnectionEnd_m2911,
	Context_get_SentConnectionEnd_m2912,
	Context_set_SentConnectionEnd_m2913,
	Context_get_SupportedCiphers_m2914,
	Context_set_SupportedCiphers_m2915,
	Context_get_HandshakeMessages_m2916,
	Context_get_WriteSequenceNumber_m2917,
	Context_set_WriteSequenceNumber_m2918,
	Context_get_ReadSequenceNumber_m2919,
	Context_set_ReadSequenceNumber_m2920,
	Context_get_ClientRandom_m2921,
	Context_set_ClientRandom_m2922,
	Context_get_ServerRandom_m2923,
	Context_set_ServerRandom_m2924,
	Context_get_RandomCS_m2925,
	Context_set_RandomCS_m2926,
	Context_get_RandomSC_m2927,
	Context_set_RandomSC_m2928,
	Context_get_MasterSecret_m2929,
	Context_set_MasterSecret_m2930,
	Context_get_ClientWriteKey_m2931,
	Context_set_ClientWriteKey_m2932,
	Context_get_ServerWriteKey_m2933,
	Context_set_ServerWriteKey_m2934,
	Context_get_ClientWriteIV_m2935,
	Context_set_ClientWriteIV_m2936,
	Context_get_ServerWriteIV_m2937,
	Context_set_ServerWriteIV_m2938,
	Context_get_RecordProtocol_m2939,
	Context_set_RecordProtocol_m2940,
	Context_GetUnixTime_m2941,
	Context_GetSecureRandomBytes_m2942,
	Context_Clear_m2943,
	Context_ClearKeyInfo_m2944,
	Context_DecodeProtocolCode_m2945,
	Context_ChangeProtocol_m2946,
	Context_get_Current_m2947,
	Context_get_Negotiating_m2948,
	Context_get_Read_m2949,
	Context_get_Write_m2950,
	Context_StartSwitchingSecurityParameters_m2951,
	Context_EndSwitchingSecurityParameters_m2952,
	HttpsClientStream__ctor_m2953,
	HttpsClientStream_get_TrustFailure_m2954,
	HttpsClientStream_RaiseServerCertificateValidation_m2955,
	HttpsClientStream_U3CHttpsClientStreamU3Em__0_m2956,
	HttpsClientStream_U3CHttpsClientStreamU3Em__1_m2957,
	ReceiveRecordAsyncResult__ctor_m2958,
	ReceiveRecordAsyncResult_get_Record_m2959,
	ReceiveRecordAsyncResult_get_ResultingBuffer_m2960,
	ReceiveRecordAsyncResult_get_InitialBuffer_m2961,
	ReceiveRecordAsyncResult_get_AsyncState_m2962,
	ReceiveRecordAsyncResult_get_AsyncException_m2963,
	ReceiveRecordAsyncResult_get_CompletedWithError_m2964,
	ReceiveRecordAsyncResult_get_AsyncWaitHandle_m2965,
	ReceiveRecordAsyncResult_get_IsCompleted_m2966,
	ReceiveRecordAsyncResult_SetComplete_m2967,
	ReceiveRecordAsyncResult_SetComplete_m2968,
	ReceiveRecordAsyncResult_SetComplete_m2969,
	SendRecordAsyncResult__ctor_m2970,
	SendRecordAsyncResult_get_Message_m2971,
	SendRecordAsyncResult_get_AsyncState_m2972,
	SendRecordAsyncResult_get_AsyncException_m2973,
	SendRecordAsyncResult_get_CompletedWithError_m2974,
	SendRecordAsyncResult_get_AsyncWaitHandle_m2975,
	SendRecordAsyncResult_get_IsCompleted_m2976,
	SendRecordAsyncResult_SetComplete_m2977,
	SendRecordAsyncResult_SetComplete_m2978,
	RecordProtocol__ctor_m2979,
	RecordProtocol__cctor_m2980,
	RecordProtocol_get_Context_m2981,
	RecordProtocol_SendRecord_m2982,
	RecordProtocol_ProcessChangeCipherSpec_m2983,
	RecordProtocol_GetMessage_m2984,
	RecordProtocol_BeginReceiveRecord_m2985,
	RecordProtocol_InternalReceiveRecordCallback_m2986,
	RecordProtocol_EndReceiveRecord_m2987,
	RecordProtocol_ReceiveRecord_m2988,
	RecordProtocol_ReadRecordBuffer_m2989,
	RecordProtocol_ReadClientHelloV2_m2990,
	RecordProtocol_ReadStandardRecordBuffer_m2991,
	RecordProtocol_ProcessAlert_m2992,
	RecordProtocol_SendAlert_m2993,
	RecordProtocol_SendAlert_m2994,
	RecordProtocol_SendAlert_m2995,
	RecordProtocol_SendChangeCipherSpec_m2996,
	RecordProtocol_BeginSendRecord_m2997,
	RecordProtocol_InternalSendRecordCallback_m2998,
	RecordProtocol_BeginSendRecord_m2999,
	RecordProtocol_EndSendRecord_m3000,
	RecordProtocol_SendRecord_m3001,
	RecordProtocol_EncodeRecord_m3002,
	RecordProtocol_EncodeRecord_m3003,
	RecordProtocol_encryptRecordFragment_m3004,
	RecordProtocol_decryptRecordFragment_m3005,
	RecordProtocol_Compare_m3006,
	RecordProtocol_ProcessCipherSpecV2Buffer_m3007,
	RecordProtocol_MapV2CipherCode_m3008,
	RSASslSignatureDeformatter__ctor_m3009,
	RSASslSignatureDeformatter_VerifySignature_m3010,
	RSASslSignatureDeformatter_SetHashAlgorithm_m3011,
	RSASslSignatureDeformatter_SetKey_m3012,
	RSASslSignatureFormatter__ctor_m3013,
	RSASslSignatureFormatter_CreateSignature_m3014,
	RSASslSignatureFormatter_SetHashAlgorithm_m3015,
	RSASslSignatureFormatter_SetKey_m3016,
	SecurityParameters__ctor_m3017,
	SecurityParameters_get_Cipher_m3018,
	SecurityParameters_set_Cipher_m3019,
	SecurityParameters_get_ClientWriteMAC_m3020,
	SecurityParameters_set_ClientWriteMAC_m3021,
	SecurityParameters_get_ServerWriteMAC_m3022,
	SecurityParameters_set_ServerWriteMAC_m3023,
	SecurityParameters_Clear_m3024,
	ValidationResult_get_Trusted_m3025,
	ValidationResult_get_ErrorCode_m3026,
	SslClientStream__ctor_m3027,
	SslClientStream__ctor_m3028,
	SslClientStream__ctor_m3029,
	SslClientStream__ctor_m3030,
	SslClientStream__ctor_m3031,
	SslClientStream_add_ServerCertValidation_m3032,
	SslClientStream_remove_ServerCertValidation_m3033,
	SslClientStream_add_ClientCertSelection_m3034,
	SslClientStream_remove_ClientCertSelection_m3035,
	SslClientStream_add_PrivateKeySelection_m3036,
	SslClientStream_remove_PrivateKeySelection_m3037,
	SslClientStream_add_ServerCertValidation2_m3038,
	SslClientStream_remove_ServerCertValidation2_m3039,
	SslClientStream_get_InputBuffer_m3040,
	SslClientStream_get_ClientCertificates_m3041,
	SslClientStream_get_SelectedClientCertificate_m3042,
	SslClientStream_get_ServerCertValidationDelegate_m3043,
	SslClientStream_set_ServerCertValidationDelegate_m3044,
	SslClientStream_get_ClientCertSelectionDelegate_m3045,
	SslClientStream_set_ClientCertSelectionDelegate_m3046,
	SslClientStream_get_PrivateKeyCertSelectionDelegate_m3047,
	SslClientStream_set_PrivateKeyCertSelectionDelegate_m3048,
	SslClientStream_Finalize_m3049,
	SslClientStream_Dispose_m3050,
	SslClientStream_OnBeginNegotiateHandshake_m3051,
	SslClientStream_SafeReceiveRecord_m3052,
	SslClientStream_OnNegotiateHandshakeCallback_m3053,
	SslClientStream_OnLocalCertificateSelection_m3054,
	SslClientStream_get_HaveRemoteValidation2Callback_m3055,
	SslClientStream_OnRemoteCertificateValidation2_m3056,
	SslClientStream_OnRemoteCertificateValidation_m3057,
	SslClientStream_RaiseServerCertificateValidation_m3058,
	SslClientStream_RaiseServerCertificateValidation2_m3059,
	SslClientStream_RaiseClientCertificateSelection_m3060,
	SslClientStream_OnLocalPrivateKeySelection_m3061,
	SslClientStream_RaisePrivateKeySelection_m3062,
	SslCipherSuite__ctor_m3063,
	SslCipherSuite_ComputeServerRecordMAC_m3064,
	SslCipherSuite_ComputeClientRecordMAC_m3065,
	SslCipherSuite_ComputeMasterSecret_m3066,
	SslCipherSuite_ComputeKeys_m3067,
	SslCipherSuite_prf_m3068,
	SslHandshakeHash__ctor_m3069,
	SslHandshakeHash_Initialize_m3070,
	SslHandshakeHash_HashFinal_m3071,
	SslHandshakeHash_HashCore_m3072,
	SslHandshakeHash_CreateSignature_m3073,
	SslHandshakeHash_initializePad_m3074,
	InternalAsyncResult__ctor_m3075,
	InternalAsyncResult_get_ProceedAfterHandshake_m3076,
	InternalAsyncResult_get_FromWrite_m3077,
	InternalAsyncResult_get_Buffer_m3078,
	InternalAsyncResult_get_Offset_m3079,
	InternalAsyncResult_get_Count_m3080,
	InternalAsyncResult_get_BytesRead_m3081,
	InternalAsyncResult_get_AsyncState_m3082,
	InternalAsyncResult_get_AsyncException_m3083,
	InternalAsyncResult_get_CompletedWithError_m3084,
	InternalAsyncResult_get_AsyncWaitHandle_m3085,
	InternalAsyncResult_get_IsCompleted_m3086,
	InternalAsyncResult_SetComplete_m3087,
	InternalAsyncResult_SetComplete_m3088,
	InternalAsyncResult_SetComplete_m3089,
	InternalAsyncResult_SetComplete_m3090,
	SslStreamBase__ctor_m3091,
	SslStreamBase__cctor_m3092,
	SslStreamBase_AsyncHandshakeCallback_m3093,
	SslStreamBase_get_MightNeedHandshake_m3094,
	SslStreamBase_NegotiateHandshake_m3095,
	SslStreamBase_RaiseLocalCertificateSelection_m3096,
	SslStreamBase_RaiseRemoteCertificateValidation_m3097,
	SslStreamBase_RaiseRemoteCertificateValidation2_m3098,
	SslStreamBase_RaiseLocalPrivateKeySelection_m3099,
	SslStreamBase_get_CheckCertRevocationStatus_m3100,
	SslStreamBase_set_CheckCertRevocationStatus_m3101,
	SslStreamBase_get_CipherAlgorithm_m3102,
	SslStreamBase_get_CipherStrength_m3103,
	SslStreamBase_get_HashAlgorithm_m3104,
	SslStreamBase_get_HashStrength_m3105,
	SslStreamBase_get_KeyExchangeStrength_m3106,
	SslStreamBase_get_KeyExchangeAlgorithm_m3107,
	SslStreamBase_get_SecurityProtocol_m3108,
	SslStreamBase_get_ServerCertificate_m3109,
	SslStreamBase_get_ServerCertificates_m3110,
	SslStreamBase_BeginNegotiateHandshake_m3111,
	SslStreamBase_EndNegotiateHandshake_m3112,
	SslStreamBase_BeginRead_m3113,
	SslStreamBase_InternalBeginRead_m3114,
	SslStreamBase_InternalReadCallback_m3115,
	SslStreamBase_InternalBeginWrite_m3116,
	SslStreamBase_InternalWriteCallback_m3117,
	SslStreamBase_BeginWrite_m3118,
	SslStreamBase_EndRead_m3119,
	SslStreamBase_EndWrite_m3120,
	SslStreamBase_Close_m3121,
	SslStreamBase_Flush_m3122,
	SslStreamBase_Read_m3123,
	SslStreamBase_Read_m3124,
	SslStreamBase_Seek_m3125,
	SslStreamBase_SetLength_m3126,
	SslStreamBase_Write_m3127,
	SslStreamBase_Write_m3128,
	SslStreamBase_get_CanRead_m3129,
	SslStreamBase_get_CanSeek_m3130,
	SslStreamBase_get_CanWrite_m3131,
	SslStreamBase_get_Length_m3132,
	SslStreamBase_get_Position_m3133,
	SslStreamBase_set_Position_m3134,
	SslStreamBase_Finalize_m3135,
	SslStreamBase_Dispose_m3136,
	SslStreamBase_resetBuffer_m3137,
	SslStreamBase_checkDisposed_m3138,
	TlsCipherSuite__ctor_m3139,
	TlsCipherSuite_ComputeServerRecordMAC_m3140,
	TlsCipherSuite_ComputeClientRecordMAC_m3141,
	TlsCipherSuite_ComputeMasterSecret_m3142,
	TlsCipherSuite_ComputeKeys_m3143,
	TlsClientSettings__ctor_m3144,
	TlsClientSettings_get_TargetHost_m3145,
	TlsClientSettings_set_TargetHost_m3146,
	TlsClientSettings_get_Certificates_m3147,
	TlsClientSettings_set_Certificates_m3148,
	TlsClientSettings_get_ClientCertificate_m3149,
	TlsClientSettings_set_ClientCertificate_m3150,
	TlsClientSettings_UpdateCertificateRSA_m3151,
	TlsException__ctor_m3152,
	TlsException__ctor_m3153,
	TlsException__ctor_m3154,
	TlsException__ctor_m3155,
	TlsException__ctor_m3156,
	TlsException__ctor_m3157,
	TlsException_get_Alert_m3158,
	TlsServerSettings__ctor_m3159,
	TlsServerSettings_get_ServerKeyExchange_m3160,
	TlsServerSettings_set_ServerKeyExchange_m3161,
	TlsServerSettings_get_Certificates_m3162,
	TlsServerSettings_set_Certificates_m3163,
	TlsServerSettings_get_CertificateRSA_m3164,
	TlsServerSettings_get_RsaParameters_m3165,
	TlsServerSettings_set_RsaParameters_m3166,
	TlsServerSettings_set_SignedParams_m3167,
	TlsServerSettings_get_CertificateRequest_m3168,
	TlsServerSettings_set_CertificateRequest_m3169,
	TlsServerSettings_set_CertificateTypes_m3170,
	TlsServerSettings_set_DistinguisedNames_m3171,
	TlsServerSettings_UpdateCertificateRSA_m3172,
	TlsStream__ctor_m3173,
	TlsStream__ctor_m3174,
	TlsStream_get_EOF_m3175,
	TlsStream_get_CanWrite_m3176,
	TlsStream_get_CanRead_m3177,
	TlsStream_get_CanSeek_m3178,
	TlsStream_get_Position_m3179,
	TlsStream_set_Position_m3180,
	TlsStream_get_Length_m3181,
	TlsStream_ReadSmallValue_m3182,
	TlsStream_ReadByte_m3183,
	TlsStream_ReadInt16_m3184,
	TlsStream_ReadInt24_m3185,
	TlsStream_ReadBytes_m3186,
	TlsStream_Write_m3187,
	TlsStream_Write_m3188,
	TlsStream_WriteInt24_m3189,
	TlsStream_Write_m3190,
	TlsStream_Write_m3191,
	TlsStream_Reset_m3192,
	TlsStream_ToArray_m3193,
	TlsStream_Flush_m3194,
	TlsStream_SetLength_m3195,
	TlsStream_Seek_m3196,
	TlsStream_Read_m3197,
	TlsStream_Write_m3198,
	HandshakeMessage__ctor_m3199,
	HandshakeMessage__ctor_m3200,
	HandshakeMessage__ctor_m3201,
	HandshakeMessage_get_Context_m3202,
	HandshakeMessage_get_HandshakeType_m3203,
	HandshakeMessage_get_ContentType_m3204,
	HandshakeMessage_Process_m3205,
	HandshakeMessage_Update_m3206,
	HandshakeMessage_EncodeMessage_m3207,
	HandshakeMessage_Compare_m3208,
	TlsClientCertificate__ctor_m3209,
	TlsClientCertificate_get_ClientCertificate_m3210,
	TlsClientCertificate_Update_m3211,
	TlsClientCertificate_GetClientCertificate_m3212,
	TlsClientCertificate_SendCertificates_m3213,
	TlsClientCertificate_ProcessAsSsl3_m3214,
	TlsClientCertificate_ProcessAsTls1_m3215,
	TlsClientCertificate_FindParentCertificate_m3216,
	TlsClientCertificateVerify__ctor_m3217,
	TlsClientCertificateVerify_Update_m3218,
	TlsClientCertificateVerify_ProcessAsSsl3_m3219,
	TlsClientCertificateVerify_ProcessAsTls1_m3220,
	TlsClientCertificateVerify_getClientCertRSA_m3221,
	TlsClientCertificateVerify_getUnsignedBigInteger_m3222,
	TlsClientFinished__ctor_m3223,
	TlsClientFinished__cctor_m3224,
	TlsClientFinished_Update_m3225,
	TlsClientFinished_ProcessAsSsl3_m3226,
	TlsClientFinished_ProcessAsTls1_m3227,
	TlsClientHello__ctor_m3228,
	TlsClientHello_Update_m3229,
	TlsClientHello_ProcessAsSsl3_m3230,
	TlsClientHello_ProcessAsTls1_m3231,
	TlsClientKeyExchange__ctor_m3232,
	TlsClientKeyExchange_ProcessAsSsl3_m3233,
	TlsClientKeyExchange_ProcessAsTls1_m3234,
	TlsClientKeyExchange_ProcessCommon_m3235,
	TlsServerCertificate__ctor_m3236,
	TlsServerCertificate_Update_m3237,
	TlsServerCertificate_ProcessAsSsl3_m3238,
	TlsServerCertificate_ProcessAsTls1_m3239,
	TlsServerCertificate_checkCertificateUsage_m3240,
	TlsServerCertificate_validateCertificates_m3241,
	TlsServerCertificate_checkServerIdentity_m3242,
	TlsServerCertificate_checkDomainName_m3243,
	TlsServerCertificate_Match_m3244,
	TlsServerCertificateRequest__ctor_m3245,
	TlsServerCertificateRequest_Update_m3246,
	TlsServerCertificateRequest_ProcessAsSsl3_m3247,
	TlsServerCertificateRequest_ProcessAsTls1_m3248,
	TlsServerFinished__ctor_m3249,
	TlsServerFinished__cctor_m3250,
	TlsServerFinished_Update_m3251,
	TlsServerFinished_ProcessAsSsl3_m3252,
	TlsServerFinished_ProcessAsTls1_m3253,
	TlsServerHello__ctor_m3254,
	TlsServerHello_Update_m3255,
	TlsServerHello_ProcessAsSsl3_m3256,
	TlsServerHello_ProcessAsTls1_m3257,
	TlsServerHello_processProtocol_m3258,
	TlsServerHelloDone__ctor_m3259,
	TlsServerHelloDone_ProcessAsSsl3_m3260,
	TlsServerHelloDone_ProcessAsTls1_m3261,
	TlsServerKeyExchange__ctor_m3262,
	TlsServerKeyExchange_Update_m3263,
	TlsServerKeyExchange_ProcessAsSsl3_m3264,
	TlsServerKeyExchange_ProcessAsTls1_m3265,
	TlsServerKeyExchange_verifySignature_m3266,
	PrimalityTest__ctor_m3267,
	PrimalityTest_Invoke_m3268,
	PrimalityTest_BeginInvoke_m3269,
	PrimalityTest_EndInvoke_m3270,
	CertificateValidationCallback__ctor_m3271,
	CertificateValidationCallback_Invoke_m3272,
	CertificateValidationCallback_BeginInvoke_m3273,
	CertificateValidationCallback_EndInvoke_m3274,
	CertificateValidationCallback2__ctor_m3275,
	CertificateValidationCallback2_Invoke_m3276,
	CertificateValidationCallback2_BeginInvoke_m3277,
	CertificateValidationCallback2_EndInvoke_m3278,
	CertificateSelectionCallback__ctor_m3279,
	CertificateSelectionCallback_Invoke_m3280,
	CertificateSelectionCallback_BeginInvoke_m3281,
	CertificateSelectionCallback_EndInvoke_m3282,
	PrivateKeySelectionCallback__ctor_m3283,
	PrivateKeySelectionCallback_Invoke_m3284,
	PrivateKeySelectionCallback_BeginInvoke_m3285,
	PrivateKeySelectionCallback_EndInvoke_m3286,
	Object__ctor_m1164,
	Object_Equals_m3361,
	Object_Equals_m2406,
	Object_Finalize_m1163,
	Object_GetHashCode_m3362,
	Object_GetType_m1185,
	Object_MemberwiseClone_m3363,
	Object_ToString_m1235,
	Object_ReferenceEquals_m1193,
	Object_InternalGetHashCode_m3364,
	ValueType__ctor_m3365,
	ValueType_InternalEquals_m3366,
	ValueType_DefaultEquals_m3367,
	ValueType_Equals_m3368,
	ValueType_InternalGetHashCode_m3369,
	ValueType_GetHashCode_m3370,
	ValueType_ToString_m3371,
	Attribute__ctor_m1198,
	Attribute_CheckParameters_m3372,
	Attribute_GetCustomAttribute_m3373,
	Attribute_GetCustomAttribute_m3374,
	Attribute_GetHashCode_m1335,
	Attribute_IsDefined_m3375,
	Attribute_IsDefined_m3376,
	Attribute_IsDefined_m3377,
	Attribute_IsDefined_m3378,
	Attribute_Equals_m3379,
	Int32_System_IConvertible_ToBoolean_m3380,
	Int32_System_IConvertible_ToByte_m3381,
	Int32_System_IConvertible_ToChar_m3382,
	Int32_System_IConvertible_ToDateTime_m3383,
	Int32_System_IConvertible_ToDecimal_m3384,
	Int32_System_IConvertible_ToDouble_m3385,
	Int32_System_IConvertible_ToInt16_m3386,
	Int32_System_IConvertible_ToInt32_m3387,
	Int32_System_IConvertible_ToInt64_m3388,
	Int32_System_IConvertible_ToSByte_m3389,
	Int32_System_IConvertible_ToSingle_m3390,
	Int32_System_IConvertible_ToType_m3391,
	Int32_System_IConvertible_ToUInt16_m3392,
	Int32_System_IConvertible_ToUInt32_m3393,
	Int32_System_IConvertible_ToUInt64_m3394,
	Int32_CompareTo_m3395,
	Int32_Equals_m3396,
	Int32_GetHashCode_m1175,
	Int32_CompareTo_m3397,
	Int32_Equals_m1177,
	Int32_ProcessTrailingWhitespace_m3398,
	Int32_Parse_m3399,
	Int32_Parse_m3400,
	Int32_CheckStyle_m3401,
	Int32_JumpOverWhite_m3402,
	Int32_FindSign_m3403,
	Int32_FindCurrency_m3404,
	Int32_FindExponent_m3405,
	Int32_FindOther_m3406,
	Int32_ValidDigit_m3407,
	Int32_GetFormatException_m3408,
	Int32_Parse_m3409,
	Int32_Parse_m2379,
	Int32_Parse_m3410,
	Int32_TryParse_m3411,
	Int32_TryParse_m2262,
	Int32_ToString_m1222,
	Int32_ToString_m1282,
	Int32_ToString_m2373,
	Int32_ToString_m3330,
	SerializableAttribute__ctor_m3412,
	AttributeUsageAttribute__ctor_m3413,
	AttributeUsageAttribute_get_AllowMultiple_m3414,
	AttributeUsageAttribute_set_AllowMultiple_m3415,
	AttributeUsageAttribute_get_Inherited_m3416,
	AttributeUsageAttribute_set_Inherited_m3417,
	ComVisibleAttribute__ctor_m3418,
	Int64_System_IConvertible_ToBoolean_m3419,
	Int64_System_IConvertible_ToByte_m3420,
	Int64_System_IConvertible_ToChar_m3421,
	Int64_System_IConvertible_ToDateTime_m3422,
	Int64_System_IConvertible_ToDecimal_m3423,
	Int64_System_IConvertible_ToDouble_m3424,
	Int64_System_IConvertible_ToInt16_m3425,
	Int64_System_IConvertible_ToInt32_m3426,
	Int64_System_IConvertible_ToInt64_m3427,
	Int64_System_IConvertible_ToSByte_m3428,
	Int64_System_IConvertible_ToSingle_m3429,
	Int64_System_IConvertible_ToType_m3430,
	Int64_System_IConvertible_ToUInt16_m3431,
	Int64_System_IConvertible_ToUInt32_m3432,
	Int64_System_IConvertible_ToUInt64_m3433,
	Int64_CompareTo_m3434,
	Int64_Equals_m3435,
	Int64_GetHashCode_m3436,
	Int64_CompareTo_m3437,
	Int64_Equals_m3438,
	Int64_Parse_m3439,
	Int64_Parse_m3440,
	Int64_Parse_m3441,
	Int64_Parse_m3442,
	Int64_Parse_m3443,
	Int64_TryParse_m3444,
	Int64_TryParse_m1279,
	Int64_ToString_m2261,
	Int64_ToString_m1280,
	Int64_ToString_m3445,
	Int64_ToString_m3446,
	UInt32_System_IConvertible_ToBoolean_m3447,
	UInt32_System_IConvertible_ToByte_m3448,
	UInt32_System_IConvertible_ToChar_m3449,
	UInt32_System_IConvertible_ToDateTime_m3450,
	UInt32_System_IConvertible_ToDecimal_m3451,
	UInt32_System_IConvertible_ToDouble_m3452,
	UInt32_System_IConvertible_ToInt16_m3453,
	UInt32_System_IConvertible_ToInt32_m3454,
	UInt32_System_IConvertible_ToInt64_m3455,
	UInt32_System_IConvertible_ToSByte_m3456,
	UInt32_System_IConvertible_ToSingle_m3457,
	UInt32_System_IConvertible_ToType_m3458,
	UInt32_System_IConvertible_ToUInt16_m3459,
	UInt32_System_IConvertible_ToUInt32_m3460,
	UInt32_System_IConvertible_ToUInt64_m3461,
	UInt32_CompareTo_m3462,
	UInt32_Equals_m3463,
	UInt32_GetHashCode_m3464,
	UInt32_CompareTo_m3465,
	UInt32_Equals_m3466,
	UInt32_Parse_m3467,
	UInt32_Parse_m3468,
	UInt32_Parse_m3469,
	UInt32_Parse_m3470,
	UInt32_TryParse_m2400,
	UInt32_TryParse_m1273,
	UInt32_ToString_m1257,
	UInt32_ToString_m1283,
	UInt32_ToString_m3471,
	UInt32_ToString_m3472,
	CLSCompliantAttribute__ctor_m3473,
	UInt64_System_IConvertible_ToBoolean_m3474,
	UInt64_System_IConvertible_ToByte_m3475,
	UInt64_System_IConvertible_ToChar_m3476,
	UInt64_System_IConvertible_ToDateTime_m3477,
	UInt64_System_IConvertible_ToDecimal_m3478,
	UInt64_System_IConvertible_ToDouble_m3479,
	UInt64_System_IConvertible_ToInt16_m3480,
	UInt64_System_IConvertible_ToInt32_m3481,
	UInt64_System_IConvertible_ToInt64_m3482,
	UInt64_System_IConvertible_ToSByte_m3483,
	UInt64_System_IConvertible_ToSingle_m3484,
	UInt64_System_IConvertible_ToType_m3485,
	UInt64_System_IConvertible_ToUInt16_m3486,
	UInt64_System_IConvertible_ToUInt32_m3487,
	UInt64_System_IConvertible_ToUInt64_m3488,
	UInt64_CompareTo_m3489,
	UInt64_Equals_m3490,
	UInt64_GetHashCode_m3491,
	UInt64_CompareTo_m3492,
	UInt64_Equals_m3493,
	UInt64_Parse_m3494,
	UInt64_Parse_m3495,
	UInt64_Parse_m3496,
	UInt64_TryParse_m1255,
	UInt64_ToString_m3497,
	UInt64_ToString_m1281,
	UInt64_ToString_m3498,
	UInt64_ToString_m3499,
	Byte_System_IConvertible_ToType_m3500,
	Byte_System_IConvertible_ToBoolean_m3501,
	Byte_System_IConvertible_ToByte_m3502,
	Byte_System_IConvertible_ToChar_m3503,
	Byte_System_IConvertible_ToDateTime_m3504,
	Byte_System_IConvertible_ToDecimal_m3505,
	Byte_System_IConvertible_ToDouble_m3506,
	Byte_System_IConvertible_ToInt16_m3507,
	Byte_System_IConvertible_ToInt32_m3508,
	Byte_System_IConvertible_ToInt64_m3509,
	Byte_System_IConvertible_ToSByte_m3510,
	Byte_System_IConvertible_ToSingle_m3511,
	Byte_System_IConvertible_ToUInt16_m3512,
	Byte_System_IConvertible_ToUInt32_m3513,
	Byte_System_IConvertible_ToUInt64_m3514,
	Byte_CompareTo_m3515,
	Byte_Equals_m3516,
	Byte_GetHashCode_m3517,
	Byte_CompareTo_m3518,
	Byte_Equals_m3519,
	Byte_Parse_m3520,
	Byte_Parse_m3521,
	Byte_Parse_m3522,
	Byte_TryParse_m3523,
	Byte_TryParse_m3524,
	Byte_ToString_m3328,
	Byte_ToString_m2315,
	Byte_ToString_m3291,
	Byte_ToString_m3295,
	SByte_System_IConvertible_ToBoolean_m3525,
	SByte_System_IConvertible_ToByte_m3526,
	SByte_System_IConvertible_ToChar_m3527,
	SByte_System_IConvertible_ToDateTime_m3528,
	SByte_System_IConvertible_ToDecimal_m3529,
	SByte_System_IConvertible_ToDouble_m3530,
	SByte_System_IConvertible_ToInt16_m3531,
	SByte_System_IConvertible_ToInt32_m3532,
	SByte_System_IConvertible_ToInt64_m3533,
	SByte_System_IConvertible_ToSByte_m3534,
	SByte_System_IConvertible_ToSingle_m3535,
	SByte_System_IConvertible_ToType_m3536,
	SByte_System_IConvertible_ToUInt16_m3537,
	SByte_System_IConvertible_ToUInt32_m3538,
	SByte_System_IConvertible_ToUInt64_m3539,
	SByte_CompareTo_m3540,
	SByte_Equals_m3541,
	SByte_GetHashCode_m3542,
	SByte_CompareTo_m3543,
	SByte_Equals_m3544,
	SByte_Parse_m3545,
	SByte_Parse_m3546,
	SByte_Parse_m3547,
	SByte_TryParse_m3548,
	SByte_ToString_m3549,
	SByte_ToString_m3550,
	SByte_ToString_m3551,
	SByte_ToString_m3552,
	Int16_System_IConvertible_ToBoolean_m3553,
	Int16_System_IConvertible_ToByte_m3554,
	Int16_System_IConvertible_ToChar_m3555,
	Int16_System_IConvertible_ToDateTime_m3556,
	Int16_System_IConvertible_ToDecimal_m3557,
	Int16_System_IConvertible_ToDouble_m3558,
	Int16_System_IConvertible_ToInt16_m3559,
	Int16_System_IConvertible_ToInt32_m3560,
	Int16_System_IConvertible_ToInt64_m3561,
	Int16_System_IConvertible_ToSByte_m3562,
	Int16_System_IConvertible_ToSingle_m3563,
	Int16_System_IConvertible_ToType_m3564,
	Int16_System_IConvertible_ToUInt16_m3565,
	Int16_System_IConvertible_ToUInt32_m3566,
	Int16_System_IConvertible_ToUInt64_m3567,
	Int16_CompareTo_m3568,
	Int16_Equals_m3569,
	Int16_GetHashCode_m3570,
	Int16_CompareTo_m3571,
	Int16_Equals_m3572,
	Int16_Parse_m3573,
	Int16_Parse_m3574,
	Int16_Parse_m3575,
	Int16_TryParse_m3576,
	Int16_ToString_m3577,
	Int16_ToString_m3578,
	Int16_ToString_m3579,
	Int16_ToString_m3580,
	UInt16_System_IConvertible_ToBoolean_m3581,
	UInt16_System_IConvertible_ToByte_m3582,
	UInt16_System_IConvertible_ToChar_m3583,
	UInt16_System_IConvertible_ToDateTime_m3584,
	UInt16_System_IConvertible_ToDecimal_m3585,
	UInt16_System_IConvertible_ToDouble_m3586,
	UInt16_System_IConvertible_ToInt16_m3587,
	UInt16_System_IConvertible_ToInt32_m3588,
	UInt16_System_IConvertible_ToInt64_m3589,
	UInt16_System_IConvertible_ToSByte_m3590,
	UInt16_System_IConvertible_ToSingle_m3591,
	UInt16_System_IConvertible_ToType_m3592,
	UInt16_System_IConvertible_ToUInt16_m3593,
	UInt16_System_IConvertible_ToUInt32_m3594,
	UInt16_System_IConvertible_ToUInt64_m3595,
	UInt16_CompareTo_m3596,
	UInt16_Equals_m3597,
	UInt16_GetHashCode_m3598,
	UInt16_CompareTo_m3599,
	UInt16_Equals_m3600,
	UInt16_Parse_m3601,
	UInt16_Parse_m3602,
	UInt16_TryParse_m3603,
	UInt16_TryParse_m3604,
	UInt16_ToString_m3605,
	UInt16_ToString_m3606,
	UInt16_ToString_m3607,
	UInt16_ToString_m3608,
	Char__cctor_m3609,
	Char_System_IConvertible_ToType_m3610,
	Char_System_IConvertible_ToBoolean_m3611,
	Char_System_IConvertible_ToByte_m3612,
	Char_System_IConvertible_ToChar_m3613,
	Char_System_IConvertible_ToDateTime_m3614,
	Char_System_IConvertible_ToDecimal_m3615,
	Char_System_IConvertible_ToDouble_m3616,
	Char_System_IConvertible_ToInt16_m3617,
	Char_System_IConvertible_ToInt32_m3618,
	Char_System_IConvertible_ToInt64_m3619,
	Char_System_IConvertible_ToSByte_m3620,
	Char_System_IConvertible_ToSingle_m3621,
	Char_System_IConvertible_ToUInt16_m3622,
	Char_System_IConvertible_ToUInt32_m3623,
	Char_System_IConvertible_ToUInt64_m3624,
	Char_GetDataTablePointers_m3625,
	Char_CompareTo_m3626,
	Char_Equals_m3627,
	Char_CompareTo_m3628,
	Char_Equals_m3629,
	Char_GetHashCode_m3630,
	Char_GetUnicodeCategory_m2385,
	Char_IsDigit_m2383,
	Char_IsLetter_m3631,
	Char_IsLetterOrDigit_m2382,
	Char_IsLower_m3632,
	Char_IsSurrogate_m3633,
	Char_IsWhiteSpace_m2384,
	Char_IsWhiteSpace_m2301,
	Char_CheckParameter_m3634,
	Char_Parse_m3635,
	Char_ToLower_m2386,
	Char_ToLowerInvariant_m3636,
	Char_ToLower_m3637,
	Char_ToUpper_m2390,
	Char_ToUpperInvariant_m2303,
	Char_ToString_m2391,
	Char_ToString_m3638,
	String__ctor_m3639,
	String__ctor_m3640,
	String__ctor_m3641,
	String__ctor_m3642,
	String__cctor_m3643,
	String_System_IConvertible_ToBoolean_m3644,
	String_System_IConvertible_ToByte_m3645,
	String_System_IConvertible_ToChar_m3646,
	String_System_IConvertible_ToDateTime_m3647,
	String_System_IConvertible_ToDecimal_m3648,
	String_System_IConvertible_ToDouble_m3649,
	String_System_IConvertible_ToInt16_m3650,
	String_System_IConvertible_ToInt32_m3651,
	String_System_IConvertible_ToInt64_m3652,
	String_System_IConvertible_ToSByte_m3653,
	String_System_IConvertible_ToSingle_m3654,
	String_System_IConvertible_ToType_m3655,
	String_System_IConvertible_ToUInt16_m3656,
	String_System_IConvertible_ToUInt32_m3657,
	String_System_IConvertible_ToUInt64_m3658,
	String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m3659,
	String_System_Collections_IEnumerable_GetEnumerator_m3660,
	String_Equals_m3661,
	String_Equals_m3662,
	String_Equals_m2362,
	String_get_Chars_m1318,
	String_CopyTo_m3663,
	String_ToCharArray_m1267,
	String_ToCharArray_m3664,
	String_Split_m1317,
	String_Split_m3665,
	String_Split_m3666,
	String_Split_m3667,
	String_Split_m2304,
	String_Substring_m1202,
	String_Substring_m1206,
	String_SubstringUnchecked_m3668,
	String_Trim_m1203,
	String_Trim_m1204,
	String_TrimStart_m2402,
	String_TrimEnd_m2302,
	String_FindNotWhiteSpace_m3669,
	String_FindNotInTable_m3670,
	String_Compare_m3671,
	String_Compare_m3672,
	String_Compare_m2276,
	String_Compare_m3360,
	String_CompareTo_m3673,
	String_CompareTo_m3674,
	String_CompareOrdinal_m2398,
	String_CompareOrdinalUnchecked_m3675,
	String_CompareOrdinalCaseInsensitiveUnchecked_m3676,
	String_EndsWith_m1319,
	String_IndexOfAny_m2397,
	String_IndexOfAny_m3677,
	String_IndexOfAny_m3313,
	String_IndexOfAnyUnchecked_m3678,
	String_IndexOf_m1200,
	String_IndexOf_m3679,
	String_IndexOfOrdinal_m3680,
	String_IndexOfOrdinalUnchecked_m3681,
	String_IndexOfOrdinalIgnoreCaseUnchecked_m3682,
	String_IndexOf_m1205,
	String_IndexOf_m1201,
	String_IndexOf_m2403,
	String_IndexOfUnchecked_m3683,
	String_IndexOf_m1218,
	String_IndexOf_m1321,
	String_IndexOf_m3684,
	String_LastIndexOfAny_m3685,
	String_LastIndexOfAnyUnchecked_m3686,
	String_LastIndexOf_m2263,
	String_LastIndexOf_m2399,
	String_LastIndexOf_m2404,
	String_LastIndexOfUnchecked_m3687,
	String_LastIndexOf_m1324,
	String_LastIndexOf_m3688,
	String_IsNullOrEmpty_m1234,
	String_PadRight_m3689,
	String_StartsWith_m1217,
	String_Replace_m1323,
	String_Replace_m1322,
	String_ReplaceUnchecked_m3690,
	String_ReplaceFallback_m3691,
	String_Remove_m1320,
	String_ToLower_m2389,
	String_ToLower_m2401,
	String_ToLowerInvariant_m3692,
	String_ToUpper_m1219,
	String_ToUpper_m3693,
	String_ToUpperInvariant_m3694,
	String_ToString_m1212,
	String_ToString_m3695,
	String_Format_m2310,
	String_Format_m3696,
	String_Format_m3697,
	String_Format_m1225,
	String_Format_m3339,
	String_FormatHelper_m3698,
	String_Concat_m1194,
	String_Concat_m2250,
	String_Concat_m1184,
	String_Concat_m1188,
	String_Concat_m1316,
	String_Concat_m1186,
	String_Concat_m1224,
	String_ConcatInternal_m3699,
	String_Insert_m1325,
	String_Join_m3700,
	String_Join_m3701,
	String_JoinUnchecked_m3702,
	String_get_Length_m1161,
	String_ParseFormatSpecifier_m3703,
	String_ParseDecimal_m3704,
	String_InternalSetChar_m3705,
	String_InternalSetLength_m3706,
	String_GetHashCode_m1178,
	String_GetCaseInsensitiveHashCode_m3707,
	String_CreateString_m3708,
	String_CreateString_m3709,
	String_CreateString_m3710,
	String_CreateString_m3711,
	String_CreateString_m3712,
	String_CreateString_m1271,
	String_CreateString_m1277,
	String_CreateString_m1276,
	String_memcpy4_m3713,
	String_memcpy2_m3714,
	String_memcpy1_m3715,
	String_memcpy_m3716,
	String_CharCopy_m3717,
	String_CharCopyReverse_m3718,
	String_CharCopy_m3719,
	String_CharCopy_m3720,
	String_CharCopyReverse_m3721,
	String_InternalSplit_m3722,
	String_InternalAllocateStr_m3723,
	String_op_Equality_m1170,
	String_op_Inequality_m2269,
	Single_System_IConvertible_ToBoolean_m3724,
	Single_System_IConvertible_ToByte_m3725,
	Single_System_IConvertible_ToChar_m3726,
	Single_System_IConvertible_ToDateTime_m3727,
	Single_System_IConvertible_ToDecimal_m3728,
	Single_System_IConvertible_ToDouble_m3729,
	Single_System_IConvertible_ToInt16_m3730,
	Single_System_IConvertible_ToInt32_m3731,
	Single_System_IConvertible_ToInt64_m3732,
	Single_System_IConvertible_ToSByte_m3733,
	Single_System_IConvertible_ToSingle_m3734,
	Single_System_IConvertible_ToType_m3735,
	Single_System_IConvertible_ToUInt16_m3736,
	Single_System_IConvertible_ToUInt32_m3737,
	Single_System_IConvertible_ToUInt64_m3738,
	Single_CompareTo_m3739,
	Single_Equals_m3740,
	Single_CompareTo_m3741,
	Single_Equals_m1195,
	Single_GetHashCode_m1176,
	Single_IsInfinity_m3742,
	Single_IsNaN_m3743,
	Single_IsNegativeInfinity_m3744,
	Single_IsPositiveInfinity_m3745,
	Single_Parse_m3746,
	Single_ToString_m3747,
	Single_ToString_m1285,
	Single_ToString_m1196,
	Single_ToString_m3748,
	Double_System_IConvertible_ToType_m3749,
	Double_System_IConvertible_ToBoolean_m3750,
	Double_System_IConvertible_ToByte_m3751,
	Double_System_IConvertible_ToChar_m3752,
	Double_System_IConvertible_ToDateTime_m3753,
	Double_System_IConvertible_ToDecimal_m3754,
	Double_System_IConvertible_ToDouble_m3755,
	Double_System_IConvertible_ToInt16_m3756,
	Double_System_IConvertible_ToInt32_m3757,
	Double_System_IConvertible_ToInt64_m3758,
	Double_System_IConvertible_ToSByte_m3759,
	Double_System_IConvertible_ToSingle_m3760,
	Double_System_IConvertible_ToUInt16_m3761,
	Double_System_IConvertible_ToUInt32_m3762,
	Double_System_IConvertible_ToUInt64_m3763,
	Double_CompareTo_m3764,
	Double_Equals_m3765,
	Double_CompareTo_m3766,
	Double_Equals_m3767,
	Double_GetHashCode_m3768,
	Double_IsInfinity_m3769,
	Double_IsNaN_m3770,
	Double_IsNegativeInfinity_m3771,
	Double_IsPositiveInfinity_m3772,
	Double_Parse_m3773,
	Double_Parse_m3774,
	Double_Parse_m3775,
	Double_Parse_m3776,
	Double_TryParseStringConstant_m3777,
	Double_ParseImpl_m3778,
	Double_TryParse_m1278,
	Double_ToString_m3779,
	Double_ToString_m3780,
	Double_ToString_m1287,
	Decimal__ctor_m3781,
	Decimal__ctor_m3782,
	Decimal__ctor_m3783,
	Decimal__ctor_m3784,
	Decimal__ctor_m3785,
	Decimal__ctor_m3786,
	Decimal__ctor_m3787,
	Decimal__cctor_m3788,
	Decimal_System_IConvertible_ToType_m3789,
	Decimal_System_IConvertible_ToBoolean_m3790,
	Decimal_System_IConvertible_ToByte_m3791,
	Decimal_System_IConvertible_ToChar_m3792,
	Decimal_System_IConvertible_ToDateTime_m3793,
	Decimal_System_IConvertible_ToDecimal_m3794,
	Decimal_System_IConvertible_ToDouble_m3795,
	Decimal_System_IConvertible_ToInt16_m3796,
	Decimal_System_IConvertible_ToInt32_m3797,
	Decimal_System_IConvertible_ToInt64_m3798,
	Decimal_System_IConvertible_ToSByte_m3799,
	Decimal_System_IConvertible_ToSingle_m3800,
	Decimal_System_IConvertible_ToUInt16_m3801,
	Decimal_System_IConvertible_ToUInt32_m3802,
	Decimal_System_IConvertible_ToUInt64_m3803,
	Decimal_GetBits_m3804,
	Decimal_Add_m3805,
	Decimal_Subtract_m3806,
	Decimal_GetHashCode_m3807,
	Decimal_u64_m3808,
	Decimal_s64_m3809,
	Decimal_Equals_m3810,
	Decimal_Equals_m3811,
	Decimal_IsZero_m3812,
	Decimal_Floor_m3813,
	Decimal_Multiply_m3814,
	Decimal_Divide_m3815,
	Decimal_Compare_m3816,
	Decimal_CompareTo_m3817,
	Decimal_CompareTo_m3818,
	Decimal_Equals_m3819,
	Decimal_Parse_m3820,
	Decimal_ThrowAtPos_m3821,
	Decimal_ThrowInvalidExp_m3822,
	Decimal_stripStyles_m3823,
	Decimal_Parse_m3824,
	Decimal_PerformParse_m3825,
	Decimal_ToString_m3826,
	Decimal_ToString_m3827,
	Decimal_ToString_m1284,
	Decimal_decimal2UInt64_m3828,
	Decimal_decimal2Int64_m3829,
	Decimal_decimalIncr_m3830,
	Decimal_string2decimal_m3831,
	Decimal_decimalSetExponent_m3832,
	Decimal_decimal2double_m3833,
	Decimal_decimalFloorAndTrunc_m3834,
	Decimal_decimalMult_m3835,
	Decimal_decimalDiv_m3836,
	Decimal_decimalCompare_m3837,
	Decimal_op_Increment_m3838,
	Decimal_op_Subtraction_m3839,
	Decimal_op_Multiply_m3840,
	Decimal_op_Division_m3841,
	Decimal_op_Explicit_m3842,
	Decimal_op_Explicit_m3843,
	Decimal_op_Explicit_m3844,
	Decimal_op_Explicit_m3845,
	Decimal_op_Explicit_m3846,
	Decimal_op_Explicit_m3847,
	Decimal_op_Explicit_m3848,
	Decimal_op_Explicit_m3849,
	Decimal_op_Implicit_m3850,
	Decimal_op_Implicit_m3851,
	Decimal_op_Implicit_m3852,
	Decimal_op_Implicit_m3853,
	Decimal_op_Implicit_m3854,
	Decimal_op_Implicit_m3855,
	Decimal_op_Implicit_m3856,
	Decimal_op_Implicit_m3857,
	Decimal_op_Explicit_m3858,
	Decimal_op_Explicit_m3859,
	Decimal_op_Explicit_m3860,
	Decimal_op_Explicit_m3861,
	Decimal_op_Inequality_m3862,
	Decimal_op_Equality_m3863,
	Decimal_op_GreaterThan_m3864,
	Decimal_op_LessThan_m3865,
	Boolean__cctor_m3866,
	Boolean_System_IConvertible_ToType_m3867,
	Boolean_System_IConvertible_ToBoolean_m3868,
	Boolean_System_IConvertible_ToByte_m3869,
	Boolean_System_IConvertible_ToChar_m3870,
	Boolean_System_IConvertible_ToDateTime_m3871,
	Boolean_System_IConvertible_ToDecimal_m3872,
	Boolean_System_IConvertible_ToDouble_m3873,
	Boolean_System_IConvertible_ToInt16_m3874,
	Boolean_System_IConvertible_ToInt32_m3875,
	Boolean_System_IConvertible_ToInt64_m3876,
	Boolean_System_IConvertible_ToSByte_m3877,
	Boolean_System_IConvertible_ToSingle_m3878,
	Boolean_System_IConvertible_ToUInt16_m3879,
	Boolean_System_IConvertible_ToUInt32_m3880,
	Boolean_System_IConvertible_ToUInt64_m3881,
	Boolean_CompareTo_m3882,
	Boolean_Equals_m3883,
	Boolean_CompareTo_m3884,
	Boolean_Equals_m3885,
	Boolean_GetHashCode_m3886,
	Boolean_Parse_m3887,
	Boolean_ToString_m1258,
	Boolean_ToString_m3888,
	IntPtr__ctor_m1226,
	IntPtr__ctor_m3889,
	IntPtr__ctor_m3890,
	IntPtr__ctor_m3891,
	IntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m3892,
	IntPtr_get_Size_m3893,
	IntPtr_Equals_m3894,
	IntPtr_GetHashCode_m3895,
	IntPtr_ToInt64_m3896,
	IntPtr_ToString_m3897,
	IntPtr_ToString_m3898,
	IntPtr_op_Equality_m1331,
	IntPtr_op_Inequality_m1229,
	IntPtr_op_Explicit_m3899,
	IntPtr_op_Explicit_m3900,
	IntPtr_op_Explicit_m3901,
	IntPtr_op_Explicit_m1330,
	IntPtr_op_Explicit_m3902,
	UIntPtr__ctor_m3903,
	UIntPtr__ctor_m3904,
	UIntPtr__ctor_m3905,
	UIntPtr__cctor_m3906,
	UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m3907,
	UIntPtr_Equals_m3908,
	UIntPtr_GetHashCode_m3909,
	UIntPtr_ToUInt32_m3910,
	UIntPtr_ToUInt64_m3911,
	UIntPtr_ToPointer_m3912,
	UIntPtr_ToString_m3913,
	UIntPtr_get_Size_m3914,
	UIntPtr_op_Equality_m3915,
	UIntPtr_op_Inequality_m3916,
	UIntPtr_op_Explicit_m3917,
	UIntPtr_op_Explicit_m3918,
	UIntPtr_op_Explicit_m3919,
	UIntPtr_op_Explicit_m3920,
	UIntPtr_op_Explicit_m3921,
	UIntPtr_op_Explicit_m3922,
	MulticastDelegate_GetObjectData_m3923,
	MulticastDelegate_Equals_m3924,
	MulticastDelegate_GetHashCode_m3925,
	MulticastDelegate_GetInvocationList_m3926,
	MulticastDelegate_CombineImpl_m3927,
	MulticastDelegate_BaseEquals_m3928,
	MulticastDelegate_KPM_m3929,
	MulticastDelegate_RemoveImpl_m3930,
	Delegate_get_Method_m3931,
	Delegate_get_Target_m3932,
	Delegate_CreateDelegate_internal_m3933,
	Delegate_SetMulticastInvoke_m3934,
	Delegate_arg_type_match_m3935,
	Delegate_return_type_match_m3936,
	Delegate_CreateDelegate_m3937,
	Delegate_CreateDelegate_m3938,
	Delegate_CreateDelegate_m3939,
	Delegate_CreateDelegate_m3940,
	Delegate_GetCandidateMethod_m3941,
	Delegate_CreateDelegate_m3942,
	Delegate_CreateDelegate_m3943,
	Delegate_CreateDelegate_m3944,
	Delegate_CreateDelegate_m3945,
	Delegate_Clone_m3946,
	Delegate_Equals_m3947,
	Delegate_GetHashCode_m3948,
	Delegate_GetObjectData_m3949,
	Delegate_GetInvocationList_m3950,
	Delegate_Combine_m1227,
	Delegate_Combine_m3951,
	Delegate_CombineImpl_m3952,
	Delegate_Remove_m1228,
	Delegate_RemoveImpl_m3953,
	Enum__ctor_m3954,
	Enum__cctor_m3955,
	Enum_System_IConvertible_ToBoolean_m3956,
	Enum_System_IConvertible_ToByte_m3957,
	Enum_System_IConvertible_ToChar_m3958,
	Enum_System_IConvertible_ToDateTime_m3959,
	Enum_System_IConvertible_ToDecimal_m3960,
	Enum_System_IConvertible_ToDouble_m3961,
	Enum_System_IConvertible_ToInt16_m3962,
	Enum_System_IConvertible_ToInt32_m3963,
	Enum_System_IConvertible_ToInt64_m3964,
	Enum_System_IConvertible_ToSByte_m3965,
	Enum_System_IConvertible_ToSingle_m3966,
	Enum_System_IConvertible_ToType_m3967,
	Enum_System_IConvertible_ToUInt16_m3968,
	Enum_System_IConvertible_ToUInt32_m3969,
	Enum_System_IConvertible_ToUInt64_m3970,
	Enum_GetTypeCode_m3971,
	Enum_get_value_m3972,
	Enum_get_Value_m3973,
	Enum_FindPosition_m3974,
	Enum_GetName_m3975,
	Enum_IsDefined_m3348,
	Enum_get_underlying_type_m3976,
	Enum_GetUnderlyingType_m3977,
	Enum_FindName_m3978,
	Enum_GetValue_m3979,
	Enum_Parse_m2381,
	Enum_compare_value_to_m3980,
	Enum_CompareTo_m3981,
	Enum_ToString_m3982,
	Enum_ToString_m3983,
	Enum_ToString_m1236,
	Enum_ToString_m3984,
	Enum_ToObject_m3985,
	Enum_ToObject_m3986,
	Enum_ToObject_m3987,
	Enum_ToObject_m3988,
	Enum_ToObject_m3989,
	Enum_ToObject_m3990,
	Enum_ToObject_m3991,
	Enum_ToObject_m3992,
	Enum_ToObject_m3993,
	Enum_Equals_m3994,
	Enum_get_hashcode_m3995,
	Enum_GetHashCode_m3996,
	Enum_FormatSpecifier_X_m3997,
	Enum_FormatFlags_m3998,
	Enum_Format_m3999,
	SimpleEnumerator__ctor_m4000,
	SimpleEnumerator_get_Current_m4001,
	SimpleEnumerator_MoveNext_m4002,
	Swapper__ctor_m4003,
	Swapper_Invoke_m4004,
	Swapper_BeginInvoke_m4005,
	Swapper_EndInvoke_m4006,
	Array__ctor_m4007,
	Array_System_Collections_IList_get_Item_m4008,
	Array_System_Collections_IList_set_Item_m4009,
	Array_System_Collections_IList_Add_m4010,
	Array_System_Collections_IList_Clear_m4011,
	Array_System_Collections_IList_Contains_m4012,
	Array_System_Collections_IList_IndexOf_m4013,
	Array_System_Collections_IList_Insert_m4014,
	Array_System_Collections_IList_Remove_m4015,
	Array_System_Collections_IList_RemoveAt_m4016,
	Array_System_Collections_ICollection_get_Count_m4017,
	Array_InternalArray__ICollection_get_Count_m4018,
	Array_InternalArray__ICollection_get_IsReadOnly_m4019,
	Array_InternalArray__ICollection_Clear_m4020,
	Array_InternalArray__RemoveAt_m4021,
	Array_get_Length_m2235,
	Array_get_LongLength_m4022,
	Array_get_Rank_m2240,
	Array_GetRank_m4023,
	Array_GetLength_m4024,
	Array_GetLongLength_m4025,
	Array_GetLowerBound_m4026,
	Array_GetValue_m4027,
	Array_SetValue_m4028,
	Array_GetValueImpl_m4029,
	Array_SetValueImpl_m4030,
	Array_FastCopy_m4031,
	Array_CreateInstanceImpl_m4032,
	Array_get_IsSynchronized_m4033,
	Array_get_SyncRoot_m4034,
	Array_get_IsFixedSize_m4035,
	Array_get_IsReadOnly_m4036,
	Array_GetEnumerator_m4037,
	Array_GetUpperBound_m4038,
	Array_GetValue_m4039,
	Array_GetValue_m4040,
	Array_GetValue_m4041,
	Array_GetValue_m4042,
	Array_GetValue_m4043,
	Array_GetValue_m4044,
	Array_SetValue_m4045,
	Array_SetValue_m4046,
	Array_SetValue_m4047,
	Array_SetValue_m2236,
	Array_SetValue_m4048,
	Array_SetValue_m4049,
	Array_CreateInstance_m4050,
	Array_CreateInstance_m4051,
	Array_CreateInstance_m4052,
	Array_CreateInstance_m4053,
	Array_CreateInstance_m4054,
	Array_GetIntArray_m4055,
	Array_CreateInstance_m4056,
	Array_GetValue_m4057,
	Array_SetValue_m4058,
	Array_BinarySearch_m4059,
	Array_BinarySearch_m4060,
	Array_BinarySearch_m4061,
	Array_BinarySearch_m4062,
	Array_DoBinarySearch_m4063,
	Array_Clear_m2458,
	Array_ClearInternal_m4064,
	Array_Clone_m4065,
	Array_Copy_m2392,
	Array_Copy_m4066,
	Array_Copy_m4067,
	Array_Copy_m4068,
	Array_IndexOf_m4069,
	Array_IndexOf_m4070,
	Array_IndexOf_m4071,
	Array_Initialize_m4072,
	Array_LastIndexOf_m4073,
	Array_LastIndexOf_m4074,
	Array_LastIndexOf_m4075,
	Array_get_swapper_m4076,
	Array_Reverse_m3290,
	Array_Reverse_m3314,
	Array_Sort_m4077,
	Array_Sort_m4078,
	Array_Sort_m4079,
	Array_Sort_m4080,
	Array_Sort_m4081,
	Array_Sort_m4082,
	Array_Sort_m4083,
	Array_Sort_m4084,
	Array_int_swapper_m4085,
	Array_obj_swapper_m4086,
	Array_slow_swapper_m4087,
	Array_double_swapper_m4088,
	Array_new_gap_m4089,
	Array_combsort_m4090,
	Array_combsort_m4091,
	Array_combsort_m4092,
	Array_qsort_m4093,
	Array_swap_m4094,
	Array_compare_m4095,
	Array_CopyTo_m4096,
	Array_CopyTo_m4097,
	Array_ConstrainedCopy_m4098,
	Type__ctor_m4099,
	Type__cctor_m4100,
	Type_FilterName_impl_m4101,
	Type_FilterNameIgnoreCase_impl_m4102,
	Type_FilterAttribute_impl_m4103,
	Type_get_Attributes_m4104,
	Type_get_DeclaringType_m4105,
	Type_get_HasElementType_m4106,
	Type_get_IsAbstract_m4107,
	Type_get_IsArray_m4108,
	Type_get_IsByRef_m4109,
	Type_get_IsClass_m4110,
	Type_get_IsContextful_m4111,
	Type_get_IsEnum_m4112,
	Type_get_IsExplicitLayout_m4113,
	Type_get_IsInterface_m4114,
	Type_get_IsMarshalByRef_m4115,
	Type_get_IsPointer_m4116,
	Type_get_IsPrimitive_m4117,
	Type_get_IsSealed_m4118,
	Type_get_IsSerializable_m4119,
	Type_get_IsValueType_m4120,
	Type_get_MemberType_m4121,
	Type_get_ReflectedType_m4122,
	Type_get_TypeHandle_m4123,
	Type_Equals_m4124,
	Type_Equals_m4125,
	Type_EqualsInternal_m4126,
	Type_internal_from_handle_m4127,
	Type_internal_from_name_m4128,
	Type_GetType_m4129,
	Type_GetType_m4130,
	Type_GetTypeCodeInternal_m4131,
	Type_GetTypeCode_m4132,
	Type_GetTypeFromHandle_m1294,
	Type_GetTypeHandle_m4133,
	Type_type_is_subtype_of_m4134,
	Type_type_is_assignable_from_m4135,
	Type_IsSubclassOf_m4136,
	Type_IsAssignableFrom_m4137,
	Type_IsInstanceOfType_m4138,
	Type_GetHashCode_m4139,
	Type_GetMethod_m4140,
	Type_GetMethod_m4141,
	Type_GetMethod_m4142,
	Type_GetMethod_m4143,
	Type_GetProperty_m4144,
	Type_GetProperty_m4145,
	Type_GetProperty_m4146,
	Type_GetProperty_m4147,
	Type_IsArrayImpl_m4148,
	Type_IsValueTypeImpl_m4149,
	Type_IsContextfulImpl_m4150,
	Type_IsMarshalByRefImpl_m4151,
	Type_GetConstructor_m4152,
	Type_GetConstructor_m4153,
	Type_GetConstructor_m4154,
	Type_GetConstructors_m4155,
	Type_ToString_m4156,
	Type_get_IsSystemType_m4157,
	Type_GetGenericArguments_m4158,
	Type_get_ContainsGenericParameters_m4159,
	Type_get_IsGenericTypeDefinition_m4160,
	Type_GetGenericTypeDefinition_impl_m4161,
	Type_GetGenericTypeDefinition_m4162,
	Type_get_IsGenericType_m4163,
	Type_MakeGenericType_m4164,
	Type_MakeGenericType_m4165,
	Type_get_IsGenericParameter_m4166,
	Type_get_IsNested_m4167,
	Type_GetPseudoCustomAttributes_m4168,
	MemberInfo__ctor_m4169,
	MemberInfo_get_Module_m4170,
	Exception__ctor_m3287,
	Exception__ctor_m1326,
	Exception__ctor_m1329,
	Exception__ctor_m1328,
	Exception_get_InnerException_m4171,
	Exception_set_HResult_m1327,
	Exception_get_ClassName_m4172,
	Exception_get_Message_m4173,
	Exception_get_Source_m4174,
	Exception_get_StackTrace_m4175,
	Exception_GetObjectData_m2408,
	Exception_ToString_m4176,
	Exception_GetFullNameForStackTrace_m4177,
	Exception_GetType_m4178,
	RuntimeFieldHandle__ctor_m4179,
	RuntimeFieldHandle_get_Value_m4180,
	RuntimeFieldHandle_GetObjectData_m4181,
	RuntimeFieldHandle_Equals_m4182,
	RuntimeFieldHandle_GetHashCode_m4183,
	RuntimeTypeHandle__ctor_m4184,
	RuntimeTypeHandle_get_Value_m4185,
	RuntimeTypeHandle_GetObjectData_m4186,
	RuntimeTypeHandle_Equals_m4187,
	RuntimeTypeHandle_GetHashCode_m4188,
	ParamArrayAttribute__ctor_m4189,
	OutAttribute__ctor_m4190,
	ObsoleteAttribute__ctor_m4191,
	ObsoleteAttribute__ctor_m4192,
	ObsoleteAttribute__ctor_m4193,
	DllImportAttribute__ctor_m4194,
	DllImportAttribute_get_Value_m4195,
	MarshalAsAttribute__ctor_m4196,
	InAttribute__ctor_m4197,
	ConditionalAttribute__ctor_m4198,
	GuidAttribute__ctor_m4199,
	ComImportAttribute__ctor_m4200,
	OptionalAttribute__ctor_m4201,
	CompilerGeneratedAttribute__ctor_m4202,
	InternalsVisibleToAttribute__ctor_m4203,
	RuntimeCompatibilityAttribute__ctor_m4204,
	RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m4205,
	DebuggerHiddenAttribute__ctor_m4206,
	DefaultMemberAttribute__ctor_m4207,
	DefaultMemberAttribute_get_MemberName_m4208,
	DecimalConstantAttribute__ctor_m4209,
	FieldOffsetAttribute__ctor_m4210,
	AsyncCallback__ctor_m3347,
	AsyncCallback_Invoke_m4211,
	AsyncCallback_BeginInvoke_m3345,
	AsyncCallback_EndInvoke_m4212,
	TypedReference_Equals_m4213,
	TypedReference_GetHashCode_m4214,
	ArgIterator_Equals_m4215,
	ArgIterator_GetHashCode_m4216,
	MarshalByRefObject__ctor_m2277,
	RuntimeHelpers_InitializeArray_m4217,
	RuntimeHelpers_InitializeArray_m2271,
	RuntimeHelpers_get_OffsetToStringData_m4218,
	Locale_GetText_m4219,
	Locale_GetText_m4220,
	MonoTODOAttribute__ctor_m4221,
	MonoTODOAttribute__ctor_m4222,
	MonoDocumentationNoteAttribute__ctor_m4223,
	SafeHandleZeroOrMinusOneIsInvalid__ctor_m4224,
	SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m4225,
	SafeWaitHandle__ctor_m4226,
	SafeWaitHandle_ReleaseHandle_m4227,
	TableRange__ctor_m4228,
	CodePointIndexer__ctor_m4229,
	CodePointIndexer_ToIndex_m4230,
	TailoringInfo__ctor_m4231,
	Contraction__ctor_m4232,
	ContractionComparer__ctor_m4233,
	ContractionComparer__cctor_m4234,
	ContractionComparer_Compare_m4235,
	Level2Map__ctor_m4236,
	Level2MapComparer__ctor_m4237,
	Level2MapComparer__cctor_m4238,
	Level2MapComparer_Compare_m4239,
	MSCompatUnicodeTable__cctor_m4240,
	MSCompatUnicodeTable_GetTailoringInfo_m4241,
	MSCompatUnicodeTable_BuildTailoringTables_m4242,
	MSCompatUnicodeTable_SetCJKReferences_m4243,
	MSCompatUnicodeTable_Category_m4244,
	MSCompatUnicodeTable_Level1_m4245,
	MSCompatUnicodeTable_Level2_m4246,
	MSCompatUnicodeTable_Level3_m4247,
	MSCompatUnicodeTable_IsIgnorable_m4248,
	MSCompatUnicodeTable_IsIgnorableNonSpacing_m4249,
	MSCompatUnicodeTable_ToKanaTypeInsensitive_m4250,
	MSCompatUnicodeTable_ToWidthCompat_m4251,
	MSCompatUnicodeTable_HasSpecialWeight_m4252,
	MSCompatUnicodeTable_IsHalfWidthKana_m4253,
	MSCompatUnicodeTable_IsHiragana_m4254,
	MSCompatUnicodeTable_IsJapaneseSmallLetter_m4255,
	MSCompatUnicodeTable_get_IsReady_m4256,
	MSCompatUnicodeTable_GetResource_m4257,
	MSCompatUnicodeTable_UInt32FromBytePtr_m4258,
	MSCompatUnicodeTable_FillCJK_m4259,
	MSCompatUnicodeTable_FillCJKCore_m4260,
	MSCompatUnicodeTableUtil__cctor_m4261,
	Context__ctor_m4262,
	PreviousInfo__ctor_m4263,
	SimpleCollator__ctor_m4264,
	SimpleCollator__cctor_m4265,
	SimpleCollator_SetCJKTable_m4266,
	SimpleCollator_GetNeutralCulture_m4267,
	SimpleCollator_Category_m4268,
	SimpleCollator_Level1_m4269,
	SimpleCollator_Level2_m4270,
	SimpleCollator_IsHalfKana_m4271,
	SimpleCollator_GetContraction_m4272,
	SimpleCollator_GetContraction_m4273,
	SimpleCollator_GetTailContraction_m4274,
	SimpleCollator_GetTailContraction_m4275,
	SimpleCollator_FilterOptions_m4276,
	SimpleCollator_GetExtenderType_m4277,
	SimpleCollator_ToDashTypeValue_m4278,
	SimpleCollator_FilterExtender_m4279,
	SimpleCollator_IsIgnorable_m4280,
	SimpleCollator_IsSafe_m4281,
	SimpleCollator_GetSortKey_m4282,
	SimpleCollator_GetSortKey_m4283,
	SimpleCollator_GetSortKey_m4284,
	SimpleCollator_FillSortKeyRaw_m4285,
	SimpleCollator_FillSurrogateSortKeyRaw_m4286,
	SimpleCollator_CompareOrdinal_m4287,
	SimpleCollator_CompareQuick_m4288,
	SimpleCollator_CompareOrdinalIgnoreCase_m4289,
	SimpleCollator_Compare_m4290,
	SimpleCollator_ClearBuffer_m4291,
	SimpleCollator_QuickCheckPossible_m4292,
	SimpleCollator_CompareInternal_m4293,
	SimpleCollator_CompareFlagPair_m4294,
	SimpleCollator_IsPrefix_m4295,
	SimpleCollator_IsPrefix_m4296,
	SimpleCollator_IsPrefix_m4297,
	SimpleCollator_IsSuffix_m4298,
	SimpleCollator_IsSuffix_m4299,
	SimpleCollator_QuickIndexOf_m4300,
	SimpleCollator_IndexOf_m4301,
	SimpleCollator_IndexOfOrdinal_m4302,
	SimpleCollator_IndexOfOrdinalIgnoreCase_m4303,
	SimpleCollator_IndexOfSortKey_m4304,
	SimpleCollator_IndexOf_m4305,
	SimpleCollator_LastIndexOf_m4306,
	SimpleCollator_LastIndexOfOrdinal_m4307,
	SimpleCollator_LastIndexOfOrdinalIgnoreCase_m4308,
	SimpleCollator_LastIndexOfSortKey_m4309,
	SimpleCollator_LastIndexOf_m4310,
	SimpleCollator_MatchesForward_m4311,
	SimpleCollator_MatchesForwardCore_m4312,
	SimpleCollator_MatchesPrimitive_m4313,
	SimpleCollator_MatchesBackward_m4314,
	SimpleCollator_MatchesBackwardCore_m4315,
	SortKey__ctor_m4316,
	SortKey__ctor_m4317,
	SortKey_Compare_m4318,
	SortKey_get_OriginalString_m4319,
	SortKey_get_KeyData_m4320,
	SortKey_Equals_m4321,
	SortKey_GetHashCode_m4322,
	SortKey_ToString_m4323,
	SortKeyBuffer__ctor_m4324,
	SortKeyBuffer_Reset_m4325,
	SortKeyBuffer_Initialize_m4326,
	SortKeyBuffer_AppendCJKExtension_m4327,
	SortKeyBuffer_AppendKana_m4328,
	SortKeyBuffer_AppendNormal_m4329,
	SortKeyBuffer_AppendLevel5_m4330,
	SortKeyBuffer_AppendBufferPrimitive_m4331,
	SortKeyBuffer_GetResultAndReset_m4332,
	SortKeyBuffer_GetOptimizedLength_m4333,
	SortKeyBuffer_GetResult_m4334,
	PrimeGeneratorBase__ctor_m4335,
	PrimeGeneratorBase_get_Confidence_m4336,
	PrimeGeneratorBase_get_PrimalityTest_m4337,
	PrimeGeneratorBase_get_TrialDivisionBounds_m4338,
	SequentialSearchPrimeGeneratorBase__ctor_m4339,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m4340,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m4341,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m4342,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m4343,
	PrimalityTests_GetSPPRounds_m4344,
	PrimalityTests_Test_m4345,
	PrimalityTests_RabinMillerTest_m4346,
	PrimalityTests_SmallPrimeSppTest_m4347,
	ModulusRing__ctor_m4348,
	ModulusRing_BarrettReduction_m4349,
	ModulusRing_Multiply_m4350,
	ModulusRing_Difference_m4351,
	ModulusRing_Pow_m4352,
	ModulusRing_Pow_m4353,
	Kernel_AddSameSign_m4354,
	Kernel_Subtract_m4355,
	Kernel_MinusEq_m4356,
	Kernel_PlusEq_m4357,
	Kernel_Compare_m4358,
	Kernel_SingleByteDivideInPlace_m4359,
	Kernel_DwordMod_m4360,
	Kernel_DwordDivMod_m4361,
	Kernel_multiByteDivide_m4362,
	Kernel_LeftShift_m4363,
	Kernel_RightShift_m4364,
	Kernel_MultiplyByDword_m4365,
	Kernel_Multiply_m4366,
	Kernel_MultiplyMod2p32pmod_m4367,
	Kernel_modInverse_m4368,
	Kernel_modInverse_m4369,
	BigInteger__ctor_m4370,
	BigInteger__ctor_m4371,
	BigInteger__ctor_m4372,
	BigInteger__ctor_m4373,
	BigInteger__ctor_m4374,
	BigInteger__cctor_m4375,
	BigInteger_get_Rng_m4376,
	BigInteger_GenerateRandom_m4377,
	BigInteger_GenerateRandom_m4378,
	BigInteger_Randomize_m4379,
	BigInteger_Randomize_m4380,
	BigInteger_BitCount_m4381,
	BigInteger_TestBit_m4382,
	BigInteger_TestBit_m4383,
	BigInteger_SetBit_m4384,
	BigInteger_SetBit_m4385,
	BigInteger_LowestSetBit_m4386,
	BigInteger_GetBytes_m4387,
	BigInteger_ToString_m4388,
	BigInteger_ToString_m4389,
	BigInteger_Normalize_m4390,
	BigInteger_Clear_m4391,
	BigInteger_GetHashCode_m4392,
	BigInteger_ToString_m4393,
	BigInteger_Equals_m4394,
	BigInteger_ModInverse_m4395,
	BigInteger_ModPow_m4396,
	BigInteger_IsProbablePrime_m4397,
	BigInteger_GeneratePseudoPrime_m4398,
	BigInteger_Incr2_m4399,
	BigInteger_op_Implicit_m4400,
	BigInteger_op_Implicit_m4401,
	BigInteger_op_Addition_m4402,
	BigInteger_op_Subtraction_m4403,
	BigInteger_op_Modulus_m4404,
	BigInteger_op_Modulus_m4405,
	BigInteger_op_Division_m4406,
	BigInteger_op_Multiply_m4407,
	BigInteger_op_Multiply_m4408,
	BigInteger_op_LeftShift_m4409,
	BigInteger_op_RightShift_m4410,
	BigInteger_op_Equality_m4411,
	BigInteger_op_Inequality_m4412,
	BigInteger_op_Equality_m4413,
	BigInteger_op_Inequality_m4414,
	BigInteger_op_GreaterThan_m4415,
	BigInteger_op_LessThan_m4416,
	BigInteger_op_GreaterThanOrEqual_m4417,
	BigInteger_op_LessThanOrEqual_m4418,
	CryptoConvert_ToInt32LE_m4419,
	CryptoConvert_ToUInt32LE_m4420,
	CryptoConvert_GetBytesLE_m4421,
	CryptoConvert_ToCapiPrivateKeyBlob_m4422,
	CryptoConvert_FromCapiPublicKeyBlob_m4423,
	CryptoConvert_FromCapiPublicKeyBlob_m4424,
	CryptoConvert_ToCapiPublicKeyBlob_m4425,
	CryptoConvert_ToCapiKeyBlob_m4426,
	KeyBuilder_get_Rng_m4427,
	KeyBuilder_Key_m4428,
	KeyBuilder_IV_m4429,
	BlockProcessor__ctor_m4430,
	BlockProcessor_Finalize_m4431,
	BlockProcessor_Initialize_m4432,
	BlockProcessor_Core_m4433,
	BlockProcessor_Core_m4434,
	BlockProcessor_Final_m4435,
	KeyGeneratedEventHandler__ctor_m4436,
	KeyGeneratedEventHandler_Invoke_m4437,
	KeyGeneratedEventHandler_BeginInvoke_m4438,
	KeyGeneratedEventHandler_EndInvoke_m4439,
	DSAManaged__ctor_m4440,
	DSAManaged_add_KeyGenerated_m4441,
	DSAManaged_remove_KeyGenerated_m4442,
	DSAManaged_Finalize_m4443,
	DSAManaged_Generate_m4444,
	DSAManaged_GenerateKeyPair_m4445,
	DSAManaged_add_m4446,
	DSAManaged_GenerateParams_m4447,
	DSAManaged_get_Random_m4448,
	DSAManaged_get_KeySize_m4449,
	DSAManaged_get_PublicOnly_m4450,
	DSAManaged_NormalizeArray_m4451,
	DSAManaged_ExportParameters_m4452,
	DSAManaged_ImportParameters_m4453,
	DSAManaged_CreateSignature_m4454,
	DSAManaged_VerifySignature_m4455,
	DSAManaged_Dispose_m4456,
	KeyPairPersistence__ctor_m4457,
	KeyPairPersistence__ctor_m4458,
	KeyPairPersistence__cctor_m4459,
	KeyPairPersistence_get_Filename_m4460,
	KeyPairPersistence_get_KeyValue_m4461,
	KeyPairPersistence_set_KeyValue_m4462,
	KeyPairPersistence_Load_m4463,
	KeyPairPersistence_Save_m4464,
	KeyPairPersistence_Remove_m4465,
	KeyPairPersistence_get_UserPath_m4466,
	KeyPairPersistence_get_MachinePath_m4467,
	KeyPairPersistence__CanSecure_m4468,
	KeyPairPersistence__ProtectUser_m4469,
	KeyPairPersistence__ProtectMachine_m4470,
	KeyPairPersistence__IsUserProtected_m4471,
	KeyPairPersistence__IsMachineProtected_m4472,
	KeyPairPersistence_CanSecure_m4473,
	KeyPairPersistence_ProtectUser_m4474,
	KeyPairPersistence_ProtectMachine_m4475,
	KeyPairPersistence_IsUserProtected_m4476,
	KeyPairPersistence_IsMachineProtected_m4477,
	KeyPairPersistence_get_CanChange_m4478,
	KeyPairPersistence_get_UseDefaultKeyContainer_m4479,
	KeyPairPersistence_get_UseMachineKeyStore_m4480,
	KeyPairPersistence_get_ContainerName_m4481,
	KeyPairPersistence_Copy_m4482,
	KeyPairPersistence_FromXml_m4483,
	KeyPairPersistence_ToXml_m4484,
	MACAlgorithm__ctor_m4485,
	MACAlgorithm_Initialize_m4486,
	MACAlgorithm_Core_m4487,
	MACAlgorithm_Final_m4488,
	PKCS1__cctor_m4489,
	PKCS1_Compare_m4490,
	PKCS1_I2OSP_m4491,
	PKCS1_OS2IP_m4492,
	PKCS1_RSAEP_m4493,
	PKCS1_RSASP1_m4494,
	PKCS1_RSAVP1_m4495,
	PKCS1_Encrypt_v15_m4496,
	PKCS1_Sign_v15_m4497,
	PKCS1_Verify_v15_m4498,
	PKCS1_Verify_v15_m4499,
	PKCS1_Encode_v15_m4500,
	PrivateKeyInfo__ctor_m4501,
	PrivateKeyInfo__ctor_m4502,
	PrivateKeyInfo_get_PrivateKey_m4503,
	PrivateKeyInfo_Decode_m4504,
	PrivateKeyInfo_RemoveLeadingZero_m4505,
	PrivateKeyInfo_Normalize_m4506,
	PrivateKeyInfo_DecodeRSA_m4507,
	PrivateKeyInfo_DecodeDSA_m4508,
	EncryptedPrivateKeyInfo__ctor_m4509,
	EncryptedPrivateKeyInfo__ctor_m4510,
	EncryptedPrivateKeyInfo_get_Algorithm_m4511,
	EncryptedPrivateKeyInfo_get_EncryptedData_m4512,
	EncryptedPrivateKeyInfo_get_Salt_m4513,
	EncryptedPrivateKeyInfo_get_IterationCount_m4514,
	EncryptedPrivateKeyInfo_Decode_m4515,
	KeyGeneratedEventHandler__ctor_m4516,
	KeyGeneratedEventHandler_Invoke_m4517,
	KeyGeneratedEventHandler_BeginInvoke_m4518,
	KeyGeneratedEventHandler_EndInvoke_m4519,
	RSAManaged__ctor_m4520,
	RSAManaged_add_KeyGenerated_m4521,
	RSAManaged_remove_KeyGenerated_m4522,
	RSAManaged_Finalize_m4523,
	RSAManaged_GenerateKeyPair_m4524,
	RSAManaged_get_KeySize_m4525,
	RSAManaged_get_PublicOnly_m4526,
	RSAManaged_DecryptValue_m4527,
	RSAManaged_EncryptValue_m4528,
	RSAManaged_ExportParameters_m4529,
	RSAManaged_ImportParameters_m4530,
	RSAManaged_Dispose_m4531,
	RSAManaged_ToXmlString_m4532,
	RSAManaged_get_IsCrtPossible_m4533,
	RSAManaged_GetPaddedValue_m4534,
	SymmetricTransform__ctor_m4535,
	SymmetricTransform_System_IDisposable_Dispose_m4536,
	SymmetricTransform_Finalize_m4537,
	SymmetricTransform_Dispose_m4538,
	SymmetricTransform_get_CanReuseTransform_m4539,
	SymmetricTransform_Transform_m4540,
	SymmetricTransform_CBC_m4541,
	SymmetricTransform_CFB_m4542,
	SymmetricTransform_OFB_m4543,
	SymmetricTransform_CTS_m4544,
	SymmetricTransform_CheckInput_m4545,
	SymmetricTransform_TransformBlock_m4546,
	SymmetricTransform_get_KeepLastBlock_m4547,
	SymmetricTransform_InternalTransformBlock_m4548,
	SymmetricTransform_Random_m4549,
	SymmetricTransform_ThrowBadPaddingException_m4550,
	SymmetricTransform_FinalEncrypt_m4551,
	SymmetricTransform_FinalDecrypt_m4552,
	SymmetricTransform_TransformFinalBlock_m4553,
	SafeBag__ctor_m4554,
	SafeBag_get_BagOID_m4555,
	SafeBag_get_ASN1_m4556,
	DeriveBytes__ctor_m4557,
	DeriveBytes__cctor_m4558,
	DeriveBytes_set_HashName_m4559,
	DeriveBytes_set_IterationCount_m4560,
	DeriveBytes_set_Password_m4561,
	DeriveBytes_set_Salt_m4562,
	DeriveBytes_Adjust_m4563,
	DeriveBytes_Derive_m4564,
	DeriveBytes_DeriveKey_m4565,
	DeriveBytes_DeriveIV_m4566,
	DeriveBytes_DeriveMAC_m4567,
	PKCS12__ctor_m4568,
	PKCS12__ctor_m4569,
	PKCS12__ctor_m4570,
	PKCS12__cctor_m4571,
	PKCS12_Decode_m4572,
	PKCS12_Finalize_m4573,
	PKCS12_set_Password_m4574,
	PKCS12_get_Certificates_m4575,
	PKCS12_Compare_m4576,
	PKCS12_GetSymmetricAlgorithm_m4577,
	PKCS12_Decrypt_m4578,
	PKCS12_Decrypt_m4579,
	PKCS12_GetExistingParameters_m4580,
	PKCS12_AddPrivateKey_m4581,
	PKCS12_ReadSafeBag_m4582,
	PKCS12_MAC_m4583,
	PKCS12_get_MaximumPasswordLength_m4584,
	X501__cctor_m4585,
	X501_ToString_m4586,
	X501_ToString_m4587,
	X501_AppendEntry_m4588,
	X509Certificate__ctor_m4589,
	X509Certificate__cctor_m4590,
	X509Certificate_Parse_m4591,
	X509Certificate_GetUnsignedBigInteger_m4592,
	X509Certificate_get_DSA_m4593,
	X509Certificate_get_IssuerName_m4594,
	X509Certificate_get_KeyAlgorithmParameters_m4595,
	X509Certificate_get_PublicKey_m4596,
	X509Certificate_get_RawData_m4597,
	X509Certificate_get_SubjectName_m4598,
	X509Certificate_get_ValidFrom_m4599,
	X509Certificate_get_ValidUntil_m4600,
	X509Certificate_GetIssuerName_m4601,
	X509Certificate_GetSubjectName_m4602,
	X509Certificate_GetObjectData_m4603,
	X509Certificate_PEM_m4604,
	X509CertificateEnumerator__ctor_m4605,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m4606,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m4607,
	X509CertificateEnumerator_get_Current_m4608,
	X509CertificateEnumerator_MoveNext_m4609,
	X509CertificateCollection__ctor_m4610,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m4611,
	X509CertificateCollection_get_Item_m4612,
	X509CertificateCollection_Add_m4613,
	X509CertificateCollection_GetEnumerator_m4614,
	X509CertificateCollection_GetHashCode_m4615,
	X509Extension__ctor_m4616,
	X509Extension_Decode_m4617,
	X509Extension_Equals_m4618,
	X509Extension_GetHashCode_m4619,
	X509Extension_WriteLine_m4620,
	X509Extension_ToString_m4621,
	X509ExtensionCollection__ctor_m4622,
	X509ExtensionCollection__ctor_m4623,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m4624,
	ASN1__ctor_m4625,
	ASN1__ctor_m4626,
	ASN1__ctor_m4627,
	ASN1_get_Count_m4628,
	ASN1_get_Tag_m4629,
	ASN1_get_Length_m4630,
	ASN1_get_Value_m4631,
	ASN1_set_Value_m4632,
	ASN1_CompareArray_m4633,
	ASN1_CompareValue_m4634,
	ASN1_Add_m4635,
	ASN1_GetBytes_m4636,
	ASN1_Decode_m4637,
	ASN1_DecodeTLV_m4638,
	ASN1_get_Item_m4639,
	ASN1_Element_m4640,
	ASN1_ToString_m4641,
	ASN1Convert_ToInt32_m4642,
	ASN1Convert_ToOid_m4643,
	ASN1Convert_ToDateTime_m4644,
	BitConverterLE_GetUIntBytes_m4645,
	BitConverterLE_GetBytes_m4646,
	BitConverterLE_UShortFromBytes_m4647,
	BitConverterLE_UIntFromBytes_m4648,
	BitConverterLE_ULongFromBytes_m4649,
	BitConverterLE_ToInt16_m4650,
	BitConverterLE_ToInt32_m4651,
	BitConverterLE_ToSingle_m4652,
	BitConverterLE_ToDouble_m4653,
	ContentInfo__ctor_m4654,
	ContentInfo__ctor_m4655,
	ContentInfo__ctor_m4656,
	ContentInfo__ctor_m4657,
	ContentInfo_get_Content_m4658,
	ContentInfo_set_Content_m4659,
	ContentInfo_get_ContentType_m4660,
	EncryptedData__ctor_m4661,
	EncryptedData__ctor_m4662,
	EncryptedData_get_EncryptionAlgorithm_m4663,
	EncryptedData_get_EncryptedContent_m4664,
	StrongName__cctor_m4665,
	StrongName_get_PublicKey_m4666,
	StrongName_get_PublicKeyToken_m4667,
	StrongName_get_TokenAlgorithm_m4668,
	SecurityParser__ctor_m4669,
	SecurityParser_LoadXml_m4670,
	SecurityParser_ToXml_m4671,
	SecurityParser_OnStartParsing_m4672,
	SecurityParser_OnProcessingInstruction_m4673,
	SecurityParser_OnIgnorableWhitespace_m4674,
	SecurityParser_OnStartElement_m4675,
	SecurityParser_OnEndElement_m4676,
	SecurityParser_OnChars_m4677,
	SecurityParser_OnEndParsing_m4678,
	AttrListImpl__ctor_m4679,
	AttrListImpl_get_Length_m4680,
	AttrListImpl_GetName_m4681,
	AttrListImpl_GetValue_m4682,
	AttrListImpl_GetValue_m4683,
	AttrListImpl_get_Names_m4684,
	AttrListImpl_get_Values_m4685,
	AttrListImpl_Clear_m4686,
	AttrListImpl_Add_m4687,
	SmallXmlParser__ctor_m4688,
	SmallXmlParser_Error_m4689,
	SmallXmlParser_UnexpectedEndError_m4690,
	SmallXmlParser_IsNameChar_m4691,
	SmallXmlParser_IsWhitespace_m4692,
	SmallXmlParser_SkipWhitespaces_m4693,
	SmallXmlParser_HandleWhitespaces_m4694,
	SmallXmlParser_SkipWhitespaces_m4695,
	SmallXmlParser_Peek_m4696,
	SmallXmlParser_Read_m4697,
	SmallXmlParser_Expect_m4698,
	SmallXmlParser_ReadUntil_m4699,
	SmallXmlParser_ReadName_m4700,
	SmallXmlParser_Parse_m4701,
	SmallXmlParser_Cleanup_m4702,
	SmallXmlParser_ReadContent_m4703,
	SmallXmlParser_HandleBufferedContent_m4704,
	SmallXmlParser_ReadCharacters_m4705,
	SmallXmlParser_ReadReference_m4706,
	SmallXmlParser_ReadCharacterReference_m4707,
	SmallXmlParser_ReadAttribute_m4708,
	SmallXmlParser_ReadCDATASection_m4709,
	SmallXmlParser_ReadComment_m4710,
	SmallXmlParserException__ctor_m4711,
	Runtime_GetDisplayName_m4712,
	KeyNotFoundException__ctor_m4713,
	KeyNotFoundException__ctor_m4714,
	SimpleEnumerator__ctor_m4715,
	SimpleEnumerator__cctor_m4716,
	SimpleEnumerator_MoveNext_m4717,
	SimpleEnumerator_get_Current_m4718,
	ArrayListWrapper__ctor_m4719,
	ArrayListWrapper_get_Item_m4720,
	ArrayListWrapper_set_Item_m4721,
	ArrayListWrapper_get_Count_m4722,
	ArrayListWrapper_get_Capacity_m4723,
	ArrayListWrapper_set_Capacity_m4724,
	ArrayListWrapper_get_IsReadOnly_m4725,
	ArrayListWrapper_get_IsSynchronized_m4726,
	ArrayListWrapper_get_SyncRoot_m4727,
	ArrayListWrapper_Add_m4728,
	ArrayListWrapper_Clear_m4729,
	ArrayListWrapper_Contains_m4730,
	ArrayListWrapper_IndexOf_m4731,
	ArrayListWrapper_IndexOf_m4732,
	ArrayListWrapper_IndexOf_m4733,
	ArrayListWrapper_Insert_m4734,
	ArrayListWrapper_InsertRange_m4735,
	ArrayListWrapper_Remove_m4736,
	ArrayListWrapper_RemoveAt_m4737,
	ArrayListWrapper_CopyTo_m4738,
	ArrayListWrapper_CopyTo_m4739,
	ArrayListWrapper_CopyTo_m4740,
	ArrayListWrapper_GetEnumerator_m4741,
	ArrayListWrapper_AddRange_m4742,
	ArrayListWrapper_Clone_m4743,
	ArrayListWrapper_Sort_m4744,
	ArrayListWrapper_Sort_m4745,
	ArrayListWrapper_ToArray_m4746,
	ArrayListWrapper_ToArray_m4747,
	SynchronizedArrayListWrapper__ctor_m4748,
	SynchronizedArrayListWrapper_get_Item_m4749,
	SynchronizedArrayListWrapper_set_Item_m4750,
	SynchronizedArrayListWrapper_get_Count_m4751,
	SynchronizedArrayListWrapper_get_Capacity_m4752,
	SynchronizedArrayListWrapper_set_Capacity_m4753,
	SynchronizedArrayListWrapper_get_IsReadOnly_m4754,
	SynchronizedArrayListWrapper_get_IsSynchronized_m4755,
	SynchronizedArrayListWrapper_get_SyncRoot_m4756,
	SynchronizedArrayListWrapper_Add_m4757,
	SynchronizedArrayListWrapper_Clear_m4758,
	SynchronizedArrayListWrapper_Contains_m4759,
	SynchronizedArrayListWrapper_IndexOf_m4760,
	SynchronizedArrayListWrapper_IndexOf_m4761,
	SynchronizedArrayListWrapper_IndexOf_m4762,
	SynchronizedArrayListWrapper_Insert_m4763,
	SynchronizedArrayListWrapper_InsertRange_m4764,
	SynchronizedArrayListWrapper_Remove_m4765,
	SynchronizedArrayListWrapper_RemoveAt_m4766,
	SynchronizedArrayListWrapper_CopyTo_m4767,
	SynchronizedArrayListWrapper_CopyTo_m4768,
	SynchronizedArrayListWrapper_CopyTo_m4769,
	SynchronizedArrayListWrapper_GetEnumerator_m4770,
	SynchronizedArrayListWrapper_AddRange_m4771,
	SynchronizedArrayListWrapper_Clone_m4772,
	SynchronizedArrayListWrapper_Sort_m4773,
	SynchronizedArrayListWrapper_Sort_m4774,
	SynchronizedArrayListWrapper_ToArray_m4775,
	SynchronizedArrayListWrapper_ToArray_m4776,
	FixedSizeArrayListWrapper__ctor_m4777,
	FixedSizeArrayListWrapper_get_ErrorMessage_m4778,
	FixedSizeArrayListWrapper_get_Capacity_m4779,
	FixedSizeArrayListWrapper_set_Capacity_m4780,
	FixedSizeArrayListWrapper_Add_m4781,
	FixedSizeArrayListWrapper_AddRange_m4782,
	FixedSizeArrayListWrapper_Clear_m4783,
	FixedSizeArrayListWrapper_Insert_m4784,
	FixedSizeArrayListWrapper_InsertRange_m4785,
	FixedSizeArrayListWrapper_Remove_m4786,
	FixedSizeArrayListWrapper_RemoveAt_m4787,
	ReadOnlyArrayListWrapper__ctor_m4788,
	ReadOnlyArrayListWrapper_get_ErrorMessage_m4789,
	ReadOnlyArrayListWrapper_get_IsReadOnly_m4790,
	ReadOnlyArrayListWrapper_get_Item_m4791,
	ReadOnlyArrayListWrapper_set_Item_m4792,
	ReadOnlyArrayListWrapper_Sort_m4793,
	ReadOnlyArrayListWrapper_Sort_m4794,
	ArrayList__ctor_m2242,
	ArrayList__ctor_m2275,
	ArrayList__ctor_m2359,
	ArrayList__ctor_m4795,
	ArrayList__cctor_m4796,
	ArrayList_get_Item_m4797,
	ArrayList_set_Item_m4798,
	ArrayList_get_Count_m4799,
	ArrayList_get_Capacity_m4800,
	ArrayList_set_Capacity_m4801,
	ArrayList_get_IsReadOnly_m4802,
	ArrayList_get_IsSynchronized_m4803,
	ArrayList_get_SyncRoot_m4804,
	ArrayList_EnsureCapacity_m4805,
	ArrayList_Shift_m4806,
	ArrayList_Add_m4807,
	ArrayList_Clear_m4808,
	ArrayList_Contains_m4809,
	ArrayList_IndexOf_m4810,
	ArrayList_IndexOf_m4811,
	ArrayList_IndexOf_m4812,
	ArrayList_Insert_m4813,
	ArrayList_InsertRange_m4814,
	ArrayList_Remove_m4815,
	ArrayList_RemoveAt_m4816,
	ArrayList_CopyTo_m4817,
	ArrayList_CopyTo_m4818,
	ArrayList_CopyTo_m4819,
	ArrayList_GetEnumerator_m4820,
	ArrayList_AddRange_m4821,
	ArrayList_Sort_m4822,
	ArrayList_Sort_m4823,
	ArrayList_ToArray_m4824,
	ArrayList_ToArray_m4825,
	ArrayList_Clone_m4826,
	ArrayList_ThrowNewArgumentOutOfRangeException_m4827,
	ArrayList_Synchronized_m4828,
	ArrayList_ReadOnly_m3308,
	BitArrayEnumerator__ctor_m4829,
	BitArrayEnumerator_get_Current_m4830,
	BitArrayEnumerator_MoveNext_m4831,
	BitArrayEnumerator_checkVersion_m4832,
	BitArray__ctor_m2395,
	BitArray_getByte_m4833,
	BitArray_get_Count_m4834,
	BitArray_get_Item_m2388,
	BitArray_set_Item_m2396,
	BitArray_get_Length_m2387,
	BitArray_get_SyncRoot_m4835,
	BitArray_CopyTo_m4836,
	BitArray_Get_m4837,
	BitArray_Set_m4838,
	BitArray_GetEnumerator_m4839,
	CaseInsensitiveComparer__ctor_m4840,
	CaseInsensitiveComparer__ctor_m4841,
	CaseInsensitiveComparer__cctor_m4842,
	CaseInsensitiveComparer_get_DefaultInvariant_m2228,
	CaseInsensitiveComparer_Compare_m4843,
	CaseInsensitiveHashCodeProvider__ctor_m4844,
	CaseInsensitiveHashCodeProvider__ctor_m4845,
	CaseInsensitiveHashCodeProvider__cctor_m4846,
	CaseInsensitiveHashCodeProvider_AreEqual_m4847,
	CaseInsensitiveHashCodeProvider_AreEqual_m4848,
	CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m2229,
	CaseInsensitiveHashCodeProvider_GetHashCode_m4849,
	CollectionBase__ctor_m2341,
	CollectionBase_System_Collections_ICollection_CopyTo_m4850,
	CollectionBase_System_Collections_ICollection_get_SyncRoot_m4851,
	CollectionBase_System_Collections_IList_Add_m4852,
	CollectionBase_System_Collections_IList_Contains_m4853,
	CollectionBase_System_Collections_IList_IndexOf_m4854,
	CollectionBase_System_Collections_IList_Insert_m4855,
	CollectionBase_System_Collections_IList_Remove_m4856,
	CollectionBase_System_Collections_IList_get_Item_m4857,
	CollectionBase_System_Collections_IList_set_Item_m4858,
	CollectionBase_get_Count_m4859,
	CollectionBase_GetEnumerator_m4860,
	CollectionBase_Clear_m4861,
	CollectionBase_RemoveAt_m4862,
	CollectionBase_get_InnerList_m2336,
	CollectionBase_get_List_m2393,
	CollectionBase_OnClear_m4863,
	CollectionBase_OnClearComplete_m4864,
	CollectionBase_OnInsert_m4865,
	CollectionBase_OnInsertComplete_m4866,
	CollectionBase_OnRemove_m4867,
	CollectionBase_OnRemoveComplete_m4868,
	CollectionBase_OnSet_m4869,
	CollectionBase_OnSetComplete_m4870,
	CollectionBase_OnValidate_m4871,
	Comparer__ctor_m4872,
	Comparer__ctor_m4873,
	Comparer__cctor_m4874,
	Comparer_Compare_m4875,
	Comparer_GetObjectData_m4876,
	DictionaryEntry__ctor_m2233,
	DictionaryEntry_get_Key_m4877,
	DictionaryEntry_get_Value_m4878,
	KeyMarker__ctor_m4879,
	KeyMarker__cctor_m4880,
	Enumerator__ctor_m4881,
	Enumerator__cctor_m4882,
	Enumerator_FailFast_m4883,
	Enumerator_Reset_m4884,
	Enumerator_MoveNext_m4885,
	Enumerator_get_Entry_m4886,
	Enumerator_get_Key_m4887,
	Enumerator_get_Value_m4888,
	Enumerator_get_Current_m4889,
	HashKeys__ctor_m4890,
	HashKeys_get_Count_m4891,
	HashKeys_get_SyncRoot_m4892,
	HashKeys_CopyTo_m4893,
	HashKeys_GetEnumerator_m4894,
	HashValues__ctor_m4895,
	HashValues_get_Count_m4896,
	HashValues_get_SyncRoot_m4897,
	HashValues_CopyTo_m4898,
	HashValues_GetEnumerator_m4899,
	Hashtable__ctor_m2378,
	Hashtable__ctor_m4900,
	Hashtable__ctor_m4901,
	Hashtable__ctor_m2380,
	Hashtable__ctor_m4902,
	Hashtable__ctor_m2230,
	Hashtable__ctor_m4903,
	Hashtable__ctor_m2231,
	Hashtable__ctor_m2272,
	Hashtable__ctor_m4904,
	Hashtable__ctor_m2241,
	Hashtable__ctor_m4905,
	Hashtable__cctor_m4906,
	Hashtable_System_Collections_IEnumerable_GetEnumerator_m4907,
	Hashtable_set_comparer_m4908,
	Hashtable_set_hcp_m4909,
	Hashtable_get_Count_m4910,
	Hashtable_get_SyncRoot_m4911,
	Hashtable_get_Keys_m4912,
	Hashtable_get_Values_m4913,
	Hashtable_get_Item_m4914,
	Hashtable_set_Item_m4915,
	Hashtable_CopyTo_m4916,
	Hashtable_Add_m4917,
	Hashtable_Clear_m4918,
	Hashtable_Contains_m4919,
	Hashtable_GetEnumerator_m4920,
	Hashtable_Remove_m4921,
	Hashtable_ContainsKey_m4922,
	Hashtable_Clone_m4923,
	Hashtable_GetObjectData_m4924,
	Hashtable_OnDeserialization_m4925,
	Hashtable_GetHash_m4926,
	Hashtable_KeyEquals_m4927,
	Hashtable_AdjustThreshold_m4928,
	Hashtable_SetTable_m4929,
	Hashtable_Find_m4930,
	Hashtable_Rehash_m4931,
	Hashtable_PutImpl_m4932,
	Hashtable_CopyToArray_m4933,
	Hashtable_TestPrime_m4934,
	Hashtable_CalcPrime_m4935,
	Hashtable_ToPrime_m4936,
	Enumerator__ctor_m4937,
	Enumerator__cctor_m4938,
	Enumerator_Reset_m4939,
	Enumerator_MoveNext_m4940,
	Enumerator_get_Entry_m4941,
	Enumerator_get_Key_m4942,
	Enumerator_get_Value_m4943,
	Enumerator_get_Current_m4944,
	SortedList__ctor_m4945,
	SortedList__ctor_m2270,
	SortedList__ctor_m4946,
	SortedList__cctor_m4947,
	SortedList_System_Collections_IEnumerable_GetEnumerator_m4948,
	SortedList_get_Count_m4949,
	SortedList_get_SyncRoot_m4950,
	SortedList_get_IsFixedSize_m4951,
	SortedList_get_IsReadOnly_m4952,
	SortedList_get_Item_m4953,
	SortedList_set_Item_m4954,
	SortedList_get_Capacity_m4955,
	SortedList_set_Capacity_m4956,
	SortedList_Add_m4957,
	SortedList_Contains_m4958,
	SortedList_GetEnumerator_m4959,
	SortedList_Remove_m4960,
	SortedList_CopyTo_m4961,
	SortedList_RemoveAt_m4962,
	SortedList_IndexOfKey_m4963,
	SortedList_ContainsKey_m4964,
	SortedList_GetByIndex_m4965,
	SortedList_EnsureCapacity_m4966,
	SortedList_PutImpl_m4967,
	SortedList_GetImpl_m4968,
	SortedList_InitTable_m4969,
	SortedList_Find_m4970,
	Enumerator__ctor_m4971,
	Enumerator_get_Current_m4972,
	Enumerator_MoveNext_m4973,
	Stack__ctor_m1336,
	Stack_Resize_m4974,
	Stack_get_Count_m4975,
	Stack_get_SyncRoot_m4976,
	Stack_Clear_m4977,
	Stack_CopyTo_m4978,
	Stack_GetEnumerator_m4979,
	Stack_Peek_m4980,
	Stack_Pop_m4981,
	Stack_Push_m4982,
	SuppressMessageAttribute__ctor_m4983,
	SuppressMessageAttribute_set_Justification_m4984,
	DebuggableAttribute__ctor_m4985,
	DebuggerDisplayAttribute__ctor_m4986,
	DebuggerDisplayAttribute_set_Name_m4987,
	DebuggerStepThroughAttribute__ctor_m4988,
	DebuggerTypeProxyAttribute__ctor_m4989,
	StackFrame__ctor_m4990,
	StackFrame__ctor_m4991,
	StackFrame_get_frame_info_m4992,
	StackFrame_GetFileLineNumber_m4993,
	StackFrame_GetFileName_m4994,
	StackFrame_GetSecureFileName_m4995,
	StackFrame_GetILOffset_m4996,
	StackFrame_GetMethod_m4997,
	StackFrame_GetNativeOffset_m4998,
	StackFrame_GetInternalMethodName_m4999,
	StackFrame_ToString_m5000,
	StackTrace__ctor_m5001,
	StackTrace__ctor_m1315,
	StackTrace__ctor_m5002,
	StackTrace__ctor_m5003,
	StackTrace__ctor_m5004,
	StackTrace_init_frames_m5005,
	StackTrace_get_trace_m5006,
	StackTrace_get_FrameCount_m5007,
	StackTrace_GetFrame_m5008,
	StackTrace_ToString_m5009,
	Calendar__ctor_m5010,
	Calendar_CheckReadOnly_m5011,
	Calendar_get_EraNames_m5012,
	CCMath_div_m5013,
	CCMath_mod_m5014,
	CCMath_div_mod_m5015,
	CCFixed_FromDateTime_m5016,
	CCFixed_day_of_week_m5017,
	CCGregorianCalendar_is_leap_year_m5018,
	CCGregorianCalendar_fixed_from_dmy_m5019,
	CCGregorianCalendar_year_from_fixed_m5020,
	CCGregorianCalendar_my_from_fixed_m5021,
	CCGregorianCalendar_dmy_from_fixed_m5022,
	CCGregorianCalendar_month_from_fixed_m5023,
	CCGregorianCalendar_day_from_fixed_m5024,
	CCGregorianCalendar_GetDayOfMonth_m5025,
	CCGregorianCalendar_GetMonth_m5026,
	CCGregorianCalendar_GetYear_m5027,
	CompareInfo__ctor_m5028,
	CompareInfo__ctor_m5029,
	CompareInfo__cctor_m5030,
	CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m5031,
	CompareInfo_get_UseManagedCollation_m5032,
	CompareInfo_construct_compareinfo_m5033,
	CompareInfo_free_internal_collator_m5034,
	CompareInfo_internal_compare_m5035,
	CompareInfo_assign_sortkey_m5036,
	CompareInfo_internal_index_m5037,
	CompareInfo_Finalize_m5038,
	CompareInfo_internal_compare_managed_m5039,
	CompareInfo_internal_compare_switch_m5040,
	CompareInfo_Compare_m5041,
	CompareInfo_Compare_m5042,
	CompareInfo_Compare_m5043,
	CompareInfo_Equals_m5044,
	CompareInfo_GetHashCode_m5045,
	CompareInfo_GetSortKey_m5046,
	CompareInfo_IndexOf_m5047,
	CompareInfo_internal_index_managed_m5048,
	CompareInfo_internal_index_switch_m5049,
	CompareInfo_IndexOf_m5050,
	CompareInfo_IsPrefix_m5051,
	CompareInfo_IsSuffix_m5052,
	CompareInfo_LastIndexOf_m5053,
	CompareInfo_LastIndexOf_m5054,
	CompareInfo_ToString_m5055,
	CompareInfo_get_LCID_m5056,
	CultureInfo__ctor_m5057,
	CultureInfo__ctor_m5058,
	CultureInfo__ctor_m5059,
	CultureInfo__ctor_m5060,
	CultureInfo__ctor_m5061,
	CultureInfo__cctor_m5062,
	CultureInfo_get_InvariantCulture_m1272,
	CultureInfo_get_CurrentCulture_m3337,
	CultureInfo_get_CurrentUICulture_m3338,
	CultureInfo_ConstructCurrentCulture_m5063,
	CultureInfo_ConstructCurrentUICulture_m5064,
	CultureInfo_get_LCID_m5065,
	CultureInfo_get_Name_m5066,
	CultureInfo_get_Parent_m5067,
	CultureInfo_get_TextInfo_m5068,
	CultureInfo_get_IcuName_m5069,
	CultureInfo_Equals_m5070,
	CultureInfo_GetHashCode_m5071,
	CultureInfo_ToString_m5072,
	CultureInfo_get_CompareInfo_m5073,
	CultureInfo_get_IsNeutralCulture_m5074,
	CultureInfo_CheckNeutral_m5075,
	CultureInfo_get_NumberFormat_m5076,
	CultureInfo_get_DateTimeFormat_m5077,
	CultureInfo_get_IsReadOnly_m5078,
	CultureInfo_GetFormat_m5079,
	CultureInfo_Construct_m5080,
	CultureInfo_ConstructInternalLocaleFromName_m5081,
	CultureInfo_ConstructInternalLocaleFromLcid_m5082,
	CultureInfo_ConstructInternalLocaleFromCurrentLocale_m5083,
	CultureInfo_construct_internal_locale_from_lcid_m5084,
	CultureInfo_construct_internal_locale_from_name_m5085,
	CultureInfo_construct_internal_locale_from_current_locale_m5086,
	CultureInfo_construct_datetime_format_m5087,
	CultureInfo_construct_number_format_m5088,
	CultureInfo_ConstructInvariant_m5089,
	CultureInfo_CreateTextInfo_m5090,
	CultureInfo_CreateCulture_m5091,
	DateTimeFormatInfo__ctor_m5092,
	DateTimeFormatInfo__ctor_m5093,
	DateTimeFormatInfo__cctor_m5094,
	DateTimeFormatInfo_GetInstance_m5095,
	DateTimeFormatInfo_get_IsReadOnly_m5096,
	DateTimeFormatInfo_ReadOnly_m5097,
	DateTimeFormatInfo_Clone_m5098,
	DateTimeFormatInfo_GetFormat_m5099,
	DateTimeFormatInfo_GetAbbreviatedMonthName_m5100,
	DateTimeFormatInfo_GetEraName_m5101,
	DateTimeFormatInfo_GetMonthName_m5102,
	DateTimeFormatInfo_get_RawAbbreviatedDayNames_m5103,
	DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m5104,
	DateTimeFormatInfo_get_RawDayNames_m5105,
	DateTimeFormatInfo_get_RawMonthNames_m5106,
	DateTimeFormatInfo_get_AMDesignator_m5107,
	DateTimeFormatInfo_get_PMDesignator_m5108,
	DateTimeFormatInfo_get_DateSeparator_m5109,
	DateTimeFormatInfo_get_TimeSeparator_m5110,
	DateTimeFormatInfo_get_LongDatePattern_m5111,
	DateTimeFormatInfo_get_ShortDatePattern_m5112,
	DateTimeFormatInfo_get_ShortTimePattern_m5113,
	DateTimeFormatInfo_get_LongTimePattern_m5114,
	DateTimeFormatInfo_get_MonthDayPattern_m5115,
	DateTimeFormatInfo_get_YearMonthPattern_m5116,
	DateTimeFormatInfo_get_FullDateTimePattern_m5117,
	DateTimeFormatInfo_get_CurrentInfo_m5118,
	DateTimeFormatInfo_get_InvariantInfo_m5119,
	DateTimeFormatInfo_get_Calendar_m5120,
	DateTimeFormatInfo_set_Calendar_m5121,
	DateTimeFormatInfo_get_RFC1123Pattern_m5122,
	DateTimeFormatInfo_get_RoundtripPattern_m5123,
	DateTimeFormatInfo_get_SortableDateTimePattern_m5124,
	DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m5125,
	DateTimeFormatInfo_GetAllDateTimePatternsInternal_m5126,
	DateTimeFormatInfo_FillAllDateTimePatterns_m5127,
	DateTimeFormatInfo_GetAllRawDateTimePatterns_m5128,
	DateTimeFormatInfo_GetDayName_m5129,
	DateTimeFormatInfo_GetAbbreviatedDayName_m5130,
	DateTimeFormatInfo_FillInvariantPatterns_m5131,
	DateTimeFormatInfo_PopulateCombinedList_m5132,
	DaylightTime__ctor_m5133,
	DaylightTime_get_Start_m5134,
	DaylightTime_get_End_m5135,
	DaylightTime_get_Delta_m5136,
	GregorianCalendar__ctor_m5137,
	GregorianCalendar__ctor_m5138,
	GregorianCalendar_get_Eras_m5139,
	GregorianCalendar_set_CalendarType_m5140,
	GregorianCalendar_GetDayOfMonth_m5141,
	GregorianCalendar_GetDayOfWeek_m5142,
	GregorianCalendar_GetEra_m5143,
	GregorianCalendar_GetMonth_m5144,
	GregorianCalendar_GetYear_m5145,
	NumberFormatInfo__ctor_m5146,
	NumberFormatInfo__ctor_m5147,
	NumberFormatInfo__ctor_m5148,
	NumberFormatInfo__cctor_m5149,
	NumberFormatInfo_get_CurrencyDecimalDigits_m5150,
	NumberFormatInfo_get_CurrencyDecimalSeparator_m5151,
	NumberFormatInfo_get_CurrencyGroupSeparator_m5152,
	NumberFormatInfo_get_RawCurrencyGroupSizes_m5153,
	NumberFormatInfo_get_CurrencyNegativePattern_m5154,
	NumberFormatInfo_get_CurrencyPositivePattern_m5155,
	NumberFormatInfo_get_CurrencySymbol_m5156,
	NumberFormatInfo_get_CurrentInfo_m5157,
	NumberFormatInfo_get_InvariantInfo_m5158,
	NumberFormatInfo_get_NaNSymbol_m5159,
	NumberFormatInfo_get_NegativeInfinitySymbol_m5160,
	NumberFormatInfo_get_NegativeSign_m5161,
	NumberFormatInfo_get_NumberDecimalDigits_m5162,
	NumberFormatInfo_get_NumberDecimalSeparator_m5163,
	NumberFormatInfo_get_NumberGroupSeparator_m5164,
	NumberFormatInfo_get_RawNumberGroupSizes_m5165,
	NumberFormatInfo_get_NumberNegativePattern_m5166,
	NumberFormatInfo_set_NumberNegativePattern_m5167,
	NumberFormatInfo_get_PercentDecimalDigits_m5168,
	NumberFormatInfo_get_PercentDecimalSeparator_m5169,
	NumberFormatInfo_get_PercentGroupSeparator_m5170,
	NumberFormatInfo_get_RawPercentGroupSizes_m5171,
	NumberFormatInfo_get_PercentNegativePattern_m5172,
	NumberFormatInfo_get_PercentPositivePattern_m5173,
	NumberFormatInfo_get_PercentSymbol_m5174,
	NumberFormatInfo_get_PerMilleSymbol_m5175,
	NumberFormatInfo_get_PositiveInfinitySymbol_m5176,
	NumberFormatInfo_get_PositiveSign_m5177,
	NumberFormatInfo_GetFormat_m5178,
	NumberFormatInfo_Clone_m5179,
	NumberFormatInfo_GetInstance_m5180,
	TextInfo__ctor_m5181,
	TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m5182,
	TextInfo_get_CultureName_m5183,
	TextInfo_Equals_m5184,
	TextInfo_GetHashCode_m5185,
	TextInfo_ToString_m5186,
	TextInfo_ToLower_m5187,
	TextInfo_ToUpper_m5188,
	TextInfo_ToLower_m5189,
	TextInfo_ToUpper_m5190,
	IsolatedStorageException__ctor_m5191,
	IsolatedStorageException__ctor_m5192,
	IsolatedStorageException__ctor_m5193,
	BinaryReader__ctor_m5194,
	BinaryReader__ctor_m5195,
	BinaryReader_System_IDisposable_Dispose_m5196,
	BinaryReader_Dispose_m5197,
	BinaryReader_FillBuffer_m5198,
	BinaryReader_Read_m5199,
	BinaryReader_Read_m5200,
	BinaryReader_Read_m5201,
	BinaryReader_ReadCharBytes_m5202,
	BinaryReader_Read7BitEncodedInt_m5203,
	BinaryReader_ReadBoolean_m5204,
	BinaryReader_ReadByte_m5205,
	BinaryReader_ReadChar_m5206,
	BinaryReader_ReadDecimal_m5207,
	BinaryReader_ReadDouble_m5208,
	BinaryReader_ReadInt16_m5209,
	BinaryReader_ReadInt32_m5210,
	BinaryReader_ReadInt64_m5211,
	BinaryReader_ReadSByte_m5212,
	BinaryReader_ReadString_m5213,
	BinaryReader_ReadSingle_m5214,
	BinaryReader_ReadUInt16_m5215,
	BinaryReader_ReadUInt32_m5216,
	BinaryReader_ReadUInt64_m5217,
	BinaryReader_CheckBuffer_m5218,
	Directory_CreateDirectory_m3324,
	Directory_CreateDirectoriesInternal_m5219,
	Directory_Exists_m3323,
	Directory_GetCurrentDirectory_m5220,
	Directory_GetFiles_m3326,
	Directory_GetFileSystemEntries_m5221,
	DirectoryInfo__ctor_m5222,
	DirectoryInfo__ctor_m5223,
	DirectoryInfo__ctor_m5224,
	DirectoryInfo_Initialize_m5225,
	DirectoryInfo_get_Exists_m5226,
	DirectoryInfo_get_Parent_m5227,
	DirectoryInfo_Create_m5228,
	DirectoryInfo_ToString_m5229,
	DirectoryNotFoundException__ctor_m5230,
	DirectoryNotFoundException__ctor_m5231,
	DirectoryNotFoundException__ctor_m5232,
	EndOfStreamException__ctor_m5233,
	EndOfStreamException__ctor_m5234,
	File_Delete_m5235,
	File_Exists_m5236,
	File_Open_m5237,
	File_OpenRead_m3322,
	File_OpenText_m5238,
	FileNotFoundException__ctor_m5239,
	FileNotFoundException__ctor_m5240,
	FileNotFoundException__ctor_m5241,
	FileNotFoundException_get_Message_m5242,
	FileNotFoundException_GetObjectData_m5243,
	FileNotFoundException_ToString_m5244,
	ReadDelegate__ctor_m5245,
	ReadDelegate_Invoke_m5246,
	ReadDelegate_BeginInvoke_m5247,
	ReadDelegate_EndInvoke_m5248,
	WriteDelegate__ctor_m5249,
	WriteDelegate_Invoke_m5250,
	WriteDelegate_BeginInvoke_m5251,
	WriteDelegate_EndInvoke_m5252,
	FileStream__ctor_m5253,
	FileStream__ctor_m5254,
	FileStream__ctor_m5255,
	FileStream__ctor_m5256,
	FileStream__ctor_m5257,
	FileStream_get_CanRead_m5258,
	FileStream_get_CanWrite_m5259,
	FileStream_get_CanSeek_m5260,
	FileStream_get_Length_m5261,
	FileStream_get_Position_m5262,
	FileStream_set_Position_m5263,
	FileStream_ReadByte_m5264,
	FileStream_WriteByte_m5265,
	FileStream_Read_m5266,
	FileStream_ReadInternal_m5267,
	FileStream_BeginRead_m5268,
	FileStream_EndRead_m5269,
	FileStream_Write_m5270,
	FileStream_WriteInternal_m5271,
	FileStream_BeginWrite_m5272,
	FileStream_EndWrite_m5273,
	FileStream_Seek_m5274,
	FileStream_SetLength_m5275,
	FileStream_Flush_m5276,
	FileStream_Finalize_m5277,
	FileStream_Dispose_m5278,
	FileStream_ReadSegment_m5279,
	FileStream_WriteSegment_m5280,
	FileStream_FlushBuffer_m5281,
	FileStream_FlushBuffer_m5282,
	FileStream_FlushBufferIfDirty_m5283,
	FileStream_RefillBuffer_m5284,
	FileStream_ReadData_m5285,
	FileStream_InitBuffer_m5286,
	FileStream_GetSecureFileName_m5287,
	FileStream_GetSecureFileName_m5288,
	FileStreamAsyncResult__ctor_m5289,
	FileStreamAsyncResult_CBWrapper_m5290,
	FileStreamAsyncResult_get_AsyncState_m5291,
	FileStreamAsyncResult_get_AsyncWaitHandle_m5292,
	FileStreamAsyncResult_get_IsCompleted_m5293,
	FileSystemInfo__ctor_m5294,
	FileSystemInfo__ctor_m5295,
	FileSystemInfo_GetObjectData_m5296,
	FileSystemInfo_get_FullName_m5297,
	FileSystemInfo_Refresh_m5298,
	FileSystemInfo_InternalRefresh_m5299,
	FileSystemInfo_CheckPath_m5300,
	IOException__ctor_m5301,
	IOException__ctor_m5302,
	IOException__ctor_m3351,
	IOException__ctor_m5303,
	IOException__ctor_m5304,
	MemoryStream__ctor_m3352,
	MemoryStream__ctor_m1223,
	MemoryStream__ctor_m3357,
	MemoryStream_InternalConstructor_m5305,
	MemoryStream_CheckIfClosedThrowDisposed_m5306,
	MemoryStream_get_CanRead_m5307,
	MemoryStream_get_CanSeek_m5308,
	MemoryStream_get_CanWrite_m5309,
	MemoryStream_set_Capacity_m5310,
	MemoryStream_get_Length_m5311,
	MemoryStream_get_Position_m5312,
	MemoryStream_set_Position_m5313,
	MemoryStream_Dispose_m5314,
	MemoryStream_Flush_m5315,
	MemoryStream_Read_m5316,
	MemoryStream_ReadByte_m5317,
	MemoryStream_Seek_m5318,
	MemoryStream_CalculateNewCapacity_m5319,
	MemoryStream_Expand_m5320,
	MemoryStream_SetLength_m5321,
	MemoryStream_ToArray_m5322,
	MemoryStream_Write_m5323,
	MemoryStream_WriteByte_m5324,
	MonoIO__cctor_m5325,
	MonoIO_GetException_m5326,
	MonoIO_GetException_m5327,
	MonoIO_CreateDirectory_m5328,
	MonoIO_GetFileSystemEntries_m5329,
	MonoIO_GetCurrentDirectory_m5330,
	MonoIO_DeleteFile_m5331,
	MonoIO_GetFileAttributes_m5332,
	MonoIO_GetFileType_m5333,
	MonoIO_ExistsFile_m5334,
	MonoIO_ExistsDirectory_m5335,
	MonoIO_GetFileStat_m5336,
	MonoIO_Open_m5337,
	MonoIO_Close_m5338,
	MonoIO_Read_m5339,
	MonoIO_Write_m5340,
	MonoIO_Seek_m5341,
	MonoIO_GetLength_m5342,
	MonoIO_SetLength_m5343,
	MonoIO_get_ConsoleOutput_m5344,
	MonoIO_get_ConsoleInput_m5345,
	MonoIO_get_ConsoleError_m5346,
	MonoIO_get_VolumeSeparatorChar_m5347,
	MonoIO_get_DirectorySeparatorChar_m5348,
	MonoIO_get_AltDirectorySeparatorChar_m5349,
	MonoIO_get_PathSeparator_m5350,
	Path__cctor_m5351,
	Path_Combine_m3325,
	Path_CleanPath_m5352,
	Path_GetDirectoryName_m5353,
	Path_GetFileName_m5354,
	Path_GetFullPath_m5355,
	Path_WindowsDriveAdjustment_m5356,
	Path_InsecureGetFullPath_m5357,
	Path_IsDsc_m5358,
	Path_GetPathRoot_m5359,
	Path_IsPathRooted_m5360,
	Path_GetInvalidPathChars_m5361,
	Path_GetServerAndShare_m5362,
	Path_SameRoot_m5363,
	Path_CanonicalizePath_m5364,
	PathTooLongException__ctor_m5365,
	PathTooLongException__ctor_m5366,
	PathTooLongException__ctor_m5367,
	SearchPattern__cctor_m5368,
	Stream__ctor_m3353,
	Stream__cctor_m5369,
	Stream_Dispose_m5370,
	Stream_Dispose_m3356,
	Stream_Close_m3355,
	Stream_ReadByte_m5371,
	Stream_WriteByte_m5372,
	Stream_BeginRead_m5373,
	Stream_BeginWrite_m5374,
	Stream_EndRead_m5375,
	Stream_EndWrite_m5376,
	NullStream__ctor_m5377,
	NullStream_get_CanRead_m5378,
	NullStream_get_CanSeek_m5379,
	NullStream_get_CanWrite_m5380,
	NullStream_get_Length_m5381,
	NullStream_get_Position_m5382,
	NullStream_set_Position_m5383,
	NullStream_Flush_m5384,
	NullStream_Read_m5385,
	NullStream_ReadByte_m5386,
	NullStream_Seek_m5387,
	NullStream_SetLength_m5388,
	NullStream_Write_m5389,
	NullStream_WriteByte_m5390,
	StreamAsyncResult__ctor_m5391,
	StreamAsyncResult_SetComplete_m5392,
	StreamAsyncResult_SetComplete_m5393,
	StreamAsyncResult_get_AsyncState_m5394,
	StreamAsyncResult_get_AsyncWaitHandle_m5395,
	StreamAsyncResult_get_IsCompleted_m5396,
	StreamAsyncResult_get_Exception_m5397,
	StreamAsyncResult_get_NBytes_m5398,
	StreamAsyncResult_get_Done_m5399,
	StreamAsyncResult_set_Done_m5400,
	NullStreamReader__ctor_m5401,
	NullStreamReader_Peek_m5402,
	NullStreamReader_Read_m5403,
	NullStreamReader_Read_m5404,
	NullStreamReader_ReadLine_m5405,
	NullStreamReader_ReadToEnd_m5406,
	StreamReader__ctor_m5407,
	StreamReader__ctor_m5408,
	StreamReader__ctor_m5409,
	StreamReader__ctor_m5410,
	StreamReader__ctor_m5411,
	StreamReader__cctor_m5412,
	StreamReader_Initialize_m5413,
	StreamReader_Dispose_m5414,
	StreamReader_DoChecks_m5415,
	StreamReader_ReadBuffer_m5416,
	StreamReader_Peek_m5417,
	StreamReader_Read_m5418,
	StreamReader_Read_m5419,
	StreamReader_FindNextEOL_m5420,
	StreamReader_ReadLine_m5421,
	StreamReader_ReadToEnd_m5422,
	StreamWriter__ctor_m5423,
	StreamWriter__ctor_m5424,
	StreamWriter__cctor_m5425,
	StreamWriter_Initialize_m5426,
	StreamWriter_set_AutoFlush_m5427,
	StreamWriter_Dispose_m5428,
	StreamWriter_Flush_m5429,
	StreamWriter_FlushBytes_m5430,
	StreamWriter_Decode_m5431,
	StreamWriter_Write_m5432,
	StreamWriter_LowLevelWrite_m5433,
	StreamWriter_LowLevelWrite_m5434,
	StreamWriter_Write_m5435,
	StreamWriter_Write_m5436,
	StreamWriter_Write_m5437,
	StreamWriter_Close_m5438,
	StreamWriter_Finalize_m5439,
	StringReader__ctor_m1216,
	StringReader_Dispose_m5440,
	StringReader_Peek_m5441,
	StringReader_Read_m5442,
	StringReader_Read_m5443,
	StringReader_ReadLine_m5444,
	StringReader_ReadToEnd_m5445,
	StringReader_CheckObjectDisposedException_m5446,
	NullTextReader__ctor_m5447,
	NullTextReader_ReadLine_m5448,
	TextReader__ctor_m5449,
	TextReader__cctor_m5450,
	TextReader_Dispose_m5451,
	TextReader_Dispose_m5452,
	TextReader_Peek_m5453,
	TextReader_Read_m5454,
	TextReader_Read_m5455,
	TextReader_ReadLine_m5456,
	TextReader_ReadToEnd_m5457,
	TextReader_Synchronized_m5458,
	SynchronizedReader__ctor_m5459,
	SynchronizedReader_Peek_m5460,
	SynchronizedReader_ReadLine_m5461,
	SynchronizedReader_ReadToEnd_m5462,
	SynchronizedReader_Read_m5463,
	SynchronizedReader_Read_m5464,
	NullTextWriter__ctor_m5465,
	NullTextWriter_Write_m5466,
	NullTextWriter_Write_m5467,
	NullTextWriter_Write_m5468,
	TextWriter__ctor_m5469,
	TextWriter__cctor_m5470,
	TextWriter_Close_m5471,
	TextWriter_Dispose_m5472,
	TextWriter_Dispose_m5473,
	TextWriter_Flush_m5474,
	TextWriter_Synchronized_m5475,
	TextWriter_Write_m5476,
	TextWriter_Write_m5477,
	TextWriter_Write_m5478,
	TextWriter_Write_m5479,
	TextWriter_WriteLine_m5480,
	TextWriter_WriteLine_m5481,
	SynchronizedWriter__ctor_m5482,
	SynchronizedWriter_Close_m5483,
	SynchronizedWriter_Flush_m5484,
	SynchronizedWriter_Write_m5485,
	SynchronizedWriter_Write_m5486,
	SynchronizedWriter_Write_m5487,
	SynchronizedWriter_Write_m5488,
	SynchronizedWriter_WriteLine_m5489,
	SynchronizedWriter_WriteLine_m5490,
	UnexceptionalStreamReader__ctor_m5491,
	UnexceptionalStreamReader__cctor_m5492,
	UnexceptionalStreamReader_Peek_m5493,
	UnexceptionalStreamReader_Read_m5494,
	UnexceptionalStreamReader_Read_m5495,
	UnexceptionalStreamReader_CheckEOL_m5496,
	UnexceptionalStreamReader_ReadLine_m5497,
	UnexceptionalStreamReader_ReadToEnd_m5498,
	UnexceptionalStreamWriter__ctor_m5499,
	UnexceptionalStreamWriter_Flush_m5500,
	UnexceptionalStreamWriter_Write_m5501,
	UnexceptionalStreamWriter_Write_m5502,
	UnexceptionalStreamWriter_Write_m5503,
	UnexceptionalStreamWriter_Write_m5504,
	AssemblyBuilder_get_Location_m5505,
	AssemblyBuilder_GetModulesInternal_m5506,
	AssemblyBuilder_GetTypes_m5507,
	AssemblyBuilder_get_IsCompilerContext_m5508,
	AssemblyBuilder_not_supported_m5509,
	AssemblyBuilder_UnprotectedGetName_m5510,
	ConstructorBuilder__ctor_m5511,
	ConstructorBuilder_get_CallingConvention_m5512,
	ConstructorBuilder_get_TypeBuilder_m5513,
	ConstructorBuilder_GetParameters_m5514,
	ConstructorBuilder_GetParametersInternal_m5515,
	ConstructorBuilder_GetParameterCount_m5516,
	ConstructorBuilder_Invoke_m5517,
	ConstructorBuilder_Invoke_m5518,
	ConstructorBuilder_get_MethodHandle_m5519,
	ConstructorBuilder_get_Attributes_m5520,
	ConstructorBuilder_get_ReflectedType_m5521,
	ConstructorBuilder_get_DeclaringType_m5522,
	ConstructorBuilder_get_Name_m5523,
	ConstructorBuilder_IsDefined_m5524,
	ConstructorBuilder_GetCustomAttributes_m5525,
	ConstructorBuilder_GetCustomAttributes_m5526,
	ConstructorBuilder_GetILGenerator_m5527,
	ConstructorBuilder_GetILGenerator_m5528,
	ConstructorBuilder_GetToken_m5529,
	ConstructorBuilder_get_Module_m5530,
	ConstructorBuilder_ToString_m5531,
	ConstructorBuilder_fixup_m5532,
	ConstructorBuilder_get_next_table_index_m5533,
	ConstructorBuilder_get_IsCompilerContext_m5534,
	ConstructorBuilder_not_supported_m5535,
	ConstructorBuilder_not_created_m5536,
	EnumBuilder_get_Assembly_m5537,
	EnumBuilder_get_AssemblyQualifiedName_m5538,
	EnumBuilder_get_BaseType_m5539,
	EnumBuilder_get_DeclaringType_m5540,
	EnumBuilder_get_FullName_m5541,
	EnumBuilder_get_Module_m5542,
	EnumBuilder_get_Name_m5543,
	EnumBuilder_get_Namespace_m5544,
	EnumBuilder_get_ReflectedType_m5545,
	EnumBuilder_get_TypeHandle_m5546,
	EnumBuilder_get_UnderlyingSystemType_m5547,
	EnumBuilder_GetAttributeFlagsImpl_m5548,
	EnumBuilder_GetConstructorImpl_m5549,
	EnumBuilder_GetConstructors_m5550,
	EnumBuilder_GetCustomAttributes_m5551,
	EnumBuilder_GetCustomAttributes_m5552,
	EnumBuilder_GetElementType_m5553,
	EnumBuilder_GetEvent_m5554,
	EnumBuilder_GetField_m5555,
	EnumBuilder_GetFields_m5556,
	EnumBuilder_GetInterfaces_m5557,
	EnumBuilder_GetMethodImpl_m5558,
	EnumBuilder_GetMethods_m5559,
	EnumBuilder_GetProperties_m5560,
	EnumBuilder_GetPropertyImpl_m5561,
	EnumBuilder_HasElementTypeImpl_m5562,
	EnumBuilder_InvokeMember_m5563,
	EnumBuilder_IsArrayImpl_m5564,
	EnumBuilder_IsByRefImpl_m5565,
	EnumBuilder_IsPointerImpl_m5566,
	EnumBuilder_IsPrimitiveImpl_m5567,
	EnumBuilder_IsValueTypeImpl_m5568,
	EnumBuilder_IsDefined_m5569,
	EnumBuilder_CreateNotSupportedException_m5570,
	FieldBuilder_get_Attributes_m5571,
	FieldBuilder_get_DeclaringType_m5572,
	FieldBuilder_get_FieldHandle_m5573,
	FieldBuilder_get_FieldType_m5574,
	FieldBuilder_get_Name_m5575,
	FieldBuilder_get_ReflectedType_m5576,
	FieldBuilder_GetCustomAttributes_m5577,
	FieldBuilder_GetCustomAttributes_m5578,
	FieldBuilder_GetValue_m5579,
	FieldBuilder_IsDefined_m5580,
	FieldBuilder_GetFieldOffset_m5581,
	FieldBuilder_SetValue_m5582,
	FieldBuilder_get_UMarshal_m5583,
	FieldBuilder_CreateNotSupportedException_m5584,
	FieldBuilder_get_Module_m5585,
	GenericTypeParameterBuilder_IsSubclassOf_m5586,
	GenericTypeParameterBuilder_GetAttributeFlagsImpl_m5587,
	GenericTypeParameterBuilder_GetConstructorImpl_m5588,
	GenericTypeParameterBuilder_GetConstructors_m5589,
	GenericTypeParameterBuilder_GetEvent_m5590,
	GenericTypeParameterBuilder_GetField_m5591,
	GenericTypeParameterBuilder_GetFields_m5592,
	GenericTypeParameterBuilder_GetInterfaces_m5593,
	GenericTypeParameterBuilder_GetMethods_m5594,
	GenericTypeParameterBuilder_GetMethodImpl_m5595,
	GenericTypeParameterBuilder_GetProperties_m5596,
	GenericTypeParameterBuilder_GetPropertyImpl_m5597,
	GenericTypeParameterBuilder_HasElementTypeImpl_m5598,
	GenericTypeParameterBuilder_IsAssignableFrom_m5599,
	GenericTypeParameterBuilder_IsInstanceOfType_m5600,
	GenericTypeParameterBuilder_IsArrayImpl_m5601,
	GenericTypeParameterBuilder_IsByRefImpl_m5602,
	GenericTypeParameterBuilder_IsPointerImpl_m5603,
	GenericTypeParameterBuilder_IsPrimitiveImpl_m5604,
	GenericTypeParameterBuilder_IsValueTypeImpl_m5605,
	GenericTypeParameterBuilder_InvokeMember_m5606,
	GenericTypeParameterBuilder_GetElementType_m5607,
	GenericTypeParameterBuilder_get_UnderlyingSystemType_m5608,
	GenericTypeParameterBuilder_get_Assembly_m5609,
	GenericTypeParameterBuilder_get_AssemblyQualifiedName_m5610,
	GenericTypeParameterBuilder_get_BaseType_m5611,
	GenericTypeParameterBuilder_get_FullName_m5612,
	GenericTypeParameterBuilder_IsDefined_m5613,
	GenericTypeParameterBuilder_GetCustomAttributes_m5614,
	GenericTypeParameterBuilder_GetCustomAttributes_m5615,
	GenericTypeParameterBuilder_get_Name_m5616,
	GenericTypeParameterBuilder_get_Namespace_m5617,
	GenericTypeParameterBuilder_get_Module_m5618,
	GenericTypeParameterBuilder_get_DeclaringType_m5619,
	GenericTypeParameterBuilder_get_ReflectedType_m5620,
	GenericTypeParameterBuilder_get_TypeHandle_m5621,
	GenericTypeParameterBuilder_GetGenericArguments_m5622,
	GenericTypeParameterBuilder_GetGenericTypeDefinition_m5623,
	GenericTypeParameterBuilder_get_ContainsGenericParameters_m5624,
	GenericTypeParameterBuilder_get_IsGenericParameter_m5625,
	GenericTypeParameterBuilder_get_IsGenericType_m5626,
	GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m5627,
	GenericTypeParameterBuilder_not_supported_m5628,
	GenericTypeParameterBuilder_ToString_m5629,
	GenericTypeParameterBuilder_Equals_m5630,
	GenericTypeParameterBuilder_GetHashCode_m5631,
	GenericTypeParameterBuilder_MakeGenericType_m5632,
	ILGenerator__ctor_m5633,
	ILGenerator__cctor_m5634,
	ILGenerator_add_token_fixup_m5635,
	ILGenerator_make_room_m5636,
	ILGenerator_emit_int_m5637,
	ILGenerator_ll_emit_m5638,
	ILGenerator_Emit_m5639,
	ILGenerator_Emit_m5640,
	ILGenerator_label_fixup_m5641,
	ILGenerator_Mono_GetCurrentOffset_m5642,
	MethodBuilder_get_ContainsGenericParameters_m5643,
	MethodBuilder_get_MethodHandle_m5644,
	MethodBuilder_get_ReturnType_m5645,
	MethodBuilder_get_ReflectedType_m5646,
	MethodBuilder_get_DeclaringType_m5647,
	MethodBuilder_get_Name_m5648,
	MethodBuilder_get_Attributes_m5649,
	MethodBuilder_get_CallingConvention_m5650,
	MethodBuilder_GetBaseDefinition_m5651,
	MethodBuilder_GetParameters_m5652,
	MethodBuilder_GetParameterCount_m5653,
	MethodBuilder_Invoke_m5654,
	MethodBuilder_IsDefined_m5655,
	MethodBuilder_GetCustomAttributes_m5656,
	MethodBuilder_GetCustomAttributes_m5657,
	MethodBuilder_check_override_m5658,
	MethodBuilder_fixup_m5659,
	MethodBuilder_ToString_m5660,
	MethodBuilder_Equals_m5661,
	MethodBuilder_GetHashCode_m5662,
	MethodBuilder_get_next_table_index_m5663,
	MethodBuilder_NotSupported_m5664,
	MethodBuilder_MakeGenericMethod_m5665,
	MethodBuilder_get_IsGenericMethodDefinition_m5666,
	MethodBuilder_get_IsGenericMethod_m5667,
	MethodBuilder_GetGenericArguments_m5668,
	MethodBuilder_get_Module_m5669,
	MethodToken__ctor_m5670,
	MethodToken__cctor_m5671,
	MethodToken_Equals_m5672,
	MethodToken_GetHashCode_m5673,
	MethodToken_get_Token_m5674,
	ModuleBuilder__cctor_m5675,
	ModuleBuilder_get_next_table_index_m5676,
	ModuleBuilder_GetTypes_m5677,
	ModuleBuilder_getToken_m5678,
	ModuleBuilder_GetToken_m5679,
	ModuleBuilder_RegisterToken_m5680,
	ModuleBuilder_GetTokenGenerator_m5681,
	ModuleBuilderTokenGenerator__ctor_m5682,
	ModuleBuilderTokenGenerator_GetToken_m5683,
	OpCode__ctor_m5684,
	OpCode_GetHashCode_m5685,
	OpCode_Equals_m5686,
	OpCode_ToString_m5687,
	OpCode_get_Name_m5688,
	OpCode_get_Size_m5689,
	OpCode_get_StackBehaviourPop_m5690,
	OpCode_get_StackBehaviourPush_m5691,
	OpCodeNames__cctor_m5692,
	OpCodes__cctor_m5693,
	ParameterBuilder_get_Attributes_m5694,
	ParameterBuilder_get_Name_m5695,
	ParameterBuilder_get_Position_m5696,
	PropertyBuilder_get_Attributes_m5697,
	PropertyBuilder_get_CanRead_m5698,
	PropertyBuilder_get_CanWrite_m5699,
	PropertyBuilder_get_DeclaringType_m5700,
	PropertyBuilder_get_Name_m5701,
	PropertyBuilder_get_PropertyType_m5702,
	PropertyBuilder_get_ReflectedType_m5703,
	PropertyBuilder_GetAccessors_m5704,
	PropertyBuilder_GetCustomAttributes_m5705,
	PropertyBuilder_GetCustomAttributes_m5706,
	PropertyBuilder_GetGetMethod_m5707,
	PropertyBuilder_GetIndexParameters_m5708,
	PropertyBuilder_GetSetMethod_m5709,
	PropertyBuilder_GetValue_m5710,
	PropertyBuilder_GetValue_m5711,
	PropertyBuilder_IsDefined_m5712,
	PropertyBuilder_SetValue_m5713,
	PropertyBuilder_SetValue_m5714,
	PropertyBuilder_get_Module_m5715,
	PropertyBuilder_not_supported_m5716,
	TypeBuilder_GetAttributeFlagsImpl_m5717,
	TypeBuilder_setup_internal_class_m5718,
	TypeBuilder_create_generic_class_m5719,
	TypeBuilder_get_Assembly_m5720,
	TypeBuilder_get_AssemblyQualifiedName_m5721,
	TypeBuilder_get_BaseType_m5722,
	TypeBuilder_get_DeclaringType_m5723,
	TypeBuilder_get_UnderlyingSystemType_m5724,
	TypeBuilder_get_FullName_m5725,
	TypeBuilder_get_Module_m5726,
	TypeBuilder_get_Name_m5727,
	TypeBuilder_get_Namespace_m5728,
	TypeBuilder_get_ReflectedType_m5729,
	TypeBuilder_GetConstructorImpl_m5730,
	TypeBuilder_IsDefined_m5731,
	TypeBuilder_GetCustomAttributes_m5732,
	TypeBuilder_GetCustomAttributes_m5733,
	TypeBuilder_DefineConstructor_m5734,
	TypeBuilder_DefineConstructor_m5735,
	TypeBuilder_DefineDefaultConstructor_m5736,
	TypeBuilder_create_runtime_class_m5737,
	TypeBuilder_is_nested_in_m5738,
	TypeBuilder_has_ctor_method_m5739,
	TypeBuilder_CreateType_m5740,
	TypeBuilder_GetConstructors_m5741,
	TypeBuilder_GetConstructorsInternal_m5742,
	TypeBuilder_GetElementType_m5743,
	TypeBuilder_GetEvent_m5744,
	TypeBuilder_GetField_m5745,
	TypeBuilder_GetFields_m5746,
	TypeBuilder_GetInterfaces_m5747,
	TypeBuilder_GetMethodsByName_m5748,
	TypeBuilder_GetMethods_m5749,
	TypeBuilder_GetMethodImpl_m5750,
	TypeBuilder_GetProperties_m5751,
	TypeBuilder_GetPropertyImpl_m5752,
	TypeBuilder_HasElementTypeImpl_m5753,
	TypeBuilder_InvokeMember_m5754,
	TypeBuilder_IsArrayImpl_m5755,
	TypeBuilder_IsByRefImpl_m5756,
	TypeBuilder_IsPointerImpl_m5757,
	TypeBuilder_IsPrimitiveImpl_m5758,
	TypeBuilder_IsValueTypeImpl_m5759,
	TypeBuilder_MakeGenericType_m5760,
	TypeBuilder_get_TypeHandle_m5761,
	TypeBuilder_SetParent_m5762,
	TypeBuilder_get_next_table_index_m5763,
	TypeBuilder_get_IsCompilerContext_m5764,
	TypeBuilder_get_is_created_m5765,
	TypeBuilder_not_supported_m5766,
	TypeBuilder_check_not_created_m5767,
	TypeBuilder_check_created_m5768,
	TypeBuilder_ToString_m5769,
	TypeBuilder_IsAssignableFrom_m5770,
	TypeBuilder_IsSubclassOf_m5771,
	TypeBuilder_IsAssignableTo_m5772,
	TypeBuilder_GetGenericArguments_m5773,
	TypeBuilder_GetGenericTypeDefinition_m5774,
	TypeBuilder_get_ContainsGenericParameters_m5775,
	TypeBuilder_get_IsGenericParameter_m5776,
	TypeBuilder_get_IsGenericTypeDefinition_m5777,
	TypeBuilder_get_IsGenericType_m5778,
	UnmanagedMarshal_ToMarshalAsAttribute_m5779,
	AmbiguousMatchException__ctor_m5780,
	AmbiguousMatchException__ctor_m5781,
	AmbiguousMatchException__ctor_m5782,
	ResolveEventHolder__ctor_m5783,
	Assembly__ctor_m5784,
	Assembly_get_code_base_m5785,
	Assembly_get_fullname_m5786,
	Assembly_get_location_m5787,
	Assembly_GetCodeBase_m5788,
	Assembly_get_FullName_m5789,
	Assembly_get_Location_m5790,
	Assembly_IsDefined_m5791,
	Assembly_GetCustomAttributes_m5792,
	Assembly_GetManifestResourceInternal_m5793,
	Assembly_GetTypes_m5794,
	Assembly_GetTypes_m5795,
	Assembly_GetType_m5796,
	Assembly_GetType_m5797,
	Assembly_InternalGetType_m5798,
	Assembly_GetType_m5799,
	Assembly_FillName_m5800,
	Assembly_GetName_m5801,
	Assembly_GetName_m5802,
	Assembly_UnprotectedGetName_m5803,
	Assembly_ToString_m5804,
	Assembly_Load_m5805,
	Assembly_GetModule_m5806,
	Assembly_GetModulesInternal_m5807,
	Assembly_GetModules_m5808,
	Assembly_GetExecutingAssembly_m5809,
	AssemblyCompanyAttribute__ctor_m5810,
	AssemblyCopyrightAttribute__ctor_m5811,
	AssemblyDefaultAliasAttribute__ctor_m5812,
	AssemblyDelaySignAttribute__ctor_m5813,
	AssemblyDescriptionAttribute__ctor_m5814,
	AssemblyFileVersionAttribute__ctor_m5815,
	AssemblyInformationalVersionAttribute__ctor_m5816,
	AssemblyKeyFileAttribute__ctor_m5817,
	AssemblyName__ctor_m5818,
	AssemblyName__ctor_m5819,
	AssemblyName_get_Name_m5820,
	AssemblyName_get_Flags_m5821,
	AssemblyName_get_FullName_m5822,
	AssemblyName_get_Version_m5823,
	AssemblyName_set_Version_m5824,
	AssemblyName_ToString_m5825,
	AssemblyName_get_IsPublicKeyValid_m5826,
	AssemblyName_InternalGetPublicKeyToken_m5827,
	AssemblyName_ComputePublicKeyToken_m5828,
	AssemblyName_SetPublicKey_m5829,
	AssemblyName_SetPublicKeyToken_m5830,
	AssemblyName_GetObjectData_m5831,
	AssemblyName_OnDeserialization_m5832,
	AssemblyProductAttribute__ctor_m5833,
	AssemblyTitleAttribute__ctor_m5834,
	Default__ctor_m5835,
	Default_BindToMethod_m5836,
	Default_ReorderParameters_m5837,
	Default_IsArrayAssignable_m5838,
	Default_ChangeType_m5839,
	Default_ReorderArgumentArray_m5840,
	Default_check_type_m5841,
	Default_check_arguments_m5842,
	Default_SelectMethod_m5843,
	Default_SelectMethod_m5844,
	Default_GetBetterMethod_m5845,
	Default_CompareCloserType_m5846,
	Default_SelectProperty_m5847,
	Default_check_arguments_with_score_m5848,
	Default_check_type_with_score_m5849,
	Binder__ctor_m5850,
	Binder__cctor_m5851,
	Binder_get_DefaultBinder_m5852,
	Binder_ConvertArgs_m5853,
	Binder_GetDerivedLevel_m5854,
	Binder_FindMostDerivedMatch_m5855,
	ConstructorInfo__ctor_m5856,
	ConstructorInfo__cctor_m5857,
	ConstructorInfo_get_MemberType_m5858,
	ConstructorInfo_Invoke_m1305,
	AddEventAdapter__ctor_m5859,
	AddEventAdapter_Invoke_m5860,
	AddEventAdapter_BeginInvoke_m5861,
	AddEventAdapter_EndInvoke_m5862,
	EventInfo__ctor_m5863,
	EventInfo_get_EventHandlerType_m5864,
	EventInfo_get_MemberType_m5865,
	FieldInfo__ctor_m5866,
	FieldInfo_get_MemberType_m5867,
	FieldInfo_get_IsLiteral_m5868,
	FieldInfo_get_IsStatic_m5869,
	FieldInfo_get_IsInitOnly_m5870,
	FieldInfo_get_IsPublic_m5871,
	FieldInfo_get_IsNotSerialized_m5872,
	FieldInfo_SetValue_m5873,
	FieldInfo_internal_from_handle_type_m5874,
	FieldInfo_GetFieldFromHandle_m5875,
	FieldInfo_GetFieldOffset_m5876,
	FieldInfo_GetUnmanagedMarshal_m5877,
	FieldInfo_get_UMarshal_m5878,
	FieldInfo_GetPseudoCustomAttributes_m5879,
	MemberInfoSerializationHolder__ctor_m5880,
	MemberInfoSerializationHolder_Serialize_m5881,
	MemberInfoSerializationHolder_Serialize_m5882,
	MemberInfoSerializationHolder_GetObjectData_m5883,
	MemberInfoSerializationHolder_GetRealObject_m5884,
	MethodBase__ctor_m5885,
	MethodBase_GetMethodFromHandleNoGenericCheck_m5886,
	MethodBase_GetMethodFromIntPtr_m5887,
	MethodBase_GetMethodFromHandle_m5888,
	MethodBase_GetMethodFromHandleInternalType_m5889,
	MethodBase_GetParameterCount_m5890,
	MethodBase_Invoke_m5891,
	MethodBase_get_CallingConvention_m5892,
	MethodBase_get_IsPublic_m5893,
	MethodBase_get_IsStatic_m5894,
	MethodBase_get_IsVirtual_m5895,
	MethodBase_get_IsAbstract_m5896,
	MethodBase_get_next_table_index_m5897,
	MethodBase_GetGenericArguments_m5898,
	MethodBase_get_ContainsGenericParameters_m5899,
	MethodBase_get_IsGenericMethodDefinition_m5900,
	MethodBase_get_IsGenericMethod_m5901,
	MethodInfo__ctor_m5902,
	MethodInfo_get_MemberType_m5903,
	MethodInfo_get_ReturnType_m5904,
	MethodInfo_MakeGenericMethod_m5905,
	MethodInfo_GetGenericArguments_m5906,
	MethodInfo_get_IsGenericMethod_m5907,
	MethodInfo_get_IsGenericMethodDefinition_m5908,
	MethodInfo_get_ContainsGenericParameters_m5909,
	Missing__ctor_m5910,
	Missing__cctor_m5911,
	Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m5912,
	Module__ctor_m5913,
	Module__cctor_m5914,
	Module_get_Assembly_m5915,
	Module_get_ScopeName_m5916,
	Module_GetCustomAttributes_m5917,
	Module_GetObjectData_m5918,
	Module_InternalGetTypes_m5919,
	Module_GetTypes_m5920,
	Module_IsDefined_m5921,
	Module_IsResource_m5922,
	Module_ToString_m5923,
	Module_filter_by_type_name_m5924,
	Module_filter_by_type_name_ignore_case_m5925,
	MonoEventInfo_get_event_info_m5926,
	MonoEventInfo_GetEventInfo_m5927,
	MonoEvent__ctor_m5928,
	MonoEvent_get_Attributes_m5929,
	MonoEvent_GetAddMethod_m5930,
	MonoEvent_get_DeclaringType_m5931,
	MonoEvent_get_ReflectedType_m5932,
	MonoEvent_get_Name_m5933,
	MonoEvent_ToString_m5934,
	MonoEvent_IsDefined_m5935,
	MonoEvent_GetCustomAttributes_m5936,
	MonoEvent_GetCustomAttributes_m5937,
	MonoEvent_GetObjectData_m5938,
	MonoField__ctor_m5939,
	MonoField_get_Attributes_m5940,
	MonoField_get_FieldHandle_m5941,
	MonoField_get_FieldType_m5942,
	MonoField_GetParentType_m5943,
	MonoField_get_ReflectedType_m5944,
	MonoField_get_DeclaringType_m5945,
	MonoField_get_Name_m5946,
	MonoField_IsDefined_m5947,
	MonoField_GetCustomAttributes_m5948,
	MonoField_GetCustomAttributes_m5949,
	MonoField_GetFieldOffset_m5950,
	MonoField_GetValueInternal_m5951,
	MonoField_GetValue_m5952,
	MonoField_ToString_m5953,
	MonoField_SetValueInternal_m5954,
	MonoField_SetValue_m5955,
	MonoField_GetObjectData_m5956,
	MonoField_CheckGeneric_m5957,
	MonoGenericMethod__ctor_m5958,
	MonoGenericMethod_get_ReflectedType_m5959,
	MonoGenericCMethod__ctor_m5960,
	MonoGenericCMethod_get_ReflectedType_m5961,
	MonoMethodInfo_get_method_info_m5962,
	MonoMethodInfo_GetMethodInfo_m5963,
	MonoMethodInfo_GetDeclaringType_m5964,
	MonoMethodInfo_GetReturnType_m5965,
	MonoMethodInfo_GetAttributes_m5966,
	MonoMethodInfo_GetCallingConvention_m5967,
	MonoMethodInfo_get_parameter_info_m5968,
	MonoMethodInfo_GetParametersInfo_m5969,
	MonoMethod__ctor_m5970,
	MonoMethod_get_name_m5971,
	MonoMethod_get_base_definition_m5972,
	MonoMethod_GetBaseDefinition_m5973,
	MonoMethod_get_ReturnType_m5974,
	MonoMethod_GetParameters_m5975,
	MonoMethod_InternalInvoke_m5976,
	MonoMethod_Invoke_m5977,
	MonoMethod_get_MethodHandle_m5978,
	MonoMethod_get_Attributes_m5979,
	MonoMethod_get_CallingConvention_m5980,
	MonoMethod_get_ReflectedType_m5981,
	MonoMethod_get_DeclaringType_m5982,
	MonoMethod_get_Name_m5983,
	MonoMethod_IsDefined_m5984,
	MonoMethod_GetCustomAttributes_m5985,
	MonoMethod_GetCustomAttributes_m5986,
	MonoMethod_GetDllImportAttribute_m5987,
	MonoMethod_GetPseudoCustomAttributes_m5988,
	MonoMethod_ShouldPrintFullName_m5989,
	MonoMethod_ToString_m5990,
	MonoMethod_GetObjectData_m5991,
	MonoMethod_MakeGenericMethod_m5992,
	MonoMethod_MakeGenericMethod_impl_m5993,
	MonoMethod_GetGenericArguments_m5994,
	MonoMethod_get_IsGenericMethodDefinition_m5995,
	MonoMethod_get_IsGenericMethod_m5996,
	MonoMethod_get_ContainsGenericParameters_m5997,
	MonoCMethod__ctor_m5998,
	MonoCMethod_GetParameters_m5999,
	MonoCMethod_InternalInvoke_m6000,
	MonoCMethod_Invoke_m6001,
	MonoCMethod_Invoke_m6002,
	MonoCMethod_get_MethodHandle_m6003,
	MonoCMethod_get_Attributes_m6004,
	MonoCMethod_get_CallingConvention_m6005,
	MonoCMethod_get_ReflectedType_m6006,
	MonoCMethod_get_DeclaringType_m6007,
	MonoCMethod_get_Name_m6008,
	MonoCMethod_IsDefined_m6009,
	MonoCMethod_GetCustomAttributes_m6010,
	MonoCMethod_GetCustomAttributes_m6011,
	MonoCMethod_ToString_m6012,
	MonoCMethod_GetObjectData_m6013,
	MonoPropertyInfo_get_property_info_m6014,
	MonoPropertyInfo_GetTypeModifiers_m6015,
	GetterAdapter__ctor_m6016,
	GetterAdapter_Invoke_m6017,
	GetterAdapter_BeginInvoke_m6018,
	GetterAdapter_EndInvoke_m6019,
	MonoProperty__ctor_m6020,
	MonoProperty_CachePropertyInfo_m6021,
	MonoProperty_get_Attributes_m6022,
	MonoProperty_get_CanRead_m6023,
	MonoProperty_get_CanWrite_m6024,
	MonoProperty_get_PropertyType_m6025,
	MonoProperty_get_ReflectedType_m6026,
	MonoProperty_get_DeclaringType_m6027,
	MonoProperty_get_Name_m6028,
	MonoProperty_GetAccessors_m6029,
	MonoProperty_GetGetMethod_m6030,
	MonoProperty_GetIndexParameters_m6031,
	MonoProperty_GetSetMethod_m6032,
	MonoProperty_IsDefined_m6033,
	MonoProperty_GetCustomAttributes_m6034,
	MonoProperty_GetCustomAttributes_m6035,
	MonoProperty_CreateGetterDelegate_m6036,
	MonoProperty_GetValue_m6037,
	MonoProperty_GetValue_m6038,
	MonoProperty_SetValue_m6039,
	MonoProperty_ToString_m6040,
	MonoProperty_GetOptionalCustomModifiers_m6041,
	MonoProperty_GetRequiredCustomModifiers_m6042,
	MonoProperty_GetObjectData_m6043,
	ParameterInfo__ctor_m6044,
	ParameterInfo__ctor_m6045,
	ParameterInfo__ctor_m6046,
	ParameterInfo_ToString_m6047,
	ParameterInfo_get_ParameterType_m6048,
	ParameterInfo_get_Attributes_m6049,
	ParameterInfo_get_IsIn_m6050,
	ParameterInfo_get_IsOptional_m6051,
	ParameterInfo_get_IsOut_m6052,
	ParameterInfo_get_IsRetval_m6053,
	ParameterInfo_get_Member_m6054,
	ParameterInfo_get_Name_m6055,
	ParameterInfo_get_Position_m6056,
	ParameterInfo_GetCustomAttributes_m6057,
	ParameterInfo_IsDefined_m6058,
	ParameterInfo_GetPseudoCustomAttributes_m6059,
	Pointer__ctor_m6060,
	Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m6061,
	PropertyInfo__ctor_m6062,
	PropertyInfo_get_MemberType_m6063,
	PropertyInfo_GetValue_m6064,
	PropertyInfo_SetValue_m6065,
	PropertyInfo_GetOptionalCustomModifiers_m6066,
	PropertyInfo_GetRequiredCustomModifiers_m6067,
	StrongNameKeyPair__ctor_m6068,
	StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m6069,
	StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6070,
	TargetException__ctor_m6071,
	TargetException__ctor_m6072,
	TargetException__ctor_m6073,
	TargetInvocationException__ctor_m6074,
	TargetInvocationException__ctor_m6075,
	TargetParameterCountException__ctor_m6076,
	TargetParameterCountException__ctor_m6077,
	TargetParameterCountException__ctor_m6078,
	NeutralResourcesLanguageAttribute__ctor_m6079,
	SatelliteContractVersionAttribute__ctor_m6080,
	CompilationRelaxationsAttribute__ctor_m6081,
	DefaultDependencyAttribute__ctor_m6082,
	StringFreezingAttribute__ctor_m6083,
	CriticalFinalizerObject__ctor_m6084,
	CriticalFinalizerObject_Finalize_m6085,
	ReliabilityContractAttribute__ctor_m6086,
	ClassInterfaceAttribute__ctor_m6087,
	ComDefaultInterfaceAttribute__ctor_m6088,
	DispIdAttribute__ctor_m6089,
	GCHandle__ctor_m6090,
	GCHandle_get_IsAllocated_m6091,
	GCHandle_get_Target_m6092,
	GCHandle_Alloc_m6093,
	GCHandle_Free_m6094,
	GCHandle_GetTarget_m6095,
	GCHandle_GetTargetHandle_m6096,
	GCHandle_FreeHandle_m6097,
	GCHandle_Equals_m6098,
	GCHandle_GetHashCode_m6099,
	InterfaceTypeAttribute__ctor_m6100,
	Marshal__cctor_m6101,
	Marshal_copy_from_unmanaged_m6102,
	Marshal_Copy_m6103,
	Marshal_Copy_m6104,
	MarshalDirectiveException__ctor_m6105,
	MarshalDirectiveException__ctor_m6106,
	PreserveSigAttribute__ctor_m6107,
	SafeHandle__ctor_m6108,
	SafeHandle_Close_m6109,
	SafeHandle_DangerousAddRef_m6110,
	SafeHandle_DangerousGetHandle_m6111,
	SafeHandle_DangerousRelease_m6112,
	SafeHandle_Dispose_m6113,
	SafeHandle_Dispose_m6114,
	SafeHandle_SetHandle_m6115,
	SafeHandle_Finalize_m6116,
	TypeLibImportClassAttribute__ctor_m6117,
	TypeLibVersionAttribute__ctor_m6118,
	ActivationServices_get_ConstructionActivator_m6119,
	ActivationServices_CreateProxyFromAttributes_m6120,
	ActivationServices_CreateConstructionCall_m6121,
	ActivationServices_AllocateUninitializedClassInstance_m6122,
	ActivationServices_EnableProxyActivation_m6123,
	AppDomainLevelActivator__ctor_m6124,
	ConstructionLevelActivator__ctor_m6125,
	ContextLevelActivator__ctor_m6126,
	UrlAttribute_get_UrlValue_m6127,
	UrlAttribute_Equals_m6128,
	UrlAttribute_GetHashCode_m6129,
	UrlAttribute_GetPropertiesForNewContext_m6130,
	UrlAttribute_IsContextOK_m6131,
	ChannelInfo__ctor_m6132,
	ChannelInfo_get_ChannelData_m6133,
	ChannelServices__cctor_m6134,
	ChannelServices_CreateClientChannelSinkChain_m6135,
	ChannelServices_CreateClientChannelSinkChain_m6136,
	ChannelServices_RegisterChannel_m6137,
	ChannelServices_RegisterChannel_m6138,
	ChannelServices_RegisterChannelConfig_m6139,
	ChannelServices_CreateProvider_m6140,
	ChannelServices_GetCurrentChannelInfo_m6141,
	CrossAppDomainData__ctor_m6142,
	CrossAppDomainData_get_DomainID_m6143,
	CrossAppDomainData_get_ProcessID_m6144,
	CrossAppDomainChannel__ctor_m6145,
	CrossAppDomainChannel__cctor_m6146,
	CrossAppDomainChannel_RegisterCrossAppDomainChannel_m6147,
	CrossAppDomainChannel_get_ChannelName_m6148,
	CrossAppDomainChannel_get_ChannelPriority_m6149,
	CrossAppDomainChannel_get_ChannelData_m6150,
	CrossAppDomainChannel_StartListening_m6151,
	CrossAppDomainChannel_CreateMessageSink_m6152,
	CrossAppDomainSink__ctor_m6153,
	CrossAppDomainSink__cctor_m6154,
	CrossAppDomainSink_GetSink_m6155,
	CrossAppDomainSink_get_TargetDomainId_m6156,
	SinkProviderData__ctor_m6157,
	SinkProviderData_get_Children_m6158,
	SinkProviderData_get_Properties_m6159,
	Context__cctor_m6160,
	Context_Finalize_m6161,
	Context_get_DefaultContext_m6162,
	Context_get_IsDefaultContext_m6163,
	Context_GetProperty_m6164,
	Context_ToString_m6165,
	ContextAttribute__ctor_m6166,
	ContextAttribute_get_Name_m6167,
	ContextAttribute_Equals_m6168,
	ContextAttribute_GetHashCode_m6169,
	ContextAttribute_GetPropertiesForNewContext_m6170,
	ContextAttribute_IsContextOK_m6171,
	CrossContextChannel__ctor_m6172,
	SynchronizationAttribute__ctor_m6173,
	SynchronizationAttribute__ctor_m6174,
	SynchronizationAttribute_set_Locked_m6175,
	SynchronizationAttribute_ReleaseLock_m6176,
	SynchronizationAttribute_GetPropertiesForNewContext_m6177,
	SynchronizationAttribute_IsContextOK_m6178,
	SynchronizationAttribute_ExitContext_m6179,
	SynchronizationAttribute_EnterContext_m6180,
	LeaseManager__ctor_m6181,
	LeaseManager_SetPollTime_m6182,
	LifetimeServices__cctor_m6183,
	LifetimeServices_set_LeaseManagerPollTime_m6184,
	LifetimeServices_set_LeaseTime_m6185,
	LifetimeServices_set_RenewOnCallTime_m6186,
	LifetimeServices_set_SponsorshipTimeout_m6187,
	ArgInfo__ctor_m6188,
	ArgInfo_GetInOutArgs_m6189,
	AsyncResult__ctor_m6190,
	AsyncResult_get_AsyncState_m6191,
	AsyncResult_get_AsyncWaitHandle_m6192,
	AsyncResult_get_CompletedSynchronously_m6193,
	AsyncResult_get_IsCompleted_m6194,
	AsyncResult_get_EndInvokeCalled_m6195,
	AsyncResult_set_EndInvokeCalled_m6196,
	AsyncResult_get_AsyncDelegate_m6197,
	AsyncResult_get_NextSink_m6198,
	AsyncResult_AsyncProcessMessage_m6199,
	AsyncResult_GetReplyMessage_m6200,
	AsyncResult_SetMessageCtrl_m6201,
	AsyncResult_SetCompletedSynchronously_m6202,
	AsyncResult_EndInvoke_m6203,
	AsyncResult_SyncProcessMessage_m6204,
	AsyncResult_get_CallMessage_m6205,
	AsyncResult_set_CallMessage_m6206,
	ConstructionCall__ctor_m6207,
	ConstructionCall__ctor_m6208,
	ConstructionCall_InitDictionary_m6209,
	ConstructionCall_set_IsContextOk_m6210,
	ConstructionCall_get_ActivationType_m6211,
	ConstructionCall_get_ActivationTypeName_m6212,
	ConstructionCall_get_Activator_m6213,
	ConstructionCall_set_Activator_m6214,
	ConstructionCall_get_CallSiteActivationAttributes_m6215,
	ConstructionCall_SetActivationAttributes_m6216,
	ConstructionCall_get_ContextProperties_m6217,
	ConstructionCall_InitMethodProperty_m6218,
	ConstructionCall_GetObjectData_m6219,
	ConstructionCall_get_Properties_m6220,
	ConstructionCallDictionary__ctor_m6221,
	ConstructionCallDictionary__cctor_m6222,
	ConstructionCallDictionary_GetMethodProperty_m6223,
	ConstructionCallDictionary_SetMethodProperty_m6224,
	EnvoyTerminatorSink__ctor_m6225,
	EnvoyTerminatorSink__cctor_m6226,
	Header__ctor_m6227,
	Header__ctor_m6228,
	Header__ctor_m6229,
	LogicalCallContext__ctor_m6230,
	LogicalCallContext__ctor_m6231,
	LogicalCallContext_GetObjectData_m6232,
	LogicalCallContext_SetData_m6233,
	CallContextRemotingData__ctor_m6234,
	MethodCall__ctor_m6235,
	MethodCall__ctor_m6236,
	MethodCall__ctor_m6237,
	MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m6238,
	MethodCall_InitMethodProperty_m6239,
	MethodCall_GetObjectData_m6240,
	MethodCall_get_Args_m6241,
	MethodCall_get_LogicalCallContext_m6242,
	MethodCall_get_MethodBase_m6243,
	MethodCall_get_MethodName_m6244,
	MethodCall_get_MethodSignature_m6245,
	MethodCall_get_Properties_m6246,
	MethodCall_InitDictionary_m6247,
	MethodCall_get_TypeName_m6248,
	MethodCall_get_Uri_m6249,
	MethodCall_set_Uri_m6250,
	MethodCall_Init_m6251,
	MethodCall_ResolveMethod_m6252,
	MethodCall_CastTo_m6253,
	MethodCall_GetTypeNameFromAssemblyQualifiedName_m6254,
	MethodCall_get_GenericArguments_m6255,
	MethodCallDictionary__ctor_m6256,
	MethodCallDictionary__cctor_m6257,
	DictionaryEnumerator__ctor_m6258,
	DictionaryEnumerator_get_Current_m6259,
	DictionaryEnumerator_MoveNext_m6260,
	DictionaryEnumerator_get_Entry_m6261,
	DictionaryEnumerator_get_Key_m6262,
	DictionaryEnumerator_get_Value_m6263,
	MethodDictionary__ctor_m6264,
	MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m6265,
	MethodDictionary_set_MethodKeys_m6266,
	MethodDictionary_AllocInternalProperties_m6267,
	MethodDictionary_GetInternalProperties_m6268,
	MethodDictionary_IsOverridenKey_m6269,
	MethodDictionary_get_Item_m6270,
	MethodDictionary_set_Item_m6271,
	MethodDictionary_GetMethodProperty_m6272,
	MethodDictionary_SetMethodProperty_m6273,
	MethodDictionary_get_Values_m6274,
	MethodDictionary_Add_m6275,
	MethodDictionary_Contains_m6276,
	MethodDictionary_Remove_m6277,
	MethodDictionary_get_Count_m6278,
	MethodDictionary_get_SyncRoot_m6279,
	MethodDictionary_CopyTo_m6280,
	MethodDictionary_GetEnumerator_m6281,
	MethodReturnDictionary__ctor_m6282,
	MethodReturnDictionary__cctor_m6283,
	MonoMethodMessage_get_Args_m6284,
	MonoMethodMessage_get_LogicalCallContext_m6285,
	MonoMethodMessage_get_MethodBase_m6286,
	MonoMethodMessage_get_MethodName_m6287,
	MonoMethodMessage_get_MethodSignature_m6288,
	MonoMethodMessage_get_TypeName_m6289,
	MonoMethodMessage_get_Uri_m6290,
	MonoMethodMessage_set_Uri_m6291,
	MonoMethodMessage_get_Exception_m6292,
	MonoMethodMessage_get_OutArgCount_m6293,
	MonoMethodMessage_get_OutArgs_m6294,
	MonoMethodMessage_get_ReturnValue_m6295,
	RemotingSurrogate__ctor_m6296,
	RemotingSurrogate_SetObjectData_m6297,
	ObjRefSurrogate__ctor_m6298,
	ObjRefSurrogate_SetObjectData_m6299,
	RemotingSurrogateSelector__ctor_m6300,
	RemotingSurrogateSelector__cctor_m6301,
	RemotingSurrogateSelector_GetSurrogate_m6302,
	ReturnMessage__ctor_m6303,
	ReturnMessage__ctor_m6304,
	ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m6305,
	ReturnMessage_get_Args_m6306,
	ReturnMessage_get_LogicalCallContext_m6307,
	ReturnMessage_get_MethodBase_m6308,
	ReturnMessage_get_MethodName_m6309,
	ReturnMessage_get_MethodSignature_m6310,
	ReturnMessage_get_Properties_m6311,
	ReturnMessage_get_TypeName_m6312,
	ReturnMessage_get_Uri_m6313,
	ReturnMessage_set_Uri_m6314,
	ReturnMessage_get_Exception_m6315,
	ReturnMessage_get_OutArgs_m6316,
	ReturnMessage_get_ReturnValue_m6317,
	SoapAttribute__ctor_m6318,
	SoapAttribute_get_UseAttribute_m6319,
	SoapAttribute_get_XmlNamespace_m6320,
	SoapAttribute_SetReflectionObject_m6321,
	SoapFieldAttribute__ctor_m6322,
	SoapFieldAttribute_get_XmlElementName_m6323,
	SoapFieldAttribute_IsInteropXmlElement_m6324,
	SoapFieldAttribute_SetReflectionObject_m6325,
	SoapMethodAttribute__ctor_m6326,
	SoapMethodAttribute_get_UseAttribute_m6327,
	SoapMethodAttribute_get_XmlNamespace_m6328,
	SoapMethodAttribute_SetReflectionObject_m6329,
	SoapParameterAttribute__ctor_m6330,
	SoapTypeAttribute__ctor_m6331,
	SoapTypeAttribute_get_UseAttribute_m6332,
	SoapTypeAttribute_get_XmlElementName_m6333,
	SoapTypeAttribute_get_XmlNamespace_m6334,
	SoapTypeAttribute_get_XmlTypeName_m6335,
	SoapTypeAttribute_get_XmlTypeNamespace_m6336,
	SoapTypeAttribute_get_IsInteropXmlElement_m6337,
	SoapTypeAttribute_get_IsInteropXmlType_m6338,
	SoapTypeAttribute_SetReflectionObject_m6339,
	ProxyAttribute_CreateInstance_m6340,
	ProxyAttribute_CreateProxy_m6341,
	ProxyAttribute_GetPropertiesForNewContext_m6342,
	ProxyAttribute_IsContextOK_m6343,
	RealProxy__ctor_m6344,
	RealProxy__ctor_m6345,
	RealProxy__ctor_m6346,
	RealProxy_InternalGetProxyType_m6347,
	RealProxy_GetProxiedType_m6348,
	RealProxy_InternalGetTransparentProxy_m6349,
	RealProxy_GetTransparentProxy_m6350,
	RealProxy_SetTargetDomain_m6351,
	RemotingProxy__ctor_m6352,
	RemotingProxy__ctor_m6353,
	RemotingProxy__cctor_m6354,
	RemotingProxy_get_TypeName_m6355,
	RemotingProxy_Finalize_m6356,
	TrackingServices__cctor_m6357,
	TrackingServices_NotifyUnmarshaledObject_m6358,
	ActivatedClientTypeEntry__ctor_m6359,
	ActivatedClientTypeEntry_get_ApplicationUrl_m6360,
	ActivatedClientTypeEntry_get_ContextAttributes_m6361,
	ActivatedClientTypeEntry_get_ObjectType_m6362,
	ActivatedClientTypeEntry_ToString_m6363,
	ActivatedServiceTypeEntry__ctor_m6364,
	ActivatedServiceTypeEntry_get_ObjectType_m6365,
	ActivatedServiceTypeEntry_ToString_m6366,
	EnvoyInfo__ctor_m6367,
	EnvoyInfo_get_EnvoySinks_m6368,
	Identity__ctor_m6369,
	Identity_get_ChannelSink_m6370,
	Identity_set_ChannelSink_m6371,
	Identity_get_ObjectUri_m6372,
	Identity_get_Disposed_m6373,
	Identity_set_Disposed_m6374,
	ClientIdentity__ctor_m6375,
	ClientIdentity_get_ClientProxy_m6376,
	ClientIdentity_set_ClientProxy_m6377,
	ClientIdentity_CreateObjRef_m6378,
	ClientIdentity_get_TargetUri_m6379,
	InternalRemotingServices__cctor_m6380,
	InternalRemotingServices_GetCachedSoapAttribute_m6381,
	ObjRef__ctor_m6382,
	ObjRef__ctor_m6383,
	ObjRef__cctor_m6384,
	ObjRef_get_IsReferenceToWellKnow_m6385,
	ObjRef_get_ChannelInfo_m6386,
	ObjRef_get_EnvoyInfo_m6387,
	ObjRef_set_EnvoyInfo_m6388,
	ObjRef_get_TypeInfo_m6389,
	ObjRef_set_TypeInfo_m6390,
	ObjRef_get_URI_m6391,
	ObjRef_set_URI_m6392,
	ObjRef_GetObjectData_m6393,
	ObjRef_GetRealObject_m6394,
	ObjRef_UpdateChannelInfo_m6395,
	ObjRef_get_ServerType_m6396,
	RemotingConfiguration__cctor_m6397,
	RemotingConfiguration_get_ApplicationName_m6398,
	RemotingConfiguration_set_ApplicationName_m6399,
	RemotingConfiguration_get_ProcessId_m6400,
	RemotingConfiguration_LoadDefaultDelayedChannels_m6401,
	RemotingConfiguration_IsRemotelyActivatedClientType_m6402,
	RemotingConfiguration_RegisterActivatedClientType_m6403,
	RemotingConfiguration_RegisterActivatedServiceType_m6404,
	RemotingConfiguration_RegisterWellKnownClientType_m6405,
	RemotingConfiguration_RegisterWellKnownServiceType_m6406,
	RemotingConfiguration_RegisterChannelTemplate_m6407,
	RemotingConfiguration_RegisterClientProviderTemplate_m6408,
	RemotingConfiguration_RegisterServerProviderTemplate_m6409,
	RemotingConfiguration_RegisterChannels_m6410,
	RemotingConfiguration_RegisterTypes_m6411,
	RemotingConfiguration_SetCustomErrorsMode_m6412,
	ConfigHandler__ctor_m6413,
	ConfigHandler_ValidatePath_m6414,
	ConfigHandler_CheckPath_m6415,
	ConfigHandler_OnStartParsing_m6416,
	ConfigHandler_OnProcessingInstruction_m6417,
	ConfigHandler_OnIgnorableWhitespace_m6418,
	ConfigHandler_OnStartElement_m6419,
	ConfigHandler_ParseElement_m6420,
	ConfigHandler_OnEndElement_m6421,
	ConfigHandler_ReadCustomProviderData_m6422,
	ConfigHandler_ReadLifetine_m6423,
	ConfigHandler_ParseTime_m6424,
	ConfigHandler_ReadChannel_m6425,
	ConfigHandler_ReadProvider_m6426,
	ConfigHandler_ReadClientActivated_m6427,
	ConfigHandler_ReadServiceActivated_m6428,
	ConfigHandler_ReadClientWellKnown_m6429,
	ConfigHandler_ReadServiceWellKnown_m6430,
	ConfigHandler_ReadInteropXml_m6431,
	ConfigHandler_ReadPreload_m6432,
	ConfigHandler_GetNotNull_m6433,
	ConfigHandler_ExtractAssembly_m6434,
	ConfigHandler_OnChars_m6435,
	ConfigHandler_OnEndParsing_m6436,
	ChannelData__ctor_m6437,
	ChannelData_get_ServerProviders_m6438,
	ChannelData_get_ClientProviders_m6439,
	ChannelData_get_CustomProperties_m6440,
	ChannelData_CopyFrom_m6441,
	ProviderData__ctor_m6442,
	ProviderData_CopyFrom_m6443,
	FormatterData__ctor_m6444,
	RemotingException__ctor_m6445,
	RemotingException__ctor_m6446,
	RemotingException__ctor_m6447,
	RemotingException__ctor_m6448,
	RemotingServices__cctor_m6449,
	RemotingServices_GetVirtualMethod_m6450,
	RemotingServices_IsTransparentProxy_m6451,
	RemotingServices_GetServerTypeForUri_m6452,
	RemotingServices_Unmarshal_m6453,
	RemotingServices_Unmarshal_m6454,
	RemotingServices_GetRealProxy_m6455,
	RemotingServices_GetMethodBaseFromMethodMessage_m6456,
	RemotingServices_GetMethodBaseFromName_m6457,
	RemotingServices_FindInterfaceMethod_m6458,
	RemotingServices_CreateClientProxy_m6459,
	RemotingServices_CreateClientProxy_m6460,
	RemotingServices_CreateClientProxyForContextBound_m6461,
	RemotingServices_GetIdentityForUri_m6462,
	RemotingServices_RemoveAppNameFromUri_m6463,
	RemotingServices_GetOrCreateClientIdentity_m6464,
	RemotingServices_GetClientChannelSinkChain_m6465,
	RemotingServices_CreateWellKnownServerIdentity_m6466,
	RemotingServices_RegisterServerIdentity_m6467,
	RemotingServices_GetProxyForRemoteObject_m6468,
	RemotingServices_GetRemoteObject_m6469,
	RemotingServices_RegisterInternalChannels_m6470,
	RemotingServices_DisposeIdentity_m6471,
	RemotingServices_GetNormalizedUri_m6472,
	ServerIdentity__ctor_m6473,
	ServerIdentity_get_ObjectType_m6474,
	ServerIdentity_CreateObjRef_m6475,
	ClientActivatedIdentity_GetServerObject_m6476,
	SingletonIdentity__ctor_m6477,
	SingleCallIdentity__ctor_m6478,
	TypeInfo__ctor_m6479,
	SoapServices__cctor_m6480,
	SoapServices_get_XmlNsForClrTypeWithAssembly_m6481,
	SoapServices_get_XmlNsForClrTypeWithNs_m6482,
	SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m6483,
	SoapServices_CodeXmlNamespaceForClrTypeNamespace_m6484,
	SoapServices_GetNameKey_m6485,
	SoapServices_GetAssemblyName_m6486,
	SoapServices_GetXmlElementForInteropType_m6487,
	SoapServices_GetXmlNamespaceForMethodCall_m6488,
	SoapServices_GetXmlNamespaceForMethodResponse_m6489,
	SoapServices_GetXmlTypeForInteropType_m6490,
	SoapServices_PreLoad_m6491,
	SoapServices_PreLoad_m6492,
	SoapServices_RegisterInteropXmlElement_m6493,
	SoapServices_RegisterInteropXmlType_m6494,
	SoapServices_EncodeNs_m6495,
	TypeEntry__ctor_m6496,
	TypeEntry_get_AssemblyName_m6497,
	TypeEntry_set_AssemblyName_m6498,
	TypeEntry_get_TypeName_m6499,
	TypeEntry_set_TypeName_m6500,
	TypeInfo__ctor_m6501,
	TypeInfo_get_TypeName_m6502,
	WellKnownClientTypeEntry__ctor_m6503,
	WellKnownClientTypeEntry_get_ApplicationUrl_m6504,
	WellKnownClientTypeEntry_get_ObjectType_m6505,
	WellKnownClientTypeEntry_get_ObjectUrl_m6506,
	WellKnownClientTypeEntry_ToString_m6507,
	WellKnownServiceTypeEntry__ctor_m6508,
	WellKnownServiceTypeEntry_get_Mode_m6509,
	WellKnownServiceTypeEntry_get_ObjectType_m6510,
	WellKnownServiceTypeEntry_get_ObjectUri_m6511,
	WellKnownServiceTypeEntry_ToString_m6512,
	BinaryCommon__cctor_m6513,
	BinaryCommon_IsPrimitive_m6514,
	BinaryCommon_GetTypeFromCode_m6515,
	BinaryCommon_SwapBytes_m6516,
	BinaryFormatter__ctor_m6517,
	BinaryFormatter__ctor_m6518,
	BinaryFormatter_get_DefaultSurrogateSelector_m6519,
	BinaryFormatter_set_AssemblyFormat_m6520,
	BinaryFormatter_get_Binder_m6521,
	BinaryFormatter_get_Context_m6522,
	BinaryFormatter_get_SurrogateSelector_m6523,
	BinaryFormatter_get_FilterLevel_m6524,
	BinaryFormatter_Deserialize_m6525,
	BinaryFormatter_NoCheckDeserialize_m6526,
	BinaryFormatter_ReadBinaryHeader_m6527,
	MessageFormatter_ReadMethodCall_m6528,
	MessageFormatter_ReadMethodResponse_m6529,
	TypeMetadata__ctor_m6530,
	ArrayNullFiller__ctor_m6531,
	ObjectReader__ctor_m6532,
	ObjectReader_ReadObjectGraph_m6533,
	ObjectReader_ReadObjectGraph_m6534,
	ObjectReader_ReadNextObject_m6535,
	ObjectReader_ReadNextObject_m6536,
	ObjectReader_get_CurrentObject_m6537,
	ObjectReader_ReadObject_m6538,
	ObjectReader_ReadAssembly_m6539,
	ObjectReader_ReadObjectInstance_m6540,
	ObjectReader_ReadRefTypeObjectInstance_m6541,
	ObjectReader_ReadObjectContent_m6542,
	ObjectReader_RegisterObject_m6543,
	ObjectReader_ReadStringIntance_m6544,
	ObjectReader_ReadGenericArray_m6545,
	ObjectReader_ReadBoxedPrimitiveTypeValue_m6546,
	ObjectReader_ReadArrayOfPrimitiveType_m6547,
	ObjectReader_BlockRead_m6548,
	ObjectReader_ReadArrayOfObject_m6549,
	ObjectReader_ReadArrayOfString_m6550,
	ObjectReader_ReadSimpleArray_m6551,
	ObjectReader_ReadTypeMetadata_m6552,
	ObjectReader_ReadValue_m6553,
	ObjectReader_SetObjectValue_m6554,
	ObjectReader_RecordFixup_m6555,
	ObjectReader_GetDeserializationType_m6556,
	ObjectReader_ReadType_m6557,
	ObjectReader_ReadPrimitiveTypeValue_m6558,
	FormatterConverter__ctor_m6559,
	FormatterConverter_Convert_m6560,
	FormatterConverter_ToBoolean_m6561,
	FormatterConverter_ToInt16_m6562,
	FormatterConverter_ToInt32_m6563,
	FormatterConverter_ToInt64_m6564,
	FormatterConverter_ToString_m6565,
	FormatterServices_GetUninitializedObject_m6566,
	FormatterServices_GetSafeUninitializedObject_m6567,
	ObjectManager__ctor_m6568,
	ObjectManager_DoFixups_m6569,
	ObjectManager_GetObjectRecord_m6570,
	ObjectManager_GetObject_m6571,
	ObjectManager_RaiseDeserializationEvent_m6572,
	ObjectManager_RaiseOnDeserializingEvent_m6573,
	ObjectManager_RaiseOnDeserializedEvent_m6574,
	ObjectManager_AddFixup_m6575,
	ObjectManager_RecordArrayElementFixup_m6576,
	ObjectManager_RecordArrayElementFixup_m6577,
	ObjectManager_RecordDelayedFixup_m6578,
	ObjectManager_RecordFixup_m6579,
	ObjectManager_RegisterObjectInternal_m6580,
	ObjectManager_RegisterObject_m6581,
	BaseFixupRecord__ctor_m6582,
	BaseFixupRecord_DoFixup_m6583,
	ArrayFixupRecord__ctor_m6584,
	ArrayFixupRecord_FixupImpl_m6585,
	MultiArrayFixupRecord__ctor_m6586,
	MultiArrayFixupRecord_FixupImpl_m6587,
	FixupRecord__ctor_m6588,
	FixupRecord_FixupImpl_m6589,
	DelayedFixupRecord__ctor_m6590,
	DelayedFixupRecord_FixupImpl_m6591,
	ObjectRecord__ctor_m6592,
	ObjectRecord_SetMemberValue_m6593,
	ObjectRecord_SetArrayValue_m6594,
	ObjectRecord_SetMemberValue_m6595,
	ObjectRecord_get_IsInstanceReady_m6596,
	ObjectRecord_get_IsUnsolvedObjectReference_m6597,
	ObjectRecord_get_IsRegistered_m6598,
	ObjectRecord_DoFixups_m6599,
	ObjectRecord_RemoveFixup_m6600,
	ObjectRecord_UnchainFixup_m6601,
	ObjectRecord_ChainFixup_m6602,
	ObjectRecord_LoadData_m6603,
	ObjectRecord_get_HasPendingFixups_m6604,
	SerializationBinder__ctor_m6605,
	CallbackHandler__ctor_m6606,
	CallbackHandler_Invoke_m6607,
	CallbackHandler_BeginInvoke_m6608,
	CallbackHandler_EndInvoke_m6609,
	SerializationCallbacks__ctor_m6610,
	SerializationCallbacks__cctor_m6611,
	SerializationCallbacks_get_HasDeserializedCallbacks_m6612,
	SerializationCallbacks_GetMethodsByAttribute_m6613,
	SerializationCallbacks_Invoke_m6614,
	SerializationCallbacks_RaiseOnDeserializing_m6615,
	SerializationCallbacks_RaiseOnDeserialized_m6616,
	SerializationCallbacks_GetSerializationCallbacks_m6617,
	SerializationEntry__ctor_m6618,
	SerializationEntry_get_Name_m6619,
	SerializationEntry_get_Value_m6620,
	SerializationException__ctor_m6621,
	SerializationException__ctor_m2247,
	SerializationException__ctor_m6622,
	SerializationInfo__ctor_m6623,
	SerializationInfo_AddValue_m2243,
	SerializationInfo_GetValue_m2246,
	SerializationInfo_SetType_m6624,
	SerializationInfo_GetEnumerator_m6625,
	SerializationInfo_AddValue_m6626,
	SerializationInfo_AddValue_m2245,
	SerializationInfo_AddValue_m2244,
	SerializationInfo_AddValue_m6627,
	SerializationInfo_AddValue_m6628,
	SerializationInfo_AddValue_m2257,
	SerializationInfo_AddValue_m6629,
	SerializationInfo_AddValue_m2256,
	SerializationInfo_GetBoolean_m2248,
	SerializationInfo_GetInt16_m6630,
	SerializationInfo_GetInt32_m2255,
	SerializationInfo_GetInt64_m2254,
	SerializationInfo_GetString_m2253,
	SerializationInfoEnumerator__ctor_m6631,
	SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m6632,
	SerializationInfoEnumerator_get_Current_m6633,
	SerializationInfoEnumerator_get_Name_m6634,
	SerializationInfoEnumerator_get_Value_m6635,
	SerializationInfoEnumerator_MoveNext_m6636,
	StreamingContext__ctor_m6637,
	StreamingContext__ctor_m6638,
	StreamingContext_get_State_m6639,
	StreamingContext_Equals_m6640,
	StreamingContext_GetHashCode_m6641,
	X509Certificate__ctor_m6642,
	X509Certificate__ctor_m3354,
	X509Certificate__ctor_m2312,
	X509Certificate__ctor_m6643,
	X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6644,
	X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m6645,
	X509Certificate_tostr_m6646,
	X509Certificate_Equals_m6647,
	X509Certificate_GetCertHash_m6648,
	X509Certificate_GetCertHashString_m2317,
	X509Certificate_GetEffectiveDateString_m6649,
	X509Certificate_GetExpirationDateString_m6650,
	X509Certificate_GetHashCode_m6651,
	X509Certificate_GetIssuerName_m6652,
	X509Certificate_GetName_m6653,
	X509Certificate_GetPublicKey_m6654,
	X509Certificate_GetRawCertData_m6655,
	X509Certificate_ToString_m6656,
	X509Certificate_ToString_m2330,
	X509Certificate_get_Issuer_m2333,
	X509Certificate_get_Subject_m2332,
	X509Certificate_Equals_m6657,
	X509Certificate_Import_m2327,
	X509Certificate_Reset_m2329,
	AsymmetricAlgorithm__ctor_m6658,
	AsymmetricAlgorithm_System_IDisposable_Dispose_m6659,
	AsymmetricAlgorithm_get_KeySize_m3304,
	AsymmetricAlgorithm_set_KeySize_m3303,
	AsymmetricAlgorithm_Clear_m3359,
	AsymmetricAlgorithm_GetNamedParam_m6660,
	AsymmetricKeyExchangeFormatter__ctor_m6661,
	AsymmetricSignatureDeformatter__ctor_m3349,
	AsymmetricSignatureFormatter__ctor_m3350,
	Base64Constants__cctor_m6662,
	CryptoConfig__cctor_m6663,
	CryptoConfig_Initialize_m6664,
	CryptoConfig_CreateFromName_m2335,
	CryptoConfig_CreateFromName_m2361,
	CryptoConfig_MapNameToOID_m3297,
	CryptoConfig_EncodeOID_m2337,
	CryptoConfig_EncodeLongNumber_m6665,
	CryptographicException__ctor_m6666,
	CryptographicException__ctor_m2291,
	CryptographicException__ctor_m2295,
	CryptographicException__ctor_m2460,
	CryptographicException__ctor_m6667,
	CryptographicUnexpectedOperationException__ctor_m6668,
	CryptographicUnexpectedOperationException__ctor_m3332,
	CryptographicUnexpectedOperationException__ctor_m6669,
	CspParameters__ctor_m3299,
	CspParameters__ctor_m6670,
	CspParameters__ctor_m6671,
	CspParameters__ctor_m6672,
	CspParameters_get_Flags_m6673,
	CspParameters_set_Flags_m3300,
	DES__ctor_m6674,
	DES__cctor_m6675,
	DES_Create_m3333,
	DES_Create_m6676,
	DES_IsWeakKey_m6677,
	DES_IsSemiWeakKey_m6678,
	DES_get_Key_m6679,
	DES_set_Key_m6680,
	DESTransform__ctor_m6681,
	DESTransform__cctor_m6682,
	DESTransform_CipherFunct_m6683,
	DESTransform_Permutation_m6684,
	DESTransform_BSwap_m6685,
	DESTransform_SetKey_m6686,
	DESTransform_ProcessBlock_m6687,
	DESTransform_ECB_m6688,
	DESTransform_GetStrongKey_m6689,
	DESCryptoServiceProvider__ctor_m6690,
	DESCryptoServiceProvider_CreateDecryptor_m6691,
	DESCryptoServiceProvider_CreateEncryptor_m6692,
	DESCryptoServiceProvider_GenerateIV_m6693,
	DESCryptoServiceProvider_GenerateKey_m6694,
	DSA__ctor_m6695,
	DSA_Create_m2286,
	DSA_Create_m6696,
	DSA_ZeroizePrivateKey_m6697,
	DSA_FromXmlString_m6698,
	DSA_ToXmlString_m6699,
	DSACryptoServiceProvider__ctor_m6700,
	DSACryptoServiceProvider__ctor_m2296,
	DSACryptoServiceProvider__ctor_m6701,
	DSACryptoServiceProvider__cctor_m6702,
	DSACryptoServiceProvider_Finalize_m6703,
	DSACryptoServiceProvider_get_KeySize_m6704,
	DSACryptoServiceProvider_get_PublicOnly_m2285,
	DSACryptoServiceProvider_ExportParameters_m6705,
	DSACryptoServiceProvider_ImportParameters_m6706,
	DSACryptoServiceProvider_CreateSignature_m6707,
	DSACryptoServiceProvider_VerifySignature_m6708,
	DSACryptoServiceProvider_Dispose_m6709,
	DSACryptoServiceProvider_OnKeyGenerated_m6710,
	DSASignatureDeformatter__ctor_m6711,
	DSASignatureDeformatter__ctor_m3318,
	DSASignatureDeformatter_SetHashAlgorithm_m6712,
	DSASignatureDeformatter_SetKey_m6713,
	DSASignatureDeformatter_VerifySignature_m6714,
	DSASignatureFormatter__ctor_m6715,
	DSASignatureFormatter_CreateSignature_m6716,
	DSASignatureFormatter_SetHashAlgorithm_m6717,
	DSASignatureFormatter_SetKey_m6718,
	HMAC__ctor_m6719,
	HMAC_get_BlockSizeValue_m6720,
	HMAC_set_BlockSizeValue_m6721,
	HMAC_set_HashName_m6722,
	HMAC_get_Key_m6723,
	HMAC_set_Key_m6724,
	HMAC_get_Block_m6725,
	HMAC_KeySetup_m6726,
	HMAC_Dispose_m6727,
	HMAC_HashCore_m6728,
	HMAC_HashFinal_m6729,
	HMAC_Initialize_m6730,
	HMAC_Create_m3311,
	HMAC_Create_m6731,
	HMACMD5__ctor_m6732,
	HMACMD5__ctor_m6733,
	HMACRIPEMD160__ctor_m6734,
	HMACRIPEMD160__ctor_m6735,
	HMACSHA1__ctor_m6736,
	HMACSHA1__ctor_m6737,
	HMACSHA256__ctor_m6738,
	HMACSHA256__ctor_m6739,
	HMACSHA384__ctor_m6740,
	HMACSHA384__ctor_m6741,
	HMACSHA384__cctor_m6742,
	HMACSHA384_set_ProduceLegacyHmacValues_m6743,
	HMACSHA512__ctor_m6744,
	HMACSHA512__ctor_m6745,
	HMACSHA512__cctor_m6746,
	HMACSHA512_set_ProduceLegacyHmacValues_m6747,
	HashAlgorithm__ctor_m3296,
	HashAlgorithm_System_IDisposable_Dispose_m6748,
	HashAlgorithm_get_CanReuseTransform_m6749,
	HashAlgorithm_ComputeHash_m2371,
	HashAlgorithm_ComputeHash_m3306,
	HashAlgorithm_Create_m3305,
	HashAlgorithm_get_Hash_m6750,
	HashAlgorithm_get_HashSize_m6751,
	HashAlgorithm_Dispose_m6752,
	HashAlgorithm_TransformBlock_m6753,
	HashAlgorithm_TransformFinalBlock_m6754,
	KeySizes__ctor_m2462,
	KeySizes_get_MaxSize_m6755,
	KeySizes_get_MinSize_m6756,
	KeySizes_get_SkipSize_m6757,
	KeySizes_IsLegal_m6758,
	KeySizes_IsLegalKeySize_m6759,
	KeyedHashAlgorithm__ctor_m3331,
	KeyedHashAlgorithm_Finalize_m6760,
	KeyedHashAlgorithm_get_Key_m6761,
	KeyedHashAlgorithm_set_Key_m6762,
	KeyedHashAlgorithm_Dispose_m6763,
	KeyedHashAlgorithm_ZeroizeKey_m6764,
	MACTripleDES__ctor_m6765,
	MACTripleDES_Setup_m6766,
	MACTripleDES_Finalize_m6767,
	MACTripleDES_Dispose_m6768,
	MACTripleDES_Initialize_m6769,
	MACTripleDES_HashCore_m6770,
	MACTripleDES_HashFinal_m6771,
	MD5__ctor_m6772,
	MD5_Create_m3315,
	MD5_Create_m6773,
	MD5CryptoServiceProvider__ctor_m6774,
	MD5CryptoServiceProvider__cctor_m6775,
	MD5CryptoServiceProvider_Finalize_m6776,
	MD5CryptoServiceProvider_Dispose_m6777,
	MD5CryptoServiceProvider_HashCore_m6778,
	MD5CryptoServiceProvider_HashFinal_m6779,
	MD5CryptoServiceProvider_Initialize_m6780,
	MD5CryptoServiceProvider_ProcessBlock_m6781,
	MD5CryptoServiceProvider_ProcessFinalBlock_m6782,
	MD5CryptoServiceProvider_AddLength_m6783,
	RC2__ctor_m6784,
	RC2_Create_m3334,
	RC2_Create_m6785,
	RC2_get_EffectiveKeySize_m6786,
	RC2_get_KeySize_m6787,
	RC2_set_KeySize_m6788,
	RC2CryptoServiceProvider__ctor_m6789,
	RC2CryptoServiceProvider_get_EffectiveKeySize_m6790,
	RC2CryptoServiceProvider_CreateDecryptor_m6791,
	RC2CryptoServiceProvider_CreateEncryptor_m6792,
	RC2CryptoServiceProvider_GenerateIV_m6793,
	RC2CryptoServiceProvider_GenerateKey_m6794,
	RC2Transform__ctor_m6795,
	RC2Transform__cctor_m6796,
	RC2Transform_ECB_m6797,
	RIPEMD160__ctor_m6798,
	RIPEMD160Managed__ctor_m6799,
	RIPEMD160Managed_Initialize_m6800,
	RIPEMD160Managed_HashCore_m6801,
	RIPEMD160Managed_HashFinal_m6802,
	RIPEMD160Managed_Finalize_m6803,
	RIPEMD160Managed_ProcessBlock_m6804,
	RIPEMD160Managed_Compress_m6805,
	RIPEMD160Managed_CompressFinal_m6806,
	RIPEMD160Managed_ROL_m6807,
	RIPEMD160Managed_F_m6808,
	RIPEMD160Managed_G_m6809,
	RIPEMD160Managed_H_m6810,
	RIPEMD160Managed_I_m6811,
	RIPEMD160Managed_J_m6812,
	RIPEMD160Managed_FF_m6813,
	RIPEMD160Managed_GG_m6814,
	RIPEMD160Managed_HH_m6815,
	RIPEMD160Managed_II_m6816,
	RIPEMD160Managed_JJ_m6817,
	RIPEMD160Managed_FFF_m6818,
	RIPEMD160Managed_GGG_m6819,
	RIPEMD160Managed_HHH_m6820,
	RIPEMD160Managed_III_m6821,
	RIPEMD160Managed_JJJ_m6822,
	RNGCryptoServiceProvider__ctor_m6823,
	RNGCryptoServiceProvider__cctor_m6824,
	RNGCryptoServiceProvider_Check_m6825,
	RNGCryptoServiceProvider_RngOpen_m6826,
	RNGCryptoServiceProvider_RngInitialize_m6827,
	RNGCryptoServiceProvider_RngGetBytes_m6828,
	RNGCryptoServiceProvider_RngClose_m6829,
	RNGCryptoServiceProvider_GetBytes_m6830,
	RNGCryptoServiceProvider_GetNonZeroBytes_m6831,
	RNGCryptoServiceProvider_Finalize_m6832,
	RSA__ctor_m3302,
	RSA_Create_m2283,
	RSA_Create_m6833,
	RSA_ZeroizePrivateKey_m6834,
	RSA_FromXmlString_m6835,
	RSA_ToXmlString_m6836,
	RSACryptoServiceProvider__ctor_m6837,
	RSACryptoServiceProvider__ctor_m3301,
	RSACryptoServiceProvider__ctor_m2297,
	RSACryptoServiceProvider__cctor_m6838,
	RSACryptoServiceProvider_Common_m6839,
	RSACryptoServiceProvider_Finalize_m6840,
	RSACryptoServiceProvider_get_KeySize_m6841,
	RSACryptoServiceProvider_get_PublicOnly_m2281,
	RSACryptoServiceProvider_DecryptValue_m6842,
	RSACryptoServiceProvider_EncryptValue_m6843,
	RSACryptoServiceProvider_ExportParameters_m6844,
	RSACryptoServiceProvider_ImportParameters_m6845,
	RSACryptoServiceProvider_Dispose_m6846,
	RSACryptoServiceProvider_OnKeyGenerated_m6847,
	RSAPKCS1KeyExchangeFormatter__ctor_m3358,
	RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m6848,
	RSAPKCS1KeyExchangeFormatter_SetRSAKey_m6849,
	RSAPKCS1SignatureDeformatter__ctor_m6850,
	RSAPKCS1SignatureDeformatter__ctor_m3319,
	RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m6851,
	RSAPKCS1SignatureDeformatter_SetKey_m6852,
	RSAPKCS1SignatureDeformatter_VerifySignature_m6853,
	RSAPKCS1SignatureFormatter__ctor_m6854,
	RSAPKCS1SignatureFormatter_CreateSignature_m6855,
	RSAPKCS1SignatureFormatter_SetHashAlgorithm_m6856,
	RSAPKCS1SignatureFormatter_SetKey_m6857,
	RandomNumberGenerator__ctor_m6858,
	RandomNumberGenerator_Create_m2455,
	RandomNumberGenerator_Create_m6859,
	Rijndael__ctor_m6860,
	Rijndael_Create_m3336,
	Rijndael_Create_m6861,
	RijndaelManaged__ctor_m6862,
	RijndaelManaged_GenerateIV_m6863,
	RijndaelManaged_GenerateKey_m6864,
	RijndaelManaged_CreateDecryptor_m6865,
	RijndaelManaged_CreateEncryptor_m6866,
	RijndaelTransform__ctor_m6867,
	RijndaelTransform__cctor_m6868,
	RijndaelTransform_Clear_m6869,
	RijndaelTransform_ECB_m6870,
	RijndaelTransform_SubByte_m6871,
	RijndaelTransform_Encrypt128_m6872,
	RijndaelTransform_Encrypt192_m6873,
	RijndaelTransform_Encrypt256_m6874,
	RijndaelTransform_Decrypt128_m6875,
	RijndaelTransform_Decrypt192_m6876,
	RijndaelTransform_Decrypt256_m6877,
	RijndaelManagedTransform__ctor_m6878,
	RijndaelManagedTransform_System_IDisposable_Dispose_m6879,
	RijndaelManagedTransform_get_CanReuseTransform_m6880,
	RijndaelManagedTransform_TransformBlock_m6881,
	RijndaelManagedTransform_TransformFinalBlock_m6882,
	SHA1__ctor_m6883,
	SHA1_Create_m2370,
	SHA1_Create_m6884,
	SHA1Internal__ctor_m6885,
	SHA1Internal_HashCore_m6886,
	SHA1Internal_HashFinal_m6887,
	SHA1Internal_Initialize_m6888,
	SHA1Internal_ProcessBlock_m6889,
	SHA1Internal_InitialiseBuff_m6890,
	SHA1Internal_FillBuff_m6891,
	SHA1Internal_ProcessFinalBlock_m6892,
	SHA1Internal_AddLength_m6893,
	SHA1CryptoServiceProvider__ctor_m6894,
	SHA1CryptoServiceProvider_Finalize_m6895,
	SHA1CryptoServiceProvider_Dispose_m6896,
	SHA1CryptoServiceProvider_HashCore_m6897,
	SHA1CryptoServiceProvider_HashFinal_m6898,
	SHA1CryptoServiceProvider_Initialize_m6899,
	SHA1Managed__ctor_m6900,
	SHA1Managed_HashCore_m6901,
	SHA1Managed_HashFinal_m6902,
	SHA1Managed_Initialize_m6903,
	SHA256__ctor_m6904,
	SHA256_Create_m3316,
	SHA256_Create_m6905,
	SHA256Managed__ctor_m6906,
	SHA256Managed_HashCore_m6907,
	SHA256Managed_HashFinal_m6908,
	SHA256Managed_Initialize_m6909,
	SHA256Managed_ProcessBlock_m6910,
	SHA256Managed_ProcessFinalBlock_m6911,
	SHA256Managed_AddLength_m6912,
	SHA384__ctor_m6913,
	SHA384Managed__ctor_m6914,
	SHA384Managed_Initialize_m6915,
	SHA384Managed_Initialize_m6916,
	SHA384Managed_HashCore_m6917,
	SHA384Managed_HashFinal_m6918,
	SHA384Managed_update_m6919,
	SHA384Managed_processWord_m6920,
	SHA384Managed_unpackWord_m6921,
	SHA384Managed_adjustByteCounts_m6922,
	SHA384Managed_processLength_m6923,
	SHA384Managed_processBlock_m6924,
	SHA512__ctor_m6925,
	SHA512Managed__ctor_m6926,
	SHA512Managed_Initialize_m6927,
	SHA512Managed_Initialize_m6928,
	SHA512Managed_HashCore_m6929,
	SHA512Managed_HashFinal_m6930,
	SHA512Managed_update_m6931,
	SHA512Managed_processWord_m6932,
	SHA512Managed_unpackWord_m6933,
	SHA512Managed_adjustByteCounts_m6934,
	SHA512Managed_processLength_m6935,
	SHA512Managed_processBlock_m6936,
	SHA512Managed_rotateRight_m6937,
	SHA512Managed_Ch_m6938,
	SHA512Managed_Maj_m6939,
	SHA512Managed_Sum0_m6940,
	SHA512Managed_Sum1_m6941,
	SHA512Managed_Sigma0_m6942,
	SHA512Managed_Sigma1_m6943,
	SHAConstants__cctor_m6944,
	SignatureDescription__ctor_m6945,
	SignatureDescription_set_DeformatterAlgorithm_m6946,
	SignatureDescription_set_DigestAlgorithm_m6947,
	SignatureDescription_set_FormatterAlgorithm_m6948,
	SignatureDescription_set_KeyAlgorithm_m6949,
	DSASignatureDescription__ctor_m6950,
	RSAPKCS1SHA1SignatureDescription__ctor_m6951,
	SymmetricAlgorithm__ctor_m2461,
	SymmetricAlgorithm_System_IDisposable_Dispose_m6952,
	SymmetricAlgorithm_Finalize_m3294,
	SymmetricAlgorithm_Clear_m3310,
	SymmetricAlgorithm_Dispose_m2469,
	SymmetricAlgorithm_get_BlockSize_m6953,
	SymmetricAlgorithm_set_BlockSize_m6954,
	SymmetricAlgorithm_get_FeedbackSize_m6955,
	SymmetricAlgorithm_get_IV_m2463,
	SymmetricAlgorithm_set_IV_m2464,
	SymmetricAlgorithm_get_Key_m2465,
	SymmetricAlgorithm_set_Key_m2466,
	SymmetricAlgorithm_get_KeySize_m2467,
	SymmetricAlgorithm_set_KeySize_m2468,
	SymmetricAlgorithm_get_LegalKeySizes_m6956,
	SymmetricAlgorithm_get_Mode_m6957,
	SymmetricAlgorithm_set_Mode_m6958,
	SymmetricAlgorithm_get_Padding_m6959,
	SymmetricAlgorithm_set_Padding_m6960,
	SymmetricAlgorithm_CreateDecryptor_m6961,
	SymmetricAlgorithm_CreateEncryptor_m6962,
	SymmetricAlgorithm_Create_m3309,
	ToBase64Transform_System_IDisposable_Dispose_m6963,
	ToBase64Transform_Finalize_m6964,
	ToBase64Transform_get_CanReuseTransform_m6965,
	ToBase64Transform_get_InputBlockSize_m6966,
	ToBase64Transform_get_OutputBlockSize_m6967,
	ToBase64Transform_Dispose_m6968,
	ToBase64Transform_TransformBlock_m6969,
	ToBase64Transform_InternalTransformBlock_m6970,
	ToBase64Transform_TransformFinalBlock_m6971,
	ToBase64Transform_InternalTransformFinalBlock_m6972,
	TripleDES__ctor_m6973,
	TripleDES_get_Key_m6974,
	TripleDES_set_Key_m6975,
	TripleDES_IsWeakKey_m6976,
	TripleDES_Create_m3335,
	TripleDES_Create_m6977,
	TripleDESCryptoServiceProvider__ctor_m6978,
	TripleDESCryptoServiceProvider_GenerateIV_m6979,
	TripleDESCryptoServiceProvider_GenerateKey_m6980,
	TripleDESCryptoServiceProvider_CreateDecryptor_m6981,
	TripleDESCryptoServiceProvider_CreateEncryptor_m6982,
	TripleDESTransform__ctor_m6983,
	TripleDESTransform_ECB_m6984,
	TripleDESTransform_GetStrongKey_m6985,
	SecurityPermission__ctor_m6986,
	SecurityPermission_set_Flags_m6987,
	SecurityPermission_IsUnrestricted_m6988,
	SecurityPermission_IsSubsetOf_m6989,
	SecurityPermission_ToXml_m6990,
	SecurityPermission_IsEmpty_m6991,
	SecurityPermission_Cast_m6992,
	StrongNamePublicKeyBlob_Equals_m6993,
	StrongNamePublicKeyBlob_GetHashCode_m6994,
	StrongNamePublicKeyBlob_ToString_m6995,
	ApplicationTrust__ctor_m6996,
	EvidenceEnumerator__ctor_m6997,
	EvidenceEnumerator_MoveNext_m6998,
	EvidenceEnumerator_get_Current_m6999,
	Evidence__ctor_m7000,
	Evidence_get_Count_m7001,
	Evidence_get_SyncRoot_m7002,
	Evidence_get_HostEvidenceList_m7003,
	Evidence_get_AssemblyEvidenceList_m7004,
	Evidence_CopyTo_m7005,
	Evidence_Equals_m7006,
	Evidence_GetEnumerator_m7007,
	Evidence_GetHashCode_m7008,
	Hash__ctor_m7009,
	Hash__ctor_m7010,
	Hash_GetObjectData_m7011,
	Hash_ToString_m7012,
	Hash_GetData_m7013,
	StrongName_get_Name_m7014,
	StrongName_get_PublicKey_m7015,
	StrongName_get_Version_m7016,
	StrongName_Equals_m7017,
	StrongName_GetHashCode_m7018,
	StrongName_ToString_m7019,
	WindowsIdentity__ctor_m7020,
	WindowsIdentity__cctor_m7021,
	WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7022,
	WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m7023,
	WindowsIdentity_Dispose_m7024,
	WindowsIdentity_GetCurrentToken_m7025,
	WindowsIdentity_GetTokenName_m7026,
	CodeAccessPermission__ctor_m7027,
	CodeAccessPermission_Equals_m7028,
	CodeAccessPermission_GetHashCode_m7029,
	CodeAccessPermission_ToString_m7030,
	CodeAccessPermission_Element_m7031,
	CodeAccessPermission_ThrowInvalidPermission_m7032,
	PermissionSet__ctor_m7033,
	PermissionSet__ctor_m7034,
	PermissionSet_set_DeclarativeSecurity_m7035,
	PermissionSet_CreateFromBinaryFormat_m7036,
	SecurityContext__ctor_m7037,
	SecurityContext__ctor_m7038,
	SecurityContext_Capture_m7039,
	SecurityContext_get_FlowSuppressed_m7040,
	SecurityContext_get_CompressedStack_m7041,
	SecurityAttribute__ctor_m7042,
	SecurityAttribute_get_Name_m7043,
	SecurityAttribute_get_Value_m7044,
	SecurityElement__ctor_m7045,
	SecurityElement__ctor_m7046,
	SecurityElement__cctor_m7047,
	SecurityElement_get_Children_m7048,
	SecurityElement_get_Tag_m7049,
	SecurityElement_set_Text_m7050,
	SecurityElement_AddAttribute_m7051,
	SecurityElement_AddChild_m7052,
	SecurityElement_Escape_m7053,
	SecurityElement_Unescape_m7054,
	SecurityElement_IsValidAttributeName_m7055,
	SecurityElement_IsValidAttributeValue_m7056,
	SecurityElement_IsValidTag_m7057,
	SecurityElement_IsValidText_m7058,
	SecurityElement_SearchForChildByTag_m7059,
	SecurityElement_ToString_m7060,
	SecurityElement_ToXml_m7061,
	SecurityElement_GetAttribute_m7062,
	SecurityException__ctor_m7063,
	SecurityException__ctor_m7064,
	SecurityException__ctor_m7065,
	SecurityException_get_Demanded_m7066,
	SecurityException_get_FirstPermissionThatFailed_m7067,
	SecurityException_get_PermissionState_m7068,
	SecurityException_get_PermissionType_m7069,
	SecurityException_get_GrantedSet_m7070,
	SecurityException_get_RefusedSet_m7071,
	SecurityException_GetObjectData_m7072,
	SecurityException_ToString_m7073,
	SecurityFrame__ctor_m7074,
	SecurityFrame__GetSecurityStack_m7075,
	SecurityFrame_InitFromRuntimeFrame_m7076,
	SecurityFrame_get_Assembly_m7077,
	SecurityFrame_get_Domain_m7078,
	SecurityFrame_ToString_m7079,
	SecurityFrame_GetStack_m7080,
	SecurityManager__cctor_m7081,
	SecurityManager_get_SecurityEnabled_m7082,
	SecurityManager_Decode_m7083,
	SecurityManager_Decode_m7084,
	SecuritySafeCriticalAttribute__ctor_m7085,
	SuppressUnmanagedCodeSecurityAttribute__ctor_m7086,
	UnverifiableCodeAttribute__ctor_m7087,
	ASCIIEncoding__ctor_m7088,
	ASCIIEncoding_GetByteCount_m7089,
	ASCIIEncoding_GetByteCount_m7090,
	ASCIIEncoding_GetBytes_m7091,
	ASCIIEncoding_GetBytes_m7092,
	ASCIIEncoding_GetBytes_m7093,
	ASCIIEncoding_GetBytes_m7094,
	ASCIIEncoding_GetCharCount_m7095,
	ASCIIEncoding_GetChars_m7096,
	ASCIIEncoding_GetChars_m7097,
	ASCIIEncoding_GetMaxByteCount_m7098,
	ASCIIEncoding_GetMaxCharCount_m7099,
	ASCIIEncoding_GetString_m7100,
	ASCIIEncoding_GetBytes_m7101,
	ASCIIEncoding_GetByteCount_m7102,
	ASCIIEncoding_GetDecoder_m7103,
	Decoder__ctor_m7104,
	Decoder_set_Fallback_m7105,
	Decoder_get_FallbackBuffer_m7106,
	DecoderExceptionFallback__ctor_m7107,
	DecoderExceptionFallback_CreateFallbackBuffer_m7108,
	DecoderExceptionFallback_Equals_m7109,
	DecoderExceptionFallback_GetHashCode_m7110,
	DecoderExceptionFallbackBuffer__ctor_m7111,
	DecoderExceptionFallbackBuffer_get_Remaining_m7112,
	DecoderExceptionFallbackBuffer_Fallback_m7113,
	DecoderExceptionFallbackBuffer_GetNextChar_m7114,
	DecoderFallback__ctor_m7115,
	DecoderFallback__cctor_m7116,
	DecoderFallback_get_ExceptionFallback_m7117,
	DecoderFallback_get_ReplacementFallback_m7118,
	DecoderFallback_get_StandardSafeFallback_m7119,
	DecoderFallbackBuffer__ctor_m7120,
	DecoderFallbackBuffer_Reset_m7121,
	DecoderFallbackException__ctor_m7122,
	DecoderFallbackException__ctor_m7123,
	DecoderFallbackException__ctor_m7124,
	DecoderReplacementFallback__ctor_m7125,
	DecoderReplacementFallback__ctor_m7126,
	DecoderReplacementFallback_get_DefaultString_m7127,
	DecoderReplacementFallback_CreateFallbackBuffer_m7128,
	DecoderReplacementFallback_Equals_m7129,
	DecoderReplacementFallback_GetHashCode_m7130,
	DecoderReplacementFallbackBuffer__ctor_m7131,
	DecoderReplacementFallbackBuffer_get_Remaining_m7132,
	DecoderReplacementFallbackBuffer_Fallback_m7133,
	DecoderReplacementFallbackBuffer_GetNextChar_m7134,
	DecoderReplacementFallbackBuffer_Reset_m7135,
	EncoderExceptionFallback__ctor_m7136,
	EncoderExceptionFallback_CreateFallbackBuffer_m7137,
	EncoderExceptionFallback_Equals_m7138,
	EncoderExceptionFallback_GetHashCode_m7139,
	EncoderExceptionFallbackBuffer__ctor_m7140,
	EncoderExceptionFallbackBuffer_get_Remaining_m7141,
	EncoderExceptionFallbackBuffer_Fallback_m7142,
	EncoderExceptionFallbackBuffer_Fallback_m7143,
	EncoderExceptionFallbackBuffer_GetNextChar_m7144,
	EncoderFallback__ctor_m7145,
	EncoderFallback__cctor_m7146,
	EncoderFallback_get_ExceptionFallback_m7147,
	EncoderFallback_get_ReplacementFallback_m7148,
	EncoderFallback_get_StandardSafeFallback_m7149,
	EncoderFallbackBuffer__ctor_m7150,
	EncoderFallbackException__ctor_m7151,
	EncoderFallbackException__ctor_m7152,
	EncoderFallbackException__ctor_m7153,
	EncoderFallbackException__ctor_m7154,
	EncoderReplacementFallback__ctor_m7155,
	EncoderReplacementFallback__ctor_m7156,
	EncoderReplacementFallback_get_DefaultString_m7157,
	EncoderReplacementFallback_CreateFallbackBuffer_m7158,
	EncoderReplacementFallback_Equals_m7159,
	EncoderReplacementFallback_GetHashCode_m7160,
	EncoderReplacementFallbackBuffer__ctor_m7161,
	EncoderReplacementFallbackBuffer_get_Remaining_m7162,
	EncoderReplacementFallbackBuffer_Fallback_m7163,
	EncoderReplacementFallbackBuffer_Fallback_m7164,
	EncoderReplacementFallbackBuffer_Fallback_m7165,
	EncoderReplacementFallbackBuffer_GetNextChar_m7166,
	ForwardingDecoder__ctor_m7167,
	ForwardingDecoder_GetChars_m7168,
	Encoding__ctor_m7169,
	Encoding__ctor_m7170,
	Encoding__cctor_m7171,
	Encoding___m7172,
	Encoding_get_IsReadOnly_m7173,
	Encoding_get_DecoderFallback_m7174,
	Encoding_set_DecoderFallback_m7175,
	Encoding_get_EncoderFallback_m7176,
	Encoding_SetFallbackInternal_m7177,
	Encoding_Equals_m7178,
	Encoding_GetByteCount_m7179,
	Encoding_GetByteCount_m7180,
	Encoding_GetBytes_m7181,
	Encoding_GetBytes_m7182,
	Encoding_GetBytes_m7183,
	Encoding_GetBytes_m7184,
	Encoding_GetChars_m7185,
	Encoding_GetDecoder_m7186,
	Encoding_InvokeI18N_m7187,
	Encoding_GetEncoding_m7188,
	Encoding_GetEncoding_m1207,
	Encoding_GetHashCode_m7189,
	Encoding_GetPreamble_m7190,
	Encoding_GetString_m7191,
	Encoding_GetString_m7192,
	Encoding_get_HeaderName_m7193,
	Encoding_get_WebName_m7194,
	Encoding_get_ASCII_m1199,
	Encoding_get_BigEndianUnicode_m3307,
	Encoding_InternalCodePage_m7195,
	Encoding_get_Default_m7196,
	Encoding_get_ISOLatin1_m7197,
	Encoding_get_UTF7_m3312,
	Encoding_get_UTF8_m1208,
	Encoding_get_UTF8Unmarked_m7198,
	Encoding_get_UTF8UnmarkedUnsafe_m7199,
	Encoding_get_Unicode_m7200,
	Encoding_get_UTF32_m7201,
	Encoding_get_BigEndianUTF32_m7202,
	Encoding_GetByteCount_m7203,
	Encoding_GetBytes_m7204,
	Latin1Encoding__ctor_m7205,
	Latin1Encoding_GetByteCount_m7206,
	Latin1Encoding_GetByteCount_m7207,
	Latin1Encoding_GetBytes_m7208,
	Latin1Encoding_GetBytes_m7209,
	Latin1Encoding_GetBytes_m7210,
	Latin1Encoding_GetBytes_m7211,
	Latin1Encoding_GetCharCount_m7212,
	Latin1Encoding_GetChars_m7213,
	Latin1Encoding_GetMaxByteCount_m7214,
	Latin1Encoding_GetMaxCharCount_m7215,
	Latin1Encoding_GetString_m7216,
	Latin1Encoding_GetString_m7217,
	Latin1Encoding_get_HeaderName_m7218,
	Latin1Encoding_get_WebName_m7219,
	StringBuilder__ctor_m7220,
	StringBuilder__ctor_m7221,
	StringBuilder__ctor_m2264,
	StringBuilder__ctor_m1268,
	StringBuilder__ctor_m2300,
	StringBuilder__ctor_m2251,
	StringBuilder__ctor_m7222,
	StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m7223,
	StringBuilder_get_Capacity_m7224,
	StringBuilder_set_Capacity_m7225,
	StringBuilder_get_Length_m2363,
	StringBuilder_set_Length_m2405,
	StringBuilder_get_Chars_m7226,
	StringBuilder_set_Chars_m7227,
	StringBuilder_ToString_m1269,
	StringBuilder_ToString_m7228,
	StringBuilder_Remove_m7229,
	StringBuilder_Replace_m7230,
	StringBuilder_Replace_m7231,
	StringBuilder_Append_m1274,
	StringBuilder_Append_m2311,
	StringBuilder_Append_m2266,
	StringBuilder_Append_m2252,
	StringBuilder_Append_m1270,
	StringBuilder_Append_m7232,
	StringBuilder_Append_m7233,
	StringBuilder_Append_m2375,
	StringBuilder_AppendFormat_m3289,
	StringBuilder_AppendFormat_m7234,
	StringBuilder_AppendFormat_m2265,
	StringBuilder_AppendFormat_m2331,
	StringBuilder_AppendFormat_m2334,
	StringBuilder_Insert_m7235,
	StringBuilder_Insert_m7236,
	StringBuilder_Insert_m7237,
	StringBuilder_InternalEnsureCapacity_m7238,
	UTF32Decoder__ctor_m7239,
	UTF32Decoder_GetChars_m7240,
	UTF32Encoding__ctor_m7241,
	UTF32Encoding__ctor_m7242,
	UTF32Encoding__ctor_m7243,
	UTF32Encoding_GetByteCount_m7244,
	UTF32Encoding_GetBytes_m7245,
	UTF32Encoding_GetCharCount_m7246,
	UTF32Encoding_GetChars_m7247,
	UTF32Encoding_GetMaxByteCount_m7248,
	UTF32Encoding_GetMaxCharCount_m7249,
	UTF32Encoding_GetDecoder_m7250,
	UTF32Encoding_GetPreamble_m7251,
	UTF32Encoding_Equals_m7252,
	UTF32Encoding_GetHashCode_m7253,
	UTF32Encoding_GetByteCount_m7254,
	UTF32Encoding_GetByteCount_m7255,
	UTF32Encoding_GetBytes_m7256,
	UTF32Encoding_GetBytes_m7257,
	UTF32Encoding_GetString_m7258,
	UTF7Decoder__ctor_m7259,
	UTF7Decoder_GetChars_m7260,
	UTF7Encoding__ctor_m7261,
	UTF7Encoding__ctor_m7262,
	UTF7Encoding__cctor_m7263,
	UTF7Encoding_GetHashCode_m7264,
	UTF7Encoding_Equals_m7265,
	UTF7Encoding_InternalGetByteCount_m7266,
	UTF7Encoding_GetByteCount_m7267,
	UTF7Encoding_InternalGetBytes_m7268,
	UTF7Encoding_GetBytes_m7269,
	UTF7Encoding_InternalGetCharCount_m7270,
	UTF7Encoding_GetCharCount_m7271,
	UTF7Encoding_InternalGetChars_m7272,
	UTF7Encoding_GetChars_m7273,
	UTF7Encoding_GetMaxByteCount_m7274,
	UTF7Encoding_GetMaxCharCount_m7275,
	UTF7Encoding_GetDecoder_m7276,
	UTF7Encoding_GetByteCount_m7277,
	UTF7Encoding_GetByteCount_m7278,
	UTF7Encoding_GetBytes_m7279,
	UTF7Encoding_GetBytes_m7280,
	UTF7Encoding_GetString_m7281,
	UTF8Decoder__ctor_m7282,
	UTF8Decoder_GetChars_m7283,
	UTF8Encoding__ctor_m7284,
	UTF8Encoding__ctor_m7285,
	UTF8Encoding__ctor_m7286,
	UTF8Encoding_InternalGetByteCount_m7287,
	UTF8Encoding_InternalGetByteCount_m7288,
	UTF8Encoding_GetByteCount_m7289,
	UTF8Encoding_GetByteCount_m7290,
	UTF8Encoding_InternalGetBytes_m7291,
	UTF8Encoding_InternalGetBytes_m7292,
	UTF8Encoding_GetBytes_m7293,
	UTF8Encoding_GetBytes_m7294,
	UTF8Encoding_GetBytes_m7295,
	UTF8Encoding_InternalGetCharCount_m7296,
	UTF8Encoding_InternalGetCharCount_m7297,
	UTF8Encoding_Fallback_m7298,
	UTF8Encoding_Fallback_m7299,
	UTF8Encoding_GetCharCount_m7300,
	UTF8Encoding_InternalGetChars_m7301,
	UTF8Encoding_InternalGetChars_m7302,
	UTF8Encoding_GetChars_m7303,
	UTF8Encoding_GetMaxByteCount_m7304,
	UTF8Encoding_GetMaxCharCount_m7305,
	UTF8Encoding_GetDecoder_m7306,
	UTF8Encoding_GetPreamble_m7307,
	UTF8Encoding_Equals_m7308,
	UTF8Encoding_GetHashCode_m7309,
	UTF8Encoding_GetByteCount_m7310,
	UTF8Encoding_GetString_m7311,
	UnicodeDecoder__ctor_m7312,
	UnicodeDecoder_GetChars_m7313,
	UnicodeEncoding__ctor_m7314,
	UnicodeEncoding__ctor_m7315,
	UnicodeEncoding__ctor_m7316,
	UnicodeEncoding_GetByteCount_m7317,
	UnicodeEncoding_GetByteCount_m7318,
	UnicodeEncoding_GetByteCount_m7319,
	UnicodeEncoding_GetBytes_m7320,
	UnicodeEncoding_GetBytes_m7321,
	UnicodeEncoding_GetBytes_m7322,
	UnicodeEncoding_GetBytesInternal_m7323,
	UnicodeEncoding_GetCharCount_m7324,
	UnicodeEncoding_GetChars_m7325,
	UnicodeEncoding_GetString_m7326,
	UnicodeEncoding_GetCharsInternal_m7327,
	UnicodeEncoding_GetMaxByteCount_m7328,
	UnicodeEncoding_GetMaxCharCount_m7329,
	UnicodeEncoding_GetDecoder_m7330,
	UnicodeEncoding_GetPreamble_m7331,
	UnicodeEncoding_Equals_m7332,
	UnicodeEncoding_GetHashCode_m7333,
	UnicodeEncoding_CopyChars_m7334,
	CompressedStack__ctor_m7335,
	CompressedStack__ctor_m7336,
	CompressedStack_CreateCopy_m7337,
	CompressedStack_Capture_m7338,
	CompressedStack_GetObjectData_m7339,
	CompressedStack_IsEmpty_m7340,
	EventWaitHandle__ctor_m7341,
	EventWaitHandle_IsManualReset_m7342,
	EventWaitHandle_Reset_m3346,
	EventWaitHandle_Set_m3344,
	ExecutionContext__ctor_m7343,
	ExecutionContext__ctor_m7344,
	ExecutionContext__ctor_m7345,
	ExecutionContext_Capture_m7346,
	ExecutionContext_GetObjectData_m7347,
	ExecutionContext_get_SecurityContext_m7348,
	ExecutionContext_set_SecurityContext_m7349,
	ExecutionContext_get_FlowSuppressed_m7350,
	ExecutionContext_IsFlowSuppressed_m7351,
	Interlocked_CompareExchange_m7352,
	ManualResetEvent__ctor_m3343,
	Monitor_Enter_m2259,
	Monitor_Exit_m2260,
	Monitor_Monitor_pulse_m7353,
	Monitor_Monitor_test_synchronised_m7354,
	Monitor_Pulse_m7355,
	Monitor_Monitor_wait_m7356,
	Monitor_Wait_m7357,
	Mutex__ctor_m7358,
	Mutex_CreateMutex_internal_m7359,
	Mutex_ReleaseMutex_internal_m7360,
	Mutex_ReleaseMutex_m7361,
	NativeEventCalls_CreateEvent_internal_m7362,
	NativeEventCalls_SetEvent_internal_m7363,
	NativeEventCalls_ResetEvent_internal_m7364,
	NativeEventCalls_CloseEvent_internal_m7365,
	SynchronizationLockException__ctor_m7366,
	SynchronizationLockException__ctor_m7367,
	SynchronizationLockException__ctor_m7368,
	Thread__ctor_m7369,
	Thread__cctor_m7370,
	Thread_get_CurrentContext_m7371,
	Thread_CurrentThread_internal_m7372,
	Thread_get_CurrentThread_m7373,
	Thread_GetDomainID_m7374,
	Thread_Thread_internal_m7375,
	Thread_Thread_init_m7376,
	Thread_GetCachedCurrentCulture_m7377,
	Thread_GetSerializedCurrentCulture_m7378,
	Thread_SetCachedCurrentCulture_m7379,
	Thread_GetCachedCurrentUICulture_m7380,
	Thread_GetSerializedCurrentUICulture_m7381,
	Thread_SetCachedCurrentUICulture_m7382,
	Thread_get_CurrentCulture_m7383,
	Thread_get_CurrentUICulture_m7384,
	Thread_set_IsBackground_m7385,
	Thread_SetName_internal_m7386,
	Thread_set_Name_m7387,
	Thread_Start_m7388,
	Thread_Thread_free_internal_m7389,
	Thread_Finalize_m7390,
	Thread_SetState_m7391,
	Thread_ClrState_m7392,
	Thread_GetNewManagedId_m7393,
	Thread_GetNewManagedId_internal_m7394,
	Thread_get_ExecutionContext_m7395,
	Thread_get_ManagedThreadId_m7396,
	Thread_GetHashCode_m7397,
	Thread_GetCompressedStack_m7398,
	ThreadAbortException__ctor_m7399,
	ThreadAbortException__ctor_m7400,
	ThreadInterruptedException__ctor_m7401,
	ThreadInterruptedException__ctor_m7402,
	ThreadPool_QueueUserWorkItem_m7403,
	ThreadStateException__ctor_m7404,
	ThreadStateException__ctor_m7405,
	TimerComparer__ctor_m7406,
	TimerComparer_Compare_m7407,
	Scheduler__ctor_m7408,
	Scheduler__cctor_m7409,
	Scheduler_get_Instance_m7410,
	Scheduler_Remove_m7411,
	Scheduler_Change_m7412,
	Scheduler_Add_m7413,
	Scheduler_InternalRemove_m7414,
	Scheduler_SchedulerThread_m7415,
	Scheduler_ShrinkIfNeeded_m7416,
	Timer__cctor_m7417,
	Timer_Change_m7418,
	Timer_Dispose_m7419,
	Timer_Change_m7420,
	WaitHandle__ctor_m7421,
	WaitHandle__cctor_m7422,
	WaitHandle_System_IDisposable_Dispose_m7423,
	WaitHandle_get_Handle_m7424,
	WaitHandle_set_Handle_m7425,
	WaitHandle_WaitOne_internal_m7426,
	WaitHandle_Dispose_m7427,
	WaitHandle_WaitOne_m7428,
	WaitHandle_WaitOne_m7429,
	WaitHandle_CheckDisposed_m7430,
	WaitHandle_Finalize_m7431,
	AccessViolationException__ctor_m7432,
	AccessViolationException__ctor_m7433,
	ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m7434,
	ActivationContext_Finalize_m7435,
	ActivationContext_Dispose_m7436,
	ActivationContext_Dispose_m7437,
	Activator_CreateInstance_m7438,
	Activator_CreateInstance_m7439,
	Activator_CreateInstance_m7440,
	Activator_CreateInstance_m7441,
	Activator_CreateInstance_m2280,
	Activator_CheckType_m7442,
	Activator_CheckAbstractType_m7443,
	Activator_CreateInstanceInternal_m7444,
	AppDomain_getFriendlyName_m7445,
	AppDomain_getCurDomain_m7446,
	AppDomain_get_CurrentDomain_m7447,
	AppDomain_LoadAssembly_m7448,
	AppDomain_Load_m7449,
	AppDomain_Load_m7450,
	AppDomain_InternalGetContext_m7451,
	AppDomain_InternalGetDefaultContext_m7452,
	AppDomain_InternalGetProcessGuid_m7453,
	AppDomain_GetProcessGuid_m7454,
	AppDomain_ToString_m7455,
	AppDomain_DoTypeResolve_m7456,
	AppDomainSetup__ctor_m7457,
	ApplicationException__ctor_m7458,
	ApplicationException__ctor_m7459,
	ApplicationException__ctor_m7460,
	ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m7461,
	ApplicationIdentity_ToString_m7462,
	ArgumentException__ctor_m7463,
	ArgumentException__ctor_m1162,
	ArgumentException__ctor_m2342,
	ArgumentException__ctor_m2237,
	ArgumentException__ctor_m7464,
	ArgumentException__ctor_m7465,
	ArgumentException_get_ParamName_m7466,
	ArgumentException_get_Message_m7467,
	ArgumentException_GetObjectData_m7468,
	ArgumentNullException__ctor_m7469,
	ArgumentNullException__ctor_m1266,
	ArgumentNullException__ctor_m2234,
	ArgumentNullException__ctor_m7470,
	ArgumentOutOfRangeException__ctor_m2374,
	ArgumentOutOfRangeException__ctor_m2239,
	ArgumentOutOfRangeException__ctor_m1275,
	ArgumentOutOfRangeException__ctor_m7471,
	ArgumentOutOfRangeException__ctor_m7472,
	ArgumentOutOfRangeException_get_Message_m7473,
	ArgumentOutOfRangeException_GetObjectData_m7474,
	ArithmeticException__ctor_m7475,
	ArithmeticException__ctor_m3288,
	ArithmeticException__ctor_m7476,
	ArrayTypeMismatchException__ctor_m7477,
	ArrayTypeMismatchException__ctor_m7478,
	ArrayTypeMismatchException__ctor_m7479,
	BitConverter__cctor_m7480,
	BitConverter_AmILittleEndian_m7481,
	BitConverter_DoubleWordsAreSwapped_m7482,
	BitConverter_DoubleToInt64Bits_m7483,
	BitConverter_GetBytes_m7484,
	BitConverter_GetBytes_m7485,
	BitConverter_PutBytes_m7486,
	BitConverter_ToInt64_m7487,
	BitConverter_ToString_m3341,
	BitConverter_ToString_m7488,
	Buffer_ByteLength_m7489,
	Buffer_BlockCopy_m2288,
	Buffer_ByteLengthInternal_m7490,
	Buffer_BlockCopyInternal_m7491,
	CharEnumerator__ctor_m7492,
	CharEnumerator_System_Collections_IEnumerator_get_Current_m7493,
	CharEnumerator_System_IDisposable_Dispose_m7494,
	CharEnumerator_get_Current_m7495,
	CharEnumerator_MoveNext_m7496,
	Console__cctor_m7497,
	Console_SetEncodings_m7498,
	Console_get_Error_m2394,
	Console_Open_m7499,
	Console_OpenStandardError_m7500,
	Console_OpenStandardInput_m7501,
	Console_OpenStandardOutput_m7502,
	ContextBoundObject__ctor_m7503,
	Convert__cctor_m7504,
	Convert_InternalFromBase64String_m7505,
	Convert_FromBase64String_m3320,
	Convert_ToBase64String_m1245,
	Convert_ToBase64String_m7506,
	Convert_ToBoolean_m7507,
	Convert_ToBoolean_m7508,
	Convert_ToBoolean_m7509,
	Convert_ToBoolean_m7510,
	Convert_ToBoolean_m7511,
	Convert_ToBoolean_m7512,
	Convert_ToBoolean_m7513,
	Convert_ToBoolean_m7514,
	Convert_ToBoolean_m7515,
	Convert_ToBoolean_m7516,
	Convert_ToBoolean_m7517,
	Convert_ToBoolean_m7518,
	Convert_ToBoolean_m1241,
	Convert_ToBoolean_m7519,
	Convert_ToByte_m7520,
	Convert_ToByte_m7521,
	Convert_ToByte_m7522,
	Convert_ToByte_m7523,
	Convert_ToByte_m7524,
	Convert_ToByte_m7525,
	Convert_ToByte_m7526,
	Convert_ToByte_m7527,
	Convert_ToByte_m7528,
	Convert_ToByte_m7529,
	Convert_ToByte_m7530,
	Convert_ToByte_m7531,
	Convert_ToByte_m7532,
	Convert_ToByte_m7533,
	Convert_ToByte_m7534,
	Convert_ToChar_m3321,
	Convert_ToChar_m7535,
	Convert_ToChar_m7536,
	Convert_ToChar_m7537,
	Convert_ToChar_m7538,
	Convert_ToChar_m7539,
	Convert_ToChar_m7540,
	Convert_ToChar_m7541,
	Convert_ToChar_m7542,
	Convert_ToChar_m7543,
	Convert_ToChar_m7544,
	Convert_ToDateTime_m7545,
	Convert_ToDateTime_m7546,
	Convert_ToDateTime_m7547,
	Convert_ToDateTime_m7548,
	Convert_ToDateTime_m7549,
	Convert_ToDateTime_m7550,
	Convert_ToDateTime_m7551,
	Convert_ToDateTime_m7552,
	Convert_ToDateTime_m7553,
	Convert_ToDateTime_m7554,
	Convert_ToDecimal_m7555,
	Convert_ToDecimal_m7556,
	Convert_ToDecimal_m7557,
	Convert_ToDecimal_m7558,
	Convert_ToDecimal_m7559,
	Convert_ToDecimal_m7560,
	Convert_ToDecimal_m7561,
	Convert_ToDecimal_m7562,
	Convert_ToDecimal_m7563,
	Convert_ToDecimal_m7564,
	Convert_ToDecimal_m7565,
	Convert_ToDecimal_m7566,
	Convert_ToDecimal_m7567,
	Convert_ToDouble_m7568,
	Convert_ToDouble_m7569,
	Convert_ToDouble_m7570,
	Convert_ToDouble_m7571,
	Convert_ToDouble_m7572,
	Convert_ToDouble_m7573,
	Convert_ToDouble_m7574,
	Convert_ToDouble_m7575,
	Convert_ToDouble_m7576,
	Convert_ToDouble_m7577,
	Convert_ToDouble_m7578,
	Convert_ToDouble_m7579,
	Convert_ToDouble_m7580,
	Convert_ToDouble_m1286,
	Convert_ToInt16_m7581,
	Convert_ToInt16_m7582,
	Convert_ToInt16_m7583,
	Convert_ToInt16_m7584,
	Convert_ToInt16_m7585,
	Convert_ToInt16_m7586,
	Convert_ToInt16_m7587,
	Convert_ToInt16_m7588,
	Convert_ToInt16_m7589,
	Convert_ToInt16_m7590,
	Convert_ToInt16_m3292,
	Convert_ToInt16_m7591,
	Convert_ToInt16_m7592,
	Convert_ToInt16_m7593,
	Convert_ToInt16_m7594,
	Convert_ToInt16_m7595,
	Convert_ToInt32_m7596,
	Convert_ToInt32_m7597,
	Convert_ToInt32_m7598,
	Convert_ToInt32_m7599,
	Convert_ToInt32_m7600,
	Convert_ToInt32_m7601,
	Convert_ToInt32_m7602,
	Convert_ToInt32_m7603,
	Convert_ToInt32_m7604,
	Convert_ToInt32_m7605,
	Convert_ToInt32_m7606,
	Convert_ToInt32_m7607,
	Convert_ToInt32_m7608,
	Convert_ToInt32_m1238,
	Convert_ToInt32_m3329,
	Convert_ToInt64_m7609,
	Convert_ToInt64_m7610,
	Convert_ToInt64_m7611,
	Convert_ToInt64_m7612,
	Convert_ToInt64_m7613,
	Convert_ToInt64_m7614,
	Convert_ToInt64_m7615,
	Convert_ToInt64_m7616,
	Convert_ToInt64_m7617,
	Convert_ToInt64_m7618,
	Convert_ToInt64_m7619,
	Convert_ToInt64_m7620,
	Convert_ToInt64_m7621,
	Convert_ToInt64_m7622,
	Convert_ToInt64_m7623,
	Convert_ToInt64_m7624,
	Convert_ToInt64_m7625,
	Convert_ToSByte_m7626,
	Convert_ToSByte_m7627,
	Convert_ToSByte_m7628,
	Convert_ToSByte_m7629,
	Convert_ToSByte_m7630,
	Convert_ToSByte_m7631,
	Convert_ToSByte_m7632,
	Convert_ToSByte_m7633,
	Convert_ToSByte_m7634,
	Convert_ToSByte_m7635,
	Convert_ToSByte_m7636,
	Convert_ToSByte_m7637,
	Convert_ToSByte_m7638,
	Convert_ToSByte_m7639,
	Convert_ToSingle_m7640,
	Convert_ToSingle_m7641,
	Convert_ToSingle_m7642,
	Convert_ToSingle_m7643,
	Convert_ToSingle_m7644,
	Convert_ToSingle_m7645,
	Convert_ToSingle_m7646,
	Convert_ToSingle_m7647,
	Convert_ToSingle_m7648,
	Convert_ToSingle_m7649,
	Convert_ToSingle_m7650,
	Convert_ToSingle_m7651,
	Convert_ToSingle_m7652,
	Convert_ToSingle_m7653,
	Convert_ToString_m7654,
	Convert_ToString_m7655,
	Convert_ToUInt16_m7656,
	Convert_ToUInt16_m7657,
	Convert_ToUInt16_m7658,
	Convert_ToUInt16_m7659,
	Convert_ToUInt16_m7660,
	Convert_ToUInt16_m7661,
	Convert_ToUInt16_m7662,
	Convert_ToUInt16_m7663,
	Convert_ToUInt16_m7664,
	Convert_ToUInt16_m7665,
	Convert_ToUInt16_m7666,
	Convert_ToUInt16_m7667,
	Convert_ToUInt16_m7668,
	Convert_ToUInt16_m1239,
	Convert_ToUInt16_m7669,
	Convert_ToUInt32_m7670,
	Convert_ToUInt32_m7671,
	Convert_ToUInt32_m7672,
	Convert_ToUInt32_m7673,
	Convert_ToUInt32_m7674,
	Convert_ToUInt32_m7675,
	Convert_ToUInt32_m7676,
	Convert_ToUInt32_m7677,
	Convert_ToUInt32_m7678,
	Convert_ToUInt32_m7679,
	Convert_ToUInt32_m7680,
	Convert_ToUInt32_m7681,
	Convert_ToUInt32_m7682,
	Convert_ToUInt32_m7683,
	Convert_ToUInt64_m7684,
	Convert_ToUInt64_m7685,
	Convert_ToUInt64_m7686,
	Convert_ToUInt64_m7687,
	Convert_ToUInt64_m7688,
	Convert_ToUInt64_m7689,
	Convert_ToUInt64_m7690,
	Convert_ToUInt64_m7691,
	Convert_ToUInt64_m7692,
	Convert_ToUInt64_m7693,
	Convert_ToUInt64_m7694,
	Convert_ToUInt64_m7695,
	Convert_ToUInt64_m7696,
	Convert_ToUInt64_m1240,
	Convert_ToUInt64_m7697,
	Convert_ChangeType_m7698,
	Convert_ToType_m7699,
	DBNull__ctor_m7700,
	DBNull__ctor_m7701,
	DBNull__cctor_m7702,
	DBNull_System_IConvertible_ToBoolean_m7703,
	DBNull_System_IConvertible_ToByte_m7704,
	DBNull_System_IConvertible_ToChar_m7705,
	DBNull_System_IConvertible_ToDateTime_m7706,
	DBNull_System_IConvertible_ToDecimal_m7707,
	DBNull_System_IConvertible_ToDouble_m7708,
	DBNull_System_IConvertible_ToInt16_m7709,
	DBNull_System_IConvertible_ToInt32_m7710,
	DBNull_System_IConvertible_ToInt64_m7711,
	DBNull_System_IConvertible_ToSByte_m7712,
	DBNull_System_IConvertible_ToSingle_m7713,
	DBNull_System_IConvertible_ToType_m7714,
	DBNull_System_IConvertible_ToUInt16_m7715,
	DBNull_System_IConvertible_ToUInt32_m7716,
	DBNull_System_IConvertible_ToUInt64_m7717,
	DBNull_GetObjectData_m7718,
	DBNull_ToString_m7719,
	DBNull_ToString_m7720,
	DateTime__ctor_m7721,
	DateTime__ctor_m7722,
	DateTime__ctor_m1311,
	DateTime__ctor_m7723,
	DateTime__ctor_m7724,
	DateTime__cctor_m7725,
	DateTime_System_IConvertible_ToBoolean_m7726,
	DateTime_System_IConvertible_ToByte_m7727,
	DateTime_System_IConvertible_ToChar_m7728,
	DateTime_System_IConvertible_ToDateTime_m7729,
	DateTime_System_IConvertible_ToDecimal_m7730,
	DateTime_System_IConvertible_ToDouble_m7731,
	DateTime_System_IConvertible_ToInt16_m7732,
	DateTime_System_IConvertible_ToInt32_m7733,
	DateTime_System_IConvertible_ToInt64_m7734,
	DateTime_System_IConvertible_ToSByte_m7735,
	DateTime_System_IConvertible_ToSingle_m7736,
	DateTime_System_IConvertible_ToType_m7737,
	DateTime_System_IConvertible_ToUInt16_m7738,
	DateTime_System_IConvertible_ToUInt32_m7739,
	DateTime_System_IConvertible_ToUInt64_m7740,
	DateTime_AbsoluteDays_m7741,
	DateTime_FromTicks_m7742,
	DateTime_get_Month_m7743,
	DateTime_get_Day_m7744,
	DateTime_get_DayOfWeek_m7745,
	DateTime_get_Hour_m7746,
	DateTime_get_Minute_m7747,
	DateTime_get_Second_m7748,
	DateTime_GetTimeMonotonic_m7749,
	DateTime_GetNow_m7750,
	DateTime_get_Now_m1179,
	DateTime_get_Ticks_m3342,
	DateTime_get_Today_m7751,
	DateTime_get_UtcNow_m3317,
	DateTime_get_Year_m7752,
	DateTime_get_Kind_m7753,
	DateTime_Add_m7754,
	DateTime_AddTicks_m7755,
	DateTime_AddMilliseconds_m2267,
	DateTime_AddSeconds_m1312,
	DateTime_Compare_m7756,
	DateTime_CompareTo_m7757,
	DateTime_CompareTo_m7758,
	DateTime_Equals_m7759,
	DateTime_FromBinary_m7760,
	DateTime_SpecifyKind_m7761,
	DateTime_DaysInMonth_m7762,
	DateTime_Equals_m7763,
	DateTime_CheckDateTimeKind_m7764,
	DateTime_GetHashCode_m7765,
	DateTime_IsLeapYear_m7766,
	DateTime_Parse_m7767,
	DateTime_Parse_m7768,
	DateTime_CoreParse_m7769,
	DateTime_YearMonthDayFormats_m7770,
	DateTime__ParseNumber_m7771,
	DateTime__ParseEnum_m7772,
	DateTime__ParseString_m7773,
	DateTime__ParseAmPm_m7774,
	DateTime__ParseTimeSeparator_m7775,
	DateTime__ParseDateSeparator_m7776,
	DateTime_IsLetter_m7777,
	DateTime__DoParse_m7778,
	DateTime_ParseExact_m3293,
	DateTime_ParseExact_m7779,
	DateTime_CheckStyle_m7780,
	DateTime_ParseExact_m7781,
	DateTime_Subtract_m7782,
	DateTime_ToString_m7783,
	DateTime_ToString_m7784,
	DateTime_ToString_m1299,
	DateTime_ToLocalTime_m2314,
	DateTime_ToUniversalTime_m1298,
	DateTime_op_Addition_m7785,
	DateTime_op_Equality_m7786,
	DateTime_op_GreaterThan_m2340,
	DateTime_op_GreaterThanOrEqual_m2268,
	DateTime_op_Inequality_m7787,
	DateTime_op_LessThan_m2339,
	DateTime_op_LessThanOrEqual_m2338,
	DateTime_op_Subtraction_m7788,
	DateTimeOffset__ctor_m7789,
	DateTimeOffset__ctor_m7790,
	DateTimeOffset__ctor_m7791,
	DateTimeOffset__ctor_m7792,
	DateTimeOffset__cctor_m7793,
	DateTimeOffset_System_IComparable_CompareTo_m7794,
	DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m7795,
	DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7796,
	DateTimeOffset_CompareTo_m7797,
	DateTimeOffset_Equals_m7798,
	DateTimeOffset_Equals_m7799,
	DateTimeOffset_GetHashCode_m7800,
	DateTimeOffset_ToString_m7801,
	DateTimeOffset_ToString_m1301,
	DateTimeOffset_ToUniversalTime_m1300,
	DateTimeOffset_get_DateTime_m7802,
	DateTimeOffset_get_Offset_m7803,
	DateTimeOffset_get_UtcDateTime_m7804,
	DateTimeUtils_CountRepeat_m7805,
	DateTimeUtils_ZeroPad_m7806,
	DateTimeUtils_ParseQuotedString_m7807,
	DateTimeUtils_GetStandardPattern_m7808,
	DateTimeUtils_GetStandardPattern_m7809,
	DateTimeUtils_ToString_m7810,
	DateTimeUtils_ToString_m7811,
	DelegateEntry__ctor_m7812,
	DelegateEntry_DeserializeDelegate_m7813,
	DelegateSerializationHolder__ctor_m7814,
	DelegateSerializationHolder_GetDelegateData_m7815,
	DelegateSerializationHolder_GetObjectData_m7816,
	DelegateSerializationHolder_GetRealObject_m7817,
	DivideByZeroException__ctor_m7818,
	DivideByZeroException__ctor_m7819,
	DllNotFoundException__ctor_m7820,
	DllNotFoundException__ctor_m7821,
	EntryPointNotFoundException__ctor_m7822,
	EntryPointNotFoundException__ctor_m7823,
	SByteComparer__ctor_m7824,
	SByteComparer_Compare_m7825,
	SByteComparer_Compare_m7826,
	ShortComparer__ctor_m7827,
	ShortComparer_Compare_m7828,
	ShortComparer_Compare_m7829,
	IntComparer__ctor_m7830,
	IntComparer_Compare_m7831,
	IntComparer_Compare_m7832,
	LongComparer__ctor_m7833,
	LongComparer_Compare_m7834,
	LongComparer_Compare_m7835,
	MonoEnumInfo__ctor_m7836,
	MonoEnumInfo__cctor_m7837,
	MonoEnumInfo_get_enum_info_m7838,
	MonoEnumInfo_get_Cache_m7839,
	MonoEnumInfo_GetInfo_m7840,
	Environment_get_SocketSecurityEnabled_m7841,
	Environment_get_NewLine_m2299,
	Environment_get_Platform_m7842,
	Environment_GetOSVersionString_m7843,
	Environment_get_OSVersion_m7844,
	Environment_get_TickCount_m1246,
	Environment_internalGetEnvironmentVariable_m7845,
	Environment_GetEnvironmentVariable_m3340,
	Environment_GetWindowsFolderPath_m7846,
	Environment_GetFolderPath_m3327,
	Environment_ReadXdgUserDir_m7847,
	Environment_InternalGetFolderPath_m7848,
	Environment_get_IsRunningOnWindows_m7849,
	Environment_GetMachineConfigPath_m7850,
	Environment_internalGetHome_m7851,
	EventArgs__ctor_m7852,
	EventArgs__cctor_m7853,
	ExecutionEngineException__ctor_m7854,
	ExecutionEngineException__ctor_m7855,
	FieldAccessException__ctor_m7856,
	FieldAccessException__ctor_m7857,
	FieldAccessException__ctor_m7858,
	FlagsAttribute__ctor_m7859,
	FormatException__ctor_m7860,
	FormatException__ctor_m1237,
	FormatException__ctor_m2407,
	GC_SuppressFinalize_m2457,
	Guid__ctor_m7861,
	Guid__ctor_m7862,
	Guid__cctor_m7863,
	Guid_CheckNull_m7864,
	Guid_CheckLength_m7865,
	Guid_CheckArray_m7866,
	Guid_Compare_m7867,
	Guid_CompareTo_m7868,
	Guid_Equals_m7869,
	Guid_CompareTo_m7870,
	Guid_Equals_m7871,
	Guid_GetHashCode_m7872,
	Guid_ToHex_m7873,
	Guid_NewGuid_m7874,
	Guid_AppendInt_m7875,
	Guid_AppendShort_m7876,
	Guid_AppendByte_m7877,
	Guid_BaseToString_m7878,
	Guid_ToString_m7879,
	Guid_ToString_m1302,
	Guid_ToString_m7880,
	IndexOutOfRangeException__ctor_m7881,
	IndexOutOfRangeException__ctor_m1197,
	IndexOutOfRangeException__ctor_m7882,
	InvalidCastException__ctor_m7883,
	InvalidCastException__ctor_m7884,
	InvalidCastException__ctor_m7885,
	InvalidOperationException__ctor_m2238,
	InvalidOperationException__ctor_m2232,
	InvalidOperationException__ctor_m7886,
	InvalidOperationException__ctor_m7887,
	Math_Abs_m7888,
	Math_Abs_m7889,
	Math_Abs_m7890,
	Math_Floor_m7891,
	Math_Max_m3298,
	Math_Min_m2456,
	Math_Round_m7892,
	Math_Round_m7893,
	Math_Pow_m7894,
	Math_Sqrt_m7895,
	MemberAccessException__ctor_m7896,
	MemberAccessException__ctor_m7897,
	MemberAccessException__ctor_m7898,
	MethodAccessException__ctor_m7899,
	MethodAccessException__ctor_m7900,
	MissingFieldException__ctor_m7901,
	MissingFieldException__ctor_m7902,
	MissingFieldException__ctor_m7903,
	MissingFieldException_get_Message_m7904,
	MissingMemberException__ctor_m7905,
	MissingMemberException__ctor_m7906,
	MissingMemberException__ctor_m7907,
	MissingMemberException__ctor_m7908,
	MissingMemberException_GetObjectData_m7909,
	MissingMemberException_get_Message_m7910,
	MissingMethodException__ctor_m7911,
	MissingMethodException__ctor_m7912,
	MissingMethodException__ctor_m7913,
	MissingMethodException__ctor_m7914,
	MissingMethodException_get_Message_m7915,
	MonoAsyncCall__ctor_m7916,
	AttributeInfo__ctor_m7917,
	AttributeInfo_get_Usage_m7918,
	AttributeInfo_get_InheritanceLevel_m7919,
	MonoCustomAttrs__cctor_m7920,
	MonoCustomAttrs_IsUserCattrProvider_m7921,
	MonoCustomAttrs_GetCustomAttributesInternal_m7922,
	MonoCustomAttrs_GetPseudoCustomAttributes_m7923,
	MonoCustomAttrs_GetCustomAttributesBase_m7924,
	MonoCustomAttrs_GetCustomAttribute_m7925,
	MonoCustomAttrs_GetCustomAttributes_m7926,
	MonoCustomAttrs_GetCustomAttributes_m7927,
	MonoCustomAttrs_IsDefined_m7928,
	MonoCustomAttrs_IsDefinedInternal_m7929,
	MonoCustomAttrs_GetBasePropertyDefinition_m7930,
	MonoCustomAttrs_GetBase_m7931,
	MonoCustomAttrs_RetrieveAttributeUsage_m7932,
	MonoTouchAOTHelper__cctor_m7933,
	MonoTypeInfo__ctor_m7934,
	MonoType_get_attributes_m7935,
	MonoType_GetDefaultConstructor_m7936,
	MonoType_GetAttributeFlagsImpl_m7937,
	MonoType_GetConstructorImpl_m7938,
	MonoType_GetConstructors_internal_m7939,
	MonoType_GetConstructors_m7940,
	MonoType_InternalGetEvent_m7941,
	MonoType_GetEvent_m7942,
	MonoType_GetField_m7943,
	MonoType_GetFields_internal_m7944,
	MonoType_GetFields_m7945,
	MonoType_GetInterfaces_m7946,
	MonoType_GetMethodsByName_m7947,
	MonoType_GetMethods_m7948,
	MonoType_GetMethodImpl_m7949,
	MonoType_GetPropertiesByName_m7950,
	MonoType_GetProperties_m7951,
	MonoType_GetPropertyImpl_m7952,
	MonoType_HasElementTypeImpl_m7953,
	MonoType_IsArrayImpl_m7954,
	MonoType_IsByRefImpl_m7955,
	MonoType_IsPointerImpl_m7956,
	MonoType_IsPrimitiveImpl_m7957,
	MonoType_IsSubclassOf_m7958,
	MonoType_InvokeMember_m7959,
	MonoType_GetElementType_m7960,
	MonoType_get_UnderlyingSystemType_m7961,
	MonoType_get_Assembly_m7962,
	MonoType_get_AssemblyQualifiedName_m7963,
	MonoType_getFullName_m7964,
	MonoType_get_BaseType_m7965,
	MonoType_get_FullName_m7966,
	MonoType_IsDefined_m7967,
	MonoType_GetCustomAttributes_m7968,
	MonoType_GetCustomAttributes_m7969,
	MonoType_get_MemberType_m7970,
	MonoType_get_Name_m7971,
	MonoType_get_Namespace_m7972,
	MonoType_get_Module_m7973,
	MonoType_get_DeclaringType_m7974,
	MonoType_get_ReflectedType_m7975,
	MonoType_get_TypeHandle_m7976,
	MonoType_GetObjectData_m7977,
	MonoType_ToString_m7978,
	MonoType_GetGenericArguments_m7979,
	MonoType_get_ContainsGenericParameters_m7980,
	MonoType_get_IsGenericParameter_m7981,
	MonoType_GetGenericTypeDefinition_m7982,
	MonoType_CheckMethodSecurity_m7983,
	MonoType_ReorderParamArrayArguments_m7984,
	MulticastNotSupportedException__ctor_m7985,
	MulticastNotSupportedException__ctor_m7986,
	MulticastNotSupportedException__ctor_m7987,
	NonSerializedAttribute__ctor_m7988,
	NotImplementedException__ctor_m7989,
	NotImplementedException__ctor_m2279,
	NotImplementedException__ctor_m7990,
	NotSupportedException__ctor_m2278,
	NotSupportedException__ctor_m2249,
	NotSupportedException__ctor_m7991,
	NullReferenceException__ctor_m7992,
	NullReferenceException__ctor_m1160,
	NullReferenceException__ctor_m7993,
	CustomInfo__ctor_m7994,
	CustomInfo_GetActiveSection_m7995,
	CustomInfo_Parse_m7996,
	CustomInfo_Format_m7997,
	NumberFormatter__ctor_m7998,
	NumberFormatter__cctor_m7999,
	NumberFormatter_GetFormatterTables_m8000,
	NumberFormatter_GetTenPowerOf_m8001,
	NumberFormatter_InitDecHexDigits_m8002,
	NumberFormatter_InitDecHexDigits_m8003,
	NumberFormatter_InitDecHexDigits_m8004,
	NumberFormatter_FastToDecHex_m8005,
	NumberFormatter_ToDecHex_m8006,
	NumberFormatter_FastDecHexLen_m8007,
	NumberFormatter_DecHexLen_m8008,
	NumberFormatter_DecHexLen_m8009,
	NumberFormatter_ScaleOrder_m8010,
	NumberFormatter_InitialFloatingPrecision_m8011,
	NumberFormatter_ParsePrecision_m8012,
	NumberFormatter_Init_m8013,
	NumberFormatter_InitHex_m8014,
	NumberFormatter_Init_m8015,
	NumberFormatter_Init_m8016,
	NumberFormatter_Init_m8017,
	NumberFormatter_Init_m8018,
	NumberFormatter_Init_m8019,
	NumberFormatter_Init_m8020,
	NumberFormatter_ResetCharBuf_m8021,
	NumberFormatter_Resize_m8022,
	NumberFormatter_Append_m8023,
	NumberFormatter_Append_m8024,
	NumberFormatter_Append_m8025,
	NumberFormatter_GetNumberFormatInstance_m8026,
	NumberFormatter_set_CurrentCulture_m8027,
	NumberFormatter_get_IntegerDigits_m8028,
	NumberFormatter_get_DecimalDigits_m8029,
	NumberFormatter_get_IsFloatingSource_m8030,
	NumberFormatter_get_IsZero_m8031,
	NumberFormatter_get_IsZeroInteger_m8032,
	NumberFormatter_RoundPos_m8033,
	NumberFormatter_RoundDecimal_m8034,
	NumberFormatter_RoundBits_m8035,
	NumberFormatter_RemoveTrailingZeros_m8036,
	NumberFormatter_AddOneToDecHex_m8037,
	NumberFormatter_AddOneToDecHex_m8038,
	NumberFormatter_CountTrailingZeros_m8039,
	NumberFormatter_CountTrailingZeros_m8040,
	NumberFormatter_GetInstance_m8041,
	NumberFormatter_Release_m8042,
	NumberFormatter_SetThreadCurrentCulture_m8043,
	NumberFormatter_NumberToString_m8044,
	NumberFormatter_NumberToString_m8045,
	NumberFormatter_NumberToString_m8046,
	NumberFormatter_NumberToString_m8047,
	NumberFormatter_NumberToString_m8048,
	NumberFormatter_NumberToString_m8049,
	NumberFormatter_NumberToString_m8050,
	NumberFormatter_NumberToString_m8051,
	NumberFormatter_NumberToString_m8052,
	NumberFormatter_NumberToString_m8053,
	NumberFormatter_NumberToString_m8054,
	NumberFormatter_NumberToString_m8055,
	NumberFormatter_NumberToString_m8056,
	NumberFormatter_NumberToString_m8057,
	NumberFormatter_NumberToString_m8058,
	NumberFormatter_NumberToString_m8059,
	NumberFormatter_NumberToString_m8060,
	NumberFormatter_FastIntegerToString_m8061,
	NumberFormatter_IntegerToString_m8062,
	NumberFormatter_NumberToString_m8063,
	NumberFormatter_FormatCurrency_m8064,
	NumberFormatter_FormatDecimal_m8065,
	NumberFormatter_FormatHexadecimal_m8066,
	NumberFormatter_FormatFixedPoint_m8067,
	NumberFormatter_FormatRoundtrip_m8068,
	NumberFormatter_FormatRoundtrip_m8069,
	NumberFormatter_FormatGeneral_m8070,
	NumberFormatter_FormatNumber_m8071,
	NumberFormatter_FormatPercent_m8072,
	NumberFormatter_FormatExponential_m8073,
	NumberFormatter_FormatExponential_m8074,
	NumberFormatter_FormatCustom_m8075,
	NumberFormatter_ZeroTrimEnd_m8076,
	NumberFormatter_IsZeroOnly_m8077,
	NumberFormatter_AppendNonNegativeNumber_m8078,
	NumberFormatter_AppendIntegerString_m8079,
	NumberFormatter_AppendIntegerString_m8080,
	NumberFormatter_AppendDecimalString_m8081,
	NumberFormatter_AppendDecimalString_m8082,
	NumberFormatter_AppendIntegerStringWithGroupSeparator_m8083,
	NumberFormatter_AppendExponent_m8084,
	NumberFormatter_AppendOneDigit_m8085,
	NumberFormatter_FastAppendDigits_m8086,
	NumberFormatter_AppendDigits_m8087,
	NumberFormatter_AppendDigits_m8088,
	NumberFormatter_Multiply10_m8089,
	NumberFormatter_Divide10_m8090,
	NumberFormatter_GetClone_m8091,
	ObjectDisposedException__ctor_m2459,
	ObjectDisposedException__ctor_m8092,
	ObjectDisposedException__ctor_m8093,
	ObjectDisposedException_get_Message_m8094,
	ObjectDisposedException_GetObjectData_m8095,
	OperatingSystem__ctor_m8096,
	OperatingSystem_get_Platform_m8097,
	OperatingSystem_GetObjectData_m8098,
	OperatingSystem_ToString_m8099,
	OutOfMemoryException__ctor_m8100,
	OutOfMemoryException__ctor_m8101,
	OverflowException__ctor_m8102,
	OverflowException__ctor_m8103,
	OverflowException__ctor_m8104,
	Random__ctor_m8105,
	Random__ctor_m1247,
	RankException__ctor_m8106,
	RankException__ctor_m8107,
	RankException__ctor_m8108,
	ResolveEventArgs__ctor_m8109,
	RuntimeMethodHandle__ctor_m8110,
	RuntimeMethodHandle__ctor_m8111,
	RuntimeMethodHandle_get_Value_m8112,
	RuntimeMethodHandle_GetObjectData_m8113,
	RuntimeMethodHandle_Equals_m8114,
	RuntimeMethodHandle_GetHashCode_m8115,
	StringComparer__ctor_m8116,
	StringComparer__cctor_m8117,
	StringComparer_get_InvariantCultureIgnoreCase_m2273,
	StringComparer_get_OrdinalIgnoreCase_m1189,
	StringComparer_Compare_m8118,
	StringComparer_Equals_m8119,
	StringComparer_GetHashCode_m8120,
	CultureAwareComparer__ctor_m8121,
	CultureAwareComparer_Compare_m8122,
	CultureAwareComparer_Equals_m8123,
	CultureAwareComparer_GetHashCode_m8124,
	OrdinalComparer__ctor_m8125,
	OrdinalComparer_Compare_m8126,
	OrdinalComparer_Equals_m8127,
	OrdinalComparer_GetHashCode_m8128,
	SystemException__ctor_m8129,
	SystemException__ctor_m2376,
	SystemException__ctor_m8130,
	SystemException__ctor_m8131,
	ThreadStaticAttribute__ctor_m8132,
	TimeSpan__ctor_m8133,
	TimeSpan__ctor_m8134,
	TimeSpan__ctor_m8135,
	TimeSpan__cctor_m8136,
	TimeSpan_CalculateTicks_m8137,
	TimeSpan_get_Days_m8138,
	TimeSpan_get_Hours_m8139,
	TimeSpan_get_Milliseconds_m8140,
	TimeSpan_get_Minutes_m8141,
	TimeSpan_get_Seconds_m8142,
	TimeSpan_get_Ticks_m8143,
	TimeSpan_get_TotalDays_m8144,
	TimeSpan_get_TotalHours_m8145,
	TimeSpan_get_TotalMilliseconds_m8146,
	TimeSpan_get_TotalMinutes_m8147,
	TimeSpan_get_TotalSeconds_m8148,
	TimeSpan_Add_m8149,
	TimeSpan_Compare_m8150,
	TimeSpan_CompareTo_m8151,
	TimeSpan_CompareTo_m8152,
	TimeSpan_Equals_m8153,
	TimeSpan_Duration_m8154,
	TimeSpan_Equals_m8155,
	TimeSpan_FromDays_m8156,
	TimeSpan_FromHours_m8157,
	TimeSpan_FromMinutes_m8158,
	TimeSpan_FromSeconds_m8159,
	TimeSpan_FromMilliseconds_m8160,
	TimeSpan_From_m8161,
	TimeSpan_GetHashCode_m8162,
	TimeSpan_Negate_m8163,
	TimeSpan_Subtract_m8164,
	TimeSpan_ToString_m8165,
	TimeSpan_op_Addition_m8166,
	TimeSpan_op_Equality_m8167,
	TimeSpan_op_GreaterThan_m8168,
	TimeSpan_op_GreaterThanOrEqual_m8169,
	TimeSpan_op_Inequality_m8170,
	TimeSpan_op_LessThan_m8171,
	TimeSpan_op_LessThanOrEqual_m8172,
	TimeSpan_op_Subtraction_m8173,
	TimeZone__ctor_m8174,
	TimeZone__cctor_m8175,
	TimeZone_get_CurrentTimeZone_m8176,
	TimeZone_IsDaylightSavingTime_m8177,
	TimeZone_IsDaylightSavingTime_m8178,
	TimeZone_ToLocalTime_m8179,
	TimeZone_ToUniversalTime_m8180,
	TimeZone_GetLocalTimeDiff_m8181,
	TimeZone_GetLocalTimeDiff_m8182,
	CurrentSystemTimeZone__ctor_m8183,
	CurrentSystemTimeZone__ctor_m8184,
	CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8185,
	CurrentSystemTimeZone_GetTimeZoneData_m8186,
	CurrentSystemTimeZone_GetDaylightChanges_m8187,
	CurrentSystemTimeZone_GetUtcOffset_m8188,
	CurrentSystemTimeZone_OnDeserialization_m8189,
	CurrentSystemTimeZone_GetDaylightTimeFromData_m8190,
	TypeInitializationException__ctor_m8191,
	TypeInitializationException_GetObjectData_m8192,
	TypeLoadException__ctor_m8193,
	TypeLoadException__ctor_m8194,
	TypeLoadException__ctor_m8195,
	TypeLoadException_get_Message_m8196,
	TypeLoadException_GetObjectData_m8197,
	UnauthorizedAccessException__ctor_m8198,
	UnauthorizedAccessException__ctor_m8199,
	UnauthorizedAccessException__ctor_m8200,
	UnhandledExceptionEventArgs__ctor_m8201,
	UnhandledExceptionEventArgs_get_ExceptionObject_m8202,
	UnhandledExceptionEventArgs_get_IsTerminating_m8203,
	UnitySerializationHolder__ctor_m8204,
	UnitySerializationHolder_GetTypeData_m8205,
	UnitySerializationHolder_GetDBNullData_m8206,
	UnitySerializationHolder_GetModuleData_m8207,
	UnitySerializationHolder_GetObjectData_m8208,
	UnitySerializationHolder_GetRealObject_m8209,
	Version__ctor_m8210,
	Version__ctor_m8211,
	Version__ctor_m2258,
	Version__ctor_m8212,
	Version_CheckedSet_m8213,
	Version_get_Build_m8214,
	Version_get_Major_m8215,
	Version_get_Minor_m8216,
	Version_get_Revision_m8217,
	Version_CompareTo_m8218,
	Version_Equals_m8219,
	Version_CompareTo_m8220,
	Version_Equals_m8221,
	Version_GetHashCode_m8222,
	Version_ToString_m8223,
	Version_CreateFromString_m8224,
	Version_op_Equality_m8225,
	Version_op_Inequality_m8226,
	WeakReference__ctor_m8227,
	WeakReference__ctor_m8228,
	WeakReference__ctor_m8229,
	WeakReference__ctor_m8230,
	WeakReference_AllocateHandle_m8231,
	WeakReference_get_Target_m8232,
	WeakReference_get_TrackResurrection_m8233,
	WeakReference_Finalize_m8234,
	WeakReference_GetObjectData_m8235,
	PrimalityTest__ctor_m8236,
	PrimalityTest_Invoke_m8237,
	PrimalityTest_BeginInvoke_m8238,
	PrimalityTest_EndInvoke_m8239,
	MemberFilter__ctor_m8240,
	MemberFilter_Invoke_m8241,
	MemberFilter_BeginInvoke_m8242,
	MemberFilter_EndInvoke_m8243,
	TypeFilter__ctor_m8244,
	TypeFilter_Invoke_m8245,
	TypeFilter_BeginInvoke_m8246,
	TypeFilter_EndInvoke_m8247,
	HeaderHandler__ctor_m8248,
	HeaderHandler_Invoke_m8249,
	HeaderHandler_BeginInvoke_m8250,
	HeaderHandler_EndInvoke_m8251,
	ThreadStart__ctor_m8252,
	ThreadStart_Invoke_m8253,
	ThreadStart_BeginInvoke_m8254,
	ThreadStart_EndInvoke_m8255,
	TimerCallback__ctor_m8256,
	TimerCallback_Invoke_m8257,
	TimerCallback_BeginInvoke_m8258,
	TimerCallback_EndInvoke_m8259,
	WaitCallback__ctor_m8260,
	WaitCallback_Invoke_m8261,
	WaitCallback_BeginInvoke_m8262,
	WaitCallback_EndInvoke_m8263,
	AppDomainInitializer__ctor_m8264,
	AppDomainInitializer_Invoke_m8265,
	AppDomainInitializer_BeginInvoke_m8266,
	AppDomainInitializer_EndInvoke_m8267,
	AssemblyLoadEventHandler__ctor_m8268,
	AssemblyLoadEventHandler_Invoke_m8269,
	AssemblyLoadEventHandler_BeginInvoke_m8270,
	AssemblyLoadEventHandler_EndInvoke_m8271,
	EventHandler__ctor_m8272,
	EventHandler_Invoke_m8273,
	EventHandler_BeginInvoke_m8274,
	EventHandler_EndInvoke_m8275,
	ResolveEventHandler__ctor_m8276,
	ResolveEventHandler_Invoke_m8277,
	ResolveEventHandler_BeginInvoke_m8278,
	ResolveEventHandler_EndInvoke_m8279,
	UnhandledExceptionEventHandler__ctor_m8280,
	UnhandledExceptionEventHandler_Invoke_m8281,
	UnhandledExceptionEventHandler_BeginInvoke_m8282,
	UnhandledExceptionEventHandler_EndInvoke_m8283,
};
