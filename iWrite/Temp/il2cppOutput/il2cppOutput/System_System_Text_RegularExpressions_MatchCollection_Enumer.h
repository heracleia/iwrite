﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_t489;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.MatchCollection/Enumerator
struct  Enumerator_t490  : public Object_t
{
	// System.Int32 System.Text.RegularExpressions.MatchCollection/Enumerator::index
	int32_t ___index_0;
	// System.Text.RegularExpressions.MatchCollection System.Text.RegularExpressions.MatchCollection/Enumerator::coll
	MatchCollection_t489 * ___coll_1;
};
