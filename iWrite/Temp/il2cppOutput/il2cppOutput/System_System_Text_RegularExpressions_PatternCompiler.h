﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t395;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.PatternCompiler
struct  PatternCompiler_t510  : public Object_t
{
	// System.Collections.ArrayList System.Text.RegularExpressions.PatternCompiler::pgm
	ArrayList_t395 * ___pgm_0;
};
