﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>
struct InternalEnumerator_1_t1654;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m10929_gshared (InternalEnumerator_1_t1654 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m10929(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1654 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m10929_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10930_gshared (InternalEnumerator_1_t1654 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10930(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1654 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10930_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m10931_gshared (InternalEnumerator_1_t1654 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m10931(__this, method) (( void (*) (InternalEnumerator_1_t1654 *, const MethodInfo*))InternalEnumerator_1_Dispose_m10931_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m10932_gshared (InternalEnumerator_1_t1654 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m10932(__this, method) (( bool (*) (InternalEnumerator_1_t1654 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m10932_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern "C" ParameterModifier_t1020  InternalEnumerator_1_get_Current_m10933_gshared (InternalEnumerator_1_t1654 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m10933(__this, method) (( ParameterModifier_t1020  (*) (InternalEnumerator_1_t1654 *, const MethodInfo*))InternalEnumerator_1_get_Current_m10933_gshared)(__this, method)
