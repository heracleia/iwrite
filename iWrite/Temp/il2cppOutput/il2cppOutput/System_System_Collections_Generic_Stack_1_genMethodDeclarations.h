﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t374;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t1906;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// System.Type
struct Type_t;
// System.Collections.Generic.Stack`1/Enumerator<System.Type>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.Stack`1<System.Type>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m1306(__this, method) (( void (*) (Stack_1_t374 *, const MethodInfo*))Stack_1__ctor_m10867_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m10868(__this, method) (( Object_t * (*) (Stack_1_t374 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m10869_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m10870(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t374 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m10871_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Type>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10872(__this, method) (( Object_t* (*) (Stack_1_t374 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10873_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Type>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m10874(__this, method) (( Object_t * (*) (Stack_1_t374 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m10875_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Type>::Pop()
#define Stack_1_Pop_m1308(__this, method) (( Type_t * (*) (Stack_1_t374 *, const MethodInfo*))Stack_1_Pop_m10876_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::Push(T)
#define Stack_1_Push_m1307(__this, ___t, method) (( void (*) (Stack_1_t374 *, Type_t *, const MethodInfo*))Stack_1_Push_m10877_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count()
#define Stack_1_get_Count_m10878(__this, method) (( int32_t (*) (Stack_1_t374 *, const MethodInfo*))Stack_1_get_Count_m10879_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Type>::GetEnumerator()
#define Stack_1_GetEnumerator_m10880(__this, method) (( Enumerator_t1652  (*) (Stack_1_t374 *, const MethodInfo*))Stack_1_GetEnumerator_m10881_gshared)(__this, method)
