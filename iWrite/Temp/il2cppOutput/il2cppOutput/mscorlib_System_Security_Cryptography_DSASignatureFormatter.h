﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.DSA
struct DSA_t561;
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureFor.h"
// System.Security.Cryptography.DSASignatureFormatter
struct  DSASignatureFormatter_t1185  : public AsymmetricSignatureFormatter_t699
{
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureFormatter::dsa
	DSA_t561 * ___dsa_0;
};
