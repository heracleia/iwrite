﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t385;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Hashtable/HashKeys
struct  HashKeys_t878  : public Object_t
{
	// System.Collections.Hashtable System.Collections.Hashtable/HashKeys::host
	Hashtable_t385 * ___host_0;
};
