﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Networking.Types.NetworkID>
struct EqualityComparer_1_t1593;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Networking.Types.NetworkID>
struct  EqualityComparer_1_t1593  : public Object_t
{
};
struct EqualityComparer_1_t1593_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Networking.Types.NetworkID>::_default
	EqualityComparer_1_t1593 * ____default_0;
};
