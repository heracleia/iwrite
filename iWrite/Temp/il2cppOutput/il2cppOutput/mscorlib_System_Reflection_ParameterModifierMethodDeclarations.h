﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.ParameterModifier
struct ParameterModifier_t1020;
struct ParameterModifier_t1020_marshaled;

void ParameterModifier_t1020_marshal(const ParameterModifier_t1020& unmarshaled, ParameterModifier_t1020_marshaled& marshaled);
void ParameterModifier_t1020_marshal_back(const ParameterModifier_t1020_marshaled& marshaled, ParameterModifier_t1020& unmarshaled);
void ParameterModifier_t1020_marshal_cleanup(ParameterModifier_t1020_marshaled& marshaled);
