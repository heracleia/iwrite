﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AccessViolationException
struct AccessViolationException_t1284;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t310;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.AccessViolationException::.ctor()
extern "C" void AccessViolationException__ctor_m7432 (AccessViolationException_t1284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AccessViolationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void AccessViolationException__ctor_m7433 (AccessViolationException_t1284 * __this, SerializationInfo_t310 * ___info, StreamingContext_t311  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
