﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
struct InternalEnumerator_1_t1751;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11817_gshared (InternalEnumerator_1_t1751 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11817(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1751 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11817_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11818_gshared (InternalEnumerator_1_t1751 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11818(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1751 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11818_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11819_gshared (InternalEnumerator_1_t1751 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11819(__this, method) (( void (*) (InternalEnumerator_1_t1751 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11819_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11820_gshared (InternalEnumerator_1_t1751 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11820(__this, method) (( bool (*) (InternalEnumerator_1_t1751 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11820_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern "C" TableRange_t797  InternalEnumerator_1_get_Current_m11821_gshared (InternalEnumerator_1_t1751 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11821(__this, method) (( TableRange_t797  (*) (InternalEnumerator_1_t1751 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11821_gshared)(__this, method)
