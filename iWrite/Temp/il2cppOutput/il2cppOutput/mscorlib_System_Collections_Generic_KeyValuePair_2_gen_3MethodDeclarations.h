﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
struct KeyValuePair_2_t1465;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m8578_gshared (KeyValuePair_2_t1465 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m8578(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1465 *, int32_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m8578_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m8579_gshared (KeyValuePair_2_t1465 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m8579(__this, method) (( int32_t (*) (KeyValuePair_2_t1465 *, const MethodInfo*))KeyValuePair_2_get_Key_m8579_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m8580_gshared (KeyValuePair_2_t1465 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m8580(__this, ___value, method) (( void (*) (KeyValuePair_2_t1465 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m8580_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m8581_gshared (KeyValuePair_2_t1465 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m8581(__this, method) (( Object_t * (*) (KeyValuePair_2_t1465 *, const MethodInfo*))KeyValuePair_2_get_Value_m8581_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m8582_gshared (KeyValuePair_2_t1465 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m8582(__this, ___value, method) (( void (*) (KeyValuePair_2_t1465 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m8582_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m8583_gshared (KeyValuePair_2_t1465 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m8583(__this, method) (( String_t* (*) (KeyValuePair_2_t1465 *, const MethodInfo*))KeyValuePair_2_ToString_m8583_gshared)(__this, method)
