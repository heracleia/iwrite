﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.StringFreezingAttribute
struct StringFreezingAttribute_t1035;

// System.Void System.Runtime.CompilerServices.StringFreezingAttribute::.ctor()
extern "C" void StringFreezingAttribute__ctor_m6083 (StringFreezingAttribute_t1035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
