﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t494;
// System.Runtime.Serialization.BaseFixupRecord
#include "mscorlib_System_Runtime_Serialization_BaseFixupRecord.h"
// System.Runtime.Serialization.MultiArrayFixupRecord
struct  MultiArrayFixupRecord_t1163  : public BaseFixupRecord_t1161
{
	// System.Int32[] System.Runtime.Serialization.MultiArrayFixupRecord::_indices
	Int32U5BU5D_t494* ____indices_4;
};
