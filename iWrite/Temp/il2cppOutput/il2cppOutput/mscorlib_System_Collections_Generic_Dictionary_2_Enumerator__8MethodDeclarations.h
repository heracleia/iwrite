﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>
struct Enumerator_t1570;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t163;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_10.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"
#define Enumerator__ctor_m9807(__this, ___dictionary, method) (( void (*) (Enumerator_t1570 *, Dictionary_2_t163 *, const MethodInfo*))Enumerator__ctor_m9708_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m9808(__this, method) (( Object_t * (*) (Enumerator_t1570 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m9709_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m9809(__this, method) (( DictionaryEntry_t560  (*) (Enumerator_t1570 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m9710_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m9810(__this, method) (( Object_t * (*) (Enumerator_t1570 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m9711_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m9811(__this, method) (( Object_t * (*) (Enumerator_t1570 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m9712_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::MoveNext()
#define Enumerator_MoveNext_m9812(__this, method) (( bool (*) (Enumerator_t1570 *, const MethodInfo*))Enumerator_MoveNext_m9713_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_Current()
#define Enumerator_get_Current_m9813(__this, method) (( KeyValuePair_2_t1567  (*) (Enumerator_t1570 *, const MethodInfo*))Enumerator_get_Current_m9714_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m9814(__this, method) (( String_t* (*) (Enumerator_t1570 *, const MethodInfo*))Enumerator_get_CurrentKey_m9715_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m9815(__this, method) (( int64_t (*) (Enumerator_t1570 *, const MethodInfo*))Enumerator_get_CurrentValue_m9716_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyState()
#define Enumerator_VerifyState_m9816(__this, method) (( void (*) (Enumerator_t1570 *, const MethodInfo*))Enumerator_VerifyState_m9717_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m9817(__this, method) (( void (*) (Enumerator_t1570 *, const MethodInfo*))Enumerator_VerifyCurrent_m9718_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::Dispose()
#define Enumerator_Dispose_m9818(__this, method) (( void (*) (Enumerator_t1570 *, const MethodInfo*))Enumerator_Dispose_m9719_gshared)(__this, method)
