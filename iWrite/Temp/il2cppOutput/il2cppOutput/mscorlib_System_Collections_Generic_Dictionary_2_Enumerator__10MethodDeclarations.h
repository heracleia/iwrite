﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
struct Enumerator_t1598;
// System.Object
struct Object_t;
// UnityEngine.Networking.Types.NetworkAccessToken
struct NetworkAccessToken_t180;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
struct Dictionary_2_t182;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9MethodDeclarations.h"
#define Enumerator__ctor_m10124(__this, ___dictionary, method) (( void (*) (Enumerator_t1598 *, Dictionary_2_t182 *, const MethodInfo*))Enumerator__ctor_m10028_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m10125(__this, method) (( Object_t * (*) (Enumerator_t1598 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m10029_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m10126(__this, method) (( DictionaryEntry_t560  (*) (Enumerator_t1598 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m10030_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m10127(__this, method) (( Object_t * (*) (Enumerator_t1598 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m10031_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m10128(__this, method) (( Object_t * (*) (Enumerator_t1598 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m10032_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::MoveNext()
#define Enumerator_MoveNext_m10129(__this, method) (( bool (*) (Enumerator_t1598 *, const MethodInfo*))Enumerator_MoveNext_m10033_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Current()
#define Enumerator_get_Current_m10130(__this, method) (( KeyValuePair_2_t1595  (*) (Enumerator_t1598 *, const MethodInfo*))Enumerator_get_Current_m10034_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m10131(__this, method) (( uint64_t (*) (Enumerator_t1598 *, const MethodInfo*))Enumerator_get_CurrentKey_m10035_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m10132(__this, method) (( NetworkAccessToken_t180 * (*) (Enumerator_t1598 *, const MethodInfo*))Enumerator_get_CurrentValue_m10036_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::VerifyState()
#define Enumerator_VerifyState_m10133(__this, method) (( void (*) (Enumerator_t1598 *, const MethodInfo*))Enumerator_VerifyState_m10037_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m10134(__this, method) (( void (*) (Enumerator_t1598 *, const MethodInfo*))Enumerator_VerifyCurrent_m10038_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::Dispose()
#define Enumerator_Dispose_m10135(__this, method) (( void (*) (Enumerator_t1598 *, const MethodInfo*))Enumerator_Dispose_m10039_gshared)(__this, method)
