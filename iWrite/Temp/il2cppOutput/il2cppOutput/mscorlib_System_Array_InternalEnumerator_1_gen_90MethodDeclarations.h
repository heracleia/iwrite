﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>
struct InternalEnumerator_1_t1780;
// System.Object
struct Object_t;
// System.Runtime.Remoting.Contexts.IContextAttribute
struct IContextAttribute_t1413;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m11945(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1780 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8302_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11946(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1780 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Dispose()
#define InternalEnumerator_1_Dispose_m11947(__this, method) (( void (*) (InternalEnumerator_1_t1780 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8306_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::MoveNext()
#define InternalEnumerator_1_MoveNext_m11948(__this, method) (( bool (*) (InternalEnumerator_1_t1780 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8308_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::get_Current()
#define InternalEnumerator_1_get_Current_m11949(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1780 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8310_gshared)(__this, method)
