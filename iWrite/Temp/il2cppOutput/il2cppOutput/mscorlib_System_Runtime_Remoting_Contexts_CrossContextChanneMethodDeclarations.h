﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Contexts.CrossContextChannel
struct CrossContextChannel_t1066;

// System.Void System.Runtime.Remoting.Contexts.CrossContextChannel::.ctor()
extern "C" void CrossContextChannel__ctor_m6172 (CrossContextChannel_t1066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
