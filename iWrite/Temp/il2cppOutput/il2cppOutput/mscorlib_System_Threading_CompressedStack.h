﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t395;
// System.Object
#include "mscorlib_System_Object.h"
// System.Threading.CompressedStack
struct  CompressedStack_t1230  : public Object_t
{
	// System.Collections.ArrayList System.Threading.CompressedStack::_list
	ArrayList_t395 * ____list_0;
};
