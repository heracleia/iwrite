﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t153;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t1845;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t289;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void List_1__ctor_m9530_gshared (List_1_t153 * __this, const MethodInfo* method);
#define List_1__ctor_m9530(__this, method) (( void (*) (List_1_t153 *, const MethodInfo*))List_1__ctor_m9530_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1233_gshared (List_1_t153 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1233(__this, ___capacity, method) (( void (*) (List_1_t153 *, int32_t, const MethodInfo*))List_1__ctor_m1233_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.cctor()
extern "C" void List_1__cctor_m9531_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m9531(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m9531_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9532_gshared (List_1_t153 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9532(__this, method) (( Object_t* (*) (List_1_t153 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9532_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m9533_gshared (List_1_t153 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m9533(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t153 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m9533_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m9534_gshared (List_1_t153 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m9534(__this, method) (( Object_t * (*) (List_1_t153 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m9534_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m9535_gshared (List_1_t153 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m9535(__this, ___item, method) (( int32_t (*) (List_1_t153 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m9535_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m9536_gshared (List_1_t153 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m9536(__this, ___item, method) (( bool (*) (List_1_t153 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m9536_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m9537_gshared (List_1_t153 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m9537(__this, ___item, method) (( int32_t (*) (List_1_t153 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m9537_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m9538_gshared (List_1_t153 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m9538(__this, ___index, ___item, method) (( void (*) (List_1_t153 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m9538_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m9539_gshared (List_1_t153 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m9539(__this, ___item, method) (( void (*) (List_1_t153 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m9539_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9540_gshared (List_1_t153 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9540(__this, method) (( bool (*) (List_1_t153 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9540_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m9541_gshared (List_1_t153 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m9541(__this, method) (( Object_t * (*) (List_1_t153 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m9541_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m9542_gshared (List_1_t153 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m9542(__this, ___index, method) (( Object_t * (*) (List_1_t153 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m9542_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m9543_gshared (List_1_t153 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m9543(__this, ___index, ___value, method) (( void (*) (List_1_t153 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m9543_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void List_1_Add_m9544_gshared (List_1_t153 * __this, UILineInfo_t150  ___item, const MethodInfo* method);
#define List_1_Add_m9544(__this, ___item, method) (( void (*) (List_1_t153 *, UILineInfo_t150 , const MethodInfo*))List_1_Add_m9544_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m9545_gshared (List_1_t153 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m9545(__this, ___newCount, method) (( void (*) (List_1_t153 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m9545_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Clear()
extern "C" void List_1_Clear_m9546_gshared (List_1_t153 * __this, const MethodInfo* method);
#define List_1_Clear_m9546(__this, method) (( void (*) (List_1_t153 *, const MethodInfo*))List_1_Clear_m9546_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool List_1_Contains_m9547_gshared (List_1_t153 * __this, UILineInfo_t150  ___item, const MethodInfo* method);
#define List_1_Contains_m9547(__this, ___item, method) (( bool (*) (List_1_t153 *, UILineInfo_t150 , const MethodInfo*))List_1_Contains_m9547_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m9548_gshared (List_1_t153 * __this, UILineInfoU5BU5D_t289* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m9548(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t153 *, UILineInfoU5BU5D_t289*, int32_t, const MethodInfo*))List_1_CopyTo_m9548_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Enumerator_t1546  List_1_GetEnumerator_m9549_gshared (List_1_t153 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m9549(__this, method) (( Enumerator_t1546  (*) (List_1_t153 *, const MethodInfo*))List_1_GetEnumerator_m9549_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m9550_gshared (List_1_t153 * __this, UILineInfo_t150  ___item, const MethodInfo* method);
#define List_1_IndexOf_m9550(__this, ___item, method) (( int32_t (*) (List_1_t153 *, UILineInfo_t150 , const MethodInfo*))List_1_IndexOf_m9550_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m9551_gshared (List_1_t153 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m9551(__this, ___start, ___delta, method) (( void (*) (List_1_t153 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m9551_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m9552_gshared (List_1_t153 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m9552(__this, ___index, method) (( void (*) (List_1_t153 *, int32_t, const MethodInfo*))List_1_CheckIndex_m9552_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m9553_gshared (List_1_t153 * __this, int32_t ___index, UILineInfo_t150  ___item, const MethodInfo* method);
#define List_1_Insert_m9553(__this, ___index, ___item, method) (( void (*) (List_1_t153 *, int32_t, UILineInfo_t150 , const MethodInfo*))List_1_Insert_m9553_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool List_1_Remove_m9554_gshared (List_1_t153 * __this, UILineInfo_t150  ___item, const MethodInfo* method);
#define List_1_Remove_m9554(__this, ___item, method) (( bool (*) (List_1_t153 *, UILineInfo_t150 , const MethodInfo*))List_1_Remove_m9554_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m9555_gshared (List_1_t153 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m9555(__this, ___index, method) (( void (*) (List_1_t153 *, int32_t, const MethodInfo*))List_1_RemoveAt_m9555_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UILineInfo>::ToArray()
extern "C" UILineInfoU5BU5D_t289* List_1_ToArray_m9556_gshared (List_1_t153 * __this, const MethodInfo* method);
#define List_1_ToArray_m9556(__this, method) (( UILineInfoU5BU5D_t289* (*) (List_1_t153 *, const MethodInfo*))List_1_ToArray_m9556_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m9557_gshared (List_1_t153 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m9557(__this, method) (( int32_t (*) (List_1_t153 *, const MethodInfo*))List_1_get_Capacity_m9557_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m9558_gshared (List_1_t153 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m9558(__this, ___value, method) (( void (*) (List_1_t153 *, int32_t, const MethodInfo*))List_1_set_Capacity_m9558_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m9559_gshared (List_1_t153 * __this, const MethodInfo* method);
#define List_1_get_Count_m9559(__this, method) (( int32_t (*) (List_1_t153 *, const MethodInfo*))List_1_get_Count_m9559_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t150  List_1_get_Item_m9560_gshared (List_1_t153 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m9560(__this, ___index, method) (( UILineInfo_t150  (*) (List_1_t153 *, int32_t, const MethodInfo*))List_1_get_Item_m9560_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m9561_gshared (List_1_t153 * __this, int32_t ___index, UILineInfo_t150  ___value, const MethodInfo* method);
#define List_1_set_Item_m9561(__this, ___index, ___value, method) (( void (*) (List_1_t153 *, int32_t, UILineInfo_t150 , const MethodInfo*))List_1_set_Item_m9561_gshared)(__this, ___index, ___value, method)
