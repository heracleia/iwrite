﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t1434;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m8293_gshared (GenericEqualityComparer_1_t1434 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m8293(__this, method) (( void (*) (GenericEqualityComparer_1_t1434 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m8293_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m12037_gshared (GenericEqualityComparer_1_t1434 * __this, DateTimeOffset_t363  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m12037(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1434 *, DateTimeOffset_t363 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m12037_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m12038_gshared (GenericEqualityComparer_1_t1434 * __this, DateTimeOffset_t363  ___x, DateTimeOffset_t363  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m12038(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1434 *, DateTimeOffset_t363 , DateTimeOffset_t363 , const MethodInfo*))GenericEqualityComparer_1_Equals_m12038_gshared)(__this, ___x, ___y, method)
