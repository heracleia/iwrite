﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.PreserveSigAttribute
struct PreserveSigAttribute_t1053;

// System.Void System.Runtime.InteropServices.PreserveSigAttribute::.ctor()
extern "C" void PreserveSigAttribute__ctor_m6107 (PreserveSigAttribute_t1053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
