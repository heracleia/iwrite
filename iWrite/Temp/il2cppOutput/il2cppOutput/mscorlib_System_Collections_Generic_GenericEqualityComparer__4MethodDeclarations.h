﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Int64>
struct GenericEqualityComparer_1_t1565;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m9765_gshared (GenericEqualityComparer_1_t1565 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m9765(__this, method) (( void (*) (GenericEqualityComparer_1_t1565 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m9765_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m9766_gshared (GenericEqualityComparer_1_t1565 * __this, int64_t ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m9766(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1565 *, int64_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m9766_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m9767_gshared (GenericEqualityComparer_1_t1565 * __this, int64_t ___x, int64_t ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m9767(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1565 *, int64_t, int64_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m9767_gshared)(__this, ___x, ___y, method)
