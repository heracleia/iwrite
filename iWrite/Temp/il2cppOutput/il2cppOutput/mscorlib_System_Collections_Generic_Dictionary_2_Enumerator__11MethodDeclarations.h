﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct Enumerator_t1622;
// System.Object
struct Object_t;
// System.String
struct String_t;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_t198;
// System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct Dictionary_2_t358;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"
#define Enumerator__ctor_m10597(__this, ___dictionary, method) (( void (*) (Enumerator_t1622 *, Dictionary_2_t358 *, const MethodInfo*))Enumerator__ctor_m8893_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m10598(__this, method) (( Object_t * (*) (Enumerator_t1622 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8894_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m10599(__this, method) (( DictionaryEntry_t560  (*) (Enumerator_t1622 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8895_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m10600(__this, method) (( Object_t * (*) (Enumerator_t1622 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8896_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m10601(__this, method) (( Object_t * (*) (Enumerator_t1622 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8897_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::MoveNext()
#define Enumerator_MoveNext_m10602(__this, method) (( bool (*) (Enumerator_t1622 *, const MethodInfo*))Enumerator_MoveNext_m8898_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Current()
#define Enumerator_get_Current_m10603(__this, method) (( KeyValuePair_2_t365  (*) (Enumerator_t1622 *, const MethodInfo*))Enumerator_get_Current_m8899_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m10604(__this, method) (( String_t* (*) (Enumerator_t1622 *, const MethodInfo*))Enumerator_get_CurrentKey_m8900_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m10605(__this, method) (( GetDelegate_t198 * (*) (Enumerator_t1622 *, const MethodInfo*))Enumerator_get_CurrentValue_m8901_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::VerifyState()
#define Enumerator_VerifyState_m10606(__this, method) (( void (*) (Enumerator_t1622 *, const MethodInfo*))Enumerator_VerifyState_m8902_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m10607(__this, method) (( void (*) (Enumerator_t1622 *, const MethodInfo*))Enumerator_VerifyCurrent_m8903_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Dispose()
#define Enumerator_Dispose_m10608(__this, method) (( void (*) (Enumerator_t1622 *, const MethodInfo*))Enumerator_Dispose_m8904_gshared)(__this, method)
