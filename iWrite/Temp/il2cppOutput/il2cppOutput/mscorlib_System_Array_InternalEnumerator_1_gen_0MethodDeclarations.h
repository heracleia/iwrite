﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Object>
struct InternalEnumerator_1_t1440;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m8302_gshared (InternalEnumerator_1_t1440 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m8302(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1440 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8302_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8304_gshared (InternalEnumerator_1_t1440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8304(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1440 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m8306_gshared (InternalEnumerator_1_t1440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m8306(__this, method) (( void (*) (InternalEnumerator_1_t1440 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8306_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m8308_gshared (InternalEnumerator_1_t1440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m8308(__this, method) (( bool (*) (InternalEnumerator_1_t1440 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8308_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern "C" Object_t * InternalEnumerator_1_get_Current_m8310_gshared (InternalEnumerator_1_t1440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m8310(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1440 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8310_gshared)(__this, method)
