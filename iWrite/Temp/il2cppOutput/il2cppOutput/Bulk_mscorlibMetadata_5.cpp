﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServices.h"
// Metadata Definition System.Runtime.Remoting.RemotingServices
extern TypeInfo RemotingServices_t1133_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServicesMethodDeclarations.h"
static const EncodedMethodIndex RemotingServices_t1133_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingServices_t1133_0_0_0;
extern const Il2CppType RemotingServices_t1133_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct RemotingServices_t1133;
const Il2CppTypeDefinitionMetadata RemotingServices_t1133_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingServices_t1133_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4488/* fieldStart */
	, 6886/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RemotingServices_t1133_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingServices"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &RemotingServices_t1133_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1136/* custom_attributes_cache */
	, &RemotingServices_t1133_0_0_0/* byval_arg */
	, &RemotingServices_t1133_1_0_0/* this_arg */
	, &RemotingServices_t1133_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingServices_t1133)/* instance_size */
	, sizeof (RemotingServices_t1133)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingServices_t1133_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentity.h"
// Metadata Definition System.Runtime.Remoting.ServerIdentity
extern TypeInfo ServerIdentity_t789_il2cpp_TypeInfo;
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentityMethodDeclarations.h"
static const EncodedMethodIndex ServerIdentity_t789_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2663,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ServerIdentity_t789_0_0_0;
extern const Il2CppType ServerIdentity_t789_1_0_0;
extern const Il2CppType Identity_t1111_0_0_0;
struct ServerIdentity_t789;
const Il2CppTypeDefinitionMetadata ServerIdentity_t789_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Identity_t1111_0_0_0/* parent */
	, ServerIdentity_t789_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4496/* fieldStart */
	, 6910/* methodStart */
	, -1/* eventStart */
	, 1447/* propertyStart */

};
TypeInfo ServerIdentity_t789_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ServerIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ServerIdentity_t789_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ServerIdentity_t789_0_0_0/* byval_arg */
	, &ServerIdentity_t789_1_0_0/* this_arg */
	, &ServerIdentity_t789_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ServerIdentity_t789)/* instance_size */
	, sizeof (ServerIdentity_t789)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentity.h"
// Metadata Definition System.Runtime.Remoting.ClientActivatedIdentity
extern TypeInfo ClientActivatedIdentity_t1134_il2cpp_TypeInfo;
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentityMethodDeclarations.h"
static const EncodedMethodIndex ClientActivatedIdentity_t1134_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2663,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClientActivatedIdentity_t1134_0_0_0;
extern const Il2CppType ClientActivatedIdentity_t1134_1_0_0;
struct ClientActivatedIdentity_t1134;
const Il2CppTypeDefinitionMetadata ClientActivatedIdentity_t1134_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t789_0_0_0/* parent */
	, ClientActivatedIdentity_t1134_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6913/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ClientActivatedIdentity_t1134_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientActivatedIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ClientActivatedIdentity_t1134_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientActivatedIdentity_t1134_0_0_0/* byval_arg */
	, &ClientActivatedIdentity_t1134_1_0_0/* this_arg */
	, &ClientActivatedIdentity_t1134_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientActivatedIdentity_t1134)/* instance_size */
	, sizeof (ClientActivatedIdentity_t1134)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentity.h"
// Metadata Definition System.Runtime.Remoting.SingletonIdentity
extern TypeInfo SingletonIdentity_t1135_il2cpp_TypeInfo;
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentityMethodDeclarations.h"
static const EncodedMethodIndex SingletonIdentity_t1135_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2663,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SingletonIdentity_t1135_0_0_0;
extern const Il2CppType SingletonIdentity_t1135_1_0_0;
struct SingletonIdentity_t1135;
const Il2CppTypeDefinitionMetadata SingletonIdentity_t1135_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t789_0_0_0/* parent */
	, SingletonIdentity_t1135_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6914/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SingletonIdentity_t1135_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SingletonIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &SingletonIdentity_t1135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SingletonIdentity_t1135_0_0_0/* byval_arg */
	, &SingletonIdentity_t1135_1_0_0/* this_arg */
	, &SingletonIdentity_t1135_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SingletonIdentity_t1135)/* instance_size */
	, sizeof (SingletonIdentity_t1135)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentity.h"
// Metadata Definition System.Runtime.Remoting.SingleCallIdentity
extern TypeInfo SingleCallIdentity_t1136_il2cpp_TypeInfo;
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentityMethodDeclarations.h"
static const EncodedMethodIndex SingleCallIdentity_t1136_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2663,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SingleCallIdentity_t1136_0_0_0;
extern const Il2CppType SingleCallIdentity_t1136_1_0_0;
struct SingleCallIdentity_t1136;
const Il2CppTypeDefinitionMetadata SingleCallIdentity_t1136_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t789_0_0_0/* parent */
	, SingleCallIdentity_t1136_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6915/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SingleCallIdentity_t1136_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SingleCallIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &SingleCallIdentity_t1136_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SingleCallIdentity_t1136_0_0_0/* byval_arg */
	, &SingleCallIdentity_t1136_1_0_0/* this_arg */
	, &SingleCallIdentity_t1136_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SingleCallIdentity_t1136)/* instance_size */
	, sizeof (SingleCallIdentity_t1136)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SoapServices
#include "mscorlib_System_Runtime_Remoting_SoapServices.h"
// Metadata Definition System.Runtime.Remoting.SoapServices
extern TypeInfo SoapServices_t1138_il2cpp_TypeInfo;
// System.Runtime.Remoting.SoapServices
#include "mscorlib_System_Runtime_Remoting_SoapServicesMethodDeclarations.h"
extern const Il2CppType TypeInfo_t1137_0_0_0;
static const Il2CppType* SoapServices_t1138_il2cpp_TypeInfo__nestedTypes[1] =
{
	&TypeInfo_t1137_0_0_0,
};
static const EncodedMethodIndex SoapServices_t1138_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SoapServices_t1138_0_0_0;
extern const Il2CppType SoapServices_t1138_1_0_0;
struct SoapServices_t1138;
const Il2CppTypeDefinitionMetadata SoapServices_t1138_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SoapServices_t1138_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SoapServices_t1138_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4499/* fieldStart */
	, 6916/* methodStart */
	, -1/* eventStart */
	, 1448/* propertyStart */

};
TypeInfo SoapServices_t1138_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoapServices"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &SoapServices_t1138_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1139/* custom_attributes_cache */
	, &SoapServices_t1138_0_0_0/* byval_arg */
	, &SoapServices_t1138_1_0_0/* this_arg */
	, &SoapServices_t1138_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoapServices_t1138)/* instance_size */
	, sizeof (SoapServices_t1138)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SoapServices_t1138_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SoapServices/TypeInfo
#include "mscorlib_System_Runtime_Remoting_SoapServices_TypeInfo.h"
// Metadata Definition System.Runtime.Remoting.SoapServices/TypeInfo
extern TypeInfo TypeInfo_t1137_il2cpp_TypeInfo;
// System.Runtime.Remoting.SoapServices/TypeInfo
#include "mscorlib_System_Runtime_Remoting_SoapServices_TypeInfoMethodDeclarations.h"
static const EncodedMethodIndex TypeInfo_t1137_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeInfo_t1137_1_0_0;
struct TypeInfo_t1137;
const Il2CppTypeDefinitionMetadata TypeInfo_t1137_DefinitionMetadata = 
{
	&SoapServices_t1138_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeInfo_t1137_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4504/* fieldStart */
	, 6932/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeInfo_t1137_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &TypeInfo_t1137_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInfo_t1137_0_0_0/* byval_arg */
	, &TypeInfo_t1137_1_0_0/* this_arg */
	, &TypeInfo_t1137_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInfo_t1137)/* instance_size */
	, sizeof (TypeInfo_t1137)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntry.h"
// Metadata Definition System.Runtime.Remoting.TypeEntry
extern TypeInfo TypeEntry_t1116_il2cpp_TypeInfo;
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntryMethodDeclarations.h"
static const EncodedMethodIndex TypeEntry_t1116_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeEntry_t1116_0_0_0;
extern const Il2CppType TypeEntry_t1116_1_0_0;
struct TypeEntry_t1116;
const Il2CppTypeDefinitionMetadata TypeEntry_t1116_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeEntry_t1116_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4506/* fieldStart */
	, 6933/* methodStart */
	, -1/* eventStart */
	, 1451/* propertyStart */

};
TypeInfo TypeEntry_t1116_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &TypeEntry_t1116_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1140/* custom_attributes_cache */
	, &TypeEntry_t1116_0_0_0/* byval_arg */
	, &TypeEntry_t1116_1_0_0/* this_arg */
	, &TypeEntry_t1116_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeEntry_t1116)/* instance_size */
	, sizeof (TypeEntry_t1116)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfo.h"
// Metadata Definition System.Runtime.Remoting.TypeInfo
extern TypeInfo TypeInfo_t1139_il2cpp_TypeInfo;
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfoMethodDeclarations.h"
static const EncodedMethodIndex TypeInfo_t1139_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2664,
};
extern const Il2CppType IRemotingTypeInfo_t1124_0_0_0;
static const Il2CppType* TypeInfo_t1139_InterfacesTypeInfos[] = 
{
	&IRemotingTypeInfo_t1124_0_0_0,
};
static Il2CppInterfaceOffsetPair TypeInfo_t1139_InterfacesOffsets[] = 
{
	{ &IRemotingTypeInfo_t1124_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeInfo_t1139_0_0_0;
extern const Il2CppType TypeInfo_t1139_1_0_0;
struct TypeInfo_t1139;
const Il2CppTypeDefinitionMetadata TypeInfo_t1139_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TypeInfo_t1139_InterfacesTypeInfos/* implementedInterfaces */
	, TypeInfo_t1139_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeInfo_t1139_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4508/* fieldStart */
	, 6938/* methodStart */
	, -1/* eventStart */
	, 1453/* propertyStart */

};
TypeInfo TypeInfo_t1139_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &TypeInfo_t1139_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInfo_t1139_0_0_0/* byval_arg */
	, &TypeInfo_t1139_1_0_0/* this_arg */
	, &TypeInfo_t1139_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInfo_t1139)/* instance_size */
	, sizeof (TypeInfo_t1139)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.WellKnownClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_WellKnownClientTypeEntry.h"
// Metadata Definition System.Runtime.Remoting.WellKnownClientTypeEntry
extern TypeInfo WellKnownClientTypeEntry_t1140_il2cpp_TypeInfo;
// System.Runtime.Remoting.WellKnownClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_WellKnownClientTypeEntryMethodDeclarations.h"
static const EncodedMethodIndex WellKnownClientTypeEntry_t1140_VTable[4] = 
{
	120,
	125,
	122,
	2665,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WellKnownClientTypeEntry_t1140_0_0_0;
extern const Il2CppType WellKnownClientTypeEntry_t1140_1_0_0;
struct WellKnownClientTypeEntry_t1140;
const Il2CppTypeDefinitionMetadata WellKnownClientTypeEntry_t1140_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeEntry_t1116_0_0_0/* parent */
	, WellKnownClientTypeEntry_t1140_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4511/* fieldStart */
	, 6940/* methodStart */
	, -1/* eventStart */
	, 1454/* propertyStart */

};
TypeInfo WellKnownClientTypeEntry_t1140_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WellKnownClientTypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &WellKnownClientTypeEntry_t1140_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1141/* custom_attributes_cache */
	, &WellKnownClientTypeEntry_t1140_0_0_0/* byval_arg */
	, &WellKnownClientTypeEntry_t1140_1_0_0/* this_arg */
	, &WellKnownClientTypeEntry_t1140_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WellKnownClientTypeEntry_t1140)/* instance_size */
	, sizeof (WellKnownClientTypeEntry_t1140)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
// Metadata Definition System.Runtime.Remoting.WellKnownObjectMode
extern TypeInfo WellKnownObjectMode_t1141_il2cpp_TypeInfo;
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectModeMethodDeclarations.h"
static const EncodedMethodIndex WellKnownObjectMode_t1141_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
extern const Il2CppType IFormattable_t1406_0_0_0;
extern const Il2CppType IConvertible_t1409_0_0_0;
extern const Il2CppType IComparable_t1408_0_0_0;
static Il2CppInterfaceOffsetPair WellKnownObjectMode_t1141_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WellKnownObjectMode_t1141_0_0_0;
extern const Il2CppType WellKnownObjectMode_t1141_1_0_0;
extern const Il2CppType Enum_t305_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t320_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata WellKnownObjectMode_t1141_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WellKnownObjectMode_t1141_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, WellKnownObjectMode_t1141_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4514/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WellKnownObjectMode_t1141_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WellKnownObjectMode"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1142/* custom_attributes_cache */
	, &WellKnownObjectMode_t1141_0_0_0/* byval_arg */
	, &WellKnownObjectMode_t1141_1_0_0/* this_arg */
	, &WellKnownObjectMode_t1141_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WellKnownObjectMode_t1141)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (WellKnownObjectMode_t1141)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.WellKnownServiceTypeEntry
#include "mscorlib_System_Runtime_Remoting_WellKnownServiceTypeEntry.h"
// Metadata Definition System.Runtime.Remoting.WellKnownServiceTypeEntry
extern TypeInfo WellKnownServiceTypeEntry_t1142_il2cpp_TypeInfo;
// System.Runtime.Remoting.WellKnownServiceTypeEntry
#include "mscorlib_System_Runtime_Remoting_WellKnownServiceTypeEntryMethodDeclarations.h"
static const EncodedMethodIndex WellKnownServiceTypeEntry_t1142_VTable[4] = 
{
	120,
	125,
	122,
	2666,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WellKnownServiceTypeEntry_t1142_0_0_0;
extern const Il2CppType WellKnownServiceTypeEntry_t1142_1_0_0;
struct WellKnownServiceTypeEntry_t1142;
const Il2CppTypeDefinitionMetadata WellKnownServiceTypeEntry_t1142_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeEntry_t1116_0_0_0/* parent */
	, WellKnownServiceTypeEntry_t1142_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4517/* fieldStart */
	, 6945/* methodStart */
	, -1/* eventStart */
	, 1457/* propertyStart */

};
TypeInfo WellKnownServiceTypeEntry_t1142_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WellKnownServiceTypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &WellKnownServiceTypeEntry_t1142_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1143/* custom_attributes_cache */
	, &WellKnownServiceTypeEntry_t1142_0_0_0/* byval_arg */
	, &WellKnownServiceTypeEntry_t1142_1_0_0/* this_arg */
	, &WellKnownServiceTypeEntry_t1142_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WellKnownServiceTypeEntry_t1142)/* instance_size */
	, sizeof (WellKnownServiceTypeEntry_t1142)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryCommon
extern TypeInfo BinaryCommon_t1143_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_BinaMethodDeclarations.h"
static const EncodedMethodIndex BinaryCommon_t1143_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryCommon_t1143_0_0_0;
extern const Il2CppType BinaryCommon_t1143_1_0_0;
struct BinaryCommon_t1143;
const Il2CppTypeDefinitionMetadata BinaryCommon_t1143_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryCommon_t1143_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4520/* fieldStart */
	, 6950/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BinaryCommon_t1143_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryCommon"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &BinaryCommon_t1143_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BinaryCommon_t1143_0_0_0/* byval_arg */
	, &BinaryCommon_t1143_1_0_0/* this_arg */
	, &BinaryCommon_t1143_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryCommon_t1143)/* instance_size */
	, sizeof (BinaryCommon_t1143)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BinaryCommon_t1143_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryElement
extern TypeInfo BinaryElement_t1144_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0MethodDeclarations.h"
static const EncodedMethodIndex BinaryElement_t1144_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair BinaryElement_t1144_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryElement_t1144_0_0_0;
extern const Il2CppType BinaryElement_t1144_1_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t326_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata BinaryElement_t1144_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BinaryElement_t1144_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, BinaryElement_t1144_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4524/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BinaryElement_t1144_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryElement"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &Byte_t326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BinaryElement_t1144_0_0_0/* byval_arg */
	, &BinaryElement_t1144_1_0_0/* this_arg */
	, &BinaryElement_t1144_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryElement_t1144)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BinaryElement_t1144)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.TypeTag
extern TypeInfo TypeTag_t1145_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_TypeMethodDeclarations.h"
static const EncodedMethodIndex TypeTag_t1145_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair TypeTag_t1145_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeTag_t1145_0_0_0;
extern const Il2CppType TypeTag_t1145_1_0_0;
const Il2CppTypeDefinitionMetadata TypeTag_t1145_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeTag_t1145_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, TypeTag_t1145_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4548/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeTag_t1145_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeTag"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &Byte_t326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeTag_t1145_0_0_0/* byval_arg */
	, &TypeTag_t1145_1_0_0/* this_arg */
	, &TypeTag_t1145_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeTag_t1145)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeTag_t1145)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Meth.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MethodFlags
extern TypeInfo MethodFlags_t1146_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MethMethodDeclarations.h"
static const EncodedMethodIndex MethodFlags_t1146_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair MethodFlags_t1146_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodFlags_t1146_0_0_0;
extern const Il2CppType MethodFlags_t1146_1_0_0;
const Il2CppTypeDefinitionMetadata MethodFlags_t1146_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodFlags_t1146_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, MethodFlags_t1146_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4557/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MethodFlags_t1146_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodFlags"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodFlags_t1146_0_0_0/* byval_arg */
	, &MethodFlags_t1146_1_0_0/* this_arg */
	, &MethodFlags_t1146_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodFlags_t1146)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodFlags_t1146)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Retu.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
extern TypeInfo ReturnTypeTag_t1147_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_RetuMethodDeclarations.h"
static const EncodedMethodIndex ReturnTypeTag_t1147_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair ReturnTypeTag_t1147_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReturnTypeTag_t1147_0_0_0;
extern const Il2CppType ReturnTypeTag_t1147_1_0_0;
const Il2CppTypeDefinitionMetadata ReturnTypeTag_t1147_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReturnTypeTag_t1147_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, ReturnTypeTag_t1147_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4568/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReturnTypeTag_t1147_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnTypeTag"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &Byte_t326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReturnTypeTag_t1147_0_0_0/* byval_arg */
	, &ReturnTypeTag_t1147_1_0_0/* this_arg */
	, &ReturnTypeTag_t1147_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnTypeTag_t1147)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReturnTypeTag_t1147)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
extern TypeInfo BinaryFormatter_t1132_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1MethodDeclarations.h"
static const EncodedMethodIndex BinaryFormatter_t1132_VTable[8] = 
{
	120,
	125,
	122,
	123,
	2667,
	2668,
	2669,
	2670,
};
extern const Il2CppType IRemotingFormatter_t2113_0_0_0;
extern const Il2CppType IFormatter_t2115_0_0_0;
static const Il2CppType* BinaryFormatter_t1132_InterfacesTypeInfos[] = 
{
	&IRemotingFormatter_t2113_0_0_0,
	&IFormatter_t2115_0_0_0,
};
static Il2CppInterfaceOffsetPair BinaryFormatter_t1132_InterfacesOffsets[] = 
{
	{ &IRemotingFormatter_t2113_0_0_0, 4},
	{ &IFormatter_t2115_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryFormatter_t1132_0_0_0;
extern const Il2CppType BinaryFormatter_t1132_1_0_0;
struct BinaryFormatter_t1132;
const Il2CppTypeDefinitionMetadata BinaryFormatter_t1132_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BinaryFormatter_t1132_InterfacesTypeInfos/* implementedInterfaces */
	, BinaryFormatter_t1132_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryFormatter_t1132_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4573/* fieldStart */
	, 6954/* methodStart */
	, -1/* eventStart */
	, 1460/* propertyStart */

};
TypeInfo BinaryFormatter_t1132_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryFormatter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &BinaryFormatter_t1132_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1144/* custom_attributes_cache */
	, &BinaryFormatter_t1132_0_0_0/* byval_arg */
	, &BinaryFormatter_t1132_1_0_0/* this_arg */
	, &BinaryFormatter_t1132_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryFormatter_t1132)/* instance_size */
	, sizeof (BinaryFormatter_t1132)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BinaryFormatter_t1132_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Mess.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MessageFormatter
extern TypeInfo MessageFormatter_t1149_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MessMethodDeclarations.h"
static const EncodedMethodIndex MessageFormatter_t1149_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MessageFormatter_t1149_0_0_0;
extern const Il2CppType MessageFormatter_t1149_1_0_0;
struct MessageFormatter_t1149;
const Il2CppTypeDefinitionMetadata MessageFormatter_t1149_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MessageFormatter_t1149_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6965/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MessageFormatter_t1149_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MessageFormatter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &MessageFormatter_t1149_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MessageFormatter_t1149_0_0_0/* byval_arg */
	, &MessageFormatter_t1149_1_0_0/* this_arg */
	, &MessageFormatter_t1149_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MessageFormatter_t1149)/* instance_size */
	, sizeof (MessageFormatter_t1149)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader
extern TypeInfo ObjectReader_t1154_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1MethodDeclarations.h"
extern const Il2CppType TypeMetadata_t1151_0_0_0;
extern const Il2CppType ArrayNullFiller_t1152_0_0_0;
static const Il2CppType* ObjectReader_t1154_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TypeMetadata_t1151_0_0_0,
	&ArrayNullFiller_t1152_0_0_0,
};
static const EncodedMethodIndex ObjectReader_t1154_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectReader_t1154_0_0_0;
extern const Il2CppType ObjectReader_t1154_1_0_0;
struct ObjectReader_t1154;
const Il2CppTypeDefinitionMetadata ObjectReader_t1154_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ObjectReader_t1154_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectReader_t1154_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4580/* fieldStart */
	, 6967/* methodStart */
	, -1/* eventStart */
	, 1466/* propertyStart */

};
TypeInfo ObjectReader_t1154_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectReader"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &ObjectReader_t1154_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectReader_t1154_0_0_0/* byval_arg */
	, &ObjectReader_t1154_1_0_0/* this_arg */
	, &ObjectReader_t1154_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectReader_t1154)/* instance_size */
	, sizeof (ObjectReader_t1154)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 27/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
extern TypeInfo TypeMetadata_t1151_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_ObjeMethodDeclarations.h"
static const EncodedMethodIndex TypeMetadata_t1151_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeMetadata_t1151_1_0_0;
struct TypeMetadata_t1151;
const Il2CppTypeDefinitionMetadata TypeMetadata_t1151_DefinitionMetadata = 
{
	&ObjectReader_t1154_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeMetadata_t1151_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4592/* fieldStart */
	, 6994/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeMetadata_t1151_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeMetadata"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &TypeMetadata_t1151_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeMetadata_t1151_0_0_0/* byval_arg */
	, &TypeMetadata_t1151_1_0_0/* this_arg */
	, &TypeMetadata_t1151_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeMetadata_t1151)/* instance_size */
	, sizeof (TypeMetadata_t1151)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
extern TypeInfo ArrayNullFiller_t1152_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0MethodDeclarations.h"
static const EncodedMethodIndex ArrayNullFiller_t1152_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayNullFiller_t1152_1_0_0;
struct ArrayNullFiller_t1152;
const Il2CppTypeDefinitionMetadata ArrayNullFiller_t1152_DefinitionMetadata = 
{
	&ObjectReader_t1154_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArrayNullFiller_t1152_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4598/* fieldStart */
	, 6995/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArrayNullFiller_t1152_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayNullFiller"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ArrayNullFiller_t1152_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArrayNullFiller_t1152_0_0_0/* byval_arg */
	, &ArrayNullFiller_t1152_1_0_0/* this_arg */
	, &ArrayNullFiller_t1152_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayNullFiller_t1152)/* instance_size */
	, sizeof (ArrayNullFiller_t1152)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAs.h"
// Metadata Definition System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
extern TypeInfo FormatterAssemblyStyle_t1155_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAsMethodDeclarations.h"
static const EncodedMethodIndex FormatterAssemblyStyle_t1155_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair FormatterAssemblyStyle_t1155_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterAssemblyStyle_t1155_0_0_0;
extern const Il2CppType FormatterAssemblyStyle_t1155_1_0_0;
const Il2CppTypeDefinitionMetadata FormatterAssemblyStyle_t1155_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatterAssemblyStyle_t1155_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, FormatterAssemblyStyle_t1155_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4599/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormatterAssemblyStyle_t1155_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterAssemblyStyle"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1147/* custom_attributes_cache */
	, &FormatterAssemblyStyle_t1155_0_0_0/* byval_arg */
	, &FormatterAssemblyStyle_t1155_1_0_0/* this_arg */
	, &FormatterAssemblyStyle_t1155_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterAssemblyStyle_t1155)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FormatterAssemblyStyle_t1155)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTy.h"
// Metadata Definition System.Runtime.Serialization.Formatters.FormatterTypeStyle
extern TypeInfo FormatterTypeStyle_t1156_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTyMethodDeclarations.h"
static const EncodedMethodIndex FormatterTypeStyle_t1156_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair FormatterTypeStyle_t1156_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterTypeStyle_t1156_0_0_0;
extern const Il2CppType FormatterTypeStyle_t1156_1_0_0;
const Il2CppTypeDefinitionMetadata FormatterTypeStyle_t1156_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatterTypeStyle_t1156_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, FormatterTypeStyle_t1156_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4602/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormatterTypeStyle_t1156_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterTypeStyle"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1148/* custom_attributes_cache */
	, &FormatterTypeStyle_t1156_0_0_0/* byval_arg */
	, &FormatterTypeStyle_t1156_1_0_0/* this_arg */
	, &FormatterTypeStyle_t1156_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterTypeStyle_t1156)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FormatterTypeStyle_t1156)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
// Metadata Definition System.Runtime.Serialization.Formatters.TypeFilterLevel
extern TypeInfo TypeFilterLevel_t1157_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterLMethodDeclarations.h"
static const EncodedMethodIndex TypeFilterLevel_t1157_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair TypeFilterLevel_t1157_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeFilterLevel_t1157_0_0_0;
extern const Il2CppType TypeFilterLevel_t1157_1_0_0;
const Il2CppTypeDefinitionMetadata TypeFilterLevel_t1157_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeFilterLevel_t1157_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, TypeFilterLevel_t1157_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4606/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeFilterLevel_t1157_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeFilterLevel"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1149/* custom_attributes_cache */
	, &TypeFilterLevel_t1157_0_0_0/* byval_arg */
	, &TypeFilterLevel_t1157_1_0_0/* this_arg */
	, &TypeFilterLevel_t1157_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeFilterLevel_t1157)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeFilterLevel_t1157)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverter.h"
// Metadata Definition System.Runtime.Serialization.FormatterConverter
extern TypeInfo FormatterConverter_t1158_il2cpp_TypeInfo;
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverterMethodDeclarations.h"
static const EncodedMethodIndex FormatterConverter_t1158_VTable[10] = 
{
	120,
	125,
	122,
	123,
	2671,
	2672,
	2673,
	2674,
	2675,
	2676,
};
extern const Il2CppType IFormatterConverter_t1175_0_0_0;
static const Il2CppType* FormatterConverter_t1158_InterfacesTypeInfos[] = 
{
	&IFormatterConverter_t1175_0_0_0,
};
static Il2CppInterfaceOffsetPair FormatterConverter_t1158_InterfacesOffsets[] = 
{
	{ &IFormatterConverter_t1175_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterConverter_t1158_0_0_0;
extern const Il2CppType FormatterConverter_t1158_1_0_0;
struct FormatterConverter_t1158;
const Il2CppTypeDefinitionMetadata FormatterConverter_t1158_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FormatterConverter_t1158_InterfacesTypeInfos/* implementedInterfaces */
	, FormatterConverter_t1158_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FormatterConverter_t1158_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6996/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormatterConverter_t1158_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterConverter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &FormatterConverter_t1158_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1150/* custom_attributes_cache */
	, &FormatterConverter_t1158_0_0_0/* byval_arg */
	, &FormatterConverter_t1158_1_0_0/* this_arg */
	, &FormatterConverter_t1158_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterConverter_t1158)/* instance_size */
	, sizeof (FormatterConverter_t1158)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServices.h"
// Metadata Definition System.Runtime.Serialization.FormatterServices
extern TypeInfo FormatterServices_t1159_il2cpp_TypeInfo;
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServicesMethodDeclarations.h"
static const EncodedMethodIndex FormatterServices_t1159_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterServices_t1159_0_0_0;
extern const Il2CppType FormatterServices_t1159_1_0_0;
struct FormatterServices_t1159;
const Il2CppTypeDefinitionMetadata FormatterServices_t1159_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FormatterServices_t1159_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7003/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormatterServices_t1159_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterServices"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &FormatterServices_t1159_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1151/* custom_attributes_cache */
	, &FormatterServices_t1159_0_0_0/* byval_arg */
	, &FormatterServices_t1159_1_0_0/* this_arg */
	, &FormatterServices_t1159_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterServices_t1159)/* instance_size */
	, sizeof (FormatterServices_t1159)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IDeserializationCallback
extern TypeInfo IDeserializationCallback_t1429_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDeserializationCallback_t1429_0_0_0;
extern const Il2CppType IDeserializationCallback_t1429_1_0_0;
struct IDeserializationCallback_t1429;
const Il2CppTypeDefinitionMetadata IDeserializationCallback_t1429_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7005/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IDeserializationCallback_t1429_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDeserializationCallback"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &IDeserializationCallback_t1429_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1152/* custom_attributes_cache */
	, &IDeserializationCallback_t1429_0_0_0/* byval_arg */
	, &IDeserializationCallback_t1429_1_0_0/* this_arg */
	, &IDeserializationCallback_t1429_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IFormatter
extern TypeInfo IFormatter_t2115_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatter_t2115_1_0_0;
struct IFormatter_t2115;
const Il2CppTypeDefinitionMetadata IFormatter_t2115_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IFormatter_t2115_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &IFormatter_t2115_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1153/* custom_attributes_cache */
	, &IFormatter_t2115_0_0_0/* byval_arg */
	, &IFormatter_t2115_1_0_0/* this_arg */
	, &IFormatter_t2115_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IFormatterConverter
extern TypeInfo IFormatterConverter_t1175_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatterConverter_t1175_1_0_0;
struct IFormatterConverter_t1175;
const Il2CppTypeDefinitionMetadata IFormatterConverter_t1175_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7006/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IFormatterConverter_t1175_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatterConverter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &IFormatterConverter_t1175_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1154/* custom_attributes_cache */
	, &IFormatterConverter_t1175_0_0_0/* byval_arg */
	, &IFormatterConverter_t1175_1_0_0/* this_arg */
	, &IFormatterConverter_t1175_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IObjectReference
extern TypeInfo IObjectReference_t1427_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IObjectReference_t1427_0_0_0;
extern const Il2CppType IObjectReference_t1427_1_0_0;
struct IObjectReference_t1427;
const Il2CppTypeDefinitionMetadata IObjectReference_t1427_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7012/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IObjectReference_t1427_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IObjectReference"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &IObjectReference_t1427_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1155/* custom_attributes_cache */
	, &IObjectReference_t1427_0_0_0/* byval_arg */
	, &IObjectReference_t1427_1_0_0/* this_arg */
	, &IObjectReference_t1427_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.ISerializationSurrogate
extern TypeInfo ISerializationSurrogate_t1167_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISerializationSurrogate_t1167_0_0_0;
extern const Il2CppType ISerializationSurrogate_t1167_1_0_0;
struct ISerializationSurrogate_t1167;
const Il2CppTypeDefinitionMetadata ISerializationSurrogate_t1167_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7013/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISerializationSurrogate_t1167_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializationSurrogate"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &ISerializationSurrogate_t1167_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1156/* custom_attributes_cache */
	, &ISerializationSurrogate_t1167_0_0_0/* byval_arg */
	, &ISerializationSurrogate_t1167_1_0_0/* this_arg */
	, &ISerializationSurrogate_t1167_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.ISurrogateSelector
extern TypeInfo ISurrogateSelector_t1100_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISurrogateSelector_t1100_0_0_0;
extern const Il2CppType ISurrogateSelector_t1100_1_0_0;
struct ISurrogateSelector_t1100;
const Il2CppTypeDefinitionMetadata ISurrogateSelector_t1100_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7014/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISurrogateSelector_t1100_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISurrogateSelector"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &ISurrogateSelector_t1100_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1157/* custom_attributes_cache */
	, &ISurrogateSelector_t1100_0_0_0/* byval_arg */
	, &ISurrogateSelector_t1100_1_0_0/* this_arg */
	, &ISurrogateSelector_t1100_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.ObjectManager
#include "mscorlib_System_Runtime_Serialization_ObjectManager.h"
// Metadata Definition System.Runtime.Serialization.ObjectManager
extern TypeInfo ObjectManager_t1153_il2cpp_TypeInfo;
// System.Runtime.Serialization.ObjectManager
#include "mscorlib_System_Runtime_Serialization_ObjectManagerMethodDeclarations.h"
static const EncodedMethodIndex ObjectManager_t1153_VTable[11] = 
{
	120,
	125,
	122,
	123,
	2677,
	2678,
	2679,
	2680,
	2681,
	2682,
	2683,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectManager_t1153_0_0_0;
extern const Il2CppType ObjectManager_t1153_1_0_0;
struct ObjectManager_t1153;
const Il2CppTypeDefinitionMetadata ObjectManager_t1153_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectManager_t1153_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4609/* fieldStart */
	, 7015/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ObjectManager_t1153_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectManager"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &ObjectManager_t1153_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1158/* custom_attributes_cache */
	, &ObjectManager_t1153_0_0_0/* byval_arg */
	, &ObjectManager_t1153_1_0_0/* this_arg */
	, &ObjectManager_t1153_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectManager_t1153)/* instance_size */
	, sizeof (ObjectManager_t1153)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.BaseFixupRecord
#include "mscorlib_System_Runtime_Serialization_BaseFixupRecord.h"
// Metadata Definition System.Runtime.Serialization.BaseFixupRecord
extern TypeInfo BaseFixupRecord_t1161_il2cpp_TypeInfo;
// System.Runtime.Serialization.BaseFixupRecord
#include "mscorlib_System_Runtime_Serialization_BaseFixupRecordMethodDeclarations.h"
static const EncodedMethodIndex BaseFixupRecord_t1161_VTable[5] = 
{
	120,
	125,
	122,
	123,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BaseFixupRecord_t1161_0_0_0;
extern const Il2CppType BaseFixupRecord_t1161_1_0_0;
struct BaseFixupRecord_t1161;
const Il2CppTypeDefinitionMetadata BaseFixupRecord_t1161_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseFixupRecord_t1161_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4618/* fieldStart */
	, 7029/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BaseFixupRecord_t1161_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseFixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &BaseFixupRecord_t1161_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseFixupRecord_t1161_0_0_0/* byval_arg */
	, &BaseFixupRecord_t1161_1_0_0/* this_arg */
	, &BaseFixupRecord_t1161_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseFixupRecord_t1161)/* instance_size */
	, sizeof (BaseFixupRecord_t1161)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.ArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_ArrayFixupRecord.h"
// Metadata Definition System.Runtime.Serialization.ArrayFixupRecord
extern TypeInfo ArrayFixupRecord_t1162_il2cpp_TypeInfo;
// System.Runtime.Serialization.ArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_ArrayFixupRecordMethodDeclarations.h"
static const EncodedMethodIndex ArrayFixupRecord_t1162_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2684,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayFixupRecord_t1162_0_0_0;
extern const Il2CppType ArrayFixupRecord_t1162_1_0_0;
struct ArrayFixupRecord_t1162;
const Il2CppTypeDefinitionMetadata ArrayFixupRecord_t1162_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseFixupRecord_t1161_0_0_0/* parent */
	, ArrayFixupRecord_t1162_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4622/* fieldStart */
	, 7032/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArrayFixupRecord_t1162_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayFixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &ArrayFixupRecord_t1162_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArrayFixupRecord_t1162_0_0_0/* byval_arg */
	, &ArrayFixupRecord_t1162_1_0_0/* this_arg */
	, &ArrayFixupRecord_t1162_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayFixupRecord_t1162)/* instance_size */
	, sizeof (ArrayFixupRecord_t1162)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.MultiArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_MultiArrayFixupRecord.h"
// Metadata Definition System.Runtime.Serialization.MultiArrayFixupRecord
extern TypeInfo MultiArrayFixupRecord_t1163_il2cpp_TypeInfo;
// System.Runtime.Serialization.MultiArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_MultiArrayFixupRecordMethodDeclarations.h"
static const EncodedMethodIndex MultiArrayFixupRecord_t1163_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2685,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MultiArrayFixupRecord_t1163_0_0_0;
extern const Il2CppType MultiArrayFixupRecord_t1163_1_0_0;
struct MultiArrayFixupRecord_t1163;
const Il2CppTypeDefinitionMetadata MultiArrayFixupRecord_t1163_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseFixupRecord_t1161_0_0_0/* parent */
	, MultiArrayFixupRecord_t1163_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4623/* fieldStart */
	, 7034/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MultiArrayFixupRecord_t1163_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MultiArrayFixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &MultiArrayFixupRecord_t1163_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MultiArrayFixupRecord_t1163_0_0_0/* byval_arg */
	, &MultiArrayFixupRecord_t1163_1_0_0/* this_arg */
	, &MultiArrayFixupRecord_t1163_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MultiArrayFixupRecord_t1163)/* instance_size */
	, sizeof (MultiArrayFixupRecord_t1163)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.FixupRecord
#include "mscorlib_System_Runtime_Serialization_FixupRecord.h"
// Metadata Definition System.Runtime.Serialization.FixupRecord
extern TypeInfo FixupRecord_t1164_il2cpp_TypeInfo;
// System.Runtime.Serialization.FixupRecord
#include "mscorlib_System_Runtime_Serialization_FixupRecordMethodDeclarations.h"
static const EncodedMethodIndex FixupRecord_t1164_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2686,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FixupRecord_t1164_0_0_0;
extern const Il2CppType FixupRecord_t1164_1_0_0;
struct FixupRecord_t1164;
const Il2CppTypeDefinitionMetadata FixupRecord_t1164_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseFixupRecord_t1161_0_0_0/* parent */
	, FixupRecord_t1164_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4624/* fieldStart */
	, 7036/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FixupRecord_t1164_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &FixupRecord_t1164_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FixupRecord_t1164_0_0_0/* byval_arg */
	, &FixupRecord_t1164_1_0_0/* this_arg */
	, &FixupRecord_t1164_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FixupRecord_t1164)/* instance_size */
	, sizeof (FixupRecord_t1164)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.DelayedFixupRecord
#include "mscorlib_System_Runtime_Serialization_DelayedFixupRecord.h"
// Metadata Definition System.Runtime.Serialization.DelayedFixupRecord
extern TypeInfo DelayedFixupRecord_t1165_il2cpp_TypeInfo;
// System.Runtime.Serialization.DelayedFixupRecord
#include "mscorlib_System_Runtime_Serialization_DelayedFixupRecordMethodDeclarations.h"
static const EncodedMethodIndex DelayedFixupRecord_t1165_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2687,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DelayedFixupRecord_t1165_0_0_0;
extern const Il2CppType DelayedFixupRecord_t1165_1_0_0;
struct DelayedFixupRecord_t1165;
const Il2CppTypeDefinitionMetadata DelayedFixupRecord_t1165_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseFixupRecord_t1161_0_0_0/* parent */
	, DelayedFixupRecord_t1165_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4625/* fieldStart */
	, 7038/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DelayedFixupRecord_t1165_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DelayedFixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &DelayedFixupRecord_t1165_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DelayedFixupRecord_t1165_0_0_0/* byval_arg */
	, &DelayedFixupRecord_t1165_1_0_0/* this_arg */
	, &DelayedFixupRecord_t1165_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DelayedFixupRecord_t1165)/* instance_size */
	, sizeof (DelayedFixupRecord_t1165)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.ObjectRecordStatus
#include "mscorlib_System_Runtime_Serialization_ObjectRecordStatus.h"
// Metadata Definition System.Runtime.Serialization.ObjectRecordStatus
extern TypeInfo ObjectRecordStatus_t1166_il2cpp_TypeInfo;
// System.Runtime.Serialization.ObjectRecordStatus
#include "mscorlib_System_Runtime_Serialization_ObjectRecordStatusMethodDeclarations.h"
static const EncodedMethodIndex ObjectRecordStatus_t1166_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair ObjectRecordStatus_t1166_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectRecordStatus_t1166_0_0_0;
extern const Il2CppType ObjectRecordStatus_t1166_1_0_0;
const Il2CppTypeDefinitionMetadata ObjectRecordStatus_t1166_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObjectRecordStatus_t1166_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, ObjectRecordStatus_t1166_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4626/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ObjectRecordStatus_t1166_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectRecordStatus"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &Byte_t326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectRecordStatus_t1166_0_0_0/* byval_arg */
	, &ObjectRecordStatus_t1166_1_0_0/* this_arg */
	, &ObjectRecordStatus_t1166_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectRecordStatus_t1166)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ObjectRecordStatus_t1166)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.ObjectRecord
#include "mscorlib_System_Runtime_Serialization_ObjectRecord.h"
// Metadata Definition System.Runtime.Serialization.ObjectRecord
extern TypeInfo ObjectRecord_t1160_il2cpp_TypeInfo;
// System.Runtime.Serialization.ObjectRecord
#include "mscorlib_System_Runtime_Serialization_ObjectRecordMethodDeclarations.h"
static const EncodedMethodIndex ObjectRecord_t1160_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectRecord_t1160_0_0_0;
extern const Il2CppType ObjectRecord_t1160_1_0_0;
struct ObjectRecord_t1160;
const Il2CppTypeDefinitionMetadata ObjectRecord_t1160_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectRecord_t1160_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4631/* fieldStart */
	, 7040/* methodStart */
	, -1/* eventStart */
	, 1467/* propertyStart */

};
TypeInfo ObjectRecord_t1160_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &ObjectRecord_t1160_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectRecord_t1160_0_0_0/* byval_arg */
	, &ObjectRecord_t1160_1_0_0/* this_arg */
	, &ObjectRecord_t1160_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectRecord_t1160)/* instance_size */
	, sizeof (ObjectRecord_t1160)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 4/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.OnDeserializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializedAttribut.h"
// Metadata Definition System.Runtime.Serialization.OnDeserializedAttribute
extern TypeInfo OnDeserializedAttribute_t1168_il2cpp_TypeInfo;
// System.Runtime.Serialization.OnDeserializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializedAttributMethodDeclarations.h"
static const EncodedMethodIndex OnDeserializedAttribute_t1168_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
extern const Il2CppType _Attribute_t2052_0_0_0;
static Il2CppInterfaceOffsetPair OnDeserializedAttribute_t1168_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OnDeserializedAttribute_t1168_0_0_0;
extern const Il2CppType OnDeserializedAttribute_t1168_1_0_0;
extern const Il2CppType Attribute_t96_0_0_0;
struct OnDeserializedAttribute_t1168;
const Il2CppTypeDefinitionMetadata OnDeserializedAttribute_t1168_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnDeserializedAttribute_t1168_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, OnDeserializedAttribute_t1168_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnDeserializedAttribute_t1168_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnDeserializedAttribute"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &OnDeserializedAttribute_t1168_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1159/* custom_attributes_cache */
	, &OnDeserializedAttribute_t1168_0_0_0/* byval_arg */
	, &OnDeserializedAttribute_t1168_1_0_0/* this_arg */
	, &OnDeserializedAttribute_t1168_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnDeserializedAttribute_t1168)/* instance_size */
	, sizeof (OnDeserializedAttribute_t1168)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.OnDeserializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializingAttribu.h"
// Metadata Definition System.Runtime.Serialization.OnDeserializingAttribute
extern TypeInfo OnDeserializingAttribute_t1169_il2cpp_TypeInfo;
// System.Runtime.Serialization.OnDeserializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializingAttribuMethodDeclarations.h"
static const EncodedMethodIndex OnDeserializingAttribute_t1169_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair OnDeserializingAttribute_t1169_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OnDeserializingAttribute_t1169_0_0_0;
extern const Il2CppType OnDeserializingAttribute_t1169_1_0_0;
struct OnDeserializingAttribute_t1169;
const Il2CppTypeDefinitionMetadata OnDeserializingAttribute_t1169_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnDeserializingAttribute_t1169_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, OnDeserializingAttribute_t1169_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnDeserializingAttribute_t1169_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnDeserializingAttribute"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &OnDeserializingAttribute_t1169_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1160/* custom_attributes_cache */
	, &OnDeserializingAttribute_t1169_0_0_0/* byval_arg */
	, &OnDeserializingAttribute_t1169_1_0_0/* this_arg */
	, &OnDeserializingAttribute_t1169_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnDeserializingAttribute_t1169)/* instance_size */
	, sizeof (OnDeserializingAttribute_t1169)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.OnSerializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializedAttribute.h"
// Metadata Definition System.Runtime.Serialization.OnSerializedAttribute
extern TypeInfo OnSerializedAttribute_t1170_il2cpp_TypeInfo;
// System.Runtime.Serialization.OnSerializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializedAttributeMethodDeclarations.h"
static const EncodedMethodIndex OnSerializedAttribute_t1170_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair OnSerializedAttribute_t1170_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OnSerializedAttribute_t1170_0_0_0;
extern const Il2CppType OnSerializedAttribute_t1170_1_0_0;
struct OnSerializedAttribute_t1170;
const Il2CppTypeDefinitionMetadata OnSerializedAttribute_t1170_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnSerializedAttribute_t1170_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, OnSerializedAttribute_t1170_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnSerializedAttribute_t1170_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnSerializedAttribute"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &OnSerializedAttribute_t1170_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1161/* custom_attributes_cache */
	, &OnSerializedAttribute_t1170_0_0_0/* byval_arg */
	, &OnSerializedAttribute_t1170_1_0_0/* this_arg */
	, &OnSerializedAttribute_t1170_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnSerializedAttribute_t1170)/* instance_size */
	, sizeof (OnSerializedAttribute_t1170)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.OnSerializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializingAttribute.h"
// Metadata Definition System.Runtime.Serialization.OnSerializingAttribute
extern TypeInfo OnSerializingAttribute_t1171_il2cpp_TypeInfo;
// System.Runtime.Serialization.OnSerializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializingAttributeMethodDeclarations.h"
static const EncodedMethodIndex OnSerializingAttribute_t1171_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair OnSerializingAttribute_t1171_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OnSerializingAttribute_t1171_0_0_0;
extern const Il2CppType OnSerializingAttribute_t1171_1_0_0;
struct OnSerializingAttribute_t1171;
const Il2CppTypeDefinitionMetadata OnSerializingAttribute_t1171_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnSerializingAttribute_t1171_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, OnSerializingAttribute_t1171_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnSerializingAttribute_t1171_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnSerializingAttribute"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &OnSerializingAttribute_t1171_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1162/* custom_attributes_cache */
	, &OnSerializingAttribute_t1171_0_0_0/* byval_arg */
	, &OnSerializingAttribute_t1171_1_0_0/* this_arg */
	, &OnSerializingAttribute_t1171_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnSerializingAttribute_t1171)/* instance_size */
	, sizeof (OnSerializingAttribute_t1171)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationBinder
#include "mscorlib_System_Runtime_Serialization_SerializationBinder.h"
// Metadata Definition System.Runtime.Serialization.SerializationBinder
extern TypeInfo SerializationBinder_t1148_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationBinder
#include "mscorlib_System_Runtime_Serialization_SerializationBinderMethodDeclarations.h"
static const EncodedMethodIndex SerializationBinder_t1148_VTable[5] = 
{
	120,
	125,
	122,
	123,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationBinder_t1148_0_0_0;
extern const Il2CppType SerializationBinder_t1148_1_0_0;
struct SerializationBinder_t1148;
const Il2CppTypeDefinitionMetadata SerializationBinder_t1148_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationBinder_t1148_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7053/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SerializationBinder_t1148_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationBinder"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &SerializationBinder_t1148_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1163/* custom_attributes_cache */
	, &SerializationBinder_t1148_0_0_0/* byval_arg */
	, &SerializationBinder_t1148_1_0_0/* this_arg */
	, &SerializationBinder_t1148_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationBinder_t1148)/* instance_size */
	, sizeof (SerializationBinder_t1148)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationCallbacks
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacks_0.h"
// Metadata Definition System.Runtime.Serialization.SerializationCallbacks
extern TypeInfo SerializationCallbacks_t1173_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationCallbacks
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacks_0MethodDeclarations.h"
extern const Il2CppType CallbackHandler_t1172_0_0_0;
static const Il2CppType* SerializationCallbacks_t1173_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CallbackHandler_t1172_0_0_0,
};
static const EncodedMethodIndex SerializationCallbacks_t1173_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationCallbacks_t1173_0_0_0;
extern const Il2CppType SerializationCallbacks_t1173_1_0_0;
struct SerializationCallbacks_t1173;
const Il2CppTypeDefinitionMetadata SerializationCallbacks_t1173_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SerializationCallbacks_t1173_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationCallbacks_t1173_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4644/* fieldStart */
	, 7055/* methodStart */
	, -1/* eventStart */
	, 1471/* propertyStart */

};
TypeInfo SerializationCallbacks_t1173_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationCallbacks"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &SerializationCallbacks_t1173_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SerializationCallbacks_t1173_0_0_0/* byval_arg */
	, &SerializationCallbacks_t1173_1_0_0/* this_arg */
	, &SerializationCallbacks_t1173_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationCallbacks_t1173)/* instance_size */
	, sizeof (SerializationCallbacks_t1173)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SerializationCallbacks_t1173_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationCallbacks/CallbackHandler
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacks.h"
// Metadata Definition System.Runtime.Serialization.SerializationCallbacks/CallbackHandler
extern TypeInfo CallbackHandler_t1172_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationCallbacks/CallbackHandler
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacksMethodDeclarations.h"
static const EncodedMethodIndex CallbackHandler_t1172_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	2688,
	2689,
	2690,
};
extern const Il2CppType ICloneable_t2056_0_0_0;
extern const Il2CppType ISerializable_t1426_0_0_0;
static Il2CppInterfaceOffsetPair CallbackHandler_t1172_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallbackHandler_t1172_1_0_0;
extern const Il2CppType MulticastDelegate_t47_0_0_0;
struct CallbackHandler_t1172;
const Il2CppTypeDefinitionMetadata CallbackHandler_t1172_DefinitionMetadata = 
{
	&SerializationCallbacks_t1173_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CallbackHandler_t1172_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, CallbackHandler_t1172_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7063/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CallbackHandler_t1172_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallbackHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CallbackHandler_t1172_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CallbackHandler_t1172_0_0_0/* byval_arg */
	, &CallbackHandler_t1172_1_0_0/* this_arg */
	, &CallbackHandler_t1172_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CallbackHandler_t1172/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallbackHandler_t1172)/* instance_size */
	, sizeof (CallbackHandler_t1172)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
// Metadata Definition System.Runtime.Serialization.SerializationEntry
extern TypeInfo SerializationEntry_t1174_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntryMethodDeclarations.h"
static const EncodedMethodIndex SerializationEntry_t1174_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationEntry_t1174_0_0_0;
extern const Il2CppType SerializationEntry_t1174_1_0_0;
extern const Il2CppType ValueType_t757_0_0_0;
const Il2CppTypeDefinitionMetadata SerializationEntry_t1174_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, SerializationEntry_t1174_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4650/* fieldStart */
	, 7067/* methodStart */
	, -1/* eventStart */
	, 1472/* propertyStart */

};
TypeInfo SerializationEntry_t1174_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationEntry"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &SerializationEntry_t1174_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1164/* custom_attributes_cache */
	, &SerializationEntry_t1174_0_0_0/* byval_arg */
	, &SerializationEntry_t1174_1_0_0/* this_arg */
	, &SerializationEntry_t1174_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationEntry_t1174)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SerializationEntry_t1174)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationException
#include "mscorlib_System_Runtime_Serialization_SerializationException.h"
// Metadata Definition System.Runtime.Serialization.SerializationException
extern TypeInfo SerializationException_t574_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationException
#include "mscorlib_System_Runtime_Serialization_SerializationExceptionMethodDeclarations.h"
static const EncodedMethodIndex SerializationException_t574_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
extern const Il2CppType _Exception_t2066_0_0_0;
static Il2CppInterfaceOffsetPair SerializationException_t574_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationException_t574_0_0_0;
extern const Il2CppType SerializationException_t574_1_0_0;
extern const Il2CppType SystemException_t597_0_0_0;
struct SerializationException_t574;
const Il2CppTypeDefinitionMetadata SerializationException_t574_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SerializationException_t574_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, SerializationException_t574_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7070/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SerializationException_t574_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationException"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &SerializationException_t574_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1165/* custom_attributes_cache */
	, &SerializationException_t574_0_0_0/* byval_arg */
	, &SerializationException_t574_1_0_0/* this_arg */
	, &SerializationException_t574_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationException_t574)/* instance_size */
	, sizeof (SerializationException_t574)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// Metadata Definition System.Runtime.Serialization.SerializationInfo
extern TypeInfo SerializationInfo_t310_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
static const EncodedMethodIndex SerializationInfo_t310_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationInfo_t310_0_0_0;
extern const Il2CppType SerializationInfo_t310_1_0_0;
struct SerializationInfo_t310;
const Il2CppTypeDefinitionMetadata SerializationInfo_t310_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationInfo_t310_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4653/* fieldStart */
	, 7073/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SerializationInfo_t310_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationInfo"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &SerializationInfo_t310_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1166/* custom_attributes_cache */
	, &SerializationInfo_t310_0_0_0/* byval_arg */
	, &SerializationInfo_t310_1_0_0/* this_arg */
	, &SerializationInfo_t310_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationInfo_t310)/* instance_size */
	, sizeof (SerializationInfo_t310)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationInfoEnumerator
#include "mscorlib_System_Runtime_Serialization_SerializationInfoEnume.h"
// Metadata Definition System.Runtime.Serialization.SerializationInfoEnumerator
extern TypeInfo SerializationInfoEnumerator_t1176_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationInfoEnumerator
#include "mscorlib_System_Runtime_Serialization_SerializationInfoEnumeMethodDeclarations.h"
static const EncodedMethodIndex SerializationInfoEnumerator_t1176_VTable[6] = 
{
	120,
	125,
	122,
	123,
	2691,
	2692,
};
extern const Il2CppType IEnumerator_t279_0_0_0;
static const Il2CppType* SerializationInfoEnumerator_t1176_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair SerializationInfoEnumerator_t1176_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationInfoEnumerator_t1176_0_0_0;
extern const Il2CppType SerializationInfoEnumerator_t1176_1_0_0;
struct SerializationInfoEnumerator_t1176;
const Il2CppTypeDefinitionMetadata SerializationInfoEnumerator_t1176_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SerializationInfoEnumerator_t1176_InterfacesTypeInfos/* implementedInterfaces */
	, SerializationInfoEnumerator_t1176_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationInfoEnumerator_t1176_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4658/* fieldStart */
	, 7091/* methodStart */
	, -1/* eventStart */
	, 1474/* propertyStart */

};
TypeInfo SerializationInfoEnumerator_t1176_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationInfoEnumerator"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &SerializationInfoEnumerator_t1176_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1169/* custom_attributes_cache */
	, &SerializationInfoEnumerator_t1176_0_0_0/* byval_arg */
	, &SerializationInfoEnumerator_t1176_1_0_0/* this_arg */
	, &SerializationInfoEnumerator_t1176_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationInfoEnumerator_t1176)/* instance_size */
	, sizeof (SerializationInfoEnumerator_t1176)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 4/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// Metadata Definition System.Runtime.Serialization.StreamingContext
extern TypeInfo StreamingContext_t311_il2cpp_TypeInfo;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContextMethodDeclarations.h"
static const EncodedMethodIndex StreamingContext_t311_VTable[4] = 
{
	2693,
	125,
	2694,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StreamingContext_t311_0_0_0;
extern const Il2CppType StreamingContext_t311_1_0_0;
const Il2CppTypeDefinitionMetadata StreamingContext_t311_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, StreamingContext_t311_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4659/* fieldStart */
	, 7097/* methodStart */
	, -1/* eventStart */
	, 1478/* propertyStart */

};
TypeInfo StreamingContext_t311_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StreamingContext"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &StreamingContext_t311_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1170/* custom_attributes_cache */
	, &StreamingContext_t311_0_0_0/* byval_arg */
	, &StreamingContext_t311_1_0_0/* this_arg */
	, &StreamingContext_t311_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StreamingContext_t311)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StreamingContext_t311)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
// Metadata Definition System.Runtime.Serialization.StreamingContextStates
extern TypeInfo StreamingContextStates_t1177_il2cpp_TypeInfo;
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStatesMethodDeclarations.h"
static const EncodedMethodIndex StreamingContextStates_t1177_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair StreamingContextStates_t1177_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StreamingContextStates_t1177_0_0_0;
extern const Il2CppType StreamingContextStates_t1177_1_0_0;
const Il2CppTypeDefinitionMetadata StreamingContextStates_t1177_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StreamingContextStates_t1177_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, StreamingContextStates_t1177_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4661/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StreamingContextStates_t1177_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StreamingContextStates"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1171/* custom_attributes_cache */
	, &StreamingContextStates_t1177_0_0_0/* byval_arg */
	, &StreamingContextStates_t1177_1_0_0/* this_arg */
	, &StreamingContextStates_t1177_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StreamingContextStates_t1177)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StreamingContextStates_t1177)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Certificate
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509C.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Certificate
extern TypeInfo X509Certificate_t449_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Certificate
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509CMethodDeclarations.h"
static const EncodedMethodIndex X509Certificate_t449_VTable[18] = 
{
	546,
	125,
	547,
	2695,
	549,
	550,
	551,
	552,
	553,
	554,
	555,
	556,
	557,
	558,
	559,
	2696,
	2697,
	2698,
};
static const Il2CppType* X509Certificate_t449_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
	&IDeserializationCallback_t1429_0_0_0,
};
static Il2CppInterfaceOffsetPair X509Certificate_t449_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &IDeserializationCallback_t1429_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509Certificate_t449_0_0_0;
extern const Il2CppType X509Certificate_t449_1_0_0;
struct X509Certificate_t449;
const Il2CppTypeDefinitionMetadata X509Certificate_t449_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509Certificate_t449_InterfacesTypeInfos/* implementedInterfaces */
	, X509Certificate_t449_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Certificate_t449_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4671/* fieldStart */
	, 7102/* methodStart */
	, -1/* eventStart */
	, 1479/* propertyStart */

};
TypeInfo X509Certificate_t449_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Certificate"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Certificate_t449_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1172/* custom_attributes_cache */
	, &X509Certificate_t449_0_0_0/* byval_arg */
	, &X509Certificate_t449_1_0_0/* this_arg */
	, &X509Certificate_t449_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Certificate_t449)/* instance_size */
	, sizeof (X509Certificate_t449)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509KeyStorageFlags
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509K.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509KeyStorageFlags
extern TypeInfo X509KeyStorageFlags_t1178_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509KeyStorageFlags
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509KMethodDeclarations.h"
static const EncodedMethodIndex X509KeyStorageFlags_t1178_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair X509KeyStorageFlags_t1178_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509KeyStorageFlags_t1178_0_0_0;
extern const Il2CppType X509KeyStorageFlags_t1178_1_0_0;
const Il2CppTypeDefinitionMetadata X509KeyStorageFlags_t1178_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509KeyStorageFlags_t1178_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, X509KeyStorageFlags_t1178_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4676/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509KeyStorageFlags_t1178_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509KeyStorageFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1178/* custom_attributes_cache */
	, &X509KeyStorageFlags_t1178_0_0_0/* byval_arg */
	, &X509KeyStorageFlags_t1178_1_0_0/* this_arg */
	, &X509KeyStorageFlags_t1178_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509KeyStorageFlags_t1178)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509KeyStorageFlags_t1178)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.AsymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_AsymmetricAlgorithm.h"
// Metadata Definition System.Security.Cryptography.AsymmetricAlgorithm
extern TypeInfo AsymmetricAlgorithm_t436_il2cpp_TypeInfo;
// System.Security.Cryptography.AsymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_AsymmetricAlgorithmMethodDeclarations.h"
static const EncodedMethodIndex AsymmetricAlgorithm_t436_VTable[10] = 
{
	120,
	125,
	122,
	123,
	828,
	2699,
	830,
	0,
	0,
	0,
};
extern const Il2CppType IDisposable_t319_0_0_0;
static const Il2CppType* AsymmetricAlgorithm_t436_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
};
static Il2CppInterfaceOffsetPair AsymmetricAlgorithm_t436_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsymmetricAlgorithm_t436_0_0_0;
extern const Il2CppType AsymmetricAlgorithm_t436_1_0_0;
struct AsymmetricAlgorithm_t436;
const Il2CppTypeDefinitionMetadata AsymmetricAlgorithm_t436_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AsymmetricAlgorithm_t436_InterfacesTypeInfos/* implementedInterfaces */
	, AsymmetricAlgorithm_t436_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsymmetricAlgorithm_t436_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4683/* fieldStart */
	, 7126/* methodStart */
	, -1/* eventStart */
	, 1481/* propertyStart */

};
TypeInfo AsymmetricAlgorithm_t436_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsymmetricAlgorithm"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AsymmetricAlgorithm_t436_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1179/* custom_attributes_cache */
	, &AsymmetricAlgorithm_t436_0_0_0/* byval_arg */
	, &AsymmetricAlgorithm_t436_1_0_0/* this_arg */
	, &AsymmetricAlgorithm_t436_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsymmetricAlgorithm_t436)/* instance_size */
	, sizeof (AsymmetricAlgorithm_t436)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricKeyExchangeF.h"
// Metadata Definition System.Security.Cryptography.AsymmetricKeyExchangeFormatter
extern TypeInfo AsymmetricKeyExchangeFormatter_t1179_il2cpp_TypeInfo;
// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricKeyExchangeFMethodDeclarations.h"
static const EncodedMethodIndex AsymmetricKeyExchangeFormatter_t1179_VTable[5] = 
{
	120,
	125,
	122,
	123,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsymmetricKeyExchangeFormatter_t1179_0_0_0;
extern const Il2CppType AsymmetricKeyExchangeFormatter_t1179_1_0_0;
struct AsymmetricKeyExchangeFormatter_t1179;
const Il2CppTypeDefinitionMetadata AsymmetricKeyExchangeFormatter_t1179_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsymmetricKeyExchangeFormatter_t1179_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7135/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AsymmetricKeyExchangeFormatter_t1179_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsymmetricKeyExchangeFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AsymmetricKeyExchangeFormatter_t1179_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1180/* custom_attributes_cache */
	, &AsymmetricKeyExchangeFormatter_t1179_0_0_0/* byval_arg */
	, &AsymmetricKeyExchangeFormatter_t1179_1_0_0/* this_arg */
	, &AsymmetricKeyExchangeFormatter_t1179_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsymmetricKeyExchangeFormatter_t1179)/* instance_size */
	, sizeof (AsymmetricKeyExchangeFormatter_t1179)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.AsymmetricSignatureDeformatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDef.h"
// Metadata Definition System.Security.Cryptography.AsymmetricSignatureDeformatter
extern TypeInfo AsymmetricSignatureDeformatter_t697_il2cpp_TypeInfo;
// System.Security.Cryptography.AsymmetricSignatureDeformatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDefMethodDeclarations.h"
static const EncodedMethodIndex AsymmetricSignatureDeformatter_t697_VTable[7] = 
{
	120,
	125,
	122,
	123,
	0,
	0,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsymmetricSignatureDeformatter_t697_0_0_0;
extern const Il2CppType AsymmetricSignatureDeformatter_t697_1_0_0;
struct AsymmetricSignatureDeformatter_t697;
const Il2CppTypeDefinitionMetadata AsymmetricSignatureDeformatter_t697_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsymmetricSignatureDeformatter_t697_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7137/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AsymmetricSignatureDeformatter_t697_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsymmetricSignatureDeformatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AsymmetricSignatureDeformatter_t697_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1181/* custom_attributes_cache */
	, &AsymmetricSignatureDeformatter_t697_0_0_0/* byval_arg */
	, &AsymmetricSignatureDeformatter_t697_1_0_0/* this_arg */
	, &AsymmetricSignatureDeformatter_t697_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsymmetricSignatureDeformatter_t697)/* instance_size */
	, sizeof (AsymmetricSignatureDeformatter_t697)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureFor.h"
// Metadata Definition System.Security.Cryptography.AsymmetricSignatureFormatter
extern TypeInfo AsymmetricSignatureFormatter_t699_il2cpp_TypeInfo;
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureForMethodDeclarations.h"
static const EncodedMethodIndex AsymmetricSignatureFormatter_t699_VTable[7] = 
{
	120,
	125,
	122,
	123,
	0,
	0,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsymmetricSignatureFormatter_t699_0_0_0;
extern const Il2CppType AsymmetricSignatureFormatter_t699_1_0_0;
struct AsymmetricSignatureFormatter_t699;
const Il2CppTypeDefinitionMetadata AsymmetricSignatureFormatter_t699_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsymmetricSignatureFormatter_t699_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7141/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AsymmetricSignatureFormatter_t699_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsymmetricSignatureFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AsymmetricSignatureFormatter_t699_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1182/* custom_attributes_cache */
	, &AsymmetricSignatureFormatter_t699_0_0_0/* byval_arg */
	, &AsymmetricSignatureFormatter_t699_1_0_0/* this_arg */
	, &AsymmetricSignatureFormatter_t699_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsymmetricSignatureFormatter_t699)/* instance_size */
	, sizeof (AsymmetricSignatureFormatter_t699)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.Base64Constants
#include "mscorlib_System_Security_Cryptography_Base64Constants.h"
// Metadata Definition System.Security.Cryptography.Base64Constants
extern TypeInfo Base64Constants_t1180_il2cpp_TypeInfo;
// System.Security.Cryptography.Base64Constants
#include "mscorlib_System_Security_Cryptography_Base64ConstantsMethodDeclarations.h"
static const EncodedMethodIndex Base64Constants_t1180_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Base64Constants_t1180_0_0_0;
extern const Il2CppType Base64Constants_t1180_1_0_0;
struct Base64Constants_t1180;
const Il2CppTypeDefinitionMetadata Base64Constants_t1180_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Base64Constants_t1180_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4685/* fieldStart */
	, 7145/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Base64Constants_t1180_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Base64Constants"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Base64Constants_t1180_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Base64Constants_t1180_0_0_0/* byval_arg */
	, &Base64Constants_t1180_1_0_0/* this_arg */
	, &Base64Constants_t1180_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Base64Constants_t1180)/* instance_size */
	, sizeof (Base64Constants_t1180)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Base64Constants_t1180_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
// Metadata Definition System.Security.Cryptography.CipherMode
extern TypeInfo CipherMode_t617_il2cpp_TypeInfo;
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherModeMethodDeclarations.h"
static const EncodedMethodIndex CipherMode_t617_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair CipherMode_t617_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CipherMode_t617_0_0_0;
extern const Il2CppType CipherMode_t617_1_0_0;
const Il2CppTypeDefinitionMetadata CipherMode_t617_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CipherMode_t617_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, CipherMode_t617_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4687/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CipherMode_t617_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CipherMode"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1183/* custom_attributes_cache */
	, &CipherMode_t617_0_0_0/* byval_arg */
	, &CipherMode_t617_1_0_0/* this_arg */
	, &CipherMode_t617_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CipherMode_t617)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CipherMode_t617)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.CryptoConfig
#include "mscorlib_System_Security_Cryptography_CryptoConfig.h"
// Metadata Definition System.Security.Cryptography.CryptoConfig
extern TypeInfo CryptoConfig_t591_il2cpp_TypeInfo;
// System.Security.Cryptography.CryptoConfig
#include "mscorlib_System_Security_Cryptography_CryptoConfigMethodDeclarations.h"
static const EncodedMethodIndex CryptoConfig_t591_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CryptoConfig_t591_0_0_0;
extern const Il2CppType CryptoConfig_t591_1_0_0;
struct CryptoConfig_t591;
const Il2CppTypeDefinitionMetadata CryptoConfig_t591_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CryptoConfig_t591_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4693/* fieldStart */
	, 7146/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CryptoConfig_t591_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CryptoConfig"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &CryptoConfig_t591_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1184/* custom_attributes_cache */
	, &CryptoConfig_t591_0_0_0/* byval_arg */
	, &CryptoConfig_t591_1_0_0/* this_arg */
	, &CryptoConfig_t591_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CryptoConfig_t591)/* instance_size */
	, sizeof (CryptoConfig_t591)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CryptoConfig_t591_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.CryptographicException
#include "mscorlib_System_Security_Cryptography_CryptographicException.h"
// Metadata Definition System.Security.Cryptography.CryptographicException
extern TypeInfo CryptographicException_t587_il2cpp_TypeInfo;
// System.Security.Cryptography.CryptographicException
#include "mscorlib_System_Security_Cryptography_CryptographicExceptionMethodDeclarations.h"
static const EncodedMethodIndex CryptographicException_t587_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static const Il2CppType* CryptographicException_t587_InterfacesTypeInfos[] = 
{
	&_Exception_t2066_0_0_0,
};
static Il2CppInterfaceOffsetPair CryptographicException_t587_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CryptographicException_t587_0_0_0;
extern const Il2CppType CryptographicException_t587_1_0_0;
struct CryptographicException_t587;
const Il2CppTypeDefinitionMetadata CryptographicException_t587_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CryptographicException_t587_InterfacesTypeInfos/* implementedInterfaces */
	, CryptographicException_t587_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, CryptographicException_t587_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7153/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CryptographicException_t587_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CryptographicException"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &CryptographicException_t587_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1186/* custom_attributes_cache */
	, &CryptographicException_t587_0_0_0/* byval_arg */
	, &CryptographicException_t587_1_0_0/* this_arg */
	, &CryptographicException_t587_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CryptographicException_t587)/* instance_size */
	, sizeof (CryptographicException_t587)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.CryptographicUnexpectedOperationException
#include "mscorlib_System_Security_Cryptography_CryptographicUnexpecte.h"
// Metadata Definition System.Security.Cryptography.CryptographicUnexpectedOperationException
extern TypeInfo CryptographicUnexpectedOperationException_t592_il2cpp_TypeInfo;
// System.Security.Cryptography.CryptographicUnexpectedOperationException
#include "mscorlib_System_Security_Cryptography_CryptographicUnexpecteMethodDeclarations.h"
static const EncodedMethodIndex CryptographicUnexpectedOperationException_t592_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair CryptographicUnexpectedOperationException_t592_InterfacesOffsets[] = 
{
	{ &_Exception_t2066_0_0_0, 5},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CryptographicUnexpectedOperationException_t592_0_0_0;
extern const Il2CppType CryptographicUnexpectedOperationException_t592_1_0_0;
struct CryptographicUnexpectedOperationException_t592;
const Il2CppTypeDefinitionMetadata CryptographicUnexpectedOperationException_t592_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CryptographicUnexpectedOperationException_t592_InterfacesOffsets/* interfaceOffsets */
	, &CryptographicException_t587_0_0_0/* parent */
	, CryptographicUnexpectedOperationException_t592_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7158/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CryptographicUnexpectedOperationException_t592_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CryptographicUnexpectedOperationException"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &CryptographicUnexpectedOperationException_t592_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1187/* custom_attributes_cache */
	, &CryptographicUnexpectedOperationException_t592_0_0_0/* byval_arg */
	, &CryptographicUnexpectedOperationException_t592_1_0_0/* this_arg */
	, &CryptographicUnexpectedOperationException_t592_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CryptographicUnexpectedOperationException_t592)/* instance_size */
	, sizeof (CryptographicUnexpectedOperationException_t592)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.CspParameters
#include "mscorlib_System_Security_Cryptography_CspParameters.h"
// Metadata Definition System.Security.Cryptography.CspParameters
extern TypeInfo CspParameters_t740_il2cpp_TypeInfo;
// System.Security.Cryptography.CspParameters
#include "mscorlib_System_Security_Cryptography_CspParametersMethodDeclarations.h"
static const EncodedMethodIndex CspParameters_t740_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CspParameters_t740_0_0_0;
extern const Il2CppType CspParameters_t740_1_0_0;
struct CspParameters_t740;
const Il2CppTypeDefinitionMetadata CspParameters_t740_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CspParameters_t740_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4696/* fieldStart */
	, 7161/* methodStart */
	, -1/* eventStart */
	, 1482/* propertyStart */

};
TypeInfo CspParameters_t740_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CspParameters"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &CspParameters_t740_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1188/* custom_attributes_cache */
	, &CspParameters_t740_0_0_0/* byval_arg */
	, &CspParameters_t740_1_0_0/* this_arg */
	, &CspParameters_t740_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CspParameters_t740)/* instance_size */
	, sizeof (CspParameters_t740)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
// Metadata Definition System.Security.Cryptography.CspProviderFlags
extern TypeInfo CspProviderFlags_t1181_il2cpp_TypeInfo;
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlagsMethodDeclarations.h"
static const EncodedMethodIndex CspProviderFlags_t1181_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair CspProviderFlags_t1181_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CspProviderFlags_t1181_0_0_0;
extern const Il2CppType CspProviderFlags_t1181_1_0_0;
const Il2CppTypeDefinitionMetadata CspProviderFlags_t1181_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CspProviderFlags_t1181_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, CspProviderFlags_t1181_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4701/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CspProviderFlags_t1181_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CspProviderFlags"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1189/* custom_attributes_cache */
	, &CspProviderFlags_t1181_0_0_0/* byval_arg */
	, &CspProviderFlags_t1181_1_0_0/* this_arg */
	, &CspProviderFlags_t1181_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CspProviderFlags_t1181)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CspProviderFlags_t1181)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.DES
#include "mscorlib_System_Security_Cryptography_DES.h"
// Metadata Definition System.Security.Cryptography.DES
extern TypeInfo DES_t749_il2cpp_TypeInfo;
// System.Security.Cryptography.DES
#include "mscorlib_System_Security_Cryptography_DESMethodDeclarations.h"
static const EncodedMethodIndex DES_t749_VTable[26] = 
{
	120,
	759,
	122,
	123,
	760,
	761,
	762,
	763,
	764,
	765,
	766,
	2700,
	2701,
	769,
	770,
	771,
	772,
	773,
	774,
	775,
	776,
	0,
	777,
	0,
	0,
	0,
};
static Il2CppInterfaceOffsetPair DES_t749_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DES_t749_0_0_0;
extern const Il2CppType DES_t749_1_0_0;
extern const Il2CppType SymmetricAlgorithm_t606_0_0_0;
struct DES_t749;
const Il2CppTypeDefinitionMetadata DES_t749_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DES_t749_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricAlgorithm_t606_0_0_0/* parent */
	, DES_t749_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4710/* fieldStart */
	, 7167/* methodStart */
	, -1/* eventStart */
	, 1483/* propertyStart */

};
TypeInfo DES_t749_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DES"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DES_t749_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1190/* custom_attributes_cache */
	, &DES_t749_0_0_0/* byval_arg */
	, &DES_t749_1_0_0/* this_arg */
	, &DES_t749_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DES_t749)/* instance_size */
	, sizeof (DES_t749)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DES_t749_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.DESTransform
#include "mscorlib_System_Security_Cryptography_DESTransform.h"
// Metadata Definition System.Security.Cryptography.DESTransform
extern TypeInfo DESTransform_t1183_il2cpp_TypeInfo;
// System.Security.Cryptography.DESTransform
#include "mscorlib_System_Security_Cryptography_DESTransformMethodDeclarations.h"
static const EncodedMethodIndex DESTransform_t1183_VTable[18] = 
{
	120,
	1534,
	122,
	123,
	1535,
	1536,
	1537,
	1538,
	1539,
	1536,
	1540,
	2702,
	1541,
	1542,
	1543,
	1544,
	1537,
	1538,
};
extern const Il2CppType ICryptoTransform_t616_0_0_0;
static Il2CppInterfaceOffsetPair DESTransform_t1183_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DESTransform_t1183_0_0_0;
extern const Il2CppType DESTransform_t1183_1_0_0;
extern const Il2CppType SymmetricTransform_t839_0_0_0;
struct DESTransform_t1183;
const Il2CppTypeDefinitionMetadata DESTransform_t1183_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DESTransform_t1183_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricTransform_t839_0_0_0/* parent */
	, DESTransform_t1183_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4712/* fieldStart */
	, 7175/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DESTransform_t1183_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DESTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DESTransform_t1183_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DESTransform_t1183_0_0_0/* byval_arg */
	, &DESTransform_t1183_1_0_0/* this_arg */
	, &DESTransform_t1183_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DESTransform_t1183)/* instance_size */
	, sizeof (DESTransform_t1183)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DESTransform_t1183_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.DESCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DESCryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.DESCryptoServiceProvider
extern TypeInfo DESCryptoServiceProvider_t1184_il2cpp_TypeInfo;
// System.Security.Cryptography.DESCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DESCryptoServiceProvidMethodDeclarations.h"
static const EncodedMethodIndex DESCryptoServiceProvider_t1184_VTable[26] = 
{
	120,
	759,
	122,
	123,
	760,
	761,
	762,
	763,
	764,
	765,
	766,
	2700,
	2701,
	769,
	770,
	771,
	772,
	773,
	774,
	775,
	776,
	2703,
	777,
	2704,
	2705,
	2706,
};
static Il2CppInterfaceOffsetPair DESCryptoServiceProvider_t1184_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DESCryptoServiceProvider_t1184_0_0_0;
extern const Il2CppType DESCryptoServiceProvider_t1184_1_0_0;
struct DESCryptoServiceProvider_t1184;
const Il2CppTypeDefinitionMetadata DESCryptoServiceProvider_t1184_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DESCryptoServiceProvider_t1184_InterfacesOffsets/* interfaceOffsets */
	, &DES_t749_0_0_0/* parent */
	, DESCryptoServiceProvider_t1184_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7184/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DESCryptoServiceProvider_t1184_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DESCryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DESCryptoServiceProvider_t1184_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1191/* custom_attributes_cache */
	, &DESCryptoServiceProvider_t1184_0_0_0/* byval_arg */
	, &DESCryptoServiceProvider_t1184_1_0_0/* this_arg */
	, &DESCryptoServiceProvider_t1184_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DESCryptoServiceProvider_t1184)/* instance_size */
	, sizeof (DESCryptoServiceProvider_t1184)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.DSA
#include "mscorlib_System_Security_Cryptography_DSA.h"
// Metadata Definition System.Security.Cryptography.DSA
extern TypeInfo DSA_t561_il2cpp_TypeInfo;
// System.Security.Cryptography.DSA
#include "mscorlib_System_Security_Cryptography_DSAMethodDeclarations.h"
static const EncodedMethodIndex DSA_t561_VTable[14] = 
{
	120,
	125,
	122,
	123,
	828,
	2699,
	830,
	0,
	1514,
	1515,
	0,
	0,
	0,
	0,
};
static Il2CppInterfaceOffsetPair DSA_t561_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSA_t561_0_0_0;
extern const Il2CppType DSA_t561_1_0_0;
struct DSA_t561;
const Il2CppTypeDefinitionMetadata DSA_t561_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DSA_t561_InterfacesOffsets/* interfaceOffsets */
	, &AsymmetricAlgorithm_t436_0_0_0/* parent */
	, DSA_t561_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7189/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DSA_t561_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSA"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSA_t561_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1192/* custom_attributes_cache */
	, &DSA_t561_0_0_0/* byval_arg */
	, &DSA_t561_1_0_0/* this_arg */
	, &DSA_t561_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSA_t561)/* instance_size */
	, sizeof (DSA_t561)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.DSACryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DSACryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.DSACryptoServiceProvider
extern TypeInfo DSACryptoServiceProvider_t585_il2cpp_TypeInfo;
// System.Security.Cryptography.DSACryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DSACryptoServiceProvidMethodDeclarations.h"
static const EncodedMethodIndex DSACryptoServiceProvider_t585_VTable[14] = 
{
	120,
	2707,
	122,
	123,
	828,
	2708,
	830,
	2709,
	1514,
	1515,
	2710,
	2711,
	2712,
	2713,
};
extern const Il2CppType ICspAsymmetricAlgorithm_t2116_0_0_0;
static const Il2CppType* DSACryptoServiceProvider_t585_InterfacesTypeInfos[] = 
{
	&ICspAsymmetricAlgorithm_t2116_0_0_0,
};
static Il2CppInterfaceOffsetPair DSACryptoServiceProvider_t585_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICspAsymmetricAlgorithm_t2116_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSACryptoServiceProvider_t585_0_0_0;
extern const Il2CppType DSACryptoServiceProvider_t585_1_0_0;
struct DSACryptoServiceProvider_t585;
const Il2CppTypeDefinitionMetadata DSACryptoServiceProvider_t585_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DSACryptoServiceProvider_t585_InterfacesTypeInfos/* implementedInterfaces */
	, DSACryptoServiceProvider_t585_InterfacesOffsets/* interfaceOffsets */
	, &DSA_t561_0_0_0/* parent */
	, DSACryptoServiceProvider_t585_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4725/* fieldStart */
	, 7199/* methodStart */
	, -1/* eventStart */
	, 1484/* propertyStart */

};
TypeInfo DSACryptoServiceProvider_t585_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSACryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSACryptoServiceProvider_t585_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1193/* custom_attributes_cache */
	, &DSACryptoServiceProvider_t585_0_0_0/* byval_arg */
	, &DSACryptoServiceProvider_t585_1_0_0/* this_arg */
	, &DSACryptoServiceProvider_t585_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSACryptoServiceProvider_t585)/* instance_size */
	, sizeof (DSACryptoServiceProvider_t585)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DSACryptoServiceProvider_t585_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
// Metadata Definition System.Security.Cryptography.DSAParameters
extern TypeInfo DSAParameters_t586_il2cpp_TypeInfo;
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParametersMethodDeclarations.h"
static const EncodedMethodIndex DSAParameters_t586_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSAParameters_t586_0_0_0;
extern const Il2CppType DSAParameters_t586_1_0_0;
const Il2CppTypeDefinitionMetadata DSAParameters_t586_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, DSAParameters_t586_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4732/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DSAParameters_t586_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSAParameters"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSAParameters_t586_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1195/* custom_attributes_cache */
	, &DSAParameters_t586_0_0_0/* byval_arg */
	, &DSAParameters_t586_1_0_0/* this_arg */
	, &DSAParameters_t586_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)DSAParameters_t586_marshal/* marshal_to_native_func */
	, (methodPointerType)DSAParameters_t586_marshal_back/* marshal_from_native_func */
	, (methodPointerType)DSAParameters_t586_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (DSAParameters_t586)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DSAParameters_t586)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(DSAParameters_t586_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.DSASignatureDeformatter
#include "mscorlib_System_Security_Cryptography_DSASignatureDeformatte.h"
// Metadata Definition System.Security.Cryptography.DSASignatureDeformatter
extern TypeInfo DSASignatureDeformatter_t745_il2cpp_TypeInfo;
// System.Security.Cryptography.DSASignatureDeformatter
#include "mscorlib_System_Security_Cryptography_DSASignatureDeformatteMethodDeclarations.h"
static const EncodedMethodIndex DSASignatureDeformatter_t745_VTable[7] = 
{
	120,
	125,
	122,
	123,
	2714,
	2715,
	2716,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSASignatureDeformatter_t745_0_0_0;
extern const Il2CppType DSASignatureDeformatter_t745_1_0_0;
struct DSASignatureDeformatter_t745;
const Il2CppTypeDefinitionMetadata DSASignatureDeformatter_t745_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricSignatureDeformatter_t697_0_0_0/* parent */
	, DSASignatureDeformatter_t745_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4740/* fieldStart */
	, 7212/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DSASignatureDeformatter_t745_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSASignatureDeformatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSASignatureDeformatter_t745_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1196/* custom_attributes_cache */
	, &DSASignatureDeformatter_t745_0_0_0/* byval_arg */
	, &DSASignatureDeformatter_t745_1_0_0/* this_arg */
	, &DSASignatureDeformatter_t745_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSASignatureDeformatter_t745)/* instance_size */
	, sizeof (DSASignatureDeformatter_t745)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.DSASignatureFormatter
#include "mscorlib_System_Security_Cryptography_DSASignatureFormatter.h"
// Metadata Definition System.Security.Cryptography.DSASignatureFormatter
extern TypeInfo DSASignatureFormatter_t1185_il2cpp_TypeInfo;
// System.Security.Cryptography.DSASignatureFormatter
#include "mscorlib_System_Security_Cryptography_DSASignatureFormatterMethodDeclarations.h"
static const EncodedMethodIndex DSASignatureFormatter_t1185_VTable[7] = 
{
	120,
	125,
	122,
	123,
	2717,
	2718,
	2719,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSASignatureFormatter_t1185_0_0_0;
extern const Il2CppType DSASignatureFormatter_t1185_1_0_0;
struct DSASignatureFormatter_t1185;
const Il2CppTypeDefinitionMetadata DSASignatureFormatter_t1185_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricSignatureFormatter_t699_0_0_0/* parent */
	, DSASignatureFormatter_t1185_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4741/* fieldStart */
	, 7217/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DSASignatureFormatter_t1185_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSASignatureFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSASignatureFormatter_t1185_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1197/* custom_attributes_cache */
	, &DSASignatureFormatter_t1185_0_0_0/* byval_arg */
	, &DSASignatureFormatter_t1185_1_0_0/* this_arg */
	, &DSASignatureFormatter_t1185_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSASignatureFormatter_t1185)/* instance_size */
	, sizeof (DSASignatureFormatter_t1185)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.HMAC
#include "mscorlib_System_Security_Cryptography_HMAC.h"
// Metadata Definition System.Security.Cryptography.HMAC
extern TypeInfo HMAC_t742_il2cpp_TypeInfo;
// System.Security.Cryptography.HMAC
#include "mscorlib_System_Security_Cryptography_HMACMethodDeclarations.h"
static const EncodedMethodIndex HMAC_t742_VTable[17] = 
{
	120,
	884,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2720,
	2721,
	822,
	2722,
	2723,
	2724,
	2725,
};
static Il2CppInterfaceOffsetPair HMAC_t742_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMAC_t742_0_0_0;
extern const Il2CppType HMAC_t742_1_0_0;
extern const Il2CppType KeyedHashAlgorithm_t664_0_0_0;
struct HMAC_t742;
const Il2CppTypeDefinitionMetadata HMAC_t742_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMAC_t742_InterfacesOffsets/* interfaceOffsets */
	, &KeyedHashAlgorithm_t664_0_0_0/* parent */
	, HMAC_t742_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4742/* fieldStart */
	, 7221/* methodStart */
	, -1/* eventStart */
	, 1486/* propertyStart */

};
TypeInfo HMAC_t742_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMAC"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMAC_t742_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1198/* custom_attributes_cache */
	, &HMAC_t742_0_0_0/* byval_arg */
	, &HMAC_t742_1_0_0/* this_arg */
	, &HMAC_t742_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMAC_t742)/* instance_size */
	, sizeof (HMAC_t742)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACMD5
#include "mscorlib_System_Security_Cryptography_HMACMD5.h"
// Metadata Definition System.Security.Cryptography.HMACMD5
extern TypeInfo HMACMD5_t1186_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACMD5
#include "mscorlib_System_Security_Cryptography_HMACMD5MethodDeclarations.h"
static const EncodedMethodIndex HMACMD5_t1186_VTable[17] = 
{
	120,
	884,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2720,
	2721,
	822,
	2722,
	2723,
	2724,
	2725,
};
static Il2CppInterfaceOffsetPair HMACMD5_t1186_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACMD5_t1186_0_0_0;
extern const Il2CppType HMACMD5_t1186_1_0_0;
struct HMACMD5_t1186;
const Il2CppTypeDefinitionMetadata HMACMD5_t1186_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACMD5_t1186_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t742_0_0_0/* parent */
	, HMACMD5_t1186_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7235/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HMACMD5_t1186_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACMD5"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMACMD5_t1186_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1199/* custom_attributes_cache */
	, &HMACMD5_t1186_0_0_0/* byval_arg */
	, &HMACMD5_t1186_1_0_0/* this_arg */
	, &HMACMD5_t1186_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACMD5_t1186)/* instance_size */
	, sizeof (HMACMD5_t1186)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACRIPEMD160
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD160.h"
// Metadata Definition System.Security.Cryptography.HMACRIPEMD160
extern TypeInfo HMACRIPEMD160_t1187_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACRIPEMD160
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD160MethodDeclarations.h"
static const EncodedMethodIndex HMACRIPEMD160_t1187_VTable[17] = 
{
	120,
	884,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2720,
	2721,
	822,
	2722,
	2723,
	2724,
	2725,
};
static Il2CppInterfaceOffsetPair HMACRIPEMD160_t1187_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACRIPEMD160_t1187_0_0_0;
extern const Il2CppType HMACRIPEMD160_t1187_1_0_0;
struct HMACRIPEMD160_t1187;
const Il2CppTypeDefinitionMetadata HMACRIPEMD160_t1187_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACRIPEMD160_t1187_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t742_0_0_0/* parent */
	, HMACRIPEMD160_t1187_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7237/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HMACRIPEMD160_t1187_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACRIPEMD160"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMACRIPEMD160_t1187_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1200/* custom_attributes_cache */
	, &HMACRIPEMD160_t1187_0_0_0/* byval_arg */
	, &HMACRIPEMD160_t1187_1_0_0/* this_arg */
	, &HMACRIPEMD160_t1187_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACRIPEMD160_t1187)/* instance_size */
	, sizeof (HMACRIPEMD160_t1187)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACSHA1
#include "mscorlib_System_Security_Cryptography_HMACSHA1.h"
// Metadata Definition System.Security.Cryptography.HMACSHA1
extern TypeInfo HMACSHA1_t741_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACSHA1
#include "mscorlib_System_Security_Cryptography_HMACSHA1MethodDeclarations.h"
static const EncodedMethodIndex HMACSHA1_t741_VTable[17] = 
{
	120,
	884,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2720,
	2721,
	822,
	2722,
	2723,
	2724,
	2725,
};
static Il2CppInterfaceOffsetPair HMACSHA1_t741_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACSHA1_t741_0_0_0;
extern const Il2CppType HMACSHA1_t741_1_0_0;
struct HMACSHA1_t741;
const Il2CppTypeDefinitionMetadata HMACSHA1_t741_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACSHA1_t741_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t742_0_0_0/* parent */
	, HMACSHA1_t741_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7239/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HMACSHA1_t741_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACSHA1"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMACSHA1_t741_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1201/* custom_attributes_cache */
	, &HMACSHA1_t741_0_0_0/* byval_arg */
	, &HMACSHA1_t741_1_0_0/* this_arg */
	, &HMACSHA1_t741_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACSHA1_t741)/* instance_size */
	, sizeof (HMACSHA1_t741)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACSHA256
#include "mscorlib_System_Security_Cryptography_HMACSHA256.h"
// Metadata Definition System.Security.Cryptography.HMACSHA256
extern TypeInfo HMACSHA256_t1188_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACSHA256
#include "mscorlib_System_Security_Cryptography_HMACSHA256MethodDeclarations.h"
static const EncodedMethodIndex HMACSHA256_t1188_VTable[17] = 
{
	120,
	884,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2720,
	2721,
	822,
	2722,
	2723,
	2724,
	2725,
};
static Il2CppInterfaceOffsetPair HMACSHA256_t1188_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACSHA256_t1188_0_0_0;
extern const Il2CppType HMACSHA256_t1188_1_0_0;
struct HMACSHA256_t1188;
const Il2CppTypeDefinitionMetadata HMACSHA256_t1188_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACSHA256_t1188_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t742_0_0_0/* parent */
	, HMACSHA256_t1188_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7241/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HMACSHA256_t1188_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACSHA256"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMACSHA256_t1188_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1202/* custom_attributes_cache */
	, &HMACSHA256_t1188_0_0_0/* byval_arg */
	, &HMACSHA256_t1188_1_0_0/* this_arg */
	, &HMACSHA256_t1188_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACSHA256_t1188)/* instance_size */
	, sizeof (HMACSHA256_t1188)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACSHA384
#include "mscorlib_System_Security_Cryptography_HMACSHA384.h"
// Metadata Definition System.Security.Cryptography.HMACSHA384
extern TypeInfo HMACSHA384_t1189_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACSHA384
#include "mscorlib_System_Security_Cryptography_HMACSHA384MethodDeclarations.h"
static const EncodedMethodIndex HMACSHA384_t1189_VTable[17] = 
{
	120,
	884,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2720,
	2721,
	822,
	2722,
	2723,
	2724,
	2725,
};
static Il2CppInterfaceOffsetPair HMACSHA384_t1189_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACSHA384_t1189_0_0_0;
extern const Il2CppType HMACSHA384_t1189_1_0_0;
struct HMACSHA384_t1189;
const Il2CppTypeDefinitionMetadata HMACSHA384_t1189_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACSHA384_t1189_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t742_0_0_0/* parent */
	, HMACSHA384_t1189_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4747/* fieldStart */
	, 7243/* methodStart */
	, -1/* eventStart */
	, 1490/* propertyStart */

};
TypeInfo HMACSHA384_t1189_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACSHA384"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMACSHA384_t1189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1203/* custom_attributes_cache */
	, &HMACSHA384_t1189_0_0_0/* byval_arg */
	, &HMACSHA384_t1189_1_0_0/* this_arg */
	, &HMACSHA384_t1189_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACSHA384_t1189)/* instance_size */
	, sizeof (HMACSHA384_t1189)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(HMACSHA384_t1189_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACSHA512
#include "mscorlib_System_Security_Cryptography_HMACSHA512.h"
// Metadata Definition System.Security.Cryptography.HMACSHA512
extern TypeInfo HMACSHA512_t1190_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACSHA512
#include "mscorlib_System_Security_Cryptography_HMACSHA512MethodDeclarations.h"
static const EncodedMethodIndex HMACSHA512_t1190_VTable[17] = 
{
	120,
	884,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2720,
	2721,
	822,
	2722,
	2723,
	2724,
	2725,
};
static Il2CppInterfaceOffsetPair HMACSHA512_t1190_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACSHA512_t1190_0_0_0;
extern const Il2CppType HMACSHA512_t1190_1_0_0;
struct HMACSHA512_t1190;
const Il2CppTypeDefinitionMetadata HMACSHA512_t1190_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACSHA512_t1190_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t742_0_0_0/* parent */
	, HMACSHA512_t1190_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4749/* fieldStart */
	, 7247/* methodStart */
	, -1/* eventStart */
	, 1491/* propertyStart */

};
TypeInfo HMACSHA512_t1190_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACSHA512"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMACSHA512_t1190_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1204/* custom_attributes_cache */
	, &HMACSHA512_t1190_0_0_0/* byval_arg */
	, &HMACSHA512_t1190_1_0_0/* this_arg */
	, &HMACSHA512_t1190_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACSHA512_t1190)/* instance_size */
	, sizeof (HMACSHA512_t1190)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(HMACSHA512_t1190_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HashAlgorithm
#include "mscorlib_System_Security_Cryptography_HashAlgorithm.h"
// Metadata Definition System.Security.Cryptography.HashAlgorithm
extern TypeInfo HashAlgorithm_t642_il2cpp_TypeInfo;
// System.Security.Cryptography.HashAlgorithm
#include "mscorlib_System_Security_Cryptography_HashAlgorithmMethodDeclarations.h"
static const EncodedMethodIndex HashAlgorithm_t642_VTable[15] = 
{
	120,
	125,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	0,
	0,
	822,
	0,
	823,
};
static const Il2CppType* HashAlgorithm_t642_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
	&ICryptoTransform_t616_0_0_0,
};
static Il2CppInterfaceOffsetPair HashAlgorithm_t642_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HashAlgorithm_t642_0_0_0;
extern const Il2CppType HashAlgorithm_t642_1_0_0;
struct HashAlgorithm_t642;
const Il2CppTypeDefinitionMetadata HashAlgorithm_t642_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, HashAlgorithm_t642_InterfacesTypeInfos/* implementedInterfaces */
	, HashAlgorithm_t642_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HashAlgorithm_t642_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4751/* fieldStart */
	, 7251/* methodStart */
	, -1/* eventStart */
	, 1492/* propertyStart */

};
TypeInfo HashAlgorithm_t642_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HashAlgorithm"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HashAlgorithm_t642_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1205/* custom_attributes_cache */
	, &HashAlgorithm_t642_0_0_0/* byval_arg */
	, &HashAlgorithm_t642_1_0_0/* this_arg */
	, &HashAlgorithm_t642_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HashAlgorithm_t642)/* instance_size */
	, sizeof (HashAlgorithm_t642)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Security.Cryptography.ICryptoTransform
extern TypeInfo ICryptoTransform_t616_il2cpp_TypeInfo;
static const Il2CppType* ICryptoTransform_t616_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICryptoTransform_t616_1_0_0;
struct ICryptoTransform_t616;
const Il2CppTypeDefinitionMetadata ICryptoTransform_t616_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ICryptoTransform_t616_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7265/* methodStart */
	, -1/* eventStart */
	, 1495/* propertyStart */

};
TypeInfo ICryptoTransform_t616_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICryptoTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &ICryptoTransform_t616_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1206/* custom_attributes_cache */
	, &ICryptoTransform_t616_0_0_0/* byval_arg */
	, &ICryptoTransform_t616_1_0_0/* this_arg */
	, &ICryptoTransform_t616_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Security.Cryptography.ICspAsymmetricAlgorithm
extern TypeInfo ICspAsymmetricAlgorithm_t2116_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICspAsymmetricAlgorithm_t2116_1_0_0;
struct ICspAsymmetricAlgorithm_t2116;
const Il2CppTypeDefinitionMetadata ICspAsymmetricAlgorithm_t2116_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICspAsymmetricAlgorithm_t2116_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICspAsymmetricAlgorithm"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &ICspAsymmetricAlgorithm_t2116_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1207/* custom_attributes_cache */
	, &ICspAsymmetricAlgorithm_t2116_0_0_0/* byval_arg */
	, &ICspAsymmetricAlgorithm_t2116_1_0_0/* this_arg */
	, &ICspAsymmetricAlgorithm_t2116_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.KeySizes
#include "mscorlib_System_Security_Cryptography_KeySizes.h"
// Metadata Definition System.Security.Cryptography.KeySizes
extern TypeInfo KeySizes_t621_il2cpp_TypeInfo;
// System.Security.Cryptography.KeySizes
#include "mscorlib_System_Security_Cryptography_KeySizesMethodDeclarations.h"
static const EncodedMethodIndex KeySizes_t621_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeySizes_t621_0_0_0;
extern const Il2CppType KeySizes_t621_1_0_0;
struct KeySizes_t621;
const Il2CppTypeDefinitionMetadata KeySizes_t621_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeySizes_t621_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4755/* fieldStart */
	, 7268/* methodStart */
	, -1/* eventStart */
	, 1496/* propertyStart */

};
TypeInfo KeySizes_t621_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeySizes"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &KeySizes_t621_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeySizes_t621_0_0_0/* byval_arg */
	, &KeySizes_t621_1_0_0/* this_arg */
	, &KeySizes_t621_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeySizes_t621)/* instance_size */
	, sizeof (KeySizes_t621)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.KeyedHashAlgorithm
#include "mscorlib_System_Security_Cryptography_KeyedHashAlgorithm.h"
// Metadata Definition System.Security.Cryptography.KeyedHashAlgorithm
extern TypeInfo KeyedHashAlgorithm_t664_il2cpp_TypeInfo;
// System.Security.Cryptography.KeyedHashAlgorithm
#include "mscorlib_System_Security_Cryptography_KeyedHashAlgorithmMethodDeclarations.h"
static const EncodedMethodIndex KeyedHashAlgorithm_t664_VTable[17] = 
{
	120,
	884,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	0,
	0,
	822,
	0,
	888,
	2726,
	2727,
};
static Il2CppInterfaceOffsetPair KeyedHashAlgorithm_t664_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyedHashAlgorithm_t664_1_0_0;
struct KeyedHashAlgorithm_t664;
const Il2CppTypeDefinitionMetadata KeyedHashAlgorithm_t664_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, KeyedHashAlgorithm_t664_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t642_0_0_0/* parent */
	, KeyedHashAlgorithm_t664_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4758/* fieldStart */
	, 7274/* methodStart */
	, -1/* eventStart */
	, 1499/* propertyStart */

};
TypeInfo KeyedHashAlgorithm_t664_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyedHashAlgorithm"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &KeyedHashAlgorithm_t664_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1208/* custom_attributes_cache */
	, &KeyedHashAlgorithm_t664_0_0_0/* byval_arg */
	, &KeyedHashAlgorithm_t664_1_0_0/* this_arg */
	, &KeyedHashAlgorithm_t664_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyedHashAlgorithm_t664)/* instance_size */
	, sizeof (KeyedHashAlgorithm_t664)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.MACTripleDES
#include "mscorlib_System_Security_Cryptography_MACTripleDES.h"
// Metadata Definition System.Security.Cryptography.MACTripleDES
extern TypeInfo MACTripleDES_t1191_il2cpp_TypeInfo;
// System.Security.Cryptography.MACTripleDES
#include "mscorlib_System_Security_Cryptography_MACTripleDESMethodDeclarations.h"
static const EncodedMethodIndex MACTripleDES_t1191_VTable[17] = 
{
	120,
	2728,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2729,
	2730,
	822,
	2731,
	2732,
	2726,
	2727,
};
static Il2CppInterfaceOffsetPair MACTripleDES_t1191_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MACTripleDES_t1191_0_0_0;
extern const Il2CppType MACTripleDES_t1191_1_0_0;
struct MACTripleDES_t1191;
const Il2CppTypeDefinitionMetadata MACTripleDES_t1191_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MACTripleDES_t1191_InterfacesOffsets/* interfaceOffsets */
	, &KeyedHashAlgorithm_t664_0_0_0/* parent */
	, MACTripleDES_t1191_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4759/* fieldStart */
	, 7280/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MACTripleDES_t1191_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MACTripleDES"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &MACTripleDES_t1191_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1209/* custom_attributes_cache */
	, &MACTripleDES_t1191_0_0_0/* byval_arg */
	, &MACTripleDES_t1191_1_0_0/* this_arg */
	, &MACTripleDES_t1191_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MACTripleDES_t1191)/* instance_size */
	, sizeof (MACTripleDES_t1191)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.MD5
#include "mscorlib_System_Security_Cryptography_MD5.h"
// Metadata Definition System.Security.Cryptography.MD5
extern TypeInfo MD5_t743_il2cpp_TypeInfo;
// System.Security.Cryptography.MD5
#include "mscorlib_System_Security_Cryptography_MD5MethodDeclarations.h"
static const EncodedMethodIndex MD5_t743_VTable[15] = 
{
	120,
	125,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	0,
	0,
	822,
	0,
	823,
};
static Il2CppInterfaceOffsetPair MD5_t743_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MD5_t743_0_0_0;
extern const Il2CppType MD5_t743_1_0_0;
struct MD5_t743;
const Il2CppTypeDefinitionMetadata MD5_t743_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MD5_t743_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t642_0_0_0/* parent */
	, MD5_t743_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7287/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MD5_t743_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MD5"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &MD5_t743_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1210/* custom_attributes_cache */
	, &MD5_t743_0_0_0/* byval_arg */
	, &MD5_t743_1_0_0/* this_arg */
	, &MD5_t743_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MD5_t743)/* instance_size */
	, sizeof (MD5_t743)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.MD5CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_MD5CryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.MD5CryptoServiceProvider
extern TypeInfo MD5CryptoServiceProvider_t1192_il2cpp_TypeInfo;
// System.Security.Cryptography.MD5CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_MD5CryptoServiceProvidMethodDeclarations.h"
static const EncodedMethodIndex MD5CryptoServiceProvider_t1192_VTable[15] = 
{
	120,
	2733,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2734,
	2735,
	822,
	2736,
	2737,
};
static Il2CppInterfaceOffsetPair MD5CryptoServiceProvider_t1192_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MD5CryptoServiceProvider_t1192_0_0_0;
extern const Il2CppType MD5CryptoServiceProvider_t1192_1_0_0;
struct MD5CryptoServiceProvider_t1192;
const Il2CppTypeDefinitionMetadata MD5CryptoServiceProvider_t1192_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MD5CryptoServiceProvider_t1192_InterfacesOffsets/* interfaceOffsets */
	, &MD5_t743_0_0_0/* parent */
	, MD5CryptoServiceProvider_t1192_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4762/* fieldStart */
	, 7290/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MD5CryptoServiceProvider_t1192_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MD5CryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &MD5CryptoServiceProvider_t1192_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1211/* custom_attributes_cache */
	, &MD5CryptoServiceProvider_t1192_0_0_0/* byval_arg */
	, &MD5CryptoServiceProvider_t1192_1_0_0/* this_arg */
	, &MD5CryptoServiceProvider_t1192_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MD5CryptoServiceProvider_t1192)/* instance_size */
	, sizeof (MD5CryptoServiceProvider_t1192)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MD5CryptoServiceProvider_t1192_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.PaddingMode
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
// Metadata Definition System.Security.Cryptography.PaddingMode
extern TypeInfo PaddingMode_t619_il2cpp_TypeInfo;
// System.Security.Cryptography.PaddingMode
#include "mscorlib_System_Security_Cryptography_PaddingModeMethodDeclarations.h"
static const EncodedMethodIndex PaddingMode_t619_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair PaddingMode_t619_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PaddingMode_t619_0_0_0;
extern const Il2CppType PaddingMode_t619_1_0_0;
const Il2CppTypeDefinitionMetadata PaddingMode_t619_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PaddingMode_t619_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, PaddingMode_t619_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4768/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PaddingMode_t619_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PaddingMode"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1212/* custom_attributes_cache */
	, &PaddingMode_t619_0_0_0/* byval_arg */
	, &PaddingMode_t619_1_0_0/* this_arg */
	, &PaddingMode_t619_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PaddingMode_t619)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PaddingMode_t619)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.RC2
#include "mscorlib_System_Security_Cryptography_RC2.h"
// Metadata Definition System.Security.Cryptography.RC2
extern TypeInfo RC2_t750_il2cpp_TypeInfo;
// System.Security.Cryptography.RC2
#include "mscorlib_System_Security_Cryptography_RC2MethodDeclarations.h"
static const EncodedMethodIndex RC2_t750_VTable[27] = 
{
	120,
	759,
	122,
	123,
	760,
	761,
	762,
	763,
	764,
	765,
	766,
	767,
	768,
	2738,
	2739,
	771,
	772,
	773,
	774,
	775,
	776,
	0,
	777,
	0,
	0,
	0,
	2740,
};
static Il2CppInterfaceOffsetPair RC2_t750_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RC2_t750_0_0_0;
extern const Il2CppType RC2_t750_1_0_0;
struct RC2_t750;
const Il2CppTypeDefinitionMetadata RC2_t750_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RC2_t750_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricAlgorithm_t606_0_0_0/* parent */
	, RC2_t750_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4774/* fieldStart */
	, 7300/* methodStart */
	, -1/* eventStart */
	, 1500/* propertyStart */

};
TypeInfo RC2_t750_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RC2"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RC2_t750_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1213/* custom_attributes_cache */
	, &RC2_t750_0_0_0/* byval_arg */
	, &RC2_t750_1_0_0/* this_arg */
	, &RC2_t750_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RC2_t750)/* instance_size */
	, sizeof (RC2_t750)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 27/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.RC2CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RC2CryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.RC2CryptoServiceProvider
extern TypeInfo RC2CryptoServiceProvider_t1193_il2cpp_TypeInfo;
// System.Security.Cryptography.RC2CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RC2CryptoServiceProvidMethodDeclarations.h"
static const EncodedMethodIndex RC2CryptoServiceProvider_t1193_VTable[27] = 
{
	120,
	759,
	122,
	123,
	760,
	761,
	762,
	763,
	764,
	765,
	766,
	767,
	768,
	2738,
	2739,
	771,
	772,
	773,
	774,
	775,
	776,
	2741,
	777,
	2742,
	2743,
	2744,
	2745,
};
static Il2CppInterfaceOffsetPair RC2CryptoServiceProvider_t1193_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RC2CryptoServiceProvider_t1193_0_0_0;
extern const Il2CppType RC2CryptoServiceProvider_t1193_1_0_0;
struct RC2CryptoServiceProvider_t1193;
const Il2CppTypeDefinitionMetadata RC2CryptoServiceProvider_t1193_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RC2CryptoServiceProvider_t1193_InterfacesOffsets/* interfaceOffsets */
	, &RC2_t750_0_0_0/* parent */
	, RC2CryptoServiceProvider_t1193_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7306/* methodStart */
	, -1/* eventStart */
	, 1502/* propertyStart */

};
TypeInfo RC2CryptoServiceProvider_t1193_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RC2CryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RC2CryptoServiceProvider_t1193_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1214/* custom_attributes_cache */
	, &RC2CryptoServiceProvider_t1193_0_0_0/* byval_arg */
	, &RC2CryptoServiceProvider_t1193_1_0_0/* this_arg */
	, &RC2CryptoServiceProvider_t1193_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RC2CryptoServiceProvider_t1193)/* instance_size */
	, sizeof (RC2CryptoServiceProvider_t1193)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 27/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.RC2Transform
#include "mscorlib_System_Security_Cryptography_RC2Transform.h"
// Metadata Definition System.Security.Cryptography.RC2Transform
extern TypeInfo RC2Transform_t1194_il2cpp_TypeInfo;
// System.Security.Cryptography.RC2Transform
#include "mscorlib_System_Security_Cryptography_RC2TransformMethodDeclarations.h"
static const EncodedMethodIndex RC2Transform_t1194_VTable[18] = 
{
	120,
	1534,
	122,
	123,
	1535,
	1536,
	1537,
	1538,
	1539,
	1536,
	1540,
	2746,
	1541,
	1542,
	1543,
	1544,
	1537,
	1538,
};
static Il2CppInterfaceOffsetPair RC2Transform_t1194_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RC2Transform_t1194_0_0_0;
extern const Il2CppType RC2Transform_t1194_1_0_0;
struct RC2Transform_t1194;
const Il2CppTypeDefinitionMetadata RC2Transform_t1194_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RC2Transform_t1194_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricTransform_t839_0_0_0/* parent */
	, RC2Transform_t1194_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4775/* fieldStart */
	, 7312/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RC2Transform_t1194_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RC2Transform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RC2Transform_t1194_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RC2Transform_t1194_0_0_0/* byval_arg */
	, &RC2Transform_t1194_1_0_0/* this_arg */
	, &RC2Transform_t1194_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RC2Transform_t1194)/* instance_size */
	, sizeof (RC2Transform_t1194)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RC2Transform_t1194_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.RIPEMD160
#include "mscorlib_System_Security_Cryptography_RIPEMD160.h"
// Metadata Definition System.Security.Cryptography.RIPEMD160
extern TypeInfo RIPEMD160_t1195_il2cpp_TypeInfo;
// System.Security.Cryptography.RIPEMD160
#include "mscorlib_System_Security_Cryptography_RIPEMD160MethodDeclarations.h"
static const EncodedMethodIndex RIPEMD160_t1195_VTable[15] = 
{
	120,
	125,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	0,
	0,
	822,
	0,
	823,
};
static Il2CppInterfaceOffsetPair RIPEMD160_t1195_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RIPEMD160_t1195_0_0_0;
extern const Il2CppType RIPEMD160_t1195_1_0_0;
struct RIPEMD160_t1195;
const Il2CppTypeDefinitionMetadata RIPEMD160_t1195_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RIPEMD160_t1195_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t642_0_0_0/* parent */
	, RIPEMD160_t1195_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7315/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RIPEMD160_t1195_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RIPEMD160"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RIPEMD160_t1195_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1215/* custom_attributes_cache */
	, &RIPEMD160_t1195_0_0_0/* byval_arg */
	, &RIPEMD160_t1195_1_0_0/* this_arg */
	, &RIPEMD160_t1195_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RIPEMD160_t1195)/* instance_size */
	, sizeof (RIPEMD160_t1195)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.RIPEMD160Managed
#include "mscorlib_System_Security_Cryptography_RIPEMD160Managed.h"
// Metadata Definition System.Security.Cryptography.RIPEMD160Managed
extern TypeInfo RIPEMD160Managed_t1196_il2cpp_TypeInfo;
// System.Security.Cryptography.RIPEMD160Managed
#include "mscorlib_System_Security_Cryptography_RIPEMD160ManagedMethodDeclarations.h"
static const EncodedMethodIndex RIPEMD160Managed_t1196_VTable[15] = 
{
	120,
	2747,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2748,
	2749,
	822,
	2750,
	823,
};
static Il2CppInterfaceOffsetPair RIPEMD160Managed_t1196_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RIPEMD160Managed_t1196_0_0_0;
extern const Il2CppType RIPEMD160Managed_t1196_1_0_0;
struct RIPEMD160Managed_t1196;
const Il2CppTypeDefinitionMetadata RIPEMD160Managed_t1196_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RIPEMD160Managed_t1196_InterfacesOffsets/* interfaceOffsets */
	, &RIPEMD160_t1195_0_0_0/* parent */
	, RIPEMD160Managed_t1196_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4782/* fieldStart */
	, 7316/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RIPEMD160Managed_t1196_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RIPEMD160Managed"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RIPEMD160Managed_t1196_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1216/* custom_attributes_cache */
	, &RIPEMD160Managed_t1196_0_0_0/* byval_arg */
	, &RIPEMD160Managed_t1196_1_0_0/* this_arg */
	, &RIPEMD160Managed_t1196_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RIPEMD160Managed_t1196)/* instance_size */
	, sizeof (RIPEMD160Managed_t1196)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.RNGCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RNGCryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.RNGCryptoServiceProvider
extern TypeInfo RNGCryptoServiceProvider_t1197_il2cpp_TypeInfo;
// System.Security.Cryptography.RNGCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RNGCryptoServiceProvidMethodDeclarations.h"
static const EncodedMethodIndex RNGCryptoServiceProvider_t1197_VTable[6] = 
{
	120,
	2751,
	122,
	123,
	2752,
	2753,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RNGCryptoServiceProvider_t1197_0_0_0;
extern const Il2CppType RNGCryptoServiceProvider_t1197_1_0_0;
extern const Il2CppType RandomNumberGenerator_t604_0_0_0;
struct RNGCryptoServiceProvider_t1197;
const Il2CppTypeDefinitionMetadata RNGCryptoServiceProvider_t1197_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &RandomNumberGenerator_t604_0_0_0/* parent */
	, RNGCryptoServiceProvider_t1197_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4787/* fieldStart */
	, 7340/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RNGCryptoServiceProvider_t1197_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RNGCryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RNGCryptoServiceProvider_t1197_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RNGCryptoServiceProvider_t1197_0_0_0/* byval_arg */
	, &RNGCryptoServiceProvider_t1197_1_0_0/* this_arg */
	, &RNGCryptoServiceProvider_t1197_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RNGCryptoServiceProvider_t1197)/* instance_size */
	, sizeof (RNGCryptoServiceProvider_t1197)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RNGCryptoServiceProvider_t1197_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.RSA
#include "mscorlib_System_Security_Cryptography_RSA.h"
// Metadata Definition System.Security.Cryptography.RSA
extern TypeInfo RSA_t562_il2cpp_TypeInfo;
// System.Security.Cryptography.RSA
#include "mscorlib_System_Security_Cryptography_RSAMethodDeclarations.h"
static const EncodedMethodIndex RSA_t562_VTable[14] = 
{
	120,
	125,
	122,
	123,
	828,
	2699,
	830,
	0,
	832,
	2754,
	0,
	0,
	0,
	0,
};
static Il2CppInterfaceOffsetPair RSA_t562_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSA_t562_0_0_0;
extern const Il2CppType RSA_t562_1_0_0;
struct RSA_t562;
const Il2CppTypeDefinitionMetadata RSA_t562_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RSA_t562_InterfacesOffsets/* interfaceOffsets */
	, &AsymmetricAlgorithm_t436_0_0_0/* parent */
	, RSA_t562_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7350/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSA_t562_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSA"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSA_t562_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1217/* custom_attributes_cache */
	, &RSA_t562_0_0_0/* byval_arg */
	, &RSA_t562_1_0_0/* this_arg */
	, &RSA_t562_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSA_t562)/* instance_size */
	, sizeof (RSA_t562)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.RSACryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RSACryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.RSACryptoServiceProvider
extern TypeInfo RSACryptoServiceProvider_t582_il2cpp_TypeInfo;
// System.Security.Cryptography.RSACryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RSACryptoServiceProvidMethodDeclarations.h"
static const EncodedMethodIndex RSACryptoServiceProvider_t582_VTable[14] = 
{
	120,
	2755,
	122,
	123,
	828,
	2756,
	830,
	2757,
	832,
	2754,
	2758,
	2759,
	2760,
	2761,
};
static const Il2CppType* RSACryptoServiceProvider_t582_InterfacesTypeInfos[] = 
{
	&ICspAsymmetricAlgorithm_t2116_0_0_0,
};
static Il2CppInterfaceOffsetPair RSACryptoServiceProvider_t582_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICspAsymmetricAlgorithm_t2116_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSACryptoServiceProvider_t582_0_0_0;
extern const Il2CppType RSACryptoServiceProvider_t582_1_0_0;
struct RSACryptoServiceProvider_t582;
const Il2CppTypeDefinitionMetadata RSACryptoServiceProvider_t582_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RSACryptoServiceProvider_t582_InterfacesTypeInfos/* implementedInterfaces */
	, RSACryptoServiceProvider_t582_InterfacesOffsets/* interfaceOffsets */
	, &RSA_t562_0_0_0/* parent */
	, RSACryptoServiceProvider_t582_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4789/* fieldStart */
	, 7360/* methodStart */
	, -1/* eventStart */
	, 1503/* propertyStart */

};
TypeInfo RSACryptoServiceProvider_t582_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSACryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSACryptoServiceProvider_t582_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1218/* custom_attributes_cache */
	, &RSACryptoServiceProvider_t582_0_0_0/* byval_arg */
	, &RSACryptoServiceProvider_t582_1_0_0/* this_arg */
	, &RSACryptoServiceProvider_t582_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSACryptoServiceProvider_t582)/* instance_size */
	, sizeof (RSACryptoServiceProvider_t582)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RSACryptoServiceProvider_t582_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1KeyExchangeFor.h"
// Metadata Definition System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter
extern TypeInfo RSAPKCS1KeyExchangeFormatter_t755_il2cpp_TypeInfo;
// System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1KeyExchangeForMethodDeclarations.h"
static const EncodedMethodIndex RSAPKCS1KeyExchangeFormatter_t755_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2762,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSAPKCS1KeyExchangeFormatter_t755_0_0_0;
extern const Il2CppType RSAPKCS1KeyExchangeFormatter_t755_1_0_0;
struct RSAPKCS1KeyExchangeFormatter_t755;
const Il2CppTypeDefinitionMetadata RSAPKCS1KeyExchangeFormatter_t755_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricKeyExchangeFormatter_t1179_0_0_0/* parent */
	, RSAPKCS1KeyExchangeFormatter_t755_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4796/* fieldStart */
	, 7374/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSAPKCS1KeyExchangeFormatter_t755_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAPKCS1KeyExchangeFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAPKCS1KeyExchangeFormatter_t755_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1220/* custom_attributes_cache */
	, &RSAPKCS1KeyExchangeFormatter_t755_0_0_0/* byval_arg */
	, &RSAPKCS1KeyExchangeFormatter_t755_1_0_0/* this_arg */
	, &RSAPKCS1KeyExchangeFormatter_t755_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSAPKCS1KeyExchangeFormatter_t755)/* instance_size */
	, sizeof (RSAPKCS1KeyExchangeFormatter_t755)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
