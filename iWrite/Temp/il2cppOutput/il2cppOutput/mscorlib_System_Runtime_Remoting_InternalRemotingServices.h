﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t385;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.InternalRemotingServices
struct  InternalRemotingServices_t1122  : public Object_t
{
};
struct InternalRemotingServices_t1122_StaticFields{
	// System.Collections.Hashtable System.Runtime.Remoting.InternalRemotingServices::_soapAttributes
	Hashtable_t385 * ____soapAttributes_0;
};
