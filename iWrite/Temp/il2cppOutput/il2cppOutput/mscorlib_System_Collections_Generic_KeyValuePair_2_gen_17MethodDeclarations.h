﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>
struct KeyValuePair_2_t1639;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13MethodDeclarations.h"
#define KeyValuePair_2__ctor_m10788(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1639 *, String_t*, KeyValuePair_2_t362 , const MethodInfo*))KeyValuePair_2__ctor_m10262_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Key()
#define KeyValuePair_2_get_Key_m10789(__this, method) (( String_t* (*) (KeyValuePair_2_t1639 *, const MethodInfo*))KeyValuePair_2_get_Key_m10263_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m10790(__this, ___value, method) (( void (*) (KeyValuePair_2_t1639 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m10264_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Value()
#define KeyValuePair_2_get_Value_m10791(__this, method) (( KeyValuePair_2_t362  (*) (KeyValuePair_2_t1639 *, const MethodInfo*))KeyValuePair_2_get_Value_m10265_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m10792(__this, ___value, method) (( void (*) (KeyValuePair_2_t1639 *, KeyValuePair_2_t362 , const MethodInfo*))KeyValuePair_2_set_Value_m10266_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::ToString()
#define KeyValuePair_2_ToString_m10793(__this, method) (( String_t* (*) (KeyValuePair_2_t1639 *, const MethodInfo*))KeyValuePair_2_ToString_m10267_gshared)(__this, method)
