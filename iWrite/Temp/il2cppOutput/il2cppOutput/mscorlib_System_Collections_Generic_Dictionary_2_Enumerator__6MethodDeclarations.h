﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>
struct Enumerator_t1523;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t77;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m9207(__this, ___dictionary, method) (( void (*) (Enumerator_t1523 *, Dictionary_2_t77 *, const MethodInfo*))Enumerator__ctor_m9119_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m9208(__this, method) (( Object_t * (*) (Enumerator_t1523 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m9120_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m9209(__this, method) (( DictionaryEntry_t560  (*) (Enumerator_t1523 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m9121_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m9210(__this, method) (( Object_t * (*) (Enumerator_t1523 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m9122_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m9211(__this, method) (( Object_t * (*) (Enumerator_t1523 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m9123_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m9212(__this, method) (( bool (*) (Enumerator_t1523 *, const MethodInfo*))Enumerator_MoveNext_m9124_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_Current()
#define Enumerator_get_Current_m9213(__this, method) (( KeyValuePair_2_t1520  (*) (Enumerator_t1523 *, const MethodInfo*))Enumerator_get_Current_m9125_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m9214(__this, method) (( String_t* (*) (Enumerator_t1523 *, const MethodInfo*))Enumerator_get_CurrentKey_m9126_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m9215(__this, method) (( int32_t (*) (Enumerator_t1523 *, const MethodInfo*))Enumerator_get_CurrentValue_m9127_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m9216(__this, method) (( void (*) (Enumerator_t1523 *, const MethodInfo*))Enumerator_VerifyState_m9128_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m9217(__this, method) (( void (*) (Enumerator_t1523 *, const MethodInfo*))Enumerator_VerifyCurrent_m9129_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Dispose()
#define Enumerator_Dispose_m9218(__this, method) (( void (*) (Enumerator_t1523 *, const MethodInfo*))Enumerator_Dispose_m9130_gshared)(__this, method)
