﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>
struct KeyValuePair_2_t1567;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m9775(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1567 *, String_t*, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m9679_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Key()
#define KeyValuePair_2_get_Key_m9776(__this, method) (( String_t* (*) (KeyValuePair_2_t1567 *, const MethodInfo*))KeyValuePair_2_get_Key_m9680_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m9777(__this, ___value, method) (( void (*) (KeyValuePair_2_t1567 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m9681_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Value()
#define KeyValuePair_2_get_Value_m9778(__this, method) (( int64_t (*) (KeyValuePair_2_t1567 *, const MethodInfo*))KeyValuePair_2_get_Value_m9682_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m9779(__this, ___value, method) (( void (*) (KeyValuePair_2_t1567 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m9683_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::ToString()
#define KeyValuePair_2_ToString_m9780(__this, method) (( String_t* (*) (KeyValuePair_2_t1567 *, const MethodInfo*))KeyValuePair_2_ToString_m9684_gshared)(__this, method)
