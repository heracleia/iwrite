﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct Enumerator_t1572;
// System.Object
struct Object_t;
// UnityEngine.Networking.Match.MatchDirectConnectInfo
struct MatchDirectConnectInfo_t171;
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct List_1_t172;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDirectConnectInfo>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1MethodDeclarations.h"
#define Enumerator__ctor_m9852(__this, ___l, method) (( void (*) (Enumerator_t1572 *, List_1_t172 *, const MethodInfo*))Enumerator__ctor_m8428_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m9853(__this, method) (( Object_t * (*) (Enumerator_t1572 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Dispose()
#define Enumerator_Dispose_m9854(__this, method) (( void (*) (Enumerator_t1572 *, const MethodInfo*))Enumerator_Dispose_m8430_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDirectConnectInfo>::VerifyState()
#define Enumerator_VerifyState_m9855(__this, method) (( void (*) (Enumerator_t1572 *, const MethodInfo*))Enumerator_VerifyState_m8431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDirectConnectInfo>::MoveNext()
#define Enumerator_MoveNext_m9856(__this, method) (( bool (*) (Enumerator_t1572 *, const MethodInfo*))Enumerator_MoveNext_m8432_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDirectConnectInfo>::get_Current()
#define Enumerator_get_Current_m9857(__this, method) (( MatchDirectConnectInfo_t171 * (*) (Enumerator_t1572 *, const MethodInfo*))Enumerator_get_Current_m8433_gshared)(__this, method)
