﻿#pragma once
#include <stdint.h>
// System.Version
struct Version_t418;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Resources.SatelliteContractVersionAttribute
struct  SatelliteContractVersionAttribute_t1029  : public Attribute_t96
{
	// System.Version System.Resources.SatelliteContractVersionAttribute::ver
	Version_t418 * ___ver_0;
};
