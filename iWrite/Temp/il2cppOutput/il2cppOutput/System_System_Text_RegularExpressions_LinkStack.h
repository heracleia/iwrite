﻿#pragma once
#include <stdint.h>
// System.Collections.Stack
struct Stack_t271;
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRef.h"
// System.Text.RegularExpressions.LinkStack
struct  LinkStack_t509  : public LinkRef_t505
{
	// System.Collections.Stack System.Text.RegularExpressions.LinkStack::stack
	Stack_t271 * ___stack_0;
};
