﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Char>
struct InternalEnumerator_1_t1528;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m9315_gshared (InternalEnumerator_1_t1528 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m9315(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1528 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m9315_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9316_gshared (InternalEnumerator_1_t1528 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9316(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1528 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9316_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m9317_gshared (InternalEnumerator_1_t1528 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m9317(__this, method) (( void (*) (InternalEnumerator_1_t1528 *, const MethodInfo*))InternalEnumerator_1_Dispose_m9317_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m9318_gshared (InternalEnumerator_1_t1528 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m9318(__this, method) (( bool (*) (InternalEnumerator_1_t1528 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m9318_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern "C" uint16_t InternalEnumerator_1_get_Current_m9319_gshared (InternalEnumerator_1_t1528 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m9319(__this, method) (( uint16_t (*) (InternalEnumerator_1_t1528 *, const MethodInfo*))InternalEnumerator_1_get_Current_m9319_gshared)(__this, method)
