﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerDisplayAttribute
struct DebuggerDisplayAttribute_t891;
// System.String
struct String_t;

// System.Void System.Diagnostics.DebuggerDisplayAttribute::.ctor(System.String)
extern "C" void DebuggerDisplayAttribute__ctor_m4986 (DebuggerDisplayAttribute_t891 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerDisplayAttribute::set_Name(System.String)
extern "C" void DebuggerDisplayAttribute_set_Name_m4987 (DebuggerDisplayAttribute_t891 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
