﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Behaviour
struct Behaviour_t34;

// System.Void UnityEngine.Behaviour::.ctor()
extern "C" void Behaviour__ctor_m541 (Behaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
