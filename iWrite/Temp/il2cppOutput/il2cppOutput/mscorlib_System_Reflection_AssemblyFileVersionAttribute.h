﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyFileVersionAttribute
struct  AssemblyFileVersionAttribute_t991  : public Attribute_t96
{
	// System.String System.Reflection.AssemblyFileVersionAttribute::name
	String_t* ___name_0;
};
