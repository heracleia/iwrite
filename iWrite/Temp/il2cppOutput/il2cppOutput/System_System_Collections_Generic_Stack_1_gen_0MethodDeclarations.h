﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t1650;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1807;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C" void Stack_1__ctor_m10867_gshared (Stack_1_t1650 * __this, const MethodInfo* method);
#define Stack_1__ctor_m10867(__this, method) (( void (*) (Stack_1_t1650 *, const MethodInfo*))Stack_1__ctor_m10867_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Stack_1_System_Collections_ICollection_get_SyncRoot_m10869_gshared (Stack_1_t1650 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m10869(__this, method) (( Object_t * (*) (Stack_1_t1650 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m10869_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Stack_1_System_Collections_ICollection_CopyTo_m10871_gshared (Stack_1_t1650 * __this, Array_t * ___dest, int32_t ___idx, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_CopyTo_m10871(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t1650 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m10871_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10873_gshared (Stack_1_t1650 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10873(__this, method) (( Object_t* (*) (Stack_1_t1650 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10873_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Stack_1_System_Collections_IEnumerable_GetEnumerator_m10875_gshared (Stack_1_t1650 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m10875(__this, method) (( Object_t * (*) (Stack_1_t1650 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m10875_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C" Object_t * Stack_1_Pop_m10876_gshared (Stack_1_t1650 * __this, const MethodInfo* method);
#define Stack_1_Pop_m10876(__this, method) (( Object_t * (*) (Stack_1_t1650 *, const MethodInfo*))Stack_1_Pop_m10876_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(T)
extern "C" void Stack_1_Push_m10877_gshared (Stack_1_t1650 * __this, Object_t * ___t, const MethodInfo* method);
#define Stack_1_Push_m10877(__this, ___t, method) (( void (*) (Stack_1_t1650 *, Object_t *, const MethodInfo*))Stack_1_Push_m10877_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C" int32_t Stack_1_get_Count_m10879_gshared (Stack_1_t1650 * __this, const MethodInfo* method);
#define Stack_1_get_Count_m10879(__this, method) (( int32_t (*) (Stack_1_t1650 *, const MethodInfo*))Stack_1_get_Count_m10879_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t1651  Stack_1_GetEnumerator_m10881_gshared (Stack_1_t1650 * __this, const MethodInfo* method);
#define Stack_1_GetEnumerator_m10881(__this, method) (( Enumerator_t1651  (*) (Stack_1_t1650 *, const MethodInfo*))Stack_1_GetEnumerator_m10881_gshared)(__this, method)
