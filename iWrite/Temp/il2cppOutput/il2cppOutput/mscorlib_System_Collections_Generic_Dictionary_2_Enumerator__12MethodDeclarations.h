﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Enumerator_t1630;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t1627;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m10724_gshared (Enumerator_t1630 * __this, Dictionary_2_t1627 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m10724(__this, ___dictionary, method) (( void (*) (Enumerator_t1630 *, Dictionary_2_t1627 *, const MethodInfo*))Enumerator__ctor_m10724_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m10725_gshared (Enumerator_t1630 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m10725(__this, method) (( Object_t * (*) (Enumerator_t1630 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m10725_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t560  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m10726_gshared (Enumerator_t1630 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m10726(__this, method) (( DictionaryEntry_t560  (*) (Enumerator_t1630 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m10726_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m10727_gshared (Enumerator_t1630 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m10727(__this, method) (( Object_t * (*) (Enumerator_t1630 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m10727_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m10728_gshared (Enumerator_t1630 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m10728(__this, method) (( Object_t * (*) (Enumerator_t1630 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m10728_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool Enumerator_MoveNext_m10729_gshared (Enumerator_t1630 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m10729(__this, method) (( bool (*) (Enumerator_t1630 *, const MethodInfo*))Enumerator_MoveNext_m10729_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t1601  Enumerator_get_Current_m10730_gshared (Enumerator_t1630 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m10730(__this, method) (( KeyValuePair_2_t1601  (*) (Enumerator_t1630 *, const MethodInfo*))Enumerator_get_Current_m10730_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m10731_gshared (Enumerator_t1630 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m10731(__this, method) (( Object_t * (*) (Enumerator_t1630 *, const MethodInfo*))Enumerator_get_CurrentKey_m10731_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_CurrentValue()
extern "C" KeyValuePair_2_t1493  Enumerator_get_CurrentValue_m10732_gshared (Enumerator_t1630 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m10732(__this, method) (( KeyValuePair_2_t1493  (*) (Enumerator_t1630 *, const MethodInfo*))Enumerator_get_CurrentValue_m10732_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern "C" void Enumerator_VerifyState_m10733_gshared (Enumerator_t1630 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m10733(__this, method) (( void (*) (Enumerator_t1630 *, const MethodInfo*))Enumerator_VerifyState_m10733_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m10734_gshared (Enumerator_t1630 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m10734(__this, method) (( void (*) (Enumerator_t1630 *, const MethodInfo*))Enumerator_VerifyCurrent_m10734_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void Enumerator_Dispose_m10735_gshared (Enumerator_t1630 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m10735(__this, method) (( void (*) (Enumerator_t1630 *, const MethodInfo*))Enumerator_Dispose_m10735_gshared)(__this, method)
