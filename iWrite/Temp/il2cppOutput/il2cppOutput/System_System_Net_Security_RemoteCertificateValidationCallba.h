﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t449;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t460;
// System.IAsyncResult
struct IAsyncResult_t44;
// System.AsyncCallback
struct AsyncCallback_t45;
// System.Net.Security.SslPolicyErrors
#include "System_System_Net_Security_SslPolicyErrors.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Net.Security.RemoteCertificateValidationCallback
struct  RemoteCertificateValidationCallback_t414  : public MulticastDelegate_t47
{
};
