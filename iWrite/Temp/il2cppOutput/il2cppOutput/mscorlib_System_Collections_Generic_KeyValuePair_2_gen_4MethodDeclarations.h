﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct KeyValuePair_2_t1482;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t54;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3MethodDeclarations.h"
#define KeyValuePair_2__ctor_m8684(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1482 *, int32_t, LayoutCache_t54 *, const MethodInfo*))KeyValuePair_2__ctor_m8578_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Key()
#define KeyValuePair_2_get_Key_m8685(__this, method) (( int32_t (*) (KeyValuePair_2_t1482 *, const MethodInfo*))KeyValuePair_2_get_Key_m8579_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m8686(__this, ___value, method) (( void (*) (KeyValuePair_2_t1482 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m8580_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Value()
#define KeyValuePair_2_get_Value_m8687(__this, method) (( LayoutCache_t54 * (*) (KeyValuePair_2_t1482 *, const MethodInfo*))KeyValuePair_2_get_Value_m8581_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m8688(__this, ___value, method) (( void (*) (KeyValuePair_2_t1482 *, LayoutCache_t54 *, const MethodInfo*))KeyValuePair_2_set_Value_m8582_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::ToString()
#define KeyValuePair_2_ToString_m8689(__this, method) (( String_t* (*) (KeyValuePair_2_t1482 *, const MethodInfo*))KeyValuePair_2_ToString_m8583_gshared)(__this, method)
