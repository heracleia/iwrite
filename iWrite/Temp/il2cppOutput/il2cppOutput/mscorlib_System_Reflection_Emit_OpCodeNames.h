﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t197;
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.Emit.OpCodeNames
struct  OpCodeNames_t973  : public Object_t
{
};
struct OpCodeNames_t973_StaticFields{
	// System.String[] System.Reflection.Emit.OpCodeNames::names
	StringU5BU5D_t197* ___names_0;
};
