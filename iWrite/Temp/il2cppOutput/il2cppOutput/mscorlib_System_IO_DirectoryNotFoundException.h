﻿#pragma once
#include <stdint.h>
// System.IO.IOException
#include "mscorlib_System_IO_IOException.h"
// System.IO.DirectoryNotFoundException
struct  DirectoryNotFoundException_t917  : public IOException_t754
{
};
