﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>
struct InternalEnumerator_1_t1750;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m11812(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1750 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8302_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11813(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1750 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m11814(__this, method) (( void (*) (InternalEnumerator_1_t1750 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8306_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m11815(__this, method) (( bool (*) (InternalEnumerator_1_t1750 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8308_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m11816(__this, method) (( MethodInfo_t * (*) (InternalEnumerator_1_t1750 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8310_gshared)(__this, method)
