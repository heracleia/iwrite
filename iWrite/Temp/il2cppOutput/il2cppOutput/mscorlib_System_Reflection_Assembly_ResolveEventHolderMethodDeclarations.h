﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Assembly/ResolveEventHolder
struct ResolveEventHolder_t983;

// System.Void System.Reflection.Assembly/ResolveEventHolder::.ctor()
extern "C" void ResolveEventHolder__ctor_m5783 (ResolveEventHolder_t983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
