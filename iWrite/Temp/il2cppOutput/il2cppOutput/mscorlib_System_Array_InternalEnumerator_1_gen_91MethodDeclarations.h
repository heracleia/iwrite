﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.DateTime>
struct InternalEnumerator_1_t1781;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11950_gshared (InternalEnumerator_1_t1781 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11950(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1781 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11950_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11951_gshared (InternalEnumerator_1_t1781 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11951(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1781 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11951_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11952_gshared (InternalEnumerator_1_t1781 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11952(__this, method) (( void (*) (InternalEnumerator_1_t1781 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11952_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11953_gshared (InternalEnumerator_1_t1781 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11953(__this, method) (( bool (*) (InternalEnumerator_1_t1781 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11953_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern "C" DateTime_t51  InternalEnumerator_1_get_Current_m11954_gshared (InternalEnumerator_1_t1781 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11954(__this, method) (( DateTime_t51  (*) (InternalEnumerator_1_t1781 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11954_gshared)(__this, method)
