﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ProviderData
struct ProviderData_t1129;

// System.Void System.Runtime.Remoting.ProviderData::.ctor()
extern "C" void ProviderData__ctor_m6442 (ProviderData_t1129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ProviderData::CopyFrom(System.Runtime.Remoting.ProviderData)
extern "C" void ProviderData_CopyFrom_m6443 (ProviderData_t1129 * __this, ProviderData_t1129 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
