﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t1505;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t56;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t71;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"
#define Enumerator__ctor_m8976(__this, ___dictionary, method) (( void (*) (Enumerator_t1505 *, Dictionary_2_t71 *, const MethodInfo*))Enumerator__ctor_m8893_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m8977(__this, method) (( Object_t * (*) (Enumerator_t1505 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8894_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8978(__this, method) (( DictionaryEntry_t560  (*) (Enumerator_t1505 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8895_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8979(__this, method) (( Object_t * (*) (Enumerator_t1505 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8896_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8980(__this, method) (( Object_t * (*) (Enumerator_t1505 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8897_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m8981(__this, method) (( bool (*) (Enumerator_t1505 *, const MethodInfo*))Enumerator_MoveNext_m8898_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m8982(__this, method) (( KeyValuePair_2_t1503  (*) (Enumerator_t1505 *, const MethodInfo*))Enumerator_get_Current_m8899_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m8983(__this, method) (( String_t* (*) (Enumerator_t1505 *, const MethodInfo*))Enumerator_get_CurrentKey_m8900_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m8984(__this, method) (( GUIStyle_t56 * (*) (Enumerator_t1505 *, const MethodInfo*))Enumerator_get_CurrentValue_m8901_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyState()
#define Enumerator_VerifyState_m8985(__this, method) (( void (*) (Enumerator_t1505 *, const MethodInfo*))Enumerator_VerifyState_m8902_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m8986(__this, method) (( void (*) (Enumerator_t1505 *, const MethodInfo*))Enumerator_VerifyCurrent_m8903_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m8987(__this, method) (( void (*) (Enumerator_t1505 *, const MethodInfo*))Enumerator_Dispose_m8904_gshared)(__this, method)
