﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoEnumInfo/SByteComparer
struct SByteComparer_t1313;
// System.Object
struct Object_t;

// System.Void System.MonoEnumInfo/SByteComparer::.ctor()
extern "C" void SByteComparer__ctor_m7824 (SByteComparer_t1313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MonoEnumInfo/SByteComparer::Compare(System.Object,System.Object)
extern "C" int32_t SByteComparer_Compare_m7825 (SByteComparer_t1313 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MonoEnumInfo/SByteComparer::Compare(System.SByte,System.SByte)
extern "C" int32_t SByteComparer_Compare_m7826 (SByteComparer_t1313 * __this, int8_t ___ix, int8_t ___iy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
