﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
struct List_1_t59;
// System.Object
struct Object_t;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t58;
// System.Collections.Generic.IEnumerator`1<UnityEngine.GUILayoutEntry>
struct IEnumerator_1_t1820;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t1486;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
#define List_1__ctor_m1187(__this, method) (( void (*) (List_1_t59 *, const MethodInfo*))List_1__ctor_m1259_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::.ctor(System.Int32)
#define List_1__ctor_m8732(__this, ___capacity, method) (( void (*) (List_1_t59 *, int32_t, const MethodInfo*))List_1__ctor_m8366_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::.cctor()
#define List_1__cctor_m8733(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m8368_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m8734(__this, method) (( Object_t* (*) (List_1_t59 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m8370_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m8735(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t59 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m8372_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m8736(__this, method) (( Object_t * (*) (List_1_t59 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m8374_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m8737(__this, ___item, method) (( int32_t (*) (List_1_t59 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m8376_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m8738(__this, ___item, method) (( bool (*) (List_1_t59 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m8378_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m8739(__this, ___item, method) (( int32_t (*) (List_1_t59 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m8380_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m8740(__this, ___index, ___item, method) (( void (*) (List_1_t59 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m8382_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m8741(__this, ___item, method) (( void (*) (List_1_t59 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m8384_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m8742(__this, method) (( bool (*) (List_1_t59 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m8386_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m8743(__this, method) (( Object_t * (*) (List_1_t59 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m8388_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m8744(__this, ___index, method) (( Object_t * (*) (List_1_t59 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m8390_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m8745(__this, ___index, ___value, method) (( void (*) (List_1_t59 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m8392_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Add(T)
#define List_1_Add_m8746(__this, ___item, method) (( void (*) (List_1_t59 *, GUILayoutEntry_t58 *, const MethodInfo*))List_1_Add_m8394_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m8747(__this, ___newCount, method) (( void (*) (List_1_t59 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m8396_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Clear()
#define List_1_Clear_m8748(__this, method) (( void (*) (List_1_t59 *, const MethodInfo*))List_1_Clear_m8398_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Contains(T)
#define List_1_Contains_m8749(__this, ___item, method) (( bool (*) (List_1_t59 *, GUILayoutEntry_t58 *, const MethodInfo*))List_1_Contains_m8400_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m8750(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t59 *, GUILayoutEntryU5BU5D_t1486*, int32_t, const MethodInfo*))List_1_CopyTo_m8402_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::GetEnumerator()
#define List_1_GetEnumerator_m1181(__this, method) (( Enumerator_t322  (*) (List_1_t59 *, const MethodInfo*))List_1_GetEnumerator_m8403_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::IndexOf(T)
#define List_1_IndexOf_m8751(__this, ___item, method) (( int32_t (*) (List_1_t59 *, GUILayoutEntry_t58 *, const MethodInfo*))List_1_IndexOf_m8405_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m8752(__this, ___start, ___delta, method) (( void (*) (List_1_t59 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m8407_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m8753(__this, ___index, method) (( void (*) (List_1_t59 *, int32_t, const MethodInfo*))List_1_CheckIndex_m8409_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Insert(System.Int32,T)
#define List_1_Insert_m8754(__this, ___index, ___item, method) (( void (*) (List_1_t59 *, int32_t, GUILayoutEntry_t58 *, const MethodInfo*))List_1_Insert_m8411_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Remove(T)
#define List_1_Remove_m8755(__this, ___item, method) (( bool (*) (List_1_t59 *, GUILayoutEntry_t58 *, const MethodInfo*))List_1_Remove_m8413_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m8756(__this, ___index, method) (( void (*) (List_1_t59 *, int32_t, const MethodInfo*))List_1_RemoveAt_m8415_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::ToArray()
#define List_1_ToArray_m8757(__this, method) (( GUILayoutEntryU5BU5D_t1486* (*) (List_1_t59 *, const MethodInfo*))List_1_ToArray_m8417_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Capacity()
#define List_1_get_Capacity_m8758(__this, method) (( int32_t (*) (List_1_t59 *, const MethodInfo*))List_1_get_Capacity_m8419_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m8759(__this, ___value, method) (( void (*) (List_1_t59 *, int32_t, const MethodInfo*))List_1_set_Capacity_m8421_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count()
#define List_1_get_Count_m8760(__this, method) (( int32_t (*) (List_1_t59 *, const MethodInfo*))List_1_get_Count_m8423_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32)
#define List_1_get_Item_m8761(__this, ___index, method) (( GUILayoutEntry_t58 * (*) (List_1_t59 *, int32_t, const MethodInfo*))List_1_get_Item_m8425_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::set_Item(System.Int32,T)
#define List_1_set_Item_m8762(__this, ___index, ___value, method) (( void (*) (List_1_t59 *, int32_t, GUILayoutEntry_t58 *, const MethodInfo*))List_1_set_Item_m8427_gshared)(__this, ___index, ___value, method)
