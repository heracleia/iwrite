﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Void
#include "mscorlib_System_Void.h"
void* RuntimeInvoker_Void_t765 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
void* RuntimeInvoker_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
void* RuntimeInvoker_Int32_t320_LayerMask_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t7  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t7 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LayerMask_t7_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t7  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LayerMask_t7  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Single
#include "mscorlib_System_Single.h"
void* RuntimeInvoker_Void_t765_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
// System.Boolean
#include "mscorlib_System_Boolean.h"
void* RuntimeInvoker_Boolean_t333 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// System.Double
#include "mscorlib_System_Double.h"
void* RuntimeInvoker_Void_t765_Object_t_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Int64
#include "mscorlib_System_Int64.h"
struct Object_t;
void* RuntimeInvoker_Void_t765_Int64_t345_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
// System.SByte
#include "mscorlib_System_SByte.h"
void* RuntimeInvoker_Void_t765_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
void* RuntimeInvoker_Void_t765_GcAchievementDescriptionData_t223_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t223  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t223 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
void* RuntimeInvoker_Void_t765_GcUserProfileData_t222_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t222  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t222 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Double_t344_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int64_t345_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct UserProfileU5BU5D_t22;
#include "UnityEngine_ArrayTypes.h"
struct Object_t;
void* RuntimeInvoker_Void_t765_UserProfileU5BU5DU26_t2128_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t22** p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t22**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct UserProfileU5BU5D_t22;
void* RuntimeInvoker_Void_t765_UserProfileU5BU5DU26_t2128_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t22** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t22**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
void* RuntimeInvoker_Void_t765_GcScoreData_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t225  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t225 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"
void* RuntimeInvoker_Boolean_t333_BoneWeight_t28_BoneWeight_t28 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t28  p1, BoneWeight_t28  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t28 *)args[0]), *((BoneWeight_t28 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Int32_t320_SByte_t349_SByte_t349_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
void* RuntimeInvoker_Object_t_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t81  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t81 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t2129 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t81 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t81 *)args[1], method);
	return ret;
}

struct Object_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
void* RuntimeInvoker_Void_t765_Color_t38_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t38  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t38 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Single_t321_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"
void* RuntimeInvoker_Void_t765_DateTime_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t51  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t51 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Object_t_Int32_t320_Single_t321_Single_t321_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, float p5, float p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
void* RuntimeInvoker_Rect_t42_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t42  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Rect_t42  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Rect_t42 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t42  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t42 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_RectU26_t2130 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t42 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t42 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Single_t321_Single_t321_Single_t321_Single_t321_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Color_t38 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t38  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t38 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_ColorU26_t2131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t38 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t38 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
void* RuntimeInvoker_EventType_t79 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
void* RuntimeInvoker_Vector2_t43 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t43  (*Func)(void* obj, const MethodInfo* method);
	Vector2_t43  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Vector2U26_t2132 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t43 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t43 *)args[0], method);
	return NULL;
}

struct Object_t;
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"
void* RuntimeInvoker_EventModifiers_t80 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Char
#include "mscorlib_System_Char.h"
void* RuntimeInvoker_Char_t576 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
void* RuntimeInvoker_KeyCode_t78 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Vector2_t43 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t43  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t43 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t43_Vector2_t43_Vector2_t43 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t43  (*Func)(void* obj, Vector2_t43  p1, Vector2_t43  p2, const MethodInfo* method);
	Vector2_t43  ret = ((Func)method->method)(obj, *((Vector2_t43 *)args[0]), *((Vector2_t43 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Vector2_t43_Vector2_t43 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t43  p1, Vector2_t43  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t43 *)args[0]), *((Vector2_t43 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Single_t321_Single_t321_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t81  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t81 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t81_Vector3_t81_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t81  (*Func)(void* obj, Vector3_t81  p1, Vector3_t81  p2, const MethodInfo* method);
	Vector3_t81  ret = ((Func)method->method)(obj, *((Vector3_t81 *)args[0]), *((Vector3_t81 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t81  (*Func)(void* obj, const MethodInfo* method);
	Vector3_t81  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t81_Vector3_t81_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t81  (*Func)(void* obj, Vector3_t81  p1, float p2, const MethodInfo* method);
	Vector3_t81  ret = ((Func)method->method)(obj, *((Vector3_t81 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Vector3_t81_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t81  p1, Vector3_t81  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t81 *)args[0]), *((Vector3_t81 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Single_t321_Single_t321_Single_t321_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Color_t38 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t38  (*Func)(void* obj, const MethodInfo* method);
	Color_t38  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t38_Color_t38_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t38  (*Func)(void* obj, Color_t38  p1, float p2, const MethodInfo* method);
	Color_t38  ret = ((Func)method->method)(obj, *((Color_t38 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
void* RuntimeInvoker_Vector4_t86_Color_t38 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t86  (*Func)(void* obj, Color_t38  p1, const MethodInfo* method);
	Vector4_t86  ret = ((Func)method->method)(obj, *((Color_t38 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_SByte_t349_SByte_t349_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
void* RuntimeInvoker_Color32_t82_Color_t38 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t82  (*Func)(void* obj, Color_t38  p1, const MethodInfo* method);
	Color32_t82  ret = ((Func)method->method)(obj, *((Color_t38 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t81  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t81 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
void* RuntimeInvoker_Matrix4x4_t84_Matrix4x4_t84 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t84  (*Func)(void* obj, Matrix4x4_t84  p1, const MethodInfo* method);
	Matrix4x4_t84  ret = ((Func)method->method)(obj, *((Matrix4x4_t84 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t84_Matrix4x4U26_t2133 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t84  (*Func)(void* obj, Matrix4x4_t84 * p1, const MethodInfo* method);
	Matrix4x4_t84  ret = ((Func)method->method)(obj, (Matrix4x4_t84 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Matrix4x4_t84_Matrix4x4U26_t2133 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t84  p1, Matrix4x4_t84 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t84 *)args[0]), (Matrix4x4_t84 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Matrix4x4U26_t2133_Matrix4x4U26_t2133 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t84 * p1, Matrix4x4_t84 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t84 *)args[0], (Matrix4x4_t84 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t84 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t84  (*Func)(void* obj, const MethodInfo* method);
	Matrix4x4_t84  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t86_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t86  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector4_t86  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Vector4_t86 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t86  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t86 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector3_t81_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t81  (*Func)(void* obj, Vector3_t81  p1, const MethodInfo* method);
	Vector3_t81  ret = ((Func)method->method)(obj, *((Vector3_t81 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t84_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t84  (*Func)(void* obj, Vector3_t81  p1, const MethodInfo* method);
	Matrix4x4_t84  ret = ((Func)method->method)(obj, *((Vector3_t81 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
void* RuntimeInvoker_Void_t765_Vector3_t81_Quaternion_t83_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t81  p1, Quaternion_t83  p2, Vector3_t81  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t81 *)args[0]), *((Quaternion_t83 *)args[1]), *((Vector3_t81 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t84_Vector3_t81_Quaternion_t83_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t84  (*Func)(void* obj, Vector3_t81  p1, Quaternion_t83  p2, Vector3_t81  p3, const MethodInfo* method);
	Matrix4x4_t84  ret = ((Func)method->method)(obj, *((Vector3_t81 *)args[0]), *((Quaternion_t83 *)args[1]), *((Vector3_t81 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t84_Vector3U26_t2129_QuaternionU26_t2134_Vector3U26_t2129 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t84  (*Func)(void* obj, Vector3_t81 * p1, Quaternion_t83 * p2, Vector3_t81 * p3, const MethodInfo* method);
	Matrix4x4_t84  ret = ((Func)method->method)(obj, (Vector3_t81 *)args[0], (Quaternion_t83 *)args[1], (Vector3_t81 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t84_Single_t321_Single_t321_Single_t321_Single_t321_Single_t321_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t84  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, const MethodInfo* method);
	Matrix4x4_t84  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t84_Single_t321_Single_t321_Single_t321_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t84  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Matrix4x4_t84  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t84_Matrix4x4_t84_Matrix4x4_t84 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t84  (*Func)(void* obj, Matrix4x4_t84  p1, Matrix4x4_t84  p2, const MethodInfo* method);
	Matrix4x4_t84  ret = ((Func)method->method)(obj, *((Matrix4x4_t84 *)args[0]), *((Matrix4x4_t84 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t86_Matrix4x4_t84_Vector4_t86 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t86  (*Func)(void* obj, Matrix4x4_t84  p1, Vector4_t86  p2, const MethodInfo* method);
	Vector4_t86  ret = ((Func)method->method)(obj, *((Matrix4x4_t84 *)args[0]), *((Vector4_t86 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Matrix4x4_t84_Matrix4x4_t84 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t84  p1, Matrix4x4_t84  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t84 *)args[0]), *((Matrix4x4_t84 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Vector3_t81_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t81  p1, Vector3_t81  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t81 *)args[0]), *((Vector3_t81 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t81  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t81 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
void* RuntimeInvoker_Void_t765_Bounds_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t85  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t85 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Bounds_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t85  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t85 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Bounds_t85_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t85  p1, Vector3_t81  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t85 *)args[0]), *((Vector3_t81 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_BoundsU26_t2135_Vector3U26_t2129 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t85 * p1, Vector3_t81 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t85 *)args[0], (Vector3_t81 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Bounds_t85_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t85  p1, Vector3_t81  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t85 *)args[0]), *((Vector3_t81 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_BoundsU26_t2135_Vector3U26_t2129 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t85 * p1, Vector3_t81 * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t85 *)args[0], (Vector3_t81 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
void* RuntimeInvoker_Boolean_t333_RayU26_t2136_BoundsU26_t2135_SingleU26_t2137 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t87 * p1, Bounds_t85 * p2, float* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t87 *)args[0], (Bounds_t85 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Ray_t87 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t87  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t87 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Ray_t87_SingleU26_t2137 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t87  p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t87 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t81_BoundsU26_t2135_Vector3U26_t2129 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t81  (*Func)(void* obj, Bounds_t85 * p1, Vector3_t81 * p2, const MethodInfo* method);
	Vector3_t81  ret = ((Func)method->method)(obj, (Bounds_t85 *)args[0], (Vector3_t81 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Bounds_t85_Bounds_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t85  p1, Bounds_t85  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t85 *)args[0]), *((Bounds_t85 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Vector4_t86_Vector4_t86 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t86  p1, Vector4_t86  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t86 *)args[0]), *((Vector4_t86 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Vector4_t86 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t86  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t86 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t86_Vector4_t86_Vector4_t86 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t86  (*Func)(void* obj, Vector4_t86  p1, Vector4_t86  p2, const MethodInfo* method);
	Vector4_t86  ret = ((Func)method->method)(obj, *((Vector4_t86 *)args[0]), *((Vector4_t86 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Vector4_t86_Vector4_t86 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t86  p1, Vector4_t86  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t86 *)args[0]), *((Vector4_t86 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Single_t321_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Single_t321_Single_t321_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Single_t321_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
void* RuntimeInvoker_Void_t765_SphericalHarmonicsL2U26_t2138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SphericalHarmonicsL2_t98 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (SphericalHarmonicsL2_t98 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Color_t38_SphericalHarmonicsL2U26_t2138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t38  p1, SphericalHarmonicsL2_t98 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t38 *)args[0]), (SphericalHarmonicsL2_t98 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_ColorU26_t2131_SphericalHarmonicsL2U26_t2138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t38 * p1, SphericalHarmonicsL2_t98 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t38 *)args[0], (SphericalHarmonicsL2_t98 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Vector3_t81_Color_t38_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t81  p1, Color_t38  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t81 *)args[0]), *((Color_t38 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Vector3_t81_Color_t38_SphericalHarmonicsL2U26_t2138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t81  p1, Color_t38  p2, SphericalHarmonicsL2_t98 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t81 *)args[0]), *((Color_t38 *)args[1]), (SphericalHarmonicsL2_t98 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Vector3U26_t2129_ColorU26_t2131_SphericalHarmonicsL2U26_t2138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t81 * p1, Color_t38 * p2, SphericalHarmonicsL2_t98 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t81 *)args[0], (Color_t38 *)args[1], (SphericalHarmonicsL2_t98 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t98_SphericalHarmonicsL2_t98_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t98  (*Func)(void* obj, SphericalHarmonicsL2_t98  p1, float p2, const MethodInfo* method);
	SphericalHarmonicsL2_t98  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t98 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t98_Single_t321_SphericalHarmonicsL2_t98 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t98  (*Func)(void* obj, float p1, SphericalHarmonicsL2_t98  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t98  ret = ((Func)method->method)(obj, *((float*)args[0]), *((SphericalHarmonicsL2_t98 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t98_SphericalHarmonicsL2_t98_SphericalHarmonicsL2_t98 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t98  (*Func)(void* obj, SphericalHarmonicsL2_t98  p1, SphericalHarmonicsL2_t98  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t98  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t98 *)args[0]), *((SphericalHarmonicsL2_t98 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_SphericalHarmonicsL2_t98_SphericalHarmonicsL2_t98 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SphericalHarmonicsL2_t98  p1, SphericalHarmonicsL2_t98  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t98 *)args[0]), *((SphericalHarmonicsL2_t98 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t349_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t349_SByte_t349_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Rect_t42 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t42  (*Func)(void* obj, const MethodInfo* method);
	Rect_t42  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_RectU26_t2130 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t42 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t42 *)args[0], method);
	return NULL;
}

struct Object_t;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
void* RuntimeInvoker_CameraClearFlags_t228 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Ray_t87_Vector3_t81 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t87  (*Func)(void* obj, Vector3_t81  p1, const MethodInfo* method);
	Ray_t87  ret = ((Func)method->method)(obj, *((Vector3_t81 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Ray_t87_Object_t_Vector3U26_t2129 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t87  (*Func)(void* obj, Object_t * p1, Vector3_t81 * p2, const MethodInfo* method);
	Ray_t87  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t81 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Ray_t87_Single_t321_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t87  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t87 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_RayU26_t2136_Single_t321_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t87 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t87 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
void* RuntimeInvoker_RenderBuffer_t227 (const MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t227  (*Func)(void* obj, const MethodInfo* method);
	RenderBuffer_t227  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_IntPtr_t_Int32U26_t2139_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_IntPtr_t_RenderBufferU26_t2140_RenderBufferU26_t2140 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t227 * p2, RenderBuffer_t227 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t227 *)args[1], (RenderBuffer_t227 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_IntPtr_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_IntPtr_t_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_IntPtr_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int32_t320_Int32_t320_Int32U26_t2139_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Vector3U26_t2129 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t81 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t81 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t349_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
void* RuntimeInvoker_SendMessageOptions_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
void* RuntimeInvoker_AnimatorStateInfo_t133 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t133  (*Func)(void* obj, const MethodInfo* method);
	AnimatorStateInfo_t133  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
void* RuntimeInvoker_AnimatorClipInfo_t134 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorClipInfo_t134  (*Func)(void* obj, const MethodInfo* method);
	AnimatorClipInfo_t134  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Color_t38_Int32_t320_Single_t321_Single_t321_Int32_t320_SByte_t349_SByte_t349_Int32_t320_Int32_t320_Int32_t320_Int32_t320_SByte_t349_Int32_t320_Vector2_t43_Vector2_t43_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t38  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, Vector2_t43  p16, Vector2_t43  p17, int8_t p18, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t38 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((Vector2_t43 *)args[15]), *((Vector2_t43 *)args[16]), *((int8_t*)args[17]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Color_t38_Int32_t320_Single_t321_Single_t321_Int32_t320_SByte_t349_SByte_t349_Int32_t320_Int32_t320_Int32_t320_Int32_t320_SByte_t349_Int32_t320_Single_t321_Single_t321_Single_t321_Single_t321_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t38  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t38 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t_ColorU26_t2131_Int32_t320_Single_t321_Single_t321_Int32_t320_SByte_t349_SByte_t349_Int32_t320_Int32_t320_Int32_t320_Int32_t320_SByte_t349_Int32_t320_Single_t321_Single_t321_Single_t321_Single_t321_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Color_t38 * p4, int32_t p5, float p6, float p7, int32_t p8, int8_t p9, int8_t p10, int32_t p11, int32_t p12, int32_t p13, int32_t p14, int8_t p15, int32_t p16, float p17, float p18, float p19, float p20, int8_t p21, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Color_t38 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((float*)args[6]), *((int32_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int32_t*)args[13]), *((int8_t*)args[14]), *((int32_t*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((float*)args[19]), *((int8_t*)args[20]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
void* RuntimeInvoker_TextGenerationSettings_t155_TextGenerationSettings_t155 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t155  (*Func)(void* obj, TextGenerationSettings_t155  p1, const MethodInfo* method);
	TextGenerationSettings_t155  ret = ((Func)method->method)(obj, *((TextGenerationSettings_t155 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t321_Object_t_TextGenerationSettings_t155 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t155  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t155 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_TextGenerationSettings_t155 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t155  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t155 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
void* RuntimeInvoker_SourceID_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
void* RuntimeInvoker_AppID_t176 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt16
#include "mscorlib_System_UInt16.h"
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t351_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt64
#include "mscorlib_System_UInt64.h"
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t348_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt32
#include "mscorlib_System_UInt32.h"
void* RuntimeInvoker_UInt32_t336 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
void* RuntimeInvoker_NetworkID_t178 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_UInt64_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
void* RuntimeInvoker_NodeID_t179 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_UInt16_t351 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_SByte_t349_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t348_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t348_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t348_UInt16_t351_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, uint16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), *((uint16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_ObjectU26_t2141 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
void* RuntimeInvoker_Void_t765_KeyValuePair_2_t298 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t298  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t298 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_KeyValuePair_2_t298 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t298  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t298 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32U26_t2139_BooleanU26_t2142 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (bool*)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_SByte_t349_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
void* RuntimeInvoker_UserState_t243 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Double_t344_SByte_t349_SByte_t349_DateTime_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t51  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t51 *)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_DateTime_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t349_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int64_t345_Object_t_DateTime_t51_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t51  p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t51 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
void* RuntimeInvoker_UserScope_t244 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
void* RuntimeInvoker_Range_t238 (const MethodInfo* method, void* obj, void** args)
{
	typedef Range_t238  (*Func)(void* obj, const MethodInfo* method);
	Range_t238  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Range_t238 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t238  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Range_t238 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
void* RuntimeInvoker_TimeScope_t245 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
void* RuntimeInvoker_Void_t765_Int32_t320_HitInfo_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t239  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t239 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_HitInfo_t239_HitInfo_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t239  p1, HitInfo_t239  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t239 *)args[0]), *((HitInfo_t239 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_HitInfo_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t239  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t239 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct String_t;
// System.String
#include "mscorlib_System_String.h"
struct String_t;
void* RuntimeInvoker_Void_t765_Object_t_StringU26_t2143_StringU26_t2143 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
void* RuntimeInvoker_Void_t765_Object_t_StreamingContext_t311 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t311  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t311 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_AnimatorStateInfo_t133_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, AnimatorStateInfo_t133  p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((AnimatorStateInfo_t133 *)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Color_t38_Color_t38 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t38  p1, Color_t38  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t38 *)args[0]), *((Color_t38 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_TextGenerationSettings_t155 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TextGenerationSettings_t155  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TextGenerationSettings_t155 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DictionaryNode_t388;
// System.Collections.Specialized.ListDictionary/DictionaryNode
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t2144 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t388 ** p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t388 **)args[1], method);
	return ret;
}

struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
void* RuntimeInvoker_DictionaryEntry_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t560  (*Func)(void* obj, const MethodInfo* method);
	DictionaryEntry_t560  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"
void* RuntimeInvoker_EditorBrowsableState_t401 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int16
#include "mscorlib_System_Int16.h"
void* RuntimeInvoker_Int16_t350_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct IPAddress_t424;
// System.Net.IPAddress
#include "System_System_Net_IPAddress.h"
void* RuntimeInvoker_Boolean_t333_Object_t_IPAddressU26_t2145 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t424 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t424 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Net.Sockets.AddressFamily
#include "System_System_Net_Sockets_AddressFamily.h"
void* RuntimeInvoker_AddressFamily_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct IPv6Address_t425;
// System.Net.IPv6Address
#include "System_System_Net_IPv6Address.h"
void* RuntimeInvoker_Boolean_t333_Object_t_IPv6AddressU26_t2146 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t425 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t425 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t351_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

struct Object_t;
// System.Net.SecurityProtocolType
#include "System_System_Net_SecurityProtocolType.h"
void* RuntimeInvoker_SecurityProtocolType_t426 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_SByte_t349_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_SByte_t349_SByte_t349_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// System.Security.Cryptography.AsnDecodeStatus
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
struct Object_t;
void* RuntimeInvoker_AsnDecodeStatus_t477_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t464_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t464_Object_t_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t464_Object_t_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t464 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32U26_t2139_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509RevocationFlag
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
void* RuntimeInvoker_X509RevocationFlag_t471 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509RevocationMode
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
void* RuntimeInvoker_X509RevocationMode_t472 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509VerificationFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
void* RuntimeInvoker_X509VerificationFlags_t476 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
void* RuntimeInvoker_X509KeyUsageFlags_t469 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509KeyUsageFlags_t469_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
// System.Byte
#include "mscorlib_System_Byte.h"
void* RuntimeInvoker_Byte_t326_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t326_Int16_t350_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t320_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
// System.Text.RegularExpressions.RegexOptions
#include "System_System_Text_RegularExpressions_RegexOptions.h"
void* RuntimeInvoker_RegexOptions_t496 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"
struct Object_t;
void* RuntimeInvoker_Category_t503_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_UInt16_t351_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32_t320_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int16_t350_SByte_t349_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_UInt16_t351_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int16_t350_Int16_t350_SByte_t349_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int16_t350_Object_t_SByte_t349_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_SByte_t349_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_SByte_t349_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt16_t351_UInt16_t351_UInt16_t351 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.OpFlags
#include "System_System_Text_RegularExpressions_OpFlags.h"
void* RuntimeInvoker_OpFlags_t498_SByte_t349_SByte_t349_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_UInt16_t351_UInt16_t351 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32_t320_Int32U26_t2139_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32_t320_Int32U26_t2139_Int32U26_t2139_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32U26_t2139_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_UInt16_t351_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32_t320_Int32_t320_SByte_t349_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32U26_t2139_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_SByte_t349_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
void* RuntimeInvoker_Interval_t518 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t518  (*Func)(void* obj, const MethodInfo* method);
	Interval_t518  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Interval_t518 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t518  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t518 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Interval_t518 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t518  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t518 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Interval_t518_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t518  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Interval_t518  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Double_t344_Interval_t518 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t518  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t518 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Interval_t518_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t518  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t518 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t344_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32U26_t2139_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32U26_t2139_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RegexOptionsU26_t2147 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_RegexOptionsU26_t2147_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32U26_t2139_Int32U26_t2139_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Category_t503 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int16_t350_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t576_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32U26_t2139_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32U26_t2139_Int32U26_t2139_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_UInt16_t351_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int16_t350_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_UInt16_t351 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// System.Text.RegularExpressions.Position
#include "System_System_Text_RegularExpressions_Position.h"
void* RuntimeInvoker_Position_t499 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UriHostNameType
#include "System_System_UriHostNameType.h"
struct Object_t;
void* RuntimeInvoker_UriHostNameType_t551_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct String_t;
void* RuntimeInvoker_Void_t765_StringU26_t2143 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t349_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t576_Object_t_Int32U26_t2139_CharU26_t2148 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct UriFormatException_t550;
// System.UriFormatException
#include "System_System_UriFormatException.h"
void* RuntimeInvoker_Void_t765_Object_t_UriFormatExceptionU26_t2149 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t550 ** p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t550 **)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_SByte_t349_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t336_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t336_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.BigInteger/Sign
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Sign_t624_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
void* RuntimeInvoker_ConfidenceFactor_t628 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_SByte_t349_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32U26_t2139_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct ByteU5BU5D_t102;
#include "mscorlib_ArrayTypes.h"
void* RuntimeInvoker_Void_t765_Object_t_Int32U26_t2139_ByteU26_t2150_Int32U26_t2139_ByteU5BU5DU26_t2151 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t102** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t102**)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t51_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t586 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t586  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t586 *)args[1]), method);
	return ret;
}

struct Object_t;
// System.Security.Cryptography.RSAParameters
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
void* RuntimeInvoker_RSAParameters_t584_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t584  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	RSAParameters_t584  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_RSAParameters_t584 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t584  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t584 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_DSAParameters_t586_BooleanU26_t2142 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t586  (*Func)(void* obj, bool* p1, const MethodInfo* method);
	DSAParameters_t586  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t349_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_DateTime_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t51  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.X509.X509ChainStatusFlags
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
void* RuntimeInvoker_X509ChainStatusFlags_t653 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Byte_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Byte_t326_Byte_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.AlertLevel
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
void* RuntimeInvoker_AlertLevel_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.AlertDescription
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
void* RuntimeInvoker_AlertDescription_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int16_t350_Object_t_Int32_t320_Int32_t320_Int32_t320_SByte_t349_SByte_t349_SByte_t349_SByte_t349_Int16_t350_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.CipherAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
void* RuntimeInvoker_CipherAlgorithmType_t669 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.HashAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
void* RuntimeInvoker_HashAlgorithmType_t687 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
void* RuntimeInvoker_ExchangeAlgorithmType_t685 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
void* RuntimeInvoker_CipherMode_t617 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct ByteU5BU5D_t102;
struct ByteU5BU5D_t102;
void* RuntimeInvoker_Void_t765_Object_t_ByteU5BU5DU26_t2151_ByteU5BU5DU26_t2151 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t102** p2, ByteU5BU5D_t102** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t102**)args[1], (ByteU5BU5D_t102**)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t326_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t350_Object_t_Int32_t320_Int32_t320_Int32_t320_SByte_t349_SByte_t349_SByte_t349_SByte_t349_Int16_t350_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.SecurityProtocolType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
void* RuntimeInvoker_SecurityProtocolType_t701 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.SecurityCompressionType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
void* RuntimeInvoker_SecurityCompressionType_t700 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
void* RuntimeInvoker_HandshakeType_t714 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.HandshakeState
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
void* RuntimeInvoker_HandshakeState_t686 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SecurityProtocolType_t701_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t326_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t326_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Byte_t326_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t326_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_SByte_t349_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int64_t345_Int64_t345_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Int32_t320_Int32_t320_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Byte_t326_Byte_t326_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_RSAParameters_t584 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t584  (*Func)(void* obj, const MethodInfo* method);
	RSAParameters_t584  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Byte_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Byte_t326_Byte_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Byte_t326_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.ContentType
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
void* RuntimeInvoker_ContentType_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t200;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_ObjectU5BU5DU26_t2152 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t200** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t200**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t200;
void* RuntimeInvoker_Int32_t320_Object_t_ObjectU5BU5DU26_t2152 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t200** p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t200**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t326_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t576_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Decimal
#include "mscorlib_System_Decimal.h"
struct Object_t;
void* RuntimeInvoker_Decimal_t347_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t347  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Decimal_t347  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t350_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t345_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t349_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t321_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t351_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t336_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t348_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t65;
// System.Exception
#include "mscorlib_System_Exception.h"
void* RuntimeInvoker_Boolean_t333_SByte_t349_Object_t_Int32_t320_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t65 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t65 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Object_t_SByte_t349_Int32U26_t2139_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t65 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t65 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Int32_t320_SByte_t349_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t65 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t65 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Int32U26_t2139_Object_t_SByte_t349_SByte_t349_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t65 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t65 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32U26_t2139_Object_t_Object_t_BooleanU26_t2142_BooleanU26_t2142 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32U26_t2139_Object_t_Object_t_BooleanU26_t2142 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Int32U26_t2139_Object_t_Int32U26_t2139_SByte_t349_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t65 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t65 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32U26_t2139_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int16_t350_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_SByte_t349_Int32U26_t2139_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t65 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t65 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Object_t_SByte_t349_Int64U26_t2154_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t65 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t65 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t345_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_SByte_t349_Int64U26_t2154_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t65 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t65 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t345_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int64U26_t2154 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_Int64U26_t2154 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Object_t_SByte_t349_UInt32U26_t2155_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t65 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t65 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_SByte_t349_UInt32U26_t2155_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t65 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t65 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t336_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t336_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_UInt32U26_t2155 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_UInt32U26_t2155 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t348_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_SByte_t349_UInt64U26_t2156_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t65 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t65 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t348_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_UInt64U26_t2156 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t326_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t326_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_ByteU26_t2150 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_ByteU26_t2150 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Object_t_SByte_t349_SByteU26_t2157_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t65 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t65 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t349_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t349_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_SByteU26_t2157 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Object_t_SByte_t349_Int16U26_t2158_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t65 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t65 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t350_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t350_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int16U26_t2158 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t351_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t351_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_UInt16U26_t2159 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_UInt16U26_t2159 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_ByteU2AU26_t2160_ByteU2AU26_t2160_DoubleU2AU26_t2161_UInt16U2AU26_t2162_UInt16U2AU26_t2162_UInt16U2AU26_t2162_UInt16U2AU26_t2162 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

struct Object_t;
// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategory.h"
void* RuntimeInvoker_UnicodeCategory_t910_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t576_Int16_t350_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int16_t350_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Char_t576_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Object_t_SByte_t349_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t_Int32_t320_Int32_t320_SByte_t349_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int16_t350_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t350_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32U26_t2139_Int32U26_t2139_Int32U26_t2139_BooleanU26_t2142_StringU26_t2143 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t350_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t321_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t344_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t344_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_SByte_t349_DoubleU26_t2163_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t65 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t65 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_DoubleU26_t2163 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_DoubleU26_t2163 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, double* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (double*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Int32_t320_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Decimal_t347_Decimal_t347_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t347  (*Func)(void* obj, Decimal_t347  p1, Decimal_t347  p2, const MethodInfo* method);
	Decimal_t347  ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), *((Decimal_t347 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t348_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t345_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Decimal_t347_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t347  p1, Decimal_t347  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), *((Decimal_t347 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t347_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t347  (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	Decimal_t347  ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Decimal_t347_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t347  p1, Decimal_t347  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), *((Decimal_t347 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t347_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t347  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Decimal_t347  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t_Int32U26_t2139_BooleanU26_t2142_BooleanU26_t2142_Int32U26_t2139_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t347_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t347  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Decimal_t347  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_DecimalU26_t2164_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t347 * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t347 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_DecimalU26_t2164_UInt64U26_t2156 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t347 * p1, uint64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t347 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_DecimalU26_t2164_Int64U26_t2154 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t347 * p1, int64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t347 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_DecimalU26_t2164_DecimalU26_t2164 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t347 * p1, Decimal_t347 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t347 *)args[0], (Decimal_t347 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_DecimalU26_t2164_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t347 * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t347 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_DecimalU26_t2164_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t347 * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t347 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t344_DecimalU26_t2164 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t347 * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t347 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_DecimalU26_t2164_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t347 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t347 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_DecimalU26_t2164_DecimalU26_t2164_DecimalU26_t2164 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t347 * p1, Decimal_t347 * p2, Decimal_t347 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t347 *)args[0], (Decimal_t347 *)args[1], (Decimal_t347 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t326_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t349_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t350_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t351_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t336_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t347_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t347  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Decimal_t347  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t347_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t347  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Decimal_t347  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t347_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t347  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Decimal_t347  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t347_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t347  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Decimal_t347  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t347_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t347  (*Func)(void* obj, float p1, const MethodInfo* method);
	Decimal_t347  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t347_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t347  (*Func)(void* obj, double p1, const MethodInfo* method);
	Decimal_t347  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t344_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_UInt64_t348_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t336_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
void* RuntimeInvoker_UIntPtr_t_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct MulticastDelegate_t47;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t2165 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t47 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t47 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t320_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
// System.TypeCode
#include "mscorlib_System_TypeCode.h"
void* RuntimeInvoker_TypeCode_t1355 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Object_t_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t348_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int64_t345_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t345_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t345_Int64_t345_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int64_t345_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int64_t345_Int64_t345_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int64_t345_Object_t_Int64_t345_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Int32_t320_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"
void* RuntimeInvoker_TypeAttributes_t1027 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypes.h"
void* RuntimeInvoker_MemberTypes_t1006 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
void* RuntimeInvoker_RuntimeTypeHandle_t767 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t767  (*Func)(void* obj, const MethodInfo* method);
	RuntimeTypeHandle_t767  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TypeCode_t1355_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t767 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t767  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t767 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_RuntimeTypeHandle_t767_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t767  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RuntimeTypeHandle_t767  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Object_t_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_SByte_t349_SByte_t349_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
void* RuntimeInvoker_Void_t765_Object_t_RuntimeFieldHandle_t768 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t768  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t768 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_IntPtr_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ContractionU5BU5D_t814;
struct Level2MapU5BU5D_t815;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_ContractionU5BU5DU26_t2166_Level2MapU5BU5DU26_t2167 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t814** p3, Level2MapU5BU5D_t815** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t814**)args[2], (Level2MapU5BU5D_t815**)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct CodePointIndexer_t799;
// Mono.Globalization.Unicode.CodePointIndexer
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
struct CodePointIndexer_t799;
void* RuntimeInvoker_Void_t765_Object_t_CodePointIndexerU26_t2168_ByteU2AU26_t2160_ByteU2AU26_t2160_CodePointIndexerU26_t2168_ByteU2AU26_t2160 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t799 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t799 ** p5, uint8_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t799 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t799 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t326_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t326_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
void* RuntimeInvoker_ExtenderType_t811_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320_BooleanU26_t2142_BooleanU26_t2142_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
// Mono.Globalization.Unicode.SimpleCollator/Context
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320_BooleanU26_t2142_BooleanU26_t2142_SByte_t349_SByte_t349_ContextU26_t2169 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t808 * p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t808 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Int32_t320_Int32_t320_SByte_t349_ContextU26_t2169 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t808 * p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t808 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Object_t_Int32_t320_Int32_t320_BooleanU26_t2142 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Object_t_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int16_t350_Int32_t320_SByte_t349_ContextU26_t2169 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t808 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t808 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Object_t_Int32_t320_Int32_t320_Object_t_ContextU26_t2169 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t808 * p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t808 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320_Object_t_Int32_t320_SByte_t349_ContextU26_t2169 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t808 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t808 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32U26_t2139_Int32_t320_Int32_t320_Object_t_SByte_t349_ContextU26_t2169 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t808 * p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t808 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Contraction_t801;
// Mono.Globalization.Unicode.Contraction
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
void* RuntimeInvoker_Boolean_t333_Object_t_Int32U26_t2139_Int32_t320_Int32_t320_Object_t_SByte_t349_Int32_t320_ContractionU26_t2170_ContextU26_t2169 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t801 ** p8, Context_t808 * p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t801 **)args[7], (Context_t808 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32U26_t2139_Int32_t320_Int32_t320_Int32_t320_Object_t_SByte_t349_ContextU26_t2169 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t808 * p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t808 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Contraction_t801;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32U26_t2139_Int32_t320_Int32_t320_Int32_t320_Object_t_SByte_t349_Int32_t320_ContractionU26_t2170_ContextU26_t2169 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t801 ** p9, Context_t808 * p10, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t801 **)args[8], (Context_t808 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Object_t_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct ByteU5BU5D_t102;
void* RuntimeInvoker_Void_t765_SByte_t349_ByteU5BU5DU26_t2151_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t102** p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t102**)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
void* RuntimeInvoker_ConfidenceFactor_t820 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.BigInteger/Sign
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Sign_t822_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DSAParameters_t586_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t586  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DSAParameters_t586  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_DSAParameters_t586 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t586  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t586 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t350_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t321_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t344_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t350_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Single_t321_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Single_t321_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Single_t321_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct MethodBase_t379;
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
struct String_t;
void* RuntimeInvoker_Boolean_t333_Int32_t320_SByte_t349_MethodBaseU26_t2171_Int32U26_t2139_Int32U26_t2139_StringU26_t2143_Int32U26_t2139_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t379 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t379 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_DateTime_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t51  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"
void* RuntimeInvoker_DayOfWeek_t1306_DateTime_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t51  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int32U26_t2139_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DayOfWeek_t1306_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32U26_t2139_Int32U26_t2139_Int32U26_t2139_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
void* RuntimeInvoker_Void_t765_DateTime_t51_DateTime_t51_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t51  p1, DateTime_t51  p2, TimeSpan_t463  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t51 *)args[0]), *((DateTime_t51 *)args[1]), *((TimeSpan_t463 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t463  (*Func)(void* obj, const MethodInfo* method);
	TimeSpan_t463  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t347  (*Func)(void* obj, const MethodInfo* method);
	Decimal_t347  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t351 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_IntPtr_t_Int32_t320_SByte_t349_Int32_t320_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320_SByte_t349_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_IntPtr_t_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOError.h"
void* RuntimeInvoker_Boolean_t333_Object_t_MonoIOErrorU26_t2172 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t320_Int32_t320_MonoIOErrorU26_t2172 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_MonoIOErrorU26_t2172 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributes.h"
struct Object_t;
void* RuntimeInvoker_FileAttributes_t920_Object_t_MonoIOErrorU26_t2172 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileType.h"
void* RuntimeInvoker_MonoFileType_t929_IntPtr_t_MonoIOErrorU26_t2172 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// System.IO.MonoIOStat
#include "mscorlib_System_IO_MonoIOStat.h"
void* RuntimeInvoker_Boolean_t333_Object_t_MonoIOStatU26_t2173_MonoIOErrorU26_t2172 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t928 * p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t928 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320_MonoIOErrorU26_t2172 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_IntPtr_t_MonoIOErrorU26_t2172 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_IntPtr_t_Object_t_Int32_t320_Int32_t320_MonoIOErrorU26_t2172 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t345_IntPtr_t_Int64_t345_Int32_t320_MonoIOErrorU26_t2172 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t345_IntPtr_t_MonoIOErrorU26_t2172 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_IntPtr_t_Int64_t345_MonoIOErrorU26_t2172 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"
void* RuntimeInvoker_CallingConventions_t1001 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
void* RuntimeInvoker_RuntimeMethodHandle_t1347 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t1347  (*Func)(void* obj, const MethodInfo* method);
	RuntimeMethodHandle_t1347  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributes.h"
void* RuntimeInvoker_MethodAttributes_t1007 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.Emit.MethodToken
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
void* RuntimeInvoker_MethodToken_t968 (const MethodInfo* method, void* obj, void** args)
{
	typedef MethodToken_t968  (*Func)(void* obj, const MethodInfo* method);
	MethodToken_t968  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributes.h"
void* RuntimeInvoker_FieldAttributes_t1004 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RuntimeFieldHandle_t768 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t768  (*Func)(void* obj, const MethodInfo* method);
	RuntimeFieldHandle_t768  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Int32_t320_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.OpCode
#include "mscorlib_System_Reflection_Emit_OpCode.h"
void* RuntimeInvoker_Void_t765_OpCode_t972 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t972  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t972 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_OpCode_t972_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t972  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t972 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.StackBehaviour
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
void* RuntimeInvoker_StackBehaviour_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributes.h"
void* RuntimeInvoker_PropertyAttributes_t1023 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Int32_t320_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Int32_t320_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_SByte_t349_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Module_t965;
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t2139_ModuleU26_t2174 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t965 ** p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t965 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
void* RuntimeInvoker_AssemblyNameFlags_t996 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t200;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Object_t_ObjectU5BU5DU26_t2152_Object_t_Object_t_Object_t_ObjectU26_t2141 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t200** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t200**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

struct Object_t;
struct ObjectU5BU5D_t200;
struct Object_t;
void* RuntimeInvoker_Void_t765_ObjectU5BU5DU26_t2152_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t200** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t200**)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t200;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_ObjectU5BU5DU26_t2152_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t200** p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t200**)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Object_t_Object_t_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributes.h"
void* RuntimeInvoker_EventAttributes_t1002 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t768 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t768  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t768 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_StreamingContext_t311 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t311  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t311 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t1347 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t1347  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t1347 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfo.h"
void* RuntimeInvoker_Void_t765_Object_t_MonoEventInfoU26_t2175 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t1012 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t1012 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_MonoEventInfo_t1012_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t1012  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	MonoEventInfo_t1012  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
void* RuntimeInvoker_Void_t765_IntPtr_t_MonoMethodInfoU26_t2176 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t1015 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t1015 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_MonoMethodInfo_t1015_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t1015  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	MonoMethodInfo_t1015  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MethodAttributes_t1007_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_CallingConventions_t1001_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t65 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t65 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
void* RuntimeInvoker_Void_t765_Object_t_MonoPropertyInfoU26_t2177_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t1016 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t1016 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributes.h"
void* RuntimeInvoker_ParameterAttributes_t1019 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
struct Object_t;
void* RuntimeInvoker_GCHandle_t1048_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t1048  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	GCHandle_t1048  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_IntPtr_t_Int32_t320_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_IntPtr_t_Object_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_BooleanU26_t2142 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t2143 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t2143 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, String_t** p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (String_t**)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t463  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t463 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_SByte_t349_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t311_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t311  p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t311 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ISurrogateSelector_t1100;
void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t311_ISurrogateSelectorU26_t2178 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t311  p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t311 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TimeSpan_t463_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t463  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TimeSpan_t463  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Object_t_StringU26_t2143 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (String_t**)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t2141 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct String_t;
struct String_t;
void* RuntimeInvoker_Boolean_t333_Object_t_StringU26_t2143_StringU26_t2143 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
void* RuntimeInvoker_WellKnownObjectMode_t1141 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_StreamingContext_t311 (const MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t311  (*Func)(void* obj, const MethodInfo* method);
	StreamingContext_t311  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
void* RuntimeInvoker_TypeFilterLevel_t1157 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_BooleanU26_t2142 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t326_Object_t_SByte_t349_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t326_Object_t_SByte_t349_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct HeaderU5BU5D_t1362;
void* RuntimeInvoker_Void_t765_Object_t_SByte_t349_ObjectU26_t2141_HeaderU5BU5DU26_t2179 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t1362** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t1362**)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct HeaderU5BU5D_t1362;
void* RuntimeInvoker_Void_t765_Byte_t326_Object_t_SByte_t349_ObjectU26_t2141_HeaderU5BU5DU26_t2179 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t1362** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t1362**)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Byte_t326_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t310;
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
void* RuntimeInvoker_Void_t765_Byte_t326_Object_t_Int64U26_t2154_ObjectU26_t2141_SerializationInfoU26_t2180 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t310 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t310 **)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t310;
void* RuntimeInvoker_Void_t765_Object_t_SByte_t349_SByte_t349_Int64U26_t2154_ObjectU26_t2141_SerializationInfoU26_t2180 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t310 ** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t310 **)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t310;
void* RuntimeInvoker_Void_t765_Object_t_Int64U26_t2154_ObjectU26_t2141_SerializationInfoU26_t2180 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t310 ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t310 **)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t310;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Int64_t345_ObjectU26_t2141_SerializationInfoU26_t2180 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t310 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t310 **)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int64_t345_Object_t_Object_t_Int64_t345_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int64U26_t2154_ObjectU26_t2141 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Int64U26_t2154_ObjectU26_t2141 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Int64_t345_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int64_t345_Int64_t345_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t345_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Byte_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int64_t345_Int32_t320_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int64_t345_Object_t_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int64_t345_Object_t_Int64_t345_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_SByte_t349_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_StreamingContext_t311 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t311  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t311 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_StreamingContext_t311 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t311  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t311 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_StreamingContext_t311 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t311  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t311 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_StreamingContext_t311_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t311  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t311 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_DateTime_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t51  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t51 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
void* RuntimeInvoker_SerializationEntry_t1174 (const MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t1174  (*Func)(void* obj, const MethodInfo* method);
	SerializationEntry_t1174  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
void* RuntimeInvoker_StreamingContextStates_t1177 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
void* RuntimeInvoker_CspProviderFlags_t1181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t336_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int64_t345_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t336_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_UInt32U26_t2155_Int32_t320_UInt32U26_t2155_Int32_t320_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int64_t345_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt64_t348_Int64_t345_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t348_Int64_t345_Int64_t345_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t348_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.PaddingMode
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
void* RuntimeInvoker_PaddingMode_t619 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct StringBuilder_t301;
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
void* RuntimeInvoker_Void_t765_StringBuilderU26_t2181_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t301 ** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t301 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct EncoderFallbackBuffer_t1254;
// System.Text.EncoderFallbackBuffer
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
struct CharU5BU5D_t300;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_EncoderFallbackBufferU26_t2182_CharU5BU5DU26_t2183 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t1254 ** p6, CharU5BU5D_t300** p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t1254 **)args[5], (CharU5BU5D_t300**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1245;
// System.Text.DecoderFallbackBuffer
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_DecoderFallbackBufferU26_t2184 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t1245 ** p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t1245 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int16_t350_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int16_t350_Int16_t350_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int16_t350_Int16_t350_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t320_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_SByte_t349_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_SByte_t349_Int32_t320_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_SByte_t349_Int32U26_t2139_BooleanU26_t2142_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_CharU26_t2148_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_CharU26_t2148_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_CharU26_t2148_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t_Int32_t320_CharU26_t2148_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1245;
struct ByteU5BU5D_t102;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Object_t_DecoderFallbackBufferU26_t2184_ByteU5BU5DU26_t2151_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t1245 ** p7, ByteU5BU5D_t102** p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t1245 **)args[6], (ByteU5BU5D_t102**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1245;
struct ByteU5BU5D_t102;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320_Object_t_DecoderFallbackBufferU26_t2184_ByteU5BU5DU26_t2151_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t1245 ** p6, ByteU5BU5D_t102** p7, int8_t p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t1245 **)args[5], (ByteU5BU5D_t102**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1245;
struct ByteU5BU5D_t102;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_DecoderFallbackBufferU26_t2184_ByteU5BU5DU26_t2151_Object_t_Int64_t345_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1245 ** p2, ByteU5BU5D_t102** p3, Object_t * p4, int64_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1245 **)args[1], (ByteU5BU5D_t102**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1245;
struct ByteU5BU5D_t102;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_DecoderFallbackBufferU26_t2184_ByteU5BU5DU26_t2151_Object_t_Int64_t345_Int32_t320_Object_t_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1245 ** p2, ByteU5BU5D_t102** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1245 **)args[1], (ByteU5BU5D_t102**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1245;
struct ByteU5BU5D_t102;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_UInt32U26_t2155_UInt32U26_t2155_Object_t_DecoderFallbackBufferU26_t2184_ByteU5BU5DU26_t2151_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t1245 ** p9, ByteU5BU5D_t102** p10, int8_t p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t1245 **)args[8], (ByteU5BU5D_t102**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1245;
struct ByteU5BU5D_t102;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t_Int32_t320_UInt32U26_t2155_UInt32U26_t2155_Object_t_DecoderFallbackBufferU26_t2184_ByteU5BU5DU26_t2151_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t1245 ** p8, ByteU5BU5D_t102** p9, int8_t p10, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t1245 **)args[7], (ByteU5BU5D_t102**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_SByte_t349_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t349_Object_t_BooleanU26_t2142 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t349_SByte_t349_Object_t_BooleanU26_t2142 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_TimeSpan_t463_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t463  p1, TimeSpan_t463  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t463 *)args[0]), *((TimeSpan_t463 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int64_t345_Int64_t345_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_IntPtr_t_Int32_t320_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t345_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t345_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Byte_t326_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t326_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t326_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t326_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t576_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t576_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t576_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t576_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t51_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t51_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t51_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t51_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t51_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, float p1, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t51_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t344_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t344_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t344_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t344_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t344_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t344_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t350_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t350_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t350_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t350_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t350_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t345_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t345_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t345_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t345_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t349_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t349_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t349_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t349_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t349_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t321_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t351_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t351_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t351_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t351_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t351_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t336_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t336_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t336_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t336_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t336_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t348_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t348_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t348_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t348_Single_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t348_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_SByte_t349_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t463  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t463 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int64_t345_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_DayOfWeek_t1306 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
void* RuntimeInvoker_DateTimeKind_t1304 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t51_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, TimeSpan_t463  p1, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, *((TimeSpan_t463 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t51_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, double p1, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_DateTime_t51_DateTime_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t51  p1, DateTime_t51  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), *((DateTime_t51 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t51_DateTime_t51_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, DateTime_t51  p1, int32_t p2, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t51_Object_t_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Int32_t320_DateTimeU26_t2185_DateTimeOffsetU26_t2186_SByte_t349_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t51 * p4, DateTimeOffset_t363 * p5, int8_t p6, Exception_t65 ** p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t51 *)args[3], (DateTimeOffset_t363 *)args[4], *((int8_t*)args[5]), (Exception_t65 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Object_t_Object_t_SByte_t349_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t65 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t65 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320_SByte_t349_SByte_t349_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t_Object_t_SByte_t349_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Int32_t320_Object_t_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Int32_t320_Object_t_SByte_t349_Int32U26_t2139_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_SByte_t349_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t_SByte_t349_DateTimeU26_t2185_DateTimeOffsetU26_t2186_Object_t_Int32_t320_SByte_t349_BooleanU26_t2142_BooleanU26_t2142 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t51 * p5, DateTimeOffset_t363 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t51 *)args[4], (DateTimeOffset_t363 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t51_Object_t_Object_t_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t65;
void* RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t_Int32_t320_DateTimeU26_t2185_SByte_t349_BooleanU26_t2142_SByte_t349_ExceptionU26_t2153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t51 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t65 ** p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t51 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t65 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t51_DateTime_t51_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, DateTime_t51  p1, TimeSpan_t463  p2, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), *((TimeSpan_t463 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_DateTime_t51_DateTime_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t51  p1, DateTime_t51  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), *((DateTime_t51 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_DateTime_t51_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t51  p1, TimeSpan_t463  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t51 *)args[0]), *((TimeSpan_t463 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int64_t345_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t463  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t463 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_DateTimeOffset_t363 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t363  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t363 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_DateTimeOffset_t363 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t363  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t363 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTimeOffset_t363 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTimeOffset_t363  (*Func)(void* obj, const MethodInfo* method);
	DateTimeOffset_t363  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t350_Object_t_BooleanU26_t2142_BooleanU26_t2142 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t350_Object_t_BooleanU26_t2142_BooleanU26_t2142_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_DateTime_t51_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t51  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_DateTime_t51_Nullable_1_t1403_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t51  p1, Nullable_1_t1403  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), *((Nullable_1_t1403 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.MonoEnumInfo
#include "mscorlib_System_MonoEnumInfo.h"
void* RuntimeInvoker_Void_t765_MonoEnumInfo_t1317 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t1317  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t1317 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_MonoEnumInfoU26_t2187 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t1317 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t1317 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int16_t350_Int16_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Int64_t345_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
void* RuntimeInvoker_PlatformID_t1344 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int16_t350_Int16_t350_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

struct Object_t;
// System.Guid
#include "mscorlib_System_Guid.h"
void* RuntimeInvoker_Int32_t320_Guid_t364 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t364  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t364 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Guid_t364 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t364  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t364 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Guid_t364 (const MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t364  (*Func)(void* obj, const MethodInfo* method);
	Guid_t364  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t349_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Double_t344_Double_t344_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TypeAttributes_t1027_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_UInt64U2AU26_t2188_Int32U2AU26_t2189_CharU2AU26_t2190_CharU2AU26_t2190_Int64U2AU26_t2191_Int32U2AU26_t2189 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Double_t344_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t347  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t347 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t349_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t350_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t345_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Single_t321_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Double_t344_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Decimal_t347_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t347  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t347 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t321_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Double_t344_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_BooleanU26_t2142_SByte_t349_Int32U26_t2139_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t320_Int32_t320_Object_t_SByte_t349_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int64_t345_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t463_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t463  (*Func)(void* obj, TimeSpan_t463  p1, const MethodInfo* method);
	TimeSpan_t463  ret = ((Func)method->method)(obj, *((TimeSpan_t463 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_TimeSpan_t463_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t463  p1, TimeSpan_t463  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t463 *)args[0]), *((TimeSpan_t463 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t463  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t463 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t463  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t463 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t463_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t463  (*Func)(void* obj, double p1, const MethodInfo* method);
	TimeSpan_t463  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t463_Double_t344_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t463  (*Func)(void* obj, double p1, int64_t p2, const MethodInfo* method);
	TimeSpan_t463  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t463_TimeSpan_t463_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t463  (*Func)(void* obj, TimeSpan_t463  p1, TimeSpan_t463  p2, const MethodInfo* method);
	TimeSpan_t463  ret = ((Func)method->method)(obj, *((TimeSpan_t463 *)args[0]), *((TimeSpan_t463 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t463_DateTime_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t463  (*Func)(void* obj, DateTime_t51  p1, const MethodInfo* method);
	TimeSpan_t463  ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_DateTime_t51_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t51  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t51_DateTime_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t51  (*Func)(void* obj, DateTime_t51  p1, const MethodInfo* method);
	DateTime_t51  ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t463_DateTime_t51_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t463  (*Func)(void* obj, DateTime_t51  p1, TimeSpan_t463  p2, const MethodInfo* method);
	TimeSpan_t463  ret = ((Func)method->method)(obj, *((DateTime_t51 *)args[0]), *((TimeSpan_t463 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Int64U5BU5D_t1391;
struct StringU5BU5D_t197;
void* RuntimeInvoker_Boolean_t333_Int32_t320_Int64U5BU5DU26_t2192_StringU5BU5DU26_t2193 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t1391** p2, StringU5BU5D_t197** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t1391**)args[1], (StringU5BU5D_t197**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
void* RuntimeInvoker_Void_t765_KeyValuePair_2_t1493 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1493  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1493 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1493 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1493  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1493 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
void* RuntimeInvoker_Enumerator_t1651 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1651  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1651  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_ObjectU26_t2141 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

struct Object_t;
struct ObjectU5BU5D_t200;
void* RuntimeInvoker_Void_t765_ObjectU5BU5DU26_t2152_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t200** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t200**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct ObjectU5BU5D_t200;
void* RuntimeInvoker_Void_t765_ObjectU5BU5DU26_t2152_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t200** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t200**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1493_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1493  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t1493  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3.h"
void* RuntimeInvoker_Enumerator_t1497 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1497  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1497  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t560_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t560  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t560  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1493 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1493  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1493  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_4.h"
void* RuntimeInvoker_Enumerator_t1496 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1496  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1496  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_6.h"
void* RuntimeInvoker_Enumerator_t1500 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1500  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1500  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1.h"
void* RuntimeInvoker_Enumerator_t1450 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1450  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1450  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
void* RuntimeInvoker_GcAchievementData_t224_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t224  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcAchievementData_t224  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_GcAchievementData_t224 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t224  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t224 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_GcAchievementData_t224 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t224  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t224 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_GcAchievementData_t224 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t224  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t224 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_GcAchievementData_t224 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t224  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t224 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_GcScoreData_t225_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t225  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcScoreData_t225  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_GcScoreData_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t225  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t225 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_GcScoreData_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t225  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t225 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_GcScoreData_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t225  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t225 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3.h"
void* RuntimeInvoker_KeyValuePair_2_t1465_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1465  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1465  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_KeyValuePair_2_t1465 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1465  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1465 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1465 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1465  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1465 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_KeyValuePair_2_t1465 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1465  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1465 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1465 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1465  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1465 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_Link.h"
void* RuntimeInvoker_Link_t864_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t864  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t864  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Link_t864 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t864  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t864 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Link_t864 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t864  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t864 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Link_t864 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t864  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t864 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Link_t864 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t864  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t864 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t560_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t560  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DictionaryEntry_t560  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_DictionaryEntry_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t560  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t560 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_DictionaryEntry_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t560  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t560 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_DictionaryEntry_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t560  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t560 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_DictionaryEntry_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t560  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t560 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1493_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1493  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1493  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_KeyValuePair_2_t1493 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1493  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1493 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1493 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1493  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1493 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"
void* RuntimeInvoker_KeyValuePair_2_t1509_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1509  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1509  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_KeyValuePair_2_t1509 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1509  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1509 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1509 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1509  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1509 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_KeyValuePair_2_t1509 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1509  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1509 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1509 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1509  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1509 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
void* RuntimeInvoker_Keyframe_t135_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t135  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Keyframe_t135  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Keyframe_t135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t135  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t135 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Keyframe_t135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t135  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t135 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Keyframe_t135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t135  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t135 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Keyframe_t135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t135  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t135 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
void* RuntimeInvoker_UIVertex_t158_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t158  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIVertex_t158  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_UIVertex_t158 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t158  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t158 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_UIVertex_t158 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t158  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t158 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_UIVertex_t158 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t158  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t158 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_UIVertex_t158 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UIVertex_t158  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UIVertex_t158 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct UIVertexU5BU5D_t287;
void* RuntimeInvoker_Void_t765_UIVertexU5BU5DU26_t2194_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t287** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t287**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UIVertexU5BU5D_t287;
void* RuntimeInvoker_Void_t765_UIVertexU5BU5DU26_t2194_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t287** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t287**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_UIVertex_t158_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UIVertex_t158  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t158 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
void* RuntimeInvoker_UICharInfo_t149_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t149  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UICharInfo_t149  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_UICharInfo_t149 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfo_t149  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UICharInfo_t149 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_UICharInfo_t149 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t149  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t149 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_UICharInfo_t149 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t149  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t149 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_UICharInfo_t149 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UICharInfo_t149  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UICharInfo_t149 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct UICharInfoU5BU5D_t288;
void* RuntimeInvoker_Void_t765_UICharInfoU5BU5DU26_t2195_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t288** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t288**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UICharInfoU5BU5D_t288;
void* RuntimeInvoker_Void_t765_UICharInfoU5BU5DU26_t2195_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t288** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t288**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_UICharInfo_t149_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UICharInfo_t149  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UICharInfo_t149 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
void* RuntimeInvoker_UILineInfo_t150_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t150  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UILineInfo_t150  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_UILineInfo_t150 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfo_t150  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UILineInfo_t150 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_UILineInfo_t150 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t150  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t150 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_UILineInfo_t150 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t150  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t150 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_UILineInfo_t150 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UILineInfo_t150  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UILineInfo_t150 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct UILineInfoU5BU5D_t289;
void* RuntimeInvoker_Void_t765_UILineInfoU5BU5DU26_t2196_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t289** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t289**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UILineInfoU5BU5D_t289;
void* RuntimeInvoker_Void_t765_UILineInfoU5BU5DU26_t2196_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t289** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t289**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_UILineInfo_t150_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UILineInfo_t150  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UILineInfo_t150 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"
void* RuntimeInvoker_KeyValuePair_2_t1552_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1552  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1552  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_KeyValuePair_2_t1552 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1552  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1552 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1552 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1552  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1552 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_KeyValuePair_2_t1552 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1552  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1552 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1552 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1552  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1552 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"
void* RuntimeInvoker_KeyValuePair_2_t1581_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1581  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1581  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_KeyValuePair_2_t1581 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1581  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1581 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1581 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1581  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1581 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_KeyValuePair_2_t1581 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1581  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1581 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1581 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1581  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1581 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_NetworkID_t178_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_UInt64_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_UInt64_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_UInt64_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"
void* RuntimeInvoker_KeyValuePair_2_t1601_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1601  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1601  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_KeyValuePair_2_t1601 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1601  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1601 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1601 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1601  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1601 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_KeyValuePair_2_t1601 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1601  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1601 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1601 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1601  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1601 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
void* RuntimeInvoker_ParameterModifier_t1020_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1020  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParameterModifier_t1020  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_ParameterModifier_t1020 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t1020  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t1020 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_ParameterModifier_t1020 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t1020  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t1020 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_ParameterModifier_t1020 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t1020  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t1020 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_ParameterModifier_t1020 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t1020  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t1020 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_HitInfo_t239_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t239  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	HitInfo_t239  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_HitInfo_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t239  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t239 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_HitInfo_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t239  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t239 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
void* RuntimeInvoker_KeyValuePair_2_t1662_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1662  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1662  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_KeyValuePair_2_t1662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1662  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1662 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1662 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1662  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1662 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_KeyValuePair_2_t1662 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1662  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1662 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1662  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1662 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
void* RuntimeInvoker_TextEditOp_t252_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"
void* RuntimeInvoker_KeyValuePair_2_t1692_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1692  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1692  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_KeyValuePair_2_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1692  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1692 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1692  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1692 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_KeyValuePair_2_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1692  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1692 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1692  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1692 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
void* RuntimeInvoker_X509ChainStatus_t457_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t457  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	X509ChainStatus_t457  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_X509ChainStatus_t457 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t457  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t457 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_X509ChainStatus_t457 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t457  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t457 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_X509ChainStatus_t457 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t457  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t457 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_X509ChainStatus_t457 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t457  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t457 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"
void* RuntimeInvoker_KeyValuePair_2_t1716_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1716  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1716  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_KeyValuePair_2_t1716 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1716  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1716 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1716 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1716  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1716 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_KeyValuePair_2_t1716 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1716  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1716 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1716 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1716  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1716 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"
void* RuntimeInvoker_Mark_t511_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t511  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Mark_t511  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Mark_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t511  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t511 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Mark_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t511  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t511 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Mark_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t511  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t511 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Mark_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t511  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t511 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"
void* RuntimeInvoker_UriScheme_t548_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t548  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UriScheme_t548  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_UriScheme_t548 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t548  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t548 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_UriScheme_t548 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t548  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t548 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_UriScheme_t548 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t548  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t548 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_UriScheme_t548 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t548  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t548 *)args[1]), method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"
void* RuntimeInvoker_ClientCertificateType_t713_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Double_t344 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

struct Object_t;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
void* RuntimeInvoker_TableRange_t797_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t797  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TableRange_t797  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_TableRange_t797 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t797  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t797 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_TableRange_t797 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t797  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t797 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_TableRange_t797 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t797  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t797 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_TableRange_t797 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t797  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t797 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_Slot.h"
void* RuntimeInvoker_Slot_t874_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t874  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t874  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Slot_t874 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t874  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t874 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Slot_t874 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t874  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t874 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Slot_t874 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t874  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t874 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Slot_t874 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t874  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t874 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"
void* RuntimeInvoker_Slot_t881_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t881  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t881  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Slot_t881 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t881  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t881 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Slot_t881 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t881  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t881 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Slot_t881 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t881  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t881 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Slot_t881 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t881  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t881 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.ILTokenInfo
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
void* RuntimeInvoker_ILTokenInfo_t959_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t959  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ILTokenInfo_t959  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_ILTokenInfo_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ILTokenInfo_t959  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ILTokenInfo_t959 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_ILTokenInfo_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ILTokenInfo_t959  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ILTokenInfo_t959 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_ILTokenInfo_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ILTokenInfo_t959  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ILTokenInfo_t959 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_ILTokenInfo_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ILTokenInfo_t959  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ILTokenInfo_t959 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.ILGenerator/LabelData
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
void* RuntimeInvoker_LabelData_t961_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t961  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelData_t961  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_LabelData_t961 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelData_t961  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelData_t961 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_LabelData_t961 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelData_t961  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelData_t961 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_LabelData_t961 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelData_t961  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelData_t961 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_LabelData_t961 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelData_t961  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelData_t961 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.ILGenerator/LabelFixup
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
void* RuntimeInvoker_LabelFixup_t960_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t960  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelFixup_t960  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_LabelFixup_t960 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelFixup_t960  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelFixup_t960 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_LabelFixup_t960 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelFixup_t960  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelFixup_t960 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_LabelFixup_t960 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelFixup_t960  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelFixup_t960 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_LabelFixup_t960 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelFixup_t960  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelFixup_t960 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_DateTime_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t51  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t51 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t347  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t347 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Decimal_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t347  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t347 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t463_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t463  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TimeSpan_t463  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_TimeSpan_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t463  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t463 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
void* RuntimeInvoker_TypeTag_t1145_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Byte_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Byte_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t765_Int32_t320_Byte_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_GcAchievementData_t224 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t224  (*Func)(void* obj, const MethodInfo* method);
	GcAchievementData_t224  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_GcScoreData_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t225  (*Func)(void* obj, const MethodInfo* method);
	GcScoreData_t225  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1465_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1465  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t1465  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t320_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32_t320_ObjectU26_t2141 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
void* RuntimeInvoker_Enumerator_t1471 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1471  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1471  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t560_Int32_t320_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t560  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t560  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1465 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1465  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1465  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Link_t864 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t864  (*Func)(void* obj, const MethodInfo* method);
	Link_t864  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_1.h"
void* RuntimeInvoker_Enumerator_t1470 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1470  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1470  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_3.h"
void* RuntimeInvoker_Enumerator_t1474 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1474  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1474  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t560_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t560  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DictionaryEntry_t560  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1465_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1465  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1465  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1493_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1493  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1493  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1509_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1509  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t1509  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5.h"
void* RuntimeInvoker_Enumerator_t1513 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1513  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1513  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t560_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t560  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t560  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1509 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1509  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1509  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_7.h"
void* RuntimeInvoker_Enumerator_t1512 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1512  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1512  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_8.h"
void* RuntimeInvoker_Enumerator_t1516 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1516  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1516  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1509_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1509  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1509  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Keyframe_t135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t135  (*Func)(void* obj, const MethodInfo* method);
	Keyframe_t135  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIVertex_t158 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t158  (*Func)(void* obj, const MethodInfo* method);
	UIVertex_t158  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"
void* RuntimeInvoker_Enumerator_t1538 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1538  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1538  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_UIVertex_t158_UIVertex_t158 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t158  p1, UIVertex_t158  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t158 *)args[0]), *((UIVertex_t158 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UICharInfo_t149 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t149  (*Func)(void* obj, const MethodInfo* method);
	UICharInfo_t149  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"
void* RuntimeInvoker_Enumerator_t1542 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1542  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1542  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_UICharInfo_t149_UICharInfo_t149 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t149  p1, UICharInfo_t149  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t149 *)args[0]), *((UICharInfo_t149 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UILineInfo_t150 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t150  (*Func)(void* obj, const MethodInfo* method);
	UILineInfo_t150  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"
void* RuntimeInvoker_Enumerator_t1546 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1546  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1546  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_UILineInfo_t150_UILineInfo_t150 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t150  p1, UILineInfo_t150  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t150 *)args[0]), *((UILineInfo_t150 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1552_Object_t_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1552  (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	KeyValuePair_2_t1552  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t345_Object_t_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7.h"
void* RuntimeInvoker_Enumerator_t1557 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1557  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1557  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t560_Object_t_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t560  (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	DictionaryEntry_t560  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1552 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1552  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1552  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_11.h"
void* RuntimeInvoker_Enumerator_t1556 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1556  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1556  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t345_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_12.h"
void* RuntimeInvoker_Enumerator_t1560 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1560  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1560  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1552_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1552  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1552  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int64_t345_Int64_t345 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_UInt64_t348_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1581_UInt64_t348_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1581  (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t1581  ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_NetworkID_t178_UInt64_t348_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_UInt64_t348_ObjectU26_t2141 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint64_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_NetworkID_t178_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9.h"
void* RuntimeInvoker_Enumerator_t1586 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1586  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1586  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t560_UInt64_t348_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t560  (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t560  ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1581 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1581  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1581  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_14.h"
void* RuntimeInvoker_Enumerator_t1585 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1585  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1585  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t348_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_15.h"
void* RuntimeInvoker_Enumerator_t1589 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1589  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1589  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1581_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1581  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1581  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_UInt64_t348_UInt64_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint64_t p1, uint64_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), *((uint64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1601 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1601  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1601  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t765_Object_t_KeyValuePair_2_t1493 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1493  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t1493 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1601_Object_t_KeyValuePair_2_t1493 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1601  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1493  p2, const MethodInfo* method);
	KeyValuePair_2_t1601  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t1493 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t1493 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1493  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t1493 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1493_Object_t_KeyValuePair_2_t1493 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1493  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1493  p2, const MethodInfo* method);
	KeyValuePair_2_t1493  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t1493 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_KeyValuePair_2U26_t2197 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1493 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (KeyValuePair_2_t1493 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__12.h"
void* RuntimeInvoker_Enumerator_t1630 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1630  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1630  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t560_Object_t_KeyValuePair_2_t1493 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t560  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1493  p2, const MethodInfo* method);
	DictionaryEntry_t560  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t1493 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_18.h"
void* RuntimeInvoker_Enumerator_t1629 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1629  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1629  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t1493_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1493  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t1493 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_19.h"
void* RuntimeInvoker_Enumerator_t1633 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1633  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1633  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1601_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1601  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1601  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1493_KeyValuePair_2_t1493 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1493  p1, KeyValuePair_2_t1493  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1493 *)args[0]), *((KeyValuePair_2_t1493 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ParameterModifier_t1020 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1020  (*Func)(void* obj, const MethodInfo* method);
	ParameterModifier_t1020  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_HitInfo_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t239  (*Func)(void* obj, const MethodInfo* method);
	HitInfo_t239  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TextEditOp_t252_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1662_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1662  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t1662  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TextEditOp_t252_Object_t_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_TextEditOpU26_t2198 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
void* RuntimeInvoker_Enumerator_t1667 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1667  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1667  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1662 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1662  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1662  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TextEditOp_t252 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_21.h"
void* RuntimeInvoker_Enumerator_t1666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1666  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1666  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_22.h"
void* RuntimeInvoker_Enumerator_t1670 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1670  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1670  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1662_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1662  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1662  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1692_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1692  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t1692  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t333_Object_t_BooleanU26_t2142 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
void* RuntimeInvoker_Enumerator_t1697 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1697  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1697  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t560_Object_t_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t560  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t560  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1692  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1692  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_24.h"
void* RuntimeInvoker_Enumerator_t1696 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1696  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1696  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t349_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_25.h"
void* RuntimeInvoker_Enumerator_t1700 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1700  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1700  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1692_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1692  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1692  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_SByte_t349_SByte_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509ChainStatus_t457 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t457  (*Func)(void* obj, const MethodInfo* method);
	X509ChainStatus_t457  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1716_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1716  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t1716  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Int32_t320_Int32U26_t2139 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__18.h"
void* RuntimeInvoker_Enumerator_t1720 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1720  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1720  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t560_Int32_t320_Int32_t320 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t560  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t560  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1716 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1716  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1716  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_27.h"
void* RuntimeInvoker_Enumerator_t1719 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1719  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1719  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28.h"
void* RuntimeInvoker_Enumerator_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1723  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1723  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t1716_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1716  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1716  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Mark_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t511  (*Func)(void* obj, const MethodInfo* method);
	Mark_t511  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UriScheme_t548 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t548  (*Func)(void* obj, const MethodInfo* method);
	UriScheme_t548  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ClientCertificateType_t713 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TableRange_t797 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t797  (*Func)(void* obj, const MethodInfo* method);
	TableRange_t797  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Slot_t874 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t874  (*Func)(void* obj, const MethodInfo* method);
	Slot_t874  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Slot_t881 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t881  (*Func)(void* obj, const MethodInfo* method);
	Slot_t881  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ILTokenInfo_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t959  (*Func)(void* obj, const MethodInfo* method);
	ILTokenInfo_t959  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LabelData_t961 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t961  (*Func)(void* obj, const MethodInfo* method);
	LabelData_t961  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LabelFixup_t960 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t960  (*Func)(void* obj, const MethodInfo* method);
	LabelFixup_t960  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TypeTag_t1145 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_DateTimeOffset_t363_DateTimeOffset_t363 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t363  p1, DateTimeOffset_t363  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t363 *)args[0]), *((DateTimeOffset_t363 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_DateTimeOffset_t363_DateTimeOffset_t363 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t363  p1, DateTimeOffset_t363  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t363 *)args[0]), *((DateTimeOffset_t363 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Nullable_1_t1403 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t1403  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t1403 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t320_Guid_t364_Guid_t364 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t364  p1, Guid_t364  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t364 *)args[0]), *((Guid_t364 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t333_Guid_t364_Guid_t364 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t364  p1, Guid_t364  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t364 *)args[0]), *((Guid_t364 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

extern const InvokerMethod g_Il2CppInvokerPointers[1204] = 
{
	RuntimeInvoker_Void_t765,
	RuntimeInvoker_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320,
	RuntimeInvoker_Object_t_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t,
	RuntimeInvoker_Int32_t320_LayerMask_t7,
	RuntimeInvoker_LayerMask_t7_Int32_t320,
	RuntimeInvoker_Void_t765_Single_t321,
	RuntimeInvoker_Void_t765_Object_t,
	RuntimeInvoker_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Object_t_Object_t,
	RuntimeInvoker_Boolean_t333,
	RuntimeInvoker_Void_t765_Object_t_Double_t344,
	RuntimeInvoker_Void_t765_Int64_t345_Object_t,
	RuntimeInvoker_Void_t765_SByte_t349,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320,
	RuntimeInvoker_Void_t765_GcAchievementDescriptionData_t223_Int32_t320,
	RuntimeInvoker_Void_t765_GcUserProfileData_t222_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Double_t344_Object_t,
	RuntimeInvoker_Void_t765_Int64_t345_Object_t_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t,
	RuntimeInvoker_Void_t765_UserProfileU5BU5DU26_t2128_Object_t_Int32_t320,
	RuntimeInvoker_Void_t765_UserProfileU5BU5DU26_t2128_Int32_t320,
	RuntimeInvoker_Void_t765_GcScoreData_t225,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Single_t321,
	RuntimeInvoker_Boolean_t333_BoneWeight_t28_BoneWeight_t28,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Int32_t320_SByte_t349_SByte_t349_IntPtr_t,
	RuntimeInvoker_Object_t_Vector3_t81,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t2129,
	RuntimeInvoker_Void_t765_Color_t38_Single_t321,
	RuntimeInvoker_Void_t765_Single_t321_Single_t321,
	RuntimeInvoker_Void_t765_DateTime_t51,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Object_t_Int32_t320_Single_t321_Single_t321_Object_t,
	RuntimeInvoker_Void_t765_Object_t_IntPtr_t,
	RuntimeInvoker_Object_t_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Object_t_Single_t321,
	RuntimeInvoker_Object_t_Int32_t320_SByte_t349,
	RuntimeInvoker_Void_t765_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Rect_t42_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Rect_t42,
	RuntimeInvoker_Void_t765_Int32_t320_RectU26_t2130,
	RuntimeInvoker_Void_t765_Single_t321_Single_t321_Single_t321_Single_t321_Object_t,
	RuntimeInvoker_Void_t765_Int32_t320_Object_t,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Color_t38,
	RuntimeInvoker_Void_t765_ColorU26_t2131,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_IntPtr_t_Int32_t320,
	RuntimeInvoker_EventType_t79,
	RuntimeInvoker_Vector2_t43,
	RuntimeInvoker_Void_t765_Vector2U26_t2132,
	RuntimeInvoker_EventModifiers_t80,
	RuntimeInvoker_Char_t576,
	RuntimeInvoker_KeyCode_t78,
	RuntimeInvoker_Void_t765_IntPtr_t,
	RuntimeInvoker_Single_t321_Vector2_t43,
	RuntimeInvoker_Vector2_t43_Vector2_t43_Vector2_t43,
	RuntimeInvoker_Boolean_t333_Vector2_t43_Vector2_t43,
	RuntimeInvoker_Void_t765_Single_t321_Single_t321_Single_t321,
	RuntimeInvoker_Single_t321_Vector3_t81,
	RuntimeInvoker_Vector3_t81_Vector3_t81_Vector3_t81,
	RuntimeInvoker_Vector3_t81,
	RuntimeInvoker_Vector3_t81_Vector3_t81_Single_t321,
	RuntimeInvoker_Boolean_t333_Vector3_t81_Vector3_t81,
	RuntimeInvoker_Void_t765_Single_t321_Single_t321_Single_t321_Single_t321,
	RuntimeInvoker_Color_t38,
	RuntimeInvoker_Color_t38_Color_t38_Single_t321,
	RuntimeInvoker_Vector4_t86_Color_t38,
	RuntimeInvoker_Void_t765_SByte_t349_SByte_t349_SByte_t349_SByte_t349,
	RuntimeInvoker_Color32_t82_Color_t38,
	RuntimeInvoker_Boolean_t333_Vector3_t81,
	RuntimeInvoker_Single_t321_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Single_t321,
	RuntimeInvoker_Single_t321_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Single_t321,
	RuntimeInvoker_Matrix4x4_t84_Matrix4x4_t84,
	RuntimeInvoker_Matrix4x4_t84_Matrix4x4U26_t2133,
	RuntimeInvoker_Boolean_t333_Matrix4x4_t84_Matrix4x4U26_t2133,
	RuntimeInvoker_Boolean_t333_Matrix4x4U26_t2133_Matrix4x4U26_t2133,
	RuntimeInvoker_Matrix4x4_t84,
	RuntimeInvoker_Vector4_t86_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Vector4_t86,
	RuntimeInvoker_Vector3_t81_Vector3_t81,
	RuntimeInvoker_Matrix4x4_t84_Vector3_t81,
	RuntimeInvoker_Void_t765_Vector3_t81_Quaternion_t83_Vector3_t81,
	RuntimeInvoker_Matrix4x4_t84_Vector3_t81_Quaternion_t83_Vector3_t81,
	RuntimeInvoker_Matrix4x4_t84_Vector3U26_t2129_QuaternionU26_t2134_Vector3U26_t2129,
	RuntimeInvoker_Matrix4x4_t84_Single_t321_Single_t321_Single_t321_Single_t321_Single_t321_Single_t321,
	RuntimeInvoker_Matrix4x4_t84_Single_t321_Single_t321_Single_t321_Single_t321,
	RuntimeInvoker_Matrix4x4_t84_Matrix4x4_t84_Matrix4x4_t84,
	RuntimeInvoker_Vector4_t86_Matrix4x4_t84_Vector4_t86,
	RuntimeInvoker_Boolean_t333_Matrix4x4_t84_Matrix4x4_t84,
	RuntimeInvoker_Void_t765_Vector3_t81_Vector3_t81,
	RuntimeInvoker_Void_t765_Vector3_t81,
	RuntimeInvoker_Void_t765_Bounds_t85,
	RuntimeInvoker_Boolean_t333_Bounds_t85,
	RuntimeInvoker_Boolean_t333_Bounds_t85_Vector3_t81,
	RuntimeInvoker_Boolean_t333_BoundsU26_t2135_Vector3U26_t2129,
	RuntimeInvoker_Single_t321_Bounds_t85_Vector3_t81,
	RuntimeInvoker_Single_t321_BoundsU26_t2135_Vector3U26_t2129,
	RuntimeInvoker_Boolean_t333_RayU26_t2136_BoundsU26_t2135_SingleU26_t2137,
	RuntimeInvoker_Boolean_t333_Ray_t87,
	RuntimeInvoker_Boolean_t333_Ray_t87_SingleU26_t2137,
	RuntimeInvoker_Vector3_t81_BoundsU26_t2135_Vector3U26_t2129,
	RuntimeInvoker_Boolean_t333_Bounds_t85_Bounds_t85,
	RuntimeInvoker_Single_t321_Vector4_t86_Vector4_t86,
	RuntimeInvoker_Single_t321_Vector4_t86,
	RuntimeInvoker_Vector4_t86_Vector4_t86_Vector4_t86,
	RuntimeInvoker_Boolean_t333_Vector4_t86_Vector4_t86,
	RuntimeInvoker_Single_t321_Single_t321,
	RuntimeInvoker_Single_t321_Single_t321_Single_t321,
	RuntimeInvoker_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Single_t321_Single_t321_Single_t321_Single_t321,
	RuntimeInvoker_Boolean_t333_Single_t321_Single_t321,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t765_SphericalHarmonicsL2U26_t2138,
	RuntimeInvoker_Void_t765_Color_t38_SphericalHarmonicsL2U26_t2138,
	RuntimeInvoker_Void_t765_ColorU26_t2131_SphericalHarmonicsL2U26_t2138,
	RuntimeInvoker_Void_t765_Vector3_t81_Color_t38_Single_t321,
	RuntimeInvoker_Void_t765_Vector3_t81_Color_t38_SphericalHarmonicsL2U26_t2138,
	RuntimeInvoker_Void_t765_Vector3U26_t2129_ColorU26_t2131_SphericalHarmonicsL2U26_t2138,
	RuntimeInvoker_SphericalHarmonicsL2_t98_SphericalHarmonicsL2_t98_Single_t321,
	RuntimeInvoker_SphericalHarmonicsL2_t98_Single_t321_SphericalHarmonicsL2_t98,
	RuntimeInvoker_SphericalHarmonicsL2_t98_SphericalHarmonicsL2_t98_SphericalHarmonicsL2_t98,
	RuntimeInvoker_Boolean_t333_SphericalHarmonicsL2_t98_SphericalHarmonicsL2_t98,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_SByte_t349_Object_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t349_SByte_t349_Object_t_SByte_t349,
	RuntimeInvoker_Boolean_t333_Object_t_SByte_t349,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Int32_t320_SByte_t349,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Rect_t42,
	RuntimeInvoker_Void_t765_RectU26_t2130,
	RuntimeInvoker_CameraClearFlags_t228,
	RuntimeInvoker_Ray_t87_Vector3_t81,
	RuntimeInvoker_Ray_t87_Object_t_Vector3U26_t2129,
	RuntimeInvoker_Object_t_Ray_t87_Single_t321_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_RayU26_t2136_Single_t321_Int32_t320,
	RuntimeInvoker_RenderBuffer_t227,
	RuntimeInvoker_Void_t765_IntPtr_t_Int32U26_t2139_Int32U26_t2139,
	RuntimeInvoker_Void_t765_IntPtr_t_RenderBufferU26_t2140_RenderBufferU26_t2140,
	RuntimeInvoker_Void_t765_IntPtr_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_IntPtr_t_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_IntPtr_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Int32_t320_Int32_t320_Int32U26_t2139_Int32U26_t2139,
	RuntimeInvoker_Boolean_t333_Int32_t320,
	RuntimeInvoker_Void_t765_Vector3U26_t2129,
	RuntimeInvoker_IntPtr_t,
	RuntimeInvoker_Object_t_SByte_t349_Object_t_Object_t,
	RuntimeInvoker_SendMessageOptions_t6,
	RuntimeInvoker_AnimatorStateInfo_t133,
	RuntimeInvoker_AnimatorClipInfo_t134,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Color_t38_Int32_t320_Single_t321_Single_t321_Int32_t320_SByte_t349_SByte_t349_Int32_t320_Int32_t320_Int32_t320_Int32_t320_SByte_t349_Int32_t320_Vector2_t43_Vector2_t43_SByte_t349,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Color_t38_Int32_t320_Single_t321_Single_t321_Int32_t320_SByte_t349_SByte_t349_Int32_t320_Int32_t320_Int32_t320_Int32_t320_SByte_t349_Int32_t320_Single_t321_Single_t321_Single_t321_Single_t321_SByte_t349,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t_ColorU26_t2131_Int32_t320_Single_t321_Single_t321_Int32_t320_SByte_t349_SByte_t349_Int32_t320_Int32_t320_Int32_t320_Int32_t320_SByte_t349_Int32_t320_Single_t321_Single_t321_Single_t321_Single_t321_SByte_t349,
	RuntimeInvoker_TextGenerationSettings_t155_TextGenerationSettings_t155,
	RuntimeInvoker_Single_t321_Object_t_TextGenerationSettings_t155,
	RuntimeInvoker_Boolean_t333_Object_t_TextGenerationSettings_t155,
	RuntimeInvoker_SourceID_t177,
	RuntimeInvoker_AppID_t176,
	RuntimeInvoker_Int32_t320_Object_t_Object_t_Object_t,
	RuntimeInvoker_UInt16_t351_Object_t_Object_t_Object_t,
	RuntimeInvoker_UInt64_t348_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t,
	RuntimeInvoker_UInt32_t336,
	RuntimeInvoker_NetworkID_t178,
	RuntimeInvoker_Void_t765_UInt64_t348,
	RuntimeInvoker_NodeID_t179,
	RuntimeInvoker_Void_t765_UInt16_t351,
	RuntimeInvoker_Object_t_UInt64_t348,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_SByte_t349_Object_t_Object_t,
	RuntimeInvoker_Object_t_UInt64_t348_Object_t_Object_t,
	RuntimeInvoker_Object_t_UInt64_t348_Object_t,
	RuntimeInvoker_Object_t_UInt64_t348_UInt16_t351_Object_t,
	RuntimeInvoker_Object_t_Int32_t320_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_ObjectU26_t2141,
	RuntimeInvoker_Void_t765_KeyValuePair_2_t298,
	RuntimeInvoker_Boolean_t333_KeyValuePair_2_t298,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t2139_BooleanU26_t2142,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int32U26_t2139,
	RuntimeInvoker_Int32_t320_Object_t_Int32U26_t2139,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Object_t_Object_t_SByte_t349_Int32_t320_Object_t,
	RuntimeInvoker_UserState_t243,
	RuntimeInvoker_Void_t765_Object_t_Double_t344_SByte_t349_SByte_t349_DateTime_t51,
	RuntimeInvoker_Double_t344,
	RuntimeInvoker_Void_t765_Double_t344,
	RuntimeInvoker_DateTime_t51,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t349_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int64_t345,
	RuntimeInvoker_Void_t765_Object_t_Int64_t345_Object_t_DateTime_t51_Object_t_Int32_t320,
	RuntimeInvoker_Int64_t345,
	RuntimeInvoker_Void_t765_Int64_t345,
	RuntimeInvoker_UserScope_t244,
	RuntimeInvoker_Range_t238,
	RuntimeInvoker_Void_t765_Range_t238,
	RuntimeInvoker_TimeScope_t245,
	RuntimeInvoker_Void_t765_Int32_t320_HitInfo_t239,
	RuntimeInvoker_Boolean_t333_HitInfo_t239_HitInfo_t239,
	RuntimeInvoker_Boolean_t333_HitInfo_t239,
	RuntimeInvoker_Void_t765_Object_t_StringU26_t2143_StringU26_t2143,
	RuntimeInvoker_Object_t_Object_t_SByte_t349,
	RuntimeInvoker_Void_t765_Object_t_StreamingContext_t311,
	RuntimeInvoker_Void_t765_Object_t_AnimatorStateInfo_t133_Int32_t320,
	RuntimeInvoker_Boolean_t333_Color_t38_Color_t38,
	RuntimeInvoker_Boolean_t333_TextGenerationSettings_t155,
	RuntimeInvoker_Void_t765_Int32_t320_SByte_t349,
	RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t2144,
	RuntimeInvoker_DictionaryEntry_t560,
	RuntimeInvoker_EditorBrowsableState_t401,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t_Int32_t320,
	RuntimeInvoker_Int16_t350_Int16_t350,
	RuntimeInvoker_Boolean_t333_Object_t_IPAddressU26_t2145,
	RuntimeInvoker_AddressFamily_t406,
	RuntimeInvoker_Object_t_Int64_t345,
	RuntimeInvoker_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_Int32U26_t2139,
	RuntimeInvoker_Boolean_t333_Object_t_IPv6AddressU26_t2146,
	RuntimeInvoker_UInt16_t351_Int16_t350,
	RuntimeInvoker_Object_t_SByte_t349,
	RuntimeInvoker_SecurityProtocolType_t426,
	RuntimeInvoker_Void_t765_Object_t_SByte_t349,
	RuntimeInvoker_Void_t765_Object_t_SByte_t349_Object_t_Object_t,
	RuntimeInvoker_Void_t765_SByte_t349_SByte_t349_Int32_t320_SByte_t349,
	RuntimeInvoker_AsnDecodeStatus_t477_Object_t,
	RuntimeInvoker_Object_t_Int32_t320_Object_t_SByte_t349,
	RuntimeInvoker_X509ChainStatusFlags_t464_Object_t,
	RuntimeInvoker_X509ChainStatusFlags_t464_Object_t_Int32_t320_SByte_t349,
	RuntimeInvoker_X509ChainStatusFlags_t464_Object_t_Object_t_SByte_t349,
	RuntimeInvoker_X509ChainStatusFlags_t464,
	RuntimeInvoker_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int32U26_t2139_Int32_t320_Int32_t320,
	RuntimeInvoker_X509RevocationFlag_t471,
	RuntimeInvoker_X509RevocationMode_t472,
	RuntimeInvoker_X509VerificationFlags_t476,
	RuntimeInvoker_Void_t765_Object_t_Object_t_SByte_t349,
	RuntimeInvoker_X509KeyUsageFlags_t469,
	RuntimeInvoker_X509KeyUsageFlags_t469_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_SByte_t349,
	RuntimeInvoker_Byte_t326_Int16_t350,
	RuntimeInvoker_Byte_t326_Int16_t350_Int16_t350,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t320_Int32_t320_SByte_t349,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_Int32_t320,
	RuntimeInvoker_RegexOptions_t496,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320,
	RuntimeInvoker_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Category_t503_Object_t,
	RuntimeInvoker_Boolean_t333_UInt16_t351_Int16_t350,
	RuntimeInvoker_Boolean_t333_Int32_t320_Int16_t350,
	RuntimeInvoker_Void_t765_Int16_t350_SByte_t349_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_UInt16_t351_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_Int16_t350_Int16_t350_SByte_t349_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_Int16_t350_Object_t_SByte_t349_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_Object_t_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_SByte_t349_Object_t,
	RuntimeInvoker_Void_t765_Int32_t320_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_SByte_t349_Int32_t320_Object_t,
	RuntimeInvoker_UInt16_t351_UInt16_t351_UInt16_t351,
	RuntimeInvoker_OpFlags_t498_SByte_t349_SByte_t349_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_UInt16_t351_UInt16_t351,
	RuntimeInvoker_Void_t765_Int16_t350,
	RuntimeInvoker_Boolean_t333_Int32_t320_Int32U26_t2139_Int32_t320,
	RuntimeInvoker_Boolean_t333_Int32_t320_Int32U26_t2139_Int32U26_t2139_SByte_t349,
	RuntimeInvoker_Boolean_t333_Int32U26_t2139_Int32_t320,
	RuntimeInvoker_Boolean_t333_UInt16_t351_Int32_t320,
	RuntimeInvoker_Boolean_t333_Int16_t350,
	RuntimeInvoker_Boolean_t333_Int32_t320_Int32_t320_SByte_t349_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Int32U26_t2139_Int32U26_t2139,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_SByte_t349_Int32_t320,
	RuntimeInvoker_Interval_t518,
	RuntimeInvoker_Boolean_t333_Interval_t518,
	RuntimeInvoker_Void_t765_Interval_t518,
	RuntimeInvoker_Interval_t518_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Double_t344_Interval_t518,
	RuntimeInvoker_Object_t_Interval_t518_Object_t_Object_t,
	RuntimeInvoker_Double_t344_Object_t,
	RuntimeInvoker_Int32_t320_Object_t_Int32U26_t2139_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_Int32U26_t2139_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t2139,
	RuntimeInvoker_Object_t_RegexOptionsU26_t2147,
	RuntimeInvoker_Void_t765_RegexOptionsU26_t2147_SByte_t349,
	RuntimeInvoker_Boolean_t333_Int32U26_t2139_Int32U26_t2139_Int32_t320,
	RuntimeInvoker_Category_t503,
	RuntimeInvoker_Int32_t320_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Int16_t350_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Int16_t350,
	RuntimeInvoker_Char_t576_Int16_t350,
	RuntimeInvoker_Int32_t320_Int32U26_t2139,
	RuntimeInvoker_Void_t765_Int32U26_t2139_Int32U26_t2139,
	RuntimeInvoker_Void_t765_Int32U26_t2139_Int32U26_t2139_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_SByte_t349,
	RuntimeInvoker_Void_t765_Object_t_Object_t_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_UInt16_t351_SByte_t349,
	RuntimeInvoker_Void_t765_Int16_t350_Int16_t350,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Object_t_SByte_t349,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_UInt16_t351,
	RuntimeInvoker_Position_t499,
	RuntimeInvoker_UriHostNameType_t551_Object_t,
	RuntimeInvoker_Object_t_Int16_t350,
	RuntimeInvoker_Void_t765_StringU26_t2143,
	RuntimeInvoker_Object_t_Object_t_SByte_t349_SByte_t349_SByte_t349,
	RuntimeInvoker_Char_t576_Object_t_Int32U26_t2139_CharU26_t2148,
	RuntimeInvoker_Void_t765_Object_t_UriFormatExceptionU26_t2149,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Object_t_SByte_t349_Object_t,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_UInt32_t336_Int32_t320,
	RuntimeInvoker_UInt32_t336_Object_t_Int32_t320,
	RuntimeInvoker_Sign_t624_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_ConfidenceFactor_t628,
	RuntimeInvoker_Void_t765_SByte_t349_Object_t,
	RuntimeInvoker_Byte_t326,
	RuntimeInvoker_Void_t765_Object_t_Int32U26_t2139_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int32U26_t2139_ByteU26_t2150_Int32U26_t2139_ByteU5BU5DU26_t2151,
	RuntimeInvoker_DateTime_t51_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t_Object_t_SByte_t349,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_DSAParameters_t586,
	RuntimeInvoker_RSAParameters_t584_SByte_t349,
	RuntimeInvoker_Void_t765_RSAParameters_t584,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_DSAParameters_t586_BooleanU26_t2142,
	RuntimeInvoker_Object_t_Object_t_SByte_t349_Object_t_SByte_t349,
	RuntimeInvoker_Boolean_t333_DateTime_t51,
	RuntimeInvoker_X509ChainStatusFlags_t653,
	RuntimeInvoker_Void_t765_Byte_t326,
	RuntimeInvoker_Void_t765_Byte_t326_Byte_t326,
	RuntimeInvoker_AlertLevel_t666,
	RuntimeInvoker_AlertDescription_t667,
	RuntimeInvoker_Object_t_Byte_t326,
	RuntimeInvoker_Void_t765_Int16_t350_Object_t_Int32_t320_Int32_t320_Int32_t320_SByte_t349_SByte_t349_SByte_t349_SByte_t349_Int16_t350_SByte_t349_SByte_t349,
	RuntimeInvoker_CipherAlgorithmType_t669,
	RuntimeInvoker_HashAlgorithmType_t687,
	RuntimeInvoker_ExchangeAlgorithmType_t685,
	RuntimeInvoker_CipherMode_t617,
	RuntimeInvoker_Int16_t350,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int16_t350,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int64_t345,
	RuntimeInvoker_Void_t765_Object_t_ByteU5BU5DU26_t2151_ByteU5BU5DU26_t2151,
	RuntimeInvoker_Object_t_Byte_t326_Object_t,
	RuntimeInvoker_Object_t_Int16_t350_Object_t_Int32_t320_Int32_t320_Int32_t320_SByte_t349_SByte_t349_SByte_t349_SByte_t349_Int16_t350_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_SecurityProtocolType_t701,
	RuntimeInvoker_SecurityCompressionType_t700,
	RuntimeInvoker_HandshakeType_t714,
	RuntimeInvoker_HandshakeState_t686,
	RuntimeInvoker_UInt64_t348,
	RuntimeInvoker_SecurityProtocolType_t701_Int16_t350,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t326_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t326_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Byte_t326_Object_t,
	RuntimeInvoker_Object_t_Byte_t326_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Object_t_SByte_t349_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Int64_t345_Int64_t345_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Int32_t320_Int32_t320_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_Byte_t326_Byte_t326_Object_t,
	RuntimeInvoker_RSAParameters_t584,
	RuntimeInvoker_Void_t765_Object_t_Byte_t326,
	RuntimeInvoker_Void_t765_Object_t_Byte_t326_Byte_t326,
	RuntimeInvoker_Void_t765_Object_t_Byte_t326_Object_t,
	RuntimeInvoker_ContentType_t680,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_ObjectU5BU5DU26_t2152,
	RuntimeInvoker_Int32_t320_Object_t_ObjectU5BU5DU26_t2152,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t349,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_SByte_t349,
	RuntimeInvoker_Byte_t326_Object_t,
	RuntimeInvoker_Char_t576_Object_t,
	RuntimeInvoker_Decimal_t347_Object_t,
	RuntimeInvoker_Int16_t350_Object_t,
	RuntimeInvoker_Int64_t345_Object_t,
	RuntimeInvoker_SByte_t349_Object_t,
	RuntimeInvoker_Single_t321_Object_t,
	RuntimeInvoker_UInt16_t351_Object_t,
	RuntimeInvoker_UInt32_t336_Object_t,
	RuntimeInvoker_UInt64_t348_Object_t,
	RuntimeInvoker_Boolean_t333_SByte_t349_Object_t_Int32_t320_ExceptionU26_t2153,
	RuntimeInvoker_Boolean_t333_Object_t_SByte_t349_Int32U26_t2139_ExceptionU26_t2153,
	RuntimeInvoker_Boolean_t333_Int32_t320_SByte_t349_ExceptionU26_t2153,
	RuntimeInvoker_Boolean_t333_Int32U26_t2139_Object_t_SByte_t349_SByte_t349_ExceptionU26_t2153,
	RuntimeInvoker_Void_t765_Int32U26_t2139_Object_t_Object_t_BooleanU26_t2142_BooleanU26_t2142,
	RuntimeInvoker_Void_t765_Int32U26_t2139_Object_t_Object_t_BooleanU26_t2142,
	RuntimeInvoker_Boolean_t333_Int32U26_t2139_Object_t_Int32U26_t2139_SByte_t349_ExceptionU26_t2153,
	RuntimeInvoker_Boolean_t333_Int32U26_t2139_Object_t_Object_t,
	RuntimeInvoker_Boolean_t333_Int16_t350_SByte_t349,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_SByte_t349_Int32U26_t2139_ExceptionU26_t2153,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_Int32U26_t2139,
	RuntimeInvoker_Int32_t320_Int64_t345,
	RuntimeInvoker_Boolean_t333_Int64_t345,
	RuntimeInvoker_Boolean_t333_Object_t_SByte_t349_Int64U26_t2154_ExceptionU26_t2153,
	RuntimeInvoker_Int64_t345_Object_t_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_SByte_t349_Int64U26_t2154_ExceptionU26_t2153,
	RuntimeInvoker_Int64_t345_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_Int64U26_t2154,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_Int64U26_t2154,
	RuntimeInvoker_Boolean_t333_Object_t_SByte_t349_UInt32U26_t2155_ExceptionU26_t2153,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_SByte_t349_UInt32U26_t2155_ExceptionU26_t2153,
	RuntimeInvoker_UInt32_t336_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_UInt32_t336_Object_t_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_UInt32U26_t2155,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_UInt32U26_t2155,
	RuntimeInvoker_UInt64_t348_Object_t_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_SByte_t349_UInt64U26_t2156_ExceptionU26_t2153,
	RuntimeInvoker_UInt64_t348_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_UInt64U26_t2156,
	RuntimeInvoker_Int32_t320_SByte_t349,
	RuntimeInvoker_Boolean_t333_SByte_t349,
	RuntimeInvoker_Byte_t326_Object_t_Object_t,
	RuntimeInvoker_Byte_t326_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_ByteU26_t2150,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_ByteU26_t2150,
	RuntimeInvoker_Boolean_t333_Object_t_SByte_t349_SByteU26_t2157_ExceptionU26_t2153,
	RuntimeInvoker_SByte_t349_Object_t_Object_t,
	RuntimeInvoker_SByte_t349_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_SByteU26_t2157,
	RuntimeInvoker_Boolean_t333_Object_t_SByte_t349_Int16U26_t2158_ExceptionU26_t2153,
	RuntimeInvoker_Int16_t350_Object_t_Object_t,
	RuntimeInvoker_Int16_t350_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_Int16U26_t2158,
	RuntimeInvoker_UInt16_t351_Object_t_Object_t,
	RuntimeInvoker_UInt16_t351_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_UInt16U26_t2159,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_UInt16U26_t2159,
	RuntimeInvoker_Void_t765_ByteU2AU26_t2160_ByteU2AU26_t2160_DoubleU2AU26_t2161_UInt16U2AU26_t2162_UInt16U2AU26_t2162_UInt16U2AU26_t2162_UInt16U2AU26_t2162,
	RuntimeInvoker_UnicodeCategory_t910_Int16_t350,
	RuntimeInvoker_Char_t576_Int16_t350_Object_t,
	RuntimeInvoker_Void_t765_Int16_t350_Int32_t320,
	RuntimeInvoker_Char_t576_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Object_t,
	RuntimeInvoker_Int32_t320_Object_t_Object_t_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_Object_t_SByte_t349_Object_t,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t_Int32_t320_Int32_t320_SByte_t349_Object_t,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Int16_t350_Int32_t320,
	RuntimeInvoker_Object_t_Int32_t320_Int16_t350,
	RuntimeInvoker_Object_t_Int16_t350_Int16_t350,
	RuntimeInvoker_Void_t765_Object_t_Int32U26_t2139_Int32U26_t2139_Int32U26_t2139_BooleanU26_t2142_StringU26_t2143,
	RuntimeInvoker_Void_t765_Int32_t320_Int16_t350,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_Int32_t320_Object_t,
	RuntimeInvoker_Object_t_Int16_t350_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Single_t321,
	RuntimeInvoker_Boolean_t333_Single_t321,
	RuntimeInvoker_Single_t321_Object_t_Object_t,
	RuntimeInvoker_Int32_t320_Double_t344,
	RuntimeInvoker_Boolean_t333_Double_t344,
	RuntimeInvoker_Double_t344_Object_t_Object_t,
	RuntimeInvoker_Double_t344_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_SByte_t349_DoubleU26_t2163_ExceptionU26_t2153,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Boolean_t333_Object_t_DoubleU26_t2163,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_DoubleU26_t2163,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Int32_t320_SByte_t349_SByte_t349,
	RuntimeInvoker_Object_t_Decimal_t347,
	RuntimeInvoker_Decimal_t347_Decimal_t347_Decimal_t347,
	RuntimeInvoker_UInt64_t348_Decimal_t347,
	RuntimeInvoker_Int64_t345_Decimal_t347,
	RuntimeInvoker_Boolean_t333_Decimal_t347_Decimal_t347,
	RuntimeInvoker_Decimal_t347_Decimal_t347,
	RuntimeInvoker_Int32_t320_Decimal_t347_Decimal_t347,
	RuntimeInvoker_Int32_t320_Decimal_t347,
	RuntimeInvoker_Boolean_t333_Decimal_t347,
	RuntimeInvoker_Decimal_t347_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t_Int32U26_t2139_BooleanU26_t2142_BooleanU26_t2142_Int32U26_t2139_SByte_t349,
	RuntimeInvoker_Decimal_t347_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_DecimalU26_t2164_SByte_t349,
	RuntimeInvoker_Int32_t320_DecimalU26_t2164_UInt64U26_t2156,
	RuntimeInvoker_Int32_t320_DecimalU26_t2164_Int64U26_t2154,
	RuntimeInvoker_Int32_t320_DecimalU26_t2164_DecimalU26_t2164,
	RuntimeInvoker_Int32_t320_DecimalU26_t2164_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_DecimalU26_t2164_Int32_t320,
	RuntimeInvoker_Double_t344_DecimalU26_t2164,
	RuntimeInvoker_Void_t765_DecimalU26_t2164_Int32_t320,
	RuntimeInvoker_Int32_t320_DecimalU26_t2164_DecimalU26_t2164_DecimalU26_t2164,
	RuntimeInvoker_Byte_t326_Decimal_t347,
	RuntimeInvoker_SByte_t349_Decimal_t347,
	RuntimeInvoker_Int16_t350_Decimal_t347,
	RuntimeInvoker_UInt16_t351_Decimal_t347,
	RuntimeInvoker_UInt32_t336_Decimal_t347,
	RuntimeInvoker_Decimal_t347_SByte_t349,
	RuntimeInvoker_Decimal_t347_Int16_t350,
	RuntimeInvoker_Decimal_t347_Int32_t320,
	RuntimeInvoker_Decimal_t347_Int64_t345,
	RuntimeInvoker_Decimal_t347_Single_t321,
	RuntimeInvoker_Decimal_t347_Double_t344,
	RuntimeInvoker_Single_t321_Decimal_t347,
	RuntimeInvoker_Double_t344_Decimal_t347,
	RuntimeInvoker_Boolean_t333_IntPtr_t_IntPtr_t,
	RuntimeInvoker_IntPtr_t_Int64_t345,
	RuntimeInvoker_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t320_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t,
	RuntimeInvoker_UInt64_t348_IntPtr_t,
	RuntimeInvoker_UInt32_t336_IntPtr_t,
	RuntimeInvoker_UIntPtr_t_Int64_t345,
	RuntimeInvoker_UIntPtr_t_Object_t,
	RuntimeInvoker_UIntPtr_t_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t2165,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t349,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t320_SByte_t349_SByte_t349,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t349_SByte_t349,
	RuntimeInvoker_TypeCode_t1355,
	RuntimeInvoker_Int32_t320_Object_t_Object_t_Object_t_SByte_t349,
	RuntimeInvoker_UInt64_t348_Object_t_Int32_t320,
	RuntimeInvoker_Object_t_Object_t_Int16_t350,
	RuntimeInvoker_Object_t_Object_t_Int64_t345,
	RuntimeInvoker_Int64_t345_Int32_t320,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Object_t_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Object_t_Int64_t345_Int64_t345,
	RuntimeInvoker_Object_t_Int64_t345_Int64_t345_Int64_t345,
	RuntimeInvoker_Void_t765_Object_t_Int64_t345_Int64_t345,
	RuntimeInvoker_Void_t765_Object_t_Int64_t345_Int64_t345_Int64_t345,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Object_t_Int64_t345_Object_t_Int64_t345_Int64_t345,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Int64_t345,
	RuntimeInvoker_Int32_t320_Object_t_Object_t_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Object_t,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Int32_t320_Int32_t320_Object_t,
	RuntimeInvoker_TypeAttributes_t1027,
	RuntimeInvoker_MemberTypes_t1006,
	RuntimeInvoker_RuntimeTypeHandle_t767,
	RuntimeInvoker_Object_t_Object_t_SByte_t349_SByte_t349,
	RuntimeInvoker_TypeCode_t1355_Object_t,
	RuntimeInvoker_Object_t_RuntimeTypeHandle_t767,
	RuntimeInvoker_RuntimeTypeHandle_t767_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t320_Object_t_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t320_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t765_SByte_t349_SByte_t349_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_RuntimeFieldHandle_t768,
	RuntimeInvoker_Void_t765_IntPtr_t_SByte_t349,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Int32_t320_SByte_t349,
	RuntimeInvoker_Void_t765_Object_t_Object_t_ContractionU5BU5DU26_t2166_Level2MapU5BU5DU26_t2167,
	RuntimeInvoker_Void_t765_Object_t_CodePointIndexerU26_t2168_ByteU2AU26_t2160_ByteU2AU26_t2160_CodePointIndexerU26_t2168_ByteU2AU26_t2160,
	RuntimeInvoker_Byte_t326_Int32_t320,
	RuntimeInvoker_Boolean_t333_Int32_t320_SByte_t349,
	RuntimeInvoker_Byte_t326_Int32_t320_Int32_t320,
	RuntimeInvoker_Boolean_t333_Int32_t320_Int32_t320,
	RuntimeInvoker_ExtenderType_t811_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Object_t_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320_BooleanU26_t2142_BooleanU26_t2142_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32_t320_BooleanU26_t2142_BooleanU26_t2142_SByte_t349_SByte_t349_ContextU26_t2169,
	RuntimeInvoker_Int32_t320_SByte_t349_SByte_t349,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Int32_t320,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Int32_t320_Int32_t320_SByte_t349_ContextU26_t2169,
	RuntimeInvoker_Int32_t320_Object_t_Object_t_Int32_t320_Int32_t320_BooleanU26_t2142,
	RuntimeInvoker_Int32_t320_Object_t_Object_t_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int16_t350_Int32_t320_SByte_t349_ContextU26_t2169,
	RuntimeInvoker_Int32_t320_Object_t_Object_t_Int32_t320_Int32_t320_Object_t_ContextU26_t2169,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320_Object_t_Int32_t320_SByte_t349_ContextU26_t2169,
	RuntimeInvoker_Boolean_t333_Object_t_Int32U26_t2139_Int32_t320_Int32_t320_Object_t_SByte_t349_ContextU26_t2169,
	RuntimeInvoker_Boolean_t333_Object_t_Int32U26_t2139_Int32_t320_Int32_t320_Object_t_SByte_t349_Int32_t320_ContractionU26_t2170_ContextU26_t2169,
	RuntimeInvoker_Boolean_t333_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_SByte_t349,
	RuntimeInvoker_Boolean_t333_Object_t_Int32U26_t2139_Int32_t320_Int32_t320_Int32_t320_Object_t_SByte_t349_ContextU26_t2169,
	RuntimeInvoker_Boolean_t333_Object_t_Int32U26_t2139_Int32_t320_Int32_t320_Int32_t320_Object_t_SByte_t349_Int32_t320_ContractionU26_t2170_ContextU26_t2169,
	RuntimeInvoker_Void_t765_Int32_t320_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t349,
	RuntimeInvoker_Void_t765_Int32_t320_Object_t_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Object_t_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Object_t_SByte_t349,
	RuntimeInvoker_Void_t765_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_SByte_t349_ByteU5BU5DU26_t2151_Int32U26_t2139,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_SByte_t349,
	RuntimeInvoker_ConfidenceFactor_t820,
	RuntimeInvoker_Sign_t822_Object_t_Object_t,
	RuntimeInvoker_DSAParameters_t586_SByte_t349,
	RuntimeInvoker_Void_t765_DSAParameters_t586,
	RuntimeInvoker_Int16_t350_Object_t_Int32_t320,
	RuntimeInvoker_Single_t321_Object_t_Int32_t320,
	RuntimeInvoker_Double_t344_Object_t_Int32_t320,
	RuntimeInvoker_Object_t_Int16_t350_SByte_t349,
	RuntimeInvoker_Void_t765_Int32_t320_Single_t321_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Object_t_Single_t321_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Int32_t320_Single_t321_Object_t,
	RuntimeInvoker_Boolean_t333_Int32_t320_SByte_t349_MethodBaseU26_t2171_Int32U26_t2139_Int32U26_t2139_StringU26_t2143_Int32U26_t2139_Int32U26_t2139,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_SByte_t349_SByte_t349,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_SByte_t349,
	RuntimeInvoker_Int32_t320_DateTime_t51,
	RuntimeInvoker_DayOfWeek_t1306_DateTime_t51,
	RuntimeInvoker_Int32_t320_Int32U26_t2139_Int32_t320_Int32_t320,
	RuntimeInvoker_DayOfWeek_t1306_Int32_t320,
	RuntimeInvoker_Void_t765_Int32U26_t2139_Int32U26_t2139_Int32U26_t2139_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_SByte_t349,
	RuntimeInvoker_Void_t765_DateTime_t51_DateTime_t51_TimeSpan_t463,
	RuntimeInvoker_TimeSpan_t463,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Object_t_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32U26_t2139,
	RuntimeInvoker_Decimal_t347,
	RuntimeInvoker_SByte_t349,
	RuntimeInvoker_UInt16_t351,
	RuntimeInvoker_Void_t765_IntPtr_t_Int32_t320_SByte_t349_Int32_t320_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320_SByte_t349_Int32_t320,
	RuntimeInvoker_Int32_t320_IntPtr_t_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_SByte_t349_SByte_t349,
	RuntimeInvoker_Boolean_t333_Object_t_MonoIOErrorU26_t2172,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t320_Int32_t320_MonoIOErrorU26_t2172,
	RuntimeInvoker_Object_t_MonoIOErrorU26_t2172,
	RuntimeInvoker_FileAttributes_t920_Object_t_MonoIOErrorU26_t2172,
	RuntimeInvoker_MonoFileType_t929_IntPtr_t_MonoIOErrorU26_t2172,
	RuntimeInvoker_Boolean_t333_Object_t_MonoIOStatU26_t2173_MonoIOErrorU26_t2172,
	RuntimeInvoker_IntPtr_t_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320_MonoIOErrorU26_t2172,
	RuntimeInvoker_Boolean_t333_IntPtr_t_MonoIOErrorU26_t2172,
	RuntimeInvoker_Int32_t320_IntPtr_t_Object_t_Int32_t320_Int32_t320_MonoIOErrorU26_t2172,
	RuntimeInvoker_Int64_t345_IntPtr_t_Int64_t345_Int32_t320_MonoIOErrorU26_t2172,
	RuntimeInvoker_Int64_t345_IntPtr_t_MonoIOErrorU26_t2172,
	RuntimeInvoker_Boolean_t333_IntPtr_t_Int64_t345_MonoIOErrorU26_t2172,
	RuntimeInvoker_Void_t765_Object_t_Int32_t320_Int32_t320_Object_t_Object_t_Object_t,
	RuntimeInvoker_CallingConventions_t1001,
	RuntimeInvoker_RuntimeMethodHandle_t1347,
	RuntimeInvoker_MethodAttributes_t1007,
	RuntimeInvoker_MethodToken_t968,
	RuntimeInvoker_FieldAttributes_t1004,
	RuntimeInvoker_RuntimeFieldHandle_t768,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Int32_t320_Object_t_Object_t,
	RuntimeInvoker_Void_t765_OpCode_t972,
	RuntimeInvoker_Void_t765_OpCode_t972_Object_t,
	RuntimeInvoker_StackBehaviour_t977,
	RuntimeInvoker_PropertyAttributes_t1023,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Int32_t320_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t320_Int32_t320_Object_t,
	RuntimeInvoker_Object_t_Int32_t320_Int32_t320_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_SByte_t349_Object_t,
	RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t2139_ModuleU26_t2174,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t349_SByte_t349,
	RuntimeInvoker_AssemblyNameFlags_t996,
	RuntimeInvoker_Object_t_Int32_t320_Object_t_ObjectU5BU5DU26_t2152_Object_t_Object_t_Object_t_ObjectU26_t2141,
	RuntimeInvoker_Void_t765_ObjectU5BU5DU26_t2152_Object_t,
	RuntimeInvoker_Object_t_Int32_t320_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Object_t_ObjectU5BU5DU26_t2152_Object_t,
	RuntimeInvoker_Object_t_Int32_t320_Object_t_Object_t_Object_t_SByte_t349,
	RuntimeInvoker_EventAttributes_t1002,
	RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Object_t_RuntimeFieldHandle_t768,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Object_t_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Object_t_StreamingContext_t311,
	RuntimeInvoker_Object_t_RuntimeMethodHandle_t1347,
	RuntimeInvoker_Void_t765_Object_t_MonoEventInfoU26_t2175,
	RuntimeInvoker_MonoEventInfo_t1012_Object_t,
	RuntimeInvoker_Void_t765_IntPtr_t_MonoMethodInfoU26_t2176,
	RuntimeInvoker_MonoMethodInfo_t1015_IntPtr_t,
	RuntimeInvoker_MethodAttributes_t1007_IntPtr_t,
	RuntimeInvoker_CallingConventions_t1001_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2153,
	RuntimeInvoker_Void_t765_Object_t_MonoPropertyInfoU26_t2177_Int32_t320,
	RuntimeInvoker_ParameterAttributes_t1019,
	RuntimeInvoker_GCHandle_t1048_Object_t_Int32_t320,
	RuntimeInvoker_Void_t765_IntPtr_t_Int32_t320_Object_t_Int32_t320,
	RuntimeInvoker_Void_t765_IntPtr_t_Object_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_BooleanU26_t2142,
	RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t2143,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t2143,
	RuntimeInvoker_Void_t765_TimeSpan_t463,
	RuntimeInvoker_Void_t765_Object_t_Object_t_SByte_t349_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t311_Object_t,
	RuntimeInvoker_Object_t_Object_t_StreamingContext_t311_ISurrogateSelectorU26_t2178,
	RuntimeInvoker_Void_t765_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_TimeSpan_t463_Object_t,
	RuntimeInvoker_Object_t_StringU26_t2143,
	RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t2141,
	RuntimeInvoker_Boolean_t333_Object_t_StringU26_t2143_StringU26_t2143,
	RuntimeInvoker_WellKnownObjectMode_t1141,
	RuntimeInvoker_StreamingContext_t311,
	RuntimeInvoker_TypeFilterLevel_t1157,
	RuntimeInvoker_Void_t765_Object_t_BooleanU26_t2142,
	RuntimeInvoker_Object_t_Byte_t326_Object_t_SByte_t349_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t326_Object_t_SByte_t349_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Object_t_SByte_t349_ObjectU26_t2141_HeaderU5BU5DU26_t2179,
	RuntimeInvoker_Void_t765_Byte_t326_Object_t_SByte_t349_ObjectU26_t2141_HeaderU5BU5DU26_t2179,
	RuntimeInvoker_Boolean_t333_Byte_t326_Object_t,
	RuntimeInvoker_Void_t765_Byte_t326_Object_t_Int64U26_t2154_ObjectU26_t2141_SerializationInfoU26_t2180,
	RuntimeInvoker_Void_t765_Object_t_SByte_t349_SByte_t349_Int64U26_t2154_ObjectU26_t2141_SerializationInfoU26_t2180,
	RuntimeInvoker_Void_t765_Object_t_Int64U26_t2154_ObjectU26_t2141_SerializationInfoU26_t2180,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Int64_t345_ObjectU26_t2141_SerializationInfoU26_t2180,
	RuntimeInvoker_Void_t765_Int64_t345_Object_t_Object_t_Int64_t345_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Object_t_Int64U26_t2154_ObjectU26_t2141,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Int64U26_t2154_ObjectU26_t2141,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Int64_t345_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Int64_t345_Int64_t345_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int64_t345_Object_t,
	RuntimeInvoker_Object_t_Object_t_Byte_t326,
	RuntimeInvoker_Void_t765_Int64_t345_Int32_t320_Int64_t345,
	RuntimeInvoker_Void_t765_Int64_t345_Object_t_Int64_t345,
	RuntimeInvoker_Void_t765_Object_t_Int64_t345_Object_t_Int64_t345_Object_t_Object_t,
	RuntimeInvoker_Boolean_t333_SByte_t349_Object_t_SByte_t349,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_StreamingContext_t311,
	RuntimeInvoker_Void_t765_Object_t_Object_t_StreamingContext_t311,
	RuntimeInvoker_Void_t765_StreamingContext_t311,
	RuntimeInvoker_Object_t_StreamingContext_t311_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Object_t_Int16_t350,
	RuntimeInvoker_Void_t765_Object_t_DateTime_t51,
	RuntimeInvoker_Void_t765_Object_t_Single_t321,
	RuntimeInvoker_SerializationEntry_t1174,
	RuntimeInvoker_StreamingContextStates_t1177,
	RuntimeInvoker_CspProviderFlags_t1181,
	RuntimeInvoker_UInt32_t336_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Object_t_Object_t_SByte_t349,
	RuntimeInvoker_Void_t765_Int64_t345_Object_t_Int32_t320,
	RuntimeInvoker_UInt32_t336_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_UInt32U26_t2155_Int32_t320_UInt32U26_t2155_Int32_t320_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t,
	RuntimeInvoker_Void_t765_Int64_t345_Int64_t345,
	RuntimeInvoker_UInt64_t348_Int64_t345_Int32_t320,
	RuntimeInvoker_UInt64_t348_Int64_t345_Int64_t345_Int64_t345,
	RuntimeInvoker_UInt64_t348_Int64_t345,
	RuntimeInvoker_PaddingMode_t619,
	RuntimeInvoker_Void_t765_StringBuilderU26_t2181_Int32_t320,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_EncoderFallbackBufferU26_t2182_CharU5BU5DU26_t2183,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_DecoderFallbackBufferU26_t2184,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t_Int32_t320,
	RuntimeInvoker_Boolean_t333_Int16_t350_Int32_t320,
	RuntimeInvoker_Boolean_t333_Int16_t350_Int16_t350_Int32_t320,
	RuntimeInvoker_Void_t765_Int16_t350_Int16_t350_Int32_t320,
	RuntimeInvoker_Object_t_Int32U26_t2139,
	RuntimeInvoker_Object_t_Int32_t320_Object_t_Int32_t320,
	RuntimeInvoker_Void_t765_SByte_t349_SByte_t349_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_SByte_t349_Int32_t320_SByte_t349_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_SByte_t349_Int32U26_t2139_BooleanU26_t2142_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_Int32U26_t2139,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_CharU26_t2148_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_CharU26_t2148_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_CharU26_t2148_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t_Int32_t320_CharU26_t2148_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Object_t_DecoderFallbackBufferU26_t2184_ByteU5BU5DU26_t2151_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320_Object_t_DecoderFallbackBufferU26_t2184_ByteU5BU5DU26_t2151_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_DecoderFallbackBufferU26_t2184_ByteU5BU5DU26_t2151_Object_t_Int64_t345_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_DecoderFallbackBufferU26_t2184_ByteU5BU5DU26_t2151_Object_t_Int64_t345_Int32_t320_Object_t_Int32U26_t2139,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Object_t_Int32_t320_UInt32U26_t2155_UInt32U26_t2155_Object_t_DecoderFallbackBufferU26_t2184_ByteU5BU5DU26_t2151_SByte_t349,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t_Int32_t320_UInt32U26_t2155_UInt32U26_t2155_Object_t_DecoderFallbackBufferU26_t2184_ByteU5BU5DU26_t2151_SByte_t349,
	RuntimeInvoker_Void_t765_SByte_t349_Int32_t320,
	RuntimeInvoker_IntPtr_t_SByte_t349_Object_t_BooleanU26_t2142,
	RuntimeInvoker_Boolean_t333_IntPtr_t,
	RuntimeInvoker_IntPtr_t_SByte_t349_SByte_t349_Object_t_BooleanU26_t2142,
	RuntimeInvoker_Boolean_t333_TimeSpan_t463_TimeSpan_t463,
	RuntimeInvoker_Boolean_t333_Int64_t345_Int64_t345_SByte_t349,
	RuntimeInvoker_Boolean_t333_IntPtr_t_Int32_t320_SByte_t349,
	RuntimeInvoker_Int64_t345_Double_t344,
	RuntimeInvoker_Object_t_Double_t344,
	RuntimeInvoker_Int64_t345_Object_t_Int32_t320,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t320_Int32_t320,
	RuntimeInvoker_Byte_t326_SByte_t349,
	RuntimeInvoker_Byte_t326_Double_t344,
	RuntimeInvoker_Byte_t326_Single_t321,
	RuntimeInvoker_Byte_t326_Int64_t345,
	RuntimeInvoker_Char_t576_SByte_t349,
	RuntimeInvoker_Char_t576_Int64_t345,
	RuntimeInvoker_Char_t576_Single_t321,
	RuntimeInvoker_Char_t576_Object_t_Object_t,
	RuntimeInvoker_DateTime_t51_Object_t_Object_t,
	RuntimeInvoker_DateTime_t51_Int16_t350,
	RuntimeInvoker_DateTime_t51_Int32_t320,
	RuntimeInvoker_DateTime_t51_Int64_t345,
	RuntimeInvoker_DateTime_t51_Single_t321,
	RuntimeInvoker_DateTime_t51_SByte_t349,
	RuntimeInvoker_Double_t344_SByte_t349,
	RuntimeInvoker_Double_t344_Double_t344,
	RuntimeInvoker_Double_t344_Single_t321,
	RuntimeInvoker_Double_t344_Int32_t320,
	RuntimeInvoker_Double_t344_Int64_t345,
	RuntimeInvoker_Double_t344_Int16_t350,
	RuntimeInvoker_Int16_t350_SByte_t349,
	RuntimeInvoker_Int16_t350_Double_t344,
	RuntimeInvoker_Int16_t350_Single_t321,
	RuntimeInvoker_Int16_t350_Int32_t320,
	RuntimeInvoker_Int16_t350_Int64_t345,
	RuntimeInvoker_Int64_t345_SByte_t349,
	RuntimeInvoker_Int64_t345_Int16_t350,
	RuntimeInvoker_Int64_t345_Single_t321,
	RuntimeInvoker_Int64_t345_Int64_t345,
	RuntimeInvoker_SByte_t349_SByte_t349,
	RuntimeInvoker_SByte_t349_Int16_t350,
	RuntimeInvoker_SByte_t349_Double_t344,
	RuntimeInvoker_SByte_t349_Single_t321,
	RuntimeInvoker_SByte_t349_Int32_t320,
	RuntimeInvoker_SByte_t349_Int64_t345,
	RuntimeInvoker_Single_t321_SByte_t349,
	RuntimeInvoker_Single_t321_Double_t344,
	RuntimeInvoker_Single_t321_Int64_t345,
	RuntimeInvoker_Single_t321_Int16_t350,
	RuntimeInvoker_UInt16_t351_SByte_t349,
	RuntimeInvoker_UInt16_t351_Double_t344,
	RuntimeInvoker_UInt16_t351_Single_t321,
	RuntimeInvoker_UInt16_t351_Int32_t320,
	RuntimeInvoker_UInt16_t351_Int64_t345,
	RuntimeInvoker_UInt32_t336_SByte_t349,
	RuntimeInvoker_UInt32_t336_Int16_t350,
	RuntimeInvoker_UInt32_t336_Double_t344,
	RuntimeInvoker_UInt32_t336_Single_t321,
	RuntimeInvoker_UInt32_t336_Int64_t345,
	RuntimeInvoker_UInt64_t348_SByte_t349,
	RuntimeInvoker_UInt64_t348_Int16_t350,
	RuntimeInvoker_UInt64_t348_Double_t344,
	RuntimeInvoker_UInt64_t348_Single_t321,
	RuntimeInvoker_UInt64_t348_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Void_t765_SByte_t349_TimeSpan_t463,
	RuntimeInvoker_Void_t765_Int64_t345_Int32_t320,
	RuntimeInvoker_DayOfWeek_t1306,
	RuntimeInvoker_DateTimeKind_t1304,
	RuntimeInvoker_DateTime_t51_TimeSpan_t463,
	RuntimeInvoker_DateTime_t51_Double_t344,
	RuntimeInvoker_Int32_t320_DateTime_t51_DateTime_t51,
	RuntimeInvoker_DateTime_t51_DateTime_t51_Int32_t320,
	RuntimeInvoker_DateTime_t51_Object_t_Object_t_Int32_t320,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Int32_t320_DateTimeU26_t2185_DateTimeOffsetU26_t2186_SByte_t349_ExceptionU26_t2153,
	RuntimeInvoker_Object_t_Object_t_SByte_t349_ExceptionU26_t2153,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320_SByte_t349_SByte_t349_Int32U26_t2139,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Object_t_Object_t_SByte_t349_Int32U26_t2139,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Int32_t320_Object_t_Int32U26_t2139,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Int32_t320_Object_t_SByte_t349_Int32U26_t2139_Int32U26_t2139,
	RuntimeInvoker_Boolean_t333_Object_t_Int32_t320_Object_t_SByte_t349_Int32U26_t2139,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t_SByte_t349_DateTimeU26_t2185_DateTimeOffsetU26_t2186_Object_t_Int32_t320_SByte_t349_BooleanU26_t2142_BooleanU26_t2142,
	RuntimeInvoker_DateTime_t51_Object_t_Object_t_Object_t_Int32_t320,
	RuntimeInvoker_Boolean_t333_Object_t_Object_t_Object_t_Int32_t320_DateTimeU26_t2185_SByte_t349_BooleanU26_t2142_SByte_t349_ExceptionU26_t2153,
	RuntimeInvoker_DateTime_t51_DateTime_t51_TimeSpan_t463,
	RuntimeInvoker_Boolean_t333_DateTime_t51_DateTime_t51,
	RuntimeInvoker_Void_t765_DateTime_t51_TimeSpan_t463,
	RuntimeInvoker_Void_t765_Int64_t345_TimeSpan_t463,
	RuntimeInvoker_Int32_t320_DateTimeOffset_t363,
	RuntimeInvoker_Boolean_t333_DateTimeOffset_t363,
	RuntimeInvoker_DateTimeOffset_t363,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int16_t350,
	RuntimeInvoker_Object_t_Int16_t350_Object_t_BooleanU26_t2142_BooleanU26_t2142,
	RuntimeInvoker_Object_t_Int16_t350_Object_t_BooleanU26_t2142_BooleanU26_t2142_SByte_t349,
	RuntimeInvoker_Object_t_DateTime_t51_Object_t_Object_t,
	RuntimeInvoker_Object_t_DateTime_t51_Nullable_1_t1403_Object_t_Object_t,
	RuntimeInvoker_Void_t765_MonoEnumInfo_t1317,
	RuntimeInvoker_Void_t765_Object_t_MonoEnumInfoU26_t2187,
	RuntimeInvoker_Int32_t320_Int16_t350_Int16_t350,
	RuntimeInvoker_Int32_t320_Int64_t345_Int64_t345,
	RuntimeInvoker_PlatformID_t1344,
	RuntimeInvoker_Void_t765_Int32_t320_Int16_t350_Int16_t350_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349_SByte_t349,
	RuntimeInvoker_Int32_t320_Guid_t364,
	RuntimeInvoker_Boolean_t333_Guid_t364,
	RuntimeInvoker_Guid_t364,
	RuntimeInvoker_Object_t_SByte_t349_SByte_t349_SByte_t349,
	RuntimeInvoker_Double_t344_Double_t344_Double_t344,
	RuntimeInvoker_TypeAttributes_t1027_Object_t,
	RuntimeInvoker_Object_t_SByte_t349_SByte_t349,
	RuntimeInvoker_Void_t765_UInt64U2AU26_t2188_Int32U2AU26_t2189_CharU2AU26_t2190_CharU2AU26_t2190_Int64U2AU26_t2191_Int32U2AU26_t2189,
	RuntimeInvoker_Void_t765_Int32_t320_Int64_t345,
	RuntimeInvoker_Void_t765_Object_t_Double_t344_Int32_t320,
	RuntimeInvoker_Void_t765_Object_t_Decimal_t347,
	RuntimeInvoker_Object_t_Object_t_SByte_t349_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int16_t350_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int64_t345_Object_t,
	RuntimeInvoker_Object_t_Object_t_Single_t321_Object_t,
	RuntimeInvoker_Object_t_Object_t_Double_t344_Object_t,
	RuntimeInvoker_Object_t_Object_t_Decimal_t347_Object_t,
	RuntimeInvoker_Object_t_Single_t321_Object_t,
	RuntimeInvoker_Object_t_Double_t344_Object_t,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Object_t,
	RuntimeInvoker_Void_t765_Object_t_BooleanU26_t2142_SByte_t349_Int32U26_t2139_Int32U26_t2139,
	RuntimeInvoker_Object_t_Object_t_Int32_t320_Int32_t320_Object_t_SByte_t349_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t765_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_Int64_t345_Int32_t320_Int32_t320_Int32_t320_Int32_t320_Int32_t320,
	RuntimeInvoker_TimeSpan_t463_TimeSpan_t463,
	RuntimeInvoker_Int32_t320_TimeSpan_t463_TimeSpan_t463,
	RuntimeInvoker_Int32_t320_TimeSpan_t463,
	RuntimeInvoker_Boolean_t333_TimeSpan_t463,
	RuntimeInvoker_TimeSpan_t463_Double_t344,
	RuntimeInvoker_TimeSpan_t463_Double_t344_Int64_t345,
	RuntimeInvoker_TimeSpan_t463_TimeSpan_t463_TimeSpan_t463,
	RuntimeInvoker_TimeSpan_t463_DateTime_t51,
	RuntimeInvoker_Boolean_t333_DateTime_t51_Object_t,
	RuntimeInvoker_DateTime_t51_DateTime_t51,
	RuntimeInvoker_TimeSpan_t463_DateTime_t51_TimeSpan_t463,
	RuntimeInvoker_Boolean_t333_Int32_t320_Int64U5BU5DU26_t2192_StringU5BU5DU26_t2193,
	RuntimeInvoker_Void_t765_KeyValuePair_2_t1493,
	RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1493,
	RuntimeInvoker_Enumerator_t1651,
	RuntimeInvoker_Void_t765_Int32_t320_ObjectU26_t2141,
	RuntimeInvoker_Void_t765_ObjectU5BU5DU26_t2152_Int32_t320,
	RuntimeInvoker_Void_t765_ObjectU5BU5DU26_t2152_Int32_t320_Int32_t320,
	RuntimeInvoker_KeyValuePair_2_t1493_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1497,
	RuntimeInvoker_DictionaryEntry_t560_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1493,
	RuntimeInvoker_Enumerator_t1496,
	RuntimeInvoker_Enumerator_t1500,
	RuntimeInvoker_Enumerator_t1450,
	RuntimeInvoker_GcAchievementData_t224_Int32_t320,
	RuntimeInvoker_Void_t765_GcAchievementData_t224,
	RuntimeInvoker_Boolean_t333_GcAchievementData_t224,
	RuntimeInvoker_Int32_t320_GcAchievementData_t224,
	RuntimeInvoker_Void_t765_Int32_t320_GcAchievementData_t224,
	RuntimeInvoker_GcScoreData_t225_Int32_t320,
	RuntimeInvoker_Boolean_t333_GcScoreData_t225,
	RuntimeInvoker_Int32_t320_GcScoreData_t225,
	RuntimeInvoker_Void_t765_Int32_t320_GcScoreData_t225,
	RuntimeInvoker_KeyValuePair_2_t1465_Int32_t320,
	RuntimeInvoker_Void_t765_KeyValuePair_2_t1465,
	RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1465,
	RuntimeInvoker_Int32_t320_KeyValuePair_2_t1465,
	RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1465,
	RuntimeInvoker_Link_t864_Int32_t320,
	RuntimeInvoker_Void_t765_Link_t864,
	RuntimeInvoker_Boolean_t333_Link_t864,
	RuntimeInvoker_Int32_t320_Link_t864,
	RuntimeInvoker_Void_t765_Int32_t320_Link_t864,
	RuntimeInvoker_DictionaryEntry_t560_Int32_t320,
	RuntimeInvoker_Void_t765_DictionaryEntry_t560,
	RuntimeInvoker_Boolean_t333_DictionaryEntry_t560,
	RuntimeInvoker_Int32_t320_DictionaryEntry_t560,
	RuntimeInvoker_Void_t765_Int32_t320_DictionaryEntry_t560,
	RuntimeInvoker_KeyValuePair_2_t1493_Int32_t320,
	RuntimeInvoker_Int32_t320_KeyValuePair_2_t1493,
	RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1493,
	RuntimeInvoker_KeyValuePair_2_t1509_Int32_t320,
	RuntimeInvoker_Void_t765_KeyValuePair_2_t1509,
	RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1509,
	RuntimeInvoker_Int32_t320_KeyValuePair_2_t1509,
	RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1509,
	RuntimeInvoker_Void_t765_Int32_t320_IntPtr_t,
	RuntimeInvoker_Keyframe_t135_Int32_t320,
	RuntimeInvoker_Void_t765_Keyframe_t135,
	RuntimeInvoker_Boolean_t333_Keyframe_t135,
	RuntimeInvoker_Int32_t320_Keyframe_t135,
	RuntimeInvoker_Void_t765_Int32_t320_Keyframe_t135,
	RuntimeInvoker_UIVertex_t158_Int32_t320,
	RuntimeInvoker_Void_t765_UIVertex_t158,
	RuntimeInvoker_Boolean_t333_UIVertex_t158,
	RuntimeInvoker_Int32_t320_UIVertex_t158,
	RuntimeInvoker_Void_t765_Int32_t320_UIVertex_t158,
	RuntimeInvoker_Void_t765_UIVertexU5BU5DU26_t2194_Int32_t320,
	RuntimeInvoker_Void_t765_UIVertexU5BU5DU26_t2194_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_UIVertex_t158_Int32_t320_Int32_t320,
	RuntimeInvoker_UICharInfo_t149_Int32_t320,
	RuntimeInvoker_Void_t765_UICharInfo_t149,
	RuntimeInvoker_Boolean_t333_UICharInfo_t149,
	RuntimeInvoker_Int32_t320_UICharInfo_t149,
	RuntimeInvoker_Void_t765_Int32_t320_UICharInfo_t149,
	RuntimeInvoker_Void_t765_UICharInfoU5BU5DU26_t2195_Int32_t320,
	RuntimeInvoker_Void_t765_UICharInfoU5BU5DU26_t2195_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_UICharInfo_t149_Int32_t320_Int32_t320,
	RuntimeInvoker_UILineInfo_t150_Int32_t320,
	RuntimeInvoker_Void_t765_UILineInfo_t150,
	RuntimeInvoker_Boolean_t333_UILineInfo_t150,
	RuntimeInvoker_Int32_t320_UILineInfo_t150,
	RuntimeInvoker_Void_t765_Int32_t320_UILineInfo_t150,
	RuntimeInvoker_Void_t765_UILineInfoU5BU5DU26_t2196_Int32_t320,
	RuntimeInvoker_Void_t765_UILineInfoU5BU5DU26_t2196_Int32_t320_Int32_t320,
	RuntimeInvoker_Int32_t320_Object_t_UILineInfo_t150_Int32_t320_Int32_t320,
	RuntimeInvoker_KeyValuePair_2_t1552_Int32_t320,
	RuntimeInvoker_Void_t765_KeyValuePair_2_t1552,
	RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1552,
	RuntimeInvoker_Int32_t320_KeyValuePair_2_t1552,
	RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1552,
	RuntimeInvoker_KeyValuePair_2_t1581_Int32_t320,
	RuntimeInvoker_Void_t765_KeyValuePair_2_t1581,
	RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1581,
	RuntimeInvoker_Int32_t320_KeyValuePair_2_t1581,
	RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1581,
	RuntimeInvoker_NetworkID_t178_Int32_t320,
	RuntimeInvoker_Boolean_t333_UInt64_t348,
	RuntimeInvoker_Int32_t320_UInt64_t348,
	RuntimeInvoker_Void_t765_Int32_t320_UInt64_t348,
	RuntimeInvoker_KeyValuePair_2_t1601_Int32_t320,
	RuntimeInvoker_Void_t765_KeyValuePair_2_t1601,
	RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1601,
	RuntimeInvoker_Int32_t320_KeyValuePair_2_t1601,
	RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1601,
	RuntimeInvoker_ParameterModifier_t1020_Int32_t320,
	RuntimeInvoker_Void_t765_ParameterModifier_t1020,
	RuntimeInvoker_Boolean_t333_ParameterModifier_t1020,
	RuntimeInvoker_Int32_t320_ParameterModifier_t1020,
	RuntimeInvoker_Void_t765_Int32_t320_ParameterModifier_t1020,
	RuntimeInvoker_HitInfo_t239_Int32_t320,
	RuntimeInvoker_Void_t765_HitInfo_t239,
	RuntimeInvoker_Int32_t320_HitInfo_t239,
	RuntimeInvoker_KeyValuePair_2_t1662_Int32_t320,
	RuntimeInvoker_Void_t765_KeyValuePair_2_t1662,
	RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1662,
	RuntimeInvoker_Int32_t320_KeyValuePair_2_t1662,
	RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1662,
	RuntimeInvoker_TextEditOp_t252_Int32_t320,
	RuntimeInvoker_KeyValuePair_2_t1692_Int32_t320,
	RuntimeInvoker_Void_t765_KeyValuePair_2_t1692,
	RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1692,
	RuntimeInvoker_Int32_t320_KeyValuePair_2_t1692,
	RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1692,
	RuntimeInvoker_X509ChainStatus_t457_Int32_t320,
	RuntimeInvoker_Void_t765_X509ChainStatus_t457,
	RuntimeInvoker_Boolean_t333_X509ChainStatus_t457,
	RuntimeInvoker_Int32_t320_X509ChainStatus_t457,
	RuntimeInvoker_Void_t765_Int32_t320_X509ChainStatus_t457,
	RuntimeInvoker_KeyValuePair_2_t1716_Int32_t320,
	RuntimeInvoker_Void_t765_KeyValuePair_2_t1716,
	RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1716,
	RuntimeInvoker_Int32_t320_KeyValuePair_2_t1716,
	RuntimeInvoker_Void_t765_Int32_t320_KeyValuePair_2_t1716,
	RuntimeInvoker_Int32_t320_Object_t_Int32_t320_Int32_t320_Int32_t320_Object_t,
	RuntimeInvoker_Mark_t511_Int32_t320,
	RuntimeInvoker_Void_t765_Mark_t511,
	RuntimeInvoker_Boolean_t333_Mark_t511,
	RuntimeInvoker_Int32_t320_Mark_t511,
	RuntimeInvoker_Void_t765_Int32_t320_Mark_t511,
	RuntimeInvoker_UriScheme_t548_Int32_t320,
	RuntimeInvoker_Void_t765_UriScheme_t548,
	RuntimeInvoker_Boolean_t333_UriScheme_t548,
	RuntimeInvoker_Int32_t320_UriScheme_t548,
	RuntimeInvoker_Void_t765_Int32_t320_UriScheme_t548,
	RuntimeInvoker_ClientCertificateType_t713_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_Double_t344,
	RuntimeInvoker_TableRange_t797_Int32_t320,
	RuntimeInvoker_Void_t765_TableRange_t797,
	RuntimeInvoker_Boolean_t333_TableRange_t797,
	RuntimeInvoker_Int32_t320_TableRange_t797,
	RuntimeInvoker_Void_t765_Int32_t320_TableRange_t797,
	RuntimeInvoker_Slot_t874_Int32_t320,
	RuntimeInvoker_Void_t765_Slot_t874,
	RuntimeInvoker_Boolean_t333_Slot_t874,
	RuntimeInvoker_Int32_t320_Slot_t874,
	RuntimeInvoker_Void_t765_Int32_t320_Slot_t874,
	RuntimeInvoker_Slot_t881_Int32_t320,
	RuntimeInvoker_Void_t765_Slot_t881,
	RuntimeInvoker_Boolean_t333_Slot_t881,
	RuntimeInvoker_Int32_t320_Slot_t881,
	RuntimeInvoker_Void_t765_Int32_t320_Slot_t881,
	RuntimeInvoker_ILTokenInfo_t959_Int32_t320,
	RuntimeInvoker_Void_t765_ILTokenInfo_t959,
	RuntimeInvoker_Boolean_t333_ILTokenInfo_t959,
	RuntimeInvoker_Int32_t320_ILTokenInfo_t959,
	RuntimeInvoker_Void_t765_Int32_t320_ILTokenInfo_t959,
	RuntimeInvoker_LabelData_t961_Int32_t320,
	RuntimeInvoker_Void_t765_LabelData_t961,
	RuntimeInvoker_Boolean_t333_LabelData_t961,
	RuntimeInvoker_Int32_t320_LabelData_t961,
	RuntimeInvoker_Void_t765_Int32_t320_LabelData_t961,
	RuntimeInvoker_LabelFixup_t960_Int32_t320,
	RuntimeInvoker_Void_t765_LabelFixup_t960,
	RuntimeInvoker_Boolean_t333_LabelFixup_t960,
	RuntimeInvoker_Int32_t320_LabelFixup_t960,
	RuntimeInvoker_Void_t765_Int32_t320_LabelFixup_t960,
	RuntimeInvoker_Void_t765_Int32_t320_DateTime_t51,
	RuntimeInvoker_Void_t765_Decimal_t347,
	RuntimeInvoker_Void_t765_Int32_t320_Decimal_t347,
	RuntimeInvoker_TimeSpan_t463_Int32_t320,
	RuntimeInvoker_Void_t765_Int32_t320_TimeSpan_t463,
	RuntimeInvoker_TypeTag_t1145_Int32_t320,
	RuntimeInvoker_Boolean_t333_Byte_t326,
	RuntimeInvoker_Int32_t320_Byte_t326,
	RuntimeInvoker_Void_t765_Int32_t320_Byte_t326,
	RuntimeInvoker_GcAchievementData_t224,
	RuntimeInvoker_GcScoreData_t225,
	RuntimeInvoker_KeyValuePair_2_t1465_Int32_t320_Object_t,
	RuntimeInvoker_Int32_t320_Int32_t320_Object_t,
	RuntimeInvoker_Boolean_t333_Int32_t320_ObjectU26_t2141,
	RuntimeInvoker_Enumerator_t1471,
	RuntimeInvoker_DictionaryEntry_t560_Int32_t320_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1465,
	RuntimeInvoker_Link_t864,
	RuntimeInvoker_Enumerator_t1470,
	RuntimeInvoker_Enumerator_t1474,
	RuntimeInvoker_DictionaryEntry_t560_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1465_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1493_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1509_Object_t_Int32_t320,
	RuntimeInvoker_Enumerator_t1513,
	RuntimeInvoker_DictionaryEntry_t560_Object_t_Int32_t320,
	RuntimeInvoker_KeyValuePair_2_t1509,
	RuntimeInvoker_Enumerator_t1512,
	RuntimeInvoker_Enumerator_t1516,
	RuntimeInvoker_KeyValuePair_2_t1509_Object_t,
	RuntimeInvoker_Keyframe_t135,
	RuntimeInvoker_UIVertex_t158,
	RuntimeInvoker_Enumerator_t1538,
	RuntimeInvoker_Boolean_t333_UIVertex_t158_UIVertex_t158,
	RuntimeInvoker_UICharInfo_t149,
	RuntimeInvoker_Enumerator_t1542,
	RuntimeInvoker_Boolean_t333_UICharInfo_t149_UICharInfo_t149,
	RuntimeInvoker_UILineInfo_t150,
	RuntimeInvoker_Enumerator_t1546,
	RuntimeInvoker_Boolean_t333_UILineInfo_t150_UILineInfo_t150,
	RuntimeInvoker_KeyValuePair_2_t1552_Object_t_Int64_t345,
	RuntimeInvoker_Int64_t345_Object_t_Int64_t345,
	RuntimeInvoker_Enumerator_t1557,
	RuntimeInvoker_DictionaryEntry_t560_Object_t_Int64_t345,
	RuntimeInvoker_KeyValuePair_2_t1552,
	RuntimeInvoker_Enumerator_t1556,
	RuntimeInvoker_Object_t_Object_t_Int64_t345_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1560,
	RuntimeInvoker_KeyValuePair_2_t1552_Object_t,
	RuntimeInvoker_Boolean_t333_Int64_t345_Int64_t345,
	RuntimeInvoker_Void_t765_UInt64_t348_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1581_UInt64_t348_Object_t,
	RuntimeInvoker_NetworkID_t178_UInt64_t348_Object_t,
	RuntimeInvoker_Boolean_t333_UInt64_t348_ObjectU26_t2141,
	RuntimeInvoker_NetworkID_t178_Object_t,
	RuntimeInvoker_Enumerator_t1586,
	RuntimeInvoker_DictionaryEntry_t560_UInt64_t348_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1581,
	RuntimeInvoker_Enumerator_t1585,
	RuntimeInvoker_Object_t_UInt64_t348_Object_t_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1589,
	RuntimeInvoker_KeyValuePair_2_t1581_Object_t,
	RuntimeInvoker_Boolean_t333_UInt64_t348_UInt64_t348,
	RuntimeInvoker_KeyValuePair_2_t1601,
	RuntimeInvoker_Void_t765_Object_t_KeyValuePair_2_t1493,
	RuntimeInvoker_KeyValuePair_2_t1601_Object_t_KeyValuePair_2_t1493,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t1493,
	RuntimeInvoker_KeyValuePair_2_t1493_Object_t_KeyValuePair_2_t1493,
	RuntimeInvoker_Boolean_t333_Object_t_KeyValuePair_2U26_t2197,
	RuntimeInvoker_Enumerator_t1630,
	RuntimeInvoker_DictionaryEntry_t560_Object_t_KeyValuePair_2_t1493,
	RuntimeInvoker_Enumerator_t1629,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t1493_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1633,
	RuntimeInvoker_KeyValuePair_2_t1601_Object_t,
	RuntimeInvoker_Boolean_t333_KeyValuePair_2_t1493_KeyValuePair_2_t1493,
	RuntimeInvoker_ParameterModifier_t1020,
	RuntimeInvoker_HitInfo_t239,
	RuntimeInvoker_TextEditOp_t252_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1662_Object_t_Int32_t320,
	RuntimeInvoker_TextEditOp_t252_Object_t_Int32_t320,
	RuntimeInvoker_Boolean_t333_Object_t_TextEditOpU26_t2198,
	RuntimeInvoker_Enumerator_t1667,
	RuntimeInvoker_KeyValuePair_2_t1662,
	RuntimeInvoker_TextEditOp_t252,
	RuntimeInvoker_Enumerator_t1666,
	RuntimeInvoker_Enumerator_t1670,
	RuntimeInvoker_KeyValuePair_2_t1662_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1692_Object_t_SByte_t349,
	RuntimeInvoker_Boolean_t333_Object_t_BooleanU26_t2142,
	RuntimeInvoker_Enumerator_t1697,
	RuntimeInvoker_DictionaryEntry_t560_Object_t_SByte_t349,
	RuntimeInvoker_KeyValuePair_2_t1692,
	RuntimeInvoker_Enumerator_t1696,
	RuntimeInvoker_Object_t_Object_t_SByte_t349_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1700,
	RuntimeInvoker_KeyValuePair_2_t1692_Object_t,
	RuntimeInvoker_Boolean_t333_SByte_t349_SByte_t349,
	RuntimeInvoker_X509ChainStatus_t457,
	RuntimeInvoker_KeyValuePair_2_t1716_Int32_t320_Int32_t320,
	RuntimeInvoker_Boolean_t333_Int32_t320_Int32U26_t2139,
	RuntimeInvoker_Enumerator_t1720,
	RuntimeInvoker_DictionaryEntry_t560_Int32_t320_Int32_t320,
	RuntimeInvoker_KeyValuePair_2_t1716,
	RuntimeInvoker_Enumerator_t1719,
	RuntimeInvoker_Enumerator_t1723,
	RuntimeInvoker_KeyValuePair_2_t1716_Object_t,
	RuntimeInvoker_Mark_t511,
	RuntimeInvoker_UriScheme_t548,
	RuntimeInvoker_ClientCertificateType_t713,
	RuntimeInvoker_TableRange_t797,
	RuntimeInvoker_Slot_t874,
	RuntimeInvoker_Slot_t881,
	RuntimeInvoker_ILTokenInfo_t959,
	RuntimeInvoker_LabelData_t961,
	RuntimeInvoker_LabelFixup_t960,
	RuntimeInvoker_TypeTag_t1145,
	RuntimeInvoker_Int32_t320_DateTimeOffset_t363_DateTimeOffset_t363,
	RuntimeInvoker_Boolean_t333_DateTimeOffset_t363_DateTimeOffset_t363,
	RuntimeInvoker_Boolean_t333_Nullable_1_t1403,
	RuntimeInvoker_Int32_t320_Guid_t364_Guid_t364,
	RuntimeInvoker_Boolean_t333_Guid_t364_Guid_t364,
};
