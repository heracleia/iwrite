﻿#pragma once
#include <stdint.h>
// System.DBNull
struct DBNull_t1302;
// System.Object
#include "mscorlib_System_Object.h"
// System.DBNull
struct  DBNull_t1302  : public Object_t
{
};
struct DBNull_t1302_StaticFields{
	// System.DBNull System.DBNull::Value
	DBNull_t1302 * ___Value_0;
};
