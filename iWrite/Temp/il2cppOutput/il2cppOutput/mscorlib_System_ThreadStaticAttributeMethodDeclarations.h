﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ThreadStaticAttribute
struct ThreadStaticAttribute_t1352;

// System.Void System.ThreadStaticAttribute::.ctor()
extern "C" void ThreadStaticAttribute__ctor_m8132 (ThreadStaticAttribute_t1352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
