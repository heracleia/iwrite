﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.HMAC
#include "mscorlib_System_Security_Cryptography_HMAC.h"
// System.Security.Cryptography.HMACSHA512
struct  HMACSHA512_t1190  : public HMAC_t742
{
	// System.Boolean System.Security.Cryptography.HMACSHA512::legacy
	bool ___legacy_11;
};
struct HMACSHA512_t1190_StaticFields{
	// System.Boolean System.Security.Cryptography.HMACSHA512::legacy_mode
	bool ___legacy_mode_10;
};
