﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
struct ShimEnumerator_t1519;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1508;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m9165_gshared (ShimEnumerator_t1519 * __this, Dictionary_2_t1508 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m9165(__this, ___host, method) (( void (*) (ShimEnumerator_t1519 *, Dictionary_2_t1508 *, const MethodInfo*))ShimEnumerator__ctor_m9165_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m9166_gshared (ShimEnumerator_t1519 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m9166(__this, method) (( bool (*) (ShimEnumerator_t1519 *, const MethodInfo*))ShimEnumerator_MoveNext_m9166_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t560  ShimEnumerator_get_Entry_m9167_gshared (ShimEnumerator_t1519 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m9167(__this, method) (( DictionaryEntry_t560  (*) (ShimEnumerator_t1519 *, const MethodInfo*))ShimEnumerator_get_Entry_m9167_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m9168_gshared (ShimEnumerator_t1519 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m9168(__this, method) (( Object_t * (*) (ShimEnumerator_t1519 *, const MethodInfo*))ShimEnumerator_get_Key_m9168_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m9169_gshared (ShimEnumerator_t1519 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m9169(__this, method) (( Object_t * (*) (ShimEnumerator_t1519 *, const MethodInfo*))ShimEnumerator_get_Value_m9169_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m9170_gshared (ShimEnumerator_t1519 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m9170(__this, method) (( Object_t * (*) (ShimEnumerator_t1519 *, const MethodInfo*))ShimEnumerator_get_Current_m9170_gshared)(__this, method)
