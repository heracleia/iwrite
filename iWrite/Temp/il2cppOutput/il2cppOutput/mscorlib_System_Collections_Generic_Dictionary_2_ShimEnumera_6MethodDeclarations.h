﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>
struct ShimEnumerator_t1703;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1691;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m11441_gshared (ShimEnumerator_t1703 * __this, Dictionary_2_t1691 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m11441(__this, ___host, method) (( void (*) (ShimEnumerator_t1703 *, Dictionary_2_t1691 *, const MethodInfo*))ShimEnumerator__ctor_m11441_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m11442_gshared (ShimEnumerator_t1703 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m11442(__this, method) (( bool (*) (ShimEnumerator_t1703 *, const MethodInfo*))ShimEnumerator_MoveNext_m11442_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Entry()
extern "C" DictionaryEntry_t560  ShimEnumerator_get_Entry_m11443_gshared (ShimEnumerator_t1703 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m11443(__this, method) (( DictionaryEntry_t560  (*) (ShimEnumerator_t1703 *, const MethodInfo*))ShimEnumerator_get_Entry_m11443_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m11444_gshared (ShimEnumerator_t1703 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m11444(__this, method) (( Object_t * (*) (ShimEnumerator_t1703 *, const MethodInfo*))ShimEnumerator_get_Key_m11444_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m11445_gshared (ShimEnumerator_t1703 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m11445(__this, method) (( Object_t * (*) (ShimEnumerator_t1703 *, const MethodInfo*))ShimEnumerator_get_Value_m11445_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m11446_gshared (ShimEnumerator_t1703 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m11446(__this, method) (( Object_t * (*) (ShimEnumerator_t1703 *, const MethodInfo*))ShimEnumerator_get_Current_m11446_gshared)(__this, method)
