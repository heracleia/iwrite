﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ShimEnumerator_t1636;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t1627;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m10770_gshared (ShimEnumerator_t1636 * __this, Dictionary_2_t1627 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m10770(__this, ___host, method) (( void (*) (ShimEnumerator_t1636 *, Dictionary_2_t1627 *, const MethodInfo*))ShimEnumerator__ctor_m10770_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m10771_gshared (ShimEnumerator_t1636 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m10771(__this, method) (( bool (*) (ShimEnumerator_t1636 *, const MethodInfo*))ShimEnumerator_MoveNext_m10771_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Entry()
extern "C" DictionaryEntry_t560  ShimEnumerator_get_Entry_m10772_gshared (ShimEnumerator_t1636 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m10772(__this, method) (( DictionaryEntry_t560  (*) (ShimEnumerator_t1636 *, const MethodInfo*))ShimEnumerator_get_Entry_m10772_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m10773_gshared (ShimEnumerator_t1636 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m10773(__this, method) (( Object_t * (*) (ShimEnumerator_t1636 *, const MethodInfo*))ShimEnumerator_get_Key_m10773_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m10774_gshared (ShimEnumerator_t1636 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m10774(__this, method) (( Object_t * (*) (ShimEnumerator_t1636 *, const MethodInfo*))ShimEnumerator_get_Value_m10774_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m10775_gshared (ShimEnumerator_t1636 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m10775(__this, method) (( Object_t * (*) (ShimEnumerator_t1636 *, const MethodInfo*))ShimEnumerator_get_Current_m10775_gshared)(__this, method)
