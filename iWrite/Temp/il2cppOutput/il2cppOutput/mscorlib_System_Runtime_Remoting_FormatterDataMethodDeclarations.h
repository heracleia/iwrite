﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.FormatterData
struct FormatterData_t1130;

// System.Void System.Runtime.Remoting.FormatterData::.ctor()
extern "C" void FormatterData__ctor_m6444 (FormatterData_t1130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
