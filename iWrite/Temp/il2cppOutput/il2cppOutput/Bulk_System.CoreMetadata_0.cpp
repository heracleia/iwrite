﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// <Module>
#include "System_Core_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t601_il2cpp_TypeInfo;
// <Module>
#include "System_Core_U3CModuleU3EMethodDeclarations.h"
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CModuleU3E_t601_0_0_0;
extern const Il2CppType U3CModuleU3E_t601_1_0_0;
struct U3CModuleU3E_t601;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t601_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CModuleU3E_t601_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CModuleU3E_t601_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t601_0_0_0/* byval_arg */
	, &U3CModuleU3E_t601_1_0_0/* this_arg */
	, &U3CModuleU3E_t601_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t601)/* instance_size */
	, sizeof (U3CModuleU3E_t601)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// Metadata Definition System.Runtime.CompilerServices.ExtensionAttribute
extern TypeInfo ExtensionAttribute_t602_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
static const EncodedMethodIndex ExtensionAttribute_t602_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
extern const Il2CppType _Attribute_t2052_0_0_0;
static Il2CppInterfaceOffsetPair ExtensionAttribute_t602_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType ExtensionAttribute_t602_0_0_0;
extern const Il2CppType ExtensionAttribute_t602_1_0_0;
extern const Il2CppType Attribute_t96_0_0_0;
struct ExtensionAttribute_t602;
const Il2CppTypeDefinitionMetadata ExtensionAttribute_t602_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExtensionAttribute_t602_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, ExtensionAttribute_t602_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2166/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExtensionAttribute_t602_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtensionAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &ExtensionAttribute_t602_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 515/* custom_attributes_cache */
	, &ExtensionAttribute_t602_0_0_0/* byval_arg */
	, &ExtensionAttribute_t602_1_0_0/* this_arg */
	, &ExtensionAttribute_t602_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExtensionAttribute_t602)/* instance_size */
	, sizeof (ExtensionAttribute_t602)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Locale
#include "System_Core_Locale.h"
// Metadata Definition Locale
extern TypeInfo Locale_t603_il2cpp_TypeInfo;
// Locale
#include "System_Core_LocaleMethodDeclarations.h"
static const EncodedMethodIndex Locale_t603_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Locale_t603_0_0_0;
extern const Il2CppType Locale_t603_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct Locale_t603;
const Il2CppTypeDefinitionMetadata Locale_t603_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Locale_t603_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2167/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Locale_t603_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Locale"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Locale_t603_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Locale_t603_0_0_0/* byval_arg */
	, &Locale_t603_1_0_0/* this_arg */
	, &Locale_t603_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Locale_t603)/* instance_size */
	, sizeof (Locale_t603)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.KeyBuilder
#include "System_Core_Mono_Security_Cryptography_KeyBuilder.h"
// Metadata Definition Mono.Security.Cryptography.KeyBuilder
extern TypeInfo KeyBuilder_t605_il2cpp_TypeInfo;
// Mono.Security.Cryptography.KeyBuilder
#include "System_Core_Mono_Security_Cryptography_KeyBuilderMethodDeclarations.h"
static const EncodedMethodIndex KeyBuilder_t605_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType KeyBuilder_t605_0_0_0;
extern const Il2CppType KeyBuilder_t605_1_0_0;
struct KeyBuilder_t605;
const Il2CppTypeDefinitionMetadata KeyBuilder_t605_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeyBuilder_t605_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1970/* fieldStart */
	, 2169/* methodStart */
	, -1/* eventStart */
	, 498/* propertyStart */

};
TypeInfo KeyBuilder_t605_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyBuilder"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &KeyBuilder_t605_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyBuilder_t605_0_0_0/* byval_arg */
	, &KeyBuilder_t605_1_0_0/* this_arg */
	, &KeyBuilder_t605_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyBuilder_t605)/* instance_size */
	, sizeof (KeyBuilder_t605)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(KeyBuilder_t605_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.SymmetricTransform
#include "System_Core_Mono_Security_Cryptography_SymmetricTransform.h"
// Metadata Definition Mono.Security.Cryptography.SymmetricTransform
extern TypeInfo SymmetricTransform_t607_il2cpp_TypeInfo;
// Mono.Security.Cryptography.SymmetricTransform
#include "System_Core_Mono_Security_Cryptography_SymmetricTransformMethodDeclarations.h"
static const EncodedMethodIndex SymmetricTransform_t607_VTable[18] = 
{
	120,
	748,
	122,
	123,
	749,
	750,
	751,
	752,
	753,
	750,
	754,
	0,
	755,
	756,
	757,
	758,
	751,
	752,
};
extern const Il2CppType IDisposable_t319_0_0_0;
extern const Il2CppType ICryptoTransform_t616_0_0_0;
static const Il2CppType* SymmetricTransform_t607_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
	&ICryptoTransform_t616_0_0_0,
};
static Il2CppInterfaceOffsetPair SymmetricTransform_t607_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType SymmetricTransform_t607_0_0_0;
extern const Il2CppType SymmetricTransform_t607_1_0_0;
struct SymmetricTransform_t607;
const Il2CppTypeDefinitionMetadata SymmetricTransform_t607_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SymmetricTransform_t607_InterfacesTypeInfos/* implementedInterfaces */
	, SymmetricTransform_t607_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SymmetricTransform_t607_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1971/* fieldStart */
	, 2172/* methodStart */
	, -1/* eventStart */
	, 499/* propertyStart */

};
TypeInfo SymmetricTransform_t607_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "SymmetricTransform"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SymmetricTransform_t607_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SymmetricTransform_t607_0_0_0/* byval_arg */
	, &SymmetricTransform_t607_1_0_0/* this_arg */
	, &SymmetricTransform_t607_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SymmetricTransform_t607)/* instance_size */
	, sizeof (SymmetricTransform_t607)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 20/* method_count */
	, 2/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.Aes
#include "System_Core_System_Security_Cryptography_Aes.h"
// Metadata Definition System.Security.Cryptography.Aes
extern TypeInfo Aes_t608_il2cpp_TypeInfo;
// System.Security.Cryptography.Aes
#include "System_Core_System_Security_Cryptography_AesMethodDeclarations.h"
static const EncodedMethodIndex Aes_t608_VTable[26] = 
{
	120,
	759,
	122,
	123,
	760,
	761,
	762,
	763,
	764,
	765,
	766,
	767,
	768,
	769,
	770,
	771,
	772,
	773,
	774,
	775,
	776,
	0,
	777,
	0,
	0,
	0,
};
static Il2CppInterfaceOffsetPair Aes_t608_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Aes_t608_0_0_0;
extern const Il2CppType Aes_t608_1_0_0;
extern const Il2CppType SymmetricAlgorithm_t606_0_0_0;
struct Aes_t608;
const Il2CppTypeDefinitionMetadata Aes_t608_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Aes_t608_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricAlgorithm_t606_0_0_0/* parent */
	, Aes_t608_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2192/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Aes_t608_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Aes"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Aes_t608_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Aes_t608_0_0_0/* byval_arg */
	, &Aes_t608_1_0_0/* this_arg */
	, &Aes_t608_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Aes_t608)/* instance_size */
	, sizeof (Aes_t608)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.AesManaged
#include "System_Core_System_Security_Cryptography_AesManaged.h"
// Metadata Definition System.Security.Cryptography.AesManaged
extern TypeInfo AesManaged_t609_il2cpp_TypeInfo;
// System.Security.Cryptography.AesManaged
#include "System_Core_System_Security_Cryptography_AesManagedMethodDeclarations.h"
static const EncodedMethodIndex AesManaged_t609_VTable[26] = 
{
	120,
	759,
	122,
	123,
	760,
	778,
	762,
	763,
	764,
	779,
	780,
	781,
	782,
	783,
	784,
	771,
	772,
	773,
	774,
	775,
	785,
	786,
	787,
	788,
	789,
	790,
};
static Il2CppInterfaceOffsetPair AesManaged_t609_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType AesManaged_t609_0_0_0;
extern const Il2CppType AesManaged_t609_1_0_0;
struct AesManaged_t609;
const Il2CppTypeDefinitionMetadata AesManaged_t609_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AesManaged_t609_InterfacesOffsets/* interfaceOffsets */
	, &Aes_t608_0_0_0/* parent */
	, AesManaged_t609_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2193/* methodStart */
	, -1/* eventStart */
	, 501/* propertyStart */

};
TypeInfo AesManaged_t609_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "AesManaged"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AesManaged_t609_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AesManaged_t609_0_0_0/* byval_arg */
	, &AesManaged_t609_1_0_0/* this_arg */
	, &AesManaged_t609_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AesManaged_t609)/* instance_size */
	, sizeof (AesManaged_t609)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.AesTransform
#include "System_Core_System_Security_Cryptography_AesTransform.h"
// Metadata Definition System.Security.Cryptography.AesTransform
extern TypeInfo AesTransform_t611_il2cpp_TypeInfo;
// System.Security.Cryptography.AesTransform
#include "System_Core_System_Security_Cryptography_AesTransformMethodDeclarations.h"
static const EncodedMethodIndex AesTransform_t611_VTable[18] = 
{
	120,
	748,
	122,
	123,
	749,
	750,
	751,
	752,
	753,
	750,
	754,
	791,
	755,
	756,
	757,
	758,
	751,
	752,
};
static Il2CppInterfaceOffsetPair AesTransform_t611_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType AesTransform_t611_0_0_0;
extern const Il2CppType AesTransform_t611_1_0_0;
struct AesTransform_t611;
const Il2CppTypeDefinitionMetadata AesTransform_t611_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AesTransform_t611_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricTransform_t607_0_0_0/* parent */
	, AesTransform_t611_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1983/* fieldStart */
	, 2207/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AesTransform_t611_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "AesTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AesTransform_t611_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AesTransform_t611_0_0_0/* byval_arg */
	, &AesTransform_t611_1_0_0/* this_arg */
	, &AesTransform_t611_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AesTransform_t611)/* instance_size */
	, sizeof (AesTransform_t611)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AesTransform_t611_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
extern TypeInfo U3CPrivateImplementationDetailsU3E_t615_il2cpp_TypeInfo;
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
extern const Il2CppType U24ArrayTypeU24120_t612_0_0_0;
extern const Il2CppType U24ArrayTypeU24256_t613_0_0_0;
extern const Il2CppType U24ArrayTypeU241024_t614_0_0_0;
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t615_il2cpp_TypeInfo__nestedTypes[3] =
{
	&U24ArrayTypeU24120_t612_0_0_0,
	&U24ArrayTypeU24256_t613_0_0_0,
	&U24ArrayTypeU241024_t614_0_0_0,
};
static const EncodedMethodIndex U3CPrivateImplementationDetailsU3E_t615_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t615_0_0_0;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t615_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t615;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t615_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t615_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t615_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1997/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t615_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CPrivateImplementationDetailsU3E_t615_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 517/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t615_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t615_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t615_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t615)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t615)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t615_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$120
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$120
extern TypeInfo U24ArrayTypeU24120_t612_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$120
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeUMethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24120_t612_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U24ArrayTypeU24120_t612_1_0_0;
extern const Il2CppType ValueType_t757_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24120_t612_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t615_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, U24ArrayTypeU24120_t612_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24120_t612_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$120"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24120_t612_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24120_t612_0_0_0/* byval_arg */
	, &U24ArrayTypeU24120_t612_1_0_0/* this_arg */
	, &U24ArrayTypeU24120_t612_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24120_t612_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t612_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t612_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24120_t612)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24120_t612)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24120_t612_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t613_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU_0MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24256_t613_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U24ArrayTypeU24256_t613_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t613_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t615_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, U24ArrayTypeU24256_t613_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24256_t613_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24256_t613_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t613_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t613_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t613_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t613_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t613_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t613_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t613)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t613)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t613_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$1024
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$1024
extern TypeInfo U24ArrayTypeU241024_t614_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$1024
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU_1MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU241024_t614_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U24ArrayTypeU241024_t614_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU241024_t614_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t615_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, U24ArrayTypeU241024_t614_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU241024_t614_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$1024"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU241024_t614_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU241024_t614_0_0_0/* byval_arg */
	, &U24ArrayTypeU241024_t614_1_0_0/* this_arg */
	, &U24ArrayTypeU241024_t614_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU241024_t614_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t614_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t614_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU241024_t614)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU241024_t614)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU241024_t614_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
