﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ComponentModel.DefaultValueAttribute
struct DefaultValueAttribute_t399;
// System.Object
struct Object_t;

// System.Void System.ComponentModel.DefaultValueAttribute::.ctor(System.Object)
extern "C" void DefaultValueAttribute__ctor_m1415 (DefaultValueAttribute_t399 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.DefaultValueAttribute::get_Value()
extern "C" Object_t * DefaultValueAttribute_get_Value_m1416 (DefaultValueAttribute_t399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DefaultValueAttribute::Equals(System.Object)
extern "C" bool DefaultValueAttribute_Equals_m1417 (DefaultValueAttribute_t399 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.DefaultValueAttribute::GetHashCode()
extern "C" int32_t DefaultValueAttribute_GetHashCode_m1418 (DefaultValueAttribute_t399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
