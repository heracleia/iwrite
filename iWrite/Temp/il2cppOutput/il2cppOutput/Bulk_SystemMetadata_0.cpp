﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// <Module>
#include "System_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t381_il2cpp_TypeInfo;
// <Module>
#include "System_U3CModuleU3EMethodDeclarations.h"
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U3CModuleU3E_t381_0_0_0;
extern const Il2CppType U3CModuleU3E_t381_1_0_0;
struct U3CModuleU3E_t381;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t381_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CModuleU3E_t381_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CModuleU3E_t381_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t381_0_0_0/* byval_arg */
	, &U3CModuleU3E_t381_1_0_0/* this_arg */
	, &U3CModuleU3E_t381_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t381)/* instance_size */
	, sizeof (U3CModuleU3E_t381)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Locale
#include "System_Locale.h"
// Metadata Definition Locale
extern TypeInfo Locale_t382_il2cpp_TypeInfo;
// Locale
#include "System_LocaleMethodDeclarations.h"
static const EncodedMethodIndex Locale_t382_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Locale_t382_0_0_0;
extern const Il2CppType Locale_t382_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct Locale_t382;
const Il2CppTypeDefinitionMetadata Locale_t382_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Locale_t382_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1210/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Locale_t382_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Locale"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Locale_t382_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Locale_t382_0_0_0/* byval_arg */
	, &Locale_t382_1_0_0/* this_arg */
	, &Locale_t382_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Locale_t382)/* instance_size */
	, sizeof (Locale_t382)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTODOAttribute
#include "System_System_MonoTODOAttribute.h"
// Metadata Definition System.MonoTODOAttribute
extern TypeInfo MonoTODOAttribute_t383_il2cpp_TypeInfo;
// System.MonoTODOAttribute
#include "System_System_MonoTODOAttributeMethodDeclarations.h"
static const EncodedMethodIndex MonoTODOAttribute_t383_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
extern const Il2CppType _Attribute_t2052_0_0_0;
static Il2CppInterfaceOffsetPair MonoTODOAttribute_t383_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MonoTODOAttribute_t383_0_0_0;
extern const Il2CppType MonoTODOAttribute_t383_1_0_0;
extern const Il2CppType Attribute_t96_0_0_0;
struct MonoTODOAttribute_t383;
const Il2CppTypeDefinitionMetadata MonoTODOAttribute_t383_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoTODOAttribute_t383_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, MonoTODOAttribute_t383_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1164/* fieldStart */
	, 1212/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoTODOAttribute_t383_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTODOAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoTODOAttribute_t383_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 442/* custom_attributes_cache */
	, &MonoTODOAttribute_t383_0_0_0/* byval_arg */
	, &MonoTODOAttribute_t383_1_0_0/* this_arg */
	, &MonoTODOAttribute_t383_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTODOAttribute_t383)/* instance_size */
	, sizeof (MonoTODOAttribute_t383)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.CodeDom.Compiler.GeneratedCodeAttribute
#include "System_System_CodeDom_Compiler_GeneratedCodeAttribute.h"
// Metadata Definition System.CodeDom.Compiler.GeneratedCodeAttribute
extern TypeInfo GeneratedCodeAttribute_t384_il2cpp_TypeInfo;
// System.CodeDom.Compiler.GeneratedCodeAttribute
#include "System_System_CodeDom_Compiler_GeneratedCodeAttributeMethodDeclarations.h"
static const EncodedMethodIndex GeneratedCodeAttribute_t384_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair GeneratedCodeAttribute_t384_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType GeneratedCodeAttribute_t384_0_0_0;
extern const Il2CppType GeneratedCodeAttribute_t384_1_0_0;
struct GeneratedCodeAttribute_t384;
const Il2CppTypeDefinitionMetadata GeneratedCodeAttribute_t384_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GeneratedCodeAttribute_t384_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, GeneratedCodeAttribute_t384_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1165/* fieldStart */
	, 1214/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GeneratedCodeAttribute_t384_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "GeneratedCodeAttribute"/* name */
	, "System.CodeDom.Compiler"/* namespaze */
	, NULL/* methods */
	, &GeneratedCodeAttribute_t384_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 443/* custom_attributes_cache */
	, &GeneratedCodeAttribute_t384_0_0_0/* byval_arg */
	, &GeneratedCodeAttribute_t384_1_0_0/* this_arg */
	, &GeneratedCodeAttribute_t384_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GeneratedCodeAttribute_t384)/* instance_size */
	, sizeof (GeneratedCodeAttribute_t384)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Stack`1
extern TypeInfo Stack_1_t2049_il2cpp_TypeInfo;
extern const Il2CppType Enumerator_t2050_0_0_0;
static const Il2CppType* Stack_1_t2049_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t2050_0_0_0,
};
static const EncodedMethodIndex Stack_1_t2049_VTable[9] = 
{
	120,
	125,
	122,
	123,
	450,
	451,
	452,
	453,
	454,
};
extern const Il2CppType ICollection_t569_0_0_0;
extern const Il2CppType IEnumerable_t302_0_0_0;
extern const Il2CppType IEnumerable_1_t2213_0_0_0;
static const Il2CppType* Stack_1_t2049_InterfacesTypeInfos[] = 
{
	&ICollection_t569_0_0_0,
	&IEnumerable_t302_0_0_0,
	&IEnumerable_1_t2213_0_0_0,
};
static Il2CppInterfaceOffsetPair Stack_1_t2049_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IEnumerable_t302_0_0_0, 7},
	{ &IEnumerable_1_t2213_0_0_0, 8},
};
extern const Il2CppType Enumerator_t2214_0_0_0;
extern const Il2CppRGCTXDefinition Stack_1_t2049_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2552 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 2513 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2553 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2554 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Stack_1_t2049_0_0_0;
extern const Il2CppType Stack_1_t2049_1_0_0;
struct Stack_1_t2049;
const Il2CppTypeDefinitionMetadata Stack_1_t2049_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Stack_1_t2049_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Stack_1_t2049_InterfacesTypeInfos/* implementedInterfaces */
	, Stack_1_t2049_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Stack_1_t2049_VTable/* vtableMethods */
	, Stack_1_t2049_RGCTXData/* rgctxDefinition */
	, 1167/* fieldStart */
	, 1215/* methodStart */
	, -1/* eventStart */
	, 287/* propertyStart */

};
TypeInfo Stack_1_t2049_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Stack`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &Stack_1_t2049_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 444/* custom_attributes_cache */
	, &Stack_1_t2049_0_0_0/* byval_arg */
	, &Stack_1_t2049_1_0_0/* this_arg */
	, &Stack_1_t2049_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 13/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Stack`1/Enumerator
extern TypeInfo Enumerator_t2050_il2cpp_TypeInfo;
static const EncodedMethodIndex Enumerator_t2050_VTable[8] = 
{
	150,
	125,
	151,
	152,
	455,
	456,
	457,
	458,
};
extern const Il2CppType IEnumerator_t279_0_0_0;
extern const Il2CppType IDisposable_t319_0_0_0;
extern const Il2CppType IEnumerator_1_t2215_0_0_0;
static const Il2CppType* Enumerator_t2050_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDisposable_t319_0_0_0,
	&IEnumerator_1_t2215_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t2050_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &IDisposable_t319_0_0_0, 6},
	{ &IEnumerator_1_t2215_0_0_0, 7},
};
extern const Il2CppType Enumerator_t2050_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Enumerator_t2050_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2555 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 2516 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Enumerator_t2050_1_0_0;
extern const Il2CppType ValueType_t757_0_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t2050_DefinitionMetadata = 
{
	&Stack_1_t2049_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t2050_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t2050_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Enumerator_t2050_VTable/* vtableMethods */
	, Enumerator_t2050_RGCTXData/* rgctxDefinition */
	, 1170/* fieldStart */
	, 1224/* methodStart */
	, -1/* eventStart */
	, 289/* propertyStart */

};
TypeInfo Enumerator_t2050_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t2050_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t2050_0_0_0/* byval_arg */
	, &Enumerator_t2050_1_0_0/* this_arg */
	, &Enumerator_t2050_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 14/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.Specialized.HybridDictionary
#include "System_System_Collections_Specialized_HybridDictionary.h"
// Metadata Definition System.Collections.Specialized.HybridDictionary
extern TypeInfo HybridDictionary_t387_il2cpp_TypeInfo;
// System.Collections.Specialized.HybridDictionary
#include "System_System_Collections_Specialized_HybridDictionaryMethodDeclarations.h"
static const EncodedMethodIndex HybridDictionary_t387_VTable[14] = 
{
	120,
	125,
	122,
	123,
	459,
	460,
	461,
	462,
	463,
	464,
	465,
	466,
	467,
	468,
};
extern const Il2CppType IDictionary_t493_0_0_0;
static const Il2CppType* HybridDictionary_t387_InterfacesTypeInfos[] = 
{
	&ICollection_t569_0_0_0,
	&IDictionary_t493_0_0_0,
	&IEnumerable_t302_0_0_0,
};
static Il2CppInterfaceOffsetPair HybridDictionary_t387_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IDictionary_t493_0_0_0, 7},
	{ &IEnumerable_t302_0_0_0, 13},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType HybridDictionary_t387_0_0_0;
extern const Il2CppType HybridDictionary_t387_1_0_0;
struct HybridDictionary_t387;
const Il2CppTypeDefinitionMetadata HybridDictionary_t387_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, HybridDictionary_t387_InterfacesTypeInfos/* implementedInterfaces */
	, HybridDictionary_t387_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HybridDictionary_t387_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1173/* fieldStart */
	, 1229/* methodStart */
	, -1/* eventStart */
	, 291/* propertyStart */

};
TypeInfo HybridDictionary_t387_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "HybridDictionary"/* name */
	, "System.Collections.Specialized"/* namespaze */
	, NULL/* methods */
	, &HybridDictionary_t387_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 445/* custom_attributes_cache */
	, &HybridDictionary_t387_0_0_0/* byval_arg */
	, &HybridDictionary_t387_1_0_0/* this_arg */
	, &HybridDictionary_t387_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HybridDictionary_t387)/* instance_size */
	, sizeof (HybridDictionary_t387)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.Specialized.ListDictionary
#include "System_System_Collections_Specialized_ListDictionary.h"
// Metadata Definition System.Collections.Specialized.ListDictionary
extern TypeInfo ListDictionary_t386_il2cpp_TypeInfo;
// System.Collections.Specialized.ListDictionary
#include "System_System_Collections_Specialized_ListDictionaryMethodDeclarations.h"
extern const Il2CppType DictionaryNode_t388_0_0_0;
extern const Il2CppType DictionaryNodeEnumerator_t389_0_0_0;
static const Il2CppType* ListDictionary_t386_il2cpp_TypeInfo__nestedTypes[2] =
{
	&DictionaryNode_t388_0_0_0,
	&DictionaryNodeEnumerator_t389_0_0_0,
};
static const EncodedMethodIndex ListDictionary_t386_VTable[15] = 
{
	120,
	125,
	122,
	123,
	469,
	470,
	471,
	472,
	473,
	474,
	475,
	476,
	477,
	478,
	479,
};
static const Il2CppType* ListDictionary_t386_InterfacesTypeInfos[] = 
{
	&ICollection_t569_0_0_0,
	&IDictionary_t493_0_0_0,
	&IEnumerable_t302_0_0_0,
};
static Il2CppInterfaceOffsetPair ListDictionary_t386_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IDictionary_t493_0_0_0, 7},
	{ &IEnumerable_t302_0_0_0, 13},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ListDictionary_t386_0_0_0;
extern const Il2CppType ListDictionary_t386_1_0_0;
struct ListDictionary_t386;
const Il2CppTypeDefinitionMetadata ListDictionary_t386_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ListDictionary_t386_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ListDictionary_t386_InterfacesTypeInfos/* implementedInterfaces */
	, ListDictionary_t386_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ListDictionary_t386_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1176/* fieldStart */
	, 1243/* methodStart */
	, -1/* eventStart */
	, 295/* propertyStart */

};
TypeInfo ListDictionary_t386_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ListDictionary"/* name */
	, "System.Collections.Specialized"/* namespaze */
	, NULL/* methods */
	, &ListDictionary_t386_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 446/* custom_attributes_cache */
	, &ListDictionary_t386_0_0_0/* byval_arg */
	, &ListDictionary_t386_1_0_0/* this_arg */
	, &ListDictionary_t386_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ListDictionary_t386)/* instance_size */
	, sizeof (ListDictionary_t386)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 15/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.Specialized.ListDictionary/DictionaryNode
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
// Metadata Definition System.Collections.Specialized.ListDictionary/DictionaryNode
extern TypeInfo DictionaryNode_t388_il2cpp_TypeInfo;
// System.Collections.Specialized.ListDictionary/DictionaryNode
#include "System_System_Collections_Specialized_ListDictionary_DictionMethodDeclarations.h"
static const EncodedMethodIndex DictionaryNode_t388_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType DictionaryNode_t388_1_0_0;
struct DictionaryNode_t388;
const Il2CppTypeDefinitionMetadata DictionaryNode_t388_DefinitionMetadata = 
{
	&ListDictionary_t386_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DictionaryNode_t388_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1180/* fieldStart */
	, 1259/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DictionaryNode_t388_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DictionaryNode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DictionaryNode_t388_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DictionaryNode_t388_0_0_0/* byval_arg */
	, &DictionaryNode_t388_1_0_0/* this_arg */
	, &DictionaryNode_t388_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DictionaryNode_t388)/* instance_size */
	, sizeof (DictionaryNode_t388)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator
#include "System_System_Collections_Specialized_ListDictionary_Diction_0.h"
// Metadata Definition System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator
extern TypeInfo DictionaryNodeEnumerator_t389_il2cpp_TypeInfo;
// System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator
#include "System_System_Collections_Specialized_ListDictionary_Diction_0MethodDeclarations.h"
static const EncodedMethodIndex DictionaryNodeEnumerator_t389_VTable[10] = 
{
	120,
	125,
	122,
	123,
	480,
	481,
	482,
	483,
	484,
	485,
};
extern const Il2CppType IDictionaryEnumerator_t559_0_0_0;
static const Il2CppType* DictionaryNodeEnumerator_t389_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDictionaryEnumerator_t559_0_0_0,
};
static Il2CppInterfaceOffsetPair DictionaryNodeEnumerator_t389_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &IDictionaryEnumerator_t559_0_0_0, 6},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType DictionaryNodeEnumerator_t389_1_0_0;
struct DictionaryNodeEnumerator_t389;
const Il2CppTypeDefinitionMetadata DictionaryNodeEnumerator_t389_DefinitionMetadata = 
{
	&ListDictionary_t386_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, DictionaryNodeEnumerator_t389_InterfacesTypeInfos/* implementedInterfaces */
	, DictionaryNodeEnumerator_t389_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DictionaryNodeEnumerator_t389_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1183/* fieldStart */
	, 1260/* methodStart */
	, -1/* eventStart */
	, 298/* propertyStart */

};
TypeInfo DictionaryNodeEnumerator_t389_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DictionaryNodeEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DictionaryNodeEnumerator_t389_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DictionaryNodeEnumerator_t389_0_0_0/* byval_arg */
	, &DictionaryNodeEnumerator_t389_1_0_0/* this_arg */
	, &DictionaryNodeEnumerator_t389_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DictionaryNodeEnumerator_t389)/* instance_size */
	, sizeof (DictionaryNodeEnumerator_t389)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 5/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Specialized.NameObjectCollectionBase
#include "System_System_Collections_Specialized_NameObjectCollectionBa_2.h"
// Metadata Definition System.Collections.Specialized.NameObjectCollectionBase
extern TypeInfo NameObjectCollectionBase_t392_il2cpp_TypeInfo;
// System.Collections.Specialized.NameObjectCollectionBase
#include "System_System_Collections_Specialized_NameObjectCollectionBa_2MethodDeclarations.h"
extern const Il2CppType _Item_t391_0_0_0;
extern const Il2CppType _KeysEnumerator_t393_0_0_0;
extern const Il2CppType KeysCollection_t394_0_0_0;
static const Il2CppType* NameObjectCollectionBase_t392_il2cpp_TypeInfo__nestedTypes[3] =
{
	&_Item_t391_0_0_0,
	&_KeysEnumerator_t393_0_0_0,
	&KeysCollection_t394_0_0_0,
};
static const EncodedMethodIndex NameObjectCollectionBase_t392_VTable[15] = 
{
	120,
	125,
	122,
	123,
	486,
	487,
	488,
	489,
	490,
	491,
	492,
	490,
	491,
	486,
	489,
};
extern const Il2CppType IDeserializationCallback_t1429_0_0_0;
extern const Il2CppType ISerializable_t1426_0_0_0;
static const Il2CppType* NameObjectCollectionBase_t392_InterfacesTypeInfos[] = 
{
	&ICollection_t569_0_0_0,
	&IDeserializationCallback_t1429_0_0_0,
	&IEnumerable_t302_0_0_0,
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair NameObjectCollectionBase_t392_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IDeserializationCallback_t1429_0_0_0, 7},
	{ &IEnumerable_t302_0_0_0, 8},
	{ &ISerializable_t1426_0_0_0, 9},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType NameObjectCollectionBase_t392_0_0_0;
extern const Il2CppType NameObjectCollectionBase_t392_1_0_0;
struct NameObjectCollectionBase_t392;
const Il2CppTypeDefinitionMetadata NameObjectCollectionBase_t392_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NameObjectCollectionBase_t392_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NameObjectCollectionBase_t392_InterfacesTypeInfos/* implementedInterfaces */
	, NameObjectCollectionBase_t392_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NameObjectCollectionBase_t392_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1187/* fieldStart */
	, 1269/* methodStart */
	, -1/* eventStart */
	, 303/* propertyStart */

};
TypeInfo NameObjectCollectionBase_t392_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "NameObjectCollectionBase"/* name */
	, "System.Collections.Specialized"/* namespaze */
	, NULL/* methods */
	, &NameObjectCollectionBase_t392_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NameObjectCollectionBase_t392_0_0_0/* byval_arg */
	, &NameObjectCollectionBase_t392_1_0_0/* this_arg */
	, &NameObjectCollectionBase_t392_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NameObjectCollectionBase_t392)/* instance_size */
	, sizeof (NameObjectCollectionBase_t392)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 4/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 15/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.Specialized.NameObjectCollectionBase/_Item
#include "System_System_Collections_Specialized_NameObjectCollectionBa.h"
// Metadata Definition System.Collections.Specialized.NameObjectCollectionBase/_Item
extern TypeInfo _Item_t391_il2cpp_TypeInfo;
// System.Collections.Specialized.NameObjectCollectionBase/_Item
#include "System_System_Collections_Specialized_NameObjectCollectionBaMethodDeclarations.h"
static const EncodedMethodIndex _Item_t391_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType _Item_t391_1_0_0;
struct _Item_t391;
const Il2CppTypeDefinitionMetadata _Item_t391_DefinitionMetadata = 
{
	&NameObjectCollectionBase_t392_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, _Item_t391_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1197/* fieldStart */
	, 1285/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Item_t391_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Item"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &_Item_t391_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &_Item_t391_0_0_0/* byval_arg */
	, &_Item_t391_1_0_0/* this_arg */
	, &_Item_t391_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (_Item_t391)/* instance_size */
	, sizeof (_Item_t391)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator
#include "System_System_Collections_Specialized_NameObjectCollectionBa_0.h"
// Metadata Definition System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator
extern TypeInfo _KeysEnumerator_t393_il2cpp_TypeInfo;
// System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator
#include "System_System_Collections_Specialized_NameObjectCollectionBa_0MethodDeclarations.h"
static const EncodedMethodIndex _KeysEnumerator_t393_VTable[7] = 
{
	120,
	125,
	122,
	123,
	493,
	494,
	495,
};
static const Il2CppType* _KeysEnumerator_t393_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair _KeysEnumerator_t393_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType _KeysEnumerator_t393_1_0_0;
struct _KeysEnumerator_t393;
const Il2CppTypeDefinitionMetadata _KeysEnumerator_t393_DefinitionMetadata = 
{
	&NameObjectCollectionBase_t392_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, _KeysEnumerator_t393_InterfacesTypeInfos/* implementedInterfaces */
	, _KeysEnumerator_t393_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, _KeysEnumerator_t393_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1199/* fieldStart */
	, 1286/* methodStart */
	, -1/* eventStart */
	, 307/* propertyStart */

};
TypeInfo _KeysEnumerator_t393_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "_KeysEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &_KeysEnumerator_t393_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &_KeysEnumerator_t393_0_0_0/* byval_arg */
	, &_KeysEnumerator_t393_1_0_0/* this_arg */
	, &_KeysEnumerator_t393_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (_KeysEnumerator_t393)/* instance_size */
	, sizeof (_KeysEnumerator_t393)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056773/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
#include "System_System_Collections_Specialized_NameObjectCollectionBa_1.h"
// Metadata Definition System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
extern TypeInfo KeysCollection_t394_il2cpp_TypeInfo;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
#include "System_System_Collections_Specialized_NameObjectCollectionBa_1MethodDeclarations.h"
static const EncodedMethodIndex KeysCollection_t394_VTable[8] = 
{
	120,
	125,
	122,
	123,
	496,
	497,
	498,
	499,
};
static const Il2CppType* KeysCollection_t394_InterfacesTypeInfos[] = 
{
	&ICollection_t569_0_0_0,
	&IEnumerable_t302_0_0_0,
};
static Il2CppInterfaceOffsetPair KeysCollection_t394_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IEnumerable_t302_0_0_0, 7},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType KeysCollection_t394_1_0_0;
struct KeysCollection_t394;
const Il2CppTypeDefinitionMetadata KeysCollection_t394_DefinitionMetadata = 
{
	&NameObjectCollectionBase_t392_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, KeysCollection_t394_InterfacesTypeInfos/* implementedInterfaces */
	, KeysCollection_t394_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeysCollection_t394_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1201/* fieldStart */
	, 1290/* methodStart */
	, -1/* eventStart */
	, 308/* propertyStart */

};
TypeInfo KeysCollection_t394_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeysCollection"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &KeysCollection_t394_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 448/* custom_attributes_cache */
	, &KeysCollection_t394_0_0_0/* byval_arg */
	, &KeysCollection_t394_1_0_0/* this_arg */
	, &KeysCollection_t394_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeysCollection_t394)/* instance_size */
	, sizeof (KeysCollection_t394)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Specialized.NameValueCollection
#include "System_System_Collections_Specialized_NameValueCollection.h"
// Metadata Definition System.Collections.Specialized.NameValueCollection
extern TypeInfo NameValueCollection_t398_il2cpp_TypeInfo;
// System.Collections.Specialized.NameValueCollection
#include "System_System_Collections_Specialized_NameValueCollectionMethodDeclarations.h"
static const EncodedMethodIndex NameValueCollection_t398_VTable[18] = 
{
	120,
	125,
	122,
	123,
	486,
	487,
	488,
	489,
	490,
	491,
	492,
	490,
	491,
	486,
	489,
	500,
	501,
	502,
};
static Il2CppInterfaceOffsetPair NameValueCollection_t398_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IDeserializationCallback_t1429_0_0_0, 7},
	{ &IEnumerable_t302_0_0_0, 8},
	{ &ISerializable_t1426_0_0_0, 9},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType NameValueCollection_t398_0_0_0;
extern const Il2CppType NameValueCollection_t398_1_0_0;
struct NameValueCollection_t398;
const Il2CppTypeDefinitionMetadata NameValueCollection_t398_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NameValueCollection_t398_InterfacesOffsets/* interfaceOffsets */
	, &NameObjectCollectionBase_t392_0_0_0/* parent */
	, NameValueCollection_t398_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1202/* fieldStart */
	, 1295/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NameValueCollection_t398_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "NameValueCollection"/* name */
	, "System.Collections.Specialized"/* namespaze */
	, NULL/* methods */
	, &NameValueCollection_t398_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 449/* custom_attributes_cache */
	, &NameValueCollection_t398_0_0_0/* byval_arg */
	, &NameValueCollection_t398_1_0_0/* this_arg */
	, &NameValueCollection_t398_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NameValueCollection_t398)/* instance_size */
	, sizeof (NameValueCollection_t398)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.ComponentModel.DefaultValueAttribute
#include "System_System_ComponentModel_DefaultValueAttribute.h"
// Metadata Definition System.ComponentModel.DefaultValueAttribute
extern TypeInfo DefaultValueAttribute_t399_il2cpp_TypeInfo;
// System.ComponentModel.DefaultValueAttribute
#include "System_System_ComponentModel_DefaultValueAttributeMethodDeclarations.h"
static const EncodedMethodIndex DefaultValueAttribute_t399_VTable[5] = 
{
	503,
	125,
	504,
	123,
	505,
};
static Il2CppInterfaceOffsetPair DefaultValueAttribute_t399_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType DefaultValueAttribute_t399_0_0_0;
extern const Il2CppType DefaultValueAttribute_t399_1_0_0;
struct DefaultValueAttribute_t399;
const Il2CppTypeDefinitionMetadata DefaultValueAttribute_t399_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultValueAttribute_t399_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, DefaultValueAttribute_t399_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1204/* fieldStart */
	, 1302/* methodStart */
	, -1/* eventStart */
	, 310/* propertyStart */

};
TypeInfo DefaultValueAttribute_t399_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultValueAttribute"/* name */
	, "System.ComponentModel"/* namespaze */
	, NULL/* methods */
	, &DefaultValueAttribute_t399_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 450/* custom_attributes_cache */
	, &DefaultValueAttribute_t399_0_0_0/* byval_arg */
	, &DefaultValueAttribute_t399_1_0_0/* this_arg */
	, &DefaultValueAttribute_t399_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultValueAttribute_t399)/* instance_size */
	, sizeof (DefaultValueAttribute_t399)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttribute.h"
// Metadata Definition System.ComponentModel.EditorBrowsableAttribute
extern TypeInfo EditorBrowsableAttribute_t400_il2cpp_TypeInfo;
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttributeMethodDeclarations.h"
static const EncodedMethodIndex EditorBrowsableAttribute_t400_VTable[4] = 
{
	506,
	125,
	507,
	123,
};
static Il2CppInterfaceOffsetPair EditorBrowsableAttribute_t400_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType EditorBrowsableAttribute_t400_0_0_0;
extern const Il2CppType EditorBrowsableAttribute_t400_1_0_0;
struct EditorBrowsableAttribute_t400;
const Il2CppTypeDefinitionMetadata EditorBrowsableAttribute_t400_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EditorBrowsableAttribute_t400_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, EditorBrowsableAttribute_t400_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1205/* fieldStart */
	, 1306/* methodStart */
	, -1/* eventStart */
	, 311/* propertyStart */

};
TypeInfo EditorBrowsableAttribute_t400_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "EditorBrowsableAttribute"/* name */
	, "System.ComponentModel"/* namespaze */
	, NULL/* methods */
	, &EditorBrowsableAttribute_t400_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 451/* custom_attributes_cache */
	, &EditorBrowsableAttribute_t400_0_0_0/* byval_arg */
	, &EditorBrowsableAttribute_t400_1_0_0/* this_arg */
	, &EditorBrowsableAttribute_t400_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EditorBrowsableAttribute_t400)/* instance_size */
	, sizeof (EditorBrowsableAttribute_t400)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"
// Metadata Definition System.ComponentModel.EditorBrowsableState
extern TypeInfo EditorBrowsableState_t401_il2cpp_TypeInfo;
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableStateMethodDeclarations.h"
static const EncodedMethodIndex EditorBrowsableState_t401_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
extern const Il2CppType IFormattable_t1406_0_0_0;
extern const Il2CppType IConvertible_t1409_0_0_0;
extern const Il2CppType IComparable_t1408_0_0_0;
static Il2CppInterfaceOffsetPair EditorBrowsableState_t401_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType EditorBrowsableState_t401_0_0_0;
extern const Il2CppType EditorBrowsableState_t401_1_0_0;
extern const Il2CppType Enum_t305_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t320_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata EditorBrowsableState_t401_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EditorBrowsableState_t401_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, EditorBrowsableState_t401_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1206/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EditorBrowsableState_t401_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "EditorBrowsableState"/* name */
	, "System.ComponentModel"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EditorBrowsableState_t401_0_0_0/* byval_arg */
	, &EditorBrowsableState_t401_1_0_0/* this_arg */
	, &EditorBrowsableState_t401_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EditorBrowsableState_t401)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EditorBrowsableState_t401)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.ComponentModel.TypeConverter
#include "System_System_ComponentModel_TypeConverter.h"
// Metadata Definition System.ComponentModel.TypeConverter
extern TypeInfo TypeConverter_t402_il2cpp_TypeInfo;
// System.ComponentModel.TypeConverter
#include "System_System_ComponentModel_TypeConverterMethodDeclarations.h"
static const EncodedMethodIndex TypeConverter_t402_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType TypeConverter_t402_0_0_0;
extern const Il2CppType TypeConverter_t402_1_0_0;
struct TypeConverter_t402;
const Il2CppTypeDefinitionMetadata TypeConverter_t402_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeConverter_t402_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeConverter_t402_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeConverter"/* name */
	, "System.ComponentModel"/* namespaze */
	, NULL/* methods */
	, &TypeConverter_t402_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 452/* custom_attributes_cache */
	, &TypeConverter_t402_0_0_0/* byval_arg */
	, &TypeConverter_t402_1_0_0/* this_arg */
	, &TypeConverter_t402_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeConverter_t402)/* instance_size */
	, sizeof (TypeConverter_t402)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ComponentModel.TypeConverterAttribute
#include "System_System_ComponentModel_TypeConverterAttribute.h"
// Metadata Definition System.ComponentModel.TypeConverterAttribute
extern TypeInfo TypeConverterAttribute_t403_il2cpp_TypeInfo;
// System.ComponentModel.TypeConverterAttribute
#include "System_System_ComponentModel_TypeConverterAttributeMethodDeclarations.h"
static const EncodedMethodIndex TypeConverterAttribute_t403_VTable[4] = 
{
	508,
	125,
	509,
	123,
};
static Il2CppInterfaceOffsetPair TypeConverterAttribute_t403_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType TypeConverterAttribute_t403_0_0_0;
extern const Il2CppType TypeConverterAttribute_t403_1_0_0;
struct TypeConverterAttribute_t403;
const Il2CppTypeDefinitionMetadata TypeConverterAttribute_t403_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeConverterAttribute_t403_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, TypeConverterAttribute_t403_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1210/* fieldStart */
	, 1310/* methodStart */
	, -1/* eventStart */
	, 312/* propertyStart */

};
TypeInfo TypeConverterAttribute_t403_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeConverterAttribute"/* name */
	, "System.ComponentModel"/* namespaze */
	, NULL/* methods */
	, &TypeConverterAttribute_t403_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 453/* custom_attributes_cache */
	, &TypeConverterAttribute_t403_0_0_0/* byval_arg */
	, &TypeConverterAttribute_t403_1_0_0/* this_arg */
	, &TypeConverterAttribute_t403_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeConverterAttribute_t403)/* instance_size */
	, sizeof (TypeConverterAttribute_t403)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TypeConverterAttribute_t403_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.Security.AuthenticationLevel
#include "System_System_Net_Security_AuthenticationLevel.h"
// Metadata Definition System.Net.Security.AuthenticationLevel
extern TypeInfo AuthenticationLevel_t404_il2cpp_TypeInfo;
// System.Net.Security.AuthenticationLevel
#include "System_System_Net_Security_AuthenticationLevelMethodDeclarations.h"
static const EncodedMethodIndex AuthenticationLevel_t404_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair AuthenticationLevel_t404_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType AuthenticationLevel_t404_0_0_0;
extern const Il2CppType AuthenticationLevel_t404_1_0_0;
const Il2CppTypeDefinitionMetadata AuthenticationLevel_t404_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AuthenticationLevel_t404_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, AuthenticationLevel_t404_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1212/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AuthenticationLevel_t404_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AuthenticationLevel"/* name */
	, "System.Net.Security"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AuthenticationLevel_t404_0_0_0/* byval_arg */
	, &AuthenticationLevel_t404_1_0_0/* this_arg */
	, &AuthenticationLevel_t404_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AuthenticationLevel_t404)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AuthenticationLevel_t404)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Net.Security.SslPolicyErrors
#include "System_System_Net_Security_SslPolicyErrors.h"
// Metadata Definition System.Net.Security.SslPolicyErrors
extern TypeInfo SslPolicyErrors_t405_il2cpp_TypeInfo;
// System.Net.Security.SslPolicyErrors
#include "System_System_Net_Security_SslPolicyErrorsMethodDeclarations.h"
static const EncodedMethodIndex SslPolicyErrors_t405_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair SslPolicyErrors_t405_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType SslPolicyErrors_t405_0_0_0;
extern const Il2CppType SslPolicyErrors_t405_1_0_0;
const Il2CppTypeDefinitionMetadata SslPolicyErrors_t405_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SslPolicyErrors_t405_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, SslPolicyErrors_t405_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1216/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SslPolicyErrors_t405_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "SslPolicyErrors"/* name */
	, "System.Net.Security"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 454/* custom_attributes_cache */
	, &SslPolicyErrors_t405_0_0_0/* byval_arg */
	, &SslPolicyErrors_t405_1_0_0/* this_arg */
	, &SslPolicyErrors_t405_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SslPolicyErrors_t405)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SslPolicyErrors_t405)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Net.Sockets.AddressFamily
#include "System_System_Net_Sockets_AddressFamily.h"
// Metadata Definition System.Net.Sockets.AddressFamily
extern TypeInfo AddressFamily_t406_il2cpp_TypeInfo;
// System.Net.Sockets.AddressFamily
#include "System_System_Net_Sockets_AddressFamilyMethodDeclarations.h"
static const EncodedMethodIndex AddressFamily_t406_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair AddressFamily_t406_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType AddressFamily_t406_0_0_0;
extern const Il2CppType AddressFamily_t406_1_0_0;
const Il2CppTypeDefinitionMetadata AddressFamily_t406_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AddressFamily_t406_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, AddressFamily_t406_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1221/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AddressFamily_t406_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AddressFamily"/* name */
	, "System.Net.Sockets"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AddressFamily_t406_0_0_0/* byval_arg */
	, &AddressFamily_t406_1_0_0/* this_arg */
	, &AddressFamily_t406_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AddressFamily_t406)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AddressFamily_t406)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 32/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Net.DefaultCertificatePolicy
#include "System_System_Net_DefaultCertificatePolicy.h"
// Metadata Definition System.Net.DefaultCertificatePolicy
extern TypeInfo DefaultCertificatePolicy_t407_il2cpp_TypeInfo;
// System.Net.DefaultCertificatePolicy
#include "System_System_Net_DefaultCertificatePolicyMethodDeclarations.h"
static const EncodedMethodIndex DefaultCertificatePolicy_t407_VTable[5] = 
{
	120,
	125,
	122,
	123,
	510,
};
extern const Il2CppType ICertificatePolicy_t428_0_0_0;
static const Il2CppType* DefaultCertificatePolicy_t407_InterfacesTypeInfos[] = 
{
	&ICertificatePolicy_t428_0_0_0,
};
static Il2CppInterfaceOffsetPair DefaultCertificatePolicy_t407_InterfacesOffsets[] = 
{
	{ &ICertificatePolicy_t428_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType DefaultCertificatePolicy_t407_0_0_0;
extern const Il2CppType DefaultCertificatePolicy_t407_1_0_0;
struct DefaultCertificatePolicy_t407;
const Il2CppTypeDefinitionMetadata DefaultCertificatePolicy_t407_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DefaultCertificatePolicy_t407_InterfacesTypeInfos/* implementedInterfaces */
	, DefaultCertificatePolicy_t407_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DefaultCertificatePolicy_t407_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1316/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DefaultCertificatePolicy_t407_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultCertificatePolicy"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &DefaultCertificatePolicy_t407_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultCertificatePolicy_t407_0_0_0/* byval_arg */
	, &DefaultCertificatePolicy_t407_1_0_0/* this_arg */
	, &DefaultCertificatePolicy_t407_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultCertificatePolicy_t407)/* instance_size */
	, sizeof (DefaultCertificatePolicy_t407)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.FileWebRequest
#include "System_System_Net_FileWebRequest.h"
// Metadata Definition System.Net.FileWebRequest
extern TypeInfo FileWebRequest_t410_il2cpp_TypeInfo;
// System.Net.FileWebRequest
#include "System_System_Net_FileWebRequestMethodDeclarations.h"
static const EncodedMethodIndex FileWebRequest_t410_VTable[6] = 
{
	120,
	125,
	122,
	123,
	511,
	512,
};
static const Il2CppType* FileWebRequest_t410_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair FileWebRequest_t410_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType FileWebRequest_t410_0_0_0;
extern const Il2CppType FileWebRequest_t410_1_0_0;
extern const Il2CppType WebRequest_t411_0_0_0;
struct FileWebRequest_t410;
const Il2CppTypeDefinitionMetadata FileWebRequest_t410_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FileWebRequest_t410_InterfacesTypeInfos/* implementedInterfaces */
	, FileWebRequest_t410_InterfacesOffsets/* interfaceOffsets */
	, &WebRequest_t411_0_0_0/* parent */
	, FileWebRequest_t410_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1253/* fieldStart */
	, 1318/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileWebRequest_t410_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileWebRequest"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &FileWebRequest_t410_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FileWebRequest_t410_0_0_0/* byval_arg */
	, &FileWebRequest_t410_1_0_0/* this_arg */
	, &FileWebRequest_t410_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileWebRequest_t410)/* instance_size */
	, sizeof (FileWebRequest_t410)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.FileWebRequestCreator
#include "System_System_Net_FileWebRequestCreator.h"
// Metadata Definition System.Net.FileWebRequestCreator
extern TypeInfo FileWebRequestCreator_t412_il2cpp_TypeInfo;
// System.Net.FileWebRequestCreator
#include "System_System_Net_FileWebRequestCreatorMethodDeclarations.h"
static const EncodedMethodIndex FileWebRequestCreator_t412_VTable[5] = 
{
	120,
	125,
	122,
	123,
	513,
};
extern const Il2CppType IWebRequestCreate_t2051_0_0_0;
static const Il2CppType* FileWebRequestCreator_t412_InterfacesTypeInfos[] = 
{
	&IWebRequestCreate_t2051_0_0_0,
};
static Il2CppInterfaceOffsetPair FileWebRequestCreator_t412_InterfacesOffsets[] = 
{
	{ &IWebRequestCreate_t2051_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType FileWebRequestCreator_t412_0_0_0;
extern const Il2CppType FileWebRequestCreator_t412_1_0_0;
struct FileWebRequestCreator_t412;
const Il2CppTypeDefinitionMetadata FileWebRequestCreator_t412_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FileWebRequestCreator_t412_InterfacesTypeInfos/* implementedInterfaces */
	, FileWebRequestCreator_t412_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FileWebRequestCreator_t412_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1322/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileWebRequestCreator_t412_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileWebRequestCreator"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &FileWebRequestCreator_t412_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FileWebRequestCreator_t412_0_0_0/* byval_arg */
	, &FileWebRequestCreator_t412_1_0_0/* this_arg */
	, &FileWebRequestCreator_t412_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileWebRequestCreator_t412)/* instance_size */
	, sizeof (FileWebRequestCreator_t412)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.FtpRequestCreator
#include "System_System_Net_FtpRequestCreator.h"
// Metadata Definition System.Net.FtpRequestCreator
extern TypeInfo FtpRequestCreator_t413_il2cpp_TypeInfo;
// System.Net.FtpRequestCreator
#include "System_System_Net_FtpRequestCreatorMethodDeclarations.h"
static const EncodedMethodIndex FtpRequestCreator_t413_VTable[5] = 
{
	120,
	125,
	122,
	123,
	514,
};
static const Il2CppType* FtpRequestCreator_t413_InterfacesTypeInfos[] = 
{
	&IWebRequestCreate_t2051_0_0_0,
};
static Il2CppInterfaceOffsetPair FtpRequestCreator_t413_InterfacesOffsets[] = 
{
	{ &IWebRequestCreate_t2051_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType FtpRequestCreator_t413_0_0_0;
extern const Il2CppType FtpRequestCreator_t413_1_0_0;
struct FtpRequestCreator_t413;
const Il2CppTypeDefinitionMetadata FtpRequestCreator_t413_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FtpRequestCreator_t413_InterfacesTypeInfos/* implementedInterfaces */
	, FtpRequestCreator_t413_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FtpRequestCreator_t413_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1324/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FtpRequestCreator_t413_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FtpRequestCreator"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &FtpRequestCreator_t413_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FtpRequestCreator_t413_0_0_0/* byval_arg */
	, &FtpRequestCreator_t413_1_0_0/* this_arg */
	, &FtpRequestCreator_t413_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FtpRequestCreator_t413)/* instance_size */
	, sizeof (FtpRequestCreator_t413)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.FtpWebRequest
#include "System_System_Net_FtpWebRequest.h"
// Metadata Definition System.Net.FtpWebRequest
extern TypeInfo FtpWebRequest_t415_il2cpp_TypeInfo;
// System.Net.FtpWebRequest
#include "System_System_Net_FtpWebRequestMethodDeclarations.h"
static const EncodedMethodIndex FtpWebRequest_t415_VTable[6] = 
{
	120,
	125,
	122,
	123,
	515,
	516,
};
static Il2CppInterfaceOffsetPair FtpWebRequest_t415_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType FtpWebRequest_t415_0_0_0;
extern const Il2CppType FtpWebRequest_t415_1_0_0;
struct FtpWebRequest_t415;
const Il2CppTypeDefinitionMetadata FtpWebRequest_t415_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FtpWebRequest_t415_InterfacesOffsets/* interfaceOffsets */
	, &WebRequest_t411_0_0_0/* parent */
	, FtpWebRequest_t415_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1262/* fieldStart */
	, 1326/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FtpWebRequest_t415_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FtpWebRequest"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &FtpWebRequest_t415_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FtpWebRequest_t415_0_0_0/* byval_arg */
	, &FtpWebRequest_t415_1_0_0/* this_arg */
	, &FtpWebRequest_t415_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FtpWebRequest_t415)/* instance_size */
	, sizeof (FtpWebRequest_t415)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(FtpWebRequest_t415_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.GlobalProxySelection
#include "System_System_Net_GlobalProxySelection.h"
// Metadata Definition System.Net.GlobalProxySelection
extern TypeInfo GlobalProxySelection_t416_il2cpp_TypeInfo;
// System.Net.GlobalProxySelection
#include "System_System_Net_GlobalProxySelectionMethodDeclarations.h"
static const EncodedMethodIndex GlobalProxySelection_t416_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType GlobalProxySelection_t416_0_0_0;
extern const Il2CppType GlobalProxySelection_t416_1_0_0;
struct GlobalProxySelection_t416;
const Il2CppTypeDefinitionMetadata GlobalProxySelection_t416_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GlobalProxySelection_t416_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1329/* methodStart */
	, -1/* eventStart */
	, 313/* propertyStart */

};
TypeInfo GlobalProxySelection_t416_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "GlobalProxySelection"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &GlobalProxySelection_t416_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 458/* custom_attributes_cache */
	, &GlobalProxySelection_t416_0_0_0/* byval_arg */
	, &GlobalProxySelection_t416_1_0_0/* this_arg */
	, &GlobalProxySelection_t416_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GlobalProxySelection_t416)/* instance_size */
	, sizeof (GlobalProxySelection_t416)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.HttpRequestCreator
#include "System_System_Net_HttpRequestCreator.h"
// Metadata Definition System.Net.HttpRequestCreator
extern TypeInfo HttpRequestCreator_t417_il2cpp_TypeInfo;
// System.Net.HttpRequestCreator
#include "System_System_Net_HttpRequestCreatorMethodDeclarations.h"
static const EncodedMethodIndex HttpRequestCreator_t417_VTable[5] = 
{
	120,
	125,
	122,
	123,
	517,
};
static const Il2CppType* HttpRequestCreator_t417_InterfacesTypeInfos[] = 
{
	&IWebRequestCreate_t2051_0_0_0,
};
static Il2CppInterfaceOffsetPair HttpRequestCreator_t417_InterfacesOffsets[] = 
{
	{ &IWebRequestCreate_t2051_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType HttpRequestCreator_t417_0_0_0;
extern const Il2CppType HttpRequestCreator_t417_1_0_0;
struct HttpRequestCreator_t417;
const Il2CppTypeDefinitionMetadata HttpRequestCreator_t417_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, HttpRequestCreator_t417_InterfacesTypeInfos/* implementedInterfaces */
	, HttpRequestCreator_t417_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HttpRequestCreator_t417_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1330/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HttpRequestCreator_t417_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "HttpRequestCreator"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &HttpRequestCreator_t417_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HttpRequestCreator_t417_0_0_0/* byval_arg */
	, &HttpRequestCreator_t417_1_0_0/* this_arg */
	, &HttpRequestCreator_t417_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HttpRequestCreator_t417)/* instance_size */
	, sizeof (HttpRequestCreator_t417)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.HttpVersion
#include "System_System_Net_HttpVersion.h"
// Metadata Definition System.Net.HttpVersion
extern TypeInfo HttpVersion_t419_il2cpp_TypeInfo;
// System.Net.HttpVersion
#include "System_System_Net_HttpVersionMethodDeclarations.h"
static const EncodedMethodIndex HttpVersion_t419_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType HttpVersion_t419_0_0_0;
extern const Il2CppType HttpVersion_t419_1_0_0;
struct HttpVersion_t419;
const Il2CppTypeDefinitionMetadata HttpVersion_t419_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HttpVersion_t419_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1273/* fieldStart */
	, 1332/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HttpVersion_t419_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "HttpVersion"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &HttpVersion_t419_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HttpVersion_t419_0_0_0/* byval_arg */
	, &HttpVersion_t419_1_0_0/* this_arg */
	, &HttpVersion_t419_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HttpVersion_t419)/* instance_size */
	, sizeof (HttpVersion_t419)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(HttpVersion_t419_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.HttpWebRequest
#include "System_System_Net_HttpWebRequest.h"
// Metadata Definition System.Net.HttpWebRequest
extern TypeInfo HttpWebRequest_t422_il2cpp_TypeInfo;
// System.Net.HttpWebRequest
#include "System_System_Net_HttpWebRequestMethodDeclarations.h"
static const EncodedMethodIndex HttpWebRequest_t422_VTable[6] = 
{
	120,
	125,
	122,
	123,
	518,
	519,
};
static const Il2CppType* HttpWebRequest_t422_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair HttpWebRequest_t422_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType HttpWebRequest_t422_0_0_0;
extern const Il2CppType HttpWebRequest_t422_1_0_0;
struct HttpWebRequest_t422;
const Il2CppTypeDefinitionMetadata HttpWebRequest_t422_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, HttpWebRequest_t422_InterfacesTypeInfos/* implementedInterfaces */
	, HttpWebRequest_t422_InterfacesOffsets/* interfaceOffsets */
	, &WebRequest_t411_0_0_0/* parent */
	, HttpWebRequest_t422_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1275/* fieldStart */
	, 1333/* methodStart */
	, -1/* eventStart */
	, 314/* propertyStart */

};
TypeInfo HttpWebRequest_t422_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "HttpWebRequest"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &HttpWebRequest_t422_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HttpWebRequest_t422_0_0_0/* byval_arg */
	, &HttpWebRequest_t422_1_0_0/* this_arg */
	, &HttpWebRequest_t422_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HttpWebRequest_t422)/* instance_size */
	, sizeof (HttpWebRequest_t422)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(HttpWebRequest_t422_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8193/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 2/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Net.ICertificatePolicy
extern TypeInfo ICertificatePolicy_t428_il2cpp_TypeInfo;
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ICertificatePolicy_t428_1_0_0;
struct ICertificatePolicy_t428;
const Il2CppTypeDefinitionMetadata ICertificatePolicy_t428_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1341/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICertificatePolicy_t428_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICertificatePolicy"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &ICertificatePolicy_t428_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICertificatePolicy_t428_0_0_0/* byval_arg */
	, &ICertificatePolicy_t428_1_0_0/* this_arg */
	, &ICertificatePolicy_t428_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Net.ICredentials
extern TypeInfo ICredentials_t432_il2cpp_TypeInfo;
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ICredentials_t432_0_0_0;
extern const Il2CppType ICredentials_t432_1_0_0;
struct ICredentials_t432;
const Il2CppTypeDefinitionMetadata ICredentials_t432_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICredentials_t432_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICredentials"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &ICredentials_t432_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICredentials_t432_0_0_0/* byval_arg */
	, &ICredentials_t432_1_0_0/* this_arg */
	, &ICredentials_t432_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.IPAddress
#include "System_System_Net_IPAddress.h"
// Metadata Definition System.Net.IPAddress
extern TypeInfo IPAddress_t424_il2cpp_TypeInfo;
// System.Net.IPAddress
#include "System_System_Net_IPAddressMethodDeclarations.h"
static const EncodedMethodIndex IPAddress_t424_VTable[4] = 
{
	520,
	125,
	521,
	522,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IPAddress_t424_0_0_0;
extern const Il2CppType IPAddress_t424_1_0_0;
struct IPAddress_t424;
const Il2CppTypeDefinitionMetadata IPAddress_t424_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IPAddress_t424_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1299/* fieldStart */
	, 1342/* methodStart */
	, -1/* eventStart */
	, 316/* propertyStart */

};
TypeInfo IPAddress_t424_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPAddress"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &IPAddress_t424_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IPAddress_t424_0_0_0/* byval_arg */
	, &IPAddress_t424_1_0_0/* this_arg */
	, &IPAddress_t424_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IPAddress_t424)/* instance_size */
	, sizeof (IPAddress_t424)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(IPAddress_t424_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 3/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.IPv6Address
#include "System_System_Net_IPv6Address.h"
// Metadata Definition System.Net.IPv6Address
extern TypeInfo IPv6Address_t425_il2cpp_TypeInfo;
// System.Net.IPv6Address
#include "System_System_Net_IPv6AddressMethodDeclarations.h"
static const EncodedMethodIndex IPv6Address_t425_VTable[4] = 
{
	523,
	125,
	524,
	525,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IPv6Address_t425_0_0_0;
extern const Il2CppType IPv6Address_t425_1_0_0;
struct IPv6Address_t425;
const Il2CppTypeDefinitionMetadata IPv6Address_t425_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IPv6Address_t425_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1310/* fieldStart */
	, 1361/* methodStart */
	, -1/* eventStart */
	, 319/* propertyStart */

};
TypeInfo IPv6Address_t425_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPv6Address"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &IPv6Address_t425_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 460/* custom_attributes_cache */
	, &IPv6Address_t425_0_0_0/* byval_arg */
	, &IPv6Address_t425_1_0_0/* this_arg */
	, &IPv6Address_t425_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IPv6Address_t425)/* instance_size */
	, sizeof (IPv6Address_t425)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(IPv6Address_t425_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Net.IWebProxy
extern TypeInfo IWebProxy_t409_il2cpp_TypeInfo;
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IWebProxy_t409_0_0_0;
extern const Il2CppType IWebProxy_t409_1_0_0;
struct IWebProxy_t409;
const Il2CppTypeDefinitionMetadata IWebProxy_t409_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1382/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IWebProxy_t409_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IWebProxy"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &IWebProxy_t409_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IWebProxy_t409_0_0_0/* byval_arg */
	, &IWebProxy_t409_1_0_0/* this_arg */
	, &IWebProxy_t409_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Net.IWebRequestCreate
extern TypeInfo IWebRequestCreate_t2051_il2cpp_TypeInfo;
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IWebRequestCreate_t2051_1_0_0;
struct IWebRequestCreate_t2051;
const Il2CppTypeDefinitionMetadata IWebRequestCreate_t2051_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IWebRequestCreate_t2051_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IWebRequestCreate"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &IWebRequestCreate_t2051_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IWebRequestCreate_t2051_0_0_0/* byval_arg */
	, &IWebRequestCreate_t2051_1_0_0/* this_arg */
	, &IWebRequestCreate_t2051_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.SecurityProtocolType
#include "System_System_Net_SecurityProtocolType.h"
// Metadata Definition System.Net.SecurityProtocolType
extern TypeInfo SecurityProtocolType_t426_il2cpp_TypeInfo;
// System.Net.SecurityProtocolType
#include "System_System_Net_SecurityProtocolTypeMethodDeclarations.h"
static const EncodedMethodIndex SecurityProtocolType_t426_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair SecurityProtocolType_t426_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType SecurityProtocolType_t426_0_0_0;
extern const Il2CppType SecurityProtocolType_t426_1_0_0;
const Il2CppTypeDefinitionMetadata SecurityProtocolType_t426_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SecurityProtocolType_t426_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, SecurityProtocolType_t426_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1315/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SecurityProtocolType_t426_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityProtocolType"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 461/* custom_attributes_cache */
	, &SecurityProtocolType_t426_0_0_0/* byval_arg */
	, &SecurityProtocolType_t426_1_0_0/* this_arg */
	, &SecurityProtocolType_t426_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityProtocolType_t426)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SecurityProtocolType_t426)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Net.ServicePoint
#include "System_System_Net_ServicePoint.h"
// Metadata Definition System.Net.ServicePoint
extern TypeInfo ServicePoint_t421_il2cpp_TypeInfo;
// System.Net.ServicePoint
#include "System_System_Net_ServicePointMethodDeclarations.h"
static const EncodedMethodIndex ServicePoint_t421_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ServicePoint_t421_0_0_0;
extern const Il2CppType ServicePoint_t421_1_0_0;
struct ServicePoint_t421;
const Il2CppTypeDefinitionMetadata ServicePoint_t421_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ServicePoint_t421_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1318/* fieldStart */
	, 1384/* methodStart */
	, -1/* eventStart */
	, 321/* propertyStart */

};
TypeInfo ServicePoint_t421_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ServicePoint"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &ServicePoint_t421_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ServicePoint_t421_0_0_0/* byval_arg */
	, &ServicePoint_t421_1_0_0/* this_arg */
	, &ServicePoint_t421_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ServicePoint_t421)/* instance_size */
	, sizeof (ServicePoint_t421)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 9/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.ServicePointManager
#include "System_System_Net_ServicePointManager.h"
// Metadata Definition System.Net.ServicePointManager
extern TypeInfo ServicePointManager_t429_il2cpp_TypeInfo;
// System.Net.ServicePointManager
#include "System_System_Net_ServicePointManagerMethodDeclarations.h"
extern const Il2CppType SPKey_t427_0_0_0;
static const Il2CppType* ServicePointManager_t429_il2cpp_TypeInfo__nestedTypes[1] =
{
	&SPKey_t427_0_0_0,
};
static const EncodedMethodIndex ServicePointManager_t429_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ServicePointManager_t429_0_0_0;
extern const Il2CppType ServicePointManager_t429_1_0_0;
struct ServicePointManager_t429;
const Il2CppTypeDefinitionMetadata ServicePointManager_t429_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ServicePointManager_t429_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ServicePointManager_t429_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1329/* fieldStart */
	, 1395/* methodStart */
	, -1/* eventStart */
	, 330/* propertyStart */

};
TypeInfo ServicePointManager_t429_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ServicePointManager"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &ServicePointManager_t429_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ServicePointManager_t429_0_0_0/* byval_arg */
	, &ServicePointManager_t429_1_0_0/* this_arg */
	, &ServicePointManager_t429_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ServicePointManager_t429)/* instance_size */
	, sizeof (ServicePointManager_t429)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ServicePointManager_t429_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 4/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.ServicePointManager/SPKey
#include "System_System_Net_ServicePointManager_SPKey.h"
// Metadata Definition System.Net.ServicePointManager/SPKey
extern TypeInfo SPKey_t427_il2cpp_TypeInfo;
// System.Net.ServicePointManager/SPKey
#include "System_System_Net_ServicePointManager_SPKeyMethodDeclarations.h"
static const EncodedMethodIndex SPKey_t427_VTable[4] = 
{
	526,
	125,
	527,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType SPKey_t427_1_0_0;
struct SPKey_t427;
const Il2CppTypeDefinitionMetadata SPKey_t427_DefinitionMetadata = 
{
	&ServicePointManager_t429_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SPKey_t427_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1339/* fieldStart */
	, 1402/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SPKey_t427_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "SPKey"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SPKey_t427_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SPKey_t427_0_0_0/* byval_arg */
	, &SPKey_t427_1_0_0/* this_arg */
	, &SPKey_t427_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SPKey_t427)/* instance_size */
	, sizeof (SPKey_t427)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.WebHeaderCollection
#include "System_System_Net_WebHeaderCollection.h"
// Metadata Definition System.Net.WebHeaderCollection
extern TypeInfo WebHeaderCollection_t408_il2cpp_TypeInfo;
// System.Net.WebHeaderCollection
#include "System_System_Net_WebHeaderCollectionMethodDeclarations.h"
static const EncodedMethodIndex WebHeaderCollection_t408_VTable[18] = 
{
	120,
	125,
	122,
	528,
	529,
	487,
	488,
	530,
	531,
	532,
	533,
	531,
	534,
	529,
	530,
	535,
	536,
	537,
};
static const Il2CppType* WebHeaderCollection_t408_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair WebHeaderCollection_t408_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IDeserializationCallback_t1429_0_0_0, 7},
	{ &IEnumerable_t302_0_0_0, 8},
	{ &ISerializable_t1426_0_0_0, 9},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType WebHeaderCollection_t408_0_0_0;
extern const Il2CppType WebHeaderCollection_t408_1_0_0;
struct WebHeaderCollection_t408;
const Il2CppTypeDefinitionMetadata WebHeaderCollection_t408_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WebHeaderCollection_t408_InterfacesTypeInfos/* implementedInterfaces */
	, WebHeaderCollection_t408_InterfacesOffsets/* interfaceOffsets */
	, &NameValueCollection_t398_0_0_0/* parent */
	, WebHeaderCollection_t408_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1341/* fieldStart */
	, 1405/* methodStart */
	, -1/* eventStart */
	, 334/* propertyStart */

};
TypeInfo WebHeaderCollection_t408_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebHeaderCollection"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &WebHeaderCollection_t408_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 464/* custom_attributes_cache */
	, &WebHeaderCollection_t408_0_0_0/* byval_arg */
	, &WebHeaderCollection_t408_1_0_0/* this_arg */
	, &WebHeaderCollection_t408_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebHeaderCollection_t408)/* instance_size */
	, sizeof (WebHeaderCollection_t408)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WebHeaderCollection_t408_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8193/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Net.WebProxy
#include "System_System_Net_WebProxy.h"
// Metadata Definition System.Net.WebProxy
extern TypeInfo WebProxy_t433_il2cpp_TypeInfo;
// System.Net.WebProxy
#include "System_System_Net_WebProxyMethodDeclarations.h"
static const EncodedMethodIndex WebProxy_t433_VTable[8] = 
{
	120,
	125,
	122,
	123,
	538,
	539,
	540,
	541,
};
static const Il2CppType* WebProxy_t433_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
	&IWebProxy_t409_0_0_0,
};
static Il2CppInterfaceOffsetPair WebProxy_t433_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &IWebProxy_t409_0_0_0, 5},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType WebProxy_t433_0_0_0;
extern const Il2CppType WebProxy_t433_1_0_0;
struct WebProxy_t433;
const Il2CppTypeDefinitionMetadata WebProxy_t433_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WebProxy_t433_InterfacesTypeInfos/* implementedInterfaces */
	, WebProxy_t433_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WebProxy_t433_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1346/* fieldStart */
	, 1423/* methodStart */
	, -1/* eventStart */
	, 336/* propertyStart */

};
TypeInfo WebProxy_t433_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebProxy"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &WebProxy_t433_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WebProxy_t433_0_0_0/* byval_arg */
	, &WebProxy_t433_1_0_0/* this_arg */
	, &WebProxy_t433_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebProxy_t433)/* instance_size */
	, sizeof (WebProxy_t433)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Net.WebRequest
#include "System_System_Net_WebRequest.h"
// Metadata Definition System.Net.WebRequest
extern TypeInfo WebRequest_t411_il2cpp_TypeInfo;
// System.Net.WebRequest
#include "System_System_Net_WebRequestMethodDeclarations.h"
static const EncodedMethodIndex WebRequest_t411_VTable[6] = 
{
	120,
	125,
	122,
	123,
	515,
	516,
};
static const Il2CppType* WebRequest_t411_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair WebRequest_t411_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType WebRequest_t411_1_0_0;
extern const Il2CppType MarshalByRefObject_t434_0_0_0;
struct WebRequest_t411;
const Il2CppTypeDefinitionMetadata WebRequest_t411_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WebRequest_t411_InterfacesTypeInfos/* implementedInterfaces */
	, WebRequest_t411_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t434_0_0_0/* parent */
	, WebRequest_t411_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1351/* fieldStart */
	, 1432/* methodStart */
	, -1/* eventStart */
	, 337/* propertyStart */

};
TypeInfo WebRequest_t411_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebRequest"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &WebRequest_t411_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WebRequest_t411_0_0_0/* byval_arg */
	, &WebRequest_t411_1_0_0/* this_arg */
	, &WebRequest_t411_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebRequest_t411)/* instance_size */
	, sizeof (WebRequest_t411)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WebRequest_t411_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.OpenFlags
#include "System_System_Security_Cryptography_X509Certificates_OpenFla.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.OpenFlags
extern TypeInfo OpenFlags_t435_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.OpenFlags
#include "System_System_Security_Cryptography_X509Certificates_OpenFlaMethodDeclarations.h"
static const EncodedMethodIndex OpenFlags_t435_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair OpenFlags_t435_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType OpenFlags_t435_0_0_0;
extern const Il2CppType OpenFlags_t435_1_0_0;
const Il2CppTypeDefinitionMetadata OpenFlags_t435_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OpenFlags_t435_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, OpenFlags_t435_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1356/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OpenFlags_t435_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "OpenFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 467/* custom_attributes_cache */
	, &OpenFlags_t435_0_0_0/* byval_arg */
	, &OpenFlags_t435_1_0_0/* this_arg */
	, &OpenFlags_t435_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OpenFlags_t435)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (OpenFlags_t435)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.PublicKey
#include "System_System_Security_Cryptography_X509Certificates_PublicK.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.PublicKey
extern TypeInfo PublicKey_t439_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.PublicKey
#include "System_System_Security_Cryptography_X509Certificates_PublicKMethodDeclarations.h"
static const EncodedMethodIndex PublicKey_t439_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PublicKey_t439_0_0_0;
extern const Il2CppType PublicKey_t439_1_0_0;
struct PublicKey_t439;
const Il2CppTypeDefinitionMetadata PublicKey_t439_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PublicKey_t439_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1362/* fieldStart */
	, 1442/* methodStart */
	, -1/* eventStart */
	, 338/* propertyStart */

};
TypeInfo PublicKey_t439_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PublicKey"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &PublicKey_t439_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PublicKey_t439_0_0_0/* byval_arg */
	, &PublicKey_t439_1_0_0/* this_arg */
	, &PublicKey_t439_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PublicKey_t439)/* instance_size */
	, sizeof (PublicKey_t439)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PublicKey_t439_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.StoreLocation
#include "System_System_Security_Cryptography_X509Certificates_StoreLo.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.StoreLocation
extern TypeInfo StoreLocation_t440_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.StoreLocation
#include "System_System_Security_Cryptography_X509Certificates_StoreLoMethodDeclarations.h"
static const EncodedMethodIndex StoreLocation_t440_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair StoreLocation_t440_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType StoreLocation_t440_0_0_0;
extern const Il2CppType StoreLocation_t440_1_0_0;
const Il2CppTypeDefinitionMetadata StoreLocation_t440_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StoreLocation_t440_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, StoreLocation_t440_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1367/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StoreLocation_t440_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "StoreLocation"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StoreLocation_t440_0_0_0/* byval_arg */
	, &StoreLocation_t440_1_0_0/* this_arg */
	, &StoreLocation_t440_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StoreLocation_t440)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StoreLocation_t440)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.StoreName
#include "System_System_Security_Cryptography_X509Certificates_StoreNa.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.StoreName
extern TypeInfo StoreName_t441_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.StoreName
#include "System_System_Security_Cryptography_X509Certificates_StoreNaMethodDeclarations.h"
static const EncodedMethodIndex StoreName_t441_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair StoreName_t441_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType StoreName_t441_0_0_0;
extern const Il2CppType StoreName_t441_1_0_0;
const Il2CppTypeDefinitionMetadata StoreName_t441_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StoreName_t441_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, StoreName_t441_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1370/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StoreName_t441_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "StoreName"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StoreName_t441_0_0_0/* byval_arg */
	, &StoreName_t441_1_0_0/* this_arg */
	, &StoreName_t441_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StoreName_t441)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StoreName_t441)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X500DistinguishedName
#include "System_System_Security_Cryptography_X509Certificates_X500Dis.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X500DistinguishedName
extern TypeInfo X500DistinguishedName_t442_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X500DistinguishedName
#include "System_System_Security_Cryptography_X509Certificates_X500DisMethodDeclarations.h"
static const EncodedMethodIndex X500DistinguishedName_t442_VTable[6] = 
{
	120,
	125,
	122,
	123,
	542,
	543,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X500DistinguishedName_t442_0_0_0;
extern const Il2CppType X500DistinguishedName_t442_1_0_0;
extern const Il2CppType AsnEncodedData_t437_0_0_0;
struct X500DistinguishedName_t442;
const Il2CppTypeDefinitionMetadata X500DistinguishedName_t442_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsnEncodedData_t437_0_0_0/* parent */
	, X500DistinguishedName_t442_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1379/* fieldStart */
	, 1450/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X500DistinguishedName_t442_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X500DistinguishedName"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X500DistinguishedName_t442_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 469/* custom_attributes_cache */
	, &X500DistinguishedName_t442_0_0_0/* byval_arg */
	, &X500DistinguishedName_t442_1_0_0/* this_arg */
	, &X500DistinguishedName_t442_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X500DistinguishedName_t442)/* instance_size */
	, sizeof (X500DistinguishedName_t442)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags
#include "System_System_Security_Cryptography_X509Certificates_X500Dis_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags
extern TypeInfo X500DistinguishedNameFlags_t443_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags
#include "System_System_Security_Cryptography_X509Certificates_X500Dis_0MethodDeclarations.h"
static const EncodedMethodIndex X500DistinguishedNameFlags_t443_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair X500DistinguishedNameFlags_t443_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X500DistinguishedNameFlags_t443_0_0_0;
extern const Il2CppType X500DistinguishedNameFlags_t443_1_0_0;
const Il2CppTypeDefinitionMetadata X500DistinguishedNameFlags_t443_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X500DistinguishedNameFlags_t443_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, X500DistinguishedNameFlags_t443_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1380/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X500DistinguishedNameFlags_t443_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X500DistinguishedNameFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 470/* custom_attributes_cache */
	, &X500DistinguishedNameFlags_t443_0_0_0/* byval_arg */
	, &X500DistinguishedNameFlags_t443_1_0_0/* this_arg */
	, &X500DistinguishedNameFlags_t443_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X500DistinguishedNameFlags_t443)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X500DistinguishedNameFlags_t443)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension
#include "System_System_Security_Cryptography_X509Certificates_X509Bas.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension
extern TypeInfo X509BasicConstraintsExtension_t444_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension
#include "System_System_Security_Cryptography_X509Certificates_X509BasMethodDeclarations.h"
static const EncodedMethodIndex X509BasicConstraintsExtension_t444_VTable[6] = 
{
	120,
	125,
	122,
	123,
	544,
	545,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509BasicConstraintsExtension_t444_0_0_0;
extern const Il2CppType X509BasicConstraintsExtension_t444_1_0_0;
extern const Il2CppType X509Extension_t445_0_0_0;
struct X509BasicConstraintsExtension_t444;
const Il2CppTypeDefinitionMetadata X509BasicConstraintsExtension_t444_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t445_0_0_0/* parent */
	, X509BasicConstraintsExtension_t444_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1391/* fieldStart */
	, 1456/* methodStart */
	, -1/* eventStart */
	, 342/* propertyStart */

};
TypeInfo X509BasicConstraintsExtension_t444_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509BasicConstraintsExtension"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509BasicConstraintsExtension_t444_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509BasicConstraintsExtension_t444_0_0_0/* byval_arg */
	, &X509BasicConstraintsExtension_t444_1_0_0/* this_arg */
	, &X509BasicConstraintsExtension_t444_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509BasicConstraintsExtension_t444)/* instance_size */
	, sizeof (X509BasicConstraintsExtension_t444)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 3/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Certificate2
#include "System_System_Security_Cryptography_X509Certificates_X509Cer.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Certificate2
extern TypeInfo X509Certificate2_t448_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Certificate2
#include "System_System_Security_Cryptography_X509Certificates_X509CerMethodDeclarations.h"
static const EncodedMethodIndex X509Certificate2_t448_VTable[18] = 
{
	546,
	125,
	547,
	548,
	549,
	550,
	551,
	552,
	553,
	554,
	555,
	556,
	557,
	558,
	559,
	560,
	561,
	562,
};
static Il2CppInterfaceOffsetPair X509Certificate2_t448_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &IDeserializationCallback_t1429_0_0_0, 5},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509Certificate2_t448_0_0_0;
extern const Il2CppType X509Certificate2_t448_1_0_0;
extern const Il2CppType X509Certificate_t449_0_0_0;
struct X509Certificate2_t448;
const Il2CppTypeDefinitionMetadata X509Certificate2_t448_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509Certificate2_t448_InterfacesOffsets/* interfaceOffsets */
	, &X509Certificate_t449_0_0_0/* parent */
	, X509Certificate2_t448_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1397/* fieldStart */
	, 1466/* methodStart */
	, -1/* eventStart */
	, 345/* propertyStart */

};
TypeInfo X509Certificate2_t448_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Certificate2"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Certificate2_t448_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Certificate2_t448_0_0_0/* byval_arg */
	, &X509Certificate2_t448_1_0_0/* this_arg */
	, &X509Certificate2_t448_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Certificate2_t448)/* instance_size */
	, sizeof (X509Certificate2_t448)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509Certificate2_t448_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 12/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Certificate2Collection
extern TypeInfo X509Certificate2Collection_t450_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_0MethodDeclarations.h"
static const EncodedMethodIndex X509Certificate2Collection_t450_VTable[26] = 
{
	120,
	125,
	563,
	123,
	564,
	565,
	566,
	567,
	568,
	569,
	570,
	571,
	572,
	573,
	574,
	575,
	576,
	577,
	578,
	579,
	580,
	581,
	582,
	583,
	584,
	585,
};
extern const Il2CppType IList_t519_0_0_0;
static Il2CppInterfaceOffsetPair X509Certificate2Collection_t450_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509Certificate2Collection_t450_0_0_0;
extern const Il2CppType X509Certificate2Collection_t450_1_0_0;
extern const Il2CppType X509CertificateCollection_t420_0_0_0;
struct X509Certificate2Collection_t450;
const Il2CppTypeDefinitionMetadata X509Certificate2Collection_t450_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509Certificate2Collection_t450_InterfacesOffsets/* interfaceOffsets */
	, &X509CertificateCollection_t420_0_0_0/* parent */
	, X509Certificate2Collection_t450_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1490/* methodStart */
	, -1/* eventStart */
	, 357/* propertyStart */

};
TypeInfo X509Certificate2Collection_t450_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Certificate2Collection"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Certificate2Collection_t450_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 474/* custom_attributes_cache */
	, &X509Certificate2Collection_t450_0_0_0/* byval_arg */
	, &X509Certificate2Collection_t450_1_0_0/* this_arg */
	, &X509Certificate2Collection_t450_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Certificate2Collection_t450)/* instance_size */
	, sizeof (X509Certificate2Collection_t450)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_2.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
extern TypeInfo X509Certificate2Enumerator_t451_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_2MethodDeclarations.h"
static const EncodedMethodIndex X509Certificate2Enumerator_t451_VTable[7] = 
{
	120,
	125,
	122,
	123,
	586,
	587,
	588,
};
static const Il2CppType* X509Certificate2Enumerator_t451_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair X509Certificate2Enumerator_t451_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509Certificate2Enumerator_t451_0_0_0;
extern const Il2CppType X509Certificate2Enumerator_t451_1_0_0;
struct X509Certificate2Enumerator_t451;
const Il2CppTypeDefinitionMetadata X509Certificate2Enumerator_t451_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509Certificate2Enumerator_t451_InterfacesTypeInfos/* implementedInterfaces */
	, X509Certificate2Enumerator_t451_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Certificate2Enumerator_t451_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1410/* fieldStart */
	, 1498/* methodStart */
	, -1/* eventStart */
	, 358/* propertyStart */

};
TypeInfo X509Certificate2Enumerator_t451_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Certificate2Enumerator"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Certificate2Enumerator_t451_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Certificate2Enumerator_t451_0_0_0/* byval_arg */
	, &X509Certificate2Enumerator_t451_1_0_0/* this_arg */
	, &X509Certificate2Enumerator_t451_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Certificate2Enumerator_t451)/* instance_size */
	, sizeof (X509Certificate2Enumerator_t451)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_1.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509CertificateCollection
extern TypeInfo X509CertificateCollection_t420_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_1MethodDeclarations.h"
extern const Il2CppType X509CertificateEnumerator_t452_0_0_0;
static const Il2CppType* X509CertificateCollection_t420_il2cpp_TypeInfo__nestedTypes[1] =
{
	&X509CertificateEnumerator_t452_0_0_0,
};
static const EncodedMethodIndex X509CertificateCollection_t420_VTable[26] = 
{
	120,
	125,
	563,
	123,
	564,
	565,
	566,
	567,
	568,
	569,
	570,
	571,
	572,
	573,
	574,
	575,
	576,
	577,
	578,
	579,
	580,
	581,
	582,
	583,
	584,
	585,
};
static Il2CppInterfaceOffsetPair X509CertificateCollection_t420_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509CertificateCollection_t420_1_0_0;
extern const Il2CppType CollectionBase_t453_0_0_0;
struct X509CertificateCollection_t420;
const Il2CppTypeDefinitionMetadata X509CertificateCollection_t420_DefinitionMetadata = 
{
	NULL/* declaringType */
	, X509CertificateCollection_t420_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509CertificateCollection_t420_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t453_0_0_0/* parent */
	, X509CertificateCollection_t420_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1503/* methodStart */
	, -1/* eventStart */
	, 360/* propertyStart */

};
TypeInfo X509CertificateCollection_t420_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509CertificateCollection"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509CertificateCollection_t420_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 477/* custom_attributes_cache */
	, &X509CertificateCollection_t420_0_0_0/* byval_arg */
	, &X509CertificateCollection_t420_1_0_0/* this_arg */
	, &X509CertificateCollection_t420_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509CertificateCollection_t420)/* instance_size */
	, sizeof (X509CertificateCollection_t420)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_3.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator
extern TypeInfo X509CertificateEnumerator_t452_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_3MethodDeclarations.h"
static const EncodedMethodIndex X509CertificateEnumerator_t452_VTable[7] = 
{
	120,
	125,
	122,
	123,
	589,
	590,
	591,
};
static const Il2CppType* X509CertificateEnumerator_t452_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair X509CertificateEnumerator_t452_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509CertificateEnumerator_t452_1_0_0;
struct X509CertificateEnumerator_t452;
const Il2CppTypeDefinitionMetadata X509CertificateEnumerator_t452_DefinitionMetadata = 
{
	&X509CertificateCollection_t420_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, X509CertificateEnumerator_t452_InterfacesTypeInfos/* implementedInterfaces */
	, X509CertificateEnumerator_t452_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509CertificateEnumerator_t452_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1411/* fieldStart */
	, 1509/* methodStart */
	, -1/* eventStart */
	, 361/* propertyStart */

};
TypeInfo X509CertificateEnumerator_t452_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509CertificateEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &X509CertificateEnumerator_t452_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509CertificateEnumerator_t452_0_0_0/* byval_arg */
	, &X509CertificateEnumerator_t452_1_0_0/* this_arg */
	, &X509CertificateEnumerator_t452_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509CertificateEnumerator_t452)/* instance_size */
	, sizeof (X509CertificateEnumerator_t452)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Chain
#include "System_System_Security_Cryptography_X509Certificates_X509Cha.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Chain
extern TypeInfo X509Chain_t460_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Chain
#include "System_System_Security_Cryptography_X509Certificates_X509ChaMethodDeclarations.h"
static const EncodedMethodIndex X509Chain_t460_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509Chain_t460_0_0_0;
extern const Il2CppType X509Chain_t460_1_0_0;
struct X509Chain_t460;
const Il2CppTypeDefinitionMetadata X509Chain_t460_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Chain_t460_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1412/* fieldStart */
	, 1514/* methodStart */
	, -1/* eventStart */
	, 363/* propertyStart */

};
TypeInfo X509Chain_t460_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Chain"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Chain_t460_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Chain_t460_0_0_0/* byval_arg */
	, &X509Chain_t460_1_0_0/* this_arg */
	, &X509Chain_t460_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Chain_t460)/* instance_size */
	, sizeof (X509Chain_t460)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509Chain_t460_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 4/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ChainElement
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainElement
extern TypeInfo X509ChainElement_t458_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ChainElement
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_0MethodDeclarations.h"
static const EncodedMethodIndex X509ChainElement_t458_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ChainElement_t458_0_0_0;
extern const Il2CppType X509ChainElement_t458_1_0_0;
struct X509ChainElement_t458;
const Il2CppTypeDefinitionMetadata X509ChainElement_t458_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509ChainElement_t458_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1427/* fieldStart */
	, 1544/* methodStart */
	, -1/* eventStart */
	, 367/* propertyStart */

};
TypeInfo X509ChainElement_t458_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainElement"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ChainElement_t458_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509ChainElement_t458_0_0_0/* byval_arg */
	, &X509ChainElement_t458_1_0_0/* this_arg */
	, &X509ChainElement_t458_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ChainElement_t458)/* instance_size */
	, sizeof (X509ChainElement_t458)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_2.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainElementCollection
extern TypeInfo X509ChainElementCollection_t454_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_2MethodDeclarations.h"
static const EncodedMethodIndex X509ChainElementCollection_t454_VTable[8] = 
{
	120,
	125,
	122,
	123,
	592,
	593,
	594,
	595,
};
static const Il2CppType* X509ChainElementCollection_t454_InterfacesTypeInfos[] = 
{
	&ICollection_t569_0_0_0,
	&IEnumerable_t302_0_0_0,
};
static Il2CppInterfaceOffsetPair X509ChainElementCollection_t454_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IEnumerable_t302_0_0_0, 7},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ChainElementCollection_t454_0_0_0;
extern const Il2CppType X509ChainElementCollection_t454_1_0_0;
struct X509ChainElementCollection_t454;
const Il2CppTypeDefinitionMetadata X509ChainElementCollection_t454_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509ChainElementCollection_t454_InterfacesTypeInfos/* implementedInterfaces */
	, X509ChainElementCollection_t454_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509ChainElementCollection_t454_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1431/* fieldStart */
	, 1552/* methodStart */
	, -1/* eventStart */
	, 370/* propertyStart */

};
TypeInfo X509ChainElementCollection_t454_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainElementCollection"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ChainElementCollection_t454_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 482/* custom_attributes_cache */
	, &X509ChainElementCollection_t454_0_0_0/* byval_arg */
	, &X509ChainElementCollection_t454_1_0_0/* this_arg */
	, &X509ChainElementCollection_t454_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ChainElementCollection_t454)/* instance_size */
	, sizeof (X509ChainElementCollection_t454)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_3.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
extern TypeInfo X509ChainElementEnumerator_t461_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_3MethodDeclarations.h"
static const EncodedMethodIndex X509ChainElementEnumerator_t461_VTable[6] = 
{
	120,
	125,
	122,
	123,
	596,
	597,
};
static const Il2CppType* X509ChainElementEnumerator_t461_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair X509ChainElementEnumerator_t461_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ChainElementEnumerator_t461_0_0_0;
extern const Il2CppType X509ChainElementEnumerator_t461_1_0_0;
struct X509ChainElementEnumerator_t461;
const Il2CppTypeDefinitionMetadata X509ChainElementEnumerator_t461_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509ChainElementEnumerator_t461_InterfacesTypeInfos/* implementedInterfaces */
	, X509ChainElementEnumerator_t461_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509ChainElementEnumerator_t461_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1432/* fieldStart */
	, 1562/* methodStart */
	, -1/* eventStart */
	, 373/* propertyStart */

};
TypeInfo X509ChainElementEnumerator_t461_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainElementEnumerator"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ChainElementEnumerator_t461_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509ChainElementEnumerator_t461_0_0_0/* byval_arg */
	, &X509ChainElementEnumerator_t461_1_0_0/* this_arg */
	, &X509ChainElementEnumerator_t461_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ChainElementEnumerator_t461)/* instance_size */
	, sizeof (X509ChainElementEnumerator_t461)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ChainPolicy
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_4.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainPolicy
extern TypeInfo X509ChainPolicy_t455_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ChainPolicy
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_4MethodDeclarations.h"
static const EncodedMethodIndex X509ChainPolicy_t455_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ChainPolicy_t455_0_0_0;
extern const Il2CppType X509ChainPolicy_t455_1_0_0;
struct X509ChainPolicy_t455;
const Il2CppTypeDefinitionMetadata X509ChainPolicy_t455_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509ChainPolicy_t455_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1433/* fieldStart */
	, 1566/* methodStart */
	, -1/* eventStart */
	, 375/* propertyStart */

};
TypeInfo X509ChainPolicy_t455_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainPolicy"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ChainPolicy_t455_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509ChainPolicy_t455_0_0_0/* byval_arg */
	, &X509ChainPolicy_t455_1_0_0/* this_arg */
	, &X509ChainPolicy_t455_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ChainPolicy_t455)/* instance_size */
	, sizeof (X509ChainPolicy_t455)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 5/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainStatus
extern TypeInfo X509ChainStatus_t457_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5MethodDeclarations.h"
static const EncodedMethodIndex X509ChainStatus_t457_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ChainStatus_t457_0_0_0;
extern const Il2CppType X509ChainStatus_t457_1_0_0;
const Il2CppTypeDefinitionMetadata X509ChainStatus_t457_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, X509ChainStatus_t457_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1441/* fieldStart */
	, 1573/* methodStart */
	, -1/* eventStart */
	, 380/* propertyStart */

};
TypeInfo X509ChainStatus_t457_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainStatus"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ChainStatus_t457_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509ChainStatus_t457_0_0_0/* byval_arg */
	, &X509ChainStatus_t457_1_0_0/* this_arg */
	, &X509ChainStatus_t457_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)X509ChainStatus_t457_marshal/* marshal_to_native_func */
	, (methodPointerType)X509ChainStatus_t457_marshal_back/* marshal_from_native_func */
	, (methodPointerType)X509ChainStatus_t457_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (X509ChainStatus_t457)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509ChainStatus_t457)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(X509ChainStatus_t457_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
extern TypeInfo X509ChainStatusFlags_t464_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1MethodDeclarations.h"
static const EncodedMethodIndex X509ChainStatusFlags_t464_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair X509ChainStatusFlags_t464_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ChainStatusFlags_t464_0_0_0;
extern const Il2CppType X509ChainStatusFlags_t464_1_0_0;
const Il2CppTypeDefinitionMetadata X509ChainStatusFlags_t464_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509ChainStatusFlags_t464_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, X509ChainStatusFlags_t464_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1443/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509ChainStatusFlags_t464_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainStatusFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 483/* custom_attributes_cache */
	, &X509ChainStatusFlags_t464_0_0_0/* byval_arg */
	, &X509ChainStatusFlags_t464_1_0_0/* this_arg */
	, &X509ChainStatusFlags_t464_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ChainStatusFlags_t464)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509ChainStatusFlags_t464)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension
#include "System_System_Security_Cryptography_X509Certificates_X509Enh.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension
extern TypeInfo X509EnhancedKeyUsageExtension_t465_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension
#include "System_System_Security_Cryptography_X509Certificates_X509EnhMethodDeclarations.h"
static const EncodedMethodIndex X509EnhancedKeyUsageExtension_t465_VTable[6] = 
{
	120,
	125,
	122,
	123,
	598,
	599,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509EnhancedKeyUsageExtension_t465_0_0_0;
extern const Il2CppType X509EnhancedKeyUsageExtension_t465_1_0_0;
struct X509EnhancedKeyUsageExtension_t465;
const Il2CppTypeDefinitionMetadata X509EnhancedKeyUsageExtension_t465_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t445_0_0_0/* parent */
	, X509EnhancedKeyUsageExtension_t465_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1467/* fieldStart */
	, 1578/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509EnhancedKeyUsageExtension_t465_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509EnhancedKeyUsageExtension"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509EnhancedKeyUsageExtension_t465_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509EnhancedKeyUsageExtension_t465_0_0_0/* byval_arg */
	, &X509EnhancedKeyUsageExtension_t465_1_0_0/* this_arg */
	, &X509EnhancedKeyUsageExtension_t465_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509EnhancedKeyUsageExtension_t465)/* instance_size */
	, sizeof (X509EnhancedKeyUsageExtension_t465)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509EnhancedKeyUsageExtension_t465_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Extension
#include "System_System_Security_Cryptography_X509Certificates_X509Ext.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Extension
extern TypeInfo X509Extension_t445_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Extension
#include "System_System_Security_Cryptography_X509Certificates_X509ExtMethodDeclarations.h"
static const EncodedMethodIndex X509Extension_t445_VTable[6] = 
{
	120,
	125,
	122,
	123,
	600,
	543,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509Extension_t445_1_0_0;
struct X509Extension_t445;
const Il2CppTypeDefinitionMetadata X509Extension_t445_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsnEncodedData_t437_0_0_0/* parent */
	, X509Extension_t445_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1470/* fieldStart */
	, 1582/* methodStart */
	, -1/* eventStart */
	, 382/* propertyStart */

};
TypeInfo X509Extension_t445_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Extension"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Extension_t445_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Extension_t445_0_0_0/* byval_arg */
	, &X509Extension_t445_1_0_0/* this_arg */
	, &X509Extension_t445_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Extension_t445)/* instance_size */
	, sizeof (X509Extension_t445)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Ext_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ExtensionCollection
extern TypeInfo X509ExtensionCollection_t446_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Ext_0MethodDeclarations.h"
static const EncodedMethodIndex X509ExtensionCollection_t446_VTable[8] = 
{
	120,
	125,
	122,
	123,
	601,
	602,
	603,
	604,
};
static const Il2CppType* X509ExtensionCollection_t446_InterfacesTypeInfos[] = 
{
	&ICollection_t569_0_0_0,
	&IEnumerable_t302_0_0_0,
};
static Il2CppInterfaceOffsetPair X509ExtensionCollection_t446_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IEnumerable_t302_0_0_0, 7},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ExtensionCollection_t446_0_0_0;
extern const Il2CppType X509ExtensionCollection_t446_1_0_0;
struct X509ExtensionCollection_t446;
const Il2CppTypeDefinitionMetadata X509ExtensionCollection_t446_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509ExtensionCollection_t446_InterfacesTypeInfos/* implementedInterfaces */
	, X509ExtensionCollection_t446_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509ExtensionCollection_t446_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1471/* fieldStart */
	, 1588/* methodStart */
	, -1/* eventStart */
	, 383/* propertyStart */

};
TypeInfo X509ExtensionCollection_t446_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ExtensionCollection"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ExtensionCollection_t446_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 485/* custom_attributes_cache */
	, &X509ExtensionCollection_t446_0_0_0/* byval_arg */
	, &X509ExtensionCollection_t446_1_0_0/* this_arg */
	, &X509ExtensionCollection_t446_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ExtensionCollection_t446)/* instance_size */
	, sizeof (X509ExtensionCollection_t446)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Ext_1.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator
extern TypeInfo X509ExtensionEnumerator_t466_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Ext_1MethodDeclarations.h"
static const EncodedMethodIndex X509ExtensionEnumerator_t466_VTable[6] = 
{
	120,
	125,
	122,
	123,
	605,
	606,
};
static const Il2CppType* X509ExtensionEnumerator_t466_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair X509ExtensionEnumerator_t466_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ExtensionEnumerator_t466_0_0_0;
extern const Il2CppType X509ExtensionEnumerator_t466_1_0_0;
struct X509ExtensionEnumerator_t466;
const Il2CppTypeDefinitionMetadata X509ExtensionEnumerator_t466_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509ExtensionEnumerator_t466_InterfacesTypeInfos/* implementedInterfaces */
	, X509ExtensionEnumerator_t466_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509ExtensionEnumerator_t466_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1472/* fieldStart */
	, 1595/* methodStart */
	, -1/* eventStart */
	, 386/* propertyStart */

};
TypeInfo X509ExtensionEnumerator_t466_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ExtensionEnumerator"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ExtensionEnumerator_t466_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509ExtensionEnumerator_t466_0_0_0/* byval_arg */
	, &X509ExtensionEnumerator_t466_1_0_0/* this_arg */
	, &X509ExtensionEnumerator_t466_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ExtensionEnumerator_t466)/* instance_size */
	, sizeof (X509ExtensionEnumerator_t466)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509FindType
#include "System_System_Security_Cryptography_X509Certificates_X509Fin.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509FindType
extern TypeInfo X509FindType_t467_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509FindType
#include "System_System_Security_Cryptography_X509Certificates_X509FinMethodDeclarations.h"
static const EncodedMethodIndex X509FindType_t467_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair X509FindType_t467_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509FindType_t467_0_0_0;
extern const Il2CppType X509FindType_t467_1_0_0;
const Il2CppTypeDefinitionMetadata X509FindType_t467_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509FindType_t467_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, X509FindType_t467_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1473/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509FindType_t467_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509FindType"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509FindType_t467_0_0_0/* byval_arg */
	, &X509FindType_t467_1_0_0/* this_arg */
	, &X509FindType_t467_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509FindType_t467)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509FindType_t467)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509KeyUsageExtension
#include "System_System_Security_Cryptography_X509Certificates_X509Key.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509KeyUsageExtension
extern TypeInfo X509KeyUsageExtension_t468_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509KeyUsageExtension
#include "System_System_Security_Cryptography_X509Certificates_X509KeyMethodDeclarations.h"
static const EncodedMethodIndex X509KeyUsageExtension_t468_VTable[6] = 
{
	120,
	125,
	122,
	123,
	607,
	608,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509KeyUsageExtension_t468_0_0_0;
extern const Il2CppType X509KeyUsageExtension_t468_1_0_0;
struct X509KeyUsageExtension_t468;
const Il2CppTypeDefinitionMetadata X509KeyUsageExtension_t468_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t445_0_0_0/* parent */
	, X509KeyUsageExtension_t468_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1489/* fieldStart */
	, 1599/* methodStart */
	, -1/* eventStart */
	, 388/* propertyStart */

};
TypeInfo X509KeyUsageExtension_t468_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509KeyUsageExtension"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509KeyUsageExtension_t468_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509KeyUsageExtension_t468_0_0_0/* byval_arg */
	, &X509KeyUsageExtension_t468_1_0_0/* this_arg */
	, &X509KeyUsageExtension_t468_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509KeyUsageExtension_t468)/* instance_size */
	, sizeof (X509KeyUsageExtension_t468)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
extern TypeInfo X509KeyUsageFlags_t469_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0MethodDeclarations.h"
static const EncodedMethodIndex X509KeyUsageFlags_t469_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair X509KeyUsageFlags_t469_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509KeyUsageFlags_t469_0_0_0;
extern const Il2CppType X509KeyUsageFlags_t469_1_0_0;
const Il2CppTypeDefinitionMetadata X509KeyUsageFlags_t469_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509KeyUsageFlags_t469_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, X509KeyUsageFlags_t469_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1494/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509KeyUsageFlags_t469_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509KeyUsageFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 486/* custom_attributes_cache */
	, &X509KeyUsageFlags_t469_0_0_0/* byval_arg */
	, &X509KeyUsageFlags_t469_1_0_0/* this_arg */
	, &X509KeyUsageFlags_t469_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509KeyUsageFlags_t469)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509KeyUsageFlags_t469)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509NameType
#include "System_System_Security_Cryptography_X509Certificates_X509Nam.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509NameType
extern TypeInfo X509NameType_t470_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509NameType
#include "System_System_Security_Cryptography_X509Certificates_X509NamMethodDeclarations.h"
static const EncodedMethodIndex X509NameType_t470_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair X509NameType_t470_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509NameType_t470_0_0_0;
extern const Il2CppType X509NameType_t470_1_0_0;
const Il2CppTypeDefinitionMetadata X509NameType_t470_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509NameType_t470_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, X509NameType_t470_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1505/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509NameType_t470_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509NameType"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509NameType_t470_0_0_0/* byval_arg */
	, &X509NameType_t470_1_0_0/* this_arg */
	, &X509NameType_t470_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509NameType_t470)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509NameType_t470)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509RevocationFlag
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509RevocationFlag
extern TypeInfo X509RevocationFlag_t471_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509RevocationFlag
#include "System_System_Security_Cryptography_X509Certificates_X509RevMethodDeclarations.h"
static const EncodedMethodIndex X509RevocationFlag_t471_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair X509RevocationFlag_t471_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509RevocationFlag_t471_0_0_0;
extern const Il2CppType X509RevocationFlag_t471_1_0_0;
const Il2CppTypeDefinitionMetadata X509RevocationFlag_t471_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509RevocationFlag_t471_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, X509RevocationFlag_t471_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1512/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509RevocationFlag_t471_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509RevocationFlag"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509RevocationFlag_t471_0_0_0/* byval_arg */
	, &X509RevocationFlag_t471_1_0_0/* this_arg */
	, &X509RevocationFlag_t471_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509RevocationFlag_t471)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509RevocationFlag_t471)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509RevocationMode
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509RevocationMode
extern TypeInfo X509RevocationMode_t472_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509RevocationMode
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0MethodDeclarations.h"
static const EncodedMethodIndex X509RevocationMode_t472_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair X509RevocationMode_t472_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509RevocationMode_t472_0_0_0;
extern const Il2CppType X509RevocationMode_t472_1_0_0;
const Il2CppTypeDefinitionMetadata X509RevocationMode_t472_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509RevocationMode_t472_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, X509RevocationMode_t472_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1516/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509RevocationMode_t472_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509RevocationMode"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509RevocationMode_t472_0_0_0/* byval_arg */
	, &X509RevocationMode_t472_1_0_0/* this_arg */
	, &X509RevocationMode_t472_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509RevocationMode_t472)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509RevocationMode_t472)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Store
#include "System_System_Security_Cryptography_X509Certificates_X509Sto.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Store
extern TypeInfo X509Store_t459_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Store
#include "System_System_Security_Cryptography_X509Certificates_X509StoMethodDeclarations.h"
static const EncodedMethodIndex X509Store_t459_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509Store_t459_0_0_0;
extern const Il2CppType X509Store_t459_1_0_0;
struct X509Store_t459;
const Il2CppTypeDefinitionMetadata X509Store_t459_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Store_t459_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1520/* fieldStart */
	, 1608/* methodStart */
	, -1/* eventStart */
	, 389/* propertyStart */

};
TypeInfo X509Store_t459_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Store"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Store_t459_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Store_t459_0_0_0/* byval_arg */
	, &X509Store_t459_1_0_0/* this_arg */
	, &X509Store_t459_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Store_t459)/* instance_size */
	, sizeof (X509Store_t459)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509Store_t459_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension
#include "System_System_Security_Cryptography_X509Certificates_X509Sub.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension
extern TypeInfo X509SubjectKeyIdentifierExtension_t474_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension
#include "System_System_Security_Cryptography_X509Certificates_X509SubMethodDeclarations.h"
static const EncodedMethodIndex X509SubjectKeyIdentifierExtension_t474_VTable[6] = 
{
	120,
	125,
	122,
	123,
	609,
	610,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509SubjectKeyIdentifierExtension_t474_0_0_0;
extern const Il2CppType X509SubjectKeyIdentifierExtension_t474_1_0_0;
struct X509SubjectKeyIdentifierExtension_t474;
const Il2CppTypeDefinitionMetadata X509SubjectKeyIdentifierExtension_t474_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t445_0_0_0/* parent */
	, X509SubjectKeyIdentifierExtension_t474_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1526/* fieldStart */
	, 1614/* methodStart */
	, -1/* eventStart */
	, 392/* propertyStart */

};
TypeInfo X509SubjectKeyIdentifierExtension_t474_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509SubjectKeyIdentifierExtension"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509SubjectKeyIdentifierExtension_t474_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509SubjectKeyIdentifierExtension_t474_0_0_0/* byval_arg */
	, &X509SubjectKeyIdentifierExtension_t474_1_0_0/* this_arg */
	, &X509SubjectKeyIdentifierExtension_t474_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509SubjectKeyIdentifierExtension_t474)/* instance_size */
	, sizeof (X509SubjectKeyIdentifierExtension_t474)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
#include "System_System_Security_Cryptography_X509Certificates_X509Sub_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
extern TypeInfo X509SubjectKeyIdentifierHashAlgorithm_t475_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
#include "System_System_Security_Cryptography_X509Certificates_X509Sub_0MethodDeclarations.h"
static const EncodedMethodIndex X509SubjectKeyIdentifierHashAlgorithm_t475_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair X509SubjectKeyIdentifierHashAlgorithm_t475_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509SubjectKeyIdentifierHashAlgorithm_t475_0_0_0;
extern const Il2CppType X509SubjectKeyIdentifierHashAlgorithm_t475_1_0_0;
const Il2CppTypeDefinitionMetadata X509SubjectKeyIdentifierHashAlgorithm_t475_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509SubjectKeyIdentifierHashAlgorithm_t475_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, X509SubjectKeyIdentifierHashAlgorithm_t475_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1531/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509SubjectKeyIdentifierHashAlgorithm_t475_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509SubjectKeyIdentifierHashAlgorithm"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509SubjectKeyIdentifierHashAlgorithm_t475_0_0_0/* byval_arg */
	, &X509SubjectKeyIdentifierHashAlgorithm_t475_1_0_0/* this_arg */
	, &X509SubjectKeyIdentifierHashAlgorithm_t475_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509SubjectKeyIdentifierHashAlgorithm_t475)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509SubjectKeyIdentifierHashAlgorithm_t475)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509VerificationFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509VerificationFlags
extern TypeInfo X509VerificationFlags_t476_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509VerificationFlags
#include "System_System_Security_Cryptography_X509Certificates_X509VerMethodDeclarations.h"
static const EncodedMethodIndex X509VerificationFlags_t476_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair X509VerificationFlags_t476_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509VerificationFlags_t476_0_0_0;
extern const Il2CppType X509VerificationFlags_t476_1_0_0;
const Il2CppTypeDefinitionMetadata X509VerificationFlags_t476_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509VerificationFlags_t476_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, X509VerificationFlags_t476_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1535/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509VerificationFlags_t476_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509VerificationFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 488/* custom_attributes_cache */
	, &X509VerificationFlags_t476_0_0_0/* byval_arg */
	, &X509VerificationFlags_t476_1_0_0/* this_arg */
	, &X509VerificationFlags_t476_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509VerificationFlags_t476)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509VerificationFlags_t476)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.AsnDecodeStatus
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
// Metadata Definition System.Security.Cryptography.AsnDecodeStatus
extern TypeInfo AsnDecodeStatus_t477_il2cpp_TypeInfo;
// System.Security.Cryptography.AsnDecodeStatus
#include "System_System_Security_Cryptography_AsnDecodeStatusMethodDeclarations.h"
static const EncodedMethodIndex AsnDecodeStatus_t477_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair AsnDecodeStatus_t477_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType AsnDecodeStatus_t477_0_0_0;
extern const Il2CppType AsnDecodeStatus_t477_1_0_0;
const Il2CppTypeDefinitionMetadata AsnDecodeStatus_t477_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AsnDecodeStatus_t477_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, AsnDecodeStatus_t477_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1550/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AsnDecodeStatus_t477_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsnDecodeStatus"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AsnDecodeStatus_t477_0_0_0/* byval_arg */
	, &AsnDecodeStatus_t477_1_0_0/* this_arg */
	, &AsnDecodeStatus_t477_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsnDecodeStatus_t477)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AsnDecodeStatus_t477)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.AsnEncodedData
#include "System_System_Security_Cryptography_AsnEncodedData.h"
// Metadata Definition System.Security.Cryptography.AsnEncodedData
extern TypeInfo AsnEncodedData_t437_il2cpp_TypeInfo;
// System.Security.Cryptography.AsnEncodedData
#include "System_System_Security_Cryptography_AsnEncodedDataMethodDeclarations.h"
static const EncodedMethodIndex AsnEncodedData_t437_VTable[6] = 
{
	120,
	125,
	122,
	123,
	542,
	543,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType AsnEncodedData_t437_1_0_0;
struct AsnEncodedData_t437;
const Il2CppTypeDefinitionMetadata AsnEncodedData_t437_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsnEncodedData_t437_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1557/* fieldStart */
	, 1628/* methodStart */
	, -1/* eventStart */
	, 393/* propertyStart */

};
TypeInfo AsnEncodedData_t437_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsnEncodedData"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AsnEncodedData_t437_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AsnEncodedData_t437_0_0_0/* byval_arg */
	, &AsnEncodedData_t437_1_0_0/* this_arg */
	, &AsnEncodedData_t437_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsnEncodedData_t437)/* instance_size */
	, sizeof (AsnEncodedData_t437)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AsnEncodedData_t437_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.Oid
#include "System_System_Security_Cryptography_Oid.h"
// Metadata Definition System.Security.Cryptography.Oid
extern TypeInfo Oid_t438_il2cpp_TypeInfo;
// System.Security.Cryptography.Oid
#include "System_System_Security_Cryptography_OidMethodDeclarations.h"
static const EncodedMethodIndex Oid_t438_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Oid_t438_0_0_0;
extern const Il2CppType Oid_t438_1_0_0;
struct Oid_t438;
const Il2CppTypeDefinitionMetadata Oid_t438_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Oid_t438_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1560/* fieldStart */
	, 1644/* methodStart */
	, -1/* eventStart */
	, 395/* propertyStart */

};
TypeInfo Oid_t438_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Oid"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Oid_t438_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Oid_t438_0_0_0/* byval_arg */
	, &Oid_t438_1_0_0/* this_arg */
	, &Oid_t438_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Oid_t438)/* instance_size */
	, sizeof (Oid_t438)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Oid_t438_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.OidCollection
#include "System_System_Security_Cryptography_OidCollection.h"
// Metadata Definition System.Security.Cryptography.OidCollection
extern TypeInfo OidCollection_t462_il2cpp_TypeInfo;
// System.Security.Cryptography.OidCollection
#include "System_System_Security_Cryptography_OidCollectionMethodDeclarations.h"
static const EncodedMethodIndex OidCollection_t462_VTable[8] = 
{
	120,
	125,
	122,
	123,
	611,
	612,
	613,
	614,
};
static const Il2CppType* OidCollection_t462_InterfacesTypeInfos[] = 
{
	&ICollection_t569_0_0_0,
	&IEnumerable_t302_0_0_0,
};
static Il2CppInterfaceOffsetPair OidCollection_t462_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IEnumerable_t302_0_0_0, 7},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType OidCollection_t462_0_0_0;
extern const Il2CppType OidCollection_t462_1_0_0;
struct OidCollection_t462;
const Il2CppTypeDefinitionMetadata OidCollection_t462_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, OidCollection_t462_InterfacesTypeInfos/* implementedInterfaces */
	, OidCollection_t462_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OidCollection_t462_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1563/* fieldStart */
	, 1651/* methodStart */
	, -1/* eventStart */
	, 397/* propertyStart */

};
TypeInfo OidCollection_t462_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "OidCollection"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &OidCollection_t462_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 491/* custom_attributes_cache */
	, &OidCollection_t462_0_0_0/* byval_arg */
	, &OidCollection_t462_1_0_0/* this_arg */
	, &OidCollection_t462_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OidCollection_t462)/* instance_size */
	, sizeof (OidCollection_t462)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.OidEnumerator
#include "System_System_Security_Cryptography_OidEnumerator.h"
// Metadata Definition System.Security.Cryptography.OidEnumerator
extern TypeInfo OidEnumerator_t478_il2cpp_TypeInfo;
// System.Security.Cryptography.OidEnumerator
#include "System_System_Security_Cryptography_OidEnumeratorMethodDeclarations.h"
static const EncodedMethodIndex OidEnumerator_t478_VTable[6] = 
{
	120,
	125,
	122,
	123,
	615,
	616,
};
static const Il2CppType* OidEnumerator_t478_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair OidEnumerator_t478_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType OidEnumerator_t478_0_0_0;
extern const Il2CppType OidEnumerator_t478_1_0_0;
struct OidEnumerator_t478;
const Il2CppTypeDefinitionMetadata OidEnumerator_t478_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, OidEnumerator_t478_InterfacesTypeInfos/* implementedInterfaces */
	, OidEnumerator_t478_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OidEnumerator_t478_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1565/* fieldStart */
	, 1658/* methodStart */
	, -1/* eventStart */
	, 400/* propertyStart */

};
TypeInfo OidEnumerator_t478_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "OidEnumerator"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &OidEnumerator_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OidEnumerator_t478_0_0_0/* byval_arg */
	, &OidEnumerator_t478_1_0_0/* this_arg */
	, &OidEnumerator_t478_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OidEnumerator_t478)/* instance_size */
	, sizeof (OidEnumerator_t478)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.BaseMachine
#include "System_System_Text_RegularExpressions_BaseMachine.h"
// Metadata Definition System.Text.RegularExpressions.BaseMachine
extern TypeInfo BaseMachine_t481_il2cpp_TypeInfo;
// System.Text.RegularExpressions.BaseMachine
#include "System_System_Text_RegularExpressions_BaseMachineMethodDeclarations.h"
extern const Il2CppType MatchAppendEvaluator_t480_0_0_0;
static const Il2CppType* BaseMachine_t481_il2cpp_TypeInfo__nestedTypes[1] =
{
	&MatchAppendEvaluator_t480_0_0_0,
};
static const EncodedMethodIndex BaseMachine_t481_VTable[8] = 
{
	120,
	125,
	122,
	123,
	617,
	618,
	618,
	617,
};
extern const Il2CppType IMachine_t488_0_0_0;
static const Il2CppType* BaseMachine_t481_InterfacesTypeInfos[] = 
{
	&IMachine_t488_0_0_0,
};
static Il2CppInterfaceOffsetPair BaseMachine_t481_InterfacesOffsets[] = 
{
	{ &IMachine_t488_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType BaseMachine_t481_0_0_0;
extern const Il2CppType BaseMachine_t481_1_0_0;
struct BaseMachine_t481;
const Il2CppTypeDefinitionMetadata BaseMachine_t481_DefinitionMetadata = 
{
	NULL/* declaringType */
	, BaseMachine_t481_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, BaseMachine_t481_InterfacesTypeInfos/* implementedInterfaces */
	, BaseMachine_t481_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseMachine_t481_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1567/* fieldStart */
	, 1661/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BaseMachine_t481_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseMachine"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &BaseMachine_t481_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseMachine_t481_0_0_0/* byval_arg */
	, &BaseMachine_t481_1_0_0/* this_arg */
	, &BaseMachine_t481_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseMachine_t481)/* instance_size */
	, sizeof (BaseMachine_t481)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
#include "System_System_Text_RegularExpressions_BaseMachine_MatchAppen.h"
// Metadata Definition System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
extern TypeInfo MatchAppendEvaluator_t480_il2cpp_TypeInfo;
// System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
#include "System_System_Text_RegularExpressions_BaseMachine_MatchAppenMethodDeclarations.h"
static const EncodedMethodIndex MatchAppendEvaluator_t480_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	619,
	620,
	621,
};
extern const Il2CppType ICloneable_t2056_0_0_0;
static Il2CppInterfaceOffsetPair MatchAppendEvaluator_t480_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MatchAppendEvaluator_t480_1_0_0;
extern const Il2CppType MulticastDelegate_t47_0_0_0;
struct MatchAppendEvaluator_t480;
const Il2CppTypeDefinitionMetadata MatchAppendEvaluator_t480_DefinitionMetadata = 
{
	&BaseMachine_t481_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MatchAppendEvaluator_t480_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, MatchAppendEvaluator_t480_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1666/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MatchAppendEvaluator_t480_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchAppendEvaluator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MatchAppendEvaluator_t480_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchAppendEvaluator_t480_0_0_0/* byval_arg */
	, &MatchAppendEvaluator_t480_1_0_0/* this_arg */
	, &MatchAppendEvaluator_t480_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_MatchAppendEvaluator_t480/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchAppendEvaluator_t480)/* instance_size */
	, sizeof (MatchAppendEvaluator_t480)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 261/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.Capture
#include "System_System_Text_RegularExpressions_Capture.h"
// Metadata Definition System.Text.RegularExpressions.Capture
extern TypeInfo Capture_t482_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Capture
#include "System_System_Text_RegularExpressions_CaptureMethodDeclarations.h"
static const EncodedMethodIndex Capture_t482_VTable[4] = 
{
	120,
	125,
	122,
	622,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Capture_t482_0_0_0;
extern const Il2CppType Capture_t482_1_0_0;
struct Capture_t482;
const Il2CppTypeDefinitionMetadata Capture_t482_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Capture_t482_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1568/* fieldStart */
	, 1670/* methodStart */
	, -1/* eventStart */
	, 401/* propertyStart */

};
TypeInfo Capture_t482_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Capture"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Capture_t482_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Capture_t482_0_0_0/* byval_arg */
	, &Capture_t482_1_0_0/* this_arg */
	, &Capture_t482_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Capture_t482)/* instance_size */
	, sizeof (Capture_t482)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.CaptureCollection
#include "System_System_Text_RegularExpressions_CaptureCollection.h"
// Metadata Definition System.Text.RegularExpressions.CaptureCollection
extern TypeInfo CaptureCollection_t484_il2cpp_TypeInfo;
// System.Text.RegularExpressions.CaptureCollection
#include "System_System_Text_RegularExpressions_CaptureCollectionMethodDeclarations.h"
static const EncodedMethodIndex CaptureCollection_t484_VTable[8] = 
{
	120,
	125,
	122,
	123,
	623,
	624,
	625,
	626,
};
static const Il2CppType* CaptureCollection_t484_InterfacesTypeInfos[] = 
{
	&ICollection_t569_0_0_0,
	&IEnumerable_t302_0_0_0,
};
static Il2CppInterfaceOffsetPair CaptureCollection_t484_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IEnumerable_t302_0_0_0, 7},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CaptureCollection_t484_0_0_0;
extern const Il2CppType CaptureCollection_t484_1_0_0;
struct CaptureCollection_t484;
const Il2CppTypeDefinitionMetadata CaptureCollection_t484_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CaptureCollection_t484_InterfacesTypeInfos/* implementedInterfaces */
	, CaptureCollection_t484_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CaptureCollection_t484_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1571/* fieldStart */
	, 1677/* methodStart */
	, -1/* eventStart */
	, 405/* propertyStart */

};
TypeInfo CaptureCollection_t484_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CaptureCollection"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &CaptureCollection_t484_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 492/* custom_attributes_cache */
	, &CaptureCollection_t484_0_0_0/* byval_arg */
	, &CaptureCollection_t484_1_0_0/* this_arg */
	, &CaptureCollection_t484_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CaptureCollection_t484)/* instance_size */
	, sizeof (CaptureCollection_t484)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.Group
#include "System_System_Text_RegularExpressions_Group.h"
// Metadata Definition System.Text.RegularExpressions.Group
extern TypeInfo Group_t485_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Group
#include "System_System_Text_RegularExpressions_GroupMethodDeclarations.h"
static const EncodedMethodIndex Group_t485_VTable[4] = 
{
	120,
	125,
	122,
	622,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Group_t485_0_0_0;
extern const Il2CppType Group_t485_1_0_0;
struct Group_t485;
const Il2CppTypeDefinitionMetadata Group_t485_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Capture_t482_0_0_0/* parent */
	, Group_t485_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1572/* fieldStart */
	, 1683/* methodStart */
	, -1/* eventStart */
	, 407/* propertyStart */

};
TypeInfo Group_t485_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Group"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Group_t485_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Group_t485_0_0_0/* byval_arg */
	, &Group_t485_1_0_0/* this_arg */
	, &Group_t485_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Group_t485)/* instance_size */
	, sizeof (Group_t485)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Group_t485_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.GroupCollection
#include "System_System_Text_RegularExpressions_GroupCollection.h"
// Metadata Definition System.Text.RegularExpressions.GroupCollection
extern TypeInfo GroupCollection_t487_il2cpp_TypeInfo;
// System.Text.RegularExpressions.GroupCollection
#include "System_System_Text_RegularExpressions_GroupCollectionMethodDeclarations.h"
static const EncodedMethodIndex GroupCollection_t487_VTable[8] = 
{
	120,
	125,
	122,
	123,
	627,
	628,
	629,
	630,
};
static const Il2CppType* GroupCollection_t487_InterfacesTypeInfos[] = 
{
	&ICollection_t569_0_0_0,
	&IEnumerable_t302_0_0_0,
};
static Il2CppInterfaceOffsetPair GroupCollection_t487_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IEnumerable_t302_0_0_0, 7},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType GroupCollection_t487_0_0_0;
extern const Il2CppType GroupCollection_t487_1_0_0;
struct GroupCollection_t487;
const Il2CppTypeDefinitionMetadata GroupCollection_t487_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, GroupCollection_t487_InterfacesTypeInfos/* implementedInterfaces */
	, GroupCollection_t487_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GroupCollection_t487_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1575/* fieldStart */
	, 1689/* methodStart */
	, -1/* eventStart */
	, 409/* propertyStart */

};
TypeInfo GroupCollection_t487_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "GroupCollection"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &GroupCollection_t487_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 493/* custom_attributes_cache */
	, &GroupCollection_t487_0_0_0/* byval_arg */
	, &GroupCollection_t487_1_0_0/* this_arg */
	, &GroupCollection_t487_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GroupCollection_t487)/* instance_size */
	, sizeof (GroupCollection_t487)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.Match
#include "System_System_Text_RegularExpressions_Match.h"
// Metadata Definition System.Text.RegularExpressions.Match
extern TypeInfo Match_t479_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Match
#include "System_System_Text_RegularExpressions_MatchMethodDeclarations.h"
static const EncodedMethodIndex Match_t479_VTable[5] = 
{
	120,
	125,
	122,
	622,
	631,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Match_t479_0_0_0;
extern const Il2CppType Match_t479_1_0_0;
struct Match_t479;
const Il2CppTypeDefinitionMetadata Match_t479_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Group_t485_0_0_0/* parent */
	, Match_t479_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1577/* fieldStart */
	, 1696/* methodStart */
	, -1/* eventStart */
	, 412/* propertyStart */

};
TypeInfo Match_t479_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Match"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Match_t479_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Match_t479_0_0_0/* byval_arg */
	, &Match_t479_1_0_0/* this_arg */
	, &Match_t479_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Match_t479)/* instance_size */
	, sizeof (Match_t479)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Match_t479_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.MatchCollection
#include "System_System_Text_RegularExpressions_MatchCollection.h"
// Metadata Definition System.Text.RegularExpressions.MatchCollection
extern TypeInfo MatchCollection_t489_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MatchCollection
#include "System_System_Text_RegularExpressions_MatchCollectionMethodDeclarations.h"
extern const Il2CppType Enumerator_t490_0_0_0;
static const Il2CppType* MatchCollection_t489_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t490_0_0_0,
};
static const EncodedMethodIndex MatchCollection_t489_VTable[9] = 
{
	120,
	125,
	122,
	123,
	632,
	633,
	634,
	635,
	636,
};
static const Il2CppType* MatchCollection_t489_InterfacesTypeInfos[] = 
{
	&ICollection_t569_0_0_0,
	&IEnumerable_t302_0_0_0,
};
static Il2CppInterfaceOffsetPair MatchCollection_t489_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IEnumerable_t302_0_0_0, 7},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MatchCollection_t489_0_0_0;
extern const Il2CppType MatchCollection_t489_1_0_0;
struct MatchCollection_t489;
const Il2CppTypeDefinitionMetadata MatchCollection_t489_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MatchCollection_t489_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, MatchCollection_t489_InterfacesTypeInfos/* implementedInterfaces */
	, MatchCollection_t489_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MatchCollection_t489_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1582/* fieldStart */
	, 1704/* methodStart */
	, -1/* eventStart */
	, 415/* propertyStart */

};
TypeInfo MatchCollection_t489_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchCollection"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &MatchCollection_t489_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 494/* custom_attributes_cache */
	, &MatchCollection_t489_0_0_0/* byval_arg */
	, &MatchCollection_t489_1_0_0/* this_arg */
	, &MatchCollection_t489_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchCollection_t489)/* instance_size */
	, sizeof (MatchCollection_t489)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.MatchCollection/Enumerator
#include "System_System_Text_RegularExpressions_MatchCollection_Enumer.h"
// Metadata Definition System.Text.RegularExpressions.MatchCollection/Enumerator
extern TypeInfo Enumerator_t490_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MatchCollection/Enumerator
#include "System_System_Text_RegularExpressions_MatchCollection_EnumerMethodDeclarations.h"
static const EncodedMethodIndex Enumerator_t490_VTable[6] = 
{
	120,
	125,
	122,
	123,
	637,
	638,
};
static const Il2CppType* Enumerator_t490_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t490_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Enumerator_t490_1_0_0;
struct Enumerator_t490;
const Il2CppTypeDefinitionMetadata Enumerator_t490_DefinitionMetadata = 
{
	&MatchCollection_t489_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t490_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t490_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t490_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1584/* fieldStart */
	, 1712/* methodStart */
	, -1/* eventStart */
	, 419/* propertyStart */

};
TypeInfo Enumerator_t490_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t490_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t490_0_0_0/* byval_arg */
	, &Enumerator_t490_1_0_0/* this_arg */
	, &Enumerator_t490_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t490)/* instance_size */
	, sizeof (Enumerator_t490)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_Regex.h"
// Metadata Definition System.Text.RegularExpressions.Regex
extern TypeInfo Regex_t380_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
static const EncodedMethodIndex Regex_t380_VTable[5] = 
{
	120,
	125,
	122,
	639,
	640,
};
static const Il2CppType* Regex_t380_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair Regex_t380_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Regex_t380_0_0_0;
extern const Il2CppType Regex_t380_1_0_0;
struct Regex_t380;
const Il2CppTypeDefinitionMetadata Regex_t380_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Regex_t380_InterfacesTypeInfos/* implementedInterfaces */
	, Regex_t380_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Regex_t380_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1586/* fieldStart */
	, 1715/* methodStart */
	, -1/* eventStart */
	, 420/* propertyStart */

};
TypeInfo Regex_t380_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Regex"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Regex_t380_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Regex_t380_0_0_0/* byval_arg */
	, &Regex_t380_1_0_0/* this_arg */
	, &Regex_t380_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Regex_t380)/* instance_size */
	, sizeof (Regex_t380)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Regex_t380_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 5/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.RegexOptions
#include "System_System_Text_RegularExpressions_RegexOptions.h"
// Metadata Definition System.Text.RegularExpressions.RegexOptions
extern TypeInfo RegexOptions_t496_il2cpp_TypeInfo;
// System.Text.RegularExpressions.RegexOptions
#include "System_System_Text_RegularExpressions_RegexOptionsMethodDeclarations.h"
static const EncodedMethodIndex RegexOptions_t496_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair RegexOptions_t496_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RegexOptions_t496_0_0_0;
extern const Il2CppType RegexOptions_t496_1_0_0;
const Il2CppTypeDefinitionMetadata RegexOptions_t496_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RegexOptions_t496_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, RegexOptions_t496_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1600/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RegexOptions_t496_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RegexOptions"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 499/* custom_attributes_cache */
	, &RegexOptions_t496_0_0_0/* byval_arg */
	, &RegexOptions_t496_1_0_0/* this_arg */
	, &RegexOptions_t496_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RegexOptions_t496)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RegexOptions_t496)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.OpCode
#include "System_System_Text_RegularExpressions_OpCode.h"
// Metadata Definition System.Text.RegularExpressions.OpCode
extern TypeInfo OpCode_t497_il2cpp_TypeInfo;
// System.Text.RegularExpressions.OpCode
#include "System_System_Text_RegularExpressions_OpCodeMethodDeclarations.h"
static const EncodedMethodIndex OpCode_t497_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair OpCode_t497_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType OpCode_t497_0_0_0;
extern const Il2CppType OpCode_t497_1_0_0;
// System.UInt16
#include "mscorlib_System_UInt16.h"
extern TypeInfo UInt16_t351_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata OpCode_t497_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OpCode_t497_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, OpCode_t497_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1610/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OpCode_t497_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "OpCode"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &UInt16_t351_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OpCode_t497_0_0_0/* byval_arg */
	, &OpCode_t497_1_0_0/* this_arg */
	, &OpCode_t497_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OpCode_t497)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (OpCode_t497)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 26/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.OpFlags
#include "System_System_Text_RegularExpressions_OpFlags.h"
// Metadata Definition System.Text.RegularExpressions.OpFlags
extern TypeInfo OpFlags_t498_il2cpp_TypeInfo;
// System.Text.RegularExpressions.OpFlags
#include "System_System_Text_RegularExpressions_OpFlagsMethodDeclarations.h"
static const EncodedMethodIndex OpFlags_t498_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair OpFlags_t498_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType OpFlags_t498_0_0_0;
extern const Il2CppType OpFlags_t498_1_0_0;
const Il2CppTypeDefinitionMetadata OpFlags_t498_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OpFlags_t498_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, OpFlags_t498_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1636/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OpFlags_t498_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "OpFlags"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &UInt16_t351_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 500/* custom_attributes_cache */
	, &OpFlags_t498_0_0_0/* byval_arg */
	, &OpFlags_t498_1_0_0/* this_arg */
	, &OpFlags_t498_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OpFlags_t498)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (OpFlags_t498)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.Position
#include "System_System_Text_RegularExpressions_Position.h"
// Metadata Definition System.Text.RegularExpressions.Position
extern TypeInfo Position_t499_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Position
#include "System_System_Text_RegularExpressions_PositionMethodDeclarations.h"
static const EncodedMethodIndex Position_t499_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair Position_t499_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Position_t499_0_0_0;
extern const Il2CppType Position_t499_1_0_0;
const Il2CppTypeDefinitionMetadata Position_t499_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Position_t499_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, Position_t499_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1642/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Position_t499_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Position"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &UInt16_t351_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Position_t499_0_0_0/* byval_arg */
	, &Position_t499_1_0_0/* this_arg */
	, &Position_t499_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Position_t499)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Position_t499)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Text.RegularExpressions.IMachine
extern TypeInfo IMachine_t488_il2cpp_TypeInfo;
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IMachine_t488_1_0_0;
struct IMachine_t488;
const Il2CppTypeDefinitionMetadata IMachine_t488_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1745/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMachine_t488_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMachine"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &IMachine_t488_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMachine_t488_0_0_0/* byval_arg */
	, &IMachine_t488_1_0_0/* this_arg */
	, &IMachine_t488_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Text.RegularExpressions.IMachineFactory
extern TypeInfo IMachineFactory_t492_il2cpp_TypeInfo;
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IMachineFactory_t492_0_0_0;
extern const Il2CppType IMachineFactory_t492_1_0_0;
struct IMachineFactory_t492;
const Il2CppTypeDefinitionMetadata IMachineFactory_t492_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1747/* methodStart */
	, -1/* eventStart */
	, 425/* propertyStart */

};
TypeInfo IMachineFactory_t492_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMachineFactory"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &IMachineFactory_t492_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMachineFactory_t492_0_0_0/* byval_arg */
	, &IMachineFactory_t492_1_0_0/* this_arg */
	, &IMachineFactory_t492_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.FactoryCache
#include "System_System_Text_RegularExpressions_FactoryCache.h"
// Metadata Definition System.Text.RegularExpressions.FactoryCache
extern TypeInfo FactoryCache_t491_il2cpp_TypeInfo;
// System.Text.RegularExpressions.FactoryCache
#include "System_System_Text_RegularExpressions_FactoryCacheMethodDeclarations.h"
extern const Il2CppType Key_t500_0_0_0;
static const Il2CppType* FactoryCache_t491_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Key_t500_0_0_0,
};
static const EncodedMethodIndex FactoryCache_t491_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType FactoryCache_t491_0_0_0;
extern const Il2CppType FactoryCache_t491_1_0_0;
struct FactoryCache_t491;
const Il2CppTypeDefinitionMetadata FactoryCache_t491_DefinitionMetadata = 
{
	NULL/* declaringType */
	, FactoryCache_t491_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FactoryCache_t491_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1653/* fieldStart */
	, 1755/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FactoryCache_t491_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FactoryCache"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &FactoryCache_t491_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FactoryCache_t491_0_0_0/* byval_arg */
	, &FactoryCache_t491_1_0_0/* this_arg */
	, &FactoryCache_t491_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FactoryCache_t491)/* instance_size */
	, sizeof (FactoryCache_t491)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
