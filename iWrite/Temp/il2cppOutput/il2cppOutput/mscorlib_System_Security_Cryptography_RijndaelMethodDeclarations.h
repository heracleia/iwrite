﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.Rijndael
struct Rijndael_t752;
// System.String
struct String_t;

// System.Void System.Security.Cryptography.Rijndael::.ctor()
extern "C" void Rijndael__ctor_m6860 (Rijndael_t752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Rijndael System.Security.Cryptography.Rijndael::Create()
extern "C" Rijndael_t752 * Rijndael_Create_m3336 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Rijndael System.Security.Cryptography.Rijndael::Create(System.String)
extern "C" Rijndael_t752 * Rijndael_Create_m6861 (Object_t * __this /* static, unused */, String_t* ___algName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
