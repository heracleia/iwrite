﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Enumerator_t1585;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Dictionary_2_t1580;
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m10023_gshared (Enumerator_t1585 * __this, Dictionary_2_t1580 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m10023(__this, ___host, method) (( void (*) (Enumerator_t1585 *, Dictionary_2_t1580 *, const MethodInfo*))Enumerator__ctor_m10023_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m10024_gshared (Enumerator_t1585 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m10024(__this, method) (( Object_t * (*) (Enumerator_t1585 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m10024_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m10025_gshared (Enumerator_t1585 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m10025(__this, method) (( void (*) (Enumerator_t1585 *, const MethodInfo*))Enumerator_Dispose_m10025_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m10026_gshared (Enumerator_t1585 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m10026(__this, method) (( bool (*) (Enumerator_t1585 *, const MethodInfo*))Enumerator_MoveNext_m10026_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Current()
extern "C" uint64_t Enumerator_get_Current_m10027_gshared (Enumerator_t1585 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m10027(__this, method) (( uint64_t (*) (Enumerator_t1585 *, const MethodInfo*))Enumerator_get_Current_m10027_gshared)(__this, method)
