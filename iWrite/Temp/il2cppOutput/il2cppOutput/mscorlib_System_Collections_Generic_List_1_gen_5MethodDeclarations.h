﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t152;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t1844;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t288;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void List_1__ctor_m9479_gshared (List_1_t152 * __this, const MethodInfo* method);
#define List_1__ctor_m9479(__this, method) (( void (*) (List_1_t152 *, const MethodInfo*))List_1__ctor_m9479_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1232_gshared (List_1_t152 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1232(__this, ___capacity, method) (( void (*) (List_1_t152 *, int32_t, const MethodInfo*))List_1__ctor_m1232_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void List_1__cctor_m9480_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m9480(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m9480_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9481_gshared (List_1_t152 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9481(__this, method) (( Object_t* (*) (List_1_t152 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9481_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m9482_gshared (List_1_t152 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m9482(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t152 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m9482_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m9483_gshared (List_1_t152 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m9483(__this, method) (( Object_t * (*) (List_1_t152 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m9483_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m9484_gshared (List_1_t152 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m9484(__this, ___item, method) (( int32_t (*) (List_1_t152 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m9484_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m9485_gshared (List_1_t152 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m9485(__this, ___item, method) (( bool (*) (List_1_t152 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m9485_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m9486_gshared (List_1_t152 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m9486(__this, ___item, method) (( int32_t (*) (List_1_t152 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m9486_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m9487_gshared (List_1_t152 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m9487(__this, ___index, ___item, method) (( void (*) (List_1_t152 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m9487_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m9488_gshared (List_1_t152 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m9488(__this, ___item, method) (( void (*) (List_1_t152 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m9488_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9489_gshared (List_1_t152 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9489(__this, method) (( bool (*) (List_1_t152 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9489_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m9490_gshared (List_1_t152 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m9490(__this, method) (( Object_t * (*) (List_1_t152 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m9490_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m9491_gshared (List_1_t152 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m9491(__this, ___index, method) (( Object_t * (*) (List_1_t152 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m9491_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m9492_gshared (List_1_t152 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m9492(__this, ___index, ___value, method) (( void (*) (List_1_t152 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m9492_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void List_1_Add_m9493_gshared (List_1_t152 * __this, UICharInfo_t149  ___item, const MethodInfo* method);
#define List_1_Add_m9493(__this, ___item, method) (( void (*) (List_1_t152 *, UICharInfo_t149 , const MethodInfo*))List_1_Add_m9493_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m9494_gshared (List_1_t152 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m9494(__this, ___newCount, method) (( void (*) (List_1_t152 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m9494_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Clear()
extern "C" void List_1_Clear_m9495_gshared (List_1_t152 * __this, const MethodInfo* method);
#define List_1_Clear_m9495(__this, method) (( void (*) (List_1_t152 *, const MethodInfo*))List_1_Clear_m9495_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool List_1_Contains_m9496_gshared (List_1_t152 * __this, UICharInfo_t149  ___item, const MethodInfo* method);
#define List_1_Contains_m9496(__this, ___item, method) (( bool (*) (List_1_t152 *, UICharInfo_t149 , const MethodInfo*))List_1_Contains_m9496_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m9497_gshared (List_1_t152 * __this, UICharInfoU5BU5D_t288* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m9497(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t152 *, UICharInfoU5BU5D_t288*, int32_t, const MethodInfo*))List_1_CopyTo_m9497_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Enumerator_t1542  List_1_GetEnumerator_m9498_gshared (List_1_t152 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m9498(__this, method) (( Enumerator_t1542  (*) (List_1_t152 *, const MethodInfo*))List_1_GetEnumerator_m9498_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m9499_gshared (List_1_t152 * __this, UICharInfo_t149  ___item, const MethodInfo* method);
#define List_1_IndexOf_m9499(__this, ___item, method) (( int32_t (*) (List_1_t152 *, UICharInfo_t149 , const MethodInfo*))List_1_IndexOf_m9499_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m9500_gshared (List_1_t152 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m9500(__this, ___start, ___delta, method) (( void (*) (List_1_t152 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m9500_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m9501_gshared (List_1_t152 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m9501(__this, ___index, method) (( void (*) (List_1_t152 *, int32_t, const MethodInfo*))List_1_CheckIndex_m9501_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m9502_gshared (List_1_t152 * __this, int32_t ___index, UICharInfo_t149  ___item, const MethodInfo* method);
#define List_1_Insert_m9502(__this, ___index, ___item, method) (( void (*) (List_1_t152 *, int32_t, UICharInfo_t149 , const MethodInfo*))List_1_Insert_m9502_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool List_1_Remove_m9503_gshared (List_1_t152 * __this, UICharInfo_t149  ___item, const MethodInfo* method);
#define List_1_Remove_m9503(__this, ___item, method) (( bool (*) (List_1_t152 *, UICharInfo_t149 , const MethodInfo*))List_1_Remove_m9503_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m9504_gshared (List_1_t152 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m9504(__this, ___index, method) (( void (*) (List_1_t152 *, int32_t, const MethodInfo*))List_1_RemoveAt_m9504_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UICharInfo>::ToArray()
extern "C" UICharInfoU5BU5D_t288* List_1_ToArray_m9505_gshared (List_1_t152 * __this, const MethodInfo* method);
#define List_1_ToArray_m9505(__this, method) (( UICharInfoU5BU5D_t288* (*) (List_1_t152 *, const MethodInfo*))List_1_ToArray_m9505_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m9506_gshared (List_1_t152 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m9506(__this, method) (( int32_t (*) (List_1_t152 *, const MethodInfo*))List_1_get_Capacity_m9506_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m9507_gshared (List_1_t152 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m9507(__this, ___value, method) (( void (*) (List_1_t152 *, int32_t, const MethodInfo*))List_1_set_Capacity_m9507_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m9508_gshared (List_1_t152 * __this, const MethodInfo* method);
#define List_1_get_Count_m9508(__this, method) (( int32_t (*) (List_1_t152 *, const MethodInfo*))List_1_get_Count_m9508_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t149  List_1_get_Item_m9509_gshared (List_1_t152 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m9509(__this, ___index, method) (( UICharInfo_t149  (*) (List_1_t152 *, int32_t, const MethodInfo*))List_1_get_Item_m9509_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m9510_gshared (List_1_t152 * __this, int32_t ___index, UICharInfo_t149  ___value, const MethodInfo* method);
#define List_1_set_Item_m9510(__this, ___index, ___value, method) (( void (*) (List_1_t152 *, int32_t, UICharInfo_t149 , const MethodInfo*))List_1_set_Item_m9510_gshared)(__this, ___index, ___value, method)
