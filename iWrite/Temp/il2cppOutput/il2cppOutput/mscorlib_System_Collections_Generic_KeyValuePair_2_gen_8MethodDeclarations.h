﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>
struct KeyValuePair_2_t1520;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m9175(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1520 *, String_t*, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m9095_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m9176(__this, method) (( String_t* (*) (KeyValuePair_2_t1520 *, const MethodInfo*))KeyValuePair_2_get_Key_m9096_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m9177(__this, ___value, method) (( void (*) (KeyValuePair_2_t1520 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m9097_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m9178(__this, method) (( int32_t (*) (KeyValuePair_2_t1520 *, const MethodInfo*))KeyValuePair_2_get_Value_m9098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m9179(__this, ___value, method) (( void (*) (KeyValuePair_2_t1520 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m9099_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m9180(__this, method) (( String_t* (*) (KeyValuePair_2_t1520 *, const MethodInfo*))KeyValuePair_2_ToString_m9100_gshared)(__this, method)
