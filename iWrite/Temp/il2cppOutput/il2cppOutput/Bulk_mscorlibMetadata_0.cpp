﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// <Module>
#include "mscorlib_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t756_il2cpp_TypeInfo;
// <Module>
#include "mscorlib_U3CModuleU3EMethodDeclarations.h"
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U3CModuleU3E_t756_0_0_0;
extern const Il2CppType U3CModuleU3E_t756_1_0_0;
struct U3CModuleU3E_t756;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t756_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CModuleU3E_t756_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CModuleU3E_t756_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t756_0_0_0/* byval_arg */
	, &U3CModuleU3E_t756_1_0_0/* this_arg */
	, &U3CModuleU3E_t756_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t756)/* instance_size */
	, sizeof (U3CModuleU3E_t756)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Object
#include "mscorlib_System_Object.h"
// Metadata Definition System.Object
extern TypeInfo Object_t_il2cpp_TypeInfo;
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
static const EncodedMethodIndex Object_t_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_1_0_0;
struct Object_t;
const Il2CppTypeDefinitionMetadata Object_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, Object_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3094/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Object_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Object"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Object_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 557/* custom_attributes_cache */
	, &Object_t_0_0_0/* byval_arg */
	, &Object_t_1_0_0/* this_arg */
	, &Object_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Object_t)/* instance_size */
	, sizeof (Object_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Metadata Definition System.ValueType
extern TypeInfo ValueType_t757_il2cpp_TypeInfo;
// System.ValueType
#include "mscorlib_System_ValueTypeMethodDeclarations.h"
static const EncodedMethodIndex ValueType_t757_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ValueType_t757_0_0_0;
extern const Il2CppType ValueType_t757_1_0_0;
struct ValueType_t757;
const Il2CppTypeDefinitionMetadata ValueType_t757_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ValueType_t757_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3104/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ValueType_t757_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ValueType"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ValueType_t757_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 561/* custom_attributes_cache */
	, &ValueType_t757_0_0_0/* byval_arg */
	, &ValueType_t757_1_0_0/* this_arg */
	, &ValueType_t757_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ValueType_t757)/* instance_size */
	, sizeof (ValueType_t757)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Attribute
#include "mscorlib_System_Attribute.h"
// Metadata Definition System.Attribute
extern TypeInfo Attribute_t96_il2cpp_TypeInfo;
// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"
static const EncodedMethodIndex Attribute_t96_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
extern const Il2CppType _Attribute_t2052_0_0_0;
static const Il2CppType* Attribute_t96_InterfacesTypeInfos[] = 
{
	&_Attribute_t2052_0_0_0,
};
static Il2CppInterfaceOffsetPair Attribute_t96_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Attribute_t96_0_0_0;
extern const Il2CppType Attribute_t96_1_0_0;
struct Attribute_t96;
const Il2CppTypeDefinitionMetadata Attribute_t96_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Attribute_t96_InterfacesTypeInfos/* implementedInterfaces */
	, Attribute_t96_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Attribute_t96_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3111/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Attribute_t96_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Attribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Attribute_t96_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 562/* custom_attributes_cache */
	, &Attribute_t96_0_0_0/* byval_arg */
	, &Attribute_t96_1_0_0/* this_arg */
	, &Attribute_t96_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Attribute_t96)/* instance_size */
	, sizeof (Attribute_t96)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Attribute
extern TypeInfo _Attribute_t2052_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Attribute_t2052_1_0_0;
struct _Attribute_t2052;
const Il2CppTypeDefinitionMetadata _Attribute_t2052_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Attribute_t2052_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Attribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_Attribute_t2052_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 563/* custom_attributes_cache */
	, &_Attribute_t2052_0_0_0/* byval_arg */
	, &_Attribute_t2052_1_0_0/* this_arg */
	, &_Attribute_t2052_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Int32
#include "mscorlib_System_Int32.h"
// Metadata Definition System.Int32
extern TypeInfo Int32_t320_il2cpp_TypeInfo;
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
static const EncodedMethodIndex Int32_t320_VTable[24] = 
{
	1041,
	125,
	1042,
	1043,
	1044,
	1045,
	1046,
	1047,
	1048,
	1049,
	1050,
	1051,
	1052,
	1053,
	1054,
	1055,
	1056,
	1057,
	1058,
	1059,
	1060,
	1061,
	1062,
	1063,
};
extern const Il2CppType IFormattable_t1406_0_0_0;
extern const Il2CppType IConvertible_t1409_0_0_0;
extern const Il2CppType IComparable_t1408_0_0_0;
extern const Il2CppType IComparable_1_t2217_0_0_0;
extern const Il2CppType IEquatable_1_t2218_0_0_0;
static const Il2CppType* Int32_t320_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2217_0_0_0,
	&IEquatable_1_t2218_0_0_0,
};
static Il2CppInterfaceOffsetPair Int32_t320_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
	{ &IComparable_1_t2217_0_0_0, 22},
	{ &IEquatable_1_t2218_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Int32_t320_0_0_0;
extern const Il2CppType Int32_t320_1_0_0;
const Il2CppTypeDefinitionMetadata Int32_t320_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Int32_t320_InterfacesTypeInfos/* implementedInterfaces */
	, Int32_t320_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Int32_t320_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2491/* fieldStart */
	, 3121/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Int32_t320_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Int32"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 564/* custom_attributes_cache */
	, &Int32_t320_0_0_0/* byval_arg */
	, &Int32_t320_1_0_0/* this_arg */
	, &Int32_t320_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Int32_t320)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Int32_t320)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 40/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// Metadata Definition System.IFormattable
extern TypeInfo IFormattable_t1406_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormattable_t1406_1_0_0;
struct IFormattable_t1406;
const Il2CppTypeDefinitionMetadata IFormattable_t1406_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3161/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IFormattable_t1406_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormattable"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IFormattable_t1406_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 565/* custom_attributes_cache */
	, &IFormattable_t1406_0_0_0/* byval_arg */
	, &IFormattable_t1406_1_0_0/* this_arg */
	, &IFormattable_t1406_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IConvertible
extern TypeInfo IConvertible_t1409_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IConvertible_t1409_1_0_0;
struct IConvertible_t1409;
const Il2CppTypeDefinitionMetadata IConvertible_t1409_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3162/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IConvertible_t1409_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IConvertible"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IConvertible_t1409_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 566/* custom_attributes_cache */
	, &IConvertible_t1409_0_0_0/* byval_arg */
	, &IConvertible_t1409_1_0_0/* this_arg */
	, &IConvertible_t1409_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IComparable
extern TypeInfo IComparable_t1408_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IComparable_t1408_1_0_0;
struct IComparable_t1408;
const Il2CppTypeDefinitionMetadata IComparable_t1408_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3178/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IComparable_t1408_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IComparable_t1408_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 567/* custom_attributes_cache */
	, &IComparable_t1408_0_0_0/* byval_arg */
	, &IComparable_t1408_1_0_0/* this_arg */
	, &IComparable_t1408_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IComparable`1
extern TypeInfo IComparable_1_t2053_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IComparable_1_t2053_0_0_0;
extern const Il2CppType IComparable_1_t2053_1_0_0;
struct IComparable_1_t2053;
const Il2CppTypeDefinitionMetadata IComparable_1_t2053_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3179/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IComparable_1_t2053_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IComparable_1_t2053_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IComparable_1_t2053_0_0_0/* byval_arg */
	, &IComparable_1_t2053_1_0_0/* this_arg */
	, &IComparable_1_t2053_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 15/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.SerializableAttribute
#include "mscorlib_System_SerializableAttribute.h"
// Metadata Definition System.SerializableAttribute
extern TypeInfo SerializableAttribute_t758_il2cpp_TypeInfo;
// System.SerializableAttribute
#include "mscorlib_System_SerializableAttributeMethodDeclarations.h"
static const EncodedMethodIndex SerializableAttribute_t758_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair SerializableAttribute_t758_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializableAttribute_t758_0_0_0;
extern const Il2CppType SerializableAttribute_t758_1_0_0;
struct SerializableAttribute_t758;
const Il2CppTypeDefinitionMetadata SerializableAttribute_t758_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SerializableAttribute_t758_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, SerializableAttribute_t758_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3180/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SerializableAttribute_t758_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializableAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &SerializableAttribute_t758_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 568/* custom_attributes_cache */
	, &SerializableAttribute_t758_0_0_0/* byval_arg */
	, &SerializableAttribute_t758_1_0_0/* this_arg */
	, &SerializableAttribute_t758_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializableAttribute_t758)/* instance_size */
	, sizeof (SerializableAttribute_t758)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// Metadata Definition System.AttributeUsageAttribute
extern TypeInfo AttributeUsageAttribute_t759_il2cpp_TypeInfo;
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
static const EncodedMethodIndex AttributeUsageAttribute_t759_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair AttributeUsageAttribute_t759_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AttributeUsageAttribute_t759_0_0_0;
extern const Il2CppType AttributeUsageAttribute_t759_1_0_0;
struct AttributeUsageAttribute_t759;
const Il2CppTypeDefinitionMetadata AttributeUsageAttribute_t759_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AttributeUsageAttribute_t759_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, AttributeUsageAttribute_t759_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2494/* fieldStart */
	, 3181/* methodStart */
	, -1/* eventStart */
	, 729/* propertyStart */

};
TypeInfo AttributeUsageAttribute_t759_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeUsageAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AttributeUsageAttribute_t759_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 569/* custom_attributes_cache */
	, &AttributeUsageAttribute_t759_0_0_0/* byval_arg */
	, &AttributeUsageAttribute_t759_1_0_0/* this_arg */
	, &AttributeUsageAttribute_t759_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeUsageAttribute_t759)/* instance_size */
	, sizeof (AttributeUsageAttribute_t759)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// Metadata Definition System.Runtime.InteropServices.ComVisibleAttribute
extern TypeInfo ComVisibleAttribute_t760_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
static const EncodedMethodIndex ComVisibleAttribute_t760_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair ComVisibleAttribute_t760_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ComVisibleAttribute_t760_0_0_0;
extern const Il2CppType ComVisibleAttribute_t760_1_0_0;
struct ComVisibleAttribute_t760;
const Il2CppTypeDefinitionMetadata ComVisibleAttribute_t760_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ComVisibleAttribute_t760_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, ComVisibleAttribute_t760_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2497/* fieldStart */
	, 3186/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ComVisibleAttribute_t760_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComVisibleAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &ComVisibleAttribute_t760_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 570/* custom_attributes_cache */
	, &ComVisibleAttribute_t760_0_0_0/* byval_arg */
	, &ComVisibleAttribute_t760_1_0_0/* this_arg */
	, &ComVisibleAttribute_t760_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComVisibleAttribute_t760)/* instance_size */
	, sizeof (ComVisibleAttribute_t760)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.IEquatable`1
extern TypeInfo IEquatable_1_t2054_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEquatable_1_t2054_0_0_0;
extern const Il2CppType IEquatable_1_t2054_1_0_0;
struct IEquatable_1_t2054;
const Il2CppTypeDefinitionMetadata IEquatable_1_t2054_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3187/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IEquatable_1_t2054_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IEquatable_1_t2054_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IEquatable_1_t2054_0_0_0/* byval_arg */
	, &IEquatable_1_t2054_1_0_0/* this_arg */
	, &IEquatable_1_t2054_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 16/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Int64
#include "mscorlib_System_Int64.h"
// Metadata Definition System.Int64
extern TypeInfo Int64_t345_il2cpp_TypeInfo;
// System.Int64
#include "mscorlib_System_Int64MethodDeclarations.h"
static const EncodedMethodIndex Int64_t345_VTable[24] = 
{
	1064,
	125,
	1065,
	1066,
	1067,
	1068,
	1069,
	1070,
	1071,
	1072,
	1073,
	1074,
	1075,
	1076,
	1077,
	1078,
	1079,
	1080,
	1081,
	1082,
	1083,
	1084,
	1085,
	1086,
};
extern const Il2CppType IComparable_1_t2219_0_0_0;
extern const Il2CppType IEquatable_1_t2220_0_0_0;
static const Il2CppType* Int64_t345_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2219_0_0_0,
	&IEquatable_1_t2220_0_0_0,
};
static Il2CppInterfaceOffsetPair Int64_t345_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
	{ &IComparable_1_t2219_0_0_0, 22},
	{ &IEquatable_1_t2220_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Int64_t345_0_0_0;
extern const Il2CppType Int64_t345_1_0_0;
const Il2CppTypeDefinitionMetadata Int64_t345_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Int64_t345_InterfacesTypeInfos/* implementedInterfaces */
	, Int64_t345_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Int64_t345_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2498/* fieldStart */
	, 3188/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Int64_t345_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Int64"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int64_t345_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 571/* custom_attributes_cache */
	, &Int64_t345_0_0_0/* byval_arg */
	, &Int64_t345_1_0_0/* this_arg */
	, &Int64_t345_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Int64_t345)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Int64_t345)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 31/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.UInt32
#include "mscorlib_System_UInt32.h"
// Metadata Definition System.UInt32
extern TypeInfo UInt32_t336_il2cpp_TypeInfo;
// System.UInt32
#include "mscorlib_System_UInt32MethodDeclarations.h"
static const EncodedMethodIndex UInt32_t336_VTable[24] = 
{
	1087,
	125,
	1088,
	1089,
	1090,
	1091,
	1092,
	1093,
	1094,
	1095,
	1096,
	1097,
	1098,
	1099,
	1100,
	1101,
	1102,
	1103,
	1104,
	1105,
	1106,
	1107,
	1108,
	1109,
};
extern const Il2CppType IComparable_1_t2221_0_0_0;
extern const Il2CppType IEquatable_1_t2222_0_0_0;
static const Il2CppType* UInt32_t336_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2221_0_0_0,
	&IEquatable_1_t2222_0_0_0,
};
static Il2CppInterfaceOffsetPair UInt32_t336_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
	{ &IComparable_1_t2221_0_0_0, 22},
	{ &IEquatable_1_t2222_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UInt32_t336_0_0_0;
extern const Il2CppType UInt32_t336_1_0_0;
const Il2CppTypeDefinitionMetadata UInt32_t336_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UInt32_t336_InterfacesTypeInfos/* implementedInterfaces */
	, UInt32_t336_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, UInt32_t336_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2499/* fieldStart */
	, 3219/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UInt32_t336_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UInt32"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UInt32_t336_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 572/* custom_attributes_cache */
	, &UInt32_t336_0_0_0/* byval_arg */
	, &UInt32_t336_1_0_0/* this_arg */
	, &UInt32_t336_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UInt32_t336)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UInt32_t336)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 30/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// Metadata Definition System.CLSCompliantAttribute
extern TypeInfo CLSCompliantAttribute_t761_il2cpp_TypeInfo;
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
static const EncodedMethodIndex CLSCompliantAttribute_t761_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair CLSCompliantAttribute_t761_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CLSCompliantAttribute_t761_0_0_0;
extern const Il2CppType CLSCompliantAttribute_t761_1_0_0;
struct CLSCompliantAttribute_t761;
const Il2CppTypeDefinitionMetadata CLSCompliantAttribute_t761_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CLSCompliantAttribute_t761_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, CLSCompliantAttribute_t761_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2500/* fieldStart */
	, 3249/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CLSCompliantAttribute_t761_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CLSCompliantAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &CLSCompliantAttribute_t761_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 577/* custom_attributes_cache */
	, &CLSCompliantAttribute_t761_0_0_0/* byval_arg */
	, &CLSCompliantAttribute_t761_1_0_0/* this_arg */
	, &CLSCompliantAttribute_t761_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CLSCompliantAttribute_t761)/* instance_size */
	, sizeof (CLSCompliantAttribute_t761)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.UInt64
#include "mscorlib_System_UInt64.h"
// Metadata Definition System.UInt64
extern TypeInfo UInt64_t348_il2cpp_TypeInfo;
// System.UInt64
#include "mscorlib_System_UInt64MethodDeclarations.h"
static const EncodedMethodIndex UInt64_t348_VTable[24] = 
{
	1110,
	125,
	1111,
	1112,
	1113,
	1114,
	1115,
	1116,
	1117,
	1118,
	1119,
	1120,
	1121,
	1122,
	1123,
	1124,
	1125,
	1126,
	1127,
	1128,
	1129,
	1130,
	1131,
	1132,
};
extern const Il2CppType IComparable_1_t2223_0_0_0;
extern const Il2CppType IEquatable_1_t2224_0_0_0;
static const Il2CppType* UInt64_t348_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2223_0_0_0,
	&IEquatable_1_t2224_0_0_0,
};
static Il2CppInterfaceOffsetPair UInt64_t348_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
	{ &IComparable_1_t2223_0_0_0, 22},
	{ &IEquatable_1_t2224_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UInt64_t348_0_0_0;
extern const Il2CppType UInt64_t348_1_0_0;
const Il2CppTypeDefinitionMetadata UInt64_t348_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UInt64_t348_InterfacesTypeInfos/* implementedInterfaces */
	, UInt64_t348_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, UInt64_t348_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2501/* fieldStart */
	, 3250/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UInt64_t348_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UInt64"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UInt64_t348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 578/* custom_attributes_cache */
	, &UInt64_t348_0_0_0/* byval_arg */
	, &UInt64_t348_1_0_0/* this_arg */
	, &UInt64_t348_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UInt64_t348)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UInt64_t348)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 28/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Byte
#include "mscorlib_System_Byte.h"
// Metadata Definition System.Byte
extern TypeInfo Byte_t326_il2cpp_TypeInfo;
// System.Byte
#include "mscorlib_System_ByteMethodDeclarations.h"
static const EncodedMethodIndex Byte_t326_VTable[24] = 
{
	1133,
	125,
	1134,
	1135,
	1136,
	1137,
	1138,
	1139,
	1140,
	1141,
	1142,
	1143,
	1144,
	1145,
	1146,
	1147,
	1148,
	1149,
	1150,
	1151,
	1152,
	1153,
	1154,
	1155,
};
extern const Il2CppType IComparable_1_t2225_0_0_0;
extern const Il2CppType IEquatable_1_t2226_0_0_0;
static const Il2CppType* Byte_t326_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2225_0_0_0,
	&IEquatable_1_t2226_0_0_0,
};
static Il2CppInterfaceOffsetPair Byte_t326_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
	{ &IComparable_1_t2225_0_0_0, 22},
	{ &IEquatable_1_t2226_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Byte_t326_0_0_0;
extern const Il2CppType Byte_t326_1_0_0;
const Il2CppTypeDefinitionMetadata Byte_t326_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Byte_t326_InterfacesTypeInfos/* implementedInterfaces */
	, Byte_t326_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Byte_t326_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2502/* fieldStart */
	, 3278/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Byte_t326_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Byte"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Byte_t326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 582/* custom_attributes_cache */
	, &Byte_t326_0_0_0/* byval_arg */
	, &Byte_t326_1_0_0/* this_arg */
	, &Byte_t326_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Byte_t326)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Byte_t326)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 29/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.SByte
#include "mscorlib_System_SByte.h"
// Metadata Definition System.SByte
extern TypeInfo SByte_t349_il2cpp_TypeInfo;
// System.SByte
#include "mscorlib_System_SByteMethodDeclarations.h"
static const EncodedMethodIndex SByte_t349_VTable[24] = 
{
	1156,
	125,
	1157,
	1158,
	1159,
	1160,
	1161,
	1162,
	1163,
	1164,
	1165,
	1166,
	1167,
	1168,
	1169,
	1170,
	1171,
	1172,
	1173,
	1174,
	1175,
	1176,
	1177,
	1178,
};
extern const Il2CppType IComparable_1_t2227_0_0_0;
extern const Il2CppType IEquatable_1_t2228_0_0_0;
static const Il2CppType* SByte_t349_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2227_0_0_0,
	&IEquatable_1_t2228_0_0_0,
};
static Il2CppInterfaceOffsetPair SByte_t349_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
	{ &IComparable_1_t2227_0_0_0, 22},
	{ &IEquatable_1_t2228_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SByte_t349_0_0_0;
extern const Il2CppType SByte_t349_1_0_0;
const Il2CppTypeDefinitionMetadata SByte_t349_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SByte_t349_InterfacesTypeInfos/* implementedInterfaces */
	, SByte_t349_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, SByte_t349_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2505/* fieldStart */
	, 3307/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SByte_t349_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SByte"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &SByte_t349_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 583/* custom_attributes_cache */
	, &SByte_t349_0_0_0/* byval_arg */
	, &SByte_t349_1_0_0/* this_arg */
	, &SByte_t349_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SByte_t349)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SByte_t349)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 28/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Int16
#include "mscorlib_System_Int16.h"
// Metadata Definition System.Int16
extern TypeInfo Int16_t350_il2cpp_TypeInfo;
// System.Int16
#include "mscorlib_System_Int16MethodDeclarations.h"
static const EncodedMethodIndex Int16_t350_VTable[24] = 
{
	1179,
	125,
	1180,
	1181,
	1182,
	1183,
	1184,
	1185,
	1186,
	1187,
	1188,
	1189,
	1190,
	1191,
	1192,
	1193,
	1194,
	1195,
	1196,
	1197,
	1198,
	1199,
	1200,
	1201,
};
extern const Il2CppType IComparable_1_t2229_0_0_0;
extern const Il2CppType IEquatable_1_t2230_0_0_0;
static const Il2CppType* Int16_t350_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2229_0_0_0,
	&IEquatable_1_t2230_0_0_0,
};
static Il2CppInterfaceOffsetPair Int16_t350_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
	{ &IComparable_1_t2229_0_0_0, 22},
	{ &IEquatable_1_t2230_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Int16_t350_0_0_0;
extern const Il2CppType Int16_t350_1_0_0;
const Il2CppTypeDefinitionMetadata Int16_t350_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Int16_t350_InterfacesTypeInfos/* implementedInterfaces */
	, Int16_t350_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Int16_t350_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2506/* fieldStart */
	, 3335/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Int16_t350_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Int16"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int16_t350_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 587/* custom_attributes_cache */
	, &Int16_t350_0_0_0/* byval_arg */
	, &Int16_t350_1_0_0/* this_arg */
	, &Int16_t350_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Int16_t350)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Int16_t350)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 28/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.UInt16
#include "mscorlib_System_UInt16.h"
// Metadata Definition System.UInt16
extern TypeInfo UInt16_t351_il2cpp_TypeInfo;
// System.UInt16
#include "mscorlib_System_UInt16MethodDeclarations.h"
static const EncodedMethodIndex UInt16_t351_VTable[24] = 
{
	1202,
	125,
	1203,
	1204,
	1205,
	1206,
	1207,
	1208,
	1209,
	1210,
	1211,
	1212,
	1213,
	1214,
	1215,
	1216,
	1217,
	1218,
	1219,
	1220,
	1221,
	1222,
	1223,
	1224,
};
extern const Il2CppType IComparable_1_t2231_0_0_0;
extern const Il2CppType IEquatable_1_t2232_0_0_0;
static const Il2CppType* UInt16_t351_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2231_0_0_0,
	&IEquatable_1_t2232_0_0_0,
};
static Il2CppInterfaceOffsetPair UInt16_t351_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
	{ &IComparable_1_t2231_0_0_0, 22},
	{ &IEquatable_1_t2232_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UInt16_t351_0_0_0;
extern const Il2CppType UInt16_t351_1_0_0;
const Il2CppTypeDefinitionMetadata UInt16_t351_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UInt16_t351_InterfacesTypeInfos/* implementedInterfaces */
	, UInt16_t351_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, UInt16_t351_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2507/* fieldStart */
	, 3363/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UInt16_t351_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UInt16"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UInt16_t351_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 588/* custom_attributes_cache */
	, &UInt16_t351_0_0_0/* byval_arg */
	, &UInt16_t351_1_0_0/* this_arg */
	, &UInt16_t351_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UInt16_t351)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UInt16_t351)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 28/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// Metadata Definition System.Collections.IEnumerator
extern TypeInfo IEnumerator_t279_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnumerator_t279_0_0_0;
extern const Il2CppType IEnumerator_t279_1_0_0;
struct IEnumerator_t279;
const Il2CppTypeDefinitionMetadata IEnumerator_t279_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3391/* methodStart */
	, -1/* eventStart */
	, 731/* propertyStart */

};
TypeInfo IEnumerator_t279_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IEnumerator_t279_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 593/* custom_attributes_cache */
	, &IEnumerator_t279_0_0_0/* byval_arg */
	, &IEnumerator_t279_1_0_0/* this_arg */
	, &IEnumerator_t279_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.IEnumerable
extern TypeInfo IEnumerable_t302_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnumerable_t302_0_0_0;
extern const Il2CppType IEnumerable_t302_1_0_0;
struct IEnumerable_t302;
const Il2CppTypeDefinitionMetadata IEnumerable_t302_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3393/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IEnumerable_t302_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IEnumerable_t302_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 594/* custom_attributes_cache */
	, &IEnumerable_t302_0_0_0/* byval_arg */
	, &IEnumerable_t302_1_0_0/* this_arg */
	, &IEnumerable_t302_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IDisposable
extern TypeInfo IDisposable_t319_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDisposable_t319_0_0_0;
extern const Il2CppType IDisposable_t319_1_0_0;
struct IDisposable_t319;
const Il2CppTypeDefinitionMetadata IDisposable_t319_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3394/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IDisposable_t319_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDisposable"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IDisposable_t319_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 596/* custom_attributes_cache */
	, &IDisposable_t319_0_0_0/* byval_arg */
	, &IDisposable_t319_1_0_0/* this_arg */
	, &IDisposable_t319_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.IEnumerator`1
extern TypeInfo IEnumerator_1_t2055_il2cpp_TypeInfo;
static const Il2CppType* IEnumerator_1_t2055_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDisposable_t319_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnumerator_1_t2055_0_0_0;
extern const Il2CppType IEnumerator_1_t2055_1_0_0;
struct IEnumerator_1_t2055;
const Il2CppTypeDefinitionMetadata IEnumerator_1_t2055_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IEnumerator_1_t2055_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3395/* methodStart */
	, -1/* eventStart */
	, 732/* propertyStart */

};
TypeInfo IEnumerator_1_t2055_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &IEnumerator_1_t2055_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IEnumerator_1_t2055_0_0_0/* byval_arg */
	, &IEnumerator_1_t2055_1_0_0/* this_arg */
	, &IEnumerator_1_t2055_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 17/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Char
#include "mscorlib_System_Char.h"
// Metadata Definition System.Char
extern TypeInfo Char_t576_il2cpp_TypeInfo;
// System.Char
#include "mscorlib_System_CharMethodDeclarations.h"
static const EncodedMethodIndex Char_t576_VTable[23] = 
{
	1225,
	125,
	1226,
	1227,
	1228,
	1229,
	1230,
	1231,
	1232,
	1233,
	1234,
	1235,
	1236,
	1237,
	1238,
	1239,
	1240,
	1241,
	1242,
	1243,
	1244,
	1245,
	1246,
};
extern const Il2CppType IComparable_1_t2233_0_0_0;
extern const Il2CppType IEquatable_1_t2234_0_0_0;
static const Il2CppType* Char_t576_InterfacesTypeInfos[] = 
{
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2233_0_0_0,
	&IEquatable_1_t2234_0_0_0,
};
static Il2CppInterfaceOffsetPair Char_t576_InterfacesOffsets[] = 
{
	{ &IConvertible_t1409_0_0_0, 4},
	{ &IComparable_t1408_0_0_0, 20},
	{ &IComparable_1_t2233_0_0_0, 21},
	{ &IEquatable_1_t2234_0_0_0, 22},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Char_t576_0_0_0;
extern const Il2CppType Char_t576_1_0_0;
const Il2CppTypeDefinitionMetadata Char_t576_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Char_t576_InterfacesTypeInfos/* implementedInterfaces */
	, Char_t576_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Char_t576_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2510/* fieldStart */
	, 3396/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Char_t576_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Char"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Char_t576_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 597/* custom_attributes_cache */
	, &Char_t576_0_0_0/* byval_arg */
	, &Char_t576_1_0_0/* this_arg */
	, &Char_t576_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Char_t576)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Char_t576)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, 1/* native_size */
	, sizeof(Char_t576_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 39/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.String
#include "mscorlib_System_String.h"
// Metadata Definition System.String
extern TypeInfo String_t_il2cpp_TypeInfo;
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
static const EncodedMethodIndex String_t_VTable[25] = 
{
	1247,
	125,
	1248,
	1249,
	1250,
	1251,
	1252,
	1253,
	1254,
	1255,
	1256,
	1257,
	1258,
	1259,
	1260,
	1261,
	1262,
	1263,
	1264,
	1265,
	1266,
	1267,
	1268,
	1269,
	1270,
};
extern const Il2CppType ICloneable_t2056_0_0_0;
extern const Il2CppType IComparable_1_t2235_0_0_0;
extern const Il2CppType IEquatable_1_t2236_0_0_0;
extern const Il2CppType IEnumerable_1_t2237_0_0_0;
static const Il2CppType* String_t_InterfacesTypeInfos[] = 
{
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IEnumerable_t302_0_0_0,
	&ICloneable_t2056_0_0_0,
	&IComparable_1_t2235_0_0_0,
	&IEquatable_1_t2236_0_0_0,
	&IEnumerable_1_t2237_0_0_0,
};
static Il2CppInterfaceOffsetPair String_t_InterfacesOffsets[] = 
{
	{ &IConvertible_t1409_0_0_0, 4},
	{ &IComparable_t1408_0_0_0, 20},
	{ &IEnumerable_t302_0_0_0, 21},
	{ &ICloneable_t2056_0_0_0, 22},
	{ &IComparable_1_t2235_0_0_0, 22},
	{ &IEquatable_1_t2236_0_0_0, 23},
	{ &IEnumerable_1_t2237_0_0_0, 24},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_1_0_0;
struct String_t;
const Il2CppTypeDefinitionMetadata String_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, String_t_InterfacesTypeInfos/* implementedInterfaces */
	, String_t_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, String_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2520/* fieldStart */
	, 3435/* methodStart */
	, -1/* eventStart */
	, 733/* propertyStart */

};
TypeInfo String_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "String"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &String_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 598/* custom_attributes_cache */
	, &String_t_0_0_0/* byval_arg */
	, &String_t_1_0_0/* this_arg */
	, &String_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (String_t)/* instance_size */
	, sizeof (String_t)/* actualSize */
	, 0/* element_size */
	, sizeof(char*)/* native_size */
	, sizeof(String_t_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 139/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 7/* interfaces_count */
	, 7/* interface_offsets_count */

};
// Metadata Definition System.ICloneable
extern TypeInfo ICloneable_t2056_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICloneable_t2056_1_0_0;
struct ICloneable_t2056;
const Il2CppTypeDefinitionMetadata ICloneable_t2056_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICloneable_t2056_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICloneable"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ICloneable_t2056_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 615/* custom_attributes_cache */
	, &ICloneable_t2056_0_0_0/* byval_arg */
	, &ICloneable_t2056_1_0_0/* this_arg */
	, &ICloneable_t2056_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.IEnumerable`1
extern TypeInfo IEnumerable_1_t2057_il2cpp_TypeInfo;
static const Il2CppType* IEnumerable_1_t2057_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnumerable_1_t2057_0_0_0;
extern const Il2CppType IEnumerable_1_t2057_1_0_0;
struct IEnumerable_1_t2057;
const Il2CppTypeDefinitionMetadata IEnumerable_1_t2057_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IEnumerable_1_t2057_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3574/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IEnumerable_1_t2057_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &IEnumerable_1_t2057_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IEnumerable_1_t2057_0_0_0/* byval_arg */
	, &IEnumerable_1_t2057_1_0_0/* this_arg */
	, &IEnumerable_1_t2057_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 18/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Single
#include "mscorlib_System_Single.h"
// Metadata Definition System.Single
extern TypeInfo Single_t321_il2cpp_TypeInfo;
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
static const EncodedMethodIndex Single_t321_VTable[24] = 
{
	1271,
	125,
	1272,
	1273,
	1274,
	1275,
	1276,
	1277,
	1278,
	1279,
	1280,
	1281,
	1282,
	1283,
	1284,
	1285,
	1286,
	1287,
	1288,
	1289,
	1290,
	1291,
	1292,
	1293,
};
extern const Il2CppType IComparable_1_t2238_0_0_0;
extern const Il2CppType IEquatable_1_t2239_0_0_0;
static const Il2CppType* Single_t321_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2238_0_0_0,
	&IEquatable_1_t2239_0_0_0,
};
static Il2CppInterfaceOffsetPair Single_t321_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
	{ &IComparable_1_t2238_0_0_0, 22},
	{ &IEquatable_1_t2239_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Single_t321_0_0_0;
extern const Il2CppType Single_t321_1_0_0;
const Il2CppTypeDefinitionMetadata Single_t321_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Single_t321_InterfacesTypeInfos/* implementedInterfaces */
	, Single_t321_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Single_t321_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2524/* fieldStart */
	, 3575/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Single_t321_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Single"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Single_t321_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 616/* custom_attributes_cache */
	, &Single_t321_0_0_0/* byval_arg */
	, &Single_t321_1_0_0/* this_arg */
	, &Single_t321_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Single_t321)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Single_t321)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(float)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 29/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Double
#include "mscorlib_System_Double.h"
// Metadata Definition System.Double
extern TypeInfo Double_t344_il2cpp_TypeInfo;
// System.Double
#include "mscorlib_System_DoubleMethodDeclarations.h"
static const EncodedMethodIndex Double_t344_VTable[24] = 
{
	1294,
	125,
	1295,
	1296,
	1297,
	1298,
	1299,
	1300,
	1301,
	1302,
	1303,
	1304,
	1305,
	1306,
	1307,
	1308,
	1309,
	1310,
	1311,
	1312,
	1313,
	1314,
	1315,
	1316,
};
extern const Il2CppType IComparable_1_t2240_0_0_0;
extern const Il2CppType IEquatable_1_t2241_0_0_0;
static const Il2CppType* Double_t344_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2240_0_0_0,
	&IEquatable_1_t2241_0_0_0,
};
static Il2CppInterfaceOffsetPair Double_t344_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
	{ &IComparable_1_t2240_0_0_0, 22},
	{ &IEquatable_1_t2241_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Double_t344_0_0_0;
extern const Il2CppType Double_t344_1_0_0;
const Il2CppTypeDefinitionMetadata Double_t344_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Double_t344_InterfacesTypeInfos/* implementedInterfaces */
	, Double_t344_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Double_t344_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2532/* fieldStart */
	, 3604/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Double_t344_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Double"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Double_t344_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 618/* custom_attributes_cache */
	, &Double_t344_0_0_0/* byval_arg */
	, &Double_t344_1_0_0/* this_arg */
	, &Double_t344_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Double_t344)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Double_t344)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(double)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 34/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Decimal
#include "mscorlib_System_Decimal.h"
// Metadata Definition System.Decimal
extern TypeInfo Decimal_t347_il2cpp_TypeInfo;
// System.Decimal
#include "mscorlib_System_DecimalMethodDeclarations.h"
static const EncodedMethodIndex Decimal_t347_VTable[24] = 
{
	1317,
	125,
	1318,
	1319,
	1320,
	1321,
	1322,
	1323,
	1324,
	1325,
	1326,
	1327,
	1328,
	1329,
	1330,
	1331,
	1332,
	1333,
	1334,
	1335,
	1336,
	1337,
	1338,
	1339,
};
extern const Il2CppType IComparable_1_t2242_0_0_0;
extern const Il2CppType IEquatable_1_t2243_0_0_0;
static const Il2CppType* Decimal_t347_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2242_0_0_0,
	&IEquatable_1_t2243_0_0_0,
};
static Il2CppInterfaceOffsetPair Decimal_t347_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
	{ &IComparable_1_t2242_0_0_0, 22},
	{ &IEquatable_1_t2243_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Decimal_t347_0_0_0;
extern const Il2CppType Decimal_t347_1_0_0;
const Il2CppTypeDefinitionMetadata Decimal_t347_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Decimal_t347_InterfacesTypeInfos/* implementedInterfaces */
	, Decimal_t347_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Decimal_t347_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2546/* fieldStart */
	, 3638/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Decimal_t347_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Decimal"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Decimal_t347_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 620/* custom_attributes_cache */
	, &Decimal_t347_0_0_0/* byval_arg */
	, &Decimal_t347_1_0_0/* this_arg */
	, &Decimal_t347_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Decimal_t347)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Decimal_t347)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Decimal_t347 )/* native_size */
	, sizeof(Decimal_t347_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 86/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Boolean
#include "mscorlib_System_Boolean.h"
// Metadata Definition System.Boolean
extern TypeInfo Boolean_t333_il2cpp_TypeInfo;
// System.Boolean
#include "mscorlib_System_BooleanMethodDeclarations.h"
static const EncodedMethodIndex Boolean_t333_VTable[23] = 
{
	1340,
	125,
	1341,
	1342,
	1343,
	1344,
	1345,
	1346,
	1347,
	1348,
	1349,
	1350,
	1351,
	1352,
	1353,
	1354,
	1355,
	1356,
	1357,
	1358,
	1359,
	1360,
	1361,
};
extern const Il2CppType IComparable_1_t2244_0_0_0;
extern const Il2CppType IEquatable_1_t2245_0_0_0;
static const Il2CppType* Boolean_t333_InterfacesTypeInfos[] = 
{
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2244_0_0_0,
	&IEquatable_1_t2245_0_0_0,
};
static Il2CppInterfaceOffsetPair Boolean_t333_InterfacesOffsets[] = 
{
	{ &IConvertible_t1409_0_0_0, 4},
	{ &IComparable_t1408_0_0_0, 20},
	{ &IComparable_1_t2244_0_0_0, 21},
	{ &IEquatable_1_t2245_0_0_0, 22},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Boolean_t333_0_0_0;
extern const Il2CppType Boolean_t333_1_0_0;
const Il2CppTypeDefinitionMetadata Boolean_t333_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Boolean_t333_InterfacesTypeInfos/* implementedInterfaces */
	, Boolean_t333_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Boolean_t333_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2555/* fieldStart */
	, 3724/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Boolean_t333_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Boolean"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Boolean_t333_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 636/* custom_attributes_cache */
	, &Boolean_t333_0_0_0/* byval_arg */
	, &Boolean_t333_1_0_0/* this_arg */
	, &Boolean_t333_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Boolean_t333)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Boolean_t333)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, 4/* native_size */
	, sizeof(Boolean_t333_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Metadata Definition System.IntPtr
extern TypeInfo IntPtr_t_il2cpp_TypeInfo;
// System.IntPtr
#include "mscorlib_System_IntPtrMethodDeclarations.h"
static const EncodedMethodIndex IntPtr_t_VTable[5] = 
{
	1362,
	125,
	1363,
	1364,
	1365,
};
extern const Il2CppType ISerializable_t1426_0_0_0;
static const Il2CppType* IntPtr_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair IntPtr_t_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_1_0_0;
const Il2CppTypeDefinitionMetadata IntPtr_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IntPtr_t_InterfacesTypeInfos/* implementedInterfaces */
	, IntPtr_t_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, IntPtr_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2558/* fieldStart */
	, 3748/* methodStart */
	, -1/* eventStart */
	, 735/* propertyStart */

};
TypeInfo IntPtr_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntPtr"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IntPtr_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 637/* custom_attributes_cache */
	, &IntPtr_t_0_0_0/* byval_arg */
	, &IntPtr_t_1_0_0/* this_arg */
	, &IntPtr_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IntPtr_t)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (IntPtr_t)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(IntPtr_t)/* native_size */
	, sizeof(IntPtr_t_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 18/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.ISerializable
extern TypeInfo ISerializable_t1426_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISerializable_t1426_1_0_0;
struct ISerializable_t1426;
const Il2CppTypeDefinitionMetadata ISerializable_t1426_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3766/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISerializable_t1426_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializable"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &ISerializable_t1426_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 649/* custom_attributes_cache */
	, &ISerializable_t1426_0_0_0/* byval_arg */
	, &ISerializable_t1426_1_0_0/* this_arg */
	, &ISerializable_t1426_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// Metadata Definition System.UIntPtr
extern TypeInfo UIntPtr_t_il2cpp_TypeInfo;
// System.UIntPtr
#include "mscorlib_System_UIntPtrMethodDeclarations.h"
static const EncodedMethodIndex UIntPtr_t_VTable[5] = 
{
	1366,
	125,
	1367,
	1368,
	1369,
};
static const Il2CppType* UIntPtr_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair UIntPtr_t_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UIntPtr_t_0_0_0;
extern const Il2CppType UIntPtr_t_1_0_0;
const Il2CppTypeDefinitionMetadata UIntPtr_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UIntPtr_t_InterfacesTypeInfos/* implementedInterfaces */
	, UIntPtr_t_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, UIntPtr_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2560/* fieldStart */
	, 3767/* methodStart */
	, -1/* eventStart */
	, 736/* propertyStart */

};
TypeInfo UIntPtr_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UIntPtr"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UIntPtr_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 650/* custom_attributes_cache */
	, &UIntPtr_t_0_0_0/* byval_arg */
	, &UIntPtr_t_1_0_0/* this_arg */
	, &UIntPtr_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UIntPtr_t)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UIntPtr_t)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UIntPtr_t )/* native_size */
	, sizeof(UIntPtr_t_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 20/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Metadata Definition System.MulticastDelegate
extern TypeInfo MulticastDelegate_t47_il2cpp_TypeInfo;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegateMethodDeclarations.h"
static const EncodedMethodIndex MulticastDelegate_t47_VTable[10] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
};
static Il2CppInterfaceOffsetPair MulticastDelegate_t47_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MulticastDelegate_t47_0_0_0;
extern const Il2CppType MulticastDelegate_t47_1_0_0;
extern const Il2CppType Delegate_t332_0_0_0;
struct MulticastDelegate_t47;
const Il2CppTypeDefinitionMetadata MulticastDelegate_t47_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MulticastDelegate_t47_InterfacesOffsets/* interfaceOffsets */
	, &Delegate_t332_0_0_0/* parent */
	, MulticastDelegate_t47_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2562/* fieldStart */
	, 3787/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MulticastDelegate_t47_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MulticastDelegate"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MulticastDelegate_t47_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 655/* custom_attributes_cache */
	, &MulticastDelegate_t47_0_0_0/* byval_arg */
	, &MulticastDelegate_t47_1_0_0/* this_arg */
	, &MulticastDelegate_t47_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MulticastDelegate_t47)/* instance_size */
	, sizeof (MulticastDelegate_t47)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Delegate
#include "mscorlib_System_Delegate.h"
// Metadata Definition System.Delegate
extern TypeInfo Delegate_t332_il2cpp_TypeInfo;
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
static const EncodedMethodIndex Delegate_t332_VTable[10] = 
{
	1370,
	125,
	1371,
	123,
	1372,
	176,
	1372,
	1373,
	1374,
	1375,
};
static const Il2CppType* Delegate_t332_InterfacesTypeInfos[] = 
{
	&ICloneable_t2056_0_0_0,
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair Delegate_t332_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Delegate_t332_1_0_0;
struct Delegate_t332;
const Il2CppTypeDefinitionMetadata Delegate_t332_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Delegate_t332_InterfacesTypeInfos/* implementedInterfaces */
	, Delegate_t332_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Delegate_t332_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2564/* fieldStart */
	, 3795/* methodStart */
	, -1/* eventStart */
	, 737/* propertyStart */

};
TypeInfo Delegate_t332_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Delegate"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Delegate_t332_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 656/* custom_attributes_cache */
	, &Delegate_t332_0_0_0/* byval_arg */
	, &Delegate_t332_1_0_0/* this_arg */
	, &Delegate_t332_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Delegate_t332)/* instance_size */
	, sizeof (Delegate_t332)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 25/* method_count */
	, 2/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Enum
#include "mscorlib_System_Enum.h"
// Metadata Definition System.Enum
extern TypeInfo Enum_t305_il2cpp_TypeInfo;
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"
static const EncodedMethodIndex Enum_t305_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static const Il2CppType* Enum_t305_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
};
static Il2CppInterfaceOffsetPair Enum_t305_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enum_t305_0_0_0;
extern const Il2CppType Enum_t305_1_0_0;
struct Enum_t305;
const Il2CppTypeDefinitionMetadata Enum_t305_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Enum_t305_InterfacesTypeInfos/* implementedInterfaces */
	, Enum_t305_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Enum_t305_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2573/* fieldStart */
	, 3820/* methodStart */
	, -1/* eventStart */
	, 739/* propertyStart */

};
TypeInfo Enum_t305_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enum"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Enum_t305_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 659/* custom_attributes_cache */
	, &Enum_t305_0_0_0/* byval_arg */
	, &Enum_t305_1_0_0/* this_arg */
	, &Enum_t305_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enum_t305)/* instance_size */
	, sizeof (Enum_t305)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Enum_t305_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 49/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Array
#include "mscorlib_System_Array.h"
// Metadata Definition System.Array
extern TypeInfo Array_t_il2cpp_TypeInfo;
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern const Il2CppType InternalEnumerator_1_t2246_0_0_0;
extern const Il2CppRGCTXDefinition Array_InternalArray__IEnumerable_GetEnumerator_m12908_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4440 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2556 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Array_InternalArray__ICollection_Contains_m12911_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Array_InternalArray__ICollection_Contains_m12911_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 2899 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Array_InternalArray__IndexOf_m12914_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Array_InternalArray__IndexOf_m12914_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 2903 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Array_InternalArray__set_Item_m12916_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Array_InternalArray__set_Item_m12916_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 2905 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m12920_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2557 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m12921_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2558 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m12922_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2559 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m12923_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2560 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m12924_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2561 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m12925_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2562 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m12926_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2563 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType TKeyU5BU5D_t2250_0_0_0;
extern const Il2CppRGCTXDefinition Array_Sort_m12927_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2564 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 2935 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2565 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m12928_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2566 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m12929_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2567 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_qsort_m12930_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2568 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2569 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2570 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType IComparer_1_t2251_0_0_0;
extern const Il2CppType Array_compare_m12931_gp_0_0_0_0;
extern const Il2CppType IComparable_1_t2253_0_0_0;
extern const Il2CppRGCTXDefinition Array_compare_m12931_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 2952 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 2951 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4441 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, 2951 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_qsort_m12932_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2571 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2572 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2573 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Resize_m12935_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2574 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType TU5BU5D_t2254_0_0_0;
extern const Il2CppRGCTXDefinition Array_Resize_m12936_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 2966 }/* Array */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_TrueForAll_m12937_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2575 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_ForEach_m12938_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2576 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType TOutputU5BU5D_t2255_0_0_0;
extern const Il2CppRGCTXDefinition Array_ConvertAll_m12939_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 2978 }/* Array */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2577 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindLastIndex_m12940_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2578 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindLastIndex_m12941_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2579 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindLastIndex_m12942_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2580 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindIndex_m12943_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2581 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindIndex_m12944_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2582 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindIndex_m12945_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2583 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_BinarySearch_m12946_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2584 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_BinarySearch_m12947_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2585 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_BinarySearch_m12948_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2586 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Comparer_1_t2256_0_0_0;
extern const Il2CppType IComparer_1_t2257_0_0_0;
extern const Il2CppRGCTXDefinition Array_BinarySearch_m12949_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2587 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4442 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3006 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_IndexOf_m12950_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2588 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_IndexOf_m12951_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2589 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType EqualityComparer_1_t2258_0_0_0;
extern const Il2CppRGCTXDefinition Array_IndexOf_m12952_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2590 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4443 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2591 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_LastIndexOf_m12953_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2592 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_LastIndexOf_m12954_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2593 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType EqualityComparer_1_t2259_0_0_0;
extern const Il2CppRGCTXDefinition Array_LastIndexOf_m12955_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2594 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4444 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2595 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType TU5BU5D_t2260_0_0_0;
extern const Il2CppRGCTXDefinition Array_FindAll_m12956_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 3019 }/* Array */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2596 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2597 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Exists_m12957_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2598 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType ArrayReadOnlyList_1_t2261_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t2262_0_0_0;
extern const Il2CppRGCTXDefinition Array_AsReadOnly_m12958_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4445 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2599 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3027 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2600 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Find_m12959_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2601 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindLast_m12960_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2602 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType InternalEnumerator_1_t2058_0_0_0;
extern const Il2CppType SimpleEnumerator_t763_0_0_0;
extern const Il2CppType ArrayReadOnlyList_1_t2059_0_0_0;
extern const Il2CppType Swapper_t764_0_0_0;
static const Il2CppType* Array_t_il2cpp_TypeInfo__nestedTypes[4] =
{
	&InternalEnumerator_1_t2058_0_0_0,
	&SimpleEnumerator_t763_0_0_0,
	&ArrayReadOnlyList_1_t2059_0_0_0,
	&Swapper_t764_0_0_0,
};
static const EncodedMethodIndex Array_t_VTable[21] = 
{
	120,
	125,
	122,
	123,
	1376,
	1377,
	1378,
	1379,
	1380,
	1381,
	1382,
	1383,
	1384,
	1385,
	1386,
	1387,
	1388,
	1389,
	1390,
	1391,
	1392,
};
extern const Il2CppType ICollection_t569_0_0_0;
extern const Il2CppType IList_t519_0_0_0;
static const Il2CppType* Array_t_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICloneable_t2056_0_0_0,
	&ICollection_t569_0_0_0,
	&IList_t519_0_0_0,
};
static Il2CppInterfaceOffsetPair Array_t_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 5},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Array_t_1_0_0;
struct Array_t;
const Il2CppTypeDefinitionMetadata Array_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Array_t_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Array_t_InterfacesTypeInfos/* implementedInterfaces */
	, Array_t_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Array_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3869/* methodStart */
	, -1/* eventStart */
	, 740/* propertyStart */

};
TypeInfo Array_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Array"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Array_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 676/* custom_attributes_cache */
	, &Array_t_0_0_0/* byval_arg */
	, &Array_t_1_0_0/* this_arg */
	, &Array_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Array_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 152/* method_count */
	, 9/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 4/* nested_type_count */
	, 21/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.Array/InternalEnumerator`1
extern TypeInfo InternalEnumerator_1_t2058_il2cpp_TypeInfo;
static const EncodedMethodIndex InternalEnumerator_1_t2058_VTable[8] = 
{
	150,
	125,
	151,
	152,
	1393,
	1394,
	1395,
	1396,
};
extern const Il2CppType IEnumerator_1_t2263_0_0_0;
static const Il2CppType* InternalEnumerator_1_t2058_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDisposable_t319_0_0_0,
	&IEnumerator_1_t2263_0_0_0,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2058_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &IDisposable_t319_0_0_0, 6},
	{ &IEnumerator_1_t2263_0_0_0, 7},
};
extern const Il2CppType InternalEnumerator_1_t2058_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition InternalEnumerator_1_t2058_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2603 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3034 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2604 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InternalEnumerator_1_t2058_1_0_0;
const Il2CppTypeDefinitionMetadata InternalEnumerator_1_t2058_DefinitionMetadata = 
{
	&Array_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, InternalEnumerator_1_t2058_InterfacesTypeInfos/* implementedInterfaces */
	, InternalEnumerator_1_t2058_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, InternalEnumerator_1_t2058_VTable/* vtableMethods */
	, InternalEnumerator_1_t2058_RGCTXData/* rgctxDefinition */
	, 2574/* fieldStart */
	, 4021/* methodStart */
	, -1/* eventStart */
	, 749/* propertyStart */

};
TypeInfo InternalEnumerator_1_t2058_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InternalEnumerator_1_t2058_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InternalEnumerator_1_t2058_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2058_1_0_0/* this_arg */
	, &InternalEnumerator_1_t2058_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 72/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Array/SimpleEnumerator
#include "mscorlib_System_Array_SimpleEnumerator.h"
// Metadata Definition System.Array/SimpleEnumerator
extern TypeInfo SimpleEnumerator_t763_il2cpp_TypeInfo;
// System.Array/SimpleEnumerator
#include "mscorlib_System_Array_SimpleEnumeratorMethodDeclarations.h"
static const EncodedMethodIndex SimpleEnumerator_t763_VTable[6] = 
{
	120,
	125,
	122,
	123,
	1397,
	1398,
};
static const Il2CppType* SimpleEnumerator_t763_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&ICloneable_t2056_0_0_0,
};
static Il2CppInterfaceOffsetPair SimpleEnumerator_t763_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SimpleEnumerator_t763_1_0_0;
struct SimpleEnumerator_t763;
const Il2CppTypeDefinitionMetadata SimpleEnumerator_t763_DefinitionMetadata = 
{
	&Array_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, SimpleEnumerator_t763_InterfacesTypeInfos/* implementedInterfaces */
	, SimpleEnumerator_t763_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SimpleEnumerator_t763_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2576/* fieldStart */
	, 4026/* methodStart */
	, -1/* eventStart */
	, 751/* propertyStart */

};
TypeInfo SimpleEnumerator_t763_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SimpleEnumerator_t763_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SimpleEnumerator_t763_0_0_0/* byval_arg */
	, &SimpleEnumerator_t763_1_0_0/* this_arg */
	, &SimpleEnumerator_t763_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleEnumerator_t763)/* instance_size */
	, sizeof (SimpleEnumerator_t763)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Array/ArrayReadOnlyList`1
extern TypeInfo ArrayReadOnlyList_1_t2059_il2cpp_TypeInfo;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t2060_0_0_0;
static const Il2CppType* ArrayReadOnlyList_1_t2059_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CGetEnumeratorU3Ec__Iterator0_t2060_0_0_0,
};
static const EncodedMethodIndex ArrayReadOnlyList_1_t2059_VTable[18] = 
{
	120,
	125,
	122,
	123,
	1399,
	1400,
	1401,
	1402,
	1403,
	1404,
	1405,
	1406,
	1407,
	1408,
	1409,
	1410,
	1411,
	1412,
};
extern const Il2CppType IList_1_t2265_0_0_0;
extern const Il2CppType ICollection_1_t2266_0_0_0;
extern const Il2CppType IEnumerable_1_t2267_0_0_0;
static const Il2CppType* ArrayReadOnlyList_1_t2059_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&IList_1_t2265_0_0_0,
	&ICollection_1_t2266_0_0_0,
	&IEnumerable_1_t2267_0_0_0,
};
static Il2CppInterfaceOffsetPair ArrayReadOnlyList_1_t2059_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &IList_1_t2265_0_0_0, 5},
	{ &ICollection_1_t2266_0_0_0, 10},
	{ &IEnumerable_1_t2267_0_0_0, 17},
};
extern const Il2CppType ArrayReadOnlyList_1_t2268_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t2269_0_0_0;
extern const Il2CppRGCTXDefinition ArrayReadOnlyList_1_t2059_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2605 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2606 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4456 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2607 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4457 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2608 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayReadOnlyList_1_t2059_1_0_0;
struct ArrayReadOnlyList_1_t2059;
const Il2CppTypeDefinitionMetadata ArrayReadOnlyList_1_t2059_DefinitionMetadata = 
{
	&Array_t_0_0_0/* declaringType */
	, ArrayReadOnlyList_1_t2059_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ArrayReadOnlyList_1_t2059_InterfacesTypeInfos/* implementedInterfaces */
	, ArrayReadOnlyList_1_t2059_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArrayReadOnlyList_1_t2059_VTable/* vtableMethods */
	, ArrayReadOnlyList_1_t2059_RGCTXData/* rgctxDefinition */
	, 2579/* fieldStart */
	, 4029/* methodStart */
	, -1/* eventStart */
	, 752/* propertyStart */

};
TypeInfo ArrayReadOnlyList_1_t2059_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayReadOnlyList`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ArrayReadOnlyList_1_t2059_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 739/* custom_attributes_cache */
	, &ArrayReadOnlyList_1_t2059_0_0_0/* byval_arg */
	, &ArrayReadOnlyList_1_t2059_1_0_0/* this_arg */
	, &ArrayReadOnlyList_1_t2059_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 73/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0
extern TypeInfo U3CGetEnumeratorU3Ec__Iterator0_t2060_il2cpp_TypeInfo;
static const EncodedMethodIndex U3CGetEnumeratorU3Ec__Iterator0_t2060_VTable[8] = 
{
	120,
	125,
	122,
	123,
	1413,
	1414,
	1415,
	1416,
};
extern const Il2CppType IEnumerator_1_t2270_0_0_0;
static const Il2CppType* U3CGetEnumeratorU3Ec__Iterator0_t2060_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDisposable_t319_0_0_0,
	&IEnumerator_1_t2270_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CGetEnumeratorU3Ec__Iterator0_t2060_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &IDisposable_t319_0_0_0, 6},
	{ &IEnumerator_1_t2270_0_0_0, 7},
};
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t2060_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition U3CGetEnumeratorU3Ec__Iterator0_t2060_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 3040 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t2060_1_0_0;
struct U3CGetEnumeratorU3Ec__Iterator0_t2060;
const Il2CppTypeDefinitionMetadata U3CGetEnumeratorU3Ec__Iterator0_t2060_DefinitionMetadata = 
{
	&ArrayReadOnlyList_1_t2059_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CGetEnumeratorU3Ec__Iterator0_t2060_InterfacesTypeInfos/* implementedInterfaces */
	, U3CGetEnumeratorU3Ec__Iterator0_t2060_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetEnumeratorU3Ec__Iterator0_t2060_VTable/* vtableMethods */
	, U3CGetEnumeratorU3Ec__Iterator0_t2060_RGCTXData/* rgctxDefinition */
	, 2580/* fieldStart */
	, 4045/* methodStart */
	, -1/* eventStart */
	, 755/* propertyStart */

};
TypeInfo U3CGetEnumeratorU3Ec__Iterator0_t2060_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetEnumerator>c__Iterator0"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetEnumeratorU3Ec__Iterator0_t2060_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 741/* custom_attributes_cache */
	, &U3CGetEnumeratorU3Ec__Iterator0_t2060_0_0_0/* byval_arg */
	, &U3CGetEnumeratorU3Ec__Iterator0_t2060_1_0_0/* this_arg */
	, &U3CGetEnumeratorU3Ec__Iterator0_t2060_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 74/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Array/Swapper
#include "mscorlib_System_Array_Swapper.h"
// Metadata Definition System.Array/Swapper
extern TypeInfo Swapper_t764_il2cpp_TypeInfo;
// System.Array/Swapper
#include "mscorlib_System_Array_SwapperMethodDeclarations.h"
static const EncodedMethodIndex Swapper_t764_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	1417,
	1418,
	1419,
};
static Il2CppInterfaceOffsetPair Swapper_t764_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Swapper_t764_1_0_0;
struct Swapper_t764;
const Il2CppTypeDefinitionMetadata Swapper_t764_DefinitionMetadata = 
{
	&Array_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Swapper_t764_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, Swapper_t764_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4050/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Swapper_t764_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Swapper"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Swapper_t764_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Swapper_t764_0_0_0/* byval_arg */
	, &Swapper_t764_1_0_0/* this_arg */
	, &Swapper_t764_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_Swapper_t764/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Swapper_t764)/* instance_size */
	, sizeof (Swapper_t764)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.ICollection
extern TypeInfo ICollection_t569_il2cpp_TypeInfo;
static const Il2CppType* ICollection_t569_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICollection_t569_1_0_0;
struct ICollection_t569;
const Il2CppTypeDefinitionMetadata ICollection_t569_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ICollection_t569_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4054/* methodStart */
	, -1/* eventStart */
	, 757/* propertyStart */

};
TypeInfo ICollection_t569_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &ICollection_t569_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 745/* custom_attributes_cache */
	, &ICollection_t569_0_0_0/* byval_arg */
	, &ICollection_t569_1_0_0/* this_arg */
	, &ICollection_t569_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.IList
extern TypeInfo IList_t519_il2cpp_TypeInfo;
static const Il2CppType* IList_t519_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_t569_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IList_t519_1_0_0;
struct IList_t519;
const Il2CppTypeDefinitionMetadata IList_t519_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IList_t519_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4057/* methodStart */
	, -1/* eventStart */
	, 759/* propertyStart */

};
TypeInfo IList_t519_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IList_t519_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 746/* custom_attributes_cache */
	, &IList_t519_0_0_0/* byval_arg */
	, &IList_t519_1_0_0/* this_arg */
	, &IList_t519_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.IList`1
extern TypeInfo IList_1_t2061_il2cpp_TypeInfo;
extern const Il2CppType ICollection_1_t2272_0_0_0;
extern const Il2CppType IEnumerable_1_t2273_0_0_0;
static const Il2CppType* IList_1_t2061_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_1_t2272_0_0_0,
	&IEnumerable_1_t2273_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IList_1_t2061_0_0_0;
extern const Il2CppType IList_1_t2061_1_0_0;
struct IList_1_t2061;
const Il2CppTypeDefinitionMetadata IList_1_t2061_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IList_1_t2061_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4066/* methodStart */
	, -1/* eventStart */
	, 760/* propertyStart */

};
TypeInfo IList_1_t2061_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &IList_1_t2061_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 747/* custom_attributes_cache */
	, &IList_1_t2061_0_0_0/* byval_arg */
	, &IList_1_t2061_1_0_0/* this_arg */
	, &IList_1_t2061_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 75/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.ICollection`1
extern TypeInfo ICollection_1_t2062_il2cpp_TypeInfo;
extern const Il2CppType IEnumerable_1_t2274_0_0_0;
static const Il2CppType* ICollection_1_t2062_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&IEnumerable_1_t2274_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICollection_1_t2062_0_0_0;
extern const Il2CppType ICollection_1_t2062_1_0_0;
struct ICollection_1_t2062;
const Il2CppTypeDefinitionMetadata ICollection_1_t2062_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ICollection_1_t2062_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4071/* methodStart */
	, -1/* eventStart */
	, 761/* propertyStart */

};
TypeInfo ICollection_1_t2062_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &ICollection_1_t2062_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICollection_1_t2062_0_0_0/* byval_arg */
	, &ICollection_1_t2062_1_0_0/* this_arg */
	, &ICollection_1_t2062_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 76/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Void
#include "mscorlib_System_Void.h"
// Metadata Definition System.Void
extern TypeInfo Void_t765_il2cpp_TypeInfo;
// System.Void
#include "mscorlib_System_VoidMethodDeclarations.h"
static const EncodedMethodIndex Void_t765_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Void_t765_0_0_0;
extern const Il2CppType Void_t765_1_0_0;
const Il2CppTypeDefinitionMetadata Void_t765_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Void_t765_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Void_t765_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Void"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Void_t765_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 748/* custom_attributes_cache */
	, &Void_t765_0_0_0/* byval_arg */
	, &Void_t765_1_0_0/* this_arg */
	, &Void_t765_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Void_t765)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Void_t765)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, 1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Type
#include "mscorlib_System_Type.h"
// Metadata Definition System.Type
extern TypeInfo Type_t_il2cpp_TypeInfo;
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
static const EncodedMethodIndex Type_t_VTable[81] = 
{
	1420,
	125,
	1421,
	1422,
	1423,
	1424,
	1425,
	1426,
	0,
	1427,
	0,
	0,
	0,
	0,
	0,
	0,
	1428,
	0,
	0,
	1429,
	1430,
	1431,
	1432,
	1433,
	1434,
	1435,
	1436,
	1437,
	1438,
	1439,
	1440,
	1441,
	1442,
	1443,
	0,
	1444,
	0,
	1445,
	1446,
	0,
	1447,
	1448,
	0,
	0,
	0,
	0,
	1449,
	1450,
	1451,
	1452,
	0,
	0,
	0,
	1453,
	1454,
	1455,
	1456,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	1457,
	1458,
	1459,
	1460,
	1461,
	1462,
	1463,
	0,
	0,
	1464,
	1465,
	1466,
	1467,
	1468,
	1469,
	1470,
};
extern const Il2CppType IReflect_t2065_0_0_0;
extern const Il2CppType _Type_t2063_0_0_0;
static const Il2CppType* Type_t_InterfacesTypeInfos[] = 
{
	&IReflect_t2065_0_0_0,
	&_Type_t2063_0_0_0,
};
extern const Il2CppType ICustomAttributeProvider_t1404_0_0_0;
extern const Il2CppType _MemberInfo_t2064_0_0_0;
static Il2CppInterfaceOffsetPair Type_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t1404_0_0_0, 4},
	{ &_MemberInfo_t2064_0_0_0, 6},
	{ &IReflect_t2065_0_0_0, 14},
	{ &_Type_t2063_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_1_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
struct Type_t;
const Il2CppTypeDefinitionMetadata Type_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Type_t_InterfacesTypeInfos/* implementedInterfaces */
	, Type_t_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, Type_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2584/* fieldStart */
	, 4078/* methodStart */
	, -1/* eventStart */
	, 763/* propertyStart */

};
TypeInfo Type_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Type"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Type_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 749/* custom_attributes_cache */
	, &Type_t_0_0_0/* byval_arg */
	, &Type_t_1_0_0/* this_arg */
	, &Type_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Type_t)/* instance_size */
	, sizeof (Type_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Type_t_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 96/* method_count */
	, 33/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 81/* vtable_count */
	, 2/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// Metadata Definition System.Reflection.MemberInfo
extern TypeInfo MemberInfo_t_il2cpp_TypeInfo;
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
static const EncodedMethodIndex MemberInfo_t_VTable[14] = 
{
	120,
	125,
	122,
	123,
	1423,
	1424,
	0,
	0,
	0,
	0,
	1471,
	0,
	0,
	0,
};
static const Il2CppType* MemberInfo_t_InterfacesTypeInfos[] = 
{
	&ICustomAttributeProvider_t1404_0_0_0,
	&_MemberInfo_t2064_0_0_0,
};
static Il2CppInterfaceOffsetPair MemberInfo_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t1404_0_0_0, 4},
	{ &_MemberInfo_t2064_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberInfo_t_1_0_0;
struct MemberInfo_t;
const Il2CppTypeDefinitionMetadata MemberInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MemberInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, MemberInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MemberInfo_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4174/* methodStart */
	, -1/* eventStart */
	, 796/* propertyStart */

};
TypeInfo MemberInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberInfo"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MemberInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 757/* custom_attributes_cache */
	, &MemberInfo_t_0_0_0/* byval_arg */
	, &MemberInfo_t_1_0_0/* this_arg */
	, &MemberInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberInfo_t)/* instance_size */
	, sizeof (MemberInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Reflection.ICustomAttributeProvider
extern TypeInfo ICustomAttributeProvider_t1404_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICustomAttributeProvider_t1404_1_0_0;
struct ICustomAttributeProvider_t1404;
const Il2CppTypeDefinitionMetadata ICustomAttributeProvider_t1404_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4183/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICustomAttributeProvider_t1404_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICustomAttributeProvider"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &ICustomAttributeProvider_t1404_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 758/* custom_attributes_cache */
	, &ICustomAttributeProvider_t1404_0_0_0/* byval_arg */
	, &ICustomAttributeProvider_t1404_1_0_0/* this_arg */
	, &ICustomAttributeProvider_t1404_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MemberInfo
extern TypeInfo _MemberInfo_t2064_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MemberInfo_t2064_1_0_0;
struct _MemberInfo_t2064;
const Il2CppTypeDefinitionMetadata _MemberInfo_t2064_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _MemberInfo_t2064_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MemberInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_MemberInfo_t2064_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 759/* custom_attributes_cache */
	, &_MemberInfo_t2064_0_0_0/* byval_arg */
	, &_MemberInfo_t2064_1_0_0/* this_arg */
	, &_MemberInfo_t2064_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Reflection.IReflect
extern TypeInfo IReflect_t2065_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IReflect_t2065_1_0_0;
struct IReflect_t2065;
const Il2CppTypeDefinitionMetadata IReflect_t2065_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IReflect_t2065_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IReflect"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &IReflect_t2065_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 760/* custom_attributes_cache */
	, &IReflect_t2065_0_0_0/* byval_arg */
	, &IReflect_t2065_1_0_0/* this_arg */
	, &IReflect_t2065_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Type
extern TypeInfo _Type_t2063_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Type_t2063_1_0_0;
struct _Type_t2063;
const Il2CppTypeDefinitionMetadata _Type_t2063_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Type_t2063_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Type"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_Type_t2063_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 761/* custom_attributes_cache */
	, &_Type_t2063_0_0_0/* byval_arg */
	, &_Type_t2063_1_0_0/* this_arg */
	, &_Type_t2063_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Exception
#include "mscorlib_System_Exception.h"
// Metadata Definition System.Exception
extern TypeInfo Exception_t65_il2cpp_TypeInfo;
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
static const EncodedMethodIndex Exception_t65_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
extern const Il2CppType _Exception_t2066_0_0_0;
static const Il2CppType* Exception_t65_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
	&_Exception_t2066_0_0_0,
};
static Il2CppInterfaceOffsetPair Exception_t65_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Exception_t65_0_0_0;
extern const Il2CppType Exception_t65_1_0_0;
struct Exception_t65;
const Il2CppTypeDefinitionMetadata Exception_t65_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Exception_t65_InterfacesTypeInfos/* implementedInterfaces */
	, Exception_t65_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Exception_t65_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2592/* fieldStart */
	, 4185/* methodStart */
	, -1/* eventStart */
	, 801/* propertyStart */

};
TypeInfo Exception_t65_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Exception"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Exception_t65_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 762/* custom_attributes_cache */
	, &Exception_t65_0_0_0/* byval_arg */
	, &Exception_t65_1_0_0/* this_arg */
	, &Exception_t65_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Exception_t65)/* instance_size */
	, sizeof (Exception_t65)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 6/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Exception
extern TypeInfo _Exception_t2066_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Exception_t2066_1_0_0;
struct _Exception_t2066;
const Il2CppTypeDefinitionMetadata _Exception_t2066_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Exception_t2066_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Exception"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_Exception_t2066_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 763/* custom_attributes_cache */
	, &_Exception_t2066_0_0_0/* byval_arg */
	, &_Exception_t2066_1_0_0/* this_arg */
	, &_Exception_t2066_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
// Metadata Definition System.RuntimeFieldHandle
extern TypeInfo RuntimeFieldHandle_t768_il2cpp_TypeInfo;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandleMethodDeclarations.h"
static const EncodedMethodIndex RuntimeFieldHandle_t768_VTable[5] = 
{
	1472,
	125,
	1473,
	152,
	1474,
};
static const Il2CppType* RuntimeFieldHandle_t768_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair RuntimeFieldHandle_t768_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeFieldHandle_t768_0_0_0;
extern const Il2CppType RuntimeFieldHandle_t768_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeFieldHandle_t768_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RuntimeFieldHandle_t768_InterfacesTypeInfos/* implementedInterfaces */
	, RuntimeFieldHandle_t768_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, RuntimeFieldHandle_t768_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2603/* fieldStart */
	, 4199/* methodStart */
	, -1/* eventStart */
	, 807/* propertyStart */

};
TypeInfo RuntimeFieldHandle_t768_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeFieldHandle"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &RuntimeFieldHandle_t768_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 764/* custom_attributes_cache */
	, &RuntimeFieldHandle_t768_0_0_0/* byval_arg */
	, &RuntimeFieldHandle_t768_1_0_0/* this_arg */
	, &RuntimeFieldHandle_t768_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeFieldHandle_t768)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeFieldHandle_t768)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeFieldHandle_t768 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// Metadata Definition System.RuntimeTypeHandle
extern TypeInfo RuntimeTypeHandle_t767_il2cpp_TypeInfo;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandleMethodDeclarations.h"
static const EncodedMethodIndex RuntimeTypeHandle_t767_VTable[5] = 
{
	1475,
	125,
	1476,
	152,
	1477,
};
static const Il2CppType* RuntimeTypeHandle_t767_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair RuntimeTypeHandle_t767_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeTypeHandle_t767_0_0_0;
extern const Il2CppType RuntimeTypeHandle_t767_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeTypeHandle_t767_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RuntimeTypeHandle_t767_InterfacesTypeInfos/* implementedInterfaces */
	, RuntimeTypeHandle_t767_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, RuntimeTypeHandle_t767_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2604/* fieldStart */
	, 4204/* methodStart */
	, -1/* eventStart */
	, 808/* propertyStart */

};
TypeInfo RuntimeTypeHandle_t767_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeTypeHandle"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &RuntimeTypeHandle_t767_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 766/* custom_attributes_cache */
	, &RuntimeTypeHandle_t767_0_0_0/* byval_arg */
	, &RuntimeTypeHandle_t767_1_0_0/* this_arg */
	, &RuntimeTypeHandle_t767_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeTypeHandle_t767)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeTypeHandle_t767)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeTypeHandle_t767 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// Metadata Definition System.ParamArrayAttribute
extern TypeInfo ParamArrayAttribute_t769_il2cpp_TypeInfo;
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
static const EncodedMethodIndex ParamArrayAttribute_t769_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair ParamArrayAttribute_t769_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParamArrayAttribute_t769_0_0_0;
extern const Il2CppType ParamArrayAttribute_t769_1_0_0;
struct ParamArrayAttribute_t769;
const Il2CppTypeDefinitionMetadata ParamArrayAttribute_t769_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ParamArrayAttribute_t769_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, ParamArrayAttribute_t769_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4209/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ParamArrayAttribute_t769_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParamArrayAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ParamArrayAttribute_t769_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 768/* custom_attributes_cache */
	, &ParamArrayAttribute_t769_0_0_0/* byval_arg */
	, &ParamArrayAttribute_t769_1_0_0/* this_arg */
	, &ParamArrayAttribute_t769_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParamArrayAttribute_t769)/* instance_size */
	, sizeof (ParamArrayAttribute_t769)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.OutAttribute
#include "mscorlib_System_Runtime_InteropServices_OutAttribute.h"
// Metadata Definition System.Runtime.InteropServices.OutAttribute
extern TypeInfo OutAttribute_t770_il2cpp_TypeInfo;
// System.Runtime.InteropServices.OutAttribute
#include "mscorlib_System_Runtime_InteropServices_OutAttributeMethodDeclarations.h"
static const EncodedMethodIndex OutAttribute_t770_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair OutAttribute_t770_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OutAttribute_t770_0_0_0;
extern const Il2CppType OutAttribute_t770_1_0_0;
struct OutAttribute_t770;
const Il2CppTypeDefinitionMetadata OutAttribute_t770_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OutAttribute_t770_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, OutAttribute_t770_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4210/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OutAttribute_t770_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OutAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &OutAttribute_t770_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 769/* custom_attributes_cache */
	, &OutAttribute_t770_0_0_0/* byval_arg */
	, &OutAttribute_t770_1_0_0/* this_arg */
	, &OutAttribute_t770_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OutAttribute_t770)/* instance_size */
	, sizeof (OutAttribute_t770)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// Metadata Definition System.ObsoleteAttribute
extern TypeInfo ObsoleteAttribute_t771_il2cpp_TypeInfo;
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
static const EncodedMethodIndex ObsoleteAttribute_t771_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair ObsoleteAttribute_t771_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObsoleteAttribute_t771_0_0_0;
extern const Il2CppType ObsoleteAttribute_t771_1_0_0;
struct ObsoleteAttribute_t771;
const Il2CppTypeDefinitionMetadata ObsoleteAttribute_t771_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObsoleteAttribute_t771_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, ObsoleteAttribute_t771_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2605/* fieldStart */
	, 4211/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ObsoleteAttribute_t771_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObsoleteAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ObsoleteAttribute_t771_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 770/* custom_attributes_cache */
	, &ObsoleteAttribute_t771_0_0_0/* byval_arg */
	, &ObsoleteAttribute_t771_1_0_0/* this_arg */
	, &ObsoleteAttribute_t771_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObsoleteAttribute_t771)/* instance_size */
	, sizeof (ObsoleteAttribute_t771)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.DllImportAttribute
#include "mscorlib_System_Runtime_InteropServices_DllImportAttribute.h"
// Metadata Definition System.Runtime.InteropServices.DllImportAttribute
extern TypeInfo DllImportAttribute_t772_il2cpp_TypeInfo;
// System.Runtime.InteropServices.DllImportAttribute
#include "mscorlib_System_Runtime_InteropServices_DllImportAttributeMethodDeclarations.h"
static const EncodedMethodIndex DllImportAttribute_t772_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair DllImportAttribute_t772_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DllImportAttribute_t772_0_0_0;
extern const Il2CppType DllImportAttribute_t772_1_0_0;
struct DllImportAttribute_t772;
const Il2CppTypeDefinitionMetadata DllImportAttribute_t772_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DllImportAttribute_t772_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, DllImportAttribute_t772_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2607/* fieldStart */
	, 4214/* methodStart */
	, -1/* eventStart */
	, 809/* propertyStart */

};
TypeInfo DllImportAttribute_t772_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DllImportAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &DllImportAttribute_t772_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 771/* custom_attributes_cache */
	, &DllImportAttribute_t772_0_0_0/* byval_arg */
	, &DllImportAttribute_t772_1_0_0/* this_arg */
	, &DllImportAttribute_t772_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DllImportAttribute_t772)/* instance_size */
	, sizeof (DllImportAttribute_t772)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.MarshalAsAttribute
#include "mscorlib_System_Runtime_InteropServices_MarshalAsAttribute.h"
// Metadata Definition System.Runtime.InteropServices.MarshalAsAttribute
extern TypeInfo MarshalAsAttribute_t773_il2cpp_TypeInfo;
// System.Runtime.InteropServices.MarshalAsAttribute
#include "mscorlib_System_Runtime_InteropServices_MarshalAsAttributeMethodDeclarations.h"
static const EncodedMethodIndex MarshalAsAttribute_t773_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair MarshalAsAttribute_t773_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MarshalAsAttribute_t773_0_0_0;
extern const Il2CppType MarshalAsAttribute_t773_1_0_0;
struct MarshalAsAttribute_t773;
const Il2CppTypeDefinitionMetadata MarshalAsAttribute_t773_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MarshalAsAttribute_t773_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, MarshalAsAttribute_t773_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2616/* fieldStart */
	, 4216/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MarshalAsAttribute_t773_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MarshalAsAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &MarshalAsAttribute_t773_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 772/* custom_attributes_cache */
	, &MarshalAsAttribute_t773_0_0_0/* byval_arg */
	, &MarshalAsAttribute_t773_1_0_0/* this_arg */
	, &MarshalAsAttribute_t773_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MarshalAsAttribute_t773)/* instance_size */
	, sizeof (MarshalAsAttribute_t773)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.InAttribute
#include "mscorlib_System_Runtime_InteropServices_InAttribute.h"
// Metadata Definition System.Runtime.InteropServices.InAttribute
extern TypeInfo InAttribute_t774_il2cpp_TypeInfo;
// System.Runtime.InteropServices.InAttribute
#include "mscorlib_System_Runtime_InteropServices_InAttributeMethodDeclarations.h"
static const EncodedMethodIndex InAttribute_t774_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair InAttribute_t774_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InAttribute_t774_0_0_0;
extern const Il2CppType InAttribute_t774_1_0_0;
struct InAttribute_t774;
const Il2CppTypeDefinitionMetadata InAttribute_t774_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InAttribute_t774_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, InAttribute_t774_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4217/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InAttribute_t774_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &InAttribute_t774_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 775/* custom_attributes_cache */
	, &InAttribute_t774_0_0_0/* byval_arg */
	, &InAttribute_t774_1_0_0/* this_arg */
	, &InAttribute_t774_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InAttribute_t774)/* instance_size */
	, sizeof (InAttribute_t774)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.ConditionalAttribute
#include "mscorlib_System_Diagnostics_ConditionalAttribute.h"
// Metadata Definition System.Diagnostics.ConditionalAttribute
extern TypeInfo ConditionalAttribute_t775_il2cpp_TypeInfo;
// System.Diagnostics.ConditionalAttribute
#include "mscorlib_System_Diagnostics_ConditionalAttributeMethodDeclarations.h"
static const EncodedMethodIndex ConditionalAttribute_t775_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair ConditionalAttribute_t775_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConditionalAttribute_t775_0_0_0;
extern const Il2CppType ConditionalAttribute_t775_1_0_0;
struct ConditionalAttribute_t775;
const Il2CppTypeDefinitionMetadata ConditionalAttribute_t775_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConditionalAttribute_t775_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, ConditionalAttribute_t775_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2623/* fieldStart */
	, 4218/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConditionalAttribute_t775_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConditionalAttribute"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &ConditionalAttribute_t775_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 776/* custom_attributes_cache */
	, &ConditionalAttribute_t775_0_0_0/* byval_arg */
	, &ConditionalAttribute_t775_1_0_0/* this_arg */
	, &ConditionalAttribute_t775_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConditionalAttribute_t775)/* instance_size */
	, sizeof (ConditionalAttribute_t775)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// Metadata Definition System.Runtime.InteropServices.GuidAttribute
extern TypeInfo GuidAttribute_t776_il2cpp_TypeInfo;
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
static const EncodedMethodIndex GuidAttribute_t776_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair GuidAttribute_t776_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GuidAttribute_t776_0_0_0;
extern const Il2CppType GuidAttribute_t776_1_0_0;
struct GuidAttribute_t776;
const Il2CppTypeDefinitionMetadata GuidAttribute_t776_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GuidAttribute_t776_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, GuidAttribute_t776_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2624/* fieldStart */
	, 4219/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GuidAttribute_t776_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GuidAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &GuidAttribute_t776_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 777/* custom_attributes_cache */
	, &GuidAttribute_t776_0_0_0/* byval_arg */
	, &GuidAttribute_t776_1_0_0/* this_arg */
	, &GuidAttribute_t776_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GuidAttribute_t776)/* instance_size */
	, sizeof (GuidAttribute_t776)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.ComImportAttribute
#include "mscorlib_System_Runtime_InteropServices_ComImportAttribute.h"
// Metadata Definition System.Runtime.InteropServices.ComImportAttribute
extern TypeInfo ComImportAttribute_t777_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ComImportAttribute
#include "mscorlib_System_Runtime_InteropServices_ComImportAttributeMethodDeclarations.h"
static const EncodedMethodIndex ComImportAttribute_t777_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair ComImportAttribute_t777_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ComImportAttribute_t777_0_0_0;
extern const Il2CppType ComImportAttribute_t777_1_0_0;
struct ComImportAttribute_t777;
const Il2CppTypeDefinitionMetadata ComImportAttribute_t777_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ComImportAttribute_t777_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, ComImportAttribute_t777_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4220/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ComImportAttribute_t777_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComImportAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &ComImportAttribute_t777_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 778/* custom_attributes_cache */
	, &ComImportAttribute_t777_0_0_0/* byval_arg */
	, &ComImportAttribute_t777_1_0_0/* this_arg */
	, &ComImportAttribute_t777_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComImportAttribute_t777)/* instance_size */
	, sizeof (ComImportAttribute_t777)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.OptionalAttribute
#include "mscorlib_System_Runtime_InteropServices_OptionalAttribute.h"
// Metadata Definition System.Runtime.InteropServices.OptionalAttribute
extern TypeInfo OptionalAttribute_t778_il2cpp_TypeInfo;
// System.Runtime.InteropServices.OptionalAttribute
#include "mscorlib_System_Runtime_InteropServices_OptionalAttributeMethodDeclarations.h"
static const EncodedMethodIndex OptionalAttribute_t778_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair OptionalAttribute_t778_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OptionalAttribute_t778_0_0_0;
extern const Il2CppType OptionalAttribute_t778_1_0_0;
struct OptionalAttribute_t778;
const Il2CppTypeDefinitionMetadata OptionalAttribute_t778_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OptionalAttribute_t778_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, OptionalAttribute_t778_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4221/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OptionalAttribute_t778_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OptionalAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &OptionalAttribute_t778_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 779/* custom_attributes_cache */
	, &OptionalAttribute_t778_0_0_0/* byval_arg */
	, &OptionalAttribute_t778_1_0_0/* this_arg */
	, &OptionalAttribute_t778_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OptionalAttribute_t778)/* instance_size */
	, sizeof (OptionalAttribute_t778)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// Metadata Definition System.Runtime.CompilerServices.CompilerGeneratedAttribute
extern TypeInfo CompilerGeneratedAttribute_t779_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
static const EncodedMethodIndex CompilerGeneratedAttribute_t779_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair CompilerGeneratedAttribute_t779_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompilerGeneratedAttribute_t779_0_0_0;
extern const Il2CppType CompilerGeneratedAttribute_t779_1_0_0;
struct CompilerGeneratedAttribute_t779;
const Il2CppTypeDefinitionMetadata CompilerGeneratedAttribute_t779_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CompilerGeneratedAttribute_t779_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, CompilerGeneratedAttribute_t779_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4222/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CompilerGeneratedAttribute_t779_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompilerGeneratedAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &CompilerGeneratedAttribute_t779_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 780/* custom_attributes_cache */
	, &CompilerGeneratedAttribute_t779_0_0_0/* byval_arg */
	, &CompilerGeneratedAttribute_t779_1_0_0/* this_arg */
	, &CompilerGeneratedAttribute_t779_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompilerGeneratedAttribute_t779)/* instance_size */
	, sizeof (CompilerGeneratedAttribute_t779)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// Metadata Definition System.Runtime.CompilerServices.InternalsVisibleToAttribute
extern TypeInfo InternalsVisibleToAttribute_t780_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
static const EncodedMethodIndex InternalsVisibleToAttribute_t780_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair InternalsVisibleToAttribute_t780_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InternalsVisibleToAttribute_t780_0_0_0;
extern const Il2CppType InternalsVisibleToAttribute_t780_1_0_0;
struct InternalsVisibleToAttribute_t780;
const Il2CppTypeDefinitionMetadata InternalsVisibleToAttribute_t780_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InternalsVisibleToAttribute_t780_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, InternalsVisibleToAttribute_t780_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2625/* fieldStart */
	, 4223/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InternalsVisibleToAttribute_t780_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalsVisibleToAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &InternalsVisibleToAttribute_t780_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 781/* custom_attributes_cache */
	, &InternalsVisibleToAttribute_t780_0_0_0/* byval_arg */
	, &InternalsVisibleToAttribute_t780_1_0_0/* this_arg */
	, &InternalsVisibleToAttribute_t780_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalsVisibleToAttribute_t780)/* instance_size */
	, sizeof (InternalsVisibleToAttribute_t780)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// Metadata Definition System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
extern TypeInfo RuntimeCompatibilityAttribute_t781_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
static const EncodedMethodIndex RuntimeCompatibilityAttribute_t781_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair RuntimeCompatibilityAttribute_t781_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeCompatibilityAttribute_t781_0_0_0;
extern const Il2CppType RuntimeCompatibilityAttribute_t781_1_0_0;
struct RuntimeCompatibilityAttribute_t781;
const Il2CppTypeDefinitionMetadata RuntimeCompatibilityAttribute_t781_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RuntimeCompatibilityAttribute_t781_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, RuntimeCompatibilityAttribute_t781_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2627/* fieldStart */
	, 4224/* methodStart */
	, -1/* eventStart */
	, 810/* propertyStart */

};
TypeInfo RuntimeCompatibilityAttribute_t781_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeCompatibilityAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &RuntimeCompatibilityAttribute_t781_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 782/* custom_attributes_cache */
	, &RuntimeCompatibilityAttribute_t781_0_0_0/* byval_arg */
	, &RuntimeCompatibilityAttribute_t781_1_0_0/* this_arg */
	, &RuntimeCompatibilityAttribute_t781_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeCompatibilityAttribute_t781)/* instance_size */
	, sizeof (RuntimeCompatibilityAttribute_t781)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// Metadata Definition System.Diagnostics.DebuggerHiddenAttribute
extern TypeInfo DebuggerHiddenAttribute_t782_il2cpp_TypeInfo;
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
static const EncodedMethodIndex DebuggerHiddenAttribute_t782_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair DebuggerHiddenAttribute_t782_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DebuggerHiddenAttribute_t782_0_0_0;
extern const Il2CppType DebuggerHiddenAttribute_t782_1_0_0;
struct DebuggerHiddenAttribute_t782;
const Il2CppTypeDefinitionMetadata DebuggerHiddenAttribute_t782_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebuggerHiddenAttribute_t782_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, DebuggerHiddenAttribute_t782_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4226/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DebuggerHiddenAttribute_t782_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebuggerHiddenAttribute"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &DebuggerHiddenAttribute_t782_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 783/* custom_attributes_cache */
	, &DebuggerHiddenAttribute_t782_0_0_0/* byval_arg */
	, &DebuggerHiddenAttribute_t782_1_0_0/* this_arg */
	, &DebuggerHiddenAttribute_t782_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebuggerHiddenAttribute_t782)/* instance_size */
	, sizeof (DebuggerHiddenAttribute_t782)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// Metadata Definition System.Reflection.DefaultMemberAttribute
extern TypeInfo DefaultMemberAttribute_t783_il2cpp_TypeInfo;
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
static const EncodedMethodIndex DefaultMemberAttribute_t783_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair DefaultMemberAttribute_t783_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DefaultMemberAttribute_t783_0_0_0;
extern const Il2CppType DefaultMemberAttribute_t783_1_0_0;
struct DefaultMemberAttribute_t783;
const Il2CppTypeDefinitionMetadata DefaultMemberAttribute_t783_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultMemberAttribute_t783_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, DefaultMemberAttribute_t783_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2628/* fieldStart */
	, 4227/* methodStart */
	, -1/* eventStart */
	, 811/* propertyStart */

};
TypeInfo DefaultMemberAttribute_t783_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultMemberAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &DefaultMemberAttribute_t783_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 784/* custom_attributes_cache */
	, &DefaultMemberAttribute_t783_0_0_0/* byval_arg */
	, &DefaultMemberAttribute_t783_1_0_0/* this_arg */
	, &DefaultMemberAttribute_t783_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultMemberAttribute_t783)/* instance_size */
	, sizeof (DefaultMemberAttribute_t783)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttr.h"
// Metadata Definition System.Runtime.CompilerServices.DecimalConstantAttribute
extern TypeInfo DecimalConstantAttribute_t784_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttrMethodDeclarations.h"
static const EncodedMethodIndex DecimalConstantAttribute_t784_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair DecimalConstantAttribute_t784_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecimalConstantAttribute_t784_0_0_0;
extern const Il2CppType DecimalConstantAttribute_t784_1_0_0;
struct DecimalConstantAttribute_t784;
const Il2CppTypeDefinitionMetadata DecimalConstantAttribute_t784_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DecimalConstantAttribute_t784_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, DecimalConstantAttribute_t784_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2629/* fieldStart */
	, 4229/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DecimalConstantAttribute_t784_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecimalConstantAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &DecimalConstantAttribute_t784_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 785/* custom_attributes_cache */
	, &DecimalConstantAttribute_t784_0_0_0/* byval_arg */
	, &DecimalConstantAttribute_t784_1_0_0/* this_arg */
	, &DecimalConstantAttribute_t784_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecimalConstantAttribute_t784)/* instance_size */
	, sizeof (DecimalConstantAttribute_t784)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.FieldOffsetAttribute
#include "mscorlib_System_Runtime_InteropServices_FieldOffsetAttribute.h"
// Metadata Definition System.Runtime.InteropServices.FieldOffsetAttribute
extern TypeInfo FieldOffsetAttribute_t785_il2cpp_TypeInfo;
// System.Runtime.InteropServices.FieldOffsetAttribute
#include "mscorlib_System_Runtime_InteropServices_FieldOffsetAttributeMethodDeclarations.h"
static const EncodedMethodIndex FieldOffsetAttribute_t785_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair FieldOffsetAttribute_t785_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldOffsetAttribute_t785_0_0_0;
extern const Il2CppType FieldOffsetAttribute_t785_1_0_0;
struct FieldOffsetAttribute_t785;
const Il2CppTypeDefinitionMetadata FieldOffsetAttribute_t785_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FieldOffsetAttribute_t785_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, FieldOffsetAttribute_t785_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2634/* fieldStart */
	, 4230/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FieldOffsetAttribute_t785_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldOffsetAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &FieldOffsetAttribute_t785_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 787/* custom_attributes_cache */
	, &FieldOffsetAttribute_t785_0_0_0/* byval_arg */
	, &FieldOffsetAttribute_t785_1_0_0/* this_arg */
	, &FieldOffsetAttribute_t785_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldOffsetAttribute_t785)/* instance_size */
	, sizeof (FieldOffsetAttribute_t785)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.RuntimeArgumentHandle
#include "mscorlib_System_RuntimeArgumentHandle.h"
// Metadata Definition System.RuntimeArgumentHandle
extern TypeInfo RuntimeArgumentHandle_t786_il2cpp_TypeInfo;
// System.RuntimeArgumentHandle
#include "mscorlib_System_RuntimeArgumentHandleMethodDeclarations.h"
static const EncodedMethodIndex RuntimeArgumentHandle_t786_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeArgumentHandle_t786_0_0_0;
extern const Il2CppType RuntimeArgumentHandle_t786_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeArgumentHandle_t786_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, RuntimeArgumentHandle_t786_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2635/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RuntimeArgumentHandle_t786_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeArgumentHandle"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &RuntimeArgumentHandle_t786_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 788/* custom_attributes_cache */
	, &RuntimeArgumentHandle_t786_0_0_0/* byval_arg */
	, &RuntimeArgumentHandle_t786_1_0_0/* this_arg */
	, &RuntimeArgumentHandle_t786_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeArgumentHandle_t786)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeArgumentHandle_t786)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeArgumentHandle_t786 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
// Metadata Definition System.AsyncCallback
extern TypeInfo AsyncCallback_t45_il2cpp_TypeInfo;
// System.AsyncCallback
#include "mscorlib_System_AsyncCallbackMethodDeclarations.h"
static const EncodedMethodIndex AsyncCallback_t45_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	1478,
	1479,
	1480,
};
static Il2CppInterfaceOffsetPair AsyncCallback_t45_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsyncCallback_t45_0_0_0;
extern const Il2CppType AsyncCallback_t45_1_0_0;
struct AsyncCallback_t45;
const Il2CppTypeDefinitionMetadata AsyncCallback_t45_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AsyncCallback_t45_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, AsyncCallback_t45_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4231/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AsyncCallback_t45_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsyncCallback"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AsyncCallback_t45_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 789/* custom_attributes_cache */
	, &AsyncCallback_t45_0_0_0/* byval_arg */
	, &AsyncCallback_t45_1_0_0/* this_arg */
	, &AsyncCallback_t45_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AsyncCallback_t45/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsyncCallback_t45)/* instance_size */
	, sizeof (AsyncCallback_t45)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.IAsyncResult
extern TypeInfo IAsyncResult_t44_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IAsyncResult_t44_0_0_0;
extern const Il2CppType IAsyncResult_t44_1_0_0;
struct IAsyncResult_t44;
const Il2CppTypeDefinitionMetadata IAsyncResult_t44_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4235/* methodStart */
	, -1/* eventStart */
	, 812/* propertyStart */

};
TypeInfo IAsyncResult_t44_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAsyncResult"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IAsyncResult_t44_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 790/* custom_attributes_cache */
	, &IAsyncResult_t44_0_0_0/* byval_arg */
	, &IAsyncResult_t44_1_0_0/* this_arg */
	, &IAsyncResult_t44_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.TypedReference
#include "mscorlib_System_TypedReference.h"
// Metadata Definition System.TypedReference
extern TypeInfo TypedReference_t787_il2cpp_TypeInfo;
// System.TypedReference
#include "mscorlib_System_TypedReferenceMethodDeclarations.h"
static const EncodedMethodIndex TypedReference_t787_VTable[4] = 
{
	1481,
	125,
	1482,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypedReference_t787_0_0_0;
extern const Il2CppType TypedReference_t787_1_0_0;
const Il2CppTypeDefinitionMetadata TypedReference_t787_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, TypedReference_t787_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2636/* fieldStart */
	, 4238/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypedReference_t787_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypedReference"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &TypedReference_t787_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 791/* custom_attributes_cache */
	, &TypedReference_t787_0_0_0/* byval_arg */
	, &TypedReference_t787_1_0_0/* this_arg */
	, &TypedReference_t787_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypedReference_t787)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypedReference_t787)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(TypedReference_t787 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ArgIterator
#include "mscorlib_System_ArgIterator.h"
// Metadata Definition System.ArgIterator
extern TypeInfo ArgIterator_t788_il2cpp_TypeInfo;
// System.ArgIterator
#include "mscorlib_System_ArgIteratorMethodDeclarations.h"
static const EncodedMethodIndex ArgIterator_t788_VTable[4] = 
{
	1483,
	125,
	1484,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgIterator_t788_0_0_0;
extern const Il2CppType ArgIterator_t788_1_0_0;
const Il2CppTypeDefinitionMetadata ArgIterator_t788_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, ArgIterator_t788_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2639/* fieldStart */
	, 4240/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArgIterator_t788_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgIterator"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ArgIterator_t788_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgIterator_t788_0_0_0/* byval_arg */
	, &ArgIterator_t788_1_0_0/* this_arg */
	, &ArgIterator_t788_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgIterator_t788)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ArgIterator_t788)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MarshalByRefObject
#include "mscorlib_System_MarshalByRefObject.h"
// Metadata Definition System.MarshalByRefObject
extern TypeInfo MarshalByRefObject_t434_il2cpp_TypeInfo;
// System.MarshalByRefObject
#include "mscorlib_System_MarshalByRefObjectMethodDeclarations.h"
static const EncodedMethodIndex MarshalByRefObject_t434_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MarshalByRefObject_t434_0_0_0;
extern const Il2CppType MarshalByRefObject_t434_1_0_0;
struct MarshalByRefObject_t434;
const Il2CppTypeDefinitionMetadata MarshalByRefObject_t434_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MarshalByRefObject_t434_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2643/* fieldStart */
	, 4242/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MarshalByRefObject_t434_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MarshalByRefObject"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MarshalByRefObject_t434_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 792/* custom_attributes_cache */
	, &MarshalByRefObject_t434_0_0_0/* byval_arg */
	, &MarshalByRefObject_t434_1_0_0/* this_arg */
	, &MarshalByRefObject_t434_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MarshalByRefObject_t434)/* instance_size */
	, sizeof (MarshalByRefObject_t434)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Nullable`1
extern TypeInfo Nullable_1_t1410_il2cpp_TypeInfo;
static const EncodedMethodIndex Nullable_1_t1410_VTable[4] = 
{
	1485,
	125,
	1486,
	1487,
};
extern const Il2CppType Nullable_1_t2275_0_0_0;
extern const Il2CppType Nullable_1_t1410_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Nullable_1_t1410_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 3064 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2609 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3063 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Nullable_1_t1410_0_0_0;
extern const Il2CppType Nullable_1_t1410_1_0_0;
const Il2CppTypeDefinitionMetadata Nullable_1_t1410_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Nullable_1_t1410_VTable/* vtableMethods */
	, Nullable_1_t1410_RGCTXData/* rgctxDefinition */
	, 2644/* fieldStart */
	, 4243/* methodStart */
	, -1/* eventStart */
	, 815/* propertyStart */

};
TypeInfo Nullable_1_t1410_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Nullable`1"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Nullable_1_t1410_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Nullable_1_t1410_0_0_0/* byval_arg */
	, &Nullable_1_t1410_1_0_0/* this_arg */
	, &Nullable_1_t1410_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 77/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.RuntimeHelpers
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpers.h"
// Metadata Definition System.Runtime.CompilerServices.RuntimeHelpers
extern TypeInfo RuntimeHelpers_t790_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.RuntimeHelpers
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpersMethodDeclarations.h"
static const EncodedMethodIndex RuntimeHelpers_t790_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeHelpers_t790_0_0_0;
extern const Il2CppType RuntimeHelpers_t790_1_0_0;
struct RuntimeHelpers_t790;
const Il2CppTypeDefinitionMetadata RuntimeHelpers_t790_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RuntimeHelpers_t790_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4250/* methodStart */
	, -1/* eventStart */
	, 817/* propertyStart */

};
TypeInfo RuntimeHelpers_t790_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeHelpers"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &RuntimeHelpers_t790_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RuntimeHelpers_t790_0_0_0/* byval_arg */
	, &RuntimeHelpers_t790_1_0_0/* this_arg */
	, &RuntimeHelpers_t790_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeHelpers_t790)/* instance_size */
	, sizeof (RuntimeHelpers_t790)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Locale
#include "mscorlib_Locale.h"
// Metadata Definition Locale
extern TypeInfo Locale_t791_il2cpp_TypeInfo;
// Locale
#include "mscorlib_LocaleMethodDeclarations.h"
static const EncodedMethodIndex Locale_t791_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Locale_t791_0_0_0;
extern const Il2CppType Locale_t791_1_0_0;
struct Locale_t791;
const Il2CppTypeDefinitionMetadata Locale_t791_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Locale_t791_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4253/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Locale_t791_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Locale"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Locale_t791_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Locale_t791_0_0_0/* byval_arg */
	, &Locale_t791_1_0_0/* this_arg */
	, &Locale_t791_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Locale_t791)/* instance_size */
	, sizeof (Locale_t791)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttribute.h"
// Metadata Definition System.MonoTODOAttribute
extern TypeInfo MonoTODOAttribute_t792_il2cpp_TypeInfo;
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttributeMethodDeclarations.h"
static const EncodedMethodIndex MonoTODOAttribute_t792_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair MonoTODOAttribute_t792_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoTODOAttribute_t792_0_0_0;
extern const Il2CppType MonoTODOAttribute_t792_1_0_0;
struct MonoTODOAttribute_t792;
const Il2CppTypeDefinitionMetadata MonoTODOAttribute_t792_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoTODOAttribute_t792_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, MonoTODOAttribute_t792_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2646/* fieldStart */
	, 4255/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoTODOAttribute_t792_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTODOAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoTODOAttribute_t792_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 794/* custom_attributes_cache */
	, &MonoTODOAttribute_t792_0_0_0/* byval_arg */
	, &MonoTODOAttribute_t792_1_0_0/* this_arg */
	, &MonoTODOAttribute_t792_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTODOAttribute_t792)/* instance_size */
	, sizeof (MonoTODOAttribute_t792)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttribute.h"
// Metadata Definition System.MonoDocumentationNoteAttribute
extern TypeInfo MonoDocumentationNoteAttribute_t793_il2cpp_TypeInfo;
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttributeMethodDeclarations.h"
static const EncodedMethodIndex MonoDocumentationNoteAttribute_t793_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair MonoDocumentationNoteAttribute_t793_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoDocumentationNoteAttribute_t793_0_0_0;
extern const Il2CppType MonoDocumentationNoteAttribute_t793_1_0_0;
struct MonoDocumentationNoteAttribute_t793;
const Il2CppTypeDefinitionMetadata MonoDocumentationNoteAttribute_t793_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoDocumentationNoteAttribute_t793_InterfacesOffsets/* interfaceOffsets */
	, &MonoTODOAttribute_t792_0_0_0/* parent */
	, MonoDocumentationNoteAttribute_t793_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4257/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoDocumentationNoteAttribute_t793_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoDocumentationNoteAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoDocumentationNoteAttribute_t793_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 795/* custom_attributes_cache */
	, &MonoDocumentationNoteAttribute_t793_0_0_0/* byval_arg */
	, &MonoDocumentationNoteAttribute_t793_1_0_0/* this_arg */
	, &MonoDocumentationNoteAttribute_t793_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoDocumentationNoteAttribute_t793)/* instance_size */
	, sizeof (MonoDocumentationNoteAttribute_t793)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOn.h"
// Metadata Definition Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
extern TypeInfo SafeHandleZeroOrMinusOneIsInvalid_t794_il2cpp_TypeInfo;
// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOnMethodDeclarations.h"
static const EncodedMethodIndex SafeHandleZeroOrMinusOneIsInvalid_t794_VTable[8] = 
{
	120,
	1488,
	122,
	123,
	1489,
	1490,
	0,
	1491,
};
static const Il2CppType* SafeHandleZeroOrMinusOneIsInvalid_t794_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
};
static Il2CppInterfaceOffsetPair SafeHandleZeroOrMinusOneIsInvalid_t794_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SafeHandleZeroOrMinusOneIsInvalid_t794_0_0_0;
extern const Il2CppType SafeHandleZeroOrMinusOneIsInvalid_t794_1_0_0;
extern const Il2CppType SafeHandle_t795_0_0_0;
struct SafeHandleZeroOrMinusOneIsInvalid_t794;
const Il2CppTypeDefinitionMetadata SafeHandleZeroOrMinusOneIsInvalid_t794_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SafeHandleZeroOrMinusOneIsInvalid_t794_InterfacesTypeInfos/* implementedInterfaces */
	, SafeHandleZeroOrMinusOneIsInvalid_t794_InterfacesOffsets/* interfaceOffsets */
	, &SafeHandle_t795_0_0_0/* parent */
	, SafeHandleZeroOrMinusOneIsInvalid_t794_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4258/* methodStart */
	, -1/* eventStart */
	, 818/* propertyStart */

};
TypeInfo SafeHandleZeroOrMinusOneIsInvalid_t794_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SafeHandleZeroOrMinusOneIsInvalid"/* name */
	, "Microsoft.Win32.SafeHandles"/* namespaze */
	, NULL/* methods */
	, &SafeHandleZeroOrMinusOneIsInvalid_t794_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SafeHandleZeroOrMinusOneIsInvalid_t794_0_0_0/* byval_arg */
	, &SafeHandleZeroOrMinusOneIsInvalid_t794_1_0_0/* this_arg */
	, &SafeHandleZeroOrMinusOneIsInvalid_t794_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SafeHandleZeroOrMinusOneIsInvalid_t794)/* instance_size */
	, sizeof (SafeHandleZeroOrMinusOneIsInvalid_t794)/* actualSize */
	, 0/* element_size */
	, sizeof(void*)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Microsoft.Win32.SafeHandles.SafeWaitHandle
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeWaitHandle.h"
// Metadata Definition Microsoft.Win32.SafeHandles.SafeWaitHandle
extern TypeInfo SafeWaitHandle_t796_il2cpp_TypeInfo;
// Microsoft.Win32.SafeHandles.SafeWaitHandle
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeWaitHandleMethodDeclarations.h"
static const EncodedMethodIndex SafeWaitHandle_t796_VTable[8] = 
{
	120,
	1488,
	122,
	123,
	1489,
	1490,
	1492,
	1491,
};
static Il2CppInterfaceOffsetPair SafeWaitHandle_t796_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SafeWaitHandle_t796_0_0_0;
extern const Il2CppType SafeWaitHandle_t796_1_0_0;
struct SafeWaitHandle_t796;
const Il2CppTypeDefinitionMetadata SafeWaitHandle_t796_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SafeWaitHandle_t796_InterfacesOffsets/* interfaceOffsets */
	, &SafeHandleZeroOrMinusOneIsInvalid_t794_0_0_0/* parent */
	, SafeWaitHandle_t796_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4260/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SafeWaitHandle_t796_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SafeWaitHandle"/* name */
	, "Microsoft.Win32.SafeHandles"/* namespaze */
	, NULL/* methods */
	, &SafeWaitHandle_t796_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SafeWaitHandle_t796_0_0_0/* byval_arg */
	, &SafeWaitHandle_t796_1_0_0/* this_arg */
	, &SafeWaitHandle_t796_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SafeWaitHandle_t796)/* instance_size */
	, sizeof (SafeWaitHandle_t796)/* actualSize */
	, 0/* element_size */
	, sizeof(void*)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Globalization.Unicode.CodePointIndexer
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
// Metadata Definition Mono.Globalization.Unicode.CodePointIndexer
extern TypeInfo CodePointIndexer_t799_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.CodePointIndexer
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexerMethodDeclarations.h"
extern const Il2CppType TableRange_t797_0_0_0;
static const Il2CppType* CodePointIndexer_t799_il2cpp_TypeInfo__nestedTypes[1] =
{
	&TableRange_t797_0_0_0,
};
static const EncodedMethodIndex CodePointIndexer_t799_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CodePointIndexer_t799_0_0_0;
extern const Il2CppType CodePointIndexer_t799_1_0_0;
struct CodePointIndexer_t799;
const Il2CppTypeDefinitionMetadata CodePointIndexer_t799_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CodePointIndexer_t799_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CodePointIndexer_t799_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2647/* fieldStart */
	, 4262/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CodePointIndexer_t799_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CodePointIndexer"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &CodePointIndexer_t799_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CodePointIndexer_t799_0_0_0/* byval_arg */
	, &CodePointIndexer_t799_1_0_0/* this_arg */
	, &CodePointIndexer_t799_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CodePointIndexer_t799)/* instance_size */
	, sizeof (CodePointIndexer_t799)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
// Metadata Definition Mono.Globalization.Unicode.CodePointIndexer/TableRange
extern TypeInfo TableRange_t797_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRaMethodDeclarations.h"
static const EncodedMethodIndex TableRange_t797_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TableRange_t797_1_0_0;
const Il2CppTypeDefinitionMetadata TableRange_t797_DefinitionMetadata = 
{
	&CodePointIndexer_t799_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, TableRange_t797_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2651/* fieldStart */
	, 4264/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TableRange_t797_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TableRange"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &TableRange_t797_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TableRange_t797_0_0_0/* byval_arg */
	, &TableRange_t797_1_0_0/* this_arg */
	, &TableRange_t797_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TableRange_t797)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TableRange_t797)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(TableRange_t797 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057037/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.TailoringInfo
#include "mscorlib_Mono_Globalization_Unicode_TailoringInfo.h"
// Metadata Definition Mono.Globalization.Unicode.TailoringInfo
extern TypeInfo TailoringInfo_t800_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.TailoringInfo
#include "mscorlib_Mono_Globalization_Unicode_TailoringInfoMethodDeclarations.h"
static const EncodedMethodIndex TailoringInfo_t800_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TailoringInfo_t800_0_0_0;
extern const Il2CppType TailoringInfo_t800_1_0_0;
struct TailoringInfo_t800;
const Il2CppTypeDefinitionMetadata TailoringInfo_t800_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TailoringInfo_t800_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2656/* fieldStart */
	, 4265/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TailoringInfo_t800_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TailoringInfo"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &TailoringInfo_t800_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TailoringInfo_t800_0_0_0/* byval_arg */
	, &TailoringInfo_t800_1_0_0/* this_arg */
	, &TailoringInfo_t800_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TailoringInfo_t800)/* instance_size */
	, sizeof (TailoringInfo_t800)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.Contraction
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
// Metadata Definition Mono.Globalization.Unicode.Contraction
extern TypeInfo Contraction_t801_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.Contraction
#include "mscorlib_Mono_Globalization_Unicode_ContractionMethodDeclarations.h"
static const EncodedMethodIndex Contraction_t801_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Contraction_t801_0_0_0;
extern const Il2CppType Contraction_t801_1_0_0;
struct Contraction_t801;
const Il2CppTypeDefinitionMetadata Contraction_t801_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Contraction_t801_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2660/* fieldStart */
	, 4266/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Contraction_t801_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Contraction"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &Contraction_t801_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Contraction_t801_0_0_0/* byval_arg */
	, &Contraction_t801_1_0_0/* this_arg */
	, &Contraction_t801_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Contraction_t801)/* instance_size */
	, sizeof (Contraction_t801)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.ContractionComparer
#include "mscorlib_Mono_Globalization_Unicode_ContractionComparer.h"
// Metadata Definition Mono.Globalization.Unicode.ContractionComparer
extern TypeInfo ContractionComparer_t802_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.ContractionComparer
#include "mscorlib_Mono_Globalization_Unicode_ContractionComparerMethodDeclarations.h"
static const EncodedMethodIndex ContractionComparer_t802_VTable[5] = 
{
	120,
	125,
	122,
	123,
	1493,
};
extern const Il2CppType IComparer_t390_0_0_0;
static const Il2CppType* ContractionComparer_t802_InterfacesTypeInfos[] = 
{
	&IComparer_t390_0_0_0,
};
static Il2CppInterfaceOffsetPair ContractionComparer_t802_InterfacesOffsets[] = 
{
	{ &IComparer_t390_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContractionComparer_t802_0_0_0;
extern const Il2CppType ContractionComparer_t802_1_0_0;
struct ContractionComparer_t802;
const Il2CppTypeDefinitionMetadata ContractionComparer_t802_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContractionComparer_t802_InterfacesTypeInfos/* implementedInterfaces */
	, ContractionComparer_t802_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ContractionComparer_t802_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2663/* fieldStart */
	, 4267/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ContractionComparer_t802_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContractionComparer"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &ContractionComparer_t802_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContractionComparer_t802_0_0_0/* byval_arg */
	, &ContractionComparer_t802_1_0_0/* this_arg */
	, &ContractionComparer_t802_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContractionComparer_t802)/* instance_size */
	, sizeof (ContractionComparer_t802)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ContractionComparer_t802_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Globalization.Unicode.Level2Map
#include "mscorlib_Mono_Globalization_Unicode_Level2Map.h"
// Metadata Definition Mono.Globalization.Unicode.Level2Map
extern TypeInfo Level2Map_t803_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.Level2Map
#include "mscorlib_Mono_Globalization_Unicode_Level2MapMethodDeclarations.h"
static const EncodedMethodIndex Level2Map_t803_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Level2Map_t803_0_0_0;
extern const Il2CppType Level2Map_t803_1_0_0;
struct Level2Map_t803;
const Il2CppTypeDefinitionMetadata Level2Map_t803_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Level2Map_t803_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2664/* fieldStart */
	, 4270/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Level2Map_t803_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Level2Map"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &Level2Map_t803_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Level2Map_t803_0_0_0/* byval_arg */
	, &Level2Map_t803_1_0_0/* this_arg */
	, &Level2Map_t803_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Level2Map_t803)/* instance_size */
	, sizeof (Level2Map_t803)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.Level2MapComparer
#include "mscorlib_Mono_Globalization_Unicode_Level2MapComparer.h"
// Metadata Definition Mono.Globalization.Unicode.Level2MapComparer
extern TypeInfo Level2MapComparer_t804_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.Level2MapComparer
#include "mscorlib_Mono_Globalization_Unicode_Level2MapComparerMethodDeclarations.h"
static const EncodedMethodIndex Level2MapComparer_t804_VTable[5] = 
{
	120,
	125,
	122,
	123,
	1494,
};
static const Il2CppType* Level2MapComparer_t804_InterfacesTypeInfos[] = 
{
	&IComparer_t390_0_0_0,
};
static Il2CppInterfaceOffsetPair Level2MapComparer_t804_InterfacesOffsets[] = 
{
	{ &IComparer_t390_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Level2MapComparer_t804_0_0_0;
extern const Il2CppType Level2MapComparer_t804_1_0_0;
struct Level2MapComparer_t804;
const Il2CppTypeDefinitionMetadata Level2MapComparer_t804_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Level2MapComparer_t804_InterfacesTypeInfos/* implementedInterfaces */
	, Level2MapComparer_t804_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Level2MapComparer_t804_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2666/* fieldStart */
	, 4271/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Level2MapComparer_t804_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Level2MapComparer"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &Level2MapComparer_t804_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Level2MapComparer_t804_0_0_0/* byval_arg */
	, &Level2MapComparer_t804_1_0_0/* this_arg */
	, &Level2MapComparer_t804_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Level2MapComparer_t804)/* instance_size */
	, sizeof (Level2MapComparer_t804)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Level2MapComparer_t804_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Globalization.Unicode.MSCompatUnicodeTable
#include "mscorlib_Mono_Globalization_Unicode_MSCompatUnicodeTable.h"
// Metadata Definition Mono.Globalization.Unicode.MSCompatUnicodeTable
extern TypeInfo MSCompatUnicodeTable_t806_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.MSCompatUnicodeTable
#include "mscorlib_Mono_Globalization_Unicode_MSCompatUnicodeTableMethodDeclarations.h"
static const EncodedMethodIndex MSCompatUnicodeTable_t806_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MSCompatUnicodeTable_t806_0_0_0;
extern const Il2CppType MSCompatUnicodeTable_t806_1_0_0;
struct MSCompatUnicodeTable_t806;
const Il2CppTypeDefinitionMetadata MSCompatUnicodeTable_t806_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MSCompatUnicodeTable_t806_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2667/* fieldStart */
	, 4274/* methodStart */
	, -1/* eventStart */
	, 819/* propertyStart */

};
TypeInfo MSCompatUnicodeTable_t806_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MSCompatUnicodeTable"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &MSCompatUnicodeTable_t806_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MSCompatUnicodeTable_t806_0_0_0/* byval_arg */
	, &MSCompatUnicodeTable_t806_1_0_0/* this_arg */
	, &MSCompatUnicodeTable_t806_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MSCompatUnicodeTable_t806)/* instance_size */
	, sizeof (MSCompatUnicodeTable_t806)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MSCompatUnicodeTable_t806_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 1/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.MSCompatUnicodeTableUtil
#include "mscorlib_Mono_Globalization_Unicode_MSCompatUnicodeTableUtil.h"
// Metadata Definition Mono.Globalization.Unicode.MSCompatUnicodeTableUtil
extern TypeInfo MSCompatUnicodeTableUtil_t807_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.MSCompatUnicodeTableUtil
#include "mscorlib_Mono_Globalization_Unicode_MSCompatUnicodeTableUtilMethodDeclarations.h"
static const EncodedMethodIndex MSCompatUnicodeTableUtil_t807_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MSCompatUnicodeTableUtil_t807_0_0_0;
extern const Il2CppType MSCompatUnicodeTableUtil_t807_1_0_0;
struct MSCompatUnicodeTableUtil_t807;
const Il2CppTypeDefinitionMetadata MSCompatUnicodeTableUtil_t807_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MSCompatUnicodeTableUtil_t807_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2689/* fieldStart */
	, 4295/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MSCompatUnicodeTableUtil_t807_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MSCompatUnicodeTableUtil"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &MSCompatUnicodeTableUtil_t807_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MSCompatUnicodeTableUtil_t807_0_0_0/* byval_arg */
	, &MSCompatUnicodeTableUtil_t807_1_0_0/* this_arg */
	, &MSCompatUnicodeTableUtil_t807_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MSCompatUnicodeTableUtil_t807)/* instance_size */
	, sizeof (MSCompatUnicodeTableUtil_t807)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MSCompatUnicodeTableUtil_t807_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
