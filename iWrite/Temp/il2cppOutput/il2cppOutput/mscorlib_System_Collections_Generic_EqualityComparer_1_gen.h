﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Object>
struct EqualityComparer_1_t1451;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Object>
struct  EqualityComparer_1_t1451  : public Object_t
{
};
struct EqualityComparer_1_t1451_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Object>::_default
	EqualityComparer_1_t1451 * ____default_0;
};
