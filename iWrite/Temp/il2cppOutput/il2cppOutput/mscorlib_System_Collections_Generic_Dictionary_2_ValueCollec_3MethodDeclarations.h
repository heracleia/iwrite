﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
struct Enumerator_t1474;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1464;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m8641_gshared (Enumerator_t1474 * __this, Dictionary_2_t1464 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m8641(__this, ___host, method) (( void (*) (Enumerator_t1474 *, Dictionary_2_t1464 *, const MethodInfo*))Enumerator__ctor_m8641_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m8642_gshared (Enumerator_t1474 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m8642(__this, method) (( Object_t * (*) (Enumerator_t1474 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8642_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m8643_gshared (Enumerator_t1474 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m8643(__this, method) (( void (*) (Enumerator_t1474 *, const MethodInfo*))Enumerator_Dispose_m8643_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m8644_gshared (Enumerator_t1474 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m8644(__this, method) (( bool (*) (Enumerator_t1474 *, const MethodInfo*))Enumerator_MoveNext_m8644_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m8645_gshared (Enumerator_t1474 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m8645(__this, method) (( Object_t * (*) (Enumerator_t1474 *, const MethodInfo*))Enumerator_get_Current_m8645_gshared)(__this, method)
