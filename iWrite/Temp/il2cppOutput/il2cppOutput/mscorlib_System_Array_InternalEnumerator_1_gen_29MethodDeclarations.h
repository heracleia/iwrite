﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>
struct InternalEnumerator_1_t1541;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m9474_gshared (InternalEnumerator_1_t1541 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m9474(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1541 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m9474_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9475_gshared (InternalEnumerator_1_t1541 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9475(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1541 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9475_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m9476_gshared (InternalEnumerator_1_t1541 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m9476(__this, method) (( void (*) (InternalEnumerator_1_t1541 *, const MethodInfo*))InternalEnumerator_1_Dispose_m9476_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m9477_gshared (InternalEnumerator_1_t1541 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m9477(__this, method) (( bool (*) (InternalEnumerator_1_t1541 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m9477_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t149  InternalEnumerator_1_get_Current_m9478_gshared (InternalEnumerator_1_t1541 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m9478(__this, method) (( UICharInfo_t149  (*) (InternalEnumerator_1_t1541 *, const MethodInfo*))InternalEnumerator_1_get_Current_m9478_gshared)(__this, method)
