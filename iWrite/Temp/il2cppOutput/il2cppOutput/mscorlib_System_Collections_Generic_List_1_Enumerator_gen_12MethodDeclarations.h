﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>
struct Enumerator_t1787;
// System.Object
struct Object_t;
// System.Security.Policy.StrongName
struct StrongName_t1226;
// System.Collections.Generic.List`1<System.Security.Policy.StrongName>
struct List_1_t1430;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1MethodDeclarations.h"
#define Enumerator__ctor_m12007(__this, ___l, method) (( void (*) (Enumerator_t1787 *, List_1_t1430 *, const MethodInfo*))Enumerator__ctor_m8428_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m12008(__this, method) (( Object_t * (*) (Enumerator_t1787 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::Dispose()
#define Enumerator_Dispose_m12009(__this, method) (( void (*) (Enumerator_t1787 *, const MethodInfo*))Enumerator_Dispose_m8430_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::VerifyState()
#define Enumerator_VerifyState_m12010(__this, method) (( void (*) (Enumerator_t1787 *, const MethodInfo*))Enumerator_VerifyState_m8431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::MoveNext()
#define Enumerator_MoveNext_m12011(__this, method) (( bool (*) (Enumerator_t1787 *, const MethodInfo*))Enumerator_MoveNext_m8432_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::get_Current()
#define Enumerator_get_Current_m12012(__this, method) (( StrongName_t1226 * (*) (Enumerator_t1787 *, const MethodInfo*))Enumerator_get_Current_m8433_gshared)(__this, method)
