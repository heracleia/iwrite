﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
struct Enumerator_t1546;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t153;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m9562_gshared (Enumerator_t1546 * __this, List_1_t153 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m9562(__this, ___l, method) (( void (*) (Enumerator_t1546 *, List_1_t153 *, const MethodInfo*))Enumerator__ctor_m9562_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m9563_gshared (Enumerator_t1546 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m9563(__this, method) (( Object_t * (*) (Enumerator_t1546 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m9563_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C" void Enumerator_Dispose_m9564_gshared (Enumerator_t1546 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m9564(__this, method) (( void (*) (Enumerator_t1546 *, const MethodInfo*))Enumerator_Dispose_m9564_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m9565_gshared (Enumerator_t1546 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m9565(__this, method) (( void (*) (Enumerator_t1546 *, const MethodInfo*))Enumerator_VerifyState_m9565_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m9566_gshared (Enumerator_t1546 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m9566(__this, method) (( bool (*) (Enumerator_t1546 *, const MethodInfo*))Enumerator_MoveNext_m9566_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t150  Enumerator_get_Current_m9567_gshared (Enumerator_t1546 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m9567(__this, method) (( UILineInfo_t150  (*) (Enumerator_t1546 *, const MethodInfo*))Enumerator_get_Current_m9567_gshared)(__this, method)
