﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
struct KeyValuePair_2_t1503;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t56;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m8945(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1503 *, String_t*, GUIStyle_t56 *, const MethodInfo*))KeyValuePair_2__ctor_m8869_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Key()
#define KeyValuePair_2_get_Key_m8946(__this, method) (( String_t* (*) (KeyValuePair_2_t1503 *, const MethodInfo*))KeyValuePair_2_get_Key_m8870_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m8947(__this, ___value, method) (( void (*) (KeyValuePair_2_t1503 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m8871_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Value()
#define KeyValuePair_2_get_Value_m8948(__this, method) (( GUIStyle_t56 * (*) (KeyValuePair_2_t1503 *, const MethodInfo*))KeyValuePair_2_get_Value_m8872_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m8949(__this, ___value, method) (( void (*) (KeyValuePair_2_t1503 *, GUIStyle_t56 *, const MethodInfo*))KeyValuePair_2_set_Value_m8873_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::ToString()
#define KeyValuePair_2_ToString_m8950(__this, method) (( String_t* (*) (KeyValuePair_2_t1503 *, const MethodInfo*))KeyValuePair_2_ToString_m8874_gshared)(__this, method)
