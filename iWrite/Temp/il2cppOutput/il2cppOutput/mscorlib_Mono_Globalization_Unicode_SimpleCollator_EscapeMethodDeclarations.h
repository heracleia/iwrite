﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/Escape
struct Escape_t810;
struct Escape_t810_marshaled;

void Escape_t810_marshal(const Escape_t810& unmarshaled, Escape_t810_marshaled& marshaled);
void Escape_t810_marshal_back(const Escape_t810_marshaled& marshaled, Escape_t810& unmarshaled);
void Escape_t810_marshal_cleanup(Escape_t810_marshaled& marshaled);
