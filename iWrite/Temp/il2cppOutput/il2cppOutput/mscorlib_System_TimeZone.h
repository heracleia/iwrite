﻿#pragma once
#include <stdint.h>
// System.TimeZone
struct TimeZone_t1353;
// System.Object
#include "mscorlib_System_Object.h"
// System.TimeZone
struct  TimeZone_t1353  : public Object_t
{
};
struct TimeZone_t1353_StaticFields{
	// System.TimeZone System.TimeZone::currentTimeZone
	TimeZone_t1353 * ___currentTimeZone_0;
};
