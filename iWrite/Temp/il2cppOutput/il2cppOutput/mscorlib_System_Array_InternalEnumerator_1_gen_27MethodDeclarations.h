﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Keyframe>
struct InternalEnumerator_1_t1536;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m9415_gshared (InternalEnumerator_1_t1536 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m9415(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1536 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m9415_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9416_gshared (InternalEnumerator_1_t1536 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9416(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1536 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9416_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m9417_gshared (InternalEnumerator_1_t1536 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m9417(__this, method) (( void (*) (InternalEnumerator_1_t1536 *, const MethodInfo*))InternalEnumerator_1_Dispose_m9417_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m9418_gshared (InternalEnumerator_1_t1536 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m9418(__this, method) (( bool (*) (InternalEnumerator_1_t1536 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m9418_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern "C" Keyframe_t135  InternalEnumerator_1_get_Current_m9419_gshared (InternalEnumerator_1_t1536 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m9419(__this, method) (( Keyframe_t135  (*) (InternalEnumerator_1_t1536 *, const MethodInfo*))InternalEnumerator_1_get_Current_m9419_gshared)(__this, method)
