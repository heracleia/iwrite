﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>
struct InternalEnumerator_1_t1769;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.Emit.ILGenerator/LabelFixup
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11892_gshared (InternalEnumerator_1_t1769 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11892(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1769 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11892_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11893_gshared (InternalEnumerator_1_t1769 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11893(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1769 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11893_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11894_gshared (InternalEnumerator_1_t1769 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11894(__this, method) (( void (*) (InternalEnumerator_1_t1769 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11894_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11895_gshared (InternalEnumerator_1_t1769 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11895(__this, method) (( bool (*) (InternalEnumerator_1_t1769 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11895_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
extern "C" LabelFixup_t960  InternalEnumerator_1_get_Current_m11896_gshared (InternalEnumerator_1_t1769 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11896(__this, method) (( LabelFixup_t960  (*) (InternalEnumerator_1_t1769 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11896_gshared)(__this, method)
