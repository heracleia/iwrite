﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.Contraction
struct Contraction_t801;
// System.Char[]
struct CharU5BU5D_t300;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t102;

// System.Void Mono.Globalization.Unicode.Contraction::.ctor(System.Char[],System.String,System.Byte[])
extern "C" void Contraction__ctor_m4232 (Contraction_t801 * __this, CharU5BU5D_t300* ___source, String_t* ___replacement, ByteU5BU5D_t102* ___sortkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
