﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.LinkRef
struct LinkRef_t505;

// System.Void System.Text.RegularExpressions.LinkRef::.ctor()
extern "C" void LinkRef__ctor_m1869 (LinkRef_t505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
