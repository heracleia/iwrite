﻿#pragma once
#include <stdint.h>
// Mono.Security.X509.X509Stores
struct X509Stores_t568;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Security.X509.X509StoreManager
struct  X509StoreManager_t654  : public Object_t
{
};
struct X509StoreManager_t654_StaticFields{
	// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::_userStore
	X509Stores_t568 * ____userStore_0;
	// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::_machineStore
	X509Stores_t568 * ____machineStore_1;
};
