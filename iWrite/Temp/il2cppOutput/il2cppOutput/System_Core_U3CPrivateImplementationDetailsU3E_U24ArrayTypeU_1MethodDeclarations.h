﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$1024
struct U24ArrayTypeU241024_t614;
struct U24ArrayTypeU241024_t614_marshaled;

void U24ArrayTypeU241024_t614_marshal(const U24ArrayTypeU241024_t614& unmarshaled, U24ArrayTypeU241024_t614_marshaled& marshaled);
void U24ArrayTypeU241024_t614_marshal_back(const U24ArrayTypeU241024_t614_marshaled& marshaled, U24ArrayTypeU241024_t614& unmarshaled);
void U24ArrayTypeU241024_t614_marshal_cleanup(U24ArrayTypeU241024_t614_marshaled& marshaled);
