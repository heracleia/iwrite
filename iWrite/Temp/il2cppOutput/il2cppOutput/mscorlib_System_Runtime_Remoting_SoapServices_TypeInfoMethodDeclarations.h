﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.SoapServices/TypeInfo
struct TypeInfo_t1137;

// System.Void System.Runtime.Remoting.SoapServices/TypeInfo::.ctor()
extern "C" void TypeInfo__ctor_m6479 (TypeInfo_t1137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
