﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>
struct Enumerator_t1512;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1508;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m9114_gshared (Enumerator_t1512 * __this, Dictionary_2_t1508 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m9114(__this, ___host, method) (( void (*) (Enumerator_t1512 *, Dictionary_2_t1508 *, const MethodInfo*))Enumerator__ctor_m9114_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m9115_gshared (Enumerator_t1512 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m9115(__this, method) (( Object_t * (*) (Enumerator_t1512 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m9115_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m9116_gshared (Enumerator_t1512 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m9116(__this, method) (( void (*) (Enumerator_t1512 *, const MethodInfo*))Enumerator_Dispose_m9116_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m9117_gshared (Enumerator_t1512 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m9117(__this, method) (( bool (*) (Enumerator_t1512 *, const MethodInfo*))Enumerator_MoveNext_m9117_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m9118_gshared (Enumerator_t1512 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m9118(__this, method) (( Object_t * (*) (Enumerator_t1512 *, const MethodInfo*))Enumerator_get_Current_m9118_gshared)(__this, method)
