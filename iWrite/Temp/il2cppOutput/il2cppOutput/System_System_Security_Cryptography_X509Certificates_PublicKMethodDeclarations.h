﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t439;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t437;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t436;
// System.Security.Cryptography.Oid
struct Oid_t438;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t447;
// System.Byte[]
struct ByteU5BU5D_t102;
// System.Security.Cryptography.DSA
struct DSA_t561;
// System.Security.Cryptography.RSA
struct RSA_t562;

// System.Void System.Security.Cryptography.X509Certificates.PublicKey::.ctor(Mono.Security.X509.X509Certificate)
extern "C" void PublicKey__ctor_m1552 (PublicKey_t439 * __this, X509Certificate_t447 * ___certificate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedKeyValue()
extern "C" AsnEncodedData_t437 * PublicKey_get_EncodedKeyValue_m1553 (PublicKey_t439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedParameters()
extern "C" AsnEncodedData_t437 * PublicKey_get_EncodedParameters_m1554 (PublicKey_t439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::get_Key()
extern "C" AsymmetricAlgorithm_t436 * PublicKey_get_Key_m1555 (PublicKey_t439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::get_Oid()
extern "C" Oid_t438 * PublicKey_get_Oid_m1556 (PublicKey_t439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.PublicKey::GetUnsignedBigInteger(System.Byte[])
extern "C" ByteU5BU5D_t102* PublicKey_GetUnsignedBigInteger_m1557 (Object_t * __this /* static, unused */, ByteU5BU5D_t102* ___integer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeDSA(System.Byte[],System.Byte[])
extern "C" DSA_t561 * PublicKey_DecodeDSA_m1558 (Object_t * __this /* static, unused */, ByteU5BU5D_t102* ___rawPublicKey, ByteU5BU5D_t102* ___rawParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeRSA(System.Byte[])
extern "C" RSA_t562 * PublicKey_DecodeRSA_m1559 (Object_t * __this /* static, unused */, ByteU5BU5D_t102* ___rawPublicKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
