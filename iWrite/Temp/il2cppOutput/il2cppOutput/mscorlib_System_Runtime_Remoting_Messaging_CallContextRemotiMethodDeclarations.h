﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.CallContextRemotingData
struct CallContextRemotingData_t1092;

// System.Void System.Runtime.Remoting.Messaging.CallContextRemotingData::.ctor()
extern "C" void CallContextRemotingData__ctor_m6234 (CallContextRemotingData_t1092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
