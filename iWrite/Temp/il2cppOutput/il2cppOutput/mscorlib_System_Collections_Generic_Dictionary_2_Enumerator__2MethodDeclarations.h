﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct Enumerator_t1485;
// System.Object
struct Object_t;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t54;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct Dictionary_2_t55;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1MethodDeclarations.h"
#define Enumerator__ctor_m8716(__this, ___dictionary, method) (( void (*) (Enumerator_t1485 *, Dictionary_2_t55 *, const MethodInfo*))Enumerator__ctor_m8612_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m8717(__this, method) (( Object_t * (*) (Enumerator_t1485 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8613_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8718(__this, method) (( DictionaryEntry_t560  (*) (Enumerator_t1485 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8614_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8719(__this, method) (( Object_t * (*) (Enumerator_t1485 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8615_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8720(__this, method) (( Object_t * (*) (Enumerator_t1485 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8616_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::MoveNext()
#define Enumerator_MoveNext_m8721(__this, method) (( bool (*) (Enumerator_t1485 *, const MethodInfo*))Enumerator_MoveNext_m8617_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Current()
#define Enumerator_get_Current_m8722(__this, method) (( KeyValuePair_2_t1482  (*) (Enumerator_t1485 *, const MethodInfo*))Enumerator_get_Current_m8618_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m8723(__this, method) (( int32_t (*) (Enumerator_t1485 *, const MethodInfo*))Enumerator_get_CurrentKey_m8619_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m8724(__this, method) (( LayoutCache_t54 * (*) (Enumerator_t1485 *, const MethodInfo*))Enumerator_get_CurrentValue_m8620_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyState()
#define Enumerator_VerifyState_m8725(__this, method) (( void (*) (Enumerator_t1485 *, const MethodInfo*))Enumerator_VerifyState_m8621_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m8726(__this, method) (( void (*) (Enumerator_t1485 *, const MethodInfo*))Enumerator_VerifyCurrent_m8622_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::Dispose()
#define Enumerator_Dispose_m8727(__this, method) (( void (*) (Enumerator_t1485 *, const MethodInfo*))Enumerator_Dispose_m8623_gshared)(__this, method)
