﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// UnityEngine.AudioClip/PCMReaderCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback.h"
// Metadata Definition UnityEngine.AudioClip/PCMReaderCallback
extern TypeInfo PCMReaderCallback_t126_il2cpp_TypeInfo;
// UnityEngine.AudioClip/PCMReaderCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbackMethodDeclarations.h"
static const EncodedMethodIndex PCMReaderCallback_t126_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	273,
	274,
	275,
};
extern const Il2CppType ICloneable_t2056_0_0_0;
extern const Il2CppType ISerializable_t1426_0_0_0;
static Il2CppInterfaceOffsetPair PCMReaderCallback_t126_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PCMReaderCallback_t126_0_0_0;
extern const Il2CppType PCMReaderCallback_t126_1_0_0;
extern const Il2CppType MulticastDelegate_t47_0_0_0;
extern TypeInfo AudioClip_t128_il2cpp_TypeInfo;
extern const Il2CppType AudioClip_t128_0_0_0;
struct PCMReaderCallback_t126;
const Il2CppTypeDefinitionMetadata PCMReaderCallback_t126_DefinitionMetadata = 
{
	&AudioClip_t128_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PCMReaderCallback_t126_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, PCMReaderCallback_t126_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 663/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PCMReaderCallback_t126_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PCMReaderCallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PCMReaderCallback_t126_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PCMReaderCallback_t126_0_0_0/* byval_arg */
	, &PCMReaderCallback_t126_1_0_0/* this_arg */
	, &PCMReaderCallback_t126_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PCMReaderCallback_t126/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PCMReaderCallback_t126)/* instance_size */
	, sizeof (PCMReaderCallback_t126)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.AudioClip/PCMSetPositionCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallback.h"
// Metadata Definition UnityEngine.AudioClip/PCMSetPositionCallback
extern TypeInfo PCMSetPositionCallback_t127_il2cpp_TypeInfo;
// UnityEngine.AudioClip/PCMSetPositionCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallbackMethodDeclarations.h"
static const EncodedMethodIndex PCMSetPositionCallback_t127_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	276,
	277,
	278,
};
static Il2CppInterfaceOffsetPair PCMSetPositionCallback_t127_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PCMSetPositionCallback_t127_0_0_0;
extern const Il2CppType PCMSetPositionCallback_t127_1_0_0;
struct PCMSetPositionCallback_t127;
const Il2CppTypeDefinitionMetadata PCMSetPositionCallback_t127_DefinitionMetadata = 
{
	&AudioClip_t128_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PCMSetPositionCallback_t127_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, PCMSetPositionCallback_t127_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 667/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PCMSetPositionCallback_t127_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PCMSetPositionCallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PCMSetPositionCallback_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PCMSetPositionCallback_t127_0_0_0/* byval_arg */
	, &PCMSetPositionCallback_t127_1_0_0/* this_arg */
	, &PCMSetPositionCallback_t127_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PCMSetPositionCallback_t127/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PCMSetPositionCallback_t127)/* instance_size */
	, sizeof (PCMSetPositionCallback_t127)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"
// Metadata Definition UnityEngine.WebCamDevice
extern TypeInfo WebCamDevice_t129_il2cpp_TypeInfo;
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDeviceMethodDeclarations.h"
static const EncodedMethodIndex WebCamDevice_t129_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WebCamDevice_t129_0_0_0;
extern const Il2CppType WebCamDevice_t129_1_0_0;
extern const Il2CppType ValueType_t757_0_0_0;
const Il2CppTypeDefinitionMetadata WebCamDevice_t129_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, WebCamDevice_t129_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 686/* fieldStart */
	, 671/* methodStart */
	, -1/* eventStart */
	, 138/* propertyStart */

};
TypeInfo WebCamDevice_t129_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamDevice"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WebCamDevice_t129_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WebCamDevice_t129_0_0_0/* byval_arg */
	, &WebCamDevice_t129_1_0_0/* this_arg */
	, &WebCamDevice_t129_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)WebCamDevice_t129_marshal/* marshal_to_native_func */
	, (methodPointerType)WebCamDevice_t129_marshal_back/* marshal_from_native_func */
	, (methodPointerType)WebCamDevice_t129_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (WebCamDevice_t129)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (WebCamDevice_t129)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(WebCamDevice_t129_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AnimationEventSource
#include "UnityEngine_UnityEngine_AnimationEventSource.h"
// Metadata Definition UnityEngine.AnimationEventSource
extern TypeInfo AnimationEventSource_t130_il2cpp_TypeInfo;
// UnityEngine.AnimationEventSource
#include "UnityEngine_UnityEngine_AnimationEventSourceMethodDeclarations.h"
static const EncodedMethodIndex AnimationEventSource_t130_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
extern const Il2CppType IFormattable_t1406_0_0_0;
extern const Il2CppType IConvertible_t1409_0_0_0;
extern const Il2CppType IComparable_t1408_0_0_0;
static Il2CppInterfaceOffsetPair AnimationEventSource_t130_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimationEventSource_t130_0_0_0;
extern const Il2CppType AnimationEventSource_t130_1_0_0;
extern const Il2CppType Enum_t305_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t320_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata AnimationEventSource_t130_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AnimationEventSource_t130_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, AnimationEventSource_t130_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 688/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AnimationEventSource_t130_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimationEventSource"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimationEventSource_t130_0_0_0/* byval_arg */
	, &AnimationEventSource_t130_1_0_0/* this_arg */
	, &AnimationEventSource_t130_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimationEventSource_t130)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AnimationEventSource_t130)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.AnimationEvent
#include "UnityEngine_UnityEngine_AnimationEvent.h"
// Metadata Definition UnityEngine.AnimationEvent
extern TypeInfo AnimationEvent_t132_il2cpp_TypeInfo;
// UnityEngine.AnimationEvent
#include "UnityEngine_UnityEngine_AnimationEventMethodDeclarations.h"
static const EncodedMethodIndex AnimationEvent_t132_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimationEvent_t132_0_0_0;
extern const Il2CppType AnimationEvent_t132_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct AnimationEvent_t132;
const Il2CppTypeDefinitionMetadata AnimationEvent_t132_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AnimationEvent_t132_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 692/* fieldStart */
	, 673/* methodStart */
	, -1/* eventStart */
	, 140/* propertyStart */

};
TypeInfo AnimationEvent_t132_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimationEvent"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AnimationEvent_t132_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimationEvent_t132_0_0_0/* byval_arg */
	, &AnimationEvent_t132_1_0_0/* this_arg */
	, &AnimationEvent_t132_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimationEvent_t132)/* instance_size */
	, sizeof (AnimationEvent_t132)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 23/* method_count */
	, 13/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
// Metadata Definition UnityEngine.Keyframe
extern TypeInfo Keyframe_t135_il2cpp_TypeInfo;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_KeyframeMethodDeclarations.h"
static const EncodedMethodIndex Keyframe_t135_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Keyframe_t135_0_0_0;
extern const Il2CppType Keyframe_t135_1_0_0;
const Il2CppTypeDefinitionMetadata Keyframe_t135_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Keyframe_t135_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 703/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Keyframe_t135_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Keyframe"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Keyframe_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Keyframe_t135_0_0_0/* byval_arg */
	, &Keyframe_t135_1_0_0/* this_arg */
	, &Keyframe_t135_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Keyframe_t135)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Keyframe_t135)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Keyframe_t135 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurve.h"
// Metadata Definition UnityEngine.AnimationCurve
extern TypeInfo AnimationCurve_t136_il2cpp_TypeInfo;
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"
static const EncodedMethodIndex AnimationCurve_t136_VTable[4] = 
{
	120,
	279,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimationCurve_t136_0_0_0;
extern const Il2CppType AnimationCurve_t136_1_0_0;
struct AnimationCurve_t136;
const Il2CppTypeDefinitionMetadata AnimationCurve_t136_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AnimationCurve_t136_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 707/* fieldStart */
	, 696/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AnimationCurve_t136_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimationCurve"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AnimationCurve_t136_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 207/* custom_attributes_cache */
	, &AnimationCurve_t136_0_0_0/* byval_arg */
	, &AnimationCurve_t136_1_0_0/* this_arg */
	, &AnimationCurve_t136_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)AnimationCurve_t136_marshal/* marshal_to_native_func */
	, (methodPointerType)AnimationCurve_t136_marshal_back/* marshal_from_native_func */
	, (methodPointerType)AnimationCurve_t136_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (AnimationCurve_t136)/* instance_size */
	, sizeof (AnimationCurve_t136)/* actualSize */
	, 0/* element_size */
	, sizeof(AnimationCurve_t136_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AnimationState
#include "UnityEngine_UnityEngine_AnimationState.h"
// Metadata Definition UnityEngine.AnimationState
extern TypeInfo AnimationState_t131_il2cpp_TypeInfo;
// UnityEngine.AnimationState
#include "UnityEngine_UnityEngine_AnimationStateMethodDeclarations.h"
static const EncodedMethodIndex AnimationState_t131_VTable[4] = 
{
	280,
	125,
	281,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimationState_t131_0_0_0;
extern const Il2CppType AnimationState_t131_1_0_0;
extern const Il2CppType TrackedReference_t137_0_0_0;
struct AnimationState_t131;
const Il2CppTypeDefinitionMetadata AnimationState_t131_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TrackedReference_t137_0_0_0/* parent */
	, AnimationState_t131_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AnimationState_t131_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimationState"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AnimationState_t131_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimationState_t131_0_0_0/* byval_arg */
	, &AnimationState_t131_1_0_0/* this_arg */
	, &AnimationState_t131_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimationState_t131)/* instance_size */
	, sizeof (AnimationState_t131)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
// Metadata Definition UnityEngine.AnimatorClipInfo
extern TypeInfo AnimatorClipInfo_t134_il2cpp_TypeInfo;
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfoMethodDeclarations.h"
static const EncodedMethodIndex AnimatorClipInfo_t134_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimatorClipInfo_t134_0_0_0;
extern const Il2CppType AnimatorClipInfo_t134_1_0_0;
const Il2CppTypeDefinitionMetadata AnimatorClipInfo_t134_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, AnimatorClipInfo_t134_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 708/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AnimatorClipInfo_t134_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimatorClipInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AnimatorClipInfo_t134_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimatorClipInfo_t134_0_0_0/* byval_arg */
	, &AnimatorClipInfo_t134_1_0_0/* this_arg */
	, &AnimatorClipInfo_t134_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimatorClipInfo_t134)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AnimatorClipInfo_t134)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(AnimatorClipInfo_t134 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
// Metadata Definition UnityEngine.AnimatorStateInfo
extern TypeInfo AnimatorStateInfo_t133_il2cpp_TypeInfo;
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfoMethodDeclarations.h"
static const EncodedMethodIndex AnimatorStateInfo_t133_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimatorStateInfo_t133_0_0_0;
extern const Il2CppType AnimatorStateInfo_t133_1_0_0;
const Il2CppTypeDefinitionMetadata AnimatorStateInfo_t133_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, AnimatorStateInfo_t133_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 710/* fieldStart */
	, 701/* methodStart */
	, -1/* eventStart */
	, 153/* propertyStart */

};
TypeInfo AnimatorStateInfo_t133_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimatorStateInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AnimatorStateInfo_t133_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimatorStateInfo_t133_0_0_0/* byval_arg */
	, &AnimatorStateInfo_t133_1_0_0/* this_arg */
	, &AnimatorStateInfo_t133_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimatorStateInfo_t133)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AnimatorStateInfo_t133)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(AnimatorStateInfo_t133 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AnimatorTransitionInfo
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo.h"
// Metadata Definition UnityEngine.AnimatorTransitionInfo
extern TypeInfo AnimatorTransitionInfo_t138_il2cpp_TypeInfo;
// UnityEngine.AnimatorTransitionInfo
#include "UnityEngine_UnityEngine_AnimatorTransitionInfoMethodDeclarations.h"
static const EncodedMethodIndex AnimatorTransitionInfo_t138_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimatorTransitionInfo_t138_0_0_0;
extern const Il2CppType AnimatorTransitionInfo_t138_1_0_0;
const Il2CppTypeDefinitionMetadata AnimatorTransitionInfo_t138_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, AnimatorTransitionInfo_t138_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 717/* fieldStart */
	, 710/* methodStart */
	, -1/* eventStart */
	, 160/* propertyStart */

};
TypeInfo AnimatorTransitionInfo_t138_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimatorTransitionInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AnimatorTransitionInfo_t138_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimatorTransitionInfo_t138_0_0_0/* byval_arg */
	, &AnimatorTransitionInfo_t138_1_0_0/* this_arg */
	, &AnimatorTransitionInfo_t138_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)AnimatorTransitionInfo_t138_marshal/* marshal_to_native_func */
	, (methodPointerType)AnimatorTransitionInfo_t138_marshal_back/* marshal_from_native_func */
	, (methodPointerType)AnimatorTransitionInfo_t138_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (AnimatorTransitionInfo_t138)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AnimatorTransitionInfo_t138)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(AnimatorTransitionInfo_t138_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"
// Metadata Definition UnityEngine.Animator
extern TypeInfo Animator_t139_il2cpp_TypeInfo;
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
static const EncodedMethodIndex Animator_t139_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Animator_t139_0_0_0;
extern const Il2CppType Animator_t139_1_0_0;
extern const Il2CppType Behaviour_t34_0_0_0;
struct Animator_t139;
const Il2CppTypeDefinitionMetadata Animator_t139_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t34_0_0_0/* parent */
	, Animator_t139_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 719/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Animator_t139_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Animator"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Animator_t139_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Animator_t139_0_0_0/* byval_arg */
	, &Animator_t139_1_0_0/* this_arg */
	, &Animator_t139_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Animator_t139)/* instance_size */
	, sizeof (Animator_t139)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SkeletonBone
#include "UnityEngine_UnityEngine_SkeletonBone.h"
// Metadata Definition UnityEngine.SkeletonBone
extern TypeInfo SkeletonBone_t140_il2cpp_TypeInfo;
// UnityEngine.SkeletonBone
#include "UnityEngine_UnityEngine_SkeletonBoneMethodDeclarations.h"
static const EncodedMethodIndex SkeletonBone_t140_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SkeletonBone_t140_0_0_0;
extern const Il2CppType SkeletonBone_t140_1_0_0;
const Il2CppTypeDefinitionMetadata SkeletonBone_t140_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, SkeletonBone_t140_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 723/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SkeletonBone_t140_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SkeletonBone"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SkeletonBone_t140_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SkeletonBone_t140_0_0_0/* byval_arg */
	, &SkeletonBone_t140_1_0_0/* this_arg */
	, &SkeletonBone_t140_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)SkeletonBone_t140_marshal/* marshal_to_native_func */
	, (methodPointerType)SkeletonBone_t140_marshal_back/* marshal_from_native_func */
	, (methodPointerType)SkeletonBone_t140_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (SkeletonBone_t140)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SkeletonBone_t140)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(SkeletonBone_t140_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.HumanLimit
#include "UnityEngine_UnityEngine_HumanLimit.h"
// Metadata Definition UnityEngine.HumanLimit
extern TypeInfo HumanLimit_t141_il2cpp_TypeInfo;
// UnityEngine.HumanLimit
#include "UnityEngine_UnityEngine_HumanLimitMethodDeclarations.h"
static const EncodedMethodIndex HumanLimit_t141_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HumanLimit_t141_0_0_0;
extern const Il2CppType HumanLimit_t141_1_0_0;
const Il2CppTypeDefinitionMetadata HumanLimit_t141_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, HumanLimit_t141_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 728/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HumanLimit_t141_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HumanLimit"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &HumanLimit_t141_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HumanLimit_t141_0_0_0/* byval_arg */
	, &HumanLimit_t141_1_0_0/* this_arg */
	, &HumanLimit_t141_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HumanLimit_t141)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HumanLimit_t141)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(HumanLimit_t141 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.HumanBone
#include "UnityEngine_UnityEngine_HumanBone.h"
// Metadata Definition UnityEngine.HumanBone
extern TypeInfo HumanBone_t142_il2cpp_TypeInfo;
// UnityEngine.HumanBone
#include "UnityEngine_UnityEngine_HumanBoneMethodDeclarations.h"
static const EncodedMethodIndex HumanBone_t142_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HumanBone_t142_0_0_0;
extern const Il2CppType HumanBone_t142_1_0_0;
const Il2CppTypeDefinitionMetadata HumanBone_t142_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, HumanBone_t142_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 733/* fieldStart */
	, 720/* methodStart */
	, -1/* eventStart */
	, 167/* propertyStart */

};
TypeInfo HumanBone_t142_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HumanBone"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &HumanBone_t142_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HumanBone_t142_0_0_0/* byval_arg */
	, &HumanBone_t142_1_0_0/* this_arg */
	, &HumanBone_t142_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)HumanBone_t142_marshal/* marshal_to_native_func */
	, (methodPointerType)HumanBone_t142_marshal_back/* marshal_from_native_func */
	, (methodPointerType)HumanBone_t142_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (HumanBone_t142)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HumanBone_t142)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(HumanBone_t142_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
// Metadata Definition UnityEngine.TextAnchor
extern TypeInfo TextAnchor_t143_il2cpp_TypeInfo;
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchorMethodDeclarations.h"
static const EncodedMethodIndex TextAnchor_t143_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair TextAnchor_t143_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextAnchor_t143_0_0_0;
extern const Il2CppType TextAnchor_t143_1_0_0;
const Il2CppTypeDefinitionMetadata TextAnchor_t143_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextAnchor_t143_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, TextAnchor_t143_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 736/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextAnchor_t143_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextAnchor"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextAnchor_t143_0_0_0/* byval_arg */
	, &TextAnchor_t143_1_0_0/* this_arg */
	, &TextAnchor_t143_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextAnchor_t143)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextAnchor_t143)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
// Metadata Definition UnityEngine.HorizontalWrapMode
extern TypeInfo HorizontalWrapMode_t144_il2cpp_TypeInfo;
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapModeMethodDeclarations.h"
static const EncodedMethodIndex HorizontalWrapMode_t144_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair HorizontalWrapMode_t144_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HorizontalWrapMode_t144_0_0_0;
extern const Il2CppType HorizontalWrapMode_t144_1_0_0;
const Il2CppTypeDefinitionMetadata HorizontalWrapMode_t144_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalWrapMode_t144_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, HorizontalWrapMode_t144_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 746/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HorizontalWrapMode_t144_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalWrapMode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HorizontalWrapMode_t144_0_0_0/* byval_arg */
	, &HorizontalWrapMode_t144_1_0_0/* this_arg */
	, &HorizontalWrapMode_t144_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalWrapMode_t144)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HorizontalWrapMode_t144)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
// Metadata Definition UnityEngine.VerticalWrapMode
extern TypeInfo VerticalWrapMode_t145_il2cpp_TypeInfo;
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapModeMethodDeclarations.h"
static const EncodedMethodIndex VerticalWrapMode_t145_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair VerticalWrapMode_t145_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType VerticalWrapMode_t145_0_0_0;
extern const Il2CppType VerticalWrapMode_t145_1_0_0;
const Il2CppTypeDefinitionMetadata VerticalWrapMode_t145_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VerticalWrapMode_t145_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, VerticalWrapMode_t145_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 749/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo VerticalWrapMode_t145_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "VerticalWrapMode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VerticalWrapMode_t145_0_0_0/* byval_arg */
	, &VerticalWrapMode_t145_1_0_0/* this_arg */
	, &VerticalWrapMode_t145_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VerticalWrapMode_t145)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (VerticalWrapMode_t145)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfo.h"
// Metadata Definition UnityEngine.CharacterInfo
extern TypeInfo CharacterInfo_t146_il2cpp_TypeInfo;
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfoMethodDeclarations.h"
static const EncodedMethodIndex CharacterInfo_t146_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CharacterInfo_t146_0_0_0;
extern const Il2CppType CharacterInfo_t146_1_0_0;
const Il2CppTypeDefinitionMetadata CharacterInfo_t146_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, CharacterInfo_t146_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 752/* fieldStart */
	, 724/* methodStart */
	, -1/* eventStart */
	, 169/* propertyStart */

};
TypeInfo CharacterInfo_t146_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharacterInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &CharacterInfo_t146_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CharacterInfo_t146_0_0_0/* byval_arg */
	, &CharacterInfo_t146_1_0_0/* this_arg */
	, &CharacterInfo_t146_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)CharacterInfo_t146_marshal/* marshal_to_native_func */
	, (methodPointerType)CharacterInfo_t146_marshal_back/* marshal_from_native_func */
	, (methodPointerType)CharacterInfo_t146_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (CharacterInfo_t146)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CharacterInfo_t146)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(CharacterInfo_t146_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 16/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
// Metadata Definition UnityEngine.Font
extern TypeInfo Font_t69_il2cpp_TypeInfo;
// UnityEngine.Font
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
extern const Il2CppType FontTextureRebuildCallback_t147_0_0_0;
static const Il2CppType* Font_t69_il2cpp_TypeInfo__nestedTypes[1] =
{
	&FontTextureRebuildCallback_t147_0_0_0,
};
static const EncodedMethodIndex Font_t69_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Font_t69_0_0_0;
extern const Il2CppType Font_t69_1_0_0;
extern const Il2CppType Object_t5_0_0_0;
struct Font_t69;
const Il2CppTypeDefinitionMetadata Font_t69_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Font_t69_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t5_0_0_0/* parent */
	, Font_t69_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 760/* fieldStart */
	, 740/* methodStart */
	, -1/* eventStart */
	, 185/* propertyStart */

};
TypeInfo Font_t69_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Font"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Font_t69_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Font_t69_0_0_0/* byval_arg */
	, &Font_t69_1_0_0/* this_arg */
	, &Font_t69_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Font_t69)/* instance_size */
	, sizeof (Font_t69)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Font_t69_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallback.h"
// Metadata Definition UnityEngine.Font/FontTextureRebuildCallback
extern TypeInfo FontTextureRebuildCallback_t147_il2cpp_TypeInfo;
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallbackMethodDeclarations.h"
static const EncodedMethodIndex FontTextureRebuildCallback_t147_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	282,
	283,
	284,
};
static Il2CppInterfaceOffsetPair FontTextureRebuildCallback_t147_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FontTextureRebuildCallback_t147_1_0_0;
struct FontTextureRebuildCallback_t147;
const Il2CppTypeDefinitionMetadata FontTextureRebuildCallback_t147_DefinitionMetadata = 
{
	&Font_t69_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FontTextureRebuildCallback_t147_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, FontTextureRebuildCallback_t147_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 742/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FontTextureRebuildCallback_t147_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FontTextureRebuildCallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &FontTextureRebuildCallback_t147_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 218/* custom_attributes_cache */
	, &FontTextureRebuildCallback_t147_0_0_0/* byval_arg */
	, &FontTextureRebuildCallback_t147_1_0_0/* this_arg */
	, &FontTextureRebuildCallback_t147_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_FontTextureRebuildCallback_t147/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FontTextureRebuildCallback_t147)/* instance_size */
	, sizeof (FontTextureRebuildCallback_t147)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
// Metadata Definition UnityEngine.UICharInfo
extern TypeInfo UICharInfo_t149_il2cpp_TypeInfo;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfoMethodDeclarations.h"
static const EncodedMethodIndex UICharInfo_t149_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UICharInfo_t149_0_0_0;
extern const Il2CppType UICharInfo_t149_1_0_0;
const Il2CppTypeDefinitionMetadata UICharInfo_t149_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, UICharInfo_t149_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 762/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UICharInfo_t149_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UICharInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &UICharInfo_t149_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UICharInfo_t149_0_0_0/* byval_arg */
	, &UICharInfo_t149_1_0_0/* this_arg */
	, &UICharInfo_t149_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UICharInfo_t149)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UICharInfo_t149)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UICharInfo_t149 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
// Metadata Definition UnityEngine.UILineInfo
extern TypeInfo UILineInfo_t150_il2cpp_TypeInfo;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfoMethodDeclarations.h"
static const EncodedMethodIndex UILineInfo_t150_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UILineInfo_t150_0_0_0;
extern const Il2CppType UILineInfo_t150_1_0_0;
const Il2CppTypeDefinitionMetadata UILineInfo_t150_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, UILineInfo_t150_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 764/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UILineInfo_t150_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UILineInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &UILineInfo_t150_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UILineInfo_t150_0_0_0/* byval_arg */
	, &UILineInfo_t150_1_0_0/* this_arg */
	, &UILineInfo_t150_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UILineInfo_t150)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UILineInfo_t150)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UILineInfo_t150 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGenerator.h"
// Metadata Definition UnityEngine.TextGenerator
extern TypeInfo TextGenerator_t154_il2cpp_TypeInfo;
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"
static const EncodedMethodIndex TextGenerator_t154_VTable[5] = 
{
	120,
	285,
	122,
	123,
	286,
};
extern const Il2CppType IDisposable_t319_0_0_0;
static const Il2CppType* TextGenerator_t154_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
};
static Il2CppInterfaceOffsetPair TextGenerator_t154_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextGenerator_t154_0_0_0;
extern const Il2CppType TextGenerator_t154_1_0_0;
struct TextGenerator_t154;
const Il2CppTypeDefinitionMetadata TextGenerator_t154_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TextGenerator_t154_InterfacesTypeInfos/* implementedInterfaces */
	, TextGenerator_t154_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextGenerator_t154_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 766/* fieldStart */
	, 746/* methodStart */
	, -1/* eventStart */
	, 186/* propertyStart */

};
TypeInfo TextGenerator_t154_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextGenerator"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TextGenerator_t154_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextGenerator_t154_0_0_0/* byval_arg */
	, &TextGenerator_t154_1_0_0/* this_arg */
	, &TextGenerator_t154_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextGenerator_t154)/* instance_size */
	, sizeof (TextGenerator_t154)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 34/* method_count */
	, 9/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
// Metadata Definition UnityEngine.Canvas
extern TypeInfo Canvas_t157_il2cpp_TypeInfo;
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
extern const Il2CppType WillRenderCanvases_t156_0_0_0;
static const Il2CppType* Canvas_t157_il2cpp_TypeInfo__nestedTypes[1] =
{
	&WillRenderCanvases_t156_0_0_0,
};
static const EncodedMethodIndex Canvas_t157_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Canvas_t157_0_0_0;
extern const Il2CppType Canvas_t157_1_0_0;
struct Canvas_t157;
const Il2CppTypeDefinitionMetadata Canvas_t157_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Canvas_t157_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t34_0_0_0/* parent */
	, Canvas_t157_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 777/* fieldStart */
	, 780/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Canvas_t157_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Canvas"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Canvas_t157_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Canvas_t157_0_0_0/* byval_arg */
	, &Canvas_t157_1_0_0/* this_arg */
	, &Canvas_t157_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Canvas_t157)/* instance_size */
	, sizeof (Canvas_t157)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Canvas_t157_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases.h"
// Metadata Definition UnityEngine.Canvas/WillRenderCanvases
extern TypeInfo WillRenderCanvases_t156_il2cpp_TypeInfo;
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvasesMethodDeclarations.h"
static const EncodedMethodIndex WillRenderCanvases_t156_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	287,
	288,
	289,
};
static Il2CppInterfaceOffsetPair WillRenderCanvases_t156_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WillRenderCanvases_t156_1_0_0;
struct WillRenderCanvases_t156;
const Il2CppTypeDefinitionMetadata WillRenderCanvases_t156_DefinitionMetadata = 
{
	&Canvas_t157_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WillRenderCanvases_t156_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, WillRenderCanvases_t156_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 781/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WillRenderCanvases_t156_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WillRenderCanvases"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &WillRenderCanvases_t156_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WillRenderCanvases_t156_0_0_0/* byval_arg */
	, &WillRenderCanvases_t156_1_0_0/* this_arg */
	, &WillRenderCanvases_t156_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_WillRenderCanvases_t156/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WillRenderCanvases_t156)/* instance_size */
	, sizeof (WillRenderCanvases_t156)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// Metadata Definition UnityEngine.UIVertex
extern TypeInfo UIVertex_t158_il2cpp_TypeInfo;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertexMethodDeclarations.h"
static const EncodedMethodIndex UIVertex_t158_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UIVertex_t158_0_0_0;
extern const Il2CppType UIVertex_t158_1_0_0;
const Il2CppTypeDefinitionMetadata UIVertex_t158_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, UIVertex_t158_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 778/* fieldStart */
	, 785/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UIVertex_t158_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UIVertex"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &UIVertex_t158_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UIVertex_t158_0_0_0/* byval_arg */
	, &UIVertex_t158_1_0_0/* this_arg */
	, &UIVertex_t158_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UIVertex_t158)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UIVertex_t158)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UIVertex_t158 )/* native_size */
	, sizeof(UIVertex_t158_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.Request
#include "UnityEngine_UnityEngine_Networking_Match_Request.h"
// Metadata Definition UnityEngine.Networking.Match.Request
extern TypeInfo Request_t159_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.Request
#include "UnityEngine_UnityEngine_Networking_Match_RequestMethodDeclarations.h"
static const EncodedMethodIndex Request_t159_VTable[4] = 
{
	120,
	125,
	122,
	290,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Request_t159_0_0_0;
extern const Il2CppType Request_t159_1_0_0;
struct Request_t159;
const Il2CppTypeDefinitionMetadata Request_t159_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Request_t159_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 787/* fieldStart */
	, 786/* methodStart */
	, -1/* eventStart */
	, 195/* propertyStart */

};
TypeInfo Request_t159_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Request"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &Request_t159_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Request_t159_0_0_0/* byval_arg */
	, &Request_t159_1_0_0/* this_arg */
	, &Request_t159_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Request_t159)/* instance_size */
	, sizeof (Request_t159)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.ResponseBase
#include "UnityEngine_UnityEngine_Networking_Match_ResponseBase.h"
// Metadata Definition UnityEngine.Networking.Match.ResponseBase
extern TypeInfo ResponseBase_t160_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.ResponseBase
#include "UnityEngine_UnityEngine_Networking_Match_ResponseBaseMethodDeclarations.h"
extern const Il2CppType List_1_t2201_0_0_0;
extern const Il2CppType ResponseBase_ParseJSONList_m12758_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition ResponseBase_ParseJSONList_m12758_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 2388 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2534 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2535 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 2389 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2536 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex ResponseBase_t160_VTable[5] = 
{
	120,
	125,
	122,
	123,
	0,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ResponseBase_t160_0_0_0;
extern const Il2CppType ResponseBase_t160_1_0_0;
struct ResponseBase_t160;
const Il2CppTypeDefinitionMetadata ResponseBase_t160_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ResponseBase_t160_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 791/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ResponseBase_t160_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResponseBase"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &ResponseBase_t160_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ResponseBase_t160_0_0_0/* byval_arg */
	, &ResponseBase_t160_1_0_0/* this_arg */
	, &ResponseBase_t160_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResponseBase_t160)/* instance_size */
	, sizeof (ResponseBase_t160)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Networking.Match.IResponse
extern TypeInfo IResponse_t2038_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IResponse_t2038_0_0_0;
extern const Il2CppType IResponse_t2038_1_0_0;
struct IResponse_t2038;
const Il2CppTypeDefinitionMetadata IResponse_t2038_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IResponse_t2038_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &IResponse_t2038_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IResponse_t2038_0_0_0/* byval_arg */
	, &IResponse_t2038_1_0_0/* this_arg */
	, &IResponse_t2038_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.Response
#include "UnityEngine_UnityEngine_Networking_Match_Response.h"
// Metadata Definition UnityEngine.Networking.Match.Response
extern TypeInfo Response_t161_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.Response
#include "UnityEngine_UnityEngine_Networking_Match_ResponseMethodDeclarations.h"
static const EncodedMethodIndex Response_t161_VTable[5] = 
{
	120,
	125,
	122,
	291,
	292,
};
static const Il2CppType* Response_t161_InterfacesTypeInfos[] = 
{
	&IResponse_t2038_0_0_0,
};
static Il2CppInterfaceOffsetPair Response_t161_InterfacesOffsets[] = 
{
	{ &IResponse_t2038_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Response_t161_0_0_0;
extern const Il2CppType Response_t161_1_0_0;
struct Response_t161;
const Il2CppTypeDefinitionMetadata Response_t161_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Response_t161_InterfacesTypeInfos/* implementedInterfaces */
	, Response_t161_InterfacesOffsets/* interfaceOffsets */
	, &ResponseBase_t160_0_0_0/* parent */
	, Response_t161_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 791/* fieldStart */
	, 799/* methodStart */
	, -1/* eventStart */
	, 198/* propertyStart */

};
TypeInfo Response_t161_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Response"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &Response_t161_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Response_t161_0_0_0/* byval_arg */
	, &Response_t161_1_0_0/* this_arg */
	, &Response_t161_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Response_t161)/* instance_size */
	, sizeof (Response_t161)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Match.BasicResponse
#include "UnityEngine_UnityEngine_Networking_Match_BasicResponse.h"
// Metadata Definition UnityEngine.Networking.Match.BasicResponse
extern TypeInfo BasicResponse_t162_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.BasicResponse
#include "UnityEngine_UnityEngine_Networking_Match_BasicResponseMethodDeclarations.h"
static const EncodedMethodIndex BasicResponse_t162_VTable[5] = 
{
	120,
	125,
	122,
	291,
	292,
};
static Il2CppInterfaceOffsetPair BasicResponse_t162_InterfacesOffsets[] = 
{
	{ &IResponse_t2038_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType BasicResponse_t162_0_0_0;
extern const Il2CppType BasicResponse_t162_1_0_0;
struct BasicResponse_t162;
const Il2CppTypeDefinitionMetadata BasicResponse_t162_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BasicResponse_t162_InterfacesOffsets/* interfaceOffsets */
	, &Response_t161_0_0_0/* parent */
	, BasicResponse_t162_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 806/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BasicResponse_t162_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BasicResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &BasicResponse_t162_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BasicResponse_t162_0_0_0/* byval_arg */
	, &BasicResponse_t162_1_0_0/* this_arg */
	, &BasicResponse_t162_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BasicResponse_t162)/* instance_size */
	, sizeof (BasicResponse_t162)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Match.CreateMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchRequest.h"
// Metadata Definition UnityEngine.Networking.Match.CreateMatchRequest
extern TypeInfo CreateMatchRequest_t164_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.CreateMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchRequestMethodDeclarations.h"
static const EncodedMethodIndex CreateMatchRequest_t164_VTable[4] = 
{
	120,
	125,
	122,
	293,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CreateMatchRequest_t164_0_0_0;
extern const Il2CppType CreateMatchRequest_t164_1_0_0;
struct CreateMatchRequest_t164;
const Il2CppTypeDefinitionMetadata CreateMatchRequest_t164_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t159_0_0_0/* parent */
	, CreateMatchRequest_t164_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 793/* fieldStart */
	, 807/* methodStart */
	, -1/* eventStart */
	, 200/* propertyStart */

};
TypeInfo CreateMatchRequest_t164_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CreateMatchRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &CreateMatchRequest_t164_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CreateMatchRequest_t164_0_0_0/* byval_arg */
	, &CreateMatchRequest_t164_1_0_0/* this_arg */
	, &CreateMatchRequest_t164_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CreateMatchRequest_t164)/* instance_size */
	, sizeof (CreateMatchRequest_t164)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.CreateMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchResponse.h"
// Metadata Definition UnityEngine.Networking.Match.CreateMatchResponse
extern TypeInfo CreateMatchResponse_t165_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.CreateMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchResponseMethodDeclarations.h"
static const EncodedMethodIndex CreateMatchResponse_t165_VTable[5] = 
{
	120,
	125,
	122,
	294,
	295,
};
static Il2CppInterfaceOffsetPair CreateMatchResponse_t165_InterfacesOffsets[] = 
{
	{ &IResponse_t2038_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CreateMatchResponse_t165_0_0_0;
extern const Il2CppType CreateMatchResponse_t165_1_0_0;
struct CreateMatchResponse_t165;
const Il2CppTypeDefinitionMetadata CreateMatchResponse_t165_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CreateMatchResponse_t165_InterfacesOffsets/* interfaceOffsets */
	, &BasicResponse_t162_0_0_0/* parent */
	, CreateMatchResponse_t165_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 798/* fieldStart */
	, 818/* methodStart */
	, -1/* eventStart */
	, 205/* propertyStart */

};
TypeInfo CreateMatchResponse_t165_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CreateMatchResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &CreateMatchResponse_t165_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CreateMatchResponse_t165_0_0_0/* byval_arg */
	, &CreateMatchResponse_t165_1_0_0/* this_arg */
	, &CreateMatchResponse_t165_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CreateMatchResponse_t165)/* instance_size */
	, sizeof (CreateMatchResponse_t165)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Match.JoinMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchRequest.h"
// Metadata Definition UnityEngine.Networking.Match.JoinMatchRequest
extern TypeInfo JoinMatchRequest_t166_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.JoinMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchRequestMethodDeclarations.h"
static const EncodedMethodIndex JoinMatchRequest_t166_VTable[4] = 
{
	120,
	125,
	122,
	296,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType JoinMatchRequest_t166_0_0_0;
extern const Il2CppType JoinMatchRequest_t166_1_0_0;
struct JoinMatchRequest_t166;
const Il2CppTypeDefinitionMetadata JoinMatchRequest_t166_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t159_0_0_0/* parent */
	, JoinMatchRequest_t166_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 804/* fieldStart */
	, 833/* methodStart */
	, -1/* eventStart */
	, 211/* propertyStart */

};
TypeInfo JoinMatchRequest_t166_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "JoinMatchRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &JoinMatchRequest_t166_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &JoinMatchRequest_t166_0_0_0/* byval_arg */
	, &JoinMatchRequest_t166_1_0_0/* this_arg */
	, &JoinMatchRequest_t166_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (JoinMatchRequest_t166)/* instance_size */
	, sizeof (JoinMatchRequest_t166)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.JoinMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchResponse.h"
// Metadata Definition UnityEngine.Networking.Match.JoinMatchResponse
extern TypeInfo JoinMatchResponse_t167_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.JoinMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchResponseMethodDeclarations.h"
static const EncodedMethodIndex JoinMatchResponse_t167_VTable[5] = 
{
	120,
	125,
	122,
	297,
	298,
};
static Il2CppInterfaceOffsetPair JoinMatchResponse_t167_InterfacesOffsets[] = 
{
	{ &IResponse_t2038_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType JoinMatchResponse_t167_0_0_0;
extern const Il2CppType JoinMatchResponse_t167_1_0_0;
struct JoinMatchResponse_t167;
const Il2CppTypeDefinitionMetadata JoinMatchResponse_t167_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, JoinMatchResponse_t167_InterfacesOffsets/* interfaceOffsets */
	, &BasicResponse_t162_0_0_0/* parent */
	, JoinMatchResponse_t167_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 806/* fieldStart */
	, 839/* methodStart */
	, -1/* eventStart */
	, 213/* propertyStart */

};
TypeInfo JoinMatchResponse_t167_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "JoinMatchResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &JoinMatchResponse_t167_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &JoinMatchResponse_t167_0_0_0/* byval_arg */
	, &JoinMatchResponse_t167_1_0_0/* this_arg */
	, &JoinMatchResponse_t167_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (JoinMatchResponse_t167)/* instance_size */
	, sizeof (JoinMatchResponse_t167)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Match.DestroyMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_DestroyMatchRequest.h"
// Metadata Definition UnityEngine.Networking.Match.DestroyMatchRequest
extern TypeInfo DestroyMatchRequest_t168_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.DestroyMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_DestroyMatchRequestMethodDeclarations.h"
static const EncodedMethodIndex DestroyMatchRequest_t168_VTable[4] = 
{
	120,
	125,
	122,
	299,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DestroyMatchRequest_t168_0_0_0;
extern const Il2CppType DestroyMatchRequest_t168_1_0_0;
struct DestroyMatchRequest_t168;
const Il2CppTypeDefinitionMetadata DestroyMatchRequest_t168_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t159_0_0_0/* parent */
	, DestroyMatchRequest_t168_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 812/* fieldStart */
	, 854/* methodStart */
	, -1/* eventStart */
	, 219/* propertyStart */

};
TypeInfo DestroyMatchRequest_t168_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DestroyMatchRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &DestroyMatchRequest_t168_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DestroyMatchRequest_t168_0_0_0/* byval_arg */
	, &DestroyMatchRequest_t168_1_0_0/* this_arg */
	, &DestroyMatchRequest_t168_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DestroyMatchRequest_t168)/* instance_size */
	, sizeof (DestroyMatchRequest_t168)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.DropConnectionRequest
#include "UnityEngine_UnityEngine_Networking_Match_DropConnectionReque.h"
// Metadata Definition UnityEngine.Networking.Match.DropConnectionRequest
extern TypeInfo DropConnectionRequest_t169_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.DropConnectionRequest
#include "UnityEngine_UnityEngine_Networking_Match_DropConnectionRequeMethodDeclarations.h"
static const EncodedMethodIndex DropConnectionRequest_t169_VTable[4] = 
{
	120,
	125,
	122,
	300,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DropConnectionRequest_t169_0_0_0;
extern const Il2CppType DropConnectionRequest_t169_1_0_0;
struct DropConnectionRequest_t169;
const Il2CppTypeDefinitionMetadata DropConnectionRequest_t169_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t159_0_0_0/* parent */
	, DropConnectionRequest_t169_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 813/* fieldStart */
	, 858/* methodStart */
	, -1/* eventStart */
	, 220/* propertyStart */

};
TypeInfo DropConnectionRequest_t169_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DropConnectionRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &DropConnectionRequest_t169_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DropConnectionRequest_t169_0_0_0/* byval_arg */
	, &DropConnectionRequest_t169_1_0_0/* this_arg */
	, &DropConnectionRequest_t169_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DropConnectionRequest_t169)/* instance_size */
	, sizeof (DropConnectionRequest_t169)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.ListMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchRequest.h"
// Metadata Definition UnityEngine.Networking.Match.ListMatchRequest
extern TypeInfo ListMatchRequest_t170_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.ListMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchRequestMethodDeclarations.h"
static const EncodedMethodIndex ListMatchRequest_t170_VTable[4] = 
{
	120,
	125,
	122,
	301,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ListMatchRequest_t170_0_0_0;
extern const Il2CppType ListMatchRequest_t170_1_0_0;
struct ListMatchRequest_t170;
const Il2CppTypeDefinitionMetadata ListMatchRequest_t170_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t159_0_0_0/* parent */
	, ListMatchRequest_t170_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 815/* fieldStart */
	, 864/* methodStart */
	, -1/* eventStart */
	, 222/* propertyStart */

};
TypeInfo ListMatchRequest_t170_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ListMatchRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &ListMatchRequest_t170_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ListMatchRequest_t170_0_0_0/* byval_arg */
	, &ListMatchRequest_t170_1_0_0/* this_arg */
	, &ListMatchRequest_t170_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ListMatchRequest_t170)/* instance_size */
	, sizeof (ListMatchRequest_t170)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.MatchDirectConnectInfo
#include "UnityEngine_UnityEngine_Networking_Match_MatchDirectConnectI.h"
// Metadata Definition UnityEngine.Networking.Match.MatchDirectConnectInfo
extern TypeInfo MatchDirectConnectInfo_t171_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.MatchDirectConnectInfo
#include "UnityEngine_UnityEngine_Networking_Match_MatchDirectConnectIMethodDeclarations.h"
static const EncodedMethodIndex MatchDirectConnectInfo_t171_VTable[5] = 
{
	120,
	125,
	122,
	302,
	303,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MatchDirectConnectInfo_t171_0_0_0;
extern const Il2CppType MatchDirectConnectInfo_t171_1_0_0;
struct MatchDirectConnectInfo_t171;
const Il2CppTypeDefinitionMetadata MatchDirectConnectInfo_t171_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ResponseBase_t160_0_0_0/* parent */
	, MatchDirectConnectInfo_t171_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 821/* fieldStart */
	, 875/* methodStart */
	, -1/* eventStart */
	, 228/* propertyStart */

};
TypeInfo MatchDirectConnectInfo_t171_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchDirectConnectInfo"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &MatchDirectConnectInfo_t171_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchDirectConnectInfo_t171_0_0_0/* byval_arg */
	, &MatchDirectConnectInfo_t171_1_0_0/* this_arg */
	, &MatchDirectConnectInfo_t171_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchDirectConnectInfo_t171)/* instance_size */
	, sizeof (MatchDirectConnectInfo_t171)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.MatchDesc
#include "UnityEngine_UnityEngine_Networking_Match_MatchDesc.h"
// Metadata Definition UnityEngine.Networking.Match.MatchDesc
extern TypeInfo MatchDesc_t173_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.MatchDesc
#include "UnityEngine_UnityEngine_Networking_Match_MatchDescMethodDeclarations.h"
static const EncodedMethodIndex MatchDesc_t173_VTable[5] = 
{
	120,
	125,
	122,
	304,
	305,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MatchDesc_t173_0_0_0;
extern const Il2CppType MatchDesc_t173_1_0_0;
struct MatchDesc_t173;
const Il2CppTypeDefinitionMetadata MatchDesc_t173_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ResponseBase_t160_0_0_0/* parent */
	, MatchDesc_t173_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 824/* fieldStart */
	, 884/* methodStart */
	, -1/* eventStart */
	, 231/* propertyStart */

};
TypeInfo MatchDesc_t173_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchDesc"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &MatchDesc_t173_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchDesc_t173_0_0_0/* byval_arg */
	, &MatchDesc_t173_1_0_0/* this_arg */
	, &MatchDesc_t173_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchDesc_t173)/* instance_size */
	, sizeof (MatchDesc_t173)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 9/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.ListMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchResponse.h"
// Metadata Definition UnityEngine.Networking.Match.ListMatchResponse
extern TypeInfo ListMatchResponse_t175_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.ListMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchResponseMethodDeclarations.h"
static const EncodedMethodIndex ListMatchResponse_t175_VTable[5] = 
{
	120,
	125,
	122,
	306,
	307,
};
static Il2CppInterfaceOffsetPair ListMatchResponse_t175_InterfacesOffsets[] = 
{
	{ &IResponse_t2038_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ListMatchResponse_t175_0_0_0;
extern const Il2CppType ListMatchResponse_t175_1_0_0;
struct ListMatchResponse_t175;
const Il2CppTypeDefinitionMetadata ListMatchResponse_t175_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ListMatchResponse_t175_InterfacesOffsets/* interfaceOffsets */
	, &BasicResponse_t162_0_0_0/* parent */
	, ListMatchResponse_t175_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 833/* fieldStart */
	, 902/* methodStart */
	, -1/* eventStart */
	, 240/* propertyStart */

};
TypeInfo ListMatchResponse_t175_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ListMatchResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &ListMatchResponse_t175_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ListMatchResponse_t175_0_0_0/* byval_arg */
	, &ListMatchResponse_t175_1_0_0/* this_arg */
	, &ListMatchResponse_t175_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ListMatchResponse_t175)/* instance_size */
	, sizeof (ListMatchResponse_t175)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
// Metadata Definition UnityEngine.Networking.Types.AppID
extern TypeInfo AppID_t176_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppIDMethodDeclarations.h"
static const EncodedMethodIndex AppID_t176_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair AppID_t176_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AppID_t176_0_0_0;
extern const Il2CppType AppID_t176_1_0_0;
// System.UInt64
#include "mscorlib_System_UInt64.h"
extern TypeInfo UInt64_t348_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata AppID_t176_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AppID_t176_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, AppID_t176_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 834/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AppID_t176_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppID"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NULL/* methods */
	, &UInt64_t348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 361/* custom_attributes_cache */
	, &AppID_t176_0_0_0/* byval_arg */
	, &AppID_t176_1_0_0/* this_arg */
	, &AppID_t176_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppID_t176)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AppID_t176)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
// Metadata Definition UnityEngine.Networking.Types.SourceID
extern TypeInfo SourceID_t177_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceIDMethodDeclarations.h"
static const EncodedMethodIndex SourceID_t177_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair SourceID_t177_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SourceID_t177_0_0_0;
extern const Il2CppType SourceID_t177_1_0_0;
const Il2CppTypeDefinitionMetadata SourceID_t177_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SourceID_t177_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, SourceID_t177_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 836/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SourceID_t177_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SourceID"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NULL/* methods */
	, &UInt64_t348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 362/* custom_attributes_cache */
	, &SourceID_t177_0_0_0/* byval_arg */
	, &SourceID_t177_1_0_0/* this_arg */
	, &SourceID_t177_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SourceID_t177)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SourceID_t177)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
// Metadata Definition UnityEngine.Networking.Types.NetworkID
extern TypeInfo NetworkID_t178_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkIDMethodDeclarations.h"
static const EncodedMethodIndex NetworkID_t178_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair NetworkID_t178_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NetworkID_t178_0_0_0;
extern const Il2CppType NetworkID_t178_1_0_0;
const Il2CppTypeDefinitionMetadata NetworkID_t178_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NetworkID_t178_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, NetworkID_t178_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 838/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NetworkID_t178_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NetworkID"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NULL/* methods */
	, &UInt64_t348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 363/* custom_attributes_cache */
	, &NetworkID_t178_0_0_0/* byval_arg */
	, &NetworkID_t178_1_0_0/* this_arg */
	, &NetworkID_t178_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NetworkID_t178)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (NetworkID_t178)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
// Metadata Definition UnityEngine.Networking.Types.NodeID
extern TypeInfo NodeID_t179_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeIDMethodDeclarations.h"
static const EncodedMethodIndex NodeID_t179_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair NodeID_t179_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NodeID_t179_0_0_0;
extern const Il2CppType NodeID_t179_1_0_0;
// System.UInt16
#include "mscorlib_System_UInt16.h"
extern TypeInfo UInt16_t351_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata NodeID_t179_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NodeID_t179_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, NodeID_t179_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 840/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NodeID_t179_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NodeID"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NULL/* methods */
	, &UInt16_t351_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 364/* custom_attributes_cache */
	, &NodeID_t179_0_0_0/* byval_arg */
	, &NodeID_t179_1_0_0/* this_arg */
	, &NodeID_t179_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NodeID_t179)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (NodeID_t179)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Types.NetworkAccessToken
#include "UnityEngine_UnityEngine_Networking_Types_NetworkAccessToken.h"
// Metadata Definition UnityEngine.Networking.Types.NetworkAccessToken
extern TypeInfo NetworkAccessToken_t180_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.NetworkAccessToken
#include "UnityEngine_UnityEngine_Networking_Types_NetworkAccessTokenMethodDeclarations.h"
static const EncodedMethodIndex NetworkAccessToken_t180_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NetworkAccessToken_t180_0_0_0;
extern const Il2CppType NetworkAccessToken_t180_1_0_0;
struct NetworkAccessToken_t180;
const Il2CppTypeDefinitionMetadata NetworkAccessToken_t180_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NetworkAccessToken_t180_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 842/* fieldStart */
	, 907/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NetworkAccessToken_t180_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NetworkAccessToken"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NULL/* methods */
	, &NetworkAccessToken_t180_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NetworkAccessToken_t180_0_0_0/* byval_arg */
	, &NetworkAccessToken_t180_1_0_0/* this_arg */
	, &NetworkAccessToken_t180_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NetworkAccessToken_t180)/* instance_size */
	, sizeof (NetworkAccessToken_t180)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Utility
#include "UnityEngine_UnityEngine_Networking_Utility.h"
// Metadata Definition UnityEngine.Networking.Utility
extern TypeInfo Utility_t183_il2cpp_TypeInfo;
// UnityEngine.Networking.Utility
#include "UnityEngine_UnityEngine_Networking_UtilityMethodDeclarations.h"
static const EncodedMethodIndex Utility_t183_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Utility_t183_0_0_0;
extern const Il2CppType Utility_t183_1_0_0;
struct Utility_t183;
const Il2CppTypeDefinitionMetadata Utility_t183_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Utility_t183_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 843/* fieldStart */
	, 909/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Utility_t183_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Utility"/* name */
	, "UnityEngine.Networking"/* namespaze */
	, NULL/* methods */
	, &Utility_t183_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Utility_t183_0_0_0/* byval_arg */
	, &Utility_t183_1_0_0/* this_arg */
	, &Utility_t183_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Utility_t183)/* instance_size */
	, sizeof (Utility_t183)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Utility_t183_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.NetworkMatch
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatch.h"
// Metadata Definition UnityEngine.Networking.Match.NetworkMatch
extern TypeInfo NetworkMatch_t185_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.NetworkMatch
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatchMethodDeclarations.h"
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t2203_0_0_0;
extern const Il2CppRGCTXDefinition NetworkMatch_ProcessMatchResponse_m12759_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 3957 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2537 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType ResponseDelegate_1_t2039_0_0_0;
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_0_0_0;
static const Il2CppType* NetworkMatch_t185_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ResponseDelegate_1_t2039_0_0_0,
	&U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_0_0_0,
};
static const EncodedMethodIndex NetworkMatch_t185_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NetworkMatch_t185_0_0_0;
extern const Il2CppType NetworkMatch_t185_1_0_0;
extern const Il2CppType MonoBehaviour_t116_0_0_0;
struct NetworkMatch_t185;
const Il2CppTypeDefinitionMetadata NetworkMatch_t185_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NetworkMatch_t185_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t116_0_0_0/* parent */
	, NetworkMatch_t185_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 848/* fieldStart */
	, 914/* methodStart */
	, -1/* eventStart */
	, 241/* propertyStart */

};
TypeInfo NetworkMatch_t185_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NetworkMatch"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &NetworkMatch_t185_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NetworkMatch_t185_0_0_0/* byval_arg */
	, &NetworkMatch_t185_1_0_0/* this_arg */
	, &NetworkMatch_t185_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NetworkMatch_t185)/* instance_size */
	, sizeof (NetworkMatch_t185)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1
extern TypeInfo ResponseDelegate_1_t2039_il2cpp_TypeInfo;
static const EncodedMethodIndex ResponseDelegate_1_t2039_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	308,
	309,
	310,
};
static Il2CppInterfaceOffsetPair ResponseDelegate_1_t2039_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ResponseDelegate_1_t2039_1_0_0;
struct ResponseDelegate_1_t2039;
const Il2CppTypeDefinitionMetadata ResponseDelegate_1_t2039_DefinitionMetadata = 
{
	&NetworkMatch_t185_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ResponseDelegate_1_t2039_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, ResponseDelegate_1_t2039_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 929/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ResponseDelegate_1_t2039_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResponseDelegate`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ResponseDelegate_1_t2039_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ResponseDelegate_1_t2039_0_0_0/* byval_arg */
	, &ResponseDelegate_1_t2039_1_0_0/* this_arg */
	, &ResponseDelegate_1_t2039_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 4/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1
extern TypeInfo U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_il2cpp_TypeInfo;
static const EncodedMethodIndex U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_VTable[8] = 
{
	120,
	125,
	122,
	123,
	311,
	312,
	313,
	314,
};
extern const Il2CppType IEnumerator_t279_0_0_0;
extern const Il2CppType IEnumerator_1_t1807_0_0_0;
static const Il2CppType* U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
	&IEnumerator_t279_0_0_0,
	&IEnumerator_1_t1807_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &IEnumerator_t279_0_0_0, 5},
	{ &IEnumerator_1_t1807_0_0_0, 7},
};
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 2419 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2538 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2539 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_1_0_0;
struct U3CProcessMatchResponseU3Ec__Iterator0_1_t2040;
const Il2CppTypeDefinitionMetadata U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_DefinitionMetadata = 
{
	&NetworkMatch_t185_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_InterfacesTypeInfos/* implementedInterfaces */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_VTable/* vtableMethods */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_RGCTXData/* rgctxDefinition */
	, 850/* fieldStart */
	, 933/* methodStart */
	, -1/* eventStart */
	, 242/* propertyStart */

};
TypeInfo U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ProcessMatchResponse>c__Iterator0`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 366/* custom_attributes_cache */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_0_0_0/* byval_arg */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_1_0_0/* this_arg */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 5/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SimpleJson.JsonArray
#include "UnityEngine_SimpleJson_JsonArray.h"
// Metadata Definition SimpleJson.JsonArray
extern TypeInfo JsonArray_t186_il2cpp_TypeInfo;
// SimpleJson.JsonArray
#include "UnityEngine_SimpleJson_JsonArrayMethodDeclarations.h"
static const EncodedMethodIndex JsonArray_t186_VTable[30] = 
{
	120,
	125,
	122,
	315,
	2147483964,
	2147483965,
	2147483966,
	2147483967,
	2147483968,
	2147483969,
	2147483970,
	2147483971,
	2147483972,
	2147483973,
	2147483974,
	2147483975,
	2147483976,
	2147483965,
	2147483977,
	2147483978,
	2147483971,
	2147483979,
	2147483980,
	2147483981,
	2147483982,
	2147483983,
	2147483984,
	2147483976,
	2147483985,
	2147483986,
};
extern const Il2CppType IEnumerable_t302_0_0_0;
extern const Il2CppType ICollection_t569_0_0_0;
extern const Il2CppType IList_t519_0_0_0;
extern const Il2CppType ICollection_1_t296_0_0_0;
extern const Il2CppType IEnumerable_1_t2205_0_0_0;
extern const Il2CppType IList_1_t1745_0_0_0;
static Il2CppInterfaceOffsetPair JsonArray_t186_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
	{ &ICollection_1_t296_0_0_0, 17},
	{ &IEnumerable_1_t2205_0_0_0, 24},
	{ &IList_1_t1745_0_0_0, 25},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType JsonArray_t186_0_0_0;
extern const Il2CppType JsonArray_t186_1_0_0;
extern const Il2CppType List_1_t187_0_0_0;
struct JsonArray_t186;
const Il2CppTypeDefinitionMetadata JsonArray_t186_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, JsonArray_t186_InterfacesOffsets/* interfaceOffsets */
	, &List_1_t187_0_0_0/* parent */
	, JsonArray_t186_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 938/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo JsonArray_t186_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "JsonArray"/* name */
	, "SimpleJson"/* namespaze */
	, NULL/* methods */
	, &JsonArray_t186_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 370/* custom_attributes_cache */
	, &JsonArray_t186_0_0_0/* byval_arg */
	, &JsonArray_t186_1_0_0/* this_arg */
	, &JsonArray_t186_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (JsonArray_t186)/* instance_size */
	, sizeof (JsonArray_t186)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 30/* vtable_count */
	, 0/* interfaces_count */
	, 6/* interface_offsets_count */

};
// SimpleJson.JsonObject
#include "UnityEngine_SimpleJson_JsonObject.h"
// Metadata Definition SimpleJson.JsonObject
extern TypeInfo JsonObject_t189_il2cpp_TypeInfo;
// SimpleJson.JsonObject
#include "UnityEngine_SimpleJson_JsonObjectMethodDeclarations.h"
static const EncodedMethodIndex JsonObject_t189_VTable[19] = 
{
	120,
	125,
	122,
	339,
	340,
	341,
	342,
	343,
	344,
	345,
	346,
	347,
	348,
	349,
	350,
	351,
	352,
	353,
	354,
};
extern const Il2CppType IDictionary_2_t290_0_0_0;
extern const Il2CppType IEnumerable_1_t2206_0_0_0;
extern const Il2CppType ICollection_1_t2207_0_0_0;
static const Il2CppType* JsonObject_t189_InterfacesTypeInfos[] = 
{
	&IDictionary_2_t290_0_0_0,
	&IEnumerable_1_t2206_0_0_0,
	&IEnumerable_t302_0_0_0,
	&ICollection_1_t2207_0_0_0,
};
static Il2CppInterfaceOffsetPair JsonObject_t189_InterfacesOffsets[] = 
{
	{ &IDictionary_2_t290_0_0_0, 4},
	{ &IEnumerable_1_t2206_0_0_0, 10},
	{ &IEnumerable_t302_0_0_0, 11},
	{ &ICollection_1_t2207_0_0_0, 12},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType JsonObject_t189_0_0_0;
extern const Il2CppType JsonObject_t189_1_0_0;
struct JsonObject_t189;
const Il2CppTypeDefinitionMetadata JsonObject_t189_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, JsonObject_t189_InterfacesTypeInfos/* implementedInterfaces */
	, JsonObject_t189_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, JsonObject_t189_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 860/* fieldStart */
	, 940/* methodStart */
	, -1/* eventStart */
	, 244/* propertyStart */

};
TypeInfo JsonObject_t189_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "JsonObject"/* name */
	, "SimpleJson"/* namespaze */
	, NULL/* methods */
	, &JsonObject_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 371/* custom_attributes_cache */
	, &JsonObject_t189_0_0_0/* byval_arg */
	, &JsonObject_t189_1_0_0/* this_arg */
	, &JsonObject_t189_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (JsonObject_t189)/* instance_size */
	, sizeof (JsonObject_t189)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 5/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// SimpleJson.SimpleJson
#include "UnityEngine_SimpleJson_SimpleJson.h"
// Metadata Definition SimpleJson.SimpleJson
extern TypeInfo SimpleJson_t192_il2cpp_TypeInfo;
// SimpleJson.SimpleJson
#include "UnityEngine_SimpleJson_SimpleJsonMethodDeclarations.h"
static const EncodedMethodIndex SimpleJson_t192_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SimpleJson_t192_0_0_0;
extern const Il2CppType SimpleJson_t192_1_0_0;
struct SimpleJson_t192;
const Il2CppTypeDefinitionMetadata SimpleJson_t192_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SimpleJson_t192_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 861/* fieldStart */
	, 957/* methodStart */
	, -1/* eventStart */
	, 249/* propertyStart */

};
TypeInfo SimpleJson_t192_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleJson"/* name */
	, "SimpleJson"/* namespaze */
	, NULL/* methods */
	, &SimpleJson_t192_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 372/* custom_attributes_cache */
	, &SimpleJson_t192_0_0_0/* byval_arg */
	, &SimpleJson_t192_1_0_0/* this_arg */
	, &SimpleJson_t192_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleJson_t192)/* instance_size */
	, sizeof (SimpleJson_t192)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SimpleJson_t192_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition SimpleJson.IJsonSerializerStrategy
extern TypeInfo IJsonSerializerStrategy_t190_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IJsonSerializerStrategy_t190_0_0_0;
extern const Il2CppType IJsonSerializerStrategy_t190_1_0_0;
struct IJsonSerializerStrategy_t190;
const Il2CppTypeDefinitionMetadata IJsonSerializerStrategy_t190_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 978/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IJsonSerializerStrategy_t190_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IJsonSerializerStrategy"/* name */
	, "SimpleJson"/* namespaze */
	, NULL/* methods */
	, &IJsonSerializerStrategy_t190_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 376/* custom_attributes_cache */
	, &IJsonSerializerStrategy_t190_0_0_0/* byval_arg */
	, &IJsonSerializerStrategy_t190_1_0_0/* this_arg */
	, &IJsonSerializerStrategy_t190_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.PocoJsonSerializerStrategy
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategy.h"
// Metadata Definition SimpleJson.PocoJsonSerializerStrategy
extern TypeInfo PocoJsonSerializerStrategy_t191_il2cpp_TypeInfo;
// SimpleJson.PocoJsonSerializerStrategy
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategyMethodDeclarations.h"
static const EncodedMethodIndex PocoJsonSerializerStrategy_t191_VTable[13] = 
{
	120,
	125,
	122,
	123,
	355,
	356,
	357,
	358,
	359,
	355,
	360,
	361,
	362,
};
static const Il2CppType* PocoJsonSerializerStrategy_t191_InterfacesTypeInfos[] = 
{
	&IJsonSerializerStrategy_t190_0_0_0,
};
static Il2CppInterfaceOffsetPair PocoJsonSerializerStrategy_t191_InterfacesOffsets[] = 
{
	{ &IJsonSerializerStrategy_t190_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PocoJsonSerializerStrategy_t191_0_0_0;
extern const Il2CppType PocoJsonSerializerStrategy_t191_1_0_0;
struct PocoJsonSerializerStrategy_t191;
const Il2CppTypeDefinitionMetadata PocoJsonSerializerStrategy_t191_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, PocoJsonSerializerStrategy_t191_InterfacesTypeInfos/* implementedInterfaces */
	, PocoJsonSerializerStrategy_t191_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PocoJsonSerializerStrategy_t191_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 863/* fieldStart */
	, 979/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PocoJsonSerializerStrategy_t191_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PocoJsonSerializerStrategy"/* name */
	, "SimpleJson"/* namespaze */
	, NULL/* methods */
	, &PocoJsonSerializerStrategy_t191_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 378/* custom_attributes_cache */
	, &PocoJsonSerializerStrategy_t191_0_0_0/* byval_arg */
	, &PocoJsonSerializerStrategy_t191_1_0_0/* this_arg */
	, &PocoJsonSerializerStrategy_t191_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PocoJsonSerializerStrategy_t191)/* instance_size */
	, sizeof (PocoJsonSerializerStrategy_t191)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PocoJsonSerializerStrategy_t191_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils
extern TypeInfo ReflectionUtils_t208_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtilsMethodDeclarations.h"
extern const Il2CppType ThreadSafeDictionary_2_t2041_0_0_0;
extern const Il2CppType GetDelegate_t198_0_0_0;
extern const Il2CppType SetDelegate_t199_0_0_0;
extern const Il2CppType ConstructorDelegate_t201_0_0_0;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t2042_0_0_0;
extern const Il2CppType U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203_0_0_0;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204_0_0_0;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205_0_0_0;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206_0_0_0;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207_0_0_0;
static const Il2CppType* ReflectionUtils_t208_il2cpp_TypeInfo__nestedTypes[10] =
{
	&ThreadSafeDictionary_2_t2041_0_0_0,
	&GetDelegate_t198_0_0_0,
	&SetDelegate_t199_0_0_0,
	&ConstructorDelegate_t201_0_0_0,
	&ThreadSafeDictionaryValueFactory_2_t2042_0_0_0,
	&U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203_0_0_0,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204_0_0_0,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205_0_0_0,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206_0_0_0,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207_0_0_0,
};
static const EncodedMethodIndex ReflectionUtils_t208_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionUtils_t208_0_0_0;
extern const Il2CppType ReflectionUtils_t208_1_0_0;
struct ReflectionUtils_t208;
const Il2CppTypeDefinitionMetadata ReflectionUtils_t208_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ReflectionUtils_t208_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReflectionUtils_t208_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 869/* fieldStart */
	, 989/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReflectionUtils_t208_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionUtils"/* name */
	, "SimpleJson.Reflection"/* namespaze */
	, NULL/* methods */
	, &ReflectionUtils_t208_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 381/* custom_attributes_cache */
	, &ReflectionUtils_t208_0_0_0/* byval_arg */
	, &ReflectionUtils_t208_1_0_0/* this_arg */
	, &ReflectionUtils_t208_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionUtils_t208)/* instance_size */
	, sizeof (ReflectionUtils_t208)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ReflectionUtils_t208_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 10/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2
extern TypeInfo ThreadSafeDictionary_2_t2041_il2cpp_TypeInfo;
static const EncodedMethodIndex ThreadSafeDictionary_2_t2041_VTable[19] = 
{
	120,
	125,
	122,
	123,
	363,
	364,
	365,
	366,
	367,
	368,
	369,
	370,
	371,
	372,
	373,
	374,
	375,
	376,
	377,
};
extern const Il2CppType IDictionary_2_t2208_0_0_0;
extern const Il2CppType ICollection_1_t2209_0_0_0;
extern const Il2CppType IEnumerable_1_t2210_0_0_0;
static const Il2CppType* ThreadSafeDictionary_2_t2041_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&IDictionary_2_t2208_0_0_0,
	&ICollection_1_t2209_0_0_0,
	&IEnumerable_1_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair ThreadSafeDictionary_2_t2041_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &IDictionary_2_t2208_0_0_0, 5},
	{ &ICollection_1_t2209_0_0_0, 11},
	{ &IEnumerable_1_t2210_0_0_0, 18},
};
extern const Il2CppType Enumerator_t2211_0_0_0;
extern const Il2CppType Dictionary_2_t2212_0_0_0;
extern const Il2CppRGCTXDefinition ThreadSafeDictionary_2_t2041_RGCTXData[15] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2540 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3975 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2541 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2542 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2543 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3976 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2544 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2545 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2546 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2547 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2548 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2549 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2550 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2551 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ThreadSafeDictionary_2_t2041_1_0_0;
struct ThreadSafeDictionary_2_t2041;
const Il2CppTypeDefinitionMetadata ThreadSafeDictionary_2_t2041_DefinitionMetadata = 
{
	&ReflectionUtils_t208_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, ThreadSafeDictionary_2_t2041_InterfacesTypeInfos/* implementedInterfaces */
	, ThreadSafeDictionary_2_t2041_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ThreadSafeDictionary_2_t2041_VTable/* vtableMethods */
	, ThreadSafeDictionary_2_t2041_RGCTXData/* rgctxDefinition */
	, 870/* fieldStart */
	, 1007/* methodStart */
	, -1/* eventStart */
	, 251/* propertyStart */

};
TypeInfo ThreadSafeDictionary_2_t2041_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadSafeDictionary`2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ThreadSafeDictionary_2_t2041_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 385/* custom_attributes_cache */
	, &ThreadSafeDictionary_2_t2041_0_0_0/* byval_arg */
	, &ThreadSafeDictionary_2_t2041_1_0_0/* this_arg */
	, &ThreadSafeDictionary_2_t2041_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 6/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048834/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_GetDelegat.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/GetDelegate
extern TypeInfo GetDelegate_t198_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_GetDelegatMethodDeclarations.h"
static const EncodedMethodIndex GetDelegate_t198_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	378,
	379,
	380,
};
static Il2CppInterfaceOffsetPair GetDelegate_t198_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GetDelegate_t198_1_0_0;
struct GetDelegate_t198;
const Il2CppTypeDefinitionMetadata GetDelegate_t198_DefinitionMetadata = 
{
	&ReflectionUtils_t208_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GetDelegate_t198_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, GetDelegate_t198_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1025/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GetDelegate_t198_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GetDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &GetDelegate_t198_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GetDelegate_t198_0_0_0/* byval_arg */
	, &GetDelegate_t198_1_0_0/* this_arg */
	, &GetDelegate_t198_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_GetDelegate_t198/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GetDelegate_t198)/* instance_size */
	, sizeof (GetDelegate_t198)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_SetDelegat.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/SetDelegate
extern TypeInfo SetDelegate_t199_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_SetDelegatMethodDeclarations.h"
static const EncodedMethodIndex SetDelegate_t199_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	381,
	382,
	383,
};
static Il2CppInterfaceOffsetPair SetDelegate_t199_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SetDelegate_t199_1_0_0;
struct SetDelegate_t199;
const Il2CppTypeDefinitionMetadata SetDelegate_t199_DefinitionMetadata = 
{
	&ReflectionUtils_t208_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SetDelegate_t199_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, SetDelegate_t199_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1029/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SetDelegate_t199_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SetDelegate_t199_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetDelegate_t199_0_0_0/* byval_arg */
	, &SetDelegate_t199_1_0_0/* this_arg */
	, &SetDelegate_t199_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_SetDelegate_t199/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetDelegate_t199)/* instance_size */
	, sizeof (SetDelegate_t199)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_Constructo.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
extern TypeInfo ConstructorDelegate_t201_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ConstructoMethodDeclarations.h"
static const EncodedMethodIndex ConstructorDelegate_t201_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	384,
	385,
	386,
};
static Il2CppInterfaceOffsetPair ConstructorDelegate_t201_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ConstructorDelegate_t201_1_0_0;
struct ConstructorDelegate_t201;
const Il2CppTypeDefinitionMetadata ConstructorDelegate_t201_DefinitionMetadata = 
{
	&ReflectionUtils_t208_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConstructorDelegate_t201_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, ConstructorDelegate_t201_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1033/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConstructorDelegate_t201_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructorDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ConstructorDelegate_t201_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructorDelegate_t201_0_0_0/* byval_arg */
	, &ConstructorDelegate_t201_1_0_0/* this_arg */
	, &ConstructorDelegate_t201_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ConstructorDelegate_t201/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructorDelegate_t201)/* instance_size */
	, sizeof (ConstructorDelegate_t201)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2
extern TypeInfo ThreadSafeDictionaryValueFactory_2_t2042_il2cpp_TypeInfo;
static const EncodedMethodIndex ThreadSafeDictionaryValueFactory_2_t2042_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	387,
	388,
	389,
};
static Il2CppInterfaceOffsetPair ThreadSafeDictionaryValueFactory_2_t2042_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t2042_1_0_0;
struct ThreadSafeDictionaryValueFactory_2_t2042;
const Il2CppTypeDefinitionMetadata ThreadSafeDictionaryValueFactory_2_t2042_DefinitionMetadata = 
{
	&ReflectionUtils_t208_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadSafeDictionaryValueFactory_2_t2042_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, ThreadSafeDictionaryValueFactory_2_t2042_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1037/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadSafeDictionaryValueFactory_2_t2042_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadSafeDictionaryValueFactory`2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ThreadSafeDictionaryValueFactory_2_t2042_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ThreadSafeDictionaryValueFactory_2_t2042_0_0_0/* byval_arg */
	, &ThreadSafeDictionaryValueFactory_2_t2042_1_0_0/* this_arg */
	, &ThreadSafeDictionaryValueFactory_2_t2042_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 7/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetCons.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
extern TypeInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetConsMethodDeclarations.h"
static const EncodedMethodIndex U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203_1_0_0;
struct U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203;
const Il2CppTypeDefinitionMetadata U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203_DefinitionMetadata = 
{
	&ReflectionUtils_t208_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 873/* fieldStart */
	, 1041/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetConstructorByReflection>c__AnonStorey1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 388/* custom_attributes_cache */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203_0_0_0/* byval_arg */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203_1_0_0/* this_arg */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203)/* instance_size */
	, sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t203)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
extern TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetMMethodDeclarations.h"
static const EncodedMethodIndex U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204_1_0_0;
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204;
const Il2CppTypeDefinitionMetadata U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204_DefinitionMetadata = 
{
	&ReflectionUtils_t208_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 874/* fieldStart */
	, 1043/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetGetMethodByReflection>c__AnonStorey2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 389/* custom_attributes_cache */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204_0_0_0/* byval_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204_1_0_0/* this_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204)/* instance_size */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t204)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM_0.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
extern TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM_0MethodDeclarations.h"
static const EncodedMethodIndex U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205_1_0_0;
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205;
const Il2CppTypeDefinitionMetadata U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205_DefinitionMetadata = 
{
	&ReflectionUtils_t208_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 875/* fieldStart */
	, 1045/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetGetMethodByReflection>c__AnonStorey3"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 390/* custom_attributes_cache */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205_0_0_0/* byval_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205_1_0_0/* this_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205)/* instance_size */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t205)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
extern TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetMMethodDeclarations.h"
static const EncodedMethodIndex U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206_1_0_0;
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206;
const Il2CppTypeDefinitionMetadata U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206_DefinitionMetadata = 
{
	&ReflectionUtils_t208_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 876/* fieldStart */
	, 1047/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetSetMethodByReflection>c__AnonStorey4"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 391/* custom_attributes_cache */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206_0_0_0/* byval_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206_1_0_0/* this_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206)/* instance_size */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t206)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM_0.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
extern TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM_0MethodDeclarations.h"
static const EncodedMethodIndex U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207_1_0_0;
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207;
const Il2CppTypeDefinitionMetadata U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207_DefinitionMetadata = 
{
	&ReflectionUtils_t208_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 877/* fieldStart */
	, 1049/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetSetMethodByReflection>c__AnonStorey5"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 392/* custom_attributes_cache */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207_0_0_0/* byval_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207_1_0_0/* this_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207)/* instance_size */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t207)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// Metadata Definition UnityEngine.WrapperlessIcall
extern TypeInfo WrapperlessIcall_t209_il2cpp_TypeInfo;
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
static const EncodedMethodIndex WrapperlessIcall_t209_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
extern const Il2CppType _Attribute_t2052_0_0_0;
static Il2CppInterfaceOffsetPair WrapperlessIcall_t209_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WrapperlessIcall_t209_0_0_0;
extern const Il2CppType WrapperlessIcall_t209_1_0_0;
extern const Il2CppType Attribute_t96_0_0_0;
struct WrapperlessIcall_t209;
const Il2CppTypeDefinitionMetadata WrapperlessIcall_t209_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WrapperlessIcall_t209_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, WrapperlessIcall_t209_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1051/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WrapperlessIcall_t209_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WrapperlessIcall"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WrapperlessIcall_t209_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WrapperlessIcall_t209_0_0_0/* byval_arg */
	, &WrapperlessIcall_t209_1_0_0/* this_arg */
	, &WrapperlessIcall_t209_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WrapperlessIcall_t209)/* instance_size */
	, sizeof (WrapperlessIcall_t209)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// Metadata Definition UnityEngine.IL2CPPStructAlignmentAttribute
extern TypeInfo IL2CPPStructAlignmentAttribute_t210_il2cpp_TypeInfo;
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
static const EncodedMethodIndex IL2CPPStructAlignmentAttribute_t210_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair IL2CPPStructAlignmentAttribute_t210_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IL2CPPStructAlignmentAttribute_t210_0_0_0;
extern const Il2CppType IL2CPPStructAlignmentAttribute_t210_1_0_0;
struct IL2CPPStructAlignmentAttribute_t210;
const Il2CppTypeDefinitionMetadata IL2CPPStructAlignmentAttribute_t210_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IL2CPPStructAlignmentAttribute_t210_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, IL2CPPStructAlignmentAttribute_t210_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 878/* fieldStart */
	, 1052/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IL2CPPStructAlignmentAttribute_t210_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IL2CPPStructAlignmentAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &IL2CPPStructAlignmentAttribute_t210_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 393/* custom_attributes_cache */
	, &IL2CPPStructAlignmentAttribute_t210_0_0_0/* byval_arg */
	, &IL2CPPStructAlignmentAttribute_t210_1_0_0/* this_arg */
	, &IL2CPPStructAlignmentAttribute_t210_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IL2CPPStructAlignmentAttribute_t210)/* instance_size */
	, sizeof (IL2CPPStructAlignmentAttribute_t210)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
// Metadata Definition UnityEngine.AttributeHelperEngine
extern TypeInfo AttributeHelperEngine_t214_il2cpp_TypeInfo;
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngineMethodDeclarations.h"
static const EncodedMethodIndex AttributeHelperEngine_t214_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AttributeHelperEngine_t214_0_0_0;
extern const Il2CppType AttributeHelperEngine_t214_1_0_0;
struct AttributeHelperEngine_t214;
const Il2CppTypeDefinitionMetadata AttributeHelperEngine_t214_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttributeHelperEngine_t214_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 879/* fieldStart */
	, 1053/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AttributeHelperEngine_t214_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeHelperEngine"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AttributeHelperEngine_t214_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttributeHelperEngine_t214_0_0_0/* byval_arg */
	, &AttributeHelperEngine_t214_1_0_0/* this_arg */
	, &AttributeHelperEngine_t214_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeHelperEngine_t214)/* instance_size */
	, sizeof (AttributeHelperEngine_t214)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AttributeHelperEngine_t214_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// Metadata Definition UnityEngine.DisallowMultipleComponent
extern TypeInfo DisallowMultipleComponent_t215_il2cpp_TypeInfo;
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
static const EncodedMethodIndex DisallowMultipleComponent_t215_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair DisallowMultipleComponent_t215_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DisallowMultipleComponent_t215_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t215_1_0_0;
struct DisallowMultipleComponent_t215;
const Il2CppTypeDefinitionMetadata DisallowMultipleComponent_t215_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DisallowMultipleComponent_t215_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, DisallowMultipleComponent_t215_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DisallowMultipleComponent_t215_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DisallowMultipleComponent"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &DisallowMultipleComponent_t215_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 394/* custom_attributes_cache */
	, &DisallowMultipleComponent_t215_0_0_0/* byval_arg */
	, &DisallowMultipleComponent_t215_1_0_0/* this_arg */
	, &DisallowMultipleComponent_t215_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DisallowMultipleComponent_t215)/* instance_size */
	, sizeof (DisallowMultipleComponent_t215)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// Metadata Definition UnityEngine.RequireComponent
extern TypeInfo RequireComponent_t216_il2cpp_TypeInfo;
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
static const EncodedMethodIndex RequireComponent_t216_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair RequireComponent_t216_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RequireComponent_t216_0_0_0;
extern const Il2CppType RequireComponent_t216_1_0_0;
struct RequireComponent_t216;
const Il2CppTypeDefinitionMetadata RequireComponent_t216_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RequireComponent_t216_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, RequireComponent_t216_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 882/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RequireComponent_t216_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RequireComponent"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RequireComponent_t216_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 395/* custom_attributes_cache */
	, &RequireComponent_t216_0_0_0/* byval_arg */
	, &RequireComponent_t216_1_0_0/* this_arg */
	, &RequireComponent_t216_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RequireComponent_t216)/* instance_size */
	, sizeof (RequireComponent_t216)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// Metadata Definition UnityEngine.AddComponentMenu
extern TypeInfo AddComponentMenu_t217_il2cpp_TypeInfo;
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
static const EncodedMethodIndex AddComponentMenu_t217_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair AddComponentMenu_t217_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AddComponentMenu_t217_0_0_0;
extern const Il2CppType AddComponentMenu_t217_1_0_0;
struct AddComponentMenu_t217;
const Il2CppTypeDefinitionMetadata AddComponentMenu_t217_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AddComponentMenu_t217_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, AddComponentMenu_t217_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 885/* fieldStart */
	, 1057/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AddComponentMenu_t217_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AddComponentMenu"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AddComponentMenu_t217_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AddComponentMenu_t217_0_0_0/* byval_arg */
	, &AddComponentMenu_t217_1_0_0/* this_arg */
	, &AddComponentMenu_t217_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AddComponentMenu_t217)/* instance_size */
	, sizeof (AddComponentMenu_t217)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// Metadata Definition UnityEngine.ExecuteInEditMode
extern TypeInfo ExecuteInEditMode_t218_il2cpp_TypeInfo;
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
static const EncodedMethodIndex ExecuteInEditMode_t218_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair ExecuteInEditMode_t218_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExecuteInEditMode_t218_0_0_0;
extern const Il2CppType ExecuteInEditMode_t218_1_0_0;
struct ExecuteInEditMode_t218;
const Il2CppTypeDefinitionMetadata ExecuteInEditMode_t218_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExecuteInEditMode_t218_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, ExecuteInEditMode_t218_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1058/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExecuteInEditMode_t218_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecuteInEditMode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ExecuteInEditMode_t218_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExecuteInEditMode_t218_0_0_0/* byval_arg */
	, &ExecuteInEditMode_t218_1_0_0/* this_arg */
	, &ExecuteInEditMode_t218_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecuteInEditMode_t218)/* instance_size */
	, sizeof (ExecuteInEditMode_t218)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.CastHelper`1
extern TypeInfo CastHelper_1_t2043_il2cpp_TypeInfo;
static const EncodedMethodIndex CastHelper_1_t2043_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CastHelper_1_t2043_0_0_0;
extern const Il2CppType CastHelper_1_t2043_1_0_0;
const Il2CppTypeDefinitionMetadata CastHelper_1_t2043_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, CastHelper_1_t2043_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 887/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CastHelper_1_t2043_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &CastHelper_1_t2043_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CastHelper_1_t2043_0_0_0/* byval_arg */
	, &CastHelper_1_t2043_1_0_0/* this_arg */
	, &CastHelper_1_t2043_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 8/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutine.h"
// Metadata Definition UnityEngine.SetupCoroutine
extern TypeInfo SetupCoroutine_t219_il2cpp_TypeInfo;
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutineMethodDeclarations.h"
static const EncodedMethodIndex SetupCoroutine_t219_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SetupCoroutine_t219_0_0_0;
extern const Il2CppType SetupCoroutine_t219_1_0_0;
struct SetupCoroutine_t219;
const Il2CppTypeDefinitionMetadata SetupCoroutine_t219_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SetupCoroutine_t219_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1059/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SetupCoroutine_t219_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetupCoroutine"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SetupCoroutine_t219_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetupCoroutine_t219_0_0_0/* byval_arg */
	, &SetupCoroutine_t219_1_0_0/* this_arg */
	, &SetupCoroutine_t219_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetupCoroutine_t219)/* instance_size */
	, sizeof (SetupCoroutine_t219)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
// Metadata Definition UnityEngine.WritableAttribute
extern TypeInfo WritableAttribute_t220_il2cpp_TypeInfo;
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
static const EncodedMethodIndex WritableAttribute_t220_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair WritableAttribute_t220_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WritableAttribute_t220_0_0_0;
extern const Il2CppType WritableAttribute_t220_1_0_0;
struct WritableAttribute_t220;
const Il2CppTypeDefinitionMetadata WritableAttribute_t220_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WritableAttribute_t220_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, WritableAttribute_t220_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1062/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WritableAttribute_t220_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WritableAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WritableAttribute_t220_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 396/* custom_attributes_cache */
	, &WritableAttribute_t220_0_0_0/* byval_arg */
	, &WritableAttribute_t220_1_0_0/* this_arg */
	, &WritableAttribute_t220_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WritableAttribute_t220)/* instance_size */
	, sizeof (WritableAttribute_t220)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly.h"
// Metadata Definition UnityEngine.AssemblyIsEditorAssembly
extern TypeInfo AssemblyIsEditorAssembly_t221_il2cpp_TypeInfo;
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssemblyMethodDeclarations.h"
static const EncodedMethodIndex AssemblyIsEditorAssembly_t221_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair AssemblyIsEditorAssembly_t221_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AssemblyIsEditorAssembly_t221_0_0_0;
extern const Il2CppType AssemblyIsEditorAssembly_t221_1_0_0;
struct AssemblyIsEditorAssembly_t221;
const Il2CppTypeDefinitionMetadata AssemblyIsEditorAssembly_t221_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyIsEditorAssembly_t221_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, AssemblyIsEditorAssembly_t221_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1063/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyIsEditorAssembly_t221_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyIsEditorAssembly"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AssemblyIsEditorAssembly_t221_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 397/* custom_attributes_cache */
	, &AssemblyIsEditorAssembly_t221_0_0_0/* byval_arg */
	, &AssemblyIsEditorAssembly_t221_1_0_0/* this_arg */
	, &AssemblyIsEditorAssembly_t221_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyIsEditorAssembly_t221)/* instance_size */
	, sizeof (AssemblyIsEditorAssembly_t221)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern TypeInfo GcUserProfileData_t222_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"
static const EncodedMethodIndex GcUserProfileData_t222_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcUserProfileData_t222_0_0_0;
extern const Il2CppType GcUserProfileData_t222_1_0_0;
const Il2CppTypeDefinitionMetadata GcUserProfileData_t222_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, GcUserProfileData_t222_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 889/* fieldStart */
	, 1064/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GcUserProfileData_t222_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcUserProfileData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, NULL/* methods */
	, &GcUserProfileData_t222_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcUserProfileData_t222_0_0_0/* byval_arg */
	, &GcUserProfileData_t222_1_0_0/* this_arg */
	, &GcUserProfileData_t222_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcUserProfileData_t222)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcUserProfileData_t222)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern TypeInfo GcAchievementDescriptionData_t223_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
static const EncodedMethodIndex GcAchievementDescriptionData_t223_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcAchievementDescriptionData_t223_0_0_0;
extern const Il2CppType GcAchievementDescriptionData_t223_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementDescriptionData_t223_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, GcAchievementDescriptionData_t223_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 893/* fieldStart */
	, 1066/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GcAchievementDescriptionData_t223_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementDescriptionData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, NULL/* methods */
	, &GcAchievementDescriptionData_t223_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementDescriptionData_t223_0_0_0/* byval_arg */
	, &GcAchievementDescriptionData_t223_1_0_0/* this_arg */
	, &GcAchievementDescriptionData_t223_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcAchievementDescriptionData_t223)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementDescriptionData_t223)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern TypeInfo GcAchievementData_t224_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
static const EncodedMethodIndex GcAchievementData_t224_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcAchievementData_t224_0_0_0;
extern const Il2CppType GcAchievementData_t224_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementData_t224_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, GcAchievementData_t224_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 900/* fieldStart */
	, 1067/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GcAchievementData_t224_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, NULL/* methods */
	, &GcAchievementData_t224_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementData_t224_0_0_0/* byval_arg */
	, &GcAchievementData_t224_1_0_0/* this_arg */
	, &GcAchievementData_t224_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcAchievementData_t224_marshal/* marshal_to_native_func */
	, (methodPointerType)GcAchievementData_t224_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcAchievementData_t224_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcAchievementData_t224)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementData_t224)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcAchievementData_t224_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern TypeInfo GcScoreData_t225_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
static const EncodedMethodIndex GcScoreData_t225_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcScoreData_t225_0_0_0;
extern const Il2CppType GcScoreData_t225_1_0_0;
const Il2CppTypeDefinitionMetadata GcScoreData_t225_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, GcScoreData_t225_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 905/* fieldStart */
	, 1068/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GcScoreData_t225_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcScoreData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, NULL/* methods */
	, &GcScoreData_t225_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcScoreData_t225_0_0_0/* byval_arg */
	, &GcScoreData_t225_1_0_0/* this_arg */
	, &GcScoreData_t225_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcScoreData_t225_marshal/* marshal_to_native_func */
	, (methodPointerType)GcScoreData_t225_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcScoreData_t225_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcScoreData_t225)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcScoreData_t225)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcScoreData_t225_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_Resolution.h"
// Metadata Definition UnityEngine.Resolution
extern TypeInfo Resolution_t226_il2cpp_TypeInfo;
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_ResolutionMethodDeclarations.h"
static const EncodedMethodIndex Resolution_t226_VTable[4] = 
{
	150,
	125,
	151,
	390,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Resolution_t226_0_0_0;
extern const Il2CppType Resolution_t226_1_0_0;
const Il2CppTypeDefinitionMetadata Resolution_t226_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Resolution_t226_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 912/* fieldStart */
	, 1069/* methodStart */
	, -1/* eventStart */
	, 256/* propertyStart */

};
TypeInfo Resolution_t226_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Resolution"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Resolution_t226_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Resolution_t226_0_0_0/* byval_arg */
	, &Resolution_t226_1_0_0/* this_arg */
	, &Resolution_t226_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Resolution_t226)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Resolution_t226)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Resolution_t226 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
// Metadata Definition UnityEngine.RenderBuffer
extern TypeInfo RenderBuffer_t227_il2cpp_TypeInfo;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBufferMethodDeclarations.h"
static const EncodedMethodIndex RenderBuffer_t227_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderBuffer_t227_0_0_0;
extern const Il2CppType RenderBuffer_t227_1_0_0;
const Il2CppTypeDefinitionMetadata RenderBuffer_t227_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, RenderBuffer_t227_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 915/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RenderBuffer_t227_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderBuffer"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RenderBuffer_t227_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderBuffer_t227_0_0_0/* byval_arg */
	, &RenderBuffer_t227_1_0_0/* this_arg */
	, &RenderBuffer_t227_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderBuffer_t227)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderBuffer_t227)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RenderBuffer_t227 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
// Metadata Definition UnityEngine.CameraClearFlags
extern TypeInfo CameraClearFlags_t228_il2cpp_TypeInfo;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlagsMethodDeclarations.h"
static const EncodedMethodIndex CameraClearFlags_t228_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair CameraClearFlags_t228_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CameraClearFlags_t228_0_0_0;
extern const Il2CppType CameraClearFlags_t228_1_0_0;
const Il2CppTypeDefinitionMetadata CameraClearFlags_t228_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CameraClearFlags_t228_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, CameraClearFlags_t228_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 917/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CameraClearFlags_t228_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraClearFlags"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CameraClearFlags_t228_0_0_0/* byval_arg */
	, &CameraClearFlags_t228_1_0_0/* this_arg */
	, &CameraClearFlags_t228_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraClearFlags_t228)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CameraClearFlags_t228)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// Metadata Definition UnityEngine.TextureFormat
extern TypeInfo TextureFormat_t229_il2cpp_TypeInfo;
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormatMethodDeclarations.h"
static const EncodedMethodIndex TextureFormat_t229_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair TextureFormat_t229_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextureFormat_t229_0_0_0;
extern const Il2CppType TextureFormat_t229_1_0_0;
const Il2CppTypeDefinitionMetadata TextureFormat_t229_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextureFormat_t229_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, TextureFormat_t229_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 923/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextureFormat_t229_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextureFormat_t229_0_0_0/* byval_arg */
	, &TextureFormat_t229_1_0_0/* this_arg */
	, &TextureFormat_t229_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextureFormat_t229)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextureFormat_t229)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 45/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfo.h"
// Metadata Definition UnityEngine.Rendering.ReflectionProbeBlendInfo
extern TypeInfo ReflectionProbeBlendInfo_t230_il2cpp_TypeInfo;
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfoMethodDeclarations.h"
static const EncodedMethodIndex ReflectionProbeBlendInfo_t230_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionProbeBlendInfo_t230_0_0_0;
extern const Il2CppType ReflectionProbeBlendInfo_t230_1_0_0;
const Il2CppTypeDefinitionMetadata ReflectionProbeBlendInfo_t230_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, ReflectionProbeBlendInfo_t230_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 968/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReflectionProbeBlendInfo_t230_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionProbeBlendInfo"/* name */
	, "UnityEngine.Rendering"/* namespaze */
	, NULL/* methods */
	, &ReflectionProbeBlendInfo_t230_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReflectionProbeBlendInfo_t230_0_0_0/* byval_arg */
	, &ReflectionProbeBlendInfo_t230_1_0_0/* this_arg */
	, &ReflectionProbeBlendInfo_t230_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionProbeBlendInfo_t230)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReflectionProbeBlendInfo_t230)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.LocalUser
extern TypeInfo LocalUser_t23_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
static const EncodedMethodIndex LocalUser_t23_VTable[9] = 
{
	120,
	125,
	122,
	391,
	392,
	393,
	394,
	395,
	396,
};
extern const Il2CppType ILocalUser_t273_0_0_0;
extern const Il2CppType IUserProfile_t1805_0_0_0;
static const Il2CppType* LocalUser_t23_InterfacesTypeInfos[] = 
{
	&ILocalUser_t273_0_0_0,
	&IUserProfile_t1805_0_0_0,
};
static Il2CppInterfaceOffsetPair LocalUser_t23_InterfacesOffsets[] = 
{
	{ &IUserProfile_t1805_0_0_0, 4},
	{ &ILocalUser_t273_0_0_0, 8},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LocalUser_t23_0_0_0;
extern const Il2CppType LocalUser_t23_1_0_0;
extern const Il2CppType UserProfile_t232_0_0_0;
struct LocalUser_t23;
const Il2CppTypeDefinitionMetadata LocalUser_t23_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LocalUser_t23_InterfacesTypeInfos/* implementedInterfaces */
	, LocalUser_t23_InterfacesOffsets/* interfaceOffsets */
	, &UserProfile_t232_0_0_0/* parent */
	, LocalUser_t23_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 970/* fieldStart */
	, 1076/* methodStart */
	, -1/* eventStart */
	, 259/* propertyStart */

};
TypeInfo LocalUser_t23_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LocalUser"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, NULL/* methods */
	, &LocalUser_t23_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LocalUser_t23_0_0_0/* byval_arg */
	, &LocalUser_t23_1_0_0/* this_arg */
	, &LocalUser_t23_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LocalUser_t23)/* instance_size */
	, sizeof (LocalUser_t23)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.UserProfile
extern TypeInfo UserProfile_t232_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
static const EncodedMethodIndex UserProfile_t232_VTable[8] = 
{
	120,
	125,
	122,
	391,
	392,
	393,
	394,
	395,
};
static const Il2CppType* UserProfile_t232_InterfacesTypeInfos[] = 
{
	&IUserProfile_t1805_0_0_0,
};
static Il2CppInterfaceOffsetPair UserProfile_t232_InterfacesOffsets[] = 
{
	{ &IUserProfile_t1805_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserProfile_t232_1_0_0;
struct UserProfile_t232;
const Il2CppTypeDefinitionMetadata UserProfile_t232_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UserProfile_t232_InterfacesTypeInfos/* implementedInterfaces */
	, UserProfile_t232_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UserProfile_t232_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 973/* fieldStart */
	, 1081/* methodStart */
	, -1/* eventStart */
	, 260/* propertyStart */

};
TypeInfo UserProfile_t232_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserProfile"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, NULL/* methods */
	, &UserProfile_t232_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserProfile_t232_0_0_0/* byval_arg */
	, &UserProfile_t232_1_0_0/* this_arg */
	, &UserProfile_t232_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserProfile_t232)/* instance_size */
	, sizeof (UserProfile_t232)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Achievement
extern TypeInfo Achievement_t233_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
static const EncodedMethodIndex Achievement_t233_VTable[11] = 
{
	120,
	125,
	122,
	397,
	398,
	399,
	400,
	401,
	402,
	403,
	404,
};
extern const Il2CppType IAchievement_t277_0_0_0;
static const Il2CppType* Achievement_t233_InterfacesTypeInfos[] = 
{
	&IAchievement_t277_0_0_0,
};
static Il2CppInterfaceOffsetPair Achievement_t233_InterfacesOffsets[] = 
{
	{ &IAchievement_t277_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Achievement_t233_0_0_0;
extern const Il2CppType Achievement_t233_1_0_0;
struct Achievement_t233;
const Il2CppTypeDefinitionMetadata Achievement_t233_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Achievement_t233_InterfacesTypeInfos/* implementedInterfaces */
	, Achievement_t233_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Achievement_t233_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 978/* fieldStart */
	, 1091/* methodStart */
	, -1/* eventStart */
	, 264/* propertyStart */

};
TypeInfo Achievement_t233_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Achievement"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, NULL/* methods */
	, &Achievement_t233_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Achievement_t233_0_0_0/* byval_arg */
	, &Achievement_t233_1_0_0/* this_arg */
	, &Achievement_t233_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Achievement_t233)/* instance_size */
	, sizeof (Achievement_t233)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.AchievementDescription
extern TypeInfo AchievementDescription_t234_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
static const EncodedMethodIndex AchievementDescription_t234_VTable[11] = 
{
	120,
	125,
	122,
	405,
	406,
	407,
	408,
	409,
	410,
	411,
	412,
};
extern const Il2CppType IAchievementDescription_t1804_0_0_0;
static const Il2CppType* AchievementDescription_t234_InterfacesTypeInfos[] = 
{
	&IAchievementDescription_t1804_0_0_0,
};
static Il2CppInterfaceOffsetPair AchievementDescription_t234_InterfacesOffsets[] = 
{
	{ &IAchievementDescription_t1804_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AchievementDescription_t234_0_0_0;
extern const Il2CppType AchievementDescription_t234_1_0_0;
struct AchievementDescription_t234;
const Il2CppTypeDefinitionMetadata AchievementDescription_t234_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AchievementDescription_t234_InterfacesTypeInfos/* implementedInterfaces */
	, AchievementDescription_t234_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AchievementDescription_t234_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 983/* fieldStart */
	, 1102/* methodStart */
	, -1/* eventStart */
	, 269/* propertyStart */

};
TypeInfo AchievementDescription_t234_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, NULL/* methods */
	, &AchievementDescription_t234_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AchievementDescription_t234_0_0_0/* byval_arg */
	, &AchievementDescription_t234_1_0_0/* this_arg */
	, &AchievementDescription_t234_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AchievementDescription_t234)/* instance_size */
	, sizeof (AchievementDescription_t234)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Score
extern TypeInfo Score_t235_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"
static const EncodedMethodIndex Score_t235_VTable[8] = 
{
	120,
	125,
	122,
	413,
	414,
	415,
	416,
	417,
};
extern const Il2CppType IScore_t236_0_0_0;
static const Il2CppType* Score_t235_InterfacesTypeInfos[] = 
{
	&IScore_t236_0_0_0,
};
static Il2CppInterfaceOffsetPair Score_t235_InterfacesOffsets[] = 
{
	{ &IScore_t236_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Score_t235_0_0_0;
extern const Il2CppType Score_t235_1_0_0;
struct Score_t235;
const Il2CppTypeDefinitionMetadata Score_t235_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Score_t235_InterfacesTypeInfos/* implementedInterfaces */
	, Score_t235_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Score_t235_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 990/* fieldStart */
	, 1112/* methodStart */
	, -1/* eventStart */
	, 275/* propertyStart */

};
TypeInfo Score_t235_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Score"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, NULL/* methods */
	, &Score_t235_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Score_t235_0_0_0/* byval_arg */
	, &Score_t235_1_0_0/* this_arg */
	, &Score_t235_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Score_t235)/* instance_size */
	, sizeof (Score_t235)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Leaderboard
extern TypeInfo Leaderboard_t26_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
static const EncodedMethodIndex Leaderboard_t26_VTable[12] = 
{
	120,
	125,
	122,
	418,
	419,
	420,
	421,
	422,
	423,
	424,
	425,
	426,
};
extern const Il2CppType ILeaderboard_t276_0_0_0;
static const Il2CppType* Leaderboard_t26_InterfacesTypeInfos[] = 
{
	&ILeaderboard_t276_0_0_0,
};
static Il2CppInterfaceOffsetPair Leaderboard_t26_InterfacesOffsets[] = 
{
	{ &ILeaderboard_t276_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Leaderboard_t26_0_0_0;
extern const Il2CppType Leaderboard_t26_1_0_0;
struct Leaderboard_t26;
const Il2CppTypeDefinitionMetadata Leaderboard_t26_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Leaderboard_t26_InterfacesTypeInfos/* implementedInterfaces */
	, Leaderboard_t26_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Leaderboard_t26_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 996/* fieldStart */
	, 1119/* methodStart */
	, -1/* eventStart */
	, 277/* propertyStart */

};
TypeInfo Leaderboard_t26_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Leaderboard"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, NULL/* methods */
	, &Leaderboard_t26_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Leaderboard_t26_0_0_0/* byval_arg */
	, &Leaderboard_t26_1_0_0/* this_arg */
	, &Leaderboard_t26_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Leaderboard_t26)/* instance_size */
	, sizeof (Leaderboard_t26)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 4/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEvents.h"
// Metadata Definition UnityEngine.SendMouseEvents
extern TypeInfo SendMouseEvents_t242_il2cpp_TypeInfo;
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEventsMethodDeclarations.h"
extern const Il2CppType HitInfo_t239_0_0_0;
static const Il2CppType* SendMouseEvents_t242_il2cpp_TypeInfo__nestedTypes[1] =
{
	&HitInfo_t239_0_0_0,
};
static const EncodedMethodIndex SendMouseEvents_t242_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SendMouseEvents_t242_0_0_0;
extern const Il2CppType SendMouseEvents_t242_1_0_0;
struct SendMouseEvents_t242;
const Il2CppTypeDefinitionMetadata SendMouseEvents_t242_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SendMouseEvents_t242_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SendMouseEvents_t242_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1006/* fieldStart */
	, 1134/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SendMouseEvents_t242_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SendMouseEvents"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SendMouseEvents_t242_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SendMouseEvents_t242_0_0_0/* byval_arg */
	, &SendMouseEvents_t242_1_0_0/* this_arg */
	, &SendMouseEvents_t242_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SendMouseEvents_t242)/* instance_size */
	, sizeof (SendMouseEvents_t242)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SendMouseEvents_t242_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
// Metadata Definition UnityEngine.SendMouseEvents/HitInfo
extern TypeInfo HitInfo_t239_il2cpp_TypeInfo;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfoMethodDeclarations.h"
static const EncodedMethodIndex HitInfo_t239_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HitInfo_t239_1_0_0;
const Il2CppTypeDefinitionMetadata HitInfo_t239_DefinitionMetadata = 
{
	&SendMouseEvents_t242_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, HitInfo_t239_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1013/* fieldStart */
	, 1137/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HitInfo_t239_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HitInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &HitInfo_t239_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HitInfo_t239_0_0_0/* byval_arg */
	, &HitInfo_t239_1_0_0/* this_arg */
	, &HitInfo_t239_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HitInfo_t239)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HitInfo_t239)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ISocialPlatform
extern TypeInfo ISocialPlatform_t2044_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ISocialPlatform_t2044_0_0_0;
extern const Il2CppType ISocialPlatform_t2044_1_0_0;
struct ISocialPlatform_t2044;
const Il2CppTypeDefinitionMetadata ISocialPlatform_t2044_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1140/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISocialPlatform_t2044_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISocialPlatform"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &ISocialPlatform_t2044_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISocialPlatform_t2044_0_0_0/* byval_arg */
	, &ISocialPlatform_t2044_1_0_0/* this_arg */
	, &ISocialPlatform_t2044_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILocalUser
extern TypeInfo ILocalUser_t273_il2cpp_TypeInfo;
static const Il2CppType* ILocalUser_t273_InterfacesTypeInfos[] = 
{
	&IUserProfile_t1805_0_0_0,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ILocalUser_t273_1_0_0;
struct ILocalUser_t273;
const Il2CppTypeDefinitionMetadata ILocalUser_t273_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILocalUser_t273_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1142/* methodStart */
	, -1/* eventStart */
	, 281/* propertyStart */

};
TypeInfo ILocalUser_t273_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILocalUser"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &ILocalUser_t273_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILocalUser_t273_0_0_0/* byval_arg */
	, &ILocalUser_t273_1_0_0/* this_arg */
	, &ILocalUser_t273_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserState
extern TypeInfo UserState_t243_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserStateMethodDeclarations.h"
static const EncodedMethodIndex UserState_t243_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair UserState_t243_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserState_t243_0_0_0;
extern const Il2CppType UserState_t243_1_0_0;
const Il2CppTypeDefinitionMetadata UserState_t243_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserState_t243_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, UserState_t243_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1015/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UserState_t243_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserState"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserState_t243_0_0_0/* byval_arg */
	, &UserState_t243_1_0_0/* this_arg */
	, &UserState_t243_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserState_t243)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserState_t243)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IUserProfile
extern TypeInfo IUserProfile_t1805_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IUserProfile_t1805_1_0_0;
struct IUserProfile_t1805;
const Il2CppTypeDefinitionMetadata IUserProfile_t1805_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IUserProfile_t1805_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUserProfile"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &IUserProfile_t1805_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IUserProfile_t1805_0_0_0/* byval_arg */
	, &IUserProfile_t1805_1_0_0/* this_arg */
	, &IUserProfile_t1805_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievement
extern TypeInfo IAchievement_t277_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IAchievement_t277_1_0_0;
struct IAchievement_t277;
const Il2CppTypeDefinitionMetadata IAchievement_t277_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IAchievement_t277_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievement"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &IAchievement_t277_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievement_t277_0_0_0/* byval_arg */
	, &IAchievement_t277_1_0_0/* this_arg */
	, &IAchievement_t277_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
