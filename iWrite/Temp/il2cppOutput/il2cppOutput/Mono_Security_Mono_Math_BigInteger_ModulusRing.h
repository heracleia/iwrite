﻿#pragma once
#include <stdint.h>
// Mono.Math.BigInteger
struct BigInteger_t625;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Math.BigInteger/ModulusRing
struct  ModulusRing_t626  : public Object_t
{
	// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::mod
	BigInteger_t625 * ___mod_0;
	// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::constant
	BigInteger_t625 * ___constant_1;
};
