﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
struct Enumerator_t322;
// System.Object
struct Object_t;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t58;
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
struct List_1_t59;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1MethodDeclarations.h"
#define Enumerator__ctor_m8728(__this, ___l, method) (( void (*) (Enumerator_t322 *, List_1_t59 *, const MethodInfo*))Enumerator__ctor_m8428_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m8729(__this, method) (( Object_t * (*) (Enumerator_t322 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::Dispose()
#define Enumerator_Dispose_m8730(__this, method) (( void (*) (Enumerator_t322 *, const MethodInfo*))Enumerator_Dispose_m8430_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::VerifyState()
#define Enumerator_VerifyState_m8731(__this, method) (( void (*) (Enumerator_t322 *, const MethodInfo*))Enumerator_VerifyState_m8431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::MoveNext()
#define Enumerator_MoveNext_m1183(__this, method) (( bool (*) (Enumerator_t322 *, const MethodInfo*))Enumerator_MoveNext_m8432_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::get_Current()
#define Enumerator_get_Current_m1182(__this, method) (( GUILayoutEntry_t58 * (*) (Enumerator_t322 *, const MethodInfo*))Enumerator_get_Current_m8433_gshared)(__this, method)
