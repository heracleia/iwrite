﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t1540;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C" void DefaultComparer__ctor_m9471_gshared (DefaultComparer_t1540 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m9471(__this, method) (( void (*) (DefaultComparer_t1540 *, const MethodInfo*))DefaultComparer__ctor_m9471_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m9472_gshared (DefaultComparer_t1540 * __this, UIVertex_t158  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m9472(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1540 *, UIVertex_t158 , const MethodInfo*))DefaultComparer_GetHashCode_m9472_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m9473_gshared (DefaultComparer_t1540 * __this, UIVertex_t158  ___x, UIVertex_t158  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m9473(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1540 *, UIVertex_t158 , UIVertex_t158 , const MethodInfo*))DefaultComparer_Equals_m9473_gshared)(__this, ___x, ___y, method)
