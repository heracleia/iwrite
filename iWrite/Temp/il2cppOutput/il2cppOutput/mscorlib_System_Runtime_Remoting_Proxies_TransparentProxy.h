﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.Proxies.RealProxy
struct RealProxy_t1109;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Proxies.TransparentProxy
struct  TransparentProxy_t1110  : public Object_t
{
	// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.Proxies.TransparentProxy::_rp
	RealProxy_t1109 * ____rp_0;
};
