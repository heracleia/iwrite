﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Single>
struct InternalEnumerator_1_t1535;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m9410_gshared (InternalEnumerator_1_t1535 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m9410(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1535 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m9410_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9411_gshared (InternalEnumerator_1_t1535 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9411(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1535 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9411_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m9412_gshared (InternalEnumerator_1_t1535 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m9412(__this, method) (( void (*) (InternalEnumerator_1_t1535 *, const MethodInfo*))InternalEnumerator_1_Dispose_m9412_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m9413_gshared (InternalEnumerator_1_t1535 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m9413(__this, method) (( bool (*) (InternalEnumerator_1_t1535 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m9413_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern "C" float InternalEnumerator_1_get_Current_m9414_gshared (InternalEnumerator_1_t1535 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m9414(__this, method) (( float (*) (InternalEnumerator_1_t1535 *, const MethodInfo*))InternalEnumerator_1_get_Current_m9414_gshared)(__this, method)
