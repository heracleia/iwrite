﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
struct KeyValuePair_2_t1552;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m9679_gshared (KeyValuePair_2_t1552 * __this, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m9679(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1552 *, Object_t *, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m9679_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m9680_gshared (KeyValuePair_2_t1552 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m9680(__this, method) (( Object_t * (*) (KeyValuePair_2_t1552 *, const MethodInfo*))KeyValuePair_2_get_Key_m9680_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m9681_gshared (KeyValuePair_2_t1552 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m9681(__this, ___value, method) (( void (*) (KeyValuePair_2_t1552 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m9681_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Value()
extern "C" int64_t KeyValuePair_2_get_Value_m9682_gshared (KeyValuePair_2_t1552 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m9682(__this, method) (( int64_t (*) (KeyValuePair_2_t1552 *, const MethodInfo*))KeyValuePair_2_get_Value_m9682_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m9683_gshared (KeyValuePair_2_t1552 * __this, int64_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m9683(__this, ___value, method) (( void (*) (KeyValuePair_2_t1552 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m9683_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m9684_gshared (KeyValuePair_2_t1552 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m9684(__this, method) (( String_t* (*) (KeyValuePair_2_t1552 *, const MethodInfo*))KeyValuePair_2_ToString_m9684_gshared)(__this, method)
