﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct List_1_t24;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t27;
// System.Collections.Generic.IEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct IEnumerator_1_t1806;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct GcLeaderboardU5BU5D_t1449;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
#define List_1__ctor_m1165(__this, method) (( void (*) (List_1_t24 *, const MethodInfo*))List_1__ctor_m1259_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::.ctor(System.Int32)
#define List_1__ctor_m8365(__this, ___capacity, method) (( void (*) (List_1_t24 *, int32_t, const MethodInfo*))List_1__ctor_m8366_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::.cctor()
#define List_1__cctor_m8367(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m8368_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m8369(__this, method) (( Object_t* (*) (List_1_t24 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m8370_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m8371(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t24 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m8372_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m8373(__this, method) (( Object_t * (*) (List_1_t24 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m8374_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m8375(__this, ___item, method) (( int32_t (*) (List_1_t24 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m8376_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m8377(__this, ___item, method) (( bool (*) (List_1_t24 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m8378_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m8379(__this, ___item, method) (( int32_t (*) (List_1_t24 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m8380_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m8381(__this, ___index, ___item, method) (( void (*) (List_1_t24 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m8382_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m8383(__this, ___item, method) (( void (*) (List_1_t24 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m8384_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m8385(__this, method) (( bool (*) (List_1_t24 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m8386_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m8387(__this, method) (( Object_t * (*) (List_1_t24 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m8388_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m8389(__this, ___index, method) (( Object_t * (*) (List_1_t24 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m8390_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m8391(__this, ___index, ___value, method) (( void (*) (List_1_t24 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m8392_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Add(T)
#define List_1_Add_m8393(__this, ___item, method) (( void (*) (List_1_t24 *, GcLeaderboard_t27 *, const MethodInfo*))List_1_Add_m8394_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m8395(__this, ___newCount, method) (( void (*) (List_1_t24 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m8396_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Clear()
#define List_1_Clear_m8397(__this, method) (( void (*) (List_1_t24 *, const MethodInfo*))List_1_Clear_m8398_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Contains(T)
#define List_1_Contains_m8399(__this, ___item, method) (( bool (*) (List_1_t24 *, GcLeaderboard_t27 *, const MethodInfo*))List_1_Contains_m8400_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m8401(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t24 *, GcLeaderboardU5BU5D_t1449*, int32_t, const MethodInfo*))List_1_CopyTo_m8402_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::GetEnumerator()
#define List_1_GetEnumerator_m1171(__this, method) (( Enumerator_t318  (*) (List_1_t24 *, const MethodInfo*))List_1_GetEnumerator_m8403_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::IndexOf(T)
#define List_1_IndexOf_m8404(__this, ___item, method) (( int32_t (*) (List_1_t24 *, GcLeaderboard_t27 *, const MethodInfo*))List_1_IndexOf_m8405_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m8406(__this, ___start, ___delta, method) (( void (*) (List_1_t24 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m8407_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m8408(__this, ___index, method) (( void (*) (List_1_t24 *, int32_t, const MethodInfo*))List_1_CheckIndex_m8409_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Insert(System.Int32,T)
#define List_1_Insert_m8410(__this, ___index, ___item, method) (( void (*) (List_1_t24 *, int32_t, GcLeaderboard_t27 *, const MethodInfo*))List_1_Insert_m8411_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Remove(T)
#define List_1_Remove_m8412(__this, ___item, method) (( bool (*) (List_1_t24 *, GcLeaderboard_t27 *, const MethodInfo*))List_1_Remove_m8413_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m8414(__this, ___index, method) (( void (*) (List_1_t24 *, int32_t, const MethodInfo*))List_1_RemoveAt_m8415_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::ToArray()
#define List_1_ToArray_m8416(__this, method) (( GcLeaderboardU5BU5D_t1449* (*) (List_1_t24 *, const MethodInfo*))List_1_ToArray_m8417_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::get_Capacity()
#define List_1_get_Capacity_m8418(__this, method) (( int32_t (*) (List_1_t24 *, const MethodInfo*))List_1_get_Capacity_m8419_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m8420(__this, ___value, method) (( void (*) (List_1_t24 *, int32_t, const MethodInfo*))List_1_set_Capacity_m8421_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::get_Count()
#define List_1_get_Count_m8422(__this, method) (( int32_t (*) (List_1_t24 *, const MethodInfo*))List_1_get_Count_m8423_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::get_Item(System.Int32)
#define List_1_get_Item_m8424(__this, ___index, method) (( GcLeaderboard_t27 * (*) (List_1_t24 *, int32_t, const MethodInfo*))List_1_get_Item_m8425_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::set_Item(System.Int32,T)
#define List_1_set_Item_m8426(__this, ___index, ___value, method) (( void (*) (List_1_t24 *, int32_t, GcLeaderboard_t27 *, const MethodInfo*))List_1_set_Item_m8427_gshared)(__this, ___index, ___value, method)
