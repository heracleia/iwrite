﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct List_1_t261;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t256;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Events.BaseInvokableCall>
struct IEnumerator_1_t1920;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t1682;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
#define List_1__ctor_m1334(__this, method) (( void (*) (List_1_t261 *, const MethodInfo*))List_1__ctor_m1259_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Int32)
#define List_1__ctor_m11217(__this, ___capacity, method) (( void (*) (List_1_t261 *, int32_t, const MethodInfo*))List_1__ctor_m8366_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.cctor()
#define List_1__cctor_m11218(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m8368_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11219(__this, method) (( Object_t* (*) (List_1_t261 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m8370_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m11220(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t261 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m8372_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m11221(__this, method) (( Object_t * (*) (List_1_t261 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m8374_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m11222(__this, ___item, method) (( int32_t (*) (List_1_t261 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m8376_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m11223(__this, ___item, method) (( bool (*) (List_1_t261 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m8378_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m11224(__this, ___item, method) (( int32_t (*) (List_1_t261 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m8380_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m11225(__this, ___index, ___item, method) (( void (*) (List_1_t261 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m8382_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m11226(__this, ___item, method) (( void (*) (List_1_t261 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m8384_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11227(__this, method) (( bool (*) (List_1_t261 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m8386_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m11228(__this, method) (( Object_t * (*) (List_1_t261 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m8388_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m11229(__this, ___index, method) (( Object_t * (*) (List_1_t261 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m8390_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m11230(__this, ___index, ___value, method) (( void (*) (List_1_t261 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m8392_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(T)
#define List_1_Add_m11231(__this, ___item, method) (( void (*) (List_1_t261 *, BaseInvokableCall_t256 *, const MethodInfo*))List_1_Add_m8394_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m11232(__this, ___newCount, method) (( void (*) (List_1_t261 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m8396_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear()
#define List_1_Clear_m11233(__this, method) (( void (*) (List_1_t261 *, const MethodInfo*))List_1_Clear_m8398_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Contains(T)
#define List_1_Contains_m11234(__this, ___item, method) (( bool (*) (List_1_t261 *, BaseInvokableCall_t256 *, const MethodInfo*))List_1_Contains_m8400_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m11235(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t261 *, BaseInvokableCallU5BU5D_t1682*, int32_t, const MethodInfo*))List_1_CopyTo_m8402_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GetEnumerator()
#define List_1_GetEnumerator_m11236(__this, method) (( Enumerator_t1683  (*) (List_1_t261 *, const MethodInfo*))List_1_GetEnumerator_m8403_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::IndexOf(T)
#define List_1_IndexOf_m11237(__this, ___item, method) (( int32_t (*) (List_1_t261 *, BaseInvokableCall_t256 *, const MethodInfo*))List_1_IndexOf_m8405_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m11238(__this, ___start, ___delta, method) (( void (*) (List_1_t261 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m8407_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m11239(__this, ___index, method) (( void (*) (List_1_t261 *, int32_t, const MethodInfo*))List_1_CheckIndex_m8409_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Insert(System.Int32,T)
#define List_1_Insert_m11240(__this, ___index, ___item, method) (( void (*) (List_1_t261 *, int32_t, BaseInvokableCall_t256 *, const MethodInfo*))List_1_Insert_m8411_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Remove(T)
#define List_1_Remove_m11241(__this, ___item, method) (( bool (*) (List_1_t261 *, BaseInvokableCall_t256 *, const MethodInfo*))List_1_Remove_m8413_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m11242(__this, ___index, method) (( void (*) (List_1_t261 *, int32_t, const MethodInfo*))List_1_RemoveAt_m8415_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::ToArray()
#define List_1_ToArray_m11243(__this, method) (( BaseInvokableCallU5BU5D_t1682* (*) (List_1_t261 *, const MethodInfo*))List_1_ToArray_m8417_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Capacity()
#define List_1_get_Capacity_m11244(__this, method) (( int32_t (*) (List_1_t261 *, const MethodInfo*))List_1_get_Capacity_m8419_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m11245(__this, ___value, method) (( void (*) (List_1_t261 *, int32_t, const MethodInfo*))List_1_set_Capacity_m8421_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count()
#define List_1_get_Count_m11246(__this, method) (( int32_t (*) (List_1_t261 *, const MethodInfo*))List_1_get_Count_m8423_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32)
#define List_1_get_Item_m11247(__this, ___index, method) (( BaseInvokableCall_t256 * (*) (List_1_t261 *, int32_t, const MethodInfo*))List_1_get_Item_m8425_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::set_Item(System.Int32,T)
#define List_1_set_Item_m11248(__this, ___index, ___value, method) (( void (*) (List_1_t261 *, int32_t, BaseInvokableCall_t256 *, const MethodInfo*))List_1_set_Item_m8427_gshared)(__this, ___index, ___value, method)
