﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t385;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.SoapServices
struct  SoapServices_t1138  : public Object_t
{
};
struct SoapServices_t1138_StaticFields{
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices::_xmlTypes
	Hashtable_t385 * ____xmlTypes_0;
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices::_xmlElements
	Hashtable_t385 * ____xmlElements_1;
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices::_soapActions
	Hashtable_t385 * ____soapActions_2;
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices::_soapActionsMethods
	Hashtable_t385 * ____soapActionsMethods_3;
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices::_typeInfos
	Hashtable_t385 * ____typeInfos_4;
};
