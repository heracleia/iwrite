﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t1148;
// System.Type
struct Type_t;
// System.String
struct String_t;

// System.Void System.Runtime.Serialization.SerializationBinder::.ctor()
extern "C" void SerializationBinder__ctor_m6605 (SerializationBinder_t1148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
