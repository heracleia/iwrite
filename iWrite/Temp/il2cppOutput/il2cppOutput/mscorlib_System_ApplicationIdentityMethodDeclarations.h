﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ApplicationIdentity
struct ApplicationIdentity_t1288;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t310;
// System.String
struct String_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.ApplicationIdentity::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m7461 (ApplicationIdentity_t1288 * __this, SerializationInfo_t310 * ___info, StreamingContext_t311  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ApplicationIdentity::ToString()
extern "C" String_t* ApplicationIdentity_ToString_m7462 (ApplicationIdentity_t1288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
