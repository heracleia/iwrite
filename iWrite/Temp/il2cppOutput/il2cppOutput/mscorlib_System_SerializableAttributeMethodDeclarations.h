﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.SerializableAttribute
struct SerializableAttribute_t758;

// System.Void System.SerializableAttribute::.ctor()
extern "C" void SerializableAttribute__ctor_m3412 (SerializableAttribute_t758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
