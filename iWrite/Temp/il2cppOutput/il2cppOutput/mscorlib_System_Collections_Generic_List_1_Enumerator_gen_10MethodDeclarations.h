﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>
struct Enumerator_t1681;
// System.Object
struct Object_t;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t258;
// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct List_1_t259;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1MethodDeclarations.h"
#define Enumerator__ctor_m11211(__this, ___l, method) (( void (*) (Enumerator_t1681 *, List_1_t259 *, const MethodInfo*))Enumerator__ctor_m8428_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m11212(__this, method) (( Object_t * (*) (Enumerator_t1681 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::Dispose()
#define Enumerator_Dispose_m11213(__this, method) (( void (*) (Enumerator_t1681 *, const MethodInfo*))Enumerator_Dispose_m8430_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::VerifyState()
#define Enumerator_VerifyState_m11214(__this, method) (( void (*) (Enumerator_t1681 *, const MethodInfo*))Enumerator_VerifyState_m8431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::MoveNext()
#define Enumerator_MoveNext_m11215(__this, method) (( bool (*) (Enumerator_t1681 *, const MethodInfo*))Enumerator_MoveNext_m8432_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::get_Current()
#define Enumerator_get_Current_m11216(__this, method) (( PersistentCall_t258 * (*) (Enumerator_t1681 *, const MethodInfo*))Enumerator_get_Current_m8433_gshared)(__this, method)
