﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
struct Enumerator_t1500;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1492;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m8922_gshared (Enumerator_t1500 * __this, Dictionary_2_t1492 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m8922(__this, ___host, method) (( void (*) (Enumerator_t1500 *, Dictionary_2_t1492 *, const MethodInfo*))Enumerator__ctor_m8922_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m8923_gshared (Enumerator_t1500 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m8923(__this, method) (( Object_t * (*) (Enumerator_t1500 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8923_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m8924_gshared (Enumerator_t1500 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m8924(__this, method) (( void (*) (Enumerator_t1500 *, const MethodInfo*))Enumerator_Dispose_m8924_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m8925_gshared (Enumerator_t1500 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m8925(__this, method) (( bool (*) (Enumerator_t1500 *, const MethodInfo*))Enumerator_MoveNext_m8925_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m8926_gshared (Enumerator_t1500 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m8926(__this, method) (( Object_t * (*) (Enumerator_t1500 *, const MethodInfo*))Enumerator_get_Current_m8926_gshared)(__this, method)
