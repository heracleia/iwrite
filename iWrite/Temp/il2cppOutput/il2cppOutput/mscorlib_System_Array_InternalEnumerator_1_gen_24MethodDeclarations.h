﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Display>
struct InternalEnumerator_1_t1532;
// System.Object
struct Object_t;
// UnityEngine.Display
struct Display_t115;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m9400(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1532 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8302_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Display>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9401(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1532 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::Dispose()
#define InternalEnumerator_1_Dispose_m9402(__this, method) (( void (*) (InternalEnumerator_1_t1532 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8306_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Display>::MoveNext()
#define InternalEnumerator_1_MoveNext_m9403(__this, method) (( bool (*) (InternalEnumerator_1_t1532 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8308_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Display>::get_Current()
#define InternalEnumerator_1_get_Current_m9404(__this, method) (( Display_t115 * (*) (InternalEnumerator_1_t1532 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8310_gshared)(__this, method)
