﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>
struct GenericEqualityComparer_1_t1705;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m11452_gshared (GenericEqualityComparer_1_t1705 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m11452(__this, method) (( void (*) (GenericEqualityComparer_1_t1705 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m11452_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m11453_gshared (GenericEqualityComparer_1_t1705 * __this, bool ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m11453(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1705 *, bool, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m11453_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m11454_gshared (GenericEqualityComparer_1_t1705 * __this, bool ___x, bool ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m11454(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1705 *, bool, bool, const MethodInfo*))GenericEqualityComparer_1_Equals_m11454_gshared)(__this, ___x, ___y, method)
