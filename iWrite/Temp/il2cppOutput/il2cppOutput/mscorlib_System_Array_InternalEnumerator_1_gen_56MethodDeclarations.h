﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Uri/UriScheme>
struct InternalEnumerator_1_t1730;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"

// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11665_gshared (InternalEnumerator_1_t1730 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11665(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1730 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11665_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11666_gshared (InternalEnumerator_1_t1730 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11666(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1730 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11666_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11667_gshared (InternalEnumerator_1_t1730 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11667(__this, method) (( void (*) (InternalEnumerator_1_t1730 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11667_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11668_gshared (InternalEnumerator_1_t1730 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11668(__this, method) (( bool (*) (InternalEnumerator_1_t1730 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11668_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
extern "C" UriScheme_t548  InternalEnumerator_1_get_Current_m11669_gshared (InternalEnumerator_1_t1730 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11669(__this, method) (( UriScheme_t548  (*) (InternalEnumerator_1_t1730 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11669_gshared)(__this, method)
