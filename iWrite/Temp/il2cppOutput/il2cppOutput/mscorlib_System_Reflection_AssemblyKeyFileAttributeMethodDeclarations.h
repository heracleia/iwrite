﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyKeyFileAttribute
struct AssemblyKeyFileAttribute_t993;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyKeyFileAttribute::.ctor(System.String)
extern "C" void AssemblyKeyFileAttribute__ctor_m5817 (AssemblyKeyFileAttribute_t993 * __this, String_t* ___keyFile, const MethodInfo* method) IL2CPP_METHOD_ATTR;
