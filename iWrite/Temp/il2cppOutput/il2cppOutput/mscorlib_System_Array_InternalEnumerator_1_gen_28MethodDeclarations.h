﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UIVertex>
struct InternalEnumerator_1_t1537;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m9423_gshared (InternalEnumerator_1_t1537 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m9423(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1537 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m9423_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9424_gshared (InternalEnumerator_1_t1537 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9424(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1537 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9424_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m9425_gshared (InternalEnumerator_1_t1537 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m9425(__this, method) (( void (*) (InternalEnumerator_1_t1537 *, const MethodInfo*))InternalEnumerator_1_Dispose_m9425_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m9426_gshared (InternalEnumerator_1_t1537 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m9426(__this, method) (( bool (*) (InternalEnumerator_1_t1537 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m9426_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::get_Current()
extern "C" UIVertex_t158  InternalEnumerator_1_get_Current_m9427_gshared (InternalEnumerator_1_t1537 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m9427(__this, method) (( UIVertex_t158  (*) (InternalEnumerator_1_t1537 *, const MethodInfo*))InternalEnumerator_1_get_Current_m9427_gshared)(__this, method)
