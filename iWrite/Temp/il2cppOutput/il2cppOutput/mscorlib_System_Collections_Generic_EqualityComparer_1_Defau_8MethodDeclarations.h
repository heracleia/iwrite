﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>
struct DefaultComparer_t1706;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>::.ctor()
extern "C" void DefaultComparer__ctor_m11455_gshared (DefaultComparer_t1706 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m11455(__this, method) (( void (*) (DefaultComparer_t1706 *, const MethodInfo*))DefaultComparer__ctor_m11455_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m11456_gshared (DefaultComparer_t1706 * __this, bool ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m11456(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1706 *, bool, const MethodInfo*))DefaultComparer_GetHashCode_m11456_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m11457_gshared (DefaultComparer_t1706 * __this, bool ___x, bool ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m11457(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1706 *, bool, bool, const MethodInfo*))DefaultComparer_Equals_m11457_gshared)(__this, ___x, ___y, method)
