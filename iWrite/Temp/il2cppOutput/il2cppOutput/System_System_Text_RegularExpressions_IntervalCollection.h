﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t395;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.IntervalCollection
struct  IntervalCollection_t522  : public Object_t
{
	// System.Collections.ArrayList System.Text.RegularExpressions.IntervalCollection::intervals
	ArrayList_t395 * ___intervals_0;
};
