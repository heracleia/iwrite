﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Enumerator_t1666;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1661;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m11066_gshared (Enumerator_t1666 * __this, Dictionary_2_t1661 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m11066(__this, ___host, method) (( void (*) (Enumerator_t1666 *, Dictionary_2_t1661 *, const MethodInfo*))Enumerator__ctor_m11066_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m11067_gshared (Enumerator_t1666 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m11067(__this, method) (( Object_t * (*) (Enumerator_t1666 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11067_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m11068_gshared (Enumerator_t1666 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m11068(__this, method) (( void (*) (Enumerator_t1666 *, const MethodInfo*))Enumerator_Dispose_m11068_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m11069_gshared (Enumerator_t1666 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m11069(__this, method) (( bool (*) (Enumerator_t1666 *, const MethodInfo*))Enumerator_MoveNext_m11069_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m11070_gshared (Enumerator_t1666 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m11070(__this, method) (( Object_t * (*) (Enumerator_t1666 *, const MethodInfo*))Enumerator_get_Current_m11070_gshared)(__this, method)
