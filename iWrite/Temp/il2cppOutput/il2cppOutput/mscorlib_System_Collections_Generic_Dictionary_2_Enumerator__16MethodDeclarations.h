﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
struct Enumerator_t1697;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1691;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m11395_gshared (Enumerator_t1697 * __this, Dictionary_2_t1691 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m11395(__this, ___dictionary, method) (( void (*) (Enumerator_t1697 *, Dictionary_2_t1691 *, const MethodInfo*))Enumerator__ctor_m11395_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m11396_gshared (Enumerator_t1697 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m11396(__this, method) (( Object_t * (*) (Enumerator_t1697 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11396_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t560  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11397_gshared (Enumerator_t1697 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11397(__this, method) (( DictionaryEntry_t560  (*) (Enumerator_t1697 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11397_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11398_gshared (Enumerator_t1697 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11398(__this, method) (( Object_t * (*) (Enumerator_t1697 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11398_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11399_gshared (Enumerator_t1697 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11399(__this, method) (( Object_t * (*) (Enumerator_t1697 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11399_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m11400_gshared (Enumerator_t1697 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m11400(__this, method) (( bool (*) (Enumerator_t1697 *, const MethodInfo*))Enumerator_MoveNext_m11400_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" KeyValuePair_2_t1692  Enumerator_get_Current_m11401_gshared (Enumerator_t1697 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m11401(__this, method) (( KeyValuePair_2_t1692  (*) (Enumerator_t1697 *, const MethodInfo*))Enumerator_get_Current_m11401_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m11402_gshared (Enumerator_t1697 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m11402(__this, method) (( Object_t * (*) (Enumerator_t1697 *, const MethodInfo*))Enumerator_get_CurrentKey_m11402_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C" bool Enumerator_get_CurrentValue_m11403_gshared (Enumerator_t1697 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m11403(__this, method) (( bool (*) (Enumerator_t1697 *, const MethodInfo*))Enumerator_get_CurrentValue_m11403_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern "C" void Enumerator_VerifyState_m11404_gshared (Enumerator_t1697 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m11404(__this, method) (( void (*) (Enumerator_t1697 *, const MethodInfo*))Enumerator_VerifyState_m11404_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m11405_gshared (Enumerator_t1697 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m11405(__this, method) (( void (*) (Enumerator_t1697 *, const MethodInfo*))Enumerator_VerifyCurrent_m11405_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m11406_gshared (Enumerator_t1697 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m11406(__this, method) (( void (*) (Enumerator_t1697 *, const MethodInfo*))Enumerator_Dispose_m11406_gshared)(__this, method)
