﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Security.Policy.StrongName>
struct List_1_t1430;
// System.Object
struct Object_t;
// System.Security.Policy.StrongName
struct StrongName_t1226;
// System.Collections.Generic.IEnumerator`1<System.Security.Policy.StrongName>
struct IEnumerator_1_t1934;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t1786;
// System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_12.h"

// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
#define List_1__ctor_m11975(__this, method) (( void (*) (List_1_t1430 *, const MethodInfo*))List_1__ctor_m1259_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::.ctor(System.Int32)
#define List_1__ctor_m8289(__this, ___capacity, method) (( void (*) (List_1_t1430 *, int32_t, const MethodInfo*))List_1__ctor_m8366_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::.cctor()
#define List_1__cctor_m11976(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m8368_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11977(__this, method) (( Object_t* (*) (List_1_t1430 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m8370_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m11978(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1430 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m8372_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m11979(__this, method) (( Object_t * (*) (List_1_t1430 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m8374_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m11980(__this, ___item, method) (( int32_t (*) (List_1_t1430 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m8376_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m11981(__this, ___item, method) (( bool (*) (List_1_t1430 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m8378_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m11982(__this, ___item, method) (( int32_t (*) (List_1_t1430 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m8380_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m11983(__this, ___index, ___item, method) (( void (*) (List_1_t1430 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m8382_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m11984(__this, ___item, method) (( void (*) (List_1_t1430 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m8384_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11985(__this, method) (( bool (*) (List_1_t1430 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m8386_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m11986(__this, method) (( Object_t * (*) (List_1_t1430 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m8388_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m11987(__this, ___index, method) (( Object_t * (*) (List_1_t1430 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m8390_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m11988(__this, ___index, ___value, method) (( void (*) (List_1_t1430 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m8392_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Add(T)
#define List_1_Add_m11989(__this, ___item, method) (( void (*) (List_1_t1430 *, StrongName_t1226 *, const MethodInfo*))List_1_Add_m8394_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m11990(__this, ___newCount, method) (( void (*) (List_1_t1430 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m8396_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Clear()
#define List_1_Clear_m11991(__this, method) (( void (*) (List_1_t1430 *, const MethodInfo*))List_1_Clear_m8398_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Contains(T)
#define List_1_Contains_m11992(__this, ___item, method) (( bool (*) (List_1_t1430 *, StrongName_t1226 *, const MethodInfo*))List_1_Contains_m8400_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m11993(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1430 *, StrongNameU5BU5D_t1786*, int32_t, const MethodInfo*))List_1_CopyTo_m8402_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Security.Policy.StrongName>::GetEnumerator()
#define List_1_GetEnumerator_m11994(__this, method) (( Enumerator_t1787  (*) (List_1_t1430 *, const MethodInfo*))List_1_GetEnumerator_m8403_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::IndexOf(T)
#define List_1_IndexOf_m11995(__this, ___item, method) (( int32_t (*) (List_1_t1430 *, StrongName_t1226 *, const MethodInfo*))List_1_IndexOf_m8405_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m11996(__this, ___start, ___delta, method) (( void (*) (List_1_t1430 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m8407_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m11997(__this, ___index, method) (( void (*) (List_1_t1430 *, int32_t, const MethodInfo*))List_1_CheckIndex_m8409_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Insert(System.Int32,T)
#define List_1_Insert_m11998(__this, ___index, ___item, method) (( void (*) (List_1_t1430 *, int32_t, StrongName_t1226 *, const MethodInfo*))List_1_Insert_m8411_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Remove(T)
#define List_1_Remove_m11999(__this, ___item, method) (( bool (*) (List_1_t1430 *, StrongName_t1226 *, const MethodInfo*))List_1_Remove_m8413_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m12000(__this, ___index, method) (( void (*) (List_1_t1430 *, int32_t, const MethodInfo*))List_1_RemoveAt_m8415_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.Security.Policy.StrongName>::ToArray()
#define List_1_ToArray_m12001(__this, method) (( StrongNameU5BU5D_t1786* (*) (List_1_t1430 *, const MethodInfo*))List_1_ToArray_m8417_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::get_Capacity()
#define List_1_get_Capacity_m12002(__this, method) (( int32_t (*) (List_1_t1430 *, const MethodInfo*))List_1_get_Capacity_m8419_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m12003(__this, ___value, method) (( void (*) (List_1_t1430 *, int32_t, const MethodInfo*))List_1_set_Capacity_m8421_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::get_Count()
#define List_1_get_Count_m12004(__this, method) (( int32_t (*) (List_1_t1430 *, const MethodInfo*))List_1_get_Count_m8423_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Security.Policy.StrongName>::get_Item(System.Int32)
#define List_1_get_Item_m12005(__this, ___index, method) (( StrongName_t1226 * (*) (List_1_t1430 *, int32_t, const MethodInfo*))List_1_get_Item_m8425_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::set_Item(System.Int32,T)
#define List_1_set_Item_m12006(__this, ___index, ___value, method) (( void (*) (List_1_t1430 *, int32_t, StrongName_t1226 *, const MethodInfo*))List_1_set_Item_m8427_gshared)(__this, ___index, ___value, method)
