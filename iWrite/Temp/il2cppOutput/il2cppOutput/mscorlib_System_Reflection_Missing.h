﻿#pragma once
#include <stdint.h>
// System.Reflection.Missing
struct Missing_t1009;
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.Missing
struct  Missing_t1009  : public Object_t
{
};
struct Missing_t1009_StaticFields{
	// System.Reflection.Missing System.Reflection.Missing::Value
	Missing_t1009 * ___Value_0;
};
