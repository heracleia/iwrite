﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ThreadSafeDictionary_2_t353;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t1872;
// System.Collections.Generic.ICollection`1<SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ICollection_1_t1873;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t201;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ThreadSafeDictionaryValueFactory_2_t352;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
struct KeyValuePair_2U5BU5D_t1874;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>>
struct IEnumerator_1_t1875;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_6MethodDeclarations.h"
#define ThreadSafeDictionary_2__ctor_m1289(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t353 *, ThreadSafeDictionaryValueFactory_2_t352 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m10275_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m10276(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t353 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m10277_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Get(TKey)
#define ThreadSafeDictionary_2_Get_m10278(__this, ___key, method) (( ConstructorDelegate_t201 * (*) (ThreadSafeDictionary_2_t353 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m10279_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::AddValue(TKey)
#define ThreadSafeDictionary_2_AddValue_m10280(__this, ___key, method) (( ConstructorDelegate_t201 * (*) (ThreadSafeDictionary_2_t353 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m10281_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(TKey,TValue)
#define ThreadSafeDictionary_2_Add_m10282(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t353 *, Type_t *, ConstructorDelegate_t201 *, const MethodInfo*))ThreadSafeDictionary_2_Add_m10283_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Keys()
#define ThreadSafeDictionary_2_get_Keys_m10284(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t353 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m10285_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::TryGetValue(TKey,TValue&)
#define ThreadSafeDictionary_2_TryGetValue_m10286(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t353 *, Type_t *, ConstructorDelegate_t201 **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m10287_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Values()
#define ThreadSafeDictionary_2_get_Values_m10288(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t353 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m10289_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Item(TKey)
#define ThreadSafeDictionary_2_get_Item_m10290(__this, ___key, method) (( ConstructorDelegate_t201 * (*) (ThreadSafeDictionary_2_t353 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m10291_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Item(TKey,TValue)
#define ThreadSafeDictionary_2_set_Item_m10292(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t353 *, Type_t *, ConstructorDelegate_t201 *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m10293_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Add_m10294(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t353 *, KeyValuePair_2_t1609 , const MethodInfo*))ThreadSafeDictionary_2_Add_m10295_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Clear()
#define ThreadSafeDictionary_2_Clear_m10296(__this, method) (( void (*) (ThreadSafeDictionary_2_t353 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m10297_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Contains_m10298(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t353 *, KeyValuePair_2_t1609 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m10299_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ThreadSafeDictionary_2_CopyTo_m10300(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t353 *, KeyValuePair_2U5BU5D_t1874*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m10301_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Count()
#define ThreadSafeDictionary_2_get_Count_m10302(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t353 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m10303_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_IsReadOnly()
#define ThreadSafeDictionary_2_get_IsReadOnly_m10304(__this, method) (( bool (*) (ThreadSafeDictionary_2_t353 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m10305_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Remove_m10306(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t353 *, KeyValuePair_2_t1609 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m10307_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetEnumerator()
#define ThreadSafeDictionary_2_GetEnumerator_m10308(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t353 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m10309_gshared)(__this, method)
