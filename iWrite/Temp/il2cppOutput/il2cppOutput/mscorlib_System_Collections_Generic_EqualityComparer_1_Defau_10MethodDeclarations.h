﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t1795;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C" void DefaultComparer__ctor_m12044_gshared (DefaultComparer_t1795 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m12044(__this, method) (( void (*) (DefaultComparer_t1795 *, const MethodInfo*))DefaultComparer__ctor_m12044_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m12045_gshared (DefaultComparer_t1795 * __this, DateTimeOffset_t363  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m12045(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1795 *, DateTimeOffset_t363 , const MethodInfo*))DefaultComparer_GetHashCode_m12045_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m12046_gshared (DefaultComparer_t1795 * __this, DateTimeOffset_t363  ___x, DateTimeOffset_t363  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m12046(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1795 *, DateTimeOffset_t363 , DateTimeOffset_t363 , const MethodInfo*))DefaultComparer_Equals_m12046_gshared)(__this, ___x, ___y, method)
