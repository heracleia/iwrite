﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct KeyValuePair_2_t365;
// System.String
struct String_t;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_t198;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m10567(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t365 *, String_t*, GetDelegate_t198 *, const MethodInfo*))KeyValuePair_2__ctor_m8869_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m1304(__this, method) (( String_t* (*) (KeyValuePair_2_t365 *, const MethodInfo*))KeyValuePair_2_get_Key_m8870_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m10568(__this, ___value, method) (( void (*) (KeyValuePair_2_t365 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m8871_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m1303(__this, method) (( GetDelegate_t198 * (*) (KeyValuePair_2_t365 *, const MethodInfo*))KeyValuePair_2_get_Value_m8872_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m10569(__this, ___value, method) (( void (*) (KeyValuePair_2_t365 *, GetDelegate_t198 *, const MethodInfo*))KeyValuePair_2_set_Value_m8873_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ToString()
#define KeyValuePair_2_ToString_m10570(__this, method) (( String_t* (*) (KeyValuePair_2_t365 *, const MethodInfo*))KeyValuePair_2_ToString_m8874_gshared)(__this, method)
