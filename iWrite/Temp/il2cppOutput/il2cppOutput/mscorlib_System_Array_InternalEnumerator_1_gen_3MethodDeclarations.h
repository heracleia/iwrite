﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievement>
struct InternalEnumerator_1_t1444;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t277;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievement>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m8334(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1444 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8302_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievement>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8335(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1444 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievement>::Dispose()
#define InternalEnumerator_1_Dispose_m8336(__this, method) (( void (*) (InternalEnumerator_1_t1444 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8306_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievement>::MoveNext()
#define InternalEnumerator_1_MoveNext_m8337(__this, method) (( bool (*) (InternalEnumerator_1_t1444 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8308_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievement>::get_Current()
#define InternalEnumerator_1_get_Current_m8338(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1444 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8310_gshared)(__this, method)
