﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.MonoIOStat
struct MonoIOStat_t928;
struct MonoIOStat_t928_marshaled;

void MonoIOStat_t928_marshal(const MonoIOStat_t928& unmarshaled, MonoIOStat_t928_marshaled& marshaled);
void MonoIOStat_t928_marshal_back(const MonoIOStat_t928_marshaled& marshaled, MonoIOStat_t928& unmarshaled);
void MonoIOStat_t928_marshal_cleanup(MonoIOStat_t928_marshaled& marshaled);
