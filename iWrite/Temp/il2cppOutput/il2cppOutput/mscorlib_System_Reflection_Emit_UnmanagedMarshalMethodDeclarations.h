﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t955;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t773;

// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.Emit.UnmanagedMarshal::ToMarshalAsAttribute()
extern "C" MarshalAsAttribute_t773 * UnmanagedMarshal_ToMarshalAsAttribute_m5779 (UnmanagedMarshal_t955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
