﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.CaseInsensitiveComparer
struct CaseInsensitiveComparer_t571;
// System.Object
struct Object_t;

// System.Void System.Collections.CaseInsensitiveComparer::.ctor()
extern "C" void CaseInsensitiveComparer__ctor_m4840 (CaseInsensitiveComparer_t571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.CaseInsensitiveComparer::.ctor(System.Boolean)
extern "C" void CaseInsensitiveComparer__ctor_m4841 (CaseInsensitiveComparer_t571 * __this, bool ___invariant, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.CaseInsensitiveComparer::.cctor()
extern "C" void CaseInsensitiveComparer__cctor_m4842 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::get_DefaultInvariant()
extern "C" CaseInsensitiveComparer_t571 * CaseInsensitiveComparer_get_DefaultInvariant_m2228 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.CaseInsensitiveComparer::Compare(System.Object,System.Object)
extern "C" int32_t CaseInsensitiveComparer_Compare_m4843 (CaseInsensitiveComparer_t571 * __this, Object_t * ___a, Object_t * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
