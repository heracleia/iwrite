﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>
struct InternalEnumerator_1_t1447;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t234;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m8355(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1447 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8302_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8356(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1447 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>::Dispose()
#define InternalEnumerator_1_Dispose_m8357(__this, method) (( void (*) (InternalEnumerator_1_t1447 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8306_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>::MoveNext()
#define InternalEnumerator_1_MoveNext_m8358(__this, method) (( bool (*) (InternalEnumerator_1_t1447 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8308_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>::get_Current()
#define InternalEnumerator_1_get_Current_m8359(__this, method) (( AchievementDescription_t234 * (*) (InternalEnumerator_1_t1447 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8310_gshared)(__this, method)
