﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.TypedReference
struct TypedReference_t787;
// System.Object
struct Object_t;

// System.Boolean System.TypedReference::Equals(System.Object)
extern "C" bool TypedReference_Equals_m4213 (TypedReference_t787 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TypedReference::GetHashCode()
extern "C" int32_t TypedReference_GetHashCode_m4214 (TypedReference_t787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
