﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct InternalEnumerator_1_t1693;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11361_gshared (InternalEnumerator_1_t1693 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11361(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1693 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11361_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11362_gshared (InternalEnumerator_1_t1693 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11362(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1693 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11362_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11363_gshared (InternalEnumerator_1_t1693 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11363(__this, method) (( void (*) (InternalEnumerator_1_t1693 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11363_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11364_gshared (InternalEnumerator_1_t1693 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11364(__this, method) (( bool (*) (InternalEnumerator_1_t1693 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11364_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern "C" KeyValuePair_2_t1692  InternalEnumerator_1_get_Current_m11365_gshared (InternalEnumerator_1_t1693 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11365(__this, method) (( KeyValuePair_2_t1692  (*) (InternalEnumerator_1_t1693 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11365_gshared)(__this, method)
