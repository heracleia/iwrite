﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Binder
struct Binder_t376;
// System.Reflection.MethodBase
struct MethodBase_t379;
// System.Reflection.MethodBase[]
struct MethodBaseU5BU5D_t1395;
// System.Object[]
struct ObjectU5BU5D_t200;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t377;
// System.Globalization.CultureInfo
struct CultureInfo_t342;
// System.String[]
struct StringU5BU5D_t197;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t196;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t372;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t369;
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"

// System.Void System.Reflection.Binder::.ctor()
extern "C" void Binder__ctor_m5850 (Binder_t376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Binder::.cctor()
extern "C" void Binder__cctor_m5851 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Binder System.Reflection.Binder::get_DefaultBinder()
extern "C" Binder_t376 * Binder_get_DefaultBinder_m5852 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Binder::ConvertArgs(System.Reflection.Binder,System.Object[],System.Reflection.ParameterInfo[],System.Globalization.CultureInfo)
extern "C" bool Binder_ConvertArgs_m5853 (Object_t * __this /* static, unused */, Binder_t376 * ___binder, ObjectU5BU5D_t200* ___args, ParameterInfoU5BU5D_t369* ___pinfo, CultureInfo_t342 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Binder::GetDerivedLevel(System.Type)
extern "C" int32_t Binder_GetDerivedLevel_m5854 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.Binder::FindMostDerivedMatch(System.Reflection.MethodBase[])
extern "C" MethodBase_t379 * Binder_FindMostDerivedMatch_m5855 (Object_t * __this /* static, unused */, MethodBaseU5BU5D_t1395* ___match, const MethodInfo* method) IL2CPP_METHOD_ATTR;
