﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.Int32>
struct GenericComparer_1_t1727;

// System.Void System.Collections.Generic.GenericComparer`1<System.Int32>::.ctor()
extern "C" void GenericComparer_1__ctor_m11656_gshared (GenericComparer_1_t1727 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m11656(__this, method) (( void (*) (GenericComparer_1_t1727 *, const MethodInfo*))GenericComparer_1__ctor_m11656_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int32>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m11657_gshared (GenericComparer_1_t1727 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m11657(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t1727 *, int32_t, int32_t, const MethodInfo*))GenericComparer_1_Compare_m11657_gshared)(__this, ___x, ___y, method)
