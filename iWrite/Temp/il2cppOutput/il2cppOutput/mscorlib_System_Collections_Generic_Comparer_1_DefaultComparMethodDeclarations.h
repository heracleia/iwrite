﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t1728;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C" void DefaultComparer__ctor_m11658_gshared (DefaultComparer_t1728 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m11658(__this, method) (( void (*) (DefaultComparer_t1728 *, const MethodInfo*))DefaultComparer__ctor_m11658_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m11659_gshared (DefaultComparer_t1728 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m11659(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1728 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m11659_gshared)(__this, ___x, ___y, method)
