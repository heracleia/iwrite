﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>
struct InternalEnumerator_1_t1714;
// System.Object
struct Object_t;
// System.Text.RegularExpressions.Group
struct Group_t485;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m11521(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1714 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8302_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11522(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1714 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>::Dispose()
#define InternalEnumerator_1_Dispose_m11523(__this, method) (( void (*) (InternalEnumerator_1_t1714 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8306_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>::MoveNext()
#define InternalEnumerator_1_MoveNext_m11524(__this, method) (( bool (*) (InternalEnumerator_1_t1714 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8308_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>::get_Current()
#define InternalEnumerator_1_get_Current_m11525(__this, method) (( Group_t485 * (*) (InternalEnumerator_1_t1714 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8310_gshared)(__this, method)
