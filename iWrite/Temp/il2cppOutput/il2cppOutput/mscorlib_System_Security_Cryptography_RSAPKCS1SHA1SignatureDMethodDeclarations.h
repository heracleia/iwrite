﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription
struct RSAPKCS1SHA1SignatureDescription_t1214;

// System.Void System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription::.ctor()
extern "C" void RSAPKCS1SHA1SignatureDescription__ctor_m6951 (RSAPKCS1SHA1SignatureDescription_t1214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
