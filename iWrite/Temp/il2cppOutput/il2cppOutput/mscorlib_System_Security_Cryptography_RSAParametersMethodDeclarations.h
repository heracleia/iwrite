﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAParameters
struct RSAParameters_t584;
struct RSAParameters_t584_marshaled;

void RSAParameters_t584_marshal(const RSAParameters_t584& unmarshaled, RSAParameters_t584_marshaled& marshaled);
void RSAParameters_t584_marshal_back(const RSAParameters_t584_marshaled& marshaled, RSAParameters_t584& unmarshaled);
void RSAParameters_t584_marshal_cleanup(RSAParameters_t584_marshaled& marshaled);
