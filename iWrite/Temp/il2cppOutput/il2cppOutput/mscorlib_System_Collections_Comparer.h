﻿#pragma once
#include <stdint.h>
// System.Collections.Comparer
struct Comparer_t873;
// System.Globalization.CompareInfo
struct CompareInfo_t753;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Comparer
struct  Comparer_t873  : public Object_t
{
	// System.Globalization.CompareInfo System.Collections.Comparer::m_compareInfo
	CompareInfo_t753 * ___m_compareInfo_2;
};
struct Comparer_t873_StaticFields{
	// System.Collections.Comparer System.Collections.Comparer::Default
	Comparer_t873 * ___Default_0;
	// System.Collections.Comparer System.Collections.Comparer::DefaultInvariant
	Comparer_t873 * ___DefaultInvariant_1;
};
