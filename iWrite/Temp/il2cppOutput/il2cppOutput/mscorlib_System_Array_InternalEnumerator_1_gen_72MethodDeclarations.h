﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>
struct InternalEnumerator_1_t1760;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_Slot.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11847_gshared (InternalEnumerator_1_t1760 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11847(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1760 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11847_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11848_gshared (InternalEnumerator_1_t1760 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11848(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1760 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11848_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11849_gshared (InternalEnumerator_1_t1760 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11849(__this, method) (( void (*) (InternalEnumerator_1_t1760 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11849_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11850_gshared (InternalEnumerator_1_t1760 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11850(__this, method) (( bool (*) (InternalEnumerator_1_t1760 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11850_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
extern "C" Slot_t874  InternalEnumerator_1_get_Current_m11851_gshared (InternalEnumerator_1_t1760 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11851(__this, method) (( Slot_t874  (*) (InternalEnumerator_1_t1760 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11851_gshared)(__this, method)
