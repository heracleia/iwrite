﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t300;
// System.IO.TextWriter
struct TextWriter_t599;
// System.Object
#include "mscorlib_System_Object.h"
// System.IO.TextWriter
struct  TextWriter_t599  : public Object_t
{
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t300* ___CoreNewLine_0;
};
struct TextWriter_t599_StaticFields{
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t599 * ___Null_1;
};
