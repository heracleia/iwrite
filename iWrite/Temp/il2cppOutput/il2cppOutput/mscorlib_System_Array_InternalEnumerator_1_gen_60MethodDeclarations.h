﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
struct InternalEnumerator_1_t1734;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11685_gshared (InternalEnumerator_1_t1734 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11685(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1734 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11685_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11686_gshared (InternalEnumerator_1_t1734 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11686(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1734 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11686_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11687_gshared (InternalEnumerator_1_t1734 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11687(__this, method) (( void (*) (InternalEnumerator_1_t1734 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11687_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11688_gshared (InternalEnumerator_1_t1734 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11688(__this, method) (( bool (*) (InternalEnumerator_1_t1734 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11688_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m11689_gshared (InternalEnumerator_1_t1734 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11689(__this, method) (( int32_t (*) (InternalEnumerator_1_t1734 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11689_gshared)(__this, method)
