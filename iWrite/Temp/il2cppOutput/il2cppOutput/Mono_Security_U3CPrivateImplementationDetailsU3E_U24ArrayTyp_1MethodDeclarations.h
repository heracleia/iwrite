﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$20
struct U24ArrayTypeU2420_t729;
struct U24ArrayTypeU2420_t729_marshaled;

void U24ArrayTypeU2420_t729_marshal(const U24ArrayTypeU2420_t729& unmarshaled, U24ArrayTypeU2420_t729_marshaled& marshaled);
void U24ArrayTypeU2420_t729_marshal_back(const U24ArrayTypeU2420_t729_marshaled& marshaled, U24ArrayTypeU2420_t729& unmarshaled);
void U24ArrayTypeU2420_t729_marshal_cleanup(U24ArrayTypeU2420_t729_marshaled& marshaled);
