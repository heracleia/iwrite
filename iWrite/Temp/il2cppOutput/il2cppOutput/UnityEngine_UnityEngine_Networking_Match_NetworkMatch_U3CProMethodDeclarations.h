﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>
struct U3CProcessMatchResponseU3Ec__Iterator0_1_t1599;
// System.Object
struct Object_t;

// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::.ctor()
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m10144_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t1599 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m10144(__this, method) (( void (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t1599 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m10144_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m10145_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t1599 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m10145(__this, method) (( Object_t * (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t1599 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m10145_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m10146_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t1599 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m10146(__this, method) (( Object_t * (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t1599 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m10146_gshared)(__this, method)
// System.Boolean UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::MoveNext()
extern "C" bool U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m10147_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t1599 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m10147(__this, method) (( bool (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t1599 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m10147_gshared)(__this, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::Dispose()
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m10148_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t1599 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m10148(__this, method) (( void (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t1599 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m10148_gshared)(__this, method)
