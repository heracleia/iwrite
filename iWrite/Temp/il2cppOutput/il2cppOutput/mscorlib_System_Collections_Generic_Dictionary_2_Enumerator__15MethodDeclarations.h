﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Enumerator_t1679;
// System.Object
struct Object_t;
// UnityEngine.Event
struct Event_t76;
struct Event_t76_marshaled;
// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t253;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14MethodDeclarations.h"
#define Enumerator__ctor_m11167(__this, ___dictionary, method) (( void (*) (Enumerator_t1679 *, Dictionary_2_t253 *, const MethodInfo*))Enumerator__ctor_m11071_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m11168(__this, method) (( Object_t * (*) (Enumerator_t1679 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11072_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11169(__this, method) (( DictionaryEntry_t560  (*) (Enumerator_t1679 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11073_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11170(__this, method) (( Object_t * (*) (Enumerator_t1679 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11074_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11171(__this, method) (( Object_t * (*) (Enumerator_t1679 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11075_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m11172(__this, method) (( bool (*) (Enumerator_t1679 *, const MethodInfo*))Enumerator_MoveNext_m11076_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m11173(__this, method) (( KeyValuePair_2_t1676  (*) (Enumerator_t1679 *, const MethodInfo*))Enumerator_get_Current_m11077_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m11174(__this, method) (( Event_t76 * (*) (Enumerator_t1679 *, const MethodInfo*))Enumerator_get_CurrentKey_m11078_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m11175(__this, method) (( int32_t (*) (Enumerator_t1679 *, const MethodInfo*))Enumerator_get_CurrentValue_m11079_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m11176(__this, method) (( void (*) (Enumerator_t1679 *, const MethodInfo*))Enumerator_VerifyState_m11080_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m11177(__this, method) (( void (*) (Enumerator_t1679 *, const MethodInfo*))Enumerator_VerifyCurrent_m11081_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m11178(__this, method) (( void (*) (Enumerator_t1679 *, const MethodInfo*))Enumerator_Dispose_m11082_gshared)(__this, method)
