﻿#pragma once
#include <stdint.h>
// System.Threading.Timer/Scheduler
struct Scheduler_t1282;
// System.Collections.SortedList
struct SortedList_t579;
// System.Object
#include "mscorlib_System_Object.h"
// System.Threading.Timer/Scheduler
struct  Scheduler_t1282  : public Object_t
{
	// System.Collections.SortedList System.Threading.Timer/Scheduler::list
	SortedList_t579 * ___list_1;
};
struct Scheduler_t1282_StaticFields{
	// System.Threading.Timer/Scheduler System.Threading.Timer/Scheduler::instance
	Scheduler_t1282 * ___instance_0;
};
