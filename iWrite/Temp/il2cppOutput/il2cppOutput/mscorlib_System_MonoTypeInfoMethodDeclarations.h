﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTypeInfo
struct MonoTypeInfo_t1337;

// System.Void System.MonoTypeInfo::.ctor()
extern "C" void MonoTypeInfo__ctor_m7934 (MonoTypeInfo_t1337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
