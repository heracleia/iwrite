﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
struct NonBacktrackingGroup_t532;
// System.Text.RegularExpressions.ICompiler
struct ICompiler_t570;

// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::.ctor()
extern "C" void NonBacktrackingGroup__ctor_m2077 (NonBacktrackingGroup_t532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern "C" void NonBacktrackingGroup_Compile_m2078 (NonBacktrackingGroup_t532 * __this, Object_t * ___cmp, bool ___reverse, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::IsComplex()
extern "C" bool NonBacktrackingGroup_IsComplex_m2079 (NonBacktrackingGroup_t532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
