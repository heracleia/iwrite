﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct KeyValuePair_2_t1662;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m11042_gshared (KeyValuePair_2_t1662 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m11042(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1662 *, Object_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m11042_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m11043_gshared (KeyValuePair_2_t1662 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m11043(__this, method) (( Object_t * (*) (KeyValuePair_2_t1662 *, const MethodInfo*))KeyValuePair_2_get_Key_m11043_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m11044_gshared (KeyValuePair_2_t1662 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m11044(__this, ___value, method) (( void (*) (KeyValuePair_2_t1662 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m11044_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m11045_gshared (KeyValuePair_2_t1662 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m11045(__this, method) (( int32_t (*) (KeyValuePair_2_t1662 *, const MethodInfo*))KeyValuePair_2_get_Value_m11045_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m11046_gshared (KeyValuePair_2_t1662 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m11046(__this, ___value, method) (( void (*) (KeyValuePair_2_t1662 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m11046_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m11047_gshared (KeyValuePair_2_t1662 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m11047(__this, method) (( String_t* (*) (KeyValuePair_2_t1662 *, const MethodInfo*))KeyValuePair_2_ToString_m11047_gshared)(__this, method)
