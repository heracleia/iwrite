﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Networking.Types.NetworkAccessToken
struct NetworkAccessToken_t180;
// System.String
struct String_t;

// System.Void UnityEngine.Networking.Types.NetworkAccessToken::.ctor()
extern "C" void NetworkAccessToken__ctor_m901 (NetworkAccessToken_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Types.NetworkAccessToken::GetByteString()
extern "C" String_t* NetworkAccessToken_GetByteString_m902 (NetworkAccessToken_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
