﻿#pragma once
#include <stdint.h>
// System.ComponentModel.TypeConverterAttribute
struct TypeConverterAttribute_t403;
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.ComponentModel.TypeConverterAttribute
struct  TypeConverterAttribute_t403  : public Attribute_t96
{
	// System.String System.ComponentModel.TypeConverterAttribute::converter_type
	String_t* ___converter_type_1;
};
struct TypeConverterAttribute_t403_StaticFields{
	// System.ComponentModel.TypeConverterAttribute System.ComponentModel.TypeConverterAttribute::Default
	TypeConverterAttribute_t403 * ___Default_0;
};
