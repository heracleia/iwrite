﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$256
struct U24ArrayTypeU24256_t613;
struct U24ArrayTypeU24256_t613_marshaled;

void U24ArrayTypeU24256_t613_marshal(const U24ArrayTypeU24256_t613& unmarshaled, U24ArrayTypeU24256_t613_marshaled& marshaled);
void U24ArrayTypeU24256_t613_marshal_back(const U24ArrayTypeU24256_t613_marshaled& marshaled, U24ArrayTypeU24256_t613& unmarshaled);
void U24ArrayTypeU24256_t613_marshal_cleanup(U24ArrayTypeU24256_t613_marshaled& marshaled);
