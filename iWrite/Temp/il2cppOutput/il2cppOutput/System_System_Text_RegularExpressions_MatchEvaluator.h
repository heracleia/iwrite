﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Text.RegularExpressions.Match
struct Match_t479;
// System.IAsyncResult
struct IAsyncResult_t44;
// System.AsyncCallback
struct AsyncCallback_t45;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Text.RegularExpressions.MatchEvaluator
struct  MatchEvaluator_t555  : public MulticastDelegate_t47
{
};
