﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.String>
struct Enumerator_t1529;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t101;

// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1MethodDeclarations.h"
#define Enumerator__ctor_m9351(__this, ___l, method) (( void (*) (Enumerator_t1529 *, List_1_t101 *, const MethodInfo*))Enumerator__ctor_m8428_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m9352(__this, method) (( Object_t * (*) (Enumerator_t1529 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
#define Enumerator_Dispose_m9353(__this, method) (( void (*) (Enumerator_t1529 *, const MethodInfo*))Enumerator_Dispose_m8430_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::VerifyState()
#define Enumerator_VerifyState_m9354(__this, method) (( void (*) (Enumerator_t1529 *, const MethodInfo*))Enumerator_VerifyState_m8431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
#define Enumerator_MoveNext_m9355(__this, method) (( bool (*) (Enumerator_t1529 *, const MethodInfo*))Enumerator_MoveNext_m8432_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
#define Enumerator_get_Current_m9356(__this, method) (( String_t* (*) (Enumerator_t1529 *, const MethodInfo*))Enumerator_get_Current_m8433_gshared)(__this, method)
