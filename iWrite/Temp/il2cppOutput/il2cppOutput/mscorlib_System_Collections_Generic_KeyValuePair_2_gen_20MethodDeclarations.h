﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
struct KeyValuePair_2_t1692;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m11366_gshared (KeyValuePair_2_t1692 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m11366(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1692 *, Object_t *, bool, const MethodInfo*))KeyValuePair_2__ctor_m11366_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m11367_gshared (KeyValuePair_2_t1692 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m11367(__this, method) (( Object_t * (*) (KeyValuePair_2_t1692 *, const MethodInfo*))KeyValuePair_2_get_Key_m11367_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m11368_gshared (KeyValuePair_2_t1692 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m11368(__this, ___value, method) (( void (*) (KeyValuePair_2_t1692 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m11368_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C" bool KeyValuePair_2_get_Value_m11369_gshared (KeyValuePair_2_t1692 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m11369(__this, method) (( bool (*) (KeyValuePair_2_t1692 *, const MethodInfo*))KeyValuePair_2_get_Value_m11369_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m11370_gshared (KeyValuePair_2_t1692 * __this, bool ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m11370(__this, ___value, method) (( void (*) (KeyValuePair_2_t1692 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m11370_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m11371_gshared (KeyValuePair_2_t1692 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m11371(__this, method) (( String_t* (*) (KeyValuePair_2_t1692 *, const MethodInfo*))KeyValuePair_2_ToString_m11371_gshared)(__this, method)
