﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern const Il2CppType Object_t_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Types[] = { &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0 = { 1, GenInst_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 4, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t27_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t27_0_0_0_Types[] = { &GcLeaderboard_t27_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t27_0_0_0 = { 1, GenInst_GcLeaderboard_t27_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t314_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t314_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t314_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t314_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t314_0_0_0_Types };
extern const Il2CppType Boolean_t333_0_0_0;
static const Il2CppType* GenInst_Boolean_t333_0_0_0_Types[] = { &Boolean_t333_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t333_0_0_0 = { 1, GenInst_Boolean_t333_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t316_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t316_0_0_0_Types[] = { &IAchievementU5BU5D_t316_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t316_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t316_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t237_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t237_0_0_0_Types[] = { &IScoreU5BU5D_t237_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t237_0_0_0 = { 1, GenInst_IScoreU5BU5D_t237_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t231_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t231_0_0_0_Types[] = { &IUserProfileU5BU5D_t231_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t231_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t231_0_0_0_Types };
extern const Il2CppType Int32_t320_0_0_0;
extern const Il2CppType LayoutCache_t54_0_0_0;
static const Il2CppType* GenInst_Int32_t320_0_0_0_LayoutCache_t54_0_0_0_Types[] = { &Int32_t320_0_0_0, &LayoutCache_t54_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t320_0_0_0_LayoutCache_t54_0_0_0 = { 2, GenInst_Int32_t320_0_0_0_LayoutCache_t54_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t58_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t58_0_0_0_Types[] = { &GUILayoutEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t58_0_0_0 = { 1, GenInst_GUILayoutEntry_t58_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType GUIStyle_t56_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t56_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t56_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t56_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t102_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t102_0_0_0_Types[] = { &ByteU5BU5D_t102_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t102_0_0_0 = { 1, GenInst_ByteU5BU5D_t102_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType Font_t69_0_0_0;
static const Il2CppType* GenInst_Font_t69_0_0_0_Types[] = { &Font_t69_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t69_0_0_0 = { 1, GenInst_Font_t69_0_0_0_Types };
extern const Il2CppType UIVertex_t158_0_0_0;
static const Il2CppType* GenInst_UIVertex_t158_0_0_0_Types[] = { &UIVertex_t158_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t158_0_0_0 = { 1, GenInst_UIVertex_t158_0_0_0_Types };
extern const Il2CppType UICharInfo_t149_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t149_0_0_0_Types[] = { &UICharInfo_t149_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t149_0_0_0 = { 1, GenInst_UICharInfo_t149_0_0_0_Types };
extern const Il2CppType UILineInfo_t150_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t150_0_0_0_Types[] = { &UILineInfo_t150_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t150_0_0_0 = { 1, GenInst_UILineInfo_t150_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType MatchDirectConnectInfo_t171_0_0_0;
static const Il2CppType* GenInst_MatchDirectConnectInfo_t171_0_0_0_Types[] = { &MatchDirectConnectInfo_t171_0_0_0 };
extern const Il2CppGenericInst GenInst_MatchDirectConnectInfo_t171_0_0_0 = { 1, GenInst_MatchDirectConnectInfo_t171_0_0_0_Types };
extern const Il2CppType MatchDesc_t173_0_0_0;
static const Il2CppType* GenInst_MatchDesc_t173_0_0_0_Types[] = { &MatchDesc_t173_0_0_0 };
extern const Il2CppGenericInst GenInst_MatchDesc_t173_0_0_0 = { 1, GenInst_MatchDesc_t173_0_0_0_Types };
extern const Il2CppType NetworkID_t178_0_0_0;
extern const Il2CppType NetworkAccessToken_t180_0_0_0;
static const Il2CppType* GenInst_NetworkID_t178_0_0_0_NetworkAccessToken_t180_0_0_0_Types[] = { &NetworkID_t178_0_0_0, &NetworkAccessToken_t180_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t178_0_0_0_NetworkAccessToken_t180_0_0_0 = { 2, GenInst_NetworkID_t178_0_0_0_NetworkAccessToken_t180_0_0_0_Types };
extern const Il2CppType CreateMatchResponse_t165_0_0_0;
static const Il2CppType* GenInst_CreateMatchResponse_t165_0_0_0_Types[] = { &CreateMatchResponse_t165_0_0_0 };
extern const Il2CppGenericInst GenInst_CreateMatchResponse_t165_0_0_0 = { 1, GenInst_CreateMatchResponse_t165_0_0_0_Types };
extern const Il2CppType JoinMatchResponse_t167_0_0_0;
static const Il2CppType* GenInst_JoinMatchResponse_t167_0_0_0_Types[] = { &JoinMatchResponse_t167_0_0_0 };
extern const Il2CppGenericInst GenInst_JoinMatchResponse_t167_0_0_0 = { 1, GenInst_JoinMatchResponse_t167_0_0_0_Types };
extern const Il2CppType BasicResponse_t162_0_0_0;
static const Il2CppType* GenInst_BasicResponse_t162_0_0_0_Types[] = { &BasicResponse_t162_0_0_0 };
extern const Il2CppGenericInst GenInst_BasicResponse_t162_0_0_0 = { 1, GenInst_BasicResponse_t162_0_0_0_Types };
extern const Il2CppType ListMatchResponse_t175_0_0_0;
static const Il2CppType* GenInst_ListMatchResponse_t175_0_0_0_Types[] = { &ListMatchResponse_t175_0_0_0 };
extern const Il2CppGenericInst GenInst_ListMatchResponse_t175_0_0_0 = { 1, GenInst_ListMatchResponse_t175_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t298_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t298_0_0_0_Types[] = { &KeyValuePair_2_t298_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t298_0_0_0 = { 1, GenInst_KeyValuePair_2_t298_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ConstructorDelegate_t201_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ConstructorDelegate_t201_0_0_0_Types[] = { &Type_t_0_0_0, &ConstructorDelegate_t201_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t201_0_0_0 = { 2, GenInst_Type_t_0_0_0_ConstructorDelegate_t201_0_0_0_Types };
extern const Il2CppType IDictionary_2_t303_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t303_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t303_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t303_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t303_0_0_0_Types };
extern const Il2CppType GetDelegate_t198_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GetDelegate_t198_0_0_0_Types[] = { &String_t_0_0_0, &GetDelegate_t198_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t198_0_0_0 = { 2, GenInst_String_t_0_0_0_GetDelegate_t198_0_0_0_Types };
extern const Il2CppType IDictionary_2_t304_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t304_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t304_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t304_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t304_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t362_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_KeyValuePair_2_t362_0_0_0_Types[] = { &String_t_0_0_0, &KeyValuePair_2_t362_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t362_0_0_0 = { 2, GenInst_String_t_0_0_0_KeyValuePair_2_t362_0_0_0_Types };
extern const Il2CppType SetDelegate_t199_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_SetDelegate_t199_0_0_0_Types[] = { &Type_t_0_0_0, &SetDelegate_t199_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SetDelegate_t199_0_0_0 = { 2, GenInst_Type_t_0_0_0_SetDelegate_t199_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t365_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t365_0_0_0_Types[] = { &KeyValuePair_2_t365_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t365_0_0_0 = { 1, GenInst_KeyValuePair_2_t365_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t202_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t202_0_0_0_Types[] = { &ConstructorInfo_t202_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t202_0_0_0 = { 1, GenInst_ConstructorInfo_t202_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType GUILayer_t36_0_0_0;
static const Il2CppType* GenInst_GUILayer_t36_0_0_0_Types[] = { &GUILayer_t36_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t36_0_0_0 = { 1, GenInst_GUILayer_t36_0_0_0_Types };
extern const Il2CppType PersistentCall_t258_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t258_0_0_0_Types[] = { &PersistentCall_t258_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t258_0_0_0 = { 1, GenInst_PersistentCall_t258_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t256_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t256_0_0_0_Types[] = { &BaseInvokableCall_t256_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t256_0_0_0 = { 1, GenInst_BaseInvokableCall_t256_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t333_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t333_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t333_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t333_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t320_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t320_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t320_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t320_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t320_0_0_0_Types[] = { &Int32_t320_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t320_0_0_0 = { 1, GenInst_Int32_t320_0_0_0_Types };
extern const Il2CppType StrongName_t1226_0_0_0;
static const Il2CppType* GenInst_StrongName_t1226_0_0_0_Types[] = { &StrongName_t1226_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t1226_0_0_0 = { 1, GenInst_StrongName_t1226_0_0_0_Types };
extern const Il2CppType DateTime_t51_0_0_0;
static const Il2CppType* GenInst_DateTime_t51_0_0_0_Types[] = { &DateTime_t51_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t51_0_0_0 = { 1, GenInst_DateTime_t51_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t363_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t363_0_0_0_Types[] = { &DateTimeOffset_t363_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t363_0_0_0 = { 1, GenInst_DateTimeOffset_t363_0_0_0_Types };
extern const Il2CppType TimeSpan_t463_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t463_0_0_0_Types[] = { &TimeSpan_t463_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t463_0_0_0 = { 1, GenInst_TimeSpan_t463_0_0_0_Types };
extern const Il2CppType Guid_t364_0_0_0;
static const Il2CppType* GenInst_Guid_t364_0_0_0_Types[] = { &Guid_t364_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t364_0_0_0 = { 1, GenInst_Guid_t364_0_0_0_Types };
extern const Il2CppType Object_t5_0_0_0;
static const Il2CppType* GenInst_Object_t5_0_0_0_Types[] = { &Object_t5_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t5_0_0_0 = { 1, GenInst_Object_t5_0_0_0_Types };
extern const Il2CppType IConvertible_t1409_0_0_0;
static const Il2CppType* GenInst_IConvertible_t1409_0_0_0_Types[] = { &IConvertible_t1409_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t1409_0_0_0 = { 1, GenInst_IConvertible_t1409_0_0_0_Types };
extern const Il2CppType IComparable_t1408_0_0_0;
static const Il2CppType* GenInst_IComparable_t1408_0_0_0_Types[] = { &IComparable_t1408_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1408_0_0_0 = { 1, GenInst_IComparable_t1408_0_0_0_Types };
extern const Il2CppType IEnumerable_t302_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t302_0_0_0_Types[] = { &IEnumerable_t302_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t302_0_0_0 = { 1, GenInst_IEnumerable_t302_0_0_0_Types };
extern const Il2CppType ICloneable_t2056_0_0_0;
static const Il2CppType* GenInst_ICloneable_t2056_0_0_0_Types[] = { &ICloneable_t2056_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t2056_0_0_0 = { 1, GenInst_ICloneable_t2056_0_0_0_Types };
extern const Il2CppType IComparable_1_t2235_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2235_0_0_0_Types[] = { &IComparable_1_t2235_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2235_0_0_0 = { 1, GenInst_IComparable_1_t2235_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2236_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2236_0_0_0_Types[] = { &IEquatable_1_t2236_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2236_0_0_0 = { 1, GenInst_IEquatable_1_t2236_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t1804_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t1804_0_0_0_Types[] = { &IAchievementDescription_t1804_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t1804_0_0_0 = { 1, GenInst_IAchievementDescription_t1804_0_0_0_Types };
extern const Il2CppType IAchievement_t277_0_0_0;
static const Il2CppType* GenInst_IAchievement_t277_0_0_0_Types[] = { &IAchievement_t277_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t277_0_0_0 = { 1, GenInst_IAchievement_t277_0_0_0_Types };
extern const Il2CppType IScore_t236_0_0_0;
static const Il2CppType* GenInst_IScore_t236_0_0_0_Types[] = { &IScore_t236_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t236_0_0_0 = { 1, GenInst_IScore_t236_0_0_0_Types };
extern const Il2CppType IUserProfile_t1805_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t1805_0_0_0_Types[] = { &IUserProfile_t1805_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t1805_0_0_0 = { 1, GenInst_IUserProfile_t1805_0_0_0_Types };
extern const Il2CppType AchievementDescription_t234_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t234_0_0_0_Types[] = { &AchievementDescription_t234_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t234_0_0_0 = { 1, GenInst_AchievementDescription_t234_0_0_0_Types };
extern const Il2CppType UserProfile_t232_0_0_0;
static const Il2CppType* GenInst_UserProfile_t232_0_0_0_Types[] = { &UserProfile_t232_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t232_0_0_0 = { 1, GenInst_UserProfile_t232_0_0_0_Types };
extern const Il2CppType IReflect_t2065_0_0_0;
static const Il2CppType* GenInst_IReflect_t2065_0_0_0_Types[] = { &IReflect_t2065_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t2065_0_0_0 = { 1, GenInst_IReflect_t2065_0_0_0_Types };
extern const Il2CppType _Type_t2063_0_0_0;
static const Il2CppType* GenInst__Type_t2063_0_0_0_Types[] = { &_Type_t2063_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t2063_0_0_0 = { 1, GenInst__Type_t2063_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t1404_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t1404_0_0_0_Types[] = { &ICustomAttributeProvider_t1404_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1404_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t1404_0_0_0_Types };
extern const Il2CppType _MemberInfo_t2064_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t2064_0_0_0_Types[] = { &_MemberInfo_t2064_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t2064_0_0_0 = { 1, GenInst__MemberInfo_t2064_0_0_0_Types };
extern const Il2CppType GcAchievementData_t224_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t224_0_0_0_Types[] = { &GcAchievementData_t224_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t224_0_0_0 = { 1, GenInst_GcAchievementData_t224_0_0_0_Types };
extern const Il2CppType ValueType_t757_0_0_0;
static const Il2CppType* GenInst_ValueType_t757_0_0_0_Types[] = { &ValueType_t757_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t757_0_0_0 = { 1, GenInst_ValueType_t757_0_0_0_Types };
extern const Il2CppType Achievement_t233_0_0_0;
static const Il2CppType* GenInst_Achievement_t233_0_0_0_Types[] = { &Achievement_t233_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t233_0_0_0 = { 1, GenInst_Achievement_t233_0_0_0_Types };
extern const Il2CppType GcScoreData_t225_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t225_0_0_0_Types[] = { &GcScoreData_t225_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t225_0_0_0 = { 1, GenInst_GcScoreData_t225_0_0_0_Types };
extern const Il2CppType Score_t235_0_0_0;
static const Il2CppType* GenInst_Score_t235_0_0_0_Types[] = { &Score_t235_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t235_0_0_0 = { 1, GenInst_Score_t235_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t63_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t63_0_0_0_Types[] = { &GUILayoutOption_t63_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t63_0_0_0 = { 1, GenInst_GUILayoutOption_t63_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t320_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t320_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t320_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int32_t320_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1465_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1465_0_0_0_Types[] = { &KeyValuePair_2_t1465_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1465_0_0_0 = { 1, GenInst_KeyValuePair_2_t1465_0_0_0_Types };
extern const Il2CppType IFormattable_t1406_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1406_0_0_0_Types[] = { &IFormattable_t1406_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1406_0_0_0 = { 1, GenInst_IFormattable_t1406_0_0_0_Types };
extern const Il2CppType IComparable_1_t2217_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2217_0_0_0_Types[] = { &IComparable_1_t2217_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2217_0_0_0 = { 1, GenInst_IComparable_1_t2217_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2218_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2218_0_0_0_Types[] = { &IEquatable_1_t2218_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2218_0_0_0 = { 1, GenInst_IEquatable_1_t2218_0_0_0_Types };
extern const Il2CppType Link_t864_0_0_0;
static const Il2CppType* GenInst_Link_t864_0_0_0_Types[] = { &Link_t864_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t864_0_0_0 = { 1, GenInst_Link_t864_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t320_0_0_0_Object_t_0_0_0_Int32_t320_0_0_0_Types[] = { &Int32_t320_0_0_0, &Object_t_0_0_0, &Int32_t320_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t320_0_0_0_Object_t_0_0_0_Int32_t320_0_0_0 = { 3, GenInst_Int32_t320_0_0_0_Object_t_0_0_0_Int32_t320_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t320_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t320_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t320_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Int32_t320_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t560_0_0_0;
static const Il2CppType* GenInst_Int32_t320_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Int32_t320_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t320_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Int32_t320_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t560_0_0_0_Types[] = { &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t560_0_0_0 = { 1, GenInst_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t320_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1465_0_0_0_Types[] = { &Int32_t320_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1465_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t320_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1465_0_0_0 = { 3, GenInst_Int32_t320_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1465_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t320_0_0_0_LayoutCache_t54_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Int32_t320_0_0_0, &LayoutCache_t54_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t320_0_0_0_LayoutCache_t54_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Int32_t320_0_0_0_LayoutCache_t54_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_LayoutCache_t54_0_0_0_Types[] = { &LayoutCache_t54_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutCache_t54_0_0_0 = { 1, GenInst_LayoutCache_t54_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1482_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1482_0_0_0_Types[] = { &KeyValuePair_2_t1482_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1482_0_0_0 = { 1, GenInst_KeyValuePair_2_t1482_0_0_0_Types };
static const Il2CppType* GenInst_GUIStyle_t56_0_0_0_Types[] = { &GUIStyle_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t56_0_0_0 = { 1, GenInst_GUIStyle_t56_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1493_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1493_0_0_0_Types[] = { &KeyValuePair_2_t1493_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1493_0_0_0 = { 1, GenInst_KeyValuePair_2_t1493_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1493_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t56_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t56_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t56_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t56_0_0_0_DictionaryEntry_t560_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1503_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1503_0_0_0_Types[] = { &KeyValuePair_2_t1503_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1503_0_0_0 = { 1, GenInst_KeyValuePair_2_t1503_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t320_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t320_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t320_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int32_t320_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1509_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1509_0_0_0_Types[] = { &KeyValuePair_2_t1509_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1509_0_0_0 = { 1, GenInst_KeyValuePair_2_t1509_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t320_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t320_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t320_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t320_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t320_0_0_0_Int32_t320_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t320_0_0_0, &Int32_t320_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t320_0_0_0_Int32_t320_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t320_0_0_0_Int32_t320_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t320_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t320_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t320_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t320_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t320_0_0_0_KeyValuePair_2_t1509_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t320_0_0_0, &KeyValuePair_2_t1509_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t320_0_0_0_KeyValuePair_2_t1509_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t320_0_0_0_KeyValuePair_2_t1509_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t320_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t320_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t320_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t320_0_0_0_DictionaryEntry_t560_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1520_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1520_0_0_0_Types[] = { &KeyValuePair_2_t1520_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1520_0_0_0 = { 1, GenInst_KeyValuePair_2_t1520_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t560_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t328_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t328_0_0_0_Types[] = { &KeyValuePair_2_t328_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t328_0_0_0 = { 1, GenInst_KeyValuePair_2_t328_0_0_0_Types };
extern const Il2CppType Byte_t326_0_0_0;
static const Il2CppType* GenInst_Byte_t326_0_0_0_Types[] = { &Byte_t326_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t326_0_0_0 = { 1, GenInst_Byte_t326_0_0_0_Types };
extern const Il2CppType IComparable_1_t2225_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2225_0_0_0_Types[] = { &IComparable_1_t2225_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2225_0_0_0 = { 1, GenInst_IComparable_1_t2225_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2226_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2226_0_0_0_Types[] = { &IEquatable_1_t2226_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2226_0_0_0 = { 1, GenInst_IEquatable_1_t2226_0_0_0_Types };
extern const Il2CppType Char_t576_0_0_0;
static const Il2CppType* GenInst_Char_t576_0_0_0_Types[] = { &Char_t576_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t576_0_0_0 = { 1, GenInst_Char_t576_0_0_0_Types };
extern const Il2CppType IComparable_1_t2233_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2233_0_0_0_Types[] = { &IComparable_1_t2233_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2233_0_0_0 = { 1, GenInst_IComparable_1_t2233_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2234_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2234_0_0_0_Types[] = { &IEquatable_1_t2234_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2234_0_0_0 = { 1, GenInst_IEquatable_1_t2234_0_0_0_Types };
extern const Il2CppType Camera_t110_0_0_0;
static const Il2CppType* GenInst_Camera_t110_0_0_0_Types[] = { &Camera_t110_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t110_0_0_0 = { 1, GenInst_Camera_t110_0_0_0_Types };
extern const Il2CppType Behaviour_t34_0_0_0;
static const Il2CppType* GenInst_Behaviour_t34_0_0_0_Types[] = { &Behaviour_t34_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t34_0_0_0 = { 1, GenInst_Behaviour_t34_0_0_0_Types };
extern const Il2CppType Component_t109_0_0_0;
static const Il2CppType* GenInst_Component_t109_0_0_0_Types[] = { &Component_t109_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t109_0_0_0 = { 1, GenInst_Component_t109_0_0_0_Types };
extern const Il2CppType Display_t115_0_0_0;
static const Il2CppType* GenInst_Display_t115_0_0_0_Types[] = { &Display_t115_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t115_0_0_0 = { 1, GenInst_Display_t115_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType ISerializable_t1426_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1426_0_0_0_Types[] = { &ISerializable_t1426_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1426_0_0_0 = { 1, GenInst_ISerializable_t1426_0_0_0_Types };
extern const Il2CppType Single_t321_0_0_0;
static const Il2CppType* GenInst_Single_t321_0_0_0_Types[] = { &Single_t321_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t321_0_0_0 = { 1, GenInst_Single_t321_0_0_0_Types };
extern const Il2CppType IComparable_1_t2238_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2238_0_0_0_Types[] = { &IComparable_1_t2238_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2238_0_0_0 = { 1, GenInst_IComparable_1_t2238_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2239_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2239_0_0_0_Types[] = { &IEquatable_1_t2239_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2239_0_0_0 = { 1, GenInst_IEquatable_1_t2239_0_0_0_Types };
extern const Il2CppType Keyframe_t135_0_0_0;
static const Il2CppType* GenInst_Keyframe_t135_0_0_0_Types[] = { &Keyframe_t135_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t135_0_0_0 = { 1, GenInst_Keyframe_t135_0_0_0_Types };
extern const Il2CppType Int64_t345_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int64_t345_0_0_0_Types[] = { &String_t_0_0_0, &Int64_t345_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t345_0_0_0 = { 2, GenInst_String_t_0_0_0_Int64_t345_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t345_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t345_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t345_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int64_t345_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1552_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1552_0_0_0_Types[] = { &KeyValuePair_2_t1552_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1552_0_0_0 = { 1, GenInst_KeyValuePair_2_t1552_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t345_0_0_0_Types[] = { &Int64_t345_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t345_0_0_0 = { 1, GenInst_Int64_t345_0_0_0_Types };
extern const Il2CppType IComparable_1_t2219_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2219_0_0_0_Types[] = { &IComparable_1_t2219_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2219_0_0_0 = { 1, GenInst_IComparable_1_t2219_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2220_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2220_0_0_0_Types[] = { &IEquatable_1_t2220_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2220_0_0_0 = { 1, GenInst_IEquatable_1_t2220_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t345_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t345_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t345_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int64_t345_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t345_0_0_0_Int64_t345_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t345_0_0_0, &Int64_t345_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t345_0_0_0_Int64_t345_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int64_t345_0_0_0_Int64_t345_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t345_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t345_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t345_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int64_t345_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t345_0_0_0_KeyValuePair_2_t1552_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t345_0_0_0, &KeyValuePair_2_t1552_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t345_0_0_0_KeyValuePair_2_t1552_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int64_t345_0_0_0_KeyValuePair_2_t1552_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int64_t345_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &String_t_0_0_0, &Int64_t345_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t345_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_String_t_0_0_0_Int64_t345_0_0_0_DictionaryEntry_t560_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1567_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1567_0_0_0_Types[] = { &KeyValuePair_2_t1567_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1567_0_0_0 = { 1, GenInst_KeyValuePair_2_t1567_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_Types[] = { &NetworkID_t178_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0 = { 2, GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1581_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1581_0_0_0_Types[] = { &KeyValuePair_2_t1581_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1581_0_0_0 = { 1, GenInst_KeyValuePair_2_t1581_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t178_0_0_0_Types[] = { &NetworkID_t178_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t178_0_0_0 = { 1, GenInst_NetworkID_t178_0_0_0_Types };
extern const Il2CppType Enum_t305_0_0_0;
static const Il2CppType* GenInst_Enum_t305_0_0_0_Types[] = { &Enum_t305_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t305_0_0_0 = { 1, GenInst_Enum_t305_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_NetworkID_t178_0_0_0_Types[] = { &NetworkID_t178_0_0_0, &Object_t_0_0_0, &NetworkID_t178_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_NetworkID_t178_0_0_0 = { 3, GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_NetworkID_t178_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &NetworkID_t178_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &NetworkID_t178_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1581_0_0_0_Types[] = { &NetworkID_t178_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1581_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1581_0_0_0 = { 3, GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1581_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t178_0_0_0_NetworkAccessToken_t180_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &NetworkID_t178_0_0_0, &NetworkAccessToken_t180_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t178_0_0_0_NetworkAccessToken_t180_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_NetworkID_t178_0_0_0_NetworkAccessToken_t180_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_NetworkAccessToken_t180_0_0_0_Types[] = { &NetworkAccessToken_t180_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkAccessToken_t180_0_0_0 = { 1, GenInst_NetworkAccessToken_t180_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1595_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1595_0_0_0_Types[] = { &KeyValuePair_2_t1595_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1595_0_0_0 = { 1, GenInst_KeyValuePair_2_t1595_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_ConstructorDelegate_t201_0_0_0_Types[] = { &ConstructorDelegate_t201_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorDelegate_t201_0_0_0 = { 1, GenInst_ConstructorDelegate_t201_0_0_0_Types };
static const Il2CppType* GenInst_GetDelegate_t198_0_0_0_Types[] = { &GetDelegate_t198_0_0_0 };
extern const Il2CppGenericInst GenInst_GetDelegate_t198_0_0_0 = { 1, GenInst_GetDelegate_t198_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t303_0_0_0_Types[] = { &IDictionary_2_t303_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t303_0_0_0 = { 1, GenInst_IDictionary_2_t303_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t362_0_0_0_Types[] = { &KeyValuePair_2_t362_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t362_0_0_0 = { 1, GenInst_KeyValuePair_2_t362_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t304_0_0_0_Types[] = { &IDictionary_2_t304_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t304_0_0_0 = { 1, GenInst_IDictionary_2_t304_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t1493_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0 = { 2, GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1601_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1601_0_0_0_Types[] = { &KeyValuePair_2_t1601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1601_0_0_0 = { 1, GenInst_KeyValuePair_2_t1601_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ConstructorDelegate_t201_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Type_t_0_0_0, &ConstructorDelegate_t201_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t201_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Type_t_0_0_0_ConstructorDelegate_t201_0_0_0_DictionaryEntry_t560_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1609_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1609_0_0_0_Types[] = { &KeyValuePair_2_t1609_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1609_0_0_0 = { 1, GenInst_KeyValuePair_2_t1609_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t303_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t303_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t303_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t303_0_0_0_DictionaryEntry_t560_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1613_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1613_0_0_0_Types[] = { &KeyValuePair_2_t1613_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1613_0_0_0 = { 1, GenInst_KeyValuePair_2_t1613_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t304_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t304_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t304_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t304_0_0_0_DictionaryEntry_t560_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1617_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1617_0_0_0_Types[] = { &KeyValuePair_2_t1617_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1617_0_0_0 = { 1, GenInst_KeyValuePair_2_t1617_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GetDelegate_t198_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &String_t_0_0_0, &GetDelegate_t198_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t198_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_String_t_0_0_0_GetDelegate_t198_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t1493_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_KeyValuePair_2_t1493_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t1493_0_0_0, &KeyValuePair_2_t1493_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_KeyValuePair_2_t1493_0_0_0 = { 3, GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_KeyValuePair_2_t1493_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t1493_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_KeyValuePair_2_t1601_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t1493_0_0_0, &KeyValuePair_2_t1601_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_KeyValuePair_2_t1601_0_0_0 = { 3, GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_KeyValuePair_2_t1601_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_KeyValuePair_2_t362_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &String_t_0_0_0, &KeyValuePair_2_t362_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t362_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_String_t_0_0_0_KeyValuePair_2_t362_0_0_0_DictionaryEntry_t560_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1639_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1639_0_0_0_Types[] = { &KeyValuePair_2_t1639_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1639_0_0_0 = { 1, GenInst_KeyValuePair_2_t1639_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t2101_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t2101_0_0_0_Types[] = { &_ConstructorInfo_t2101_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t2101_0_0_0 = { 1, GenInst__ConstructorInfo_t2101_0_0_0_Types };
extern const Il2CppType MethodBase_t379_0_0_0;
static const Il2CppType* GenInst_MethodBase_t379_0_0_0_Types[] = { &MethodBase_t379_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t379_0_0_0 = { 1, GenInst_MethodBase_t379_0_0_0_Types };
extern const Il2CppType _MethodBase_t2104_0_0_0;
static const Il2CppType* GenInst__MethodBase_t2104_0_0_0_Types[] = { &_MethodBase_t2104_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t2104_0_0_0 = { 1, GenInst__MethodBase_t2104_0_0_0_Types };
extern const Il2CppType ParameterInfo_t370_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t370_0_0_0_Types[] = { &ParameterInfo_t370_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t370_0_0_0 = { 1, GenInst_ParameterInfo_t370_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t2107_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t2107_0_0_0_Types[] = { &_ParameterInfo_t2107_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t2107_0_0_0 = { 1, GenInst__ParameterInfo_t2107_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t2108_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t2108_0_0_0_Types[] = { &_PropertyInfo_t2108_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t2108_0_0_0 = { 1, GenInst__PropertyInfo_t2108_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2103_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2103_0_0_0_Types[] = { &_FieldInfo_t2103_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2103_0_0_0 = { 1, GenInst__FieldInfo_t2103_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t215_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t215_0_0_0_Types[] = { &DisallowMultipleComponent_t215_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t215_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t215_0_0_0_Types };
extern const Il2CppType Attribute_t96_0_0_0;
static const Il2CppType* GenInst_Attribute_t96_0_0_0_Types[] = { &Attribute_t96_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t96_0_0_0 = { 1, GenInst_Attribute_t96_0_0_0_Types };
extern const Il2CppType _Attribute_t2052_0_0_0;
static const Il2CppType* GenInst__Attribute_t2052_0_0_0_Types[] = { &_Attribute_t2052_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t2052_0_0_0 = { 1, GenInst__Attribute_t2052_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t218_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t218_0_0_0_Types[] = { &ExecuteInEditMode_t218_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t218_0_0_0 = { 1, GenInst_ExecuteInEditMode_t218_0_0_0_Types };
extern const Il2CppType RequireComponent_t216_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t216_0_0_0_Types[] = { &RequireComponent_t216_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t216_0_0_0 = { 1, GenInst_RequireComponent_t216_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1020_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1020_0_0_0_Types[] = { &ParameterModifier_t1020_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1020_0_0_0 = { 1, GenInst_ParameterModifier_t1020_0_0_0_Types };
extern const Il2CppType HitInfo_t239_0_0_0;
static const Il2CppType* GenInst_HitInfo_t239_0_0_0_Types[] = { &HitInfo_t239_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t239_0_0_0 = { 1, GenInst_HitInfo_t239_0_0_0_Types };
extern const Il2CppType Event_t76_0_0_0;
extern const Il2CppType TextEditOp_t252_0_0_0;
static const Il2CppType* GenInst_Event_t76_0_0_0_TextEditOp_t252_0_0_0_Types[] = { &Event_t76_0_0_0, &TextEditOp_t252_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t76_0_0_0_TextEditOp_t252_0_0_0 = { 2, GenInst_Event_t76_0_0_0_TextEditOp_t252_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t252_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0 = { 2, GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1662_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1662_0_0_0_Types[] = { &KeyValuePair_2_t1662_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1662_0_0_0 = { 1, GenInst_KeyValuePair_2_t1662_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t252_0_0_0_Types[] = { &TextEditOp_t252_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t252_0_0_0 = { 1, GenInst_TextEditOp_t252_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t252_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_TextEditOp_t252_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t252_0_0_0, &TextEditOp_t252_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_TextEditOp_t252_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_TextEditOp_t252_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t252_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_KeyValuePair_2_t1662_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t252_0_0_0, &KeyValuePair_2_t1662_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_KeyValuePair_2_t1662_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_KeyValuePair_2_t1662_0_0_0_Types };
static const Il2CppType* GenInst_Event_t76_0_0_0_Types[] = { &Event_t76_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t76_0_0_0 = { 1, GenInst_Event_t76_0_0_0_Types };
static const Il2CppType* GenInst_Event_t76_0_0_0_TextEditOp_t252_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Event_t76_0_0_0, &TextEditOp_t252_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t76_0_0_0_TextEditOp_t252_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Event_t76_0_0_0_TextEditOp_t252_0_0_0_DictionaryEntry_t560_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1676_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1676_0_0_0_Types[] = { &KeyValuePair_2_t1676_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1676_0_0_0 = { 1, GenInst_KeyValuePair_2_t1676_0_0_0_Types };
extern const Il2CppType UInt16_t351_0_0_0;
static const Il2CppType* GenInst_UInt16_t351_0_0_0_Types[] = { &UInt16_t351_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t351_0_0_0 = { 1, GenInst_UInt16_t351_0_0_0_Types };
extern const Il2CppType IComparable_1_t2231_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2231_0_0_0_Types[] = { &IComparable_1_t2231_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2231_0_0_0 = { 1, GenInst_IComparable_1_t2231_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2232_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2232_0_0_0_Types[] = { &IEquatable_1_t2232_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2232_0_0_0 = { 1, GenInst_IEquatable_1_t2232_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t333_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t333_0_0_0 = { 2, GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1692_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1692_0_0_0_Types[] = { &KeyValuePair_2_t1692_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1692_0_0_0 = { 1, GenInst_KeyValuePair_2_t1692_0_0_0_Types };
extern const Il2CppType IComparable_1_t2244_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2244_0_0_0_Types[] = { &IComparable_1_t2244_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2244_0_0_0 = { 1, GenInst_IComparable_1_t2244_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2245_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2245_0_0_0_Types[] = { &IEquatable_1_t2245_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2245_0_0_0 = { 1, GenInst_IEquatable_1_t2245_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t333_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_Boolean_t333_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t333_0_0_0, &Boolean_t333_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_Boolean_t333_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_Boolean_t333_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t333_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_KeyValuePair_2_t1692_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t333_0_0_0, &KeyValuePair_2_t1692_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_KeyValuePair_2_t1692_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_KeyValuePair_2_t1692_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t333_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t333_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t333_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t333_0_0_0_DictionaryEntry_t560_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1707_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1707_0_0_0_Types[] = { &KeyValuePair_2_t1707_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1707_0_0_0 = { 1, GenInst_KeyValuePair_2_t1707_0_0_0_Types };
extern const Il2CppType X509Certificate_t449_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t449_0_0_0_Types[] = { &X509Certificate_t449_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t449_0_0_0 = { 1, GenInst_X509Certificate_t449_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t1429_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t1429_0_0_0_Types[] = { &IDeserializationCallback_t1429_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1429_0_0_0 = { 1, GenInst_IDeserializationCallback_t1429_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t457_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t457_0_0_0_Types[] = { &X509ChainStatus_t457_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t457_0_0_0 = { 1, GenInst_X509ChainStatus_t457_0_0_0_Types };
extern const Il2CppType Capture_t482_0_0_0;
static const Il2CppType* GenInst_Capture_t482_0_0_0_Types[] = { &Capture_t482_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t482_0_0_0 = { 1, GenInst_Capture_t482_0_0_0_Types };
extern const Il2CppType Group_t485_0_0_0;
static const Il2CppType* GenInst_Group_t485_0_0_0_Types[] = { &Group_t485_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t485_0_0_0 = { 1, GenInst_Group_t485_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_Types[] = { &Int32_t320_0_0_0, &Int32_t320_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0 = { 2, GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1716_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1716_0_0_0_Types[] = { &KeyValuePair_2_t1716_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1716_0_0_0 = { 1, GenInst_KeyValuePair_2_t1716_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_Int32_t320_0_0_0_Types[] = { &Int32_t320_0_0_0, &Int32_t320_0_0_0, &Int32_t320_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_Int32_t320_0_0_0 = { 3, GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_Int32_t320_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Int32_t320_0_0_0, &Int32_t320_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_KeyValuePair_2_t1716_0_0_0_Types[] = { &Int32_t320_0_0_0, &Int32_t320_0_0_0, &KeyValuePair_2_t1716_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_KeyValuePair_2_t1716_0_0_0 = { 3, GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_KeyValuePair_2_t1716_0_0_0_Types };
extern const Il2CppType Mark_t511_0_0_0;
static const Il2CppType* GenInst_Mark_t511_0_0_0_Types[] = { &Mark_t511_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t511_0_0_0 = { 1, GenInst_Mark_t511_0_0_0_Types };
extern const Il2CppType UriScheme_t548_0_0_0;
static const Il2CppType* GenInst_UriScheme_t548_0_0_0_Types[] = { &UriScheme_t548_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t548_0_0_0 = { 1, GenInst_UriScheme_t548_0_0_0_Types };
extern const Il2CppType KeySizes_t621_0_0_0;
static const Il2CppType* GenInst_KeySizes_t621_0_0_0_Types[] = { &KeySizes_t621_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t621_0_0_0 = { 1, GenInst_KeySizes_t621_0_0_0_Types };
extern const Il2CppType UInt32_t336_0_0_0;
static const Il2CppType* GenInst_UInt32_t336_0_0_0_Types[] = { &UInt32_t336_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t336_0_0_0 = { 1, GenInst_UInt32_t336_0_0_0_Types };
extern const Il2CppType IComparable_1_t2221_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2221_0_0_0_Types[] = { &IComparable_1_t2221_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2221_0_0_0 = { 1, GenInst_IComparable_1_t2221_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2222_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2222_0_0_0_Types[] = { &IEquatable_1_t2222_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2222_0_0_0 = { 1, GenInst_IEquatable_1_t2222_0_0_0_Types };
extern const Il2CppType BigInteger_t625_0_0_0;
static const Il2CppType* GenInst_BigInteger_t625_0_0_0_Types[] = { &BigInteger_t625_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t625_0_0_0 = { 1, GenInst_BigInteger_t625_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t713_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t713_0_0_0_Types[] = { &ClientCertificateType_t713_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t713_0_0_0 = { 1, GenInst_ClientCertificateType_t713_0_0_0_Types };
extern const Il2CppType UInt64_t348_0_0_0;
static const Il2CppType* GenInst_UInt64_t348_0_0_0_Types[] = { &UInt64_t348_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t348_0_0_0 = { 1, GenInst_UInt64_t348_0_0_0_Types };
extern const Il2CppType SByte_t349_0_0_0;
static const Il2CppType* GenInst_SByte_t349_0_0_0_Types[] = { &SByte_t349_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t349_0_0_0 = { 1, GenInst_SByte_t349_0_0_0_Types };
extern const Il2CppType Int16_t350_0_0_0;
static const Il2CppType* GenInst_Int16_t350_0_0_0_Types[] = { &Int16_t350_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t350_0_0_0 = { 1, GenInst_Int16_t350_0_0_0_Types };
extern const Il2CppType Double_t344_0_0_0;
static const Il2CppType* GenInst_Double_t344_0_0_0_Types[] = { &Double_t344_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t344_0_0_0 = { 1, GenInst_Double_t344_0_0_0_Types };
extern const Il2CppType Decimal_t347_0_0_0;
static const Il2CppType* GenInst_Decimal_t347_0_0_0_Types[] = { &Decimal_t347_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t347_0_0_0 = { 1, GenInst_Decimal_t347_0_0_0_Types };
extern const Il2CppType Delegate_t332_0_0_0;
static const Il2CppType* GenInst_Delegate_t332_0_0_0_Types[] = { &Delegate_t332_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t332_0_0_0 = { 1, GenInst_Delegate_t332_0_0_0_Types };
extern const Il2CppType IComparable_1_t2223_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2223_0_0_0_Types[] = { &IComparable_1_t2223_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2223_0_0_0 = { 1, GenInst_IComparable_1_t2223_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2224_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2224_0_0_0_Types[] = { &IEquatable_1_t2224_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2224_0_0_0 = { 1, GenInst_IEquatable_1_t2224_0_0_0_Types };
extern const Il2CppType IComparable_1_t2229_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2229_0_0_0_Types[] = { &IComparable_1_t2229_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2229_0_0_0 = { 1, GenInst_IComparable_1_t2229_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2230_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2230_0_0_0_Types[] = { &IEquatable_1_t2230_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2230_0_0_0 = { 1, GenInst_IEquatable_1_t2230_0_0_0_Types };
extern const Il2CppType IComparable_1_t2227_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2227_0_0_0_Types[] = { &IComparable_1_t2227_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2227_0_0_0 = { 1, GenInst_IComparable_1_t2227_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2228_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2228_0_0_0_Types[] = { &IEquatable_1_t2228_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2228_0_0_0 = { 1, GenInst_IEquatable_1_t2228_0_0_0_Types };
extern const Il2CppType IComparable_1_t2240_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2240_0_0_0_Types[] = { &IComparable_1_t2240_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2240_0_0_0 = { 1, GenInst_IComparable_1_t2240_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2241_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2241_0_0_0_Types[] = { &IEquatable_1_t2241_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2241_0_0_0 = { 1, GenInst_IEquatable_1_t2241_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t2105_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t2105_0_0_0_Types[] = { &_MethodInfo_t2105_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t2105_0_0_0 = { 1, GenInst__MethodInfo_t2105_0_0_0_Types };
extern const Il2CppType TableRange_t797_0_0_0;
static const Il2CppType* GenInst_TableRange_t797_0_0_0_Types[] = { &TableRange_t797_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t797_0_0_0 = { 1, GenInst_TableRange_t797_0_0_0_Types };
extern const Il2CppType TailoringInfo_t800_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t800_0_0_0_Types[] = { &TailoringInfo_t800_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t800_0_0_0 = { 1, GenInst_TailoringInfo_t800_0_0_0_Types };
extern const Il2CppType Contraction_t801_0_0_0;
static const Il2CppType* GenInst_Contraction_t801_0_0_0_Types[] = { &Contraction_t801_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t801_0_0_0 = { 1, GenInst_Contraction_t801_0_0_0_Types };
extern const Il2CppType Level2Map_t803_0_0_0;
static const Il2CppType* GenInst_Level2Map_t803_0_0_0_Types[] = { &Level2Map_t803_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t803_0_0_0 = { 1, GenInst_Level2Map_t803_0_0_0_Types };
extern const Il2CppType BigInteger_t823_0_0_0;
static const Il2CppType* GenInst_BigInteger_t823_0_0_0_Types[] = { &BigInteger_t823_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t823_0_0_0 = { 1, GenInst_BigInteger_t823_0_0_0_Types };
extern const Il2CppType Slot_t874_0_0_0;
static const Il2CppType* GenInst_Slot_t874_0_0_0_Types[] = { &Slot_t874_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t874_0_0_0 = { 1, GenInst_Slot_t874_0_0_0_Types };
extern const Il2CppType Slot_t881_0_0_0;
static const Il2CppType* GenInst_Slot_t881_0_0_0_Types[] = { &Slot_t881_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t881_0_0_0 = { 1, GenInst_Slot_t881_0_0_0_Types };
extern const Il2CppType StackFrame_t378_0_0_0;
static const Il2CppType* GenInst_StackFrame_t378_0_0_0_Types[] = { &StackFrame_t378_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t378_0_0_0 = { 1, GenInst_StackFrame_t378_0_0_0_Types };
extern const Il2CppType Calendar_t895_0_0_0;
static const Il2CppType* GenInst_Calendar_t895_0_0_0_Types[] = { &Calendar_t895_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t895_0_0_0 = { 1, GenInst_Calendar_t895_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t971_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t971_0_0_0_Types[] = { &ModuleBuilder_t971_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t971_0_0_0 = { 1, GenInst_ModuleBuilder_t971_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t2095_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t2095_0_0_0_Types[] = { &_ModuleBuilder_t2095_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t2095_0_0_0 = { 1, GenInst__ModuleBuilder_t2095_0_0_0_Types };
extern const Il2CppType Module_t965_0_0_0;
static const Il2CppType* GenInst_Module_t965_0_0_0_Types[] = { &Module_t965_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t965_0_0_0 = { 1, GenInst_Module_t965_0_0_0_Types };
extern const Il2CppType _Module_t2106_0_0_0;
static const Il2CppType* GenInst__Module_t2106_0_0_0_Types[] = { &_Module_t2106_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2106_0_0_0 = { 1, GenInst__Module_t2106_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t975_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t975_0_0_0_Types[] = { &ParameterBuilder_t975_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t975_0_0_0 = { 1, GenInst_ParameterBuilder_t975_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2096_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2096_0_0_0_Types[] = { &_ParameterBuilder_t2096_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2096_0_0_0 = { 1, GenInst__ParameterBuilder_t2096_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t196_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t196_0_0_0_Types[] = { &TypeU5BU5D_t196_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t196_0_0_0 = { 1, GenInst_TypeU5BU5D_t196_0_0_0_Types };
extern const Il2CppType Array_t_0_0_0;
static const Il2CppType* GenInst_Array_t_0_0_0_Types[] = { &Array_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_t_0_0_0 = { 1, GenInst_Array_t_0_0_0_Types };
extern const Il2CppType ICollection_t569_0_0_0;
static const Il2CppType* GenInst_ICollection_t569_0_0_0_Types[] = { &ICollection_t569_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t569_0_0_0 = { 1, GenInst_ICollection_t569_0_0_0_Types };
extern const Il2CppType IList_t519_0_0_0;
static const Il2CppType* GenInst_IList_t519_0_0_0_Types[] = { &IList_t519_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t519_0_0_0 = { 1, GenInst_IList_t519_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t959_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t959_0_0_0_Types[] = { &ILTokenInfo_t959_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t959_0_0_0 = { 1, GenInst_ILTokenInfo_t959_0_0_0_Types };
extern const Il2CppType LabelData_t961_0_0_0;
static const Il2CppType* GenInst_LabelData_t961_0_0_0_Types[] = { &LabelData_t961_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t961_0_0_0 = { 1, GenInst_LabelData_t961_0_0_0_Types };
extern const Il2CppType LabelFixup_t960_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t960_0_0_0_Types[] = { &LabelFixup_t960_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t960_0_0_0 = { 1, GenInst_LabelFixup_t960_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t958_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t958_0_0_0_Types[] = { &GenericTypeParameterBuilder_t958_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t958_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t958_0_0_0_Types };
extern const Il2CppType TypeBuilder_t950_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t950_0_0_0_Types[] = { &TypeBuilder_t950_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t950_0_0_0 = { 1, GenInst_TypeBuilder_t950_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2098_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2098_0_0_0_Types[] = { &_TypeBuilder_t2098_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2098_0_0_0 = { 1, GenInst__TypeBuilder_t2098_0_0_0_Types };
extern const Il2CppType MethodBuilder_t957_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t957_0_0_0_Types[] = { &MethodBuilder_t957_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t957_0_0_0 = { 1, GenInst_MethodBuilder_t957_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t2094_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t2094_0_0_0_Types[] = { &_MethodBuilder_t2094_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t2094_0_0_0 = { 1, GenInst__MethodBuilder_t2094_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t953_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t953_0_0_0_Types[] = { &ConstructorBuilder_t953_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t953_0_0_0 = { 1, GenInst_ConstructorBuilder_t953_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t2090_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t2090_0_0_0_Types[] = { &_ConstructorBuilder_t2090_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t2090_0_0_0 = { 1, GenInst__ConstructorBuilder_t2090_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t976_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t976_0_0_0_Types[] = { &PropertyBuilder_t976_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t976_0_0_0 = { 1, GenInst_PropertyBuilder_t976_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t2097_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t2097_0_0_0_Types[] = { &_PropertyBuilder_t2097_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t2097_0_0_0 = { 1, GenInst__PropertyBuilder_t2097_0_0_0_Types };
extern const Il2CppType FieldBuilder_t956_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t956_0_0_0_Types[] = { &FieldBuilder_t956_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t956_0_0_0 = { 1, GenInst_FieldBuilder_t956_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t2092_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t2092_0_0_0_Types[] = { &_FieldBuilder_t2092_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t2092_0_0_0 = { 1, GenInst__FieldBuilder_t2092_0_0_0_Types };
extern const Il2CppType Header_t1091_0_0_0;
static const Il2CppType* GenInst_Header_t1091_0_0_0_Types[] = { &Header_t1091_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t1091_0_0_0 = { 1, GenInst_Header_t1091_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t1421_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t1421_0_0_0_Types[] = { &ITrackingHandler_t1421_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t1421_0_0_0 = { 1, GenInst_ITrackingHandler_t1421_0_0_0_Types };
extern const Il2CppType IContextAttribute_t1413_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t1413_0_0_0_Types[] = { &IContextAttribute_t1413_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t1413_0_0_0 = { 1, GenInst_IContextAttribute_t1413_0_0_0_Types };
extern const Il2CppType IComparable_1_t2376_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2376_0_0_0_Types[] = { &IComparable_1_t2376_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2376_0_0_0 = { 1, GenInst_IComparable_1_t2376_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2377_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2377_0_0_0_Types[] = { &IEquatable_1_t2377_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2377_0_0_0 = { 1, GenInst_IEquatable_1_t2377_0_0_0_Types };
extern const Il2CppType IComparable_1_t2242_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2242_0_0_0_Types[] = { &IComparable_1_t2242_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2242_0_0_0 = { 1, GenInst_IComparable_1_t2242_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2243_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2243_0_0_0_Types[] = { &IEquatable_1_t2243_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2243_0_0_0 = { 1, GenInst_IEquatable_1_t2243_0_0_0_Types };
extern const Il2CppType IComparable_1_t2386_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2386_0_0_0_Types[] = { &IComparable_1_t2386_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2386_0_0_0 = { 1, GenInst_IComparable_1_t2386_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2387_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2387_0_0_0_Types[] = { &IEquatable_1_t2387_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2387_0_0_0 = { 1, GenInst_IEquatable_1_t2387_0_0_0_Types };
extern const Il2CppType TypeTag_t1145_0_0_0;
static const Il2CppType* GenInst_TypeTag_t1145_0_0_0_Types[] = { &TypeTag_t1145_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t1145_0_0_0 = { 1, GenInst_TypeTag_t1145_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType Version_t418_0_0_0;
static const Il2CppType* GenInst_Version_t418_0_0_0_Types[] = { &Version_t418_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t418_0_0_0 = { 1, GenInst_Version_t418_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t560_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &DictionaryEntry_t560_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t560_0_0_0_DictionaryEntry_t560_0_0_0 = { 2, GenInst_DictionaryEntry_t560_0_0_0_DictionaryEntry_t560_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1465_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1465_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1465_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1465_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1465_0_0_0_KeyValuePair_2_t1465_0_0_0_Types[] = { &KeyValuePair_2_t1465_0_0_0, &KeyValuePair_2_t1465_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1465_0_0_0_KeyValuePair_2_t1465_0_0_0 = { 2, GenInst_KeyValuePair_2_t1465_0_0_0_KeyValuePair_2_t1465_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1493_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1493_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1493_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1493_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1493_0_0_0_KeyValuePair_2_t1493_0_0_0_Types[] = { &KeyValuePair_2_t1493_0_0_0, &KeyValuePair_2_t1493_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1493_0_0_0_KeyValuePair_2_t1493_0_0_0 = { 2, GenInst_KeyValuePair_2_t1493_0_0_0_KeyValuePair_2_t1493_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1509_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1509_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1509_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1509_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1509_0_0_0_KeyValuePair_2_t1509_0_0_0_Types[] = { &KeyValuePair_2_t1509_0_0_0, &KeyValuePair_2_t1509_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1509_0_0_0_KeyValuePair_2_t1509_0_0_0 = { 2, GenInst_KeyValuePair_2_t1509_0_0_0_KeyValuePair_2_t1509_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t345_0_0_0_Object_t_0_0_0_Types[] = { &Int64_t345_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t345_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int64_t345_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t345_0_0_0_Int64_t345_0_0_0_Types[] = { &Int64_t345_0_0_0, &Int64_t345_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t345_0_0_0_Int64_t345_0_0_0 = { 2, GenInst_Int64_t345_0_0_0_Int64_t345_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1552_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1552_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1552_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1552_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1552_0_0_0_KeyValuePair_2_t1552_0_0_0_Types[] = { &KeyValuePair_2_t1552_0_0_0, &KeyValuePair_2_t1552_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1552_0_0_0_KeyValuePair_2_t1552_0_0_0 = { 2, GenInst_KeyValuePair_2_t1552_0_0_0_KeyValuePair_2_t1552_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t178_0_0_0_NetworkID_t178_0_0_0_Types[] = { &NetworkID_t178_0_0_0, &NetworkID_t178_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t178_0_0_0_NetworkID_t178_0_0_0 = { 2, GenInst_NetworkID_t178_0_0_0_NetworkID_t178_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1581_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1581_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1581_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1581_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1581_0_0_0_KeyValuePair_2_t1581_0_0_0_Types[] = { &KeyValuePair_2_t1581_0_0_0, &KeyValuePair_2_t1581_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1581_0_0_0_KeyValuePair_2_t1581_0_0_0 = { 2, GenInst_KeyValuePair_2_t1581_0_0_0_KeyValuePair_2_t1581_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1601_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1601_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1601_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1601_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1601_0_0_0_KeyValuePair_2_t1601_0_0_0_Types[] = { &KeyValuePair_2_t1601_0_0_0, &KeyValuePair_2_t1601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1601_0_0_0_KeyValuePair_2_t1601_0_0_0 = { 2, GenInst_KeyValuePair_2_t1601_0_0_0_KeyValuePair_2_t1601_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t252_0_0_0_Object_t_0_0_0_Types[] = { &TextEditOp_t252_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t252_0_0_0_Object_t_0_0_0 = { 2, GenInst_TextEditOp_t252_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t252_0_0_0_TextEditOp_t252_0_0_0_Types[] = { &TextEditOp_t252_0_0_0, &TextEditOp_t252_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t252_0_0_0_TextEditOp_t252_0_0_0 = { 2, GenInst_TextEditOp_t252_0_0_0_TextEditOp_t252_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1662_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1662_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1662_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1662_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1662_0_0_0_KeyValuePair_2_t1662_0_0_0_Types[] = { &KeyValuePair_2_t1662_0_0_0, &KeyValuePair_2_t1662_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1662_0_0_0_KeyValuePair_2_t1662_0_0_0 = { 2, GenInst_KeyValuePair_2_t1662_0_0_0_KeyValuePair_2_t1662_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t333_0_0_0_Object_t_0_0_0_Types[] = { &Boolean_t333_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t333_0_0_0_Object_t_0_0_0 = { 2, GenInst_Boolean_t333_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t333_0_0_0_Boolean_t333_0_0_0_Types[] = { &Boolean_t333_0_0_0, &Boolean_t333_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t333_0_0_0_Boolean_t333_0_0_0 = { 2, GenInst_Boolean_t333_0_0_0_Boolean_t333_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1692_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1692_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1692_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1692_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1692_0_0_0_KeyValuePair_2_t1692_0_0_0_Types[] = { &KeyValuePair_2_t1692_0_0_0, &KeyValuePair_2_t1692_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1692_0_0_0_KeyValuePair_2_t1692_0_0_0 = { 2, GenInst_KeyValuePair_2_t1692_0_0_0_KeyValuePair_2_t1692_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1716_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1716_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1716_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1716_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1716_0_0_0_KeyValuePair_2_t1716_0_0_0_Types[] = { &KeyValuePair_2_t1716_0_0_0, &KeyValuePair_2_t1716_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1716_0_0_0_KeyValuePair_2_t1716_0_0_0 = { 2, GenInst_KeyValuePair_2_t1716_0_0_0_KeyValuePair_2_t1716_0_0_0_Types };
extern const Il2CppType ResponseBase_ParseJSONList_m12758_gp_0_0_0_0;
static const Il2CppType* GenInst_ResponseBase_ParseJSONList_m12758_gp_0_0_0_0_Types[] = { &ResponseBase_ParseJSONList_m12758_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseBase_ParseJSONList_m12758_gp_0_0_0_0 = { 1, GenInst_ResponseBase_ParseJSONList_m12758_gp_0_0_0_0_Types };
extern const Il2CppType NetworkMatch_ProcessMatchResponse_m12759_gp_0_0_0_0;
static const Il2CppType* GenInst_NetworkMatch_ProcessMatchResponse_m12759_gp_0_0_0_0_Types[] = { &NetworkMatch_ProcessMatchResponse_m12759_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkMatch_ProcessMatchResponse_m12759_gp_0_0_0_0 = { 1, GenInst_NetworkMatch_ProcessMatchResponse_m12759_gp_0_0_0_0_Types };
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_gp_0_0_0_0_Types[] = { &U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_gp_0_0_0_0 = { 1, GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_gp_0_0_0_0_Types };
extern const Il2CppType ThreadSafeDictionary_2_t2041_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t2041_gp_1_0_0_0;
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t2041_gp_0_0_0_0_ThreadSafeDictionary_2_t2041_gp_1_0_0_0_Types[] = { &ThreadSafeDictionary_2_t2041_gp_0_0_0_0, &ThreadSafeDictionary_2_t2041_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t2041_gp_0_0_0_0_ThreadSafeDictionary_2_t2041_gp_1_0_0_0 = { 2, GenInst_ThreadSafeDictionary_2_t2041_gp_0_0_0_0_ThreadSafeDictionary_2_t2041_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t2041_gp_0_0_0_0_Types[] = { &ThreadSafeDictionary_2_t2041_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t2041_gp_0_0_0_0 = { 1, GenInst_ThreadSafeDictionary_2_t2041_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t2041_gp_1_0_0_0_Types[] = { &ThreadSafeDictionary_2_t2041_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t2041_gp_1_0_0_0 = { 1, GenInst_ThreadSafeDictionary_2_t2041_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3008_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3008_0_0_0_Types[] = { &KeyValuePair_2_t3008_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3008_0_0_0 = { 1, GenInst_KeyValuePair_2_t3008_0_0_0_Types };
extern const Il2CppType Stack_1_t2049_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t2049_gp_0_0_0_0_Types[] = { &Stack_1_t2049_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t2049_gp_0_0_0_0 = { 1, GenInst_Stack_1_t2049_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2050_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2050_gp_0_0_0_0_Types[] = { &Enumerator_t2050_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2050_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2050_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2057_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2057_gp_0_0_0_0_Types[] = { &IEnumerable_1_t2057_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2057_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t2057_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m12908_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m12908_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m12908_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m12908_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m12908_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m12922_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m12922_gp_0_0_0_0_Types[] = { &Array_Sort_m12922_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12922_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m12922_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m12923_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m12923_gp_0_0_0_0_Types[] = { &Array_Sort_m12923_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12923_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m12923_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m12926_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m12926_gp_0_0_0_0_Types[] = { &Array_Sort_m12926_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12926_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m12926_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m12927_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m12927_gp_0_0_0_0_Types[] = { &Array_Sort_m12927_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12927_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m12927_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m12928_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m12928_gp_0_0_0_0_Types[] = { &Array_Sort_m12928_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12928_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m12928_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m12929_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m12929_gp_0_0_0_0_Types[] = { &Array_Sort_m12929_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12929_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m12929_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m12930_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m12930_gp_0_0_0_0_Types[] = { &Array_qsort_m12930_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m12930_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m12930_gp_0_0_0_0_Types };
extern const Il2CppType Array_compare_m12931_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m12931_gp_0_0_0_0_Types[] = { &Array_compare_m12931_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m12931_gp_0_0_0_0 = { 1, GenInst_Array_compare_m12931_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m12932_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m12932_gp_0_0_0_0_Types[] = { &Array_qsort_m12932_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m12932_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m12932_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m12937_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m12937_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m12937_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m12937_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m12937_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m12938_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m12938_gp_0_0_0_0_Types[] = { &Array_ForEach_m12938_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m12938_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m12938_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m12939_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m12939_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m12939_gp_0_0_0_0_Array_ConvertAll_m12939_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m12939_gp_0_0_0_0, &Array_ConvertAll_m12939_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m12939_gp_0_0_0_0_Array_ConvertAll_m12939_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m12939_gp_0_0_0_0_Array_ConvertAll_m12939_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m12940_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m12940_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m12940_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m12940_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m12940_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m12941_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m12941_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m12941_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m12941_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m12941_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m12942_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m12942_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m12942_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m12942_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m12942_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m12943_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m12943_gp_0_0_0_0_Types[] = { &Array_FindIndex_m12943_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m12943_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m12943_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m12944_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m12944_gp_0_0_0_0_Types[] = { &Array_FindIndex_m12944_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m12944_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m12944_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m12945_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m12945_gp_0_0_0_0_Types[] = { &Array_FindIndex_m12945_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m12945_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m12945_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m12947_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m12947_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m12947_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m12947_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m12947_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m12949_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m12949_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m12949_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m12949_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m12949_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m12956_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m12956_gp_0_0_0_0_Types[] = { &Array_FindAll_m12956_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m12956_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m12956_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m12957_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m12957_gp_0_0_0_0_Types[] = { &Array_Exists_m12957_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m12957_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m12957_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m12958_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m12958_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m12958_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m12958_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m12958_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m12959_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m12959_gp_0_0_0_0_Types[] = { &Array_Find_m12959_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m12959_gp_0_0_0_0 = { 1, GenInst_Array_Find_m12959_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m12960_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m12960_gp_0_0_0_0_Types[] = { &Array_FindLast_m12960_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m12960_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m12960_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t2059_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t2059_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t2059_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t2059_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t2059_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t2060_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2060_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t2060_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2060_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2060_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1410_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1410_gp_0_0_0_0_Types[] = { &Nullable_1_t1410_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1410_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1410_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t2069_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t2069_gp_0_0_0_0_Types[] = { &Comparer_1_t2069_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t2069_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t2069_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t2036_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t2036_gp_0_0_0_0_Types[] = { &GenericComparer_1_t2036_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t2036_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t2036_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2071_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2071_gp_0_0_0_0_Types[] = { &Dictionary_2_t2071_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2071_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2071_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2071_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_Types[] = { &Dictionary_2_t2071_gp_0_0_0_0, &Dictionary_2_t2071_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2071_gp_1_0_0_0_Types[] = { &Dictionary_2_t2071_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2071_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2071_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2309_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2309_0_0_0_Types[] = { &KeyValuePair_2_t2309_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2309_0_0_0 = { 1, GenInst_KeyValuePair_2_t2309_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m13108_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m13108_gp_0_0_0_0_Types[] = { &Dictionary_2_t2071_gp_0_0_0_0, &Dictionary_2_t2071_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m13108_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m13108_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m13108_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m13113_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m13113_gp_0_0_0_0_Types[] = { &Dictionary_2_t2071_gp_0_0_0_0, &Dictionary_2_t2071_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m13113_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m13113_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m13113_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_DictionaryEntry_t560_0_0_0_Types[] = { &Dictionary_2_t2071_gp_0_0_0_0, &Dictionary_2_t2071_gp_1_0_0_0, &DictionaryEntry_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_DictionaryEntry_t560_0_0_0 = { 3, GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_DictionaryEntry_t560_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t2072_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t2072_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t2072_gp_0_0_0_0_ShimEnumerator_t2072_gp_1_0_0_0_Types[] = { &ShimEnumerator_t2072_gp_0_0_0_0, &ShimEnumerator_t2072_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t2072_gp_0_0_0_0_ShimEnumerator_t2072_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t2072_gp_0_0_0_0_ShimEnumerator_t2072_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2073_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2073_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2073_gp_0_0_0_0_Enumerator_t2073_gp_1_0_0_0_Types[] = { &Enumerator_t2073_gp_0_0_0_0, &Enumerator_t2073_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2073_gp_0_0_0_0_Enumerator_t2073_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2073_gp_0_0_0_0_Enumerator_t2073_gp_1_0_0_0_Types };
extern const Il2CppType KeyCollection_t2074_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t2074_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t2074_gp_0_0_0_0_KeyCollection_t2074_gp_1_0_0_0_Types[] = { &KeyCollection_t2074_gp_0_0_0_0, &KeyCollection_t2074_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2074_gp_0_0_0_0_KeyCollection_t2074_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t2074_gp_0_0_0_0_KeyCollection_t2074_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2074_gp_0_0_0_0_Types[] = { &KeyCollection_t2074_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2074_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t2074_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2075_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2075_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2075_gp_0_0_0_0_Enumerator_t2075_gp_1_0_0_0_Types[] = { &Enumerator_t2075_gp_0_0_0_0, &Enumerator_t2075_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2075_gp_0_0_0_0_Enumerator_t2075_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2075_gp_0_0_0_0_Enumerator_t2075_gp_1_0_0_0_Types };
extern const Il2CppType ValueCollection_t2076_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2076_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2076_gp_0_0_0_0_ValueCollection_t2076_gp_1_0_0_0_Types[] = { &ValueCollection_t2076_gp_0_0_0_0, &ValueCollection_t2076_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2076_gp_0_0_0_0_ValueCollection_t2076_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2076_gp_0_0_0_0_ValueCollection_t2076_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2076_gp_1_0_0_0_Types[] = { &ValueCollection_t2076_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2076_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2076_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2077_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2077_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2077_gp_0_0_0_0_Enumerator_t2077_gp_1_0_0_0_Types[] = { &Enumerator_t2077_gp_0_0_0_0, &Enumerator_t2077_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2077_gp_0_0_0_0_Enumerator_t2077_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2077_gp_0_0_0_0_Enumerator_t2077_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2079_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2079_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2079_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2079_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2079_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2035_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2035_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2035_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2035_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2035_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2082_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t2082_gp_0_0_0_0_Types[] = { &IDictionary_2_t2082_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2082_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t2082_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2082_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t2082_gp_1_0_0_0_Types[] = { &IDictionary_2_t2082_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2082_gp_1_0_0_0 = { 1, GenInst_IDictionary_2_t2082_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t2085_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t2085_gp_0_0_0_0_Types[] = { &List_1_t2085_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2085_gp_0_0_0_0 = { 1, GenInst_List_1_t2085_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2086_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2086_gp_0_0_0_0_Types[] = { &Enumerator_t2086_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2086_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2086_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t2087_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t2087_gp_0_0_0_0_Types[] = { &Collection_1_t2087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t2087_gp_0_0_0_0 = { 1, GenInst_Collection_1_t2087_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t2088_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t2088_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t2088_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2088_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t2088_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m13363_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m13363_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m13363_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m13363_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m13363_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m13363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m13363_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m13363_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m13363_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m13363_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m13364_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m13364_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m13364_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m13364_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m13364_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m12920_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m12920_gp_0_0_0_0_Array_Sort_m12920_gp_0_0_0_0_Types[] = { &Array_Sort_m12920_gp_0_0_0_0, &Array_Sort_m12920_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12920_gp_0_0_0_0_Array_Sort_m12920_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m12920_gp_0_0_0_0_Array_Sort_m12920_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m12921_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m12921_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m12921_gp_0_0_0_0_Array_Sort_m12921_gp_1_0_0_0_Types[] = { &Array_Sort_m12921_gp_0_0_0_0, &Array_Sort_m12921_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12921_gp_0_0_0_0_Array_Sort_m12921_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m12921_gp_0_0_0_0_Array_Sort_m12921_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m12922_gp_0_0_0_0_Array_Sort_m12922_gp_0_0_0_0_Types[] = { &Array_Sort_m12922_gp_0_0_0_0, &Array_Sort_m12922_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12922_gp_0_0_0_0_Array_Sort_m12922_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m12922_gp_0_0_0_0_Array_Sort_m12922_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m12923_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m12923_gp_0_0_0_0_Array_Sort_m12923_gp_1_0_0_0_Types[] = { &Array_Sort_m12923_gp_0_0_0_0, &Array_Sort_m12923_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12923_gp_0_0_0_0_Array_Sort_m12923_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m12923_gp_0_0_0_0_Array_Sort_m12923_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m12924_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m12924_gp_0_0_0_0_Array_Sort_m12924_gp_0_0_0_0_Types[] = { &Array_Sort_m12924_gp_0_0_0_0, &Array_Sort_m12924_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12924_gp_0_0_0_0_Array_Sort_m12924_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m12924_gp_0_0_0_0_Array_Sort_m12924_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m12925_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m12925_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m12925_gp_0_0_0_0_Array_Sort_m12925_gp_1_0_0_0_Types[] = { &Array_Sort_m12925_gp_0_0_0_0, &Array_Sort_m12925_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12925_gp_0_0_0_0_Array_Sort_m12925_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m12925_gp_0_0_0_0_Array_Sort_m12925_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m12926_gp_0_0_0_0_Array_Sort_m12926_gp_0_0_0_0_Types[] = { &Array_Sort_m12926_gp_0_0_0_0, &Array_Sort_m12926_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12926_gp_0_0_0_0_Array_Sort_m12926_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m12926_gp_0_0_0_0_Array_Sort_m12926_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m12927_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m12927_gp_1_0_0_0_Types[] = { &Array_Sort_m12927_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12927_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m12927_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m12927_gp_0_0_0_0_Array_Sort_m12927_gp_1_0_0_0_Types[] = { &Array_Sort_m12927_gp_0_0_0_0, &Array_Sort_m12927_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m12927_gp_0_0_0_0_Array_Sort_m12927_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m12927_gp_0_0_0_0_Array_Sort_m12927_gp_1_0_0_0_Types };
extern const Il2CppType Array_qsort_m12930_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m12930_gp_0_0_0_0_Array_qsort_m12930_gp_1_0_0_0_Types[] = { &Array_qsort_m12930_gp_0_0_0_0, &Array_qsort_m12930_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m12930_gp_0_0_0_0_Array_qsort_m12930_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m12930_gp_0_0_0_0_Array_qsort_m12930_gp_1_0_0_0_Types };
extern const Il2CppType Array_Resize_m12935_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m12935_gp_0_0_0_0_Types[] = { &Array_Resize_m12935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m12935_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m12935_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m12946_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m12946_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m12946_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m12946_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m12946_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m12948_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m12948_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m12948_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m12948_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m12948_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m12950_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m12950_gp_0_0_0_0_Types[] = { &Array_IndexOf_m12950_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m12950_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m12950_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m12951_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m12951_gp_0_0_0_0_Types[] = { &Array_IndexOf_m12951_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m12951_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m12951_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m12952_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m12952_gp_0_0_0_0_Types[] = { &Array_IndexOf_m12952_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m12952_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m12952_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m12953_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m12953_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m12953_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m12953_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m12953_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m12954_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m12954_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m12954_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m12954_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m12954_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m12955_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m12955_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m12955_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m12955_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m12955_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t2058_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t2058_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t2058_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t2058_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t2058_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t2061_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t2061_gp_0_0_0_0_Types[] = { &IList_1_t2061_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2061_gp_0_0_0_0 = { 1, GenInst_IList_1_t2061_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t2062_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2062_gp_0_0_0_0_Types[] = { &ICollection_1_t2062_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2062_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t2062_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t2070_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t2070_gp_0_0_0_0_Types[] = { &DefaultComparer_t2070_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t2070_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t2070_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m13113_gp_0_0_0_0_Object_t_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m13113_gp_0_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m13113_gp_0_0_0_0_Object_t_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m13113_gp_0_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_KeyValuePair_2_t2309_0_0_0_Types[] = { &Dictionary_2_t2071_gp_0_0_0_0, &Dictionary_2_t2071_gp_1_0_0_0, &KeyValuePair_2_t2309_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_KeyValuePair_2_t2309_0_0_0 = { 3, GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_KeyValuePair_2_t2309_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2309_0_0_0_KeyValuePair_2_t2309_0_0_0_Types[] = { &KeyValuePair_2_t2309_0_0_0, &KeyValuePair_2_t2309_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2309_0_0_0_KeyValuePair_2_t2309_0_0_0 = { 2, GenInst_KeyValuePair_2_t2309_0_0_0_KeyValuePair_2_t2309_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2318_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2318_0_0_0_Types[] = { &KeyValuePair_2_t2318_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2318_0_0_0 = { 1, GenInst_KeyValuePair_2_t2318_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2074_gp_0_0_0_0_KeyCollection_t2074_gp_1_0_0_0_KeyCollection_t2074_gp_0_0_0_0_Types[] = { &KeyCollection_t2074_gp_0_0_0_0, &KeyCollection_t2074_gp_1_0_0_0, &KeyCollection_t2074_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2074_gp_0_0_0_0_KeyCollection_t2074_gp_1_0_0_0_KeyCollection_t2074_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t2074_gp_0_0_0_0_KeyCollection_t2074_gp_1_0_0_0_KeyCollection_t2074_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2074_gp_0_0_0_0_KeyCollection_t2074_gp_0_0_0_0_Types[] = { &KeyCollection_t2074_gp_0_0_0_0, &KeyCollection_t2074_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2074_gp_0_0_0_0_KeyCollection_t2074_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t2074_gp_0_0_0_0_KeyCollection_t2074_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t2075_gp_0_0_0_0_Types[] = { &Enumerator_t2075_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2075_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2075_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2076_gp_0_0_0_0_ValueCollection_t2076_gp_1_0_0_0_ValueCollection_t2076_gp_1_0_0_0_Types[] = { &ValueCollection_t2076_gp_0_0_0_0, &ValueCollection_t2076_gp_1_0_0_0, &ValueCollection_t2076_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2076_gp_0_0_0_0_ValueCollection_t2076_gp_1_0_0_0_ValueCollection_t2076_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2076_gp_0_0_0_0_ValueCollection_t2076_gp_1_0_0_0_ValueCollection_t2076_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2076_gp_1_0_0_0_ValueCollection_t2076_gp_1_0_0_0_Types[] = { &ValueCollection_t2076_gp_1_0_0_0, &ValueCollection_t2076_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2076_gp_1_0_0_0_ValueCollection_t2076_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2076_gp_1_0_0_0_ValueCollection_t2076_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t2077_gp_1_0_0_0_Types[] = { &Enumerator_t2077_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2077_gp_1_0_0_0 = { 1, GenInst_Enumerator_t2077_gp_1_0_0_0_Types };
extern const Il2CppType DefaultComparer_t2080_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t2080_gp_0_0_0_0_Types[] = { &DefaultComparer_t2080_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t2080_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t2080_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3060_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3060_0_0_0_Types[] = { &KeyValuePair_2_t3060_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3060_0_0_0 = { 1, GenInst_KeyValuePair_2_t3060_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t2082_gp_0_0_0_0_IDictionary_2_t2082_gp_1_0_0_0_Types[] = { &IDictionary_2_t2082_gp_0_0_0_0, &IDictionary_2_t2082_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2082_gp_0_0_0_0_IDictionary_2_t2082_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t2082_gp_0_0_0_0_IDictionary_2_t2082_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2084_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t2084_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2084_gp_0_0_0_0_KeyValuePair_2_t2084_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t2084_gp_0_0_0_0, &KeyValuePair_2_t2084_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2084_gp_0_0_0_0_KeyValuePair_2_t2084_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t2084_gp_0_0_0_0_KeyValuePair_2_t2084_gp_1_0_0_0_Types };
extern const Il2CppGenericInst* g_Il2CppGenericInstTable[411] = 
{
	&GenInst_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_GcLeaderboard_t27_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t314_0_0_0,
	&GenInst_Boolean_t333_0_0_0,
	&GenInst_IAchievementU5BU5D_t316_0_0_0,
	&GenInst_IScoreU5BU5D_t237_0_0_0,
	&GenInst_IUserProfileU5BU5D_t231_0_0_0,
	&GenInst_Int32_t320_0_0_0_LayoutCache_t54_0_0_0,
	&GenInst_GUILayoutEntry_t58_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t56_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_ByteU5BU5D_t102_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_Font_t69_0_0_0,
	&GenInst_UIVertex_t158_0_0_0,
	&GenInst_UICharInfo_t149_0_0_0,
	&GenInst_UILineInfo_t150_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0,
	&GenInst_MatchDirectConnectInfo_t171_0_0_0,
	&GenInst_MatchDesc_t173_0_0_0,
	&GenInst_NetworkID_t178_0_0_0_NetworkAccessToken_t180_0_0_0,
	&GenInst_CreateMatchResponse_t165_0_0_0,
	&GenInst_JoinMatchResponse_t167_0_0_0,
	&GenInst_BasicResponse_t162_0_0_0,
	&GenInst_ListMatchResponse_t175_0_0_0,
	&GenInst_KeyValuePair_2_t298_0_0_0,
	&GenInst_Type_t_0_0_0_ConstructorDelegate_t201_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t303_0_0_0,
	&GenInst_String_t_0_0_0_GetDelegate_t198_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t304_0_0_0,
	&GenInst_String_t_0_0_0_KeyValuePair_2_t362_0_0_0,
	&GenInst_Type_t_0_0_0_SetDelegate_t199_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst_KeyValuePair_2_t365_0_0_0,
	&GenInst_ConstructorInfo_t202_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_GUILayer_t36_0_0_0,
	&GenInst_PersistentCall_t258_0_0_0,
	&GenInst_BaseInvokableCall_t256_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t333_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t320_0_0_0,
	&GenInst_Int32_t320_0_0_0,
	&GenInst_StrongName_t1226_0_0_0,
	&GenInst_DateTime_t51_0_0_0,
	&GenInst_DateTimeOffset_t363_0_0_0,
	&GenInst_TimeSpan_t463_0_0_0,
	&GenInst_Guid_t364_0_0_0,
	&GenInst_Object_t5_0_0_0,
	&GenInst_IConvertible_t1409_0_0_0,
	&GenInst_IComparable_t1408_0_0_0,
	&GenInst_IEnumerable_t302_0_0_0,
	&GenInst_ICloneable_t2056_0_0_0,
	&GenInst_IComparable_1_t2235_0_0_0,
	&GenInst_IEquatable_1_t2236_0_0_0,
	&GenInst_IAchievementDescription_t1804_0_0_0,
	&GenInst_IAchievement_t277_0_0_0,
	&GenInst_IScore_t236_0_0_0,
	&GenInst_IUserProfile_t1805_0_0_0,
	&GenInst_AchievementDescription_t234_0_0_0,
	&GenInst_UserProfile_t232_0_0_0,
	&GenInst_IReflect_t2065_0_0_0,
	&GenInst__Type_t2063_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t1404_0_0_0,
	&GenInst__MemberInfo_t2064_0_0_0,
	&GenInst_GcAchievementData_t224_0_0_0,
	&GenInst_ValueType_t757_0_0_0,
	&GenInst_Achievement_t233_0_0_0,
	&GenInst_GcScoreData_t225_0_0_0,
	&GenInst_Score_t235_0_0_0,
	&GenInst_GUILayoutOption_t63_0_0_0,
	&GenInst_Int32_t320_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1465_0_0_0,
	&GenInst_IFormattable_t1406_0_0_0,
	&GenInst_IComparable_1_t2217_0_0_0,
	&GenInst_IEquatable_1_t2218_0_0_0,
	&GenInst_Link_t864_0_0_0,
	&GenInst_Int32_t320_0_0_0_Object_t_0_0_0_Int32_t320_0_0_0,
	&GenInst_Int32_t320_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Int32_t320_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_DictionaryEntry_t560_0_0_0,
	&GenInst_Int32_t320_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1465_0_0_0,
	&GenInst_Int32_t320_0_0_0_LayoutCache_t54_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_LayoutCache_t54_0_0_0,
	&GenInst_KeyValuePair_2_t1482_0_0_0,
	&GenInst_GUIStyle_t56_0_0_0,
	&GenInst_KeyValuePair_2_t1493_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t56_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_KeyValuePair_2_t1503_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t320_0_0_0,
	&GenInst_KeyValuePair_2_t1509_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t320_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t320_0_0_0_Int32_t320_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t320_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t320_0_0_0_KeyValuePair_2_t1509_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t320_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_KeyValuePair_2_t1520_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_KeyValuePair_2_t328_0_0_0,
	&GenInst_Byte_t326_0_0_0,
	&GenInst_IComparable_1_t2225_0_0_0,
	&GenInst_IEquatable_1_t2226_0_0_0,
	&GenInst_Char_t576_0_0_0,
	&GenInst_IComparable_1_t2233_0_0_0,
	&GenInst_IEquatable_1_t2234_0_0_0,
	&GenInst_Camera_t110_0_0_0,
	&GenInst_Behaviour_t34_0_0_0,
	&GenInst_Component_t109_0_0_0,
	&GenInst_Display_t115_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_ISerializable_t1426_0_0_0,
	&GenInst_Single_t321_0_0_0,
	&GenInst_IComparable_1_t2238_0_0_0,
	&GenInst_IEquatable_1_t2239_0_0_0,
	&GenInst_Keyframe_t135_0_0_0,
	&GenInst_String_t_0_0_0_Int64_t345_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t345_0_0_0,
	&GenInst_KeyValuePair_2_t1552_0_0_0,
	&GenInst_Int64_t345_0_0_0,
	&GenInst_IComparable_1_t2219_0_0_0,
	&GenInst_IEquatable_1_t2220_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t345_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t345_0_0_0_Int64_t345_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t345_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t345_0_0_0_KeyValuePair_2_t1552_0_0_0,
	&GenInst_String_t_0_0_0_Int64_t345_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_KeyValuePair_2_t1567_0_0_0,
	&GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1581_0_0_0,
	&GenInst_NetworkID_t178_0_0_0,
	&GenInst_Enum_t305_0_0_0,
	&GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_NetworkID_t178_0_0_0,
	&GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_NetworkID_t178_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1581_0_0_0,
	&GenInst_NetworkID_t178_0_0_0_NetworkAccessToken_t180_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_NetworkAccessToken_t180_0_0_0,
	&GenInst_KeyValuePair_2_t1595_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_ConstructorDelegate_t201_0_0_0,
	&GenInst_GetDelegate_t198_0_0_0,
	&GenInst_IDictionary_2_t303_0_0_0,
	&GenInst_KeyValuePair_2_t362_0_0_0,
	&GenInst_IDictionary_2_t304_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0,
	&GenInst_KeyValuePair_2_t1601_0_0_0,
	&GenInst_Type_t_0_0_0_ConstructorDelegate_t201_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_KeyValuePair_2_t1609_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t303_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_KeyValuePair_2_t1613_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t304_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_KeyValuePair_2_t1617_0_0_0,
	&GenInst_String_t_0_0_0_GetDelegate_t198_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_KeyValuePair_2_t1493_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t1493_0_0_0_KeyValuePair_2_t1601_0_0_0,
	&GenInst_String_t_0_0_0_KeyValuePair_2_t362_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_KeyValuePair_2_t1639_0_0_0,
	&GenInst__ConstructorInfo_t2101_0_0_0,
	&GenInst_MethodBase_t379_0_0_0,
	&GenInst__MethodBase_t2104_0_0_0,
	&GenInst_ParameterInfo_t370_0_0_0,
	&GenInst__ParameterInfo_t2107_0_0_0,
	&GenInst__PropertyInfo_t2108_0_0_0,
	&GenInst__FieldInfo_t2103_0_0_0,
	&GenInst_DisallowMultipleComponent_t215_0_0_0,
	&GenInst_Attribute_t96_0_0_0,
	&GenInst__Attribute_t2052_0_0_0,
	&GenInst_ExecuteInEditMode_t218_0_0_0,
	&GenInst_RequireComponent_t216_0_0_0,
	&GenInst_ParameterModifier_t1020_0_0_0,
	&GenInst_HitInfo_t239_0_0_0,
	&GenInst_Event_t76_0_0_0_TextEditOp_t252_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0,
	&GenInst_KeyValuePair_2_t1662_0_0_0,
	&GenInst_TextEditOp_t252_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_TextEditOp_t252_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t252_0_0_0_KeyValuePair_2_t1662_0_0_0,
	&GenInst_Event_t76_0_0_0,
	&GenInst_Event_t76_0_0_0_TextEditOp_t252_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_KeyValuePair_2_t1676_0_0_0,
	&GenInst_UInt16_t351_0_0_0,
	&GenInst_IComparable_1_t2231_0_0_0,
	&GenInst_IEquatable_1_t2232_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t333_0_0_0,
	&GenInst_KeyValuePair_2_t1692_0_0_0,
	&GenInst_IComparable_1_t2244_0_0_0,
	&GenInst_IEquatable_1_t2245_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_Boolean_t333_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t333_0_0_0_KeyValuePair_2_t1692_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t333_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_KeyValuePair_2_t1707_0_0_0,
	&GenInst_X509Certificate_t449_0_0_0,
	&GenInst_IDeserializationCallback_t1429_0_0_0,
	&GenInst_X509ChainStatus_t457_0_0_0,
	&GenInst_Capture_t482_0_0_0,
	&GenInst_Group_t485_0_0_0,
	&GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0,
	&GenInst_KeyValuePair_2_t1716_0_0_0,
	&GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_Int32_t320_0_0_0,
	&GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_Int32_t320_0_0_0_Int32_t320_0_0_0_KeyValuePair_2_t1716_0_0_0,
	&GenInst_Mark_t511_0_0_0,
	&GenInst_UriScheme_t548_0_0_0,
	&GenInst_KeySizes_t621_0_0_0,
	&GenInst_UInt32_t336_0_0_0,
	&GenInst_IComparable_1_t2221_0_0_0,
	&GenInst_IEquatable_1_t2222_0_0_0,
	&GenInst_BigInteger_t625_0_0_0,
	&GenInst_ClientCertificateType_t713_0_0_0,
	&GenInst_UInt64_t348_0_0_0,
	&GenInst_SByte_t349_0_0_0,
	&GenInst_Int16_t350_0_0_0,
	&GenInst_Double_t344_0_0_0,
	&GenInst_Decimal_t347_0_0_0,
	&GenInst_Delegate_t332_0_0_0,
	&GenInst_IComparable_1_t2223_0_0_0,
	&GenInst_IEquatable_1_t2224_0_0_0,
	&GenInst_IComparable_1_t2229_0_0_0,
	&GenInst_IEquatable_1_t2230_0_0_0,
	&GenInst_IComparable_1_t2227_0_0_0,
	&GenInst_IEquatable_1_t2228_0_0_0,
	&GenInst_IComparable_1_t2240_0_0_0,
	&GenInst_IEquatable_1_t2241_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t2105_0_0_0,
	&GenInst_TableRange_t797_0_0_0,
	&GenInst_TailoringInfo_t800_0_0_0,
	&GenInst_Contraction_t801_0_0_0,
	&GenInst_Level2Map_t803_0_0_0,
	&GenInst_BigInteger_t823_0_0_0,
	&GenInst_Slot_t874_0_0_0,
	&GenInst_Slot_t881_0_0_0,
	&GenInst_StackFrame_t378_0_0_0,
	&GenInst_Calendar_t895_0_0_0,
	&GenInst_ModuleBuilder_t971_0_0_0,
	&GenInst__ModuleBuilder_t2095_0_0_0,
	&GenInst_Module_t965_0_0_0,
	&GenInst__Module_t2106_0_0_0,
	&GenInst_ParameterBuilder_t975_0_0_0,
	&GenInst__ParameterBuilder_t2096_0_0_0,
	&GenInst_TypeU5BU5D_t196_0_0_0,
	&GenInst_Array_t_0_0_0,
	&GenInst_ICollection_t569_0_0_0,
	&GenInst_IList_t519_0_0_0,
	&GenInst_ILTokenInfo_t959_0_0_0,
	&GenInst_LabelData_t961_0_0_0,
	&GenInst_LabelFixup_t960_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t958_0_0_0,
	&GenInst_TypeBuilder_t950_0_0_0,
	&GenInst__TypeBuilder_t2098_0_0_0,
	&GenInst_MethodBuilder_t957_0_0_0,
	&GenInst__MethodBuilder_t2094_0_0_0,
	&GenInst_ConstructorBuilder_t953_0_0_0,
	&GenInst__ConstructorBuilder_t2090_0_0_0,
	&GenInst_PropertyBuilder_t976_0_0_0,
	&GenInst__PropertyBuilder_t2097_0_0_0,
	&GenInst_FieldBuilder_t956_0_0_0,
	&GenInst__FieldBuilder_t2092_0_0_0,
	&GenInst_Header_t1091_0_0_0,
	&GenInst_ITrackingHandler_t1421_0_0_0,
	&GenInst_IContextAttribute_t1413_0_0_0,
	&GenInst_IComparable_1_t2376_0_0_0,
	&GenInst_IEquatable_1_t2377_0_0_0,
	&GenInst_IComparable_1_t2242_0_0_0,
	&GenInst_IEquatable_1_t2243_0_0_0,
	&GenInst_IComparable_1_t2386_0_0_0,
	&GenInst_IEquatable_1_t2387_0_0_0,
	&GenInst_TypeTag_t1145_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_Version_t418_0_0_0,
	&GenInst_DictionaryEntry_t560_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_KeyValuePair_2_t1465_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1465_0_0_0_KeyValuePair_2_t1465_0_0_0,
	&GenInst_KeyValuePair_2_t1493_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1493_0_0_0_KeyValuePair_2_t1493_0_0_0,
	&GenInst_KeyValuePair_2_t1509_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1509_0_0_0_KeyValuePair_2_t1509_0_0_0,
	&GenInst_Int64_t345_0_0_0_Object_t_0_0_0,
	&GenInst_Int64_t345_0_0_0_Int64_t345_0_0_0,
	&GenInst_KeyValuePair_2_t1552_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1552_0_0_0_KeyValuePair_2_t1552_0_0_0,
	&GenInst_NetworkID_t178_0_0_0_NetworkID_t178_0_0_0,
	&GenInst_KeyValuePair_2_t1581_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1581_0_0_0_KeyValuePair_2_t1581_0_0_0,
	&GenInst_KeyValuePair_2_t1601_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1601_0_0_0_KeyValuePair_2_t1601_0_0_0,
	&GenInst_TextEditOp_t252_0_0_0_Object_t_0_0_0,
	&GenInst_TextEditOp_t252_0_0_0_TextEditOp_t252_0_0_0,
	&GenInst_KeyValuePair_2_t1662_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1662_0_0_0_KeyValuePair_2_t1662_0_0_0,
	&GenInst_Boolean_t333_0_0_0_Object_t_0_0_0,
	&GenInst_Boolean_t333_0_0_0_Boolean_t333_0_0_0,
	&GenInst_KeyValuePair_2_t1692_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1692_0_0_0_KeyValuePair_2_t1692_0_0_0,
	&GenInst_KeyValuePair_2_t1716_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1716_0_0_0_KeyValuePair_2_t1716_0_0_0,
	&GenInst_ResponseBase_ParseJSONList_m12758_gp_0_0_0_0,
	&GenInst_NetworkMatch_ProcessMatchResponse_m12759_gp_0_0_0_0,
	&GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t2040_gp_0_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t2041_gp_0_0_0_0_ThreadSafeDictionary_2_t2041_gp_1_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t2041_gp_0_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t2041_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3008_0_0_0,
	&GenInst_Stack_1_t2049_gp_0_0_0_0,
	&GenInst_Enumerator_t2050_gp_0_0_0_0,
	&GenInst_IEnumerable_1_t2057_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m12908_gp_0_0_0_0,
	&GenInst_Array_Sort_m12922_gp_0_0_0_0,
	&GenInst_Array_Sort_m12923_gp_0_0_0_0,
	&GenInst_Array_Sort_m12926_gp_0_0_0_0,
	&GenInst_Array_Sort_m12927_gp_0_0_0_0,
	&GenInst_Array_Sort_m12928_gp_0_0_0_0,
	&GenInst_Array_Sort_m12929_gp_0_0_0_0,
	&GenInst_Array_qsort_m12930_gp_0_0_0_0,
	&GenInst_Array_compare_m12931_gp_0_0_0_0,
	&GenInst_Array_qsort_m12932_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m12937_gp_0_0_0_0,
	&GenInst_Array_ForEach_m12938_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m12939_gp_0_0_0_0_Array_ConvertAll_m12939_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m12940_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m12941_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m12942_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m12943_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m12944_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m12945_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m12947_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m12949_gp_0_0_0_0,
	&GenInst_Array_FindAll_m12956_gp_0_0_0_0,
	&GenInst_Array_Exists_m12957_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m12958_gp_0_0_0_0,
	&GenInst_Array_Find_m12959_gp_0_0_0_0,
	&GenInst_Array_FindLast_m12960_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t2059_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2060_gp_0_0_0_0,
	&GenInst_Nullable_1_t1410_gp_0_0_0_0,
	&GenInst_Comparer_1_t2069_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t2036_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2071_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0,
	&GenInst_Dictionary_2_t2071_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2309_0_0_0,
	&GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m13108_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m13113_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_DictionaryEntry_t560_0_0_0,
	&GenInst_ShimEnumerator_t2072_gp_0_0_0_0_ShimEnumerator_t2072_gp_1_0_0_0,
	&GenInst_Enumerator_t2073_gp_0_0_0_0_Enumerator_t2073_gp_1_0_0_0,
	&GenInst_KeyCollection_t2074_gp_0_0_0_0_KeyCollection_t2074_gp_1_0_0_0,
	&GenInst_KeyCollection_t2074_gp_0_0_0_0,
	&GenInst_Enumerator_t2075_gp_0_0_0_0_Enumerator_t2075_gp_1_0_0_0,
	&GenInst_ValueCollection_t2076_gp_0_0_0_0_ValueCollection_t2076_gp_1_0_0_0,
	&GenInst_ValueCollection_t2076_gp_1_0_0_0,
	&GenInst_Enumerator_t2077_gp_0_0_0_0_Enumerator_t2077_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2079_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2035_gp_0_0_0_0,
	&GenInst_IDictionary_2_t2082_gp_0_0_0_0,
	&GenInst_IDictionary_2_t2082_gp_1_0_0_0,
	&GenInst_List_1_t2085_gp_0_0_0_0,
	&GenInst_Enumerator_t2086_gp_0_0_0_0,
	&GenInst_Collection_1_t2087_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t2088_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m13363_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m13363_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m13364_gp_0_0_0_0,
	&GenInst_Array_Sort_m12920_gp_0_0_0_0_Array_Sort_m12920_gp_0_0_0_0,
	&GenInst_Array_Sort_m12921_gp_0_0_0_0_Array_Sort_m12921_gp_1_0_0_0,
	&GenInst_Array_Sort_m12922_gp_0_0_0_0_Array_Sort_m12922_gp_0_0_0_0,
	&GenInst_Array_Sort_m12923_gp_0_0_0_0_Array_Sort_m12923_gp_1_0_0_0,
	&GenInst_Array_Sort_m12924_gp_0_0_0_0_Array_Sort_m12924_gp_0_0_0_0,
	&GenInst_Array_Sort_m12925_gp_0_0_0_0_Array_Sort_m12925_gp_1_0_0_0,
	&GenInst_Array_Sort_m12926_gp_0_0_0_0_Array_Sort_m12926_gp_0_0_0_0,
	&GenInst_Array_Sort_m12927_gp_1_0_0_0,
	&GenInst_Array_Sort_m12927_gp_0_0_0_0_Array_Sort_m12927_gp_1_0_0_0,
	&GenInst_Array_qsort_m12930_gp_0_0_0_0_Array_qsort_m12930_gp_1_0_0_0,
	&GenInst_Array_Resize_m12935_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m12946_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m12948_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m12950_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m12951_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m12952_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m12953_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m12954_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m12955_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t2058_gp_0_0_0_0,
	&GenInst_IList_1_t2061_gp_0_0_0_0,
	&GenInst_ICollection_1_t2062_gp_0_0_0_0,
	&GenInst_DefaultComparer_t2070_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m13113_gp_0_0_0_0_Object_t_0_0_0,
	&GenInst_Dictionary_2_t2071_gp_0_0_0_0_Dictionary_2_t2071_gp_1_0_0_0_KeyValuePair_2_t2309_0_0_0,
	&GenInst_KeyValuePair_2_t2309_0_0_0_KeyValuePair_2_t2309_0_0_0,
	&GenInst_KeyValuePair_2_t2318_0_0_0,
	&GenInst_KeyCollection_t2074_gp_0_0_0_0_KeyCollection_t2074_gp_1_0_0_0_KeyCollection_t2074_gp_0_0_0_0,
	&GenInst_KeyCollection_t2074_gp_0_0_0_0_KeyCollection_t2074_gp_0_0_0_0,
	&GenInst_Enumerator_t2075_gp_0_0_0_0,
	&GenInst_ValueCollection_t2076_gp_0_0_0_0_ValueCollection_t2076_gp_1_0_0_0_ValueCollection_t2076_gp_1_0_0_0,
	&GenInst_ValueCollection_t2076_gp_1_0_0_0_ValueCollection_t2076_gp_1_0_0_0,
	&GenInst_Enumerator_t2077_gp_1_0_0_0,
	&GenInst_DefaultComparer_t2080_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t3060_0_0_0,
	&GenInst_IDictionary_2_t2082_gp_0_0_0_0_IDictionary_2_t2082_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2084_gp_0_0_0_0_KeyValuePair_2_t2084_gp_1_0_0_0,
};
