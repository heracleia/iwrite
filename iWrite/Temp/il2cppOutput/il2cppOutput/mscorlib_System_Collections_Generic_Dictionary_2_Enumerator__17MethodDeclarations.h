﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>
struct Enumerator_t1710;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t430;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16MethodDeclarations.h"
#define Enumerator__ctor_m11494(__this, ___dictionary, method) (( void (*) (Enumerator_t1710 *, Dictionary_2_t430 *, const MethodInfo*))Enumerator__ctor_m11395_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m11495(__this, method) (( Object_t * (*) (Enumerator_t1710 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11396_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11496(__this, method) (( DictionaryEntry_t560  (*) (Enumerator_t1710 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11397_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11497(__this, method) (( Object_t * (*) (Enumerator_t1710 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11398_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11498(__this, method) (( Object_t * (*) (Enumerator_t1710 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11399_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m11499(__this, method) (( bool (*) (Enumerator_t1710 *, const MethodInfo*))Enumerator_MoveNext_m11400_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m11500(__this, method) (( KeyValuePair_2_t1707  (*) (Enumerator_t1710 *, const MethodInfo*))Enumerator_get_Current_m11401_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m11501(__this, method) (( String_t* (*) (Enumerator_t1710 *, const MethodInfo*))Enumerator_get_CurrentKey_m11402_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m11502(__this, method) (( bool (*) (Enumerator_t1710 *, const MethodInfo*))Enumerator_get_CurrentValue_m11403_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m11503(__this, method) (( void (*) (Enumerator_t1710 *, const MethodInfo*))Enumerator_VerifyState_m11404_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m11504(__this, method) (( void (*) (Enumerator_t1710 *, const MethodInfo*))Enumerator_VerifyCurrent_m11405_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m11505(__this, method) (( void (*) (Enumerator_t1710 *, const MethodInfo*))Enumerator_Dispose_m11406_gshared)(__this, method)
