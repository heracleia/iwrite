﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssemblyIsEditorAssembly
struct AssemblyIsEditorAssembly_t221;

// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern "C" void AssemblyIsEditorAssembly__ctor_m1024 (AssemblyIsEditorAssembly_t221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
