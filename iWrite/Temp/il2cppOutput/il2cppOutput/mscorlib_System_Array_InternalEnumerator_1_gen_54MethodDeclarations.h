﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct InternalEnumerator_1_t1717;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11575_gshared (InternalEnumerator_1_t1717 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11575(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1717 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11575_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11576_gshared (InternalEnumerator_1_t1717 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11576(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1717 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11576_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11577_gshared (InternalEnumerator_1_t1717 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11577(__this, method) (( void (*) (InternalEnumerator_1_t1717 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11577_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11578_gshared (InternalEnumerator_1_t1717 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11578(__this, method) (( bool (*) (InternalEnumerator_1_t1717 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11578_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::get_Current()
extern "C" KeyValuePair_2_t1716  InternalEnumerator_1_get_Current_m11579_gshared (InternalEnumerator_1_t1717 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11579(__this, method) (( KeyValuePair_2_t1716  (*) (InternalEnumerator_1_t1717 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11579_gshared)(__this, method)
