﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Text.RegularExpressions.FactoryCache/Key
#include "System_System_Text_RegularExpressions_FactoryCache_Key.h"
// Metadata Definition System.Text.RegularExpressions.FactoryCache/Key
extern TypeInfo Key_t500_il2cpp_TypeInfo;
// System.Text.RegularExpressions.FactoryCache/Key
#include "System_System_Text_RegularExpressions_FactoryCache_KeyMethodDeclarations.h"
static const EncodedMethodIndex Key_t500_VTable[4] = 
{
	641,
	125,
	642,
	643,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Key_t500_0_0_0;
extern const Il2CppType Key_t500_1_0_0;
extern const Il2CppType Object_t_0_0_0;
extern TypeInfo FactoryCache_t491_il2cpp_TypeInfo;
extern const Il2CppType FactoryCache_t491_0_0_0;
struct Key_t500;
const Il2CppTypeDefinitionMetadata Key_t500_DefinitionMetadata = 
{
	&FactoryCache_t491_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Key_t500_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1656/* fieldStart */
	, 1759/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Key_t500_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Key"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Key_t500_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Key_t500_0_0_0/* byval_arg */
	, &Key_t500_1_0_0/* this_arg */
	, &Key_t500_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Key_t500)/* instance_size */
	, sizeof (Key_t500)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.MRUList
#include "System_System_Text_RegularExpressions_MRUList.h"
// Metadata Definition System.Text.RegularExpressions.MRUList
extern TypeInfo MRUList_t501_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MRUList
#include "System_System_Text_RegularExpressions_MRUListMethodDeclarations.h"
extern const Il2CppType Node_t502_0_0_0;
static const Il2CppType* MRUList_t501_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Node_t502_0_0_0,
};
static const EncodedMethodIndex MRUList_t501_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MRUList_t501_0_0_0;
extern const Il2CppType MRUList_t501_1_0_0;
struct MRUList_t501;
const Il2CppTypeDefinitionMetadata MRUList_t501_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MRUList_t501_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MRUList_t501_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1658/* fieldStart */
	, 1763/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MRUList_t501_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MRUList"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &MRUList_t501_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MRUList_t501_0_0_0/* byval_arg */
	, &MRUList_t501_1_0_0/* this_arg */
	, &MRUList_t501_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MRUList_t501)/* instance_size */
	, sizeof (MRUList_t501)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.MRUList/Node
#include "System_System_Text_RegularExpressions_MRUList_Node.h"
// Metadata Definition System.Text.RegularExpressions.MRUList/Node
extern TypeInfo Node_t502_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MRUList/Node
#include "System_System_Text_RegularExpressions_MRUList_NodeMethodDeclarations.h"
static const EncodedMethodIndex Node_t502_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Node_t502_1_0_0;
struct Node_t502;
const Il2CppTypeDefinitionMetadata Node_t502_DefinitionMetadata = 
{
	&MRUList_t501_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Node_t502_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1660/* fieldStart */
	, 1766/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Node_t502_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Node"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Node_t502_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Node_t502_0_0_0/* byval_arg */
	, &Node_t502_1_0_0/* this_arg */
	, &Node_t502_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Node_t502)/* instance_size */
	, sizeof (Node_t502)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"
// Metadata Definition System.Text.RegularExpressions.Category
extern TypeInfo Category_t503_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_CategoryMethodDeclarations.h"
static const EncodedMethodIndex Category_t503_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
extern const Il2CppType IFormattable_t1406_0_0_0;
extern const Il2CppType IConvertible_t1409_0_0_0;
extern const Il2CppType IComparable_t1408_0_0_0;
static Il2CppInterfaceOffsetPair Category_t503_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Category_t503_0_0_0;
extern const Il2CppType Category_t503_1_0_0;
extern const Il2CppType Enum_t305_0_0_0;
// System.UInt16
#include "mscorlib_System_UInt16.h"
extern TypeInfo UInt16_t351_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Category_t503_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Category_t503_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, Category_t503_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1663/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Category_t503_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Category"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &UInt16_t351_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Category_t503_0_0_0/* byval_arg */
	, &Category_t503_1_0_0/* this_arg */
	, &Category_t503_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Category_t503)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Category_t503)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 146/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.CategoryUtils
#include "System_System_Text_RegularExpressions_CategoryUtils.h"
// Metadata Definition System.Text.RegularExpressions.CategoryUtils
extern TypeInfo CategoryUtils_t504_il2cpp_TypeInfo;
// System.Text.RegularExpressions.CategoryUtils
#include "System_System_Text_RegularExpressions_CategoryUtilsMethodDeclarations.h"
static const EncodedMethodIndex CategoryUtils_t504_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CategoryUtils_t504_0_0_0;
extern const Il2CppType CategoryUtils_t504_1_0_0;
struct CategoryUtils_t504;
const Il2CppTypeDefinitionMetadata CategoryUtils_t504_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CategoryUtils_t504_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1767/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CategoryUtils_t504_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CategoryUtils"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &CategoryUtils_t504_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CategoryUtils_t504_0_0_0/* byval_arg */
	, &CategoryUtils_t504_1_0_0/* this_arg */
	, &CategoryUtils_t504_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CategoryUtils_t504)/* instance_size */
	, sizeof (CategoryUtils_t504)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRef.h"
// Metadata Definition System.Text.RegularExpressions.LinkRef
extern TypeInfo LinkRef_t505_il2cpp_TypeInfo;
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRefMethodDeclarations.h"
static const EncodedMethodIndex LinkRef_t505_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType LinkRef_t505_0_0_0;
extern const Il2CppType LinkRef_t505_1_0_0;
struct LinkRef_t505;
const Il2CppTypeDefinitionMetadata LinkRef_t505_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LinkRef_t505_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1770/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LinkRef_t505_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkRef"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &LinkRef_t505_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LinkRef_t505_0_0_0/* byval_arg */
	, &LinkRef_t505_1_0_0/* this_arg */
	, &LinkRef_t505_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkRef_t505)/* instance_size */
	, sizeof (LinkRef_t505)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Text.RegularExpressions.ICompiler
extern TypeInfo ICompiler_t570_il2cpp_TypeInfo;
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ICompiler_t570_0_0_0;
extern const Il2CppType ICompiler_t570_1_0_0;
struct ICompiler_t570;
const Il2CppTypeDefinitionMetadata ICompiler_t570_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1771/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICompiler_t570_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICompiler"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &ICompiler_t570_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICompiler_t570_0_0_0/* byval_arg */
	, &ICompiler_t570_1_0_0/* this_arg */
	, &ICompiler_t570_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.InterpreterFactory
#include "System_System_Text_RegularExpressions_InterpreterFactory.h"
// Metadata Definition System.Text.RegularExpressions.InterpreterFactory
extern TypeInfo InterpreterFactory_t506_il2cpp_TypeInfo;
// System.Text.RegularExpressions.InterpreterFactory
#include "System_System_Text_RegularExpressions_InterpreterFactoryMethodDeclarations.h"
static const EncodedMethodIndex InterpreterFactory_t506_VTable[12] = 
{
	120,
	125,
	122,
	123,
	644,
	645,
	646,
	647,
	648,
	649,
	650,
	651,
};
extern const Il2CppType IMachineFactory_t492_0_0_0;
static const Il2CppType* InterpreterFactory_t506_InterfacesTypeInfos[] = 
{
	&IMachineFactory_t492_0_0_0,
};
static Il2CppInterfaceOffsetPair InterpreterFactory_t506_InterfacesOffsets[] = 
{
	{ &IMachineFactory_t492_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType InterpreterFactory_t506_0_0_0;
extern const Il2CppType InterpreterFactory_t506_1_0_0;
struct InterpreterFactory_t506;
const Il2CppTypeDefinitionMetadata InterpreterFactory_t506_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, InterpreterFactory_t506_InterfacesTypeInfos/* implementedInterfaces */
	, InterpreterFactory_t506_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InterpreterFactory_t506_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1809/* fieldStart */
	, 1801/* methodStart */
	, -1/* eventStart */
	, 429/* propertyStart */

};
TypeInfo InterpreterFactory_t506_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "InterpreterFactory"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &InterpreterFactory_t506_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InterpreterFactory_t506_0_0_0/* byval_arg */
	, &InterpreterFactory_t506_1_0_0/* this_arg */
	, &InterpreterFactory_t506_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InterpreterFactory_t506)/* instance_size */
	, sizeof (InterpreterFactory_t506)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler
#include "System_System_Text_RegularExpressions_PatternCompiler.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler
extern TypeInfo PatternCompiler_t510_il2cpp_TypeInfo;
// System.Text.RegularExpressions.PatternCompiler
#include "System_System_Text_RegularExpressions_PatternCompilerMethodDeclarations.h"
extern const Il2CppType PatternLinkStack_t508_0_0_0;
static const Il2CppType* PatternCompiler_t510_il2cpp_TypeInfo__nestedTypes[1] =
{
	&PatternLinkStack_t508_0_0_0,
};
static const EncodedMethodIndex PatternCompiler_t510_VTable[34] = 
{
	120,
	125,
	122,
	123,
	652,
	653,
	654,
	655,
	656,
	657,
	658,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	667,
	668,
	669,
	670,
	671,
	672,
	673,
	674,
	675,
	676,
	677,
	678,
	679,
	680,
	681,
};
static const Il2CppType* PatternCompiler_t510_InterfacesTypeInfos[] = 
{
	&ICompiler_t570_0_0_0,
};
static Il2CppInterfaceOffsetPair PatternCompiler_t510_InterfacesOffsets[] = 
{
	{ &ICompiler_t570_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PatternCompiler_t510_0_0_0;
extern const Il2CppType PatternCompiler_t510_1_0_0;
struct PatternCompiler_t510;
const Il2CppTypeDefinitionMetadata PatternCompiler_t510_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PatternCompiler_t510_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, PatternCompiler_t510_InterfacesTypeInfos/* implementedInterfaces */
	, PatternCompiler_t510_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PatternCompiler_t510_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1813/* fieldStart */
	, 1810/* methodStart */
	, -1/* eventStart */
	, 433/* propertyStart */

};
TypeInfo PatternCompiler_t510_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PatternCompiler"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &PatternCompiler_t510_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PatternCompiler_t510_0_0_0/* byval_arg */
	, &PatternCompiler_t510_1_0_0/* this_arg */
	, &PatternCompiler_t510_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PatternCompiler_t510)/* instance_size */
	, sizeof (PatternCompiler_t510)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 40/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 34/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter_0.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
extern TypeInfo PatternLinkStack_t508_il2cpp_TypeInfo;
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter_0MethodDeclarations.h"
extern const Il2CppType Link_t507_0_0_0;
static const Il2CppType* PatternLinkStack_t508_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Link_t507_0_0_0,
};
static const EncodedMethodIndex PatternLinkStack_t508_VTable[6] = 
{
	120,
	125,
	122,
	123,
	682,
	683,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PatternLinkStack_t508_1_0_0;
extern const Il2CppType LinkStack_t509_0_0_0;
struct PatternLinkStack_t508;
const Il2CppTypeDefinitionMetadata PatternLinkStack_t508_DefinitionMetadata = 
{
	&PatternCompiler_t510_0_0_0/* declaringType */
	, PatternLinkStack_t508_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &LinkStack_t509_0_0_0/* parent */
	, PatternLinkStack_t508_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1814/* fieldStart */
	, 1850/* methodStart */
	, -1/* eventStart */
	, 434/* propertyStart */

};
TypeInfo PatternLinkStack_t508_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PatternLinkStack"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PatternLinkStack_t508_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PatternLinkStack_t508_0_0_0/* byval_arg */
	, &PatternLinkStack_t508_1_0_0/* this_arg */
	, &PatternLinkStack_t508_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PatternLinkStack_t508)/* instance_size */
	, sizeof (PatternLinkStack_t508)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
extern TypeInfo Link_t507_il2cpp_TypeInfo;
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_PatterMethodDeclarations.h"
static const EncodedMethodIndex Link_t507_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Link_t507_1_0_0;
extern const Il2CppType ValueType_t757_0_0_0;
const Il2CppTypeDefinitionMetadata Link_t507_DefinitionMetadata = 
{
	&PatternLinkStack_t508_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Link_t507_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1815/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Link_t507_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Link_t507_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Link_t507_0_0_0/* byval_arg */
	, &Link_t507_1_0_0/* this_arg */
	, &Link_t507_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Link_t507)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Link_t507)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Link_t507 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStack.h"
// Metadata Definition System.Text.RegularExpressions.LinkStack
extern TypeInfo LinkStack_t509_il2cpp_TypeInfo;
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStackMethodDeclarations.h"
static const EncodedMethodIndex LinkStack_t509_VTable[6] = 
{
	120,
	125,
	122,
	123,
	0,
	0,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType LinkStack_t509_1_0_0;
struct LinkStack_t509;
const Il2CppTypeDefinitionMetadata LinkStack_t509_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &LinkRef_t505_0_0_0/* parent */
	, LinkStack_t509_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1817/* fieldStart */
	, 1857/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LinkStack_t509_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkStack"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &LinkStack_t509_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LinkStack_t509_0_0_0/* byval_arg */
	, &LinkStack_t509_1_0_0/* this_arg */
	, &LinkStack_t509_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkStack_t509)/* instance_size */
	, sizeof (LinkStack_t509)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"
// Metadata Definition System.Text.RegularExpressions.Mark
extern TypeInfo Mark_t511_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_MarkMethodDeclarations.h"
static const EncodedMethodIndex Mark_t511_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Mark_t511_0_0_0;
extern const Il2CppType Mark_t511_1_0_0;
const Il2CppTypeDefinitionMetadata Mark_t511_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Mark_t511_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1818/* fieldStart */
	, 1862/* methodStart */
	, -1/* eventStart */
	, 436/* propertyStart */

};
TypeInfo Mark_t511_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mark"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Mark_t511_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mark_t511_0_0_0/* byval_arg */
	, &Mark_t511_1_0_0/* this_arg */
	, &Mark_t511_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mark_t511)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mark_t511)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Mark_t511 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter
#include "System_System_Text_RegularExpressions_Interpreter.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter
extern TypeInfo Interpreter_t517_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter
#include "System_System_Text_RegularExpressions_InterpreterMethodDeclarations.h"
extern const Il2CppType IntStack_t512_0_0_0;
extern const Il2CppType RepeatContext_t513_0_0_0;
extern const Il2CppType Mode_t514_0_0_0;
static const Il2CppType* Interpreter_t517_il2cpp_TypeInfo__nestedTypes[3] =
{
	&IntStack_t512_0_0_0,
	&RepeatContext_t513_0_0_0,
	&Mode_t514_0_0_0,
};
static const EncodedMethodIndex Interpreter_t517_VTable[8] = 
{
	120,
	125,
	122,
	123,
	684,
	618,
	618,
	684,
};
extern const Il2CppType IMachine_t488_0_0_0;
static Il2CppInterfaceOffsetPair Interpreter_t517_InterfacesOffsets[] = 
{
	{ &IMachine_t488_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Interpreter_t517_0_0_0;
extern const Il2CppType Interpreter_t517_1_0_0;
extern const Il2CppType BaseMachine_t481_0_0_0;
struct Interpreter_t517;
const Il2CppTypeDefinitionMetadata Interpreter_t517_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Interpreter_t517_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Interpreter_t517_InterfacesOffsets/* interfaceOffsets */
	, &BaseMachine_t481_0_0_0/* parent */
	, Interpreter_t517_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1821/* fieldStart */
	, 1865/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Interpreter_t517_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Interpreter"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Interpreter_t517_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Interpreter_t517_0_0_0/* byval_arg */
	, &Interpreter_t517_1_0_0/* this_arg */
	, &Interpreter_t517_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Interpreter_t517)/* instance_size */
	, sizeof (Interpreter_t517)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/IntStack
#include "System_System_Text_RegularExpressions_Interpreter_IntStack.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/IntStack
extern TypeInfo IntStack_t512_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/IntStack
#include "System_System_Text_RegularExpressions_Interpreter_IntStackMethodDeclarations.h"
static const EncodedMethodIndex IntStack_t512_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IntStack_t512_1_0_0;
const Il2CppTypeDefinitionMetadata IntStack_t512_DefinitionMetadata = 
{
	&Interpreter_t517_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, IntStack_t512_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1837/* fieldStart */
	, 1886/* methodStart */
	, -1/* eventStart */
	, 439/* propertyStart */

};
TypeInfo IntStack_t512_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntStack"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &IntStack_t512_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IntStack_t512_0_0_0/* byval_arg */
	, &IntStack_t512_1_0_0/* this_arg */
	, &IntStack_t512_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)IntStack_t512_marshal/* marshal_to_native_func */
	, (methodPointerType)IntStack_t512_marshal_back/* marshal_from_native_func */
	, (methodPointerType)IntStack_t512_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (IntStack_t512)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (IntStack_t512)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(IntStack_t512_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/RepeatContext
#include "System_System_Text_RegularExpressions_Interpreter_RepeatCont.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/RepeatContext
extern TypeInfo RepeatContext_t513_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/RepeatContext
#include "System_System_Text_RegularExpressions_Interpreter_RepeatContMethodDeclarations.h"
static const EncodedMethodIndex RepeatContext_t513_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RepeatContext_t513_1_0_0;
struct RepeatContext_t513;
const Il2CppTypeDefinitionMetadata RepeatContext_t513_DefinitionMetadata = 
{
	&Interpreter_t517_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RepeatContext_t513_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1839/* fieldStart */
	, 1890/* methodStart */
	, -1/* eventStart */
	, 440/* propertyStart */

};
TypeInfo RepeatContext_t513_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RepeatContext"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &RepeatContext_t513_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RepeatContext_t513_0_0_0/* byval_arg */
	, &RepeatContext_t513_1_0_0/* this_arg */
	, &RepeatContext_t513_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RepeatContext_t513)/* instance_size */
	, sizeof (RepeatContext_t513)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/Mode
#include "System_System_Text_RegularExpressions_Interpreter_Mode.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/Mode
extern TypeInfo Mode_t514_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/Mode
#include "System_System_Text_RegularExpressions_Interpreter_ModeMethodDeclarations.h"
static const EncodedMethodIndex Mode_t514_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair Mode_t514_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Mode_t514_1_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t320_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Mode_t514_DefinitionMetadata = 
{
	&Interpreter_t517_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Mode_t514_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, Mode_t514_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1846/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Mode_t514_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mode_t514_0_0_0/* byval_arg */
	, &Mode_t514_1_0_0/* this_arg */
	, &Mode_t514_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mode_t514)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mode_t514)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
// Metadata Definition System.Text.RegularExpressions.Interval
extern TypeInfo Interval_t518_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_IntervalMethodDeclarations.h"
static const EncodedMethodIndex Interval_t518_VTable[5] = 
{
	150,
	125,
	151,
	152,
	685,
};
static const Il2CppType* Interval_t518_InterfacesTypeInfos[] = 
{
	&IComparable_t1408_0_0_0,
};
static Il2CppInterfaceOffsetPair Interval_t518_InterfacesOffsets[] = 
{
	{ &IComparable_t1408_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Interval_t518_0_0_0;
extern const Il2CppType Interval_t518_1_0_0;
const Il2CppTypeDefinitionMetadata Interval_t518_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Interval_t518_InterfacesTypeInfos/* implementedInterfaces */
	, Interval_t518_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Interval_t518_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1850/* fieldStart */
	, 1900/* methodStart */
	, -1/* eventStart */
	, 447/* propertyStart */

};
TypeInfo Interval_t518_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Interval"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Interval_t518_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Interval_t518_0_0_0/* byval_arg */
	, &Interval_t518_1_0_0/* this_arg */
	, &Interval_t518_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Interval_t518_marshal/* marshal_to_native_func */
	, (methodPointerType)Interval_t518_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Interval_t518_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Interval_t518)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Interval_t518)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Interval_t518_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection
#include "System_System_Text_RegularExpressions_IntervalCollection.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection
extern TypeInfo IntervalCollection_t522_il2cpp_TypeInfo;
// System.Text.RegularExpressions.IntervalCollection
#include "System_System_Text_RegularExpressions_IntervalCollectionMethodDeclarations.h"
extern const Il2CppType Enumerator_t520_0_0_0;
extern const Il2CppType CostDelegate_t521_0_0_0;
static const Il2CppType* IntervalCollection_t522_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Enumerator_t520_0_0_0,
	&CostDelegate_t521_0_0_0,
};
static const EncodedMethodIndex IntervalCollection_t522_VTable[8] = 
{
	120,
	125,
	122,
	123,
	686,
	687,
	688,
	689,
};
extern const Il2CppType ICollection_t569_0_0_0;
extern const Il2CppType IEnumerable_t302_0_0_0;
static const Il2CppType* IntervalCollection_t522_InterfacesTypeInfos[] = 
{
	&ICollection_t569_0_0_0,
	&IEnumerable_t302_0_0_0,
};
static Il2CppInterfaceOffsetPair IntervalCollection_t522_InterfacesOffsets[] = 
{
	{ &ICollection_t569_0_0_0, 4},
	{ &IEnumerable_t302_0_0_0, 7},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IntervalCollection_t522_0_0_0;
extern const Il2CppType IntervalCollection_t522_1_0_0;
struct IntervalCollection_t522;
const Il2CppTypeDefinitionMetadata IntervalCollection_t522_DefinitionMetadata = 
{
	NULL/* declaringType */
	, IntervalCollection_t522_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, IntervalCollection_t522_InterfacesTypeInfos/* implementedInterfaces */
	, IntervalCollection_t522_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IntervalCollection_t522_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1853/* fieldStart */
	, 1913/* methodStart */
	, -1/* eventStart */
	, 452/* propertyStart */

};
TypeInfo IntervalCollection_t522_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntervalCollection"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &IntervalCollection_t522_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 501/* custom_attributes_cache */
	, &IntervalCollection_t522_0_0_0/* byval_arg */
	, &IntervalCollection_t522_1_0_0/* this_arg */
	, &IntervalCollection_t522_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IntervalCollection_t522)/* instance_size */
	, sizeof (IntervalCollection_t522)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection/Enumerator
#include "System_System_Text_RegularExpressions_IntervalCollection_Enu.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection/Enumerator
extern TypeInfo Enumerator_t520_il2cpp_TypeInfo;
// System.Text.RegularExpressions.IntervalCollection/Enumerator
#include "System_System_Text_RegularExpressions_IntervalCollection_EnuMethodDeclarations.h"
static const EncodedMethodIndex Enumerator_t520_VTable[7] = 
{
	120,
	125,
	122,
	123,
	690,
	691,
	692,
};
extern const Il2CppType IEnumerator_t279_0_0_0;
static const Il2CppType* Enumerator_t520_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t520_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Enumerator_t520_1_0_0;
struct Enumerator_t520;
const Il2CppTypeDefinitionMetadata Enumerator_t520_DefinitionMetadata = 
{
	&IntervalCollection_t522_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t520_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t520_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t520_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1854/* fieldStart */
	, 1923/* methodStart */
	, -1/* eventStart */
	, 455/* propertyStart */

};
TypeInfo Enumerator_t520_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t520_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t520_0_0_0/* byval_arg */
	, &Enumerator_t520_1_0_0/* this_arg */
	, &Enumerator_t520_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t520)/* instance_size */
	, sizeof (Enumerator_t520)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
#include "System_System_Text_RegularExpressions_IntervalCollection_Cos.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection/CostDelegate
extern TypeInfo CostDelegate_t521_il2cpp_TypeInfo;
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
#include "System_System_Text_RegularExpressions_IntervalCollection_CosMethodDeclarations.h"
static const EncodedMethodIndex CostDelegate_t521_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	693,
	694,
	695,
};
extern const Il2CppType ICloneable_t2056_0_0_0;
extern const Il2CppType ISerializable_t1426_0_0_0;
static Il2CppInterfaceOffsetPair CostDelegate_t521_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CostDelegate_t521_1_0_0;
extern const Il2CppType MulticastDelegate_t47_0_0_0;
struct CostDelegate_t521;
const Il2CppTypeDefinitionMetadata CostDelegate_t521_DefinitionMetadata = 
{
	&IntervalCollection_t522_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CostDelegate_t521_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, CostDelegate_t521_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1927/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CostDelegate_t521_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CostDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CostDelegate_t521_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CostDelegate_t521_0_0_0/* byval_arg */
	, &CostDelegate_t521_1_0_0/* this_arg */
	, &CostDelegate_t521_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CostDelegate_t521/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CostDelegate_t521)/* instance_size */
	, sizeof (CostDelegate_t521)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Parser
#include "System_System_Text_RegularExpressions_Syntax_Parser.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Parser
extern TypeInfo Parser_t523_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Parser
#include "System_System_Text_RegularExpressions_Syntax_ParserMethodDeclarations.h"
static const EncodedMethodIndex Parser_t523_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Parser_t523_0_0_0;
extern const Il2CppType Parser_t523_1_0_0;
struct Parser_t523;
const Il2CppTypeDefinitionMetadata Parser_t523_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Parser_t523_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1856/* fieldStart */
	, 1931/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Parser_t523_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Parser"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &Parser_t523_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Parser_t523_0_0_0/* byval_arg */
	, &Parser_t523_1_0_0/* this_arg */
	, &Parser_t523_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Parser_t523)/* instance_size */
	, sizeof (Parser_t523)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.QuickSearch
#include "System_System_Text_RegularExpressions_QuickSearch.h"
// Metadata Definition System.Text.RegularExpressions.QuickSearch
extern TypeInfo QuickSearch_t515_il2cpp_TypeInfo;
// System.Text.RegularExpressions.QuickSearch
#include "System_System_Text_RegularExpressions_QuickSearchMethodDeclarations.h"
static const EncodedMethodIndex QuickSearch_t515_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType QuickSearch_t515_0_0_0;
extern const Il2CppType QuickSearch_t515_1_0_0;
struct QuickSearch_t515;
const Il2CppTypeDefinitionMetadata QuickSearch_t515_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, QuickSearch_t515_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1862/* fieldStart */
	, 1962/* methodStart */
	, -1/* eventStart */
	, 456/* propertyStart */

};
TypeInfo QuickSearch_t515_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "QuickSearch"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &QuickSearch_t515_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QuickSearch_t515_0_0_0/* byval_arg */
	, &QuickSearch_t515_1_0_0/* this_arg */
	, &QuickSearch_t515_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QuickSearch_t515)/* instance_size */
	, sizeof (QuickSearch_t515)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(QuickSearch_t515_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.ReplacementEvaluator
#include "System_System_Text_RegularExpressions_ReplacementEvaluator.h"
// Metadata Definition System.Text.RegularExpressions.ReplacementEvaluator
extern TypeInfo ReplacementEvaluator_t524_il2cpp_TypeInfo;
// System.Text.RegularExpressions.ReplacementEvaluator
#include "System_System_Text_RegularExpressions_ReplacementEvaluatorMethodDeclarations.h"
static const EncodedMethodIndex ReplacementEvaluator_t524_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ReplacementEvaluator_t524_0_0_0;
extern const Il2CppType ReplacementEvaluator_t524_1_0_0;
struct ReplacementEvaluator_t524;
const Il2CppTypeDefinitionMetadata ReplacementEvaluator_t524_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReplacementEvaluator_t524_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1869/* fieldStart */
	, 1969/* methodStart */
	, -1/* eventStart */
	, 457/* propertyStart */

};
TypeInfo ReplacementEvaluator_t524_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReplacementEvaluator"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &ReplacementEvaluator_t524_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReplacementEvaluator_t524_0_0_0/* byval_arg */
	, &ReplacementEvaluator_t524_1_0_0/* this_arg */
	, &ReplacementEvaluator_t524_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReplacementEvaluator_t524)/* instance_size */
	, sizeof (ReplacementEvaluator_t524)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.ExpressionCollection
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColle.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.ExpressionCollection
extern TypeInfo ExpressionCollection_t525_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.ExpressionCollection
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColleMethodDeclarations.h"
static const EncodedMethodIndex ExpressionCollection_t525_VTable[26] = 
{
	120,
	125,
	122,
	123,
	564,
	565,
	566,
	567,
	568,
	569,
	570,
	571,
	572,
	573,
	574,
	575,
	576,
	577,
	578,
	579,
	580,
	581,
	582,
	583,
	584,
	696,
};
extern const Il2CppType IList_t519_0_0_0;
static Il2CppInterfaceOffsetPair ExpressionCollection_t525_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ExpressionCollection_t525_0_0_0;
extern const Il2CppType ExpressionCollection_t525_1_0_0;
extern const Il2CppType CollectionBase_t453_0_0_0;
struct ExpressionCollection_t525;
const Il2CppTypeDefinitionMetadata ExpressionCollection_t525_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExpressionCollection_t525_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t453_0_0_0/* parent */
	, ExpressionCollection_t525_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1978/* methodStart */
	, -1/* eventStart */
	, 458/* propertyStart */

};
TypeInfo ExpressionCollection_t525_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExpressionCollection"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &ExpressionCollection_t525_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 502/* custom_attributes_cache */
	, &ExpressionCollection_t525_0_0_0/* byval_arg */
	, &ExpressionCollection_t525_1_0_0/* this_arg */
	, &ExpressionCollection_t525_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExpressionCollection_t525)/* instance_size */
	, sizeof (ExpressionCollection_t525)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_Expression.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Expression
extern TypeInfo Expression_t526_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_ExpressionMethodDeclarations.h"
static const EncodedMethodIndex Expression_t526_VTable[8] = 
{
	120,
	125,
	122,
	123,
	0,
	0,
	697,
	0,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Expression_t526_0_0_0;
extern const Il2CppType Expression_t526_1_0_0;
struct Expression_t526;
const Il2CppTypeDefinitionMetadata Expression_t526_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Expression_t526_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1983/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Expression_t526_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Expression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &Expression_t526_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Expression_t526_0_0_0/* byval_arg */
	, &Expression_t526_1_0_0/* this_arg */
	, &Expression_t526_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Expression_t526)/* instance_size */
	, sizeof (Expression_t526)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CompositeExpression
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpres.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CompositeExpression
extern TypeInfo CompositeExpression_t527_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CompositeExpression
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpresMethodDeclarations.h"
static const EncodedMethodIndex CompositeExpression_t527_VTable[8] = 
{
	120,
	125,
	122,
	123,
	0,
	0,
	697,
	698,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CompositeExpression_t527_0_0_0;
extern const Il2CppType CompositeExpression_t527_1_0_0;
struct CompositeExpression_t527;
const Il2CppTypeDefinitionMetadata CompositeExpression_t527_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t526_0_0_0/* parent */
	, CompositeExpression_t527_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1873/* fieldStart */
	, 1989/* methodStart */
	, -1/* eventStart */
	, 459/* propertyStart */

};
TypeInfo CompositeExpression_t527_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompositeExpression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &CompositeExpression_t527_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CompositeExpression_t527_0_0_0/* byval_arg */
	, &CompositeExpression_t527_1_0_0/* this_arg */
	, &CompositeExpression_t527_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompositeExpression_t527)/* instance_size */
	, sizeof (CompositeExpression_t527)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Group
#include "System_System_Text_RegularExpressions_Syntax_Group.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Group
extern TypeInfo Group_t528_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Group
#include "System_System_Text_RegularExpressions_Syntax_GroupMethodDeclarations.h"
static const EncodedMethodIndex Group_t528_VTable[8] = 
{
	120,
	125,
	122,
	123,
	699,
	700,
	701,
	698,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Group_t528_0_0_0;
extern const Il2CppType Group_t528_1_0_0;
struct Group_t528;
const Il2CppTypeDefinitionMetadata Group_t528_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t527_0_0_0/* parent */
	, Group_t528_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1993/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Group_t528_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Group"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &Group_t528_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Group_t528_0_0_0/* byval_arg */
	, &Group_t528_1_0_0/* this_arg */
	, &Group_t528_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Group_t528)/* instance_size */
	, sizeof (Group_t528)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.RegularExpression
#include "System_System_Text_RegularExpressions_Syntax_RegularExpressi.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.RegularExpression
extern TypeInfo RegularExpression_t529_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.RegularExpression
#include "System_System_Text_RegularExpressions_Syntax_RegularExpressiMethodDeclarations.h"
static const EncodedMethodIndex RegularExpression_t529_VTable[8] = 
{
	120,
	125,
	122,
	123,
	702,
	700,
	701,
	698,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RegularExpression_t529_0_0_0;
extern const Il2CppType RegularExpression_t529_1_0_0;
struct RegularExpression_t529;
const Il2CppTypeDefinitionMetadata RegularExpression_t529_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Group_t528_0_0_0/* parent */
	, RegularExpression_t529_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1874/* fieldStart */
	, 1998/* methodStart */
	, -1/* eventStart */
	, 460/* propertyStart */

};
TypeInfo RegularExpression_t529_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RegularExpression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &RegularExpression_t529_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RegularExpression_t529_0_0_0/* byval_arg */
	, &RegularExpression_t529_1_0_0/* this_arg */
	, &RegularExpression_t529_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RegularExpression_t529)/* instance_size */
	, sizeof (RegularExpression_t529)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CapturingGroup
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroup.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CapturingGroup
extern TypeInfo CapturingGroup_t530_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CapturingGroup
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroupMethodDeclarations.h"
static const EncodedMethodIndex CapturingGroup_t530_VTable[9] = 
{
	120,
	125,
	122,
	123,
	703,
	700,
	701,
	704,
	705,
};
static const Il2CppType* CapturingGroup_t530_InterfacesTypeInfos[] = 
{
	&IComparable_t1408_0_0_0,
};
static Il2CppInterfaceOffsetPair CapturingGroup_t530_InterfacesOffsets[] = 
{
	{ &IComparable_t1408_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CapturingGroup_t530_0_0_0;
extern const Il2CppType CapturingGroup_t530_1_0_0;
struct CapturingGroup_t530;
const Il2CppTypeDefinitionMetadata CapturingGroup_t530_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CapturingGroup_t530_InterfacesTypeInfos/* implementedInterfaces */
	, CapturingGroup_t530_InterfacesOffsets/* interfaceOffsets */
	, &Group_t528_0_0_0/* parent */
	, CapturingGroup_t530_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1875/* fieldStart */
	, 2001/* methodStart */
	, -1/* eventStart */
	, 461/* propertyStart */

};
TypeInfo CapturingGroup_t530_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CapturingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &CapturingGroup_t530_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CapturingGroup_t530_0_0_0/* byval_arg */
	, &CapturingGroup_t530_1_0_0/* this_arg */
	, &CapturingGroup_t530_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CapturingGroup_t530)/* instance_size */
	, sizeof (CapturingGroup_t530)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.BalancingGroup
#include "System_System_Text_RegularExpressions_Syntax_BalancingGroup.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.BalancingGroup
extern TypeInfo BalancingGroup_t531_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.BalancingGroup
#include "System_System_Text_RegularExpressions_Syntax_BalancingGroupMethodDeclarations.h"
static const EncodedMethodIndex BalancingGroup_t531_VTable[9] = 
{
	120,
	125,
	122,
	123,
	706,
	700,
	701,
	704,
	705,
};
static Il2CppInterfaceOffsetPair BalancingGroup_t531_InterfacesOffsets[] = 
{
	{ &IComparable_t1408_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType BalancingGroup_t531_0_0_0;
extern const Il2CppType BalancingGroup_t531_1_0_0;
struct BalancingGroup_t531;
const Il2CppTypeDefinitionMetadata BalancingGroup_t531_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BalancingGroup_t531_InterfacesOffsets/* interfaceOffsets */
	, &CapturingGroup_t530_0_0_0/* parent */
	, BalancingGroup_t531_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1877/* fieldStart */
	, 2010/* methodStart */
	, -1/* eventStart */
	, 464/* propertyStart */

};
TypeInfo BalancingGroup_t531_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "BalancingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &BalancingGroup_t531_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BalancingGroup_t531_0_0_0/* byval_arg */
	, &BalancingGroup_t531_1_0_0/* this_arg */
	, &BalancingGroup_t531_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BalancingGroup_t531)/* instance_size */
	, sizeof (BalancingGroup_t531)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
#include "System_System_Text_RegularExpressions_Syntax_NonBacktracking.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
extern TypeInfo NonBacktrackingGroup_t532_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
#include "System_System_Text_RegularExpressions_Syntax_NonBacktrackingMethodDeclarations.h"
static const EncodedMethodIndex NonBacktrackingGroup_t532_VTable[8] = 
{
	120,
	125,
	122,
	123,
	707,
	700,
	701,
	708,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType NonBacktrackingGroup_t532_0_0_0;
extern const Il2CppType NonBacktrackingGroup_t532_1_0_0;
struct NonBacktrackingGroup_t532;
const Il2CppTypeDefinitionMetadata NonBacktrackingGroup_t532_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Group_t528_0_0_0/* parent */
	, NonBacktrackingGroup_t532_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2013/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NonBacktrackingGroup_t532_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "NonBacktrackingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &NonBacktrackingGroup_t532_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NonBacktrackingGroup_t532_0_0_0/* byval_arg */
	, &NonBacktrackingGroup_t532_1_0_0/* this_arg */
	, &NonBacktrackingGroup_t532_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NonBacktrackingGroup_t532)/* instance_size */
	, sizeof (NonBacktrackingGroup_t532)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Repetition
#include "System_System_Text_RegularExpressions_Syntax_Repetition.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Repetition
extern TypeInfo Repetition_t533_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Repetition
#include "System_System_Text_RegularExpressions_Syntax_RepetitionMethodDeclarations.h"
static const EncodedMethodIndex Repetition_t533_VTable[8] = 
{
	120,
	125,
	122,
	123,
	709,
	710,
	711,
	698,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Repetition_t533_0_0_0;
extern const Il2CppType Repetition_t533_1_0_0;
struct Repetition_t533;
const Il2CppTypeDefinitionMetadata Repetition_t533_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t527_0_0_0/* parent */
	, Repetition_t533_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1878/* fieldStart */
	, 2016/* methodStart */
	, -1/* eventStart */
	, 465/* propertyStart */

};
TypeInfo Repetition_t533_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Repetition"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &Repetition_t533_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Repetition_t533_0_0_0/* byval_arg */
	, &Repetition_t533_1_0_0/* this_arg */
	, &Repetition_t533_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Repetition_t533)/* instance_size */
	, sizeof (Repetition_t533)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Assertion
#include "System_System_Text_RegularExpressions_Syntax_Assertion.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Assertion
extern TypeInfo Assertion_t534_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Assertion
#include "System_System_Text_RegularExpressions_Syntax_AssertionMethodDeclarations.h"
static const EncodedMethodIndex Assertion_t534_VTable[8] = 
{
	120,
	125,
	122,
	123,
	0,
	712,
	697,
	698,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Assertion_t534_0_0_0;
extern const Il2CppType Assertion_t534_1_0_0;
struct Assertion_t534;
const Il2CppTypeDefinitionMetadata Assertion_t534_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t527_0_0_0/* parent */
	, Assertion_t534_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2023/* methodStart */
	, -1/* eventStart */
	, 467/* propertyStart */

};
TypeInfo Assertion_t534_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Assertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &Assertion_t534_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Assertion_t534_0_0_0/* byval_arg */
	, &Assertion_t534_1_0_0/* this_arg */
	, &Assertion_t534_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Assertion_t534)/* instance_size */
	, sizeof (Assertion_t534)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CaptureAssertion
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertio.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CaptureAssertion
extern TypeInfo CaptureAssertion_t537_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CaptureAssertion
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertioMethodDeclarations.h"
static const EncodedMethodIndex CaptureAssertion_t537_VTable[8] = 
{
	120,
	125,
	122,
	123,
	713,
	712,
	697,
	714,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CaptureAssertion_t537_0_0_0;
extern const Il2CppType CaptureAssertion_t537_1_0_0;
struct CaptureAssertion_t537;
const Il2CppTypeDefinitionMetadata CaptureAssertion_t537_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Assertion_t534_0_0_0/* parent */
	, CaptureAssertion_t537_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1881/* fieldStart */
	, 2029/* methodStart */
	, -1/* eventStart */
	, 469/* propertyStart */

};
TypeInfo CaptureAssertion_t537_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CaptureAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &CaptureAssertion_t537_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CaptureAssertion_t537_0_0_0/* byval_arg */
	, &CaptureAssertion_t537_1_0_0/* this_arg */
	, &CaptureAssertion_t537_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CaptureAssertion_t537)/* instance_size */
	, sizeof (CaptureAssertion_t537)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsser.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.ExpressionAssertion
extern TypeInfo ExpressionAssertion_t535_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsserMethodDeclarations.h"
static const EncodedMethodIndex ExpressionAssertion_t535_VTable[8] = 
{
	120,
	125,
	122,
	123,
	715,
	712,
	697,
	716,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ExpressionAssertion_t535_0_0_0;
extern const Il2CppType ExpressionAssertion_t535_1_0_0;
struct ExpressionAssertion_t535;
const Il2CppTypeDefinitionMetadata ExpressionAssertion_t535_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Assertion_t534_0_0_0/* parent */
	, ExpressionAssertion_t535_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1884/* fieldStart */
	, 2034/* methodStart */
	, -1/* eventStart */
	, 471/* propertyStart */

};
TypeInfo ExpressionAssertion_t535_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExpressionAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &ExpressionAssertion_t535_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExpressionAssertion_t535_0_0_0/* byval_arg */
	, &ExpressionAssertion_t535_1_0_0/* this_arg */
	, &ExpressionAssertion_t535_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExpressionAssertion_t535)/* instance_size */
	, sizeof (ExpressionAssertion_t535)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Alternation
#include "System_System_Text_RegularExpressions_Syntax_Alternation.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Alternation
extern TypeInfo Alternation_t538_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Alternation
#include "System_System_Text_RegularExpressions_Syntax_AlternationMethodDeclarations.h"
static const EncodedMethodIndex Alternation_t538_VTable[8] = 
{
	120,
	125,
	122,
	123,
	717,
	718,
	697,
	698,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Alternation_t538_0_0_0;
extern const Il2CppType Alternation_t538_1_0_0;
struct Alternation_t538;
const Il2CppTypeDefinitionMetadata Alternation_t538_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t527_0_0_0/* parent */
	, Alternation_t538_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2041/* methodStart */
	, -1/* eventStart */
	, 474/* propertyStart */

};
TypeInfo Alternation_t538_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Alternation"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &Alternation_t538_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Alternation_t538_0_0_0/* byval_arg */
	, &Alternation_t538_1_0_0/* this_arg */
	, &Alternation_t538_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Alternation_t538)/* instance_size */
	, sizeof (Alternation_t538)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Literal
#include "System_System_Text_RegularExpressions_Syntax_Literal.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Literal
extern TypeInfo Literal_t536_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Literal
#include "System_System_Text_RegularExpressions_Syntax_LiteralMethodDeclarations.h"
static const EncodedMethodIndex Literal_t536_VTable[8] = 
{
	120,
	125,
	122,
	123,
	719,
	720,
	721,
	722,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Literal_t536_0_0_0;
extern const Il2CppType Literal_t536_1_0_0;
struct Literal_t536;
const Il2CppTypeDefinitionMetadata Literal_t536_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t526_0_0_0/* parent */
	, Literal_t536_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1886/* fieldStart */
	, 2046/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Literal_t536_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Literal"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &Literal_t536_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Literal_t536_0_0_0/* byval_arg */
	, &Literal_t536_1_0_0/* this_arg */
	, &Literal_t536_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Literal_t536)/* instance_size */
	, sizeof (Literal_t536)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.PositionAssertion
#include "System_System_Text_RegularExpressions_Syntax_PositionAsserti.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.PositionAssertion
extern TypeInfo PositionAssertion_t539_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.PositionAssertion
#include "System_System_Text_RegularExpressions_Syntax_PositionAssertiMethodDeclarations.h"
static const EncodedMethodIndex PositionAssertion_t539_VTable[8] = 
{
	120,
	125,
	122,
	123,
	723,
	724,
	725,
	726,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PositionAssertion_t539_0_0_0;
extern const Il2CppType PositionAssertion_t539_1_0_0;
struct PositionAssertion_t539;
const Il2CppTypeDefinitionMetadata PositionAssertion_t539_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t526_0_0_0/* parent */
	, PositionAssertion_t539_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1888/* fieldStart */
	, 2052/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PositionAssertion_t539_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PositionAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &PositionAssertion_t539_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PositionAssertion_t539_0_0_0/* byval_arg */
	, &PositionAssertion_t539_1_0_0/* this_arg */
	, &PositionAssertion_t539_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PositionAssertion_t539)/* instance_size */
	, sizeof (PositionAssertion_t539)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Reference
#include "System_System_Text_RegularExpressions_Syntax_Reference.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Reference
extern TypeInfo Reference_t540_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Reference
#include "System_System_Text_RegularExpressions_Syntax_ReferenceMethodDeclarations.h"
static const EncodedMethodIndex Reference_t540_VTable[8] = 
{
	120,
	125,
	122,
	123,
	727,
	728,
	697,
	729,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Reference_t540_0_0_0;
extern const Il2CppType Reference_t540_1_0_0;
struct Reference_t540;
const Il2CppTypeDefinitionMetadata Reference_t540_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t526_0_0_0/* parent */
	, Reference_t540_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1889/* fieldStart */
	, 2057/* methodStart */
	, -1/* eventStart */
	, 475/* propertyStart */

};
TypeInfo Reference_t540_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Reference"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &Reference_t540_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Reference_t540_0_0_0/* byval_arg */
	, &Reference_t540_1_0_0/* this_arg */
	, &Reference_t540_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Reference_t540)/* instance_size */
	, sizeof (Reference_t540)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.BackslashNumber
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumber.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.BackslashNumber
extern TypeInfo BackslashNumber_t541_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.BackslashNumber
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumberMethodDeclarations.h"
static const EncodedMethodIndex BackslashNumber_t541_VTable[8] = 
{
	120,
	125,
	122,
	123,
	730,
	728,
	697,
	729,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType BackslashNumber_t541_0_0_0;
extern const Il2CppType BackslashNumber_t541_1_0_0;
struct BackslashNumber_t541;
const Il2CppTypeDefinitionMetadata BackslashNumber_t541_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Reference_t540_0_0_0/* parent */
	, BackslashNumber_t541_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1891/* fieldStart */
	, 2064/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BackslashNumber_t541_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "BackslashNumber"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &BackslashNumber_t541_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BackslashNumber_t541_0_0_0/* byval_arg */
	, &BackslashNumber_t541_1_0_0/* this_arg */
	, &BackslashNumber_t541_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BackslashNumber_t541)/* instance_size */
	, sizeof (BackslashNumber_t541)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CharacterClass
#include "System_System_Text_RegularExpressions_Syntax_CharacterClass.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CharacterClass
extern TypeInfo CharacterClass_t543_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CharacterClass
#include "System_System_Text_RegularExpressions_Syntax_CharacterClassMethodDeclarations.h"
static const EncodedMethodIndex CharacterClass_t543_VTable[8] = 
{
	120,
	125,
	122,
	123,
	731,
	732,
	697,
	733,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CharacterClass_t543_0_0_0;
extern const Il2CppType CharacterClass_t543_1_0_0;
struct CharacterClass_t543;
const Il2CppTypeDefinitionMetadata CharacterClass_t543_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t526_0_0_0/* parent */
	, CharacterClass_t543_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1893/* fieldStart */
	, 2067/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CharacterClass_t543_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharacterClass"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &CharacterClass_t543_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CharacterClass_t543_0_0_0/* byval_arg */
	, &CharacterClass_t543_1_0_0/* this_arg */
	, &CharacterClass_t543_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CharacterClass_t543)/* instance_size */
	, sizeof (CharacterClass_t543)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CharacterClass_t543_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.AnchorInfo
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfo.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.AnchorInfo
extern TypeInfo AnchorInfo_t544_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.AnchorInfo
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfoMethodDeclarations.h"
static const EncodedMethodIndex AnchorInfo_t544_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType AnchorInfo_t544_0_0_0;
extern const Il2CppType AnchorInfo_t544_1_0_0;
struct AnchorInfo_t544;
const Il2CppTypeDefinitionMetadata AnchorInfo_t544_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AnchorInfo_t544_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1899/* fieldStart */
	, 2077/* methodStart */
	, -1/* eventStart */
	, 477/* propertyStart */

};
TypeInfo AnchorInfo_t544_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnchorInfo"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NULL/* methods */
	, &AnchorInfo_t544_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnchorInfo_t544_0_0_0/* byval_arg */
	, &AnchorInfo_t544_1_0_0/* this_arg */
	, &AnchorInfo_t544_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnchorInfo_t544)/* instance_size */
	, sizeof (AnchorInfo_t544)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 10/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DefaultUriParser
#include "System_System_DefaultUriParser.h"
// Metadata Definition System.DefaultUriParser
extern TypeInfo DefaultUriParser_t545_il2cpp_TypeInfo;
// System.DefaultUriParser
#include "System_System_DefaultUriParserMethodDeclarations.h"
static const EncodedMethodIndex DefaultUriParser_t545_VTable[6] = 
{
	120,
	125,
	122,
	123,
	734,
	735,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType DefaultUriParser_t545_0_0_0;
extern const Il2CppType DefaultUriParser_t545_1_0_0;
extern const Il2CppType UriParser_t546_0_0_0;
struct DefaultUriParser_t545;
const Il2CppTypeDefinitionMetadata DefaultUriParser_t545_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UriParser_t546_0_0_0/* parent */
	, DefaultUriParser_t545_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2091/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DefaultUriParser_t545_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultUriParser"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DefaultUriParser_t545_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultUriParser_t545_0_0_0/* byval_arg */
	, &DefaultUriParser_t545_1_0_0/* this_arg */
	, &DefaultUriParser_t545_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultUriParser_t545)/* instance_size */
	, sizeof (DefaultUriParser_t545)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.GenericUriParser
#include "System_System_GenericUriParser.h"
// Metadata Definition System.GenericUriParser
extern TypeInfo GenericUriParser_t547_il2cpp_TypeInfo;
// System.GenericUriParser
#include "System_System_GenericUriParserMethodDeclarations.h"
static const EncodedMethodIndex GenericUriParser_t547_VTable[6] = 
{
	120,
	125,
	122,
	123,
	734,
	735,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType GenericUriParser_t547_0_0_0;
extern const Il2CppType GenericUriParser_t547_1_0_0;
struct GenericUriParser_t547;
const Il2CppTypeDefinitionMetadata GenericUriParser_t547_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UriParser_t546_0_0_0/* parent */
	, GenericUriParser_t547_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GenericUriParser_t547_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericUriParser"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &GenericUriParser_t547_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericUriParser_t547_0_0_0/* byval_arg */
	, &GenericUriParser_t547_1_0_0/* this_arg */
	, &GenericUriParser_t547_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericUriParser_t547)/* instance_size */
	, sizeof (GenericUriParser_t547)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Uri
#include "System_System_Uri.h"
// Metadata Definition System.Uri
extern TypeInfo Uri_t184_il2cpp_TypeInfo;
// System.Uri
#include "System_System_UriMethodDeclarations.h"
extern const Il2CppType UriScheme_t548_0_0_0;
static const Il2CppType* Uri_t184_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UriScheme_t548_0_0_0,
};
static const EncodedMethodIndex Uri_t184_VTable[6] = 
{
	736,
	125,
	737,
	738,
	739,
	740,
};
static const Il2CppType* Uri_t184_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair Uri_t184_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Uri_t184_0_0_0;
extern const Il2CppType Uri_t184_1_0_0;
struct Uri_t184;
const Il2CppTypeDefinitionMetadata Uri_t184_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Uri_t184_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Uri_t184_InterfacesTypeInfos/* implementedInterfaces */
	, Uri_t184_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Uri_t184_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1905/* fieldStart */
	, 2093/* methodStart */
	, -1/* eventStart */
	, 487/* propertyStart */

};
TypeInfo Uri_t184_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Uri"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Uri_t184_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 503/* custom_attributes_cache */
	, &Uri_t184_0_0_0/* byval_arg */
	, &Uri_t184_1_0_0/* this_arg */
	, &Uri_t184_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Uri_t184)/* instance_size */
	, sizeof (Uri_t184)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Uri_t184_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 50/* method_count */
	, 9/* property_count */
	, 38/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"
// Metadata Definition System.Uri/UriScheme
extern TypeInfo UriScheme_t548_il2cpp_TypeInfo;
// System.Uri/UriScheme
#include "System_System_Uri_UriSchemeMethodDeclarations.h"
static const EncodedMethodIndex UriScheme_t548_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriScheme_t548_1_0_0;
const Il2CppTypeDefinitionMetadata UriScheme_t548_DefinitionMetadata = 
{
	&Uri_t184_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, UriScheme_t548_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1943/* fieldStart */
	, 2143/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UriScheme_t548_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriScheme"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &UriScheme_t548_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriScheme_t548_0_0_0/* byval_arg */
	, &UriScheme_t548_1_0_0/* this_arg */
	, &UriScheme_t548_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)UriScheme_t548_marshal/* marshal_to_native_func */
	, (methodPointerType)UriScheme_t548_marshal_back/* marshal_from_native_func */
	, (methodPointerType)UriScheme_t548_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (UriScheme_t548)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriScheme_t548)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UriScheme_t548_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UriFormatException
#include "System_System_UriFormatException.h"
// Metadata Definition System.UriFormatException
extern TypeInfo UriFormatException_t550_il2cpp_TypeInfo;
// System.UriFormatException
#include "System_System_UriFormatExceptionMethodDeclarations.h"
static const EncodedMethodIndex UriFormatException_t550_VTable[11] = 
{
	120,
	125,
	122,
	203,
	741,
	205,
	206,
	207,
	208,
	204,
	209,
};
static const Il2CppType* UriFormatException_t550_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
extern const Il2CppType _Exception_t2066_0_0_0;
static Il2CppInterfaceOffsetPair UriFormatException_t550_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriFormatException_t550_0_0_0;
extern const Il2CppType UriFormatException_t550_1_0_0;
extern const Il2CppType FormatException_t334_0_0_0;
struct UriFormatException_t550;
const Il2CppTypeDefinitionMetadata UriFormatException_t550_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UriFormatException_t550_InterfacesTypeInfos/* implementedInterfaces */
	, UriFormatException_t550_InterfacesOffsets/* interfaceOffsets */
	, &FormatException_t334_0_0_0/* parent */
	, UriFormatException_t550_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2144/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UriFormatException_t550_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriFormatException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UriFormatException_t550_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriFormatException_t550_0_0_0/* byval_arg */
	, &UriFormatException_t550_1_0_0/* this_arg */
	, &UriFormatException_t550_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriFormatException_t550)/* instance_size */
	, sizeof (UriFormatException_t550)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UriHostNameType
#include "System_System_UriHostNameType.h"
// Metadata Definition System.UriHostNameType
extern TypeInfo UriHostNameType_t551_il2cpp_TypeInfo;
// System.UriHostNameType
#include "System_System_UriHostNameTypeMethodDeclarations.h"
static const EncodedMethodIndex UriHostNameType_t551_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair UriHostNameType_t551_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriHostNameType_t551_0_0_0;
extern const Il2CppType UriHostNameType_t551_1_0_0;
const Il2CppTypeDefinitionMetadata UriHostNameType_t551_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriHostNameType_t551_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, UriHostNameType_t551_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1946/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UriHostNameType_t551_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriHostNameType"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriHostNameType_t551_0_0_0/* byval_arg */
	, &UriHostNameType_t551_1_0_0/* this_arg */
	, &UriHostNameType_t551_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriHostNameType_t551)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriHostNameType_t551)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriKind
#include "System_System_UriKind.h"
// Metadata Definition System.UriKind
extern TypeInfo UriKind_t552_il2cpp_TypeInfo;
// System.UriKind
#include "System_System_UriKindMethodDeclarations.h"
static const EncodedMethodIndex UriKind_t552_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair UriKind_t552_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriKind_t552_0_0_0;
extern const Il2CppType UriKind_t552_1_0_0;
const Il2CppTypeDefinitionMetadata UriKind_t552_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriKind_t552_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, UriKind_t552_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1952/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UriKind_t552_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriKind"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriKind_t552_0_0_0/* byval_arg */
	, &UriKind_t552_1_0_0/* this_arg */
	, &UriKind_t552_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriKind_t552)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriKind_t552)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriParser
#include "System_System_UriParser.h"
// Metadata Definition System.UriParser
extern TypeInfo UriParser_t546_il2cpp_TypeInfo;
// System.UriParser
#include "System_System_UriParserMethodDeclarations.h"
static const EncodedMethodIndex UriParser_t546_VTable[6] = 
{
	120,
	125,
	122,
	123,
	734,
	735,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriParser_t546_1_0_0;
struct UriParser_t546;
const Il2CppTypeDefinitionMetadata UriParser_t546_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UriParser_t546_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1956/* fieldStart */
	, 2148/* methodStart */
	, -1/* eventStart */
	, 496/* propertyStart */

};
TypeInfo UriParser_t546_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriParser"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UriParser_t546_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriParser_t546_0_0_0/* byval_arg */
	, &UriParser_t546_1_0_0/* this_arg */
	, &UriParser_t546_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriParser_t546)/* instance_size */
	, sizeof (UriParser_t546)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(UriParser_t546_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UriPartial
#include "System_System_UriPartial.h"
// Metadata Definition System.UriPartial
extern TypeInfo UriPartial_t553_il2cpp_TypeInfo;
// System.UriPartial
#include "System_System_UriPartialMethodDeclarations.h"
static const EncodedMethodIndex UriPartial_t553_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair UriPartial_t553_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriPartial_t553_0_0_0;
extern const Il2CppType UriPartial_t553_1_0_0;
const Il2CppTypeDefinitionMetadata UriPartial_t553_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriPartial_t553_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, UriPartial_t553_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1962/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UriPartial_t553_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriPartial"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriPartial_t553_0_0_0/* byval_arg */
	, &UriPartial_t553_1_0_0/* this_arg */
	, &UriPartial_t553_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriPartial_t553)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriPartial_t553)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriTypeConverter
#include "System_System_UriTypeConverter.h"
// Metadata Definition System.UriTypeConverter
extern TypeInfo UriTypeConverter_t554_il2cpp_TypeInfo;
// System.UriTypeConverter
#include "System_System_UriTypeConverterMethodDeclarations.h"
static const EncodedMethodIndex UriTypeConverter_t554_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriTypeConverter_t554_0_0_0;
extern const Il2CppType UriTypeConverter_t554_1_0_0;
extern const Il2CppType TypeConverter_t402_0_0_0;
struct UriTypeConverter_t554;
const Il2CppTypeDefinitionMetadata UriTypeConverter_t554_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeConverter_t402_0_0_0/* parent */
	, UriTypeConverter_t554_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UriTypeConverter_t554_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriTypeConverter"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UriTypeConverter_t554_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriTypeConverter_t554_0_0_0/* byval_arg */
	, &UriTypeConverter_t554_1_0_0/* this_arg */
	, &UriTypeConverter_t554_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriTypeConverter_t554)/* instance_size */
	, sizeof (UriTypeConverter_t554)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.Security.RemoteCertificateValidationCallback
#include "System_System_Net_Security_RemoteCertificateValidationCallba.h"
// Metadata Definition System.Net.Security.RemoteCertificateValidationCallback
extern TypeInfo RemoteCertificateValidationCallback_t414_il2cpp_TypeInfo;
// System.Net.Security.RemoteCertificateValidationCallback
#include "System_System_Net_Security_RemoteCertificateValidationCallbaMethodDeclarations.h"
static const EncodedMethodIndex RemoteCertificateValidationCallback_t414_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	742,
	743,
	744,
};
static Il2CppInterfaceOffsetPair RemoteCertificateValidationCallback_t414_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RemoteCertificateValidationCallback_t414_0_0_0;
extern const Il2CppType RemoteCertificateValidationCallback_t414_1_0_0;
struct RemoteCertificateValidationCallback_t414;
const Il2CppTypeDefinitionMetadata RemoteCertificateValidationCallback_t414_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RemoteCertificateValidationCallback_t414_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, RemoteCertificateValidationCallback_t414_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2158/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RemoteCertificateValidationCallback_t414_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemoteCertificateValidationCallback"/* name */
	, "System.Net.Security"/* namespaze */
	, NULL/* methods */
	, &RemoteCertificateValidationCallback_t414_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemoteCertificateValidationCallback_t414_0_0_0/* byval_arg */
	, &RemoteCertificateValidationCallback_t414_1_0_0/* this_arg */
	, &RemoteCertificateValidationCallback_t414_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t414/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemoteCertificateValidationCallback_t414)/* instance_size */
	, sizeof (RemoteCertificateValidationCallback_t414)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.MatchEvaluator
#include "System_System_Text_RegularExpressions_MatchEvaluator.h"
// Metadata Definition System.Text.RegularExpressions.MatchEvaluator
extern TypeInfo MatchEvaluator_t555_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MatchEvaluator
#include "System_System_Text_RegularExpressions_MatchEvaluatorMethodDeclarations.h"
static const EncodedMethodIndex MatchEvaluator_t555_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	745,
	746,
	747,
};
static Il2CppInterfaceOffsetPair MatchEvaluator_t555_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MatchEvaluator_t555_0_0_0;
extern const Il2CppType MatchEvaluator_t555_1_0_0;
struct MatchEvaluator_t555;
const Il2CppTypeDefinitionMetadata MatchEvaluator_t555_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MatchEvaluator_t555_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, MatchEvaluator_t555_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2162/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MatchEvaluator_t555_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchEvaluator"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &MatchEvaluator_t555_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchEvaluator_t555_0_0_0/* byval_arg */
	, &MatchEvaluator_t555_1_0_0/* this_arg */
	, &MatchEvaluator_t555_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_MatchEvaluator_t555/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchEvaluator_t555)/* instance_size */
	, sizeof (MatchEvaluator_t555)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "System_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
extern TypeInfo U3CPrivateImplementationDetailsU3E_t558_il2cpp_TypeInfo;
// <PrivateImplementationDetails>
#include "System_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
extern const Il2CppType U24ArrayTypeU24128_t556_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t557_0_0_0;
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t558_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U24ArrayTypeU24128_t556_0_0_0,
	&U24ArrayTypeU2412_t557_0_0_0,
};
static const EncodedMethodIndex U3CPrivateImplementationDetailsU3E_t558_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t558_0_0_0;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t558_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t558;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t558_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t558_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t558_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1967/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t558_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CPrivateImplementationDetailsU3E_t558_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 513/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t558_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t558_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t558_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t558)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t558)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t558_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$128
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$128
extern TypeInfo U24ArrayTypeU24128_t556_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$128
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24128_t556_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U24ArrayTypeU24128_t556_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24128_t556_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t558_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, U24ArrayTypeU24128_t556_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24128_t556_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$128"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24128_t556_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24128_t556_0_0_0/* byval_arg */
	, &U24ArrayTypeU24128_t556_1_0_0/* this_arg */
	, &U24ArrayTypeU24128_t556_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24128_t556_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t556_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t556_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24128_t556)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24128_t556)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24128_t556_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t557_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2412_t557_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t557_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t557_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t558_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, U24ArrayTypeU2412_t557_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2412_t557_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2412_t557_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t557_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t557_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t557_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t557_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t557_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t557_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t557)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t557)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t557_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
