﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.UInt16>
struct InternalEnumerator_1_t1688;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11259_gshared (InternalEnumerator_1_t1688 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11259(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1688 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11259_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11260_gshared (InternalEnumerator_1_t1688 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11260(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1688 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11260_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11261_gshared (InternalEnumerator_1_t1688 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11261(__this, method) (( void (*) (InternalEnumerator_1_t1688 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11261_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11262_gshared (InternalEnumerator_1_t1688 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11262(__this, method) (( bool (*) (InternalEnumerator_1_t1688 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11262_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern "C" uint16_t InternalEnumerator_1_get_Current_m11263_gshared (InternalEnumerator_1_t1688 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11263(__this, method) (( uint16_t (*) (InternalEnumerator_1_t1688 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11263_gshared)(__this, method)
