﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_t987;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
extern "C" void AssemblyCopyrightAttribute__ctor_m5811 (AssemblyCopyrightAttribute_t987 * __this, String_t* ___copyright, const MethodInfo* method) IL2CPP_METHOD_ATTR;
