﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Threading.ThreadInterruptedException
#include "mscorlib_System_Threading_ThreadInterruptedException.h"
// Metadata Definition System.Threading.ThreadInterruptedException
extern TypeInfo ThreadInterruptedException_t1277_il2cpp_TypeInfo;
// System.Threading.ThreadInterruptedException
#include "mscorlib_System_Threading_ThreadInterruptedExceptionMethodDeclarations.h"
static const EncodedMethodIndex ThreadInterruptedException_t1277_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
extern const Il2CppType ISerializable_t1426_0_0_0;
extern const Il2CppType _Exception_t2066_0_0_0;
static Il2CppInterfaceOffsetPair ThreadInterruptedException_t1277_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadInterruptedException_t1277_0_0_0;
extern const Il2CppType ThreadInterruptedException_t1277_1_0_0;
extern const Il2CppType SystemException_t597_0_0_0;
struct ThreadInterruptedException_t1277;
const Il2CppTypeDefinitionMetadata ThreadInterruptedException_t1277_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadInterruptedException_t1277_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, ThreadInterruptedException_t1277_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7996/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadInterruptedException_t1277_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadInterruptedException"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ThreadInterruptedException_t1277_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1338/* custom_attributes_cache */
	, &ThreadInterruptedException_t1277_0_0_0/* byval_arg */
	, &ThreadInterruptedException_t1277_1_0_0/* this_arg */
	, &ThreadInterruptedException_t1277_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadInterruptedException_t1277)/* instance_size */
	, sizeof (ThreadInterruptedException_t1277)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.ThreadPool
#include "mscorlib_System_Threading_ThreadPool.h"
// Metadata Definition System.Threading.ThreadPool
extern TypeInfo ThreadPool_t1278_il2cpp_TypeInfo;
// System.Threading.ThreadPool
#include "mscorlib_System_Threading_ThreadPoolMethodDeclarations.h"
static const EncodedMethodIndex ThreadPool_t1278_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadPool_t1278_0_0_0;
extern const Il2CppType ThreadPool_t1278_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct ThreadPool_t1278;
const Il2CppTypeDefinitionMetadata ThreadPool_t1278_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ThreadPool_t1278_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7998/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadPool_t1278_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadPool"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ThreadPool_t1278_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ThreadPool_t1278_0_0_0/* byval_arg */
	, &ThreadPool_t1278_1_0_0/* this_arg */
	, &ThreadPool_t1278_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadPool_t1278)/* instance_size */
	, sizeof (ThreadPool_t1278)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Threading.ThreadState
#include "mscorlib_System_Threading_ThreadState.h"
// Metadata Definition System.Threading.ThreadState
extern TypeInfo ThreadState_t1279_il2cpp_TypeInfo;
// System.Threading.ThreadState
#include "mscorlib_System_Threading_ThreadStateMethodDeclarations.h"
static const EncodedMethodIndex ThreadState_t1279_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
extern const Il2CppType IFormattable_t1406_0_0_0;
extern const Il2CppType IConvertible_t1409_0_0_0;
extern const Il2CppType IComparable_t1408_0_0_0;
static Il2CppInterfaceOffsetPair ThreadState_t1279_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadState_t1279_0_0_0;
extern const Il2CppType ThreadState_t1279_1_0_0;
extern const Il2CppType Enum_t305_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t320_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata ThreadState_t1279_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadState_t1279_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, ThreadState_t1279_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5108/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadState_t1279_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadState"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1339/* custom_attributes_cache */
	, &ThreadState_t1279_0_0_0/* byval_arg */
	, &ThreadState_t1279_1_0_0/* this_arg */
	, &ThreadState_t1279_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadState_t1279)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ThreadState_t1279)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Threading.ThreadStateException
#include "mscorlib_System_Threading_ThreadStateException.h"
// Metadata Definition System.Threading.ThreadStateException
extern TypeInfo ThreadStateException_t1280_il2cpp_TypeInfo;
// System.Threading.ThreadStateException
#include "mscorlib_System_Threading_ThreadStateExceptionMethodDeclarations.h"
static const EncodedMethodIndex ThreadStateException_t1280_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair ThreadStateException_t1280_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadStateException_t1280_0_0_0;
extern const Il2CppType ThreadStateException_t1280_1_0_0;
struct ThreadStateException_t1280;
const Il2CppTypeDefinitionMetadata ThreadStateException_t1280_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadStateException_t1280_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, ThreadStateException_t1280_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7999/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadStateException_t1280_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadStateException"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ThreadStateException_t1280_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1340/* custom_attributes_cache */
	, &ThreadStateException_t1280_0_0_0/* byval_arg */
	, &ThreadStateException_t1280_1_0_0/* this_arg */
	, &ThreadStateException_t1280_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadStateException_t1280)/* instance_size */
	, sizeof (ThreadStateException_t1280)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.Timer
#include "mscorlib_System_Threading_Timer.h"
// Metadata Definition System.Threading.Timer
extern TypeInfo Timer_t1076_il2cpp_TypeInfo;
// System.Threading.Timer
#include "mscorlib_System_Threading_TimerMethodDeclarations.h"
extern const Il2CppType TimerComparer_t1281_0_0_0;
extern const Il2CppType Scheduler_t1282_0_0_0;
static const Il2CppType* Timer_t1076_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TimerComparer_t1281_0_0_0,
	&Scheduler_t1282_0_0_0,
};
static const EncodedMethodIndex Timer_t1076_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2988,
};
extern const Il2CppType IDisposable_t319_0_0_0;
static const Il2CppType* Timer_t1076_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
};
static Il2CppInterfaceOffsetPair Timer_t1076_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Timer_t1076_0_0_0;
extern const Il2CppType Timer_t1076_1_0_0;
extern const Il2CppType MarshalByRefObject_t434_0_0_0;
struct Timer_t1076;
const Il2CppTypeDefinitionMetadata Timer_t1076_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Timer_t1076_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Timer_t1076_InterfacesTypeInfos/* implementedInterfaces */
	, Timer_t1076_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t434_0_0_0/* parent */
	, Timer_t1076_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5119/* fieldStart */
	, 8001/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Timer_t1076_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Timer"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Timer_t1076_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1341/* custom_attributes_cache */
	, &Timer_t1076_0_0_0/* byval_arg */
	, &Timer_t1076_1_0_0/* this_arg */
	, &Timer_t1076_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Timer_t1076)/* instance_size */
	, sizeof (Timer_t1076)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Timer_t1076_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.Timer/TimerComparer
#include "mscorlib_System_Threading_Timer_TimerComparer.h"
// Metadata Definition System.Threading.Timer/TimerComparer
extern TypeInfo TimerComparer_t1281_il2cpp_TypeInfo;
// System.Threading.Timer/TimerComparer
#include "mscorlib_System_Threading_Timer_TimerComparerMethodDeclarations.h"
static const EncodedMethodIndex TimerComparer_t1281_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2989,
};
extern const Il2CppType IComparer_t390_0_0_0;
static const Il2CppType* TimerComparer_t1281_InterfacesTypeInfos[] = 
{
	&IComparer_t390_0_0_0,
};
static Il2CppInterfaceOffsetPair TimerComparer_t1281_InterfacesOffsets[] = 
{
	{ &IComparer_t390_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimerComparer_t1281_1_0_0;
struct TimerComparer_t1281;
const Il2CppTypeDefinitionMetadata TimerComparer_t1281_DefinitionMetadata = 
{
	&Timer_t1076_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, TimerComparer_t1281_InterfacesTypeInfos/* implementedInterfaces */
	, TimerComparer_t1281_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TimerComparer_t1281_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8005/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TimerComparer_t1281_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimerComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &TimerComparer_t1281_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TimerComparer_t1281_0_0_0/* byval_arg */
	, &TimerComparer_t1281_1_0_0/* this_arg */
	, &TimerComparer_t1281_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimerComparer_t1281)/* instance_size */
	, sizeof (TimerComparer_t1281)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.Timer/Scheduler
#include "mscorlib_System_Threading_Timer_Scheduler.h"
// Metadata Definition System.Threading.Timer/Scheduler
extern TypeInfo Scheduler_t1282_il2cpp_TypeInfo;
// System.Threading.Timer/Scheduler
#include "mscorlib_System_Threading_Timer_SchedulerMethodDeclarations.h"
static const EncodedMethodIndex Scheduler_t1282_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Scheduler_t1282_1_0_0;
struct Scheduler_t1282;
const Il2CppTypeDefinitionMetadata Scheduler_t1282_DefinitionMetadata = 
{
	&Timer_t1076_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Scheduler_t1282_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5126/* fieldStart */
	, 8007/* methodStart */
	, -1/* eventStart */
	, 1595/* propertyStart */

};
TypeInfo Scheduler_t1282_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Scheduler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Scheduler_t1282_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Scheduler_t1282_0_0_0/* byval_arg */
	, &Scheduler_t1282_1_0_0/* this_arg */
	, &Scheduler_t1282_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Scheduler_t1282)/* instance_size */
	, sizeof (Scheduler_t1282)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Scheduler_t1282_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Threading.WaitHandle
#include "mscorlib_System_Threading_WaitHandle.h"
// Metadata Definition System.Threading.WaitHandle
extern TypeInfo WaitHandle_t738_il2cpp_TypeInfo;
// System.Threading.WaitHandle
#include "mscorlib_System_Threading_WaitHandleMethodDeclarations.h"
static const EncodedMethodIndex WaitHandle_t738_VTable[10] = 
{
	120,
	2978,
	122,
	123,
	2979,
	2980,
	2981,
	2982,
	2983,
	2984,
};
static const Il2CppType* WaitHandle_t738_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
};
static Il2CppInterfaceOffsetPair WaitHandle_t738_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WaitHandle_t738_0_0_0;
extern const Il2CppType WaitHandle_t738_1_0_0;
struct WaitHandle_t738;
const Il2CppTypeDefinitionMetadata WaitHandle_t738_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WaitHandle_t738_InterfacesTypeInfos/* implementedInterfaces */
	, WaitHandle_t738_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t434_0_0_0/* parent */
	, WaitHandle_t738_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5128/* fieldStart */
	, 8016/* methodStart */
	, -1/* eventStart */
	, 1596/* propertyStart */

};
TypeInfo WaitHandle_t738_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaitHandle"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &WaitHandle_t738_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1342/* custom_attributes_cache */
	, &WaitHandle_t738_0_0_0/* byval_arg */
	, &WaitHandle_t738_1_0_0/* this_arg */
	, &WaitHandle_t738_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaitHandle_t738)/* instance_size */
	, sizeof (WaitHandle_t738)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WaitHandle_t738_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.AccessViolationException
#include "mscorlib_System_AccessViolationException.h"
// Metadata Definition System.AccessViolationException
extern TypeInfo AccessViolationException_t1284_il2cpp_TypeInfo;
// System.AccessViolationException
#include "mscorlib_System_AccessViolationExceptionMethodDeclarations.h"
static const EncodedMethodIndex AccessViolationException_t1284_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair AccessViolationException_t1284_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AccessViolationException_t1284_0_0_0;
extern const Il2CppType AccessViolationException_t1284_1_0_0;
struct AccessViolationException_t1284;
const Il2CppTypeDefinitionMetadata AccessViolationException_t1284_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AccessViolationException_t1284_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, AccessViolationException_t1284_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8027/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AccessViolationException_t1284_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AccessViolationException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AccessViolationException_t1284_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1344/* custom_attributes_cache */
	, &AccessViolationException_t1284_0_0_0/* byval_arg */
	, &AccessViolationException_t1284_1_0_0/* this_arg */
	, &AccessViolationException_t1284_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AccessViolationException_t1284)/* instance_size */
	, sizeof (AccessViolationException_t1284)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ActivationContext
#include "mscorlib_System_ActivationContext.h"
// Metadata Definition System.ActivationContext
extern TypeInfo ActivationContext_t1285_il2cpp_TypeInfo;
// System.ActivationContext
#include "mscorlib_System_ActivationContextMethodDeclarations.h"
static const EncodedMethodIndex ActivationContext_t1285_VTable[6] = 
{
	120,
	2990,
	122,
	123,
	2991,
	2992,
};
static const Il2CppType* ActivationContext_t1285_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair ActivationContext_t1285_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivationContext_t1285_0_0_0;
extern const Il2CppType ActivationContext_t1285_1_0_0;
struct ActivationContext_t1285;
const Il2CppTypeDefinitionMetadata ActivationContext_t1285_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ActivationContext_t1285_InterfacesTypeInfos/* implementedInterfaces */
	, ActivationContext_t1285_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ActivationContext_t1285_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5132/* fieldStart */
	, 8029/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ActivationContext_t1285_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivationContext"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ActivationContext_t1285_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1345/* custom_attributes_cache */
	, &ActivationContext_t1285_0_0_0/* byval_arg */
	, &ActivationContext_t1285_1_0_0/* this_arg */
	, &ActivationContext_t1285_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivationContext_t1285)/* instance_size */
	, sizeof (ActivationContext_t1285)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Activator
#include "mscorlib_System_Activator.h"
// Metadata Definition System.Activator
extern TypeInfo Activator_t1286_il2cpp_TypeInfo;
// System.Activator
#include "mscorlib_System_ActivatorMethodDeclarations.h"
extern const Il2CppType Activator_CreateInstance_m13478_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Activator_CreateInstance_m13478_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 3635 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3635 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex Activator_t1286_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern const Il2CppType _Activator_t2109_0_0_0;
static const Il2CppType* Activator_t1286_InterfacesTypeInfos[] = 
{
	&_Activator_t2109_0_0_0,
};
static Il2CppInterfaceOffsetPair Activator_t1286_InterfacesOffsets[] = 
{
	{ &_Activator_t2109_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Activator_t1286_0_0_0;
extern const Il2CppType Activator_t1286_1_0_0;
struct Activator_t1286;
const Il2CppTypeDefinitionMetadata Activator_t1286_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Activator_t1286_InterfacesTypeInfos/* implementedInterfaces */
	, Activator_t1286_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Activator_t1286_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8033/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Activator_t1286_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Activator"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Activator_t1286_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1347/* custom_attributes_cache */
	, &Activator_t1286_0_0_0/* byval_arg */
	, &Activator_t1286_1_0_0/* this_arg */
	, &Activator_t1286_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Activator_t1286)/* instance_size */
	, sizeof (Activator_t1286)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.AppDomain
#include "mscorlib_System_AppDomain.h"
// Metadata Definition System.AppDomain
extern TypeInfo AppDomain_t1236_il2cpp_TypeInfo;
// System.AppDomain
#include "mscorlib_System_AppDomainMethodDeclarations.h"
static const EncodedMethodIndex AppDomain_t1236_VTable[4] = 
{
	120,
	125,
	122,
	2993,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomain_t1236_0_0_0;
extern const Il2CppType AppDomain_t1236_1_0_0;
struct AppDomain_t1236;
const Il2CppTypeDefinitionMetadata AppDomain_t1236_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MarshalByRefObject_t434_0_0_0/* parent */
	, AppDomain_t1236_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5133/* fieldStart */
	, 8042/* methodStart */
	, -1/* eventStart */
	, 1597/* propertyStart */

};
TypeInfo AppDomain_t1236_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomain"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AppDomain_t1236_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1349/* custom_attributes_cache */
	, &AppDomain_t1236_0_0_0/* byval_arg */
	, &AppDomain_t1236_1_0_0/* this_arg */
	, &AppDomain_t1236_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomain_t1236)/* instance_size */
	, sizeof (AppDomain_t1236)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AppDomain_t1236_StaticFields)/* static_fields_size */
	, sizeof(AppDomain_t1236_ThreadStaticFields)/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 21/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.AppDomainManager
#include "mscorlib_System_AppDomainManager.h"
// Metadata Definition System.AppDomainManager
extern TypeInfo AppDomainManager_t1287_il2cpp_TypeInfo;
// System.AppDomainManager
#include "mscorlib_System_AppDomainManagerMethodDeclarations.h"
static const EncodedMethodIndex AppDomainManager_t1287_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainManager_t1287_0_0_0;
extern const Il2CppType AppDomainManager_t1287_1_0_0;
struct AppDomainManager_t1287;
const Il2CppTypeDefinitionMetadata AppDomainManager_t1287_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MarshalByRefObject_t434_0_0_0/* parent */
	, AppDomainManager_t1287_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AppDomainManager_t1287_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainManager"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AppDomainManager_t1287_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1354/* custom_attributes_cache */
	, &AppDomainManager_t1287_0_0_0/* byval_arg */
	, &AppDomainManager_t1287_1_0_0/* this_arg */
	, &AppDomainManager_t1287_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainManager_t1287)/* instance_size */
	, sizeof (AppDomainManager_t1287)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.AppDomainSetup
#include "mscorlib_System_AppDomainSetup.h"
// Metadata Definition System.AppDomainSetup
extern TypeInfo AppDomainSetup_t1294_il2cpp_TypeInfo;
// System.AppDomainSetup
#include "mscorlib_System_AppDomainSetupMethodDeclarations.h"
static const EncodedMethodIndex AppDomainSetup_t1294_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainSetup_t1294_0_0_0;
extern const Il2CppType AppDomainSetup_t1294_1_0_0;
struct AppDomainSetup_t1294;
const Il2CppTypeDefinitionMetadata AppDomainSetup_t1294_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AppDomainSetup_t1294_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5154/* fieldStart */
	, 8054/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AppDomainSetup_t1294_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainSetup"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AppDomainSetup_t1294_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1355/* custom_attributes_cache */
	, &AppDomainSetup_t1294_0_0_0/* byval_arg */
	, &AppDomainSetup_t1294_1_0_0/* this_arg */
	, &AppDomainSetup_t1294_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainSetup_t1294)/* instance_size */
	, sizeof (AppDomainSetup_t1294)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ApplicationException
#include "mscorlib_System_ApplicationException.h"
// Metadata Definition System.ApplicationException
extern TypeInfo ApplicationException_t1295_il2cpp_TypeInfo;
// System.ApplicationException
#include "mscorlib_System_ApplicationExceptionMethodDeclarations.h"
static const EncodedMethodIndex ApplicationException_t1295_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair ApplicationException_t1295_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ApplicationException_t1295_0_0_0;
extern const Il2CppType ApplicationException_t1295_1_0_0;
extern const Il2CppType Exception_t65_0_0_0;
struct ApplicationException_t1295;
const Il2CppTypeDefinitionMetadata ApplicationException_t1295_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ApplicationException_t1295_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t65_0_0_0/* parent */
	, ApplicationException_t1295_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8055/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ApplicationException_t1295_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ApplicationException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ApplicationException_t1295_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1356/* custom_attributes_cache */
	, &ApplicationException_t1295_0_0_0/* byval_arg */
	, &ApplicationException_t1295_1_0_0/* this_arg */
	, &ApplicationException_t1295_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ApplicationException_t1295)/* instance_size */
	, sizeof (ApplicationException_t1295)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ApplicationIdentity
#include "mscorlib_System_ApplicationIdentity.h"
// Metadata Definition System.ApplicationIdentity
extern TypeInfo ApplicationIdentity_t1288_il2cpp_TypeInfo;
// System.ApplicationIdentity
#include "mscorlib_System_ApplicationIdentityMethodDeclarations.h"
static const EncodedMethodIndex ApplicationIdentity_t1288_VTable[5] = 
{
	120,
	125,
	122,
	2994,
	2995,
};
static const Il2CppType* ApplicationIdentity_t1288_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair ApplicationIdentity_t1288_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ApplicationIdentity_t1288_0_0_0;
extern const Il2CppType ApplicationIdentity_t1288_1_0_0;
struct ApplicationIdentity_t1288;
const Il2CppTypeDefinitionMetadata ApplicationIdentity_t1288_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ApplicationIdentity_t1288_InterfacesTypeInfos/* implementedInterfaces */
	, ApplicationIdentity_t1288_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ApplicationIdentity_t1288_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5176/* fieldStart */
	, 8058/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ApplicationIdentity_t1288_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ApplicationIdentity"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ApplicationIdentity_t1288_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1357/* custom_attributes_cache */
	, &ApplicationIdentity_t1288_0_0_0/* byval_arg */
	, &ApplicationIdentity_t1288_1_0_0/* this_arg */
	, &ApplicationIdentity_t1288_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ApplicationIdentity_t1288)/* instance_size */
	, sizeof (ApplicationIdentity_t1288)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// Metadata Definition System.ArgumentException
extern TypeInfo ArgumentException_t313_il2cpp_TypeInfo;
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
static const EncodedMethodIndex ArgumentException_t313_VTable[12] = 
{
	120,
	125,
	122,
	203,
	2869,
	205,
	2870,
	207,
	208,
	2869,
	209,
	2871,
};
static Il2CppInterfaceOffsetPair ArgumentException_t313_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgumentException_t313_0_0_0;
extern const Il2CppType ArgumentException_t313_1_0_0;
struct ArgumentException_t313;
const Il2CppTypeDefinitionMetadata ArgumentException_t313_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArgumentException_t313_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, ArgumentException_t313_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5177/* fieldStart */
	, 8060/* methodStart */
	, -1/* eventStart */
	, 1598/* propertyStart */

};
TypeInfo ArgumentException_t313_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ArgumentException_t313_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1359/* custom_attributes_cache */
	, &ArgumentException_t313_0_0_0/* byval_arg */
	, &ArgumentException_t313_1_0_0/* this_arg */
	, &ArgumentException_t313_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentException_t313)/* instance_size */
	, sizeof (ArgumentException_t313)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// Metadata Definition System.ArgumentNullException
extern TypeInfo ArgumentNullException_t341_il2cpp_TypeInfo;
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
static const EncodedMethodIndex ArgumentNullException_t341_VTable[12] = 
{
	120,
	125,
	122,
	203,
	2869,
	205,
	2870,
	207,
	208,
	2869,
	209,
	2871,
};
static Il2CppInterfaceOffsetPair ArgumentNullException_t341_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgumentNullException_t341_0_0_0;
extern const Il2CppType ArgumentNullException_t341_1_0_0;
struct ArgumentNullException_t341;
const Il2CppTypeDefinitionMetadata ArgumentNullException_t341_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArgumentNullException_t341_InterfacesOffsets/* interfaceOffsets */
	, &ArgumentException_t313_0_0_0/* parent */
	, ArgumentNullException_t341_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5179/* fieldStart */
	, 8069/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArgumentNullException_t341_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentNullException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ArgumentNullException_t341_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1360/* custom_attributes_cache */
	, &ArgumentNullException_t341_0_0_0/* byval_arg */
	, &ArgumentNullException_t341_1_0_0/* this_arg */
	, &ArgumentNullException_t341_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentNullException_t341)/* instance_size */
	, sizeof (ArgumentNullException_t341)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Metadata Definition System.ArgumentOutOfRangeException
extern TypeInfo ArgumentOutOfRangeException_t343_il2cpp_TypeInfo;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
static const EncodedMethodIndex ArgumentOutOfRangeException_t343_VTable[12] = 
{
	120,
	125,
	122,
	203,
	2996,
	205,
	2997,
	207,
	208,
	2996,
	209,
	2871,
};
static Il2CppInterfaceOffsetPair ArgumentOutOfRangeException_t343_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgumentOutOfRangeException_t343_0_0_0;
extern const Il2CppType ArgumentOutOfRangeException_t343_1_0_0;
struct ArgumentOutOfRangeException_t343;
const Il2CppTypeDefinitionMetadata ArgumentOutOfRangeException_t343_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArgumentOutOfRangeException_t343_InterfacesOffsets/* interfaceOffsets */
	, &ArgumentException_t313_0_0_0/* parent */
	, ArgumentOutOfRangeException_t343_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5180/* fieldStart */
	, 8073/* methodStart */
	, -1/* eventStart */
	, 1600/* propertyStart */

};
TypeInfo ArgumentOutOfRangeException_t343_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentOutOfRangeException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ArgumentOutOfRangeException_t343_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1361/* custom_attributes_cache */
	, &ArgumentOutOfRangeException_t343_0_0_0/* byval_arg */
	, &ArgumentOutOfRangeException_t343_1_0_0/* this_arg */
	, &ArgumentOutOfRangeException_t343_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentOutOfRangeException_t343)/* instance_size */
	, sizeof (ArgumentOutOfRangeException_t343)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ArithmeticException
#include "mscorlib_System_ArithmeticException.h"
// Metadata Definition System.ArithmeticException
extern TypeInfo ArithmeticException_t739_il2cpp_TypeInfo;
// System.ArithmeticException
#include "mscorlib_System_ArithmeticExceptionMethodDeclarations.h"
static const EncodedMethodIndex ArithmeticException_t739_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair ArithmeticException_t739_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArithmeticException_t739_0_0_0;
extern const Il2CppType ArithmeticException_t739_1_0_0;
struct ArithmeticException_t739;
const Il2CppTypeDefinitionMetadata ArithmeticException_t739_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArithmeticException_t739_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, ArithmeticException_t739_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8080/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArithmeticException_t739_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArithmeticException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ArithmeticException_t739_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1362/* custom_attributes_cache */
	, &ArithmeticException_t739_0_0_0/* byval_arg */
	, &ArithmeticException_t739_1_0_0/* this_arg */
	, &ArithmeticException_t739_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArithmeticException_t739)/* instance_size */
	, sizeof (ArithmeticException_t739)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ArrayTypeMismatchException
#include "mscorlib_System_ArrayTypeMismatchException.h"
// Metadata Definition System.ArrayTypeMismatchException
extern TypeInfo ArrayTypeMismatchException_t1296_il2cpp_TypeInfo;
// System.ArrayTypeMismatchException
#include "mscorlib_System_ArrayTypeMismatchExceptionMethodDeclarations.h"
static const EncodedMethodIndex ArrayTypeMismatchException_t1296_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair ArrayTypeMismatchException_t1296_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayTypeMismatchException_t1296_0_0_0;
extern const Il2CppType ArrayTypeMismatchException_t1296_1_0_0;
struct ArrayTypeMismatchException_t1296;
const Il2CppTypeDefinitionMetadata ArrayTypeMismatchException_t1296_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArrayTypeMismatchException_t1296_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, ArrayTypeMismatchException_t1296_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5181/* fieldStart */
	, 8083/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArrayTypeMismatchException_t1296_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayTypeMismatchException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ArrayTypeMismatchException_t1296_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1363/* custom_attributes_cache */
	, &ArrayTypeMismatchException_t1296_0_0_0/* byval_arg */
	, &ArrayTypeMismatchException_t1296_1_0_0/* this_arg */
	, &ArrayTypeMismatchException_t1296_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayTypeMismatchException_t1296)/* instance_size */
	, sizeof (ArrayTypeMismatchException_t1296)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.AssemblyLoadEventArgs
#include "mscorlib_System_AssemblyLoadEventArgs.h"
// Metadata Definition System.AssemblyLoadEventArgs
extern TypeInfo AssemblyLoadEventArgs_t1297_il2cpp_TypeInfo;
// System.AssemblyLoadEventArgs
#include "mscorlib_System_AssemblyLoadEventArgsMethodDeclarations.h"
static const EncodedMethodIndex AssemblyLoadEventArgs_t1297_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyLoadEventArgs_t1297_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t1297_1_0_0;
extern const Il2CppType EventArgs_t648_0_0_0;
struct AssemblyLoadEventArgs_t1297;
const Il2CppTypeDefinitionMetadata AssemblyLoadEventArgs_t1297_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t648_0_0_0/* parent */
	, AssemblyLoadEventArgs_t1297_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyLoadEventArgs_t1297_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyLoadEventArgs"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AssemblyLoadEventArgs_t1297_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1364/* custom_attributes_cache */
	, &AssemblyLoadEventArgs_t1297_0_0_0/* byval_arg */
	, &AssemblyLoadEventArgs_t1297_1_0_0/* this_arg */
	, &AssemblyLoadEventArgs_t1297_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyLoadEventArgs_t1297)/* instance_size */
	, sizeof (AssemblyLoadEventArgs_t1297)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.AttributeTargets
#include "mscorlib_System_AttributeTargets.h"
// Metadata Definition System.AttributeTargets
extern TypeInfo AttributeTargets_t1298_il2cpp_TypeInfo;
// System.AttributeTargets
#include "mscorlib_System_AttributeTargetsMethodDeclarations.h"
static const EncodedMethodIndex AttributeTargets_t1298_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair AttributeTargets_t1298_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AttributeTargets_t1298_0_0_0;
extern const Il2CppType AttributeTargets_t1298_1_0_0;
const Il2CppTypeDefinitionMetadata AttributeTargets_t1298_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AttributeTargets_t1298_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, AttributeTargets_t1298_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5182/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AttributeTargets_t1298_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeTargets"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1365/* custom_attributes_cache */
	, &AttributeTargets_t1298_0_0_0/* byval_arg */
	, &AttributeTargets_t1298_1_0_0/* this_arg */
	, &AttributeTargets_t1298_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeTargets_t1298)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AttributeTargets_t1298)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.BitConverter
#include "mscorlib_System_BitConverter.h"
// Metadata Definition System.BitConverter
extern TypeInfo BitConverter_t578_il2cpp_TypeInfo;
// System.BitConverter
#include "mscorlib_System_BitConverterMethodDeclarations.h"
static const EncodedMethodIndex BitConverter_t578_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BitConverter_t578_0_0_0;
extern const Il2CppType BitConverter_t578_1_0_0;
struct BitConverter_t578;
const Il2CppTypeDefinitionMetadata BitConverter_t578_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BitConverter_t578_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5199/* fieldStart */
	, 8086/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BitConverter_t578_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BitConverter"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &BitConverter_t578_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BitConverter_t578_0_0_0/* byval_arg */
	, &BitConverter_t578_1_0_0/* this_arg */
	, &BitConverter_t578_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BitConverter_t578)/* instance_size */
	, sizeof (BitConverter_t578)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BitConverter_t578_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Buffer
#include "mscorlib_System_Buffer.h"
// Metadata Definition System.Buffer
extern TypeInfo Buffer_t1299_il2cpp_TypeInfo;
// System.Buffer
#include "mscorlib_System_BufferMethodDeclarations.h"
static const EncodedMethodIndex Buffer_t1299_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Buffer_t1299_0_0_0;
extern const Il2CppType Buffer_t1299_1_0_0;
struct Buffer_t1299;
const Il2CppTypeDefinitionMetadata Buffer_t1299_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Buffer_t1299_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8096/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Buffer_t1299_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Buffer"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Buffer_t1299_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1366/* custom_attributes_cache */
	, &Buffer_t1299_0_0_0/* byval_arg */
	, &Buffer_t1299_1_0_0/* this_arg */
	, &Buffer_t1299_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Buffer_t1299)/* instance_size */
	, sizeof (Buffer_t1299)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.CharEnumerator
#include "mscorlib_System_CharEnumerator.h"
// Metadata Definition System.CharEnumerator
extern TypeInfo CharEnumerator_t1300_il2cpp_TypeInfo;
// System.CharEnumerator
#include "mscorlib_System_CharEnumeratorMethodDeclarations.h"
static const EncodedMethodIndex CharEnumerator_t1300_VTable[8] = 
{
	120,
	125,
	122,
	123,
	2998,
	2999,
	3000,
	3001,
};
extern const Il2CppType IEnumerator_t279_0_0_0;
extern const Il2CppType ICloneable_t2056_0_0_0;
extern const Il2CppType IEnumerator_1_t1389_0_0_0;
static const Il2CppType* CharEnumerator_t1300_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDisposable_t319_0_0_0,
	&ICloneable_t2056_0_0_0,
	&IEnumerator_1_t1389_0_0_0,
};
static Il2CppInterfaceOffsetPair CharEnumerator_t1300_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &IDisposable_t319_0_0_0, 6},
	{ &ICloneable_t2056_0_0_0, 7},
	{ &IEnumerator_1_t1389_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CharEnumerator_t1300_0_0_0;
extern const Il2CppType CharEnumerator_t1300_1_0_0;
struct CharEnumerator_t1300;
const Il2CppTypeDefinitionMetadata CharEnumerator_t1300_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CharEnumerator_t1300_InterfacesTypeInfos/* implementedInterfaces */
	, CharEnumerator_t1300_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CharEnumerator_t1300_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5201/* fieldStart */
	, 8100/* methodStart */
	, -1/* eventStart */
	, 1601/* propertyStart */

};
TypeInfo CharEnumerator_t1300_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharEnumerator"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &CharEnumerator_t1300_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1367/* custom_attributes_cache */
	, &CharEnumerator_t1300_0_0_0/* byval_arg */
	, &CharEnumerator_t1300_1_0_0/* this_arg */
	, &CharEnumerator_t1300_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CharEnumerator_t1300)/* instance_size */
	, sizeof (CharEnumerator_t1300)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Console
#include "mscorlib_System_Console.h"
// Metadata Definition System.Console
extern TypeInfo Console_t598_il2cpp_TypeInfo;
// System.Console
#include "mscorlib_System_ConsoleMethodDeclarations.h"
static const EncodedMethodIndex Console_t598_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Console_t598_0_0_0;
extern const Il2CppType Console_t598_1_0_0;
struct Console_t598;
const Il2CppTypeDefinitionMetadata Console_t598_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Console_t598_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5204/* fieldStart */
	, 8105/* methodStart */
	, -1/* eventStart */
	, 1603/* propertyStart */

};
TypeInfo Console_t598_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Console"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Console_t598_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Console_t598_0_0_0/* byval_arg */
	, &Console_t598_1_0_0/* this_arg */
	, &Console_t598_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Console_t598)/* instance_size */
	, sizeof (Console_t598)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Console_t598_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 385/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ContextBoundObject
#include "mscorlib_System_ContextBoundObject.h"
// Metadata Definition System.ContextBoundObject
extern TypeInfo ContextBoundObject_t1301_il2cpp_TypeInfo;
// System.ContextBoundObject
#include "mscorlib_System_ContextBoundObjectMethodDeclarations.h"
static const EncodedMethodIndex ContextBoundObject_t1301_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContextBoundObject_t1301_0_0_0;
extern const Il2CppType ContextBoundObject_t1301_1_0_0;
struct ContextBoundObject_t1301;
const Il2CppTypeDefinitionMetadata ContextBoundObject_t1301_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MarshalByRefObject_t434_0_0_0/* parent */
	, ContextBoundObject_t1301_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8112/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ContextBoundObject_t1301_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextBoundObject"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ContextBoundObject_t1301_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1368/* custom_attributes_cache */
	, &ContextBoundObject_t1301_0_0_0/* byval_arg */
	, &ContextBoundObject_t1301_1_0_0/* this_arg */
	, &ContextBoundObject_t1301_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextBoundObject_t1301)/* instance_size */
	, sizeof (ContextBoundObject_t1301)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Convert
#include "mscorlib_System_Convert.h"
// Metadata Definition System.Convert
extern TypeInfo Convert_t335_il2cpp_TypeInfo;
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"
static const EncodedMethodIndex Convert_t335_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Convert_t335_0_0_0;
extern const Il2CppType Convert_t335_1_0_0;
struct Convert_t335;
const Il2CppTypeDefinitionMetadata Convert_t335_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Convert_t335_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5209/* fieldStart */
	, 8113/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Convert_t335_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Convert"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Convert_t335_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Convert_t335_0_0_0/* byval_arg */
	, &Convert_t335_1_0_0/* this_arg */
	, &Convert_t335_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Convert_t335)/* instance_size */
	, sizeof (Convert_t335)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Convert_t335_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 206/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DBNull
#include "mscorlib_System_DBNull.h"
// Metadata Definition System.DBNull
extern TypeInfo DBNull_t1302_il2cpp_TypeInfo;
// System.DBNull
#include "mscorlib_System_DBNullMethodDeclarations.h"
static const EncodedMethodIndex DBNull_t1302_VTable[21] = 
{
	120,
	125,
	122,
	3002,
	3003,
	3004,
	3005,
	3006,
	3007,
	3008,
	3009,
	3010,
	3011,
	3012,
	3013,
	3014,
	3015,
	3016,
	3017,
	3018,
	3019,
};
static const Il2CppType* DBNull_t1302_InterfacesTypeInfos[] = 
{
	&IConvertible_t1409_0_0_0,
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair DBNull_t1302_InterfacesOffsets[] = 
{
	{ &IConvertible_t1409_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 20},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DBNull_t1302_0_0_0;
extern const Il2CppType DBNull_t1302_1_0_0;
struct DBNull_t1302;
const Il2CppTypeDefinitionMetadata DBNull_t1302_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DBNull_t1302_InterfacesTypeInfos/* implementedInterfaces */
	, DBNull_t1302_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DBNull_t1302_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5211/* fieldStart */
	, 8319/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DBNull_t1302_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DBNull"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DBNull_t1302_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1467/* custom_attributes_cache */
	, &DBNull_t1302_0_0_0/* byval_arg */
	, &DBNull_t1302_1_0_0/* this_arg */
	, &DBNull_t1302_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DBNull_t1302)/* instance_size */
	, sizeof (DBNull_t1302)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DBNull_t1302_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 21/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.DateTime
#include "mscorlib_System_DateTime.h"
// Metadata Definition System.DateTime
extern TypeInfo DateTime_t51_il2cpp_TypeInfo;
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
extern const Il2CppType Which_t1303_0_0_0;
static const Il2CppType* DateTime_t51_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Which_t1303_0_0_0,
};
static const EncodedMethodIndex DateTime_t51_VTable[24] = 
{
	3020,
	125,
	3021,
	3022,
	3023,
	3024,
	3025,
	3026,
	3027,
	3028,
	3029,
	3030,
	3031,
	3032,
	3033,
	3034,
	3035,
	3036,
	3037,
	3038,
	3039,
	3040,
	3041,
	3042,
};
extern const Il2CppType IComparable_1_t2376_0_0_0;
extern const Il2CppType IEquatable_1_t2377_0_0_0;
static const Il2CppType* DateTime_t51_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IConvertible_t1409_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2376_0_0_0,
	&IEquatable_1_t2377_0_0_0,
};
static Il2CppInterfaceOffsetPair DateTime_t51_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
	{ &IComparable_1_t2376_0_0_0, 22},
	{ &IEquatable_1_t2377_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTime_t51_0_0_0;
extern const Il2CppType DateTime_t51_1_0_0;
extern const Il2CppType ValueType_t757_0_0_0;
const Il2CppTypeDefinitionMetadata DateTime_t51_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DateTime_t51_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, DateTime_t51_InterfacesTypeInfos/* implementedInterfaces */
	, DateTime_t51_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, DateTime_t51_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5212/* fieldStart */
	, 8340/* methodStart */
	, -1/* eventStart */
	, 1604/* propertyStart */

};
TypeInfo DateTime_t51_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTime"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DateTime_t51_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DateTime_t51_0_0_0/* byval_arg */
	, &DateTime_t51_1_0_0/* this_arg */
	, &DateTime_t51_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTime_t51)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DateTime_t51)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DateTime_t51_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 82/* method_count */
	, 12/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.DateTime/Which
#include "mscorlib_System_DateTime_Which.h"
// Metadata Definition System.DateTime/Which
extern TypeInfo Which_t1303_il2cpp_TypeInfo;
// System.DateTime/Which
#include "mscorlib_System_DateTime_WhichMethodDeclarations.h"
static const EncodedMethodIndex Which_t1303_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair Which_t1303_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Which_t1303_1_0_0;
const Il2CppTypeDefinitionMetadata Which_t1303_DefinitionMetadata = 
{
	&DateTime_t51_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Which_t1303_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, Which_t1303_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5227/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Which_t1303_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Which"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Which_t1303_0_0_0/* byval_arg */
	, &Which_t1303_1_0_0/* this_arg */
	, &Which_t1303_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Which_t1303)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Which_t1303)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
// Metadata Definition System.DateTimeKind
extern TypeInfo DateTimeKind_t1304_il2cpp_TypeInfo;
// System.DateTimeKind
#include "mscorlib_System_DateTimeKindMethodDeclarations.h"
static const EncodedMethodIndex DateTimeKind_t1304_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair DateTimeKind_t1304_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTimeKind_t1304_0_0_0;
extern const Il2CppType DateTimeKind_t1304_1_0_0;
const Il2CppTypeDefinitionMetadata DateTimeKind_t1304_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DateTimeKind_t1304_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, DateTimeKind_t1304_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5232/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DateTimeKind_t1304_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTimeKind"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1468/* custom_attributes_cache */
	, &DateTimeKind_t1304_0_0_0/* byval_arg */
	, &DateTimeKind_t1304_1_0_0/* this_arg */
	, &DateTimeKind_t1304_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTimeKind_t1304)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DateTimeKind_t1304)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"
// Metadata Definition System.DateTimeOffset
extern TypeInfo DateTimeOffset_t363_il2cpp_TypeInfo;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffsetMethodDeclarations.h"
static const EncodedMethodIndex DateTimeOffset_t363_VTable[10] = 
{
	3043,
	125,
	3044,
	3045,
	3046,
	3047,
	3048,
	3049,
	3050,
	3051,
};
extern const Il2CppType IComparable_1_t2378_0_0_0;
extern const Il2CppType IEquatable_1_t2379_0_0_0;
extern const Il2CppType IDeserializationCallback_t1429_0_0_0;
static const Il2CppType* DateTimeOffset_t363_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IComparable_t1408_0_0_0,
	&ISerializable_t1426_0_0_0,
	&IComparable_1_t2378_0_0_0,
	&IEquatable_1_t2379_0_0_0,
	&IDeserializationCallback_t1429_0_0_0,
};
static Il2CppInterfaceOffsetPair DateTimeOffset_t363_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IComparable_t1408_0_0_0, 5},
	{ &ISerializable_t1426_0_0_0, 6},
	{ &IComparable_1_t2378_0_0_0, 7},
	{ &IEquatable_1_t2379_0_0_0, 8},
	{ &IDeserializationCallback_t1429_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTimeOffset_t363_0_0_0;
extern const Il2CppType DateTimeOffset_t363_1_0_0;
const Il2CppTypeDefinitionMetadata DateTimeOffset_t363_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DateTimeOffset_t363_InterfacesTypeInfos/* implementedInterfaces */
	, DateTimeOffset_t363_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, DateTimeOffset_t363_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5236/* fieldStart */
	, 8422/* methodStart */
	, -1/* eventStart */
	, 1616/* propertyStart */

};
TypeInfo DateTimeOffset_t363_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTimeOffset"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DateTimeOffset_t363_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DateTimeOffset_t363_0_0_0/* byval_arg */
	, &DateTimeOffset_t363_1_0_0/* this_arg */
	, &DateTimeOffset_t363_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTimeOffset_t363)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DateTimeOffset_t363)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DateTimeOffset_t363_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.DateTimeUtils
#include "mscorlib_System_DateTimeUtils.h"
// Metadata Definition System.DateTimeUtils
extern TypeInfo DateTimeUtils_t1305_il2cpp_TypeInfo;
// System.DateTimeUtils
#include "mscorlib_System_DateTimeUtilsMethodDeclarations.h"
static const EncodedMethodIndex DateTimeUtils_t1305_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTimeUtils_t1305_0_0_0;
extern const Il2CppType DateTimeUtils_t1305_1_0_0;
struct DateTimeUtils_t1305;
const Il2CppTypeDefinitionMetadata DateTimeUtils_t1305_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DateTimeUtils_t1305_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8440/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DateTimeUtils_t1305_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTimeUtils"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DateTimeUtils_t1305_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DateTimeUtils_t1305_0_0_0/* byval_arg */
	, &DateTimeUtils_t1305_1_0_0/* this_arg */
	, &DateTimeUtils_t1305_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTimeUtils_t1305)/* instance_size */
	, sizeof (DateTimeUtils_t1305)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"
// Metadata Definition System.DayOfWeek
extern TypeInfo DayOfWeek_t1306_il2cpp_TypeInfo;
// System.DayOfWeek
#include "mscorlib_System_DayOfWeekMethodDeclarations.h"
static const EncodedMethodIndex DayOfWeek_t1306_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair DayOfWeek_t1306_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DayOfWeek_t1306_0_0_0;
extern const Il2CppType DayOfWeek_t1306_1_0_0;
const Il2CppTypeDefinitionMetadata DayOfWeek_t1306_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DayOfWeek_t1306_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, DayOfWeek_t1306_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5240/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DayOfWeek_t1306_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DayOfWeek"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1470/* custom_attributes_cache */
	, &DayOfWeek_t1306_0_0_0/* byval_arg */
	, &DayOfWeek_t1306_1_0_0/* this_arg */
	, &DayOfWeek_t1306_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DayOfWeek_t1306)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DayOfWeek_t1306)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.DelegateData
#include "mscorlib_System_DelegateData.h"
// Metadata Definition System.DelegateData
extern TypeInfo DelegateData_t762_il2cpp_TypeInfo;
// System.DelegateData
#include "mscorlib_System_DelegateDataMethodDeclarations.h"
static const EncodedMethodIndex DelegateData_t762_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DelegateData_t762_0_0_0;
extern const Il2CppType DelegateData_t762_1_0_0;
struct DelegateData_t762;
const Il2CppTypeDefinitionMetadata DelegateData_t762_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DelegateData_t762_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5248/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DelegateData_t762_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DelegateData"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DelegateData_t762_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DelegateData_t762_0_0_0/* byval_arg */
	, &DelegateData_t762_1_0_0/* this_arg */
	, &DelegateData_t762_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DelegateData_t762)/* instance_size */
	, sizeof (DelegateData_t762)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DelegateSerializationHolder
#include "mscorlib_System_DelegateSerializationHolder.h"
// Metadata Definition System.DelegateSerializationHolder
extern TypeInfo DelegateSerializationHolder_t1308_il2cpp_TypeInfo;
// System.DelegateSerializationHolder
#include "mscorlib_System_DelegateSerializationHolderMethodDeclarations.h"
extern const Il2CppType DelegateEntry_t1307_0_0_0;
static const Il2CppType* DelegateSerializationHolder_t1308_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DelegateEntry_t1307_0_0_0,
};
static const EncodedMethodIndex DelegateSerializationHolder_t1308_VTable[6] = 
{
	120,
	125,
	122,
	123,
	3052,
	3053,
};
extern const Il2CppType IObjectReference_t1427_0_0_0;
static const Il2CppType* DelegateSerializationHolder_t1308_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
	&IObjectReference_t1427_0_0_0,
};
static Il2CppInterfaceOffsetPair DelegateSerializationHolder_t1308_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &IObjectReference_t1427_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DelegateSerializationHolder_t1308_0_0_0;
extern const Il2CppType DelegateSerializationHolder_t1308_1_0_0;
struct DelegateSerializationHolder_t1308;
const Il2CppTypeDefinitionMetadata DelegateSerializationHolder_t1308_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DelegateSerializationHolder_t1308_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, DelegateSerializationHolder_t1308_InterfacesTypeInfos/* implementedInterfaces */
	, DelegateSerializationHolder_t1308_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DelegateSerializationHolder_t1308_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5250/* fieldStart */
	, 8447/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DelegateSerializationHolder_t1308_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DelegateSerializationHolder"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DelegateSerializationHolder_t1308_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DelegateSerializationHolder_t1308_0_0_0/* byval_arg */
	, &DelegateSerializationHolder_t1308_1_0_0/* this_arg */
	, &DelegateSerializationHolder_t1308_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DelegateSerializationHolder_t1308)/* instance_size */
	, sizeof (DelegateSerializationHolder_t1308)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.DelegateSerializationHolder/DelegateEntry
#include "mscorlib_System_DelegateSerializationHolder_DelegateEntry.h"
// Metadata Definition System.DelegateSerializationHolder/DelegateEntry
extern TypeInfo DelegateEntry_t1307_il2cpp_TypeInfo;
// System.DelegateSerializationHolder/DelegateEntry
#include "mscorlib_System_DelegateSerializationHolder_DelegateEntryMethodDeclarations.h"
static const EncodedMethodIndex DelegateEntry_t1307_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DelegateEntry_t1307_1_0_0;
struct DelegateEntry_t1307;
const Il2CppTypeDefinitionMetadata DelegateEntry_t1307_DefinitionMetadata = 
{
	&DelegateSerializationHolder_t1308_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DelegateEntry_t1307_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5251/* fieldStart */
	, 8451/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DelegateEntry_t1307_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DelegateEntry"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DelegateEntry_t1307_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DelegateEntry_t1307_0_0_0/* byval_arg */
	, &DelegateEntry_t1307_1_0_0/* this_arg */
	, &DelegateEntry_t1307_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DelegateEntry_t1307)/* instance_size */
	, sizeof (DelegateEntry_t1307)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DivideByZeroException
#include "mscorlib_System_DivideByZeroException.h"
// Metadata Definition System.DivideByZeroException
extern TypeInfo DivideByZeroException_t1309_il2cpp_TypeInfo;
// System.DivideByZeroException
#include "mscorlib_System_DivideByZeroExceptionMethodDeclarations.h"
static const EncodedMethodIndex DivideByZeroException_t1309_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair DivideByZeroException_t1309_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DivideByZeroException_t1309_0_0_0;
extern const Il2CppType DivideByZeroException_t1309_1_0_0;
struct DivideByZeroException_t1309;
const Il2CppTypeDefinitionMetadata DivideByZeroException_t1309_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DivideByZeroException_t1309_InterfacesOffsets/* interfaceOffsets */
	, &ArithmeticException_t739_0_0_0/* parent */
	, DivideByZeroException_t1309_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8453/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DivideByZeroException_t1309_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DivideByZeroException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DivideByZeroException_t1309_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1471/* custom_attributes_cache */
	, &DivideByZeroException_t1309_0_0_0/* byval_arg */
	, &DivideByZeroException_t1309_1_0_0/* this_arg */
	, &DivideByZeroException_t1309_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DivideByZeroException_t1309)/* instance_size */
	, sizeof (DivideByZeroException_t1309)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.DllNotFoundException
#include "mscorlib_System_DllNotFoundException.h"
// Metadata Definition System.DllNotFoundException
extern TypeInfo DllNotFoundException_t1310_il2cpp_TypeInfo;
// System.DllNotFoundException
#include "mscorlib_System_DllNotFoundExceptionMethodDeclarations.h"
static const EncodedMethodIndex DllNotFoundException_t1310_VTable[11] = 
{
	120,
	125,
	122,
	203,
	3054,
	205,
	3055,
	207,
	208,
	3054,
	209,
};
static Il2CppInterfaceOffsetPair DllNotFoundException_t1310_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DllNotFoundException_t1310_0_0_0;
extern const Il2CppType DllNotFoundException_t1310_1_0_0;
extern const Il2CppType TypeLoadException_t1311_0_0_0;
struct DllNotFoundException_t1310;
const Il2CppTypeDefinitionMetadata DllNotFoundException_t1310_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DllNotFoundException_t1310_InterfacesOffsets/* interfaceOffsets */
	, &TypeLoadException_t1311_0_0_0/* parent */
	, DllNotFoundException_t1310_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5258/* fieldStart */
	, 8455/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DllNotFoundException_t1310_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DllNotFoundException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DllNotFoundException_t1310_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1472/* custom_attributes_cache */
	, &DllNotFoundException_t1310_0_0_0/* byval_arg */
	, &DllNotFoundException_t1310_1_0_0/* this_arg */
	, &DllNotFoundException_t1310_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DllNotFoundException_t1310)/* instance_size */
	, sizeof (DllNotFoundException_t1310)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.EntryPointNotFoundException
#include "mscorlib_System_EntryPointNotFoundException.h"
// Metadata Definition System.EntryPointNotFoundException
extern TypeInfo EntryPointNotFoundException_t1312_il2cpp_TypeInfo;
// System.EntryPointNotFoundException
#include "mscorlib_System_EntryPointNotFoundExceptionMethodDeclarations.h"
static const EncodedMethodIndex EntryPointNotFoundException_t1312_VTable[11] = 
{
	120,
	125,
	122,
	203,
	3054,
	205,
	3055,
	207,
	208,
	3054,
	209,
};
static Il2CppInterfaceOffsetPair EntryPointNotFoundException_t1312_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EntryPointNotFoundException_t1312_0_0_0;
extern const Il2CppType EntryPointNotFoundException_t1312_1_0_0;
struct EntryPointNotFoundException_t1312;
const Il2CppTypeDefinitionMetadata EntryPointNotFoundException_t1312_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EntryPointNotFoundException_t1312_InterfacesOffsets/* interfaceOffsets */
	, &TypeLoadException_t1311_0_0_0/* parent */
	, EntryPointNotFoundException_t1312_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5259/* fieldStart */
	, 8457/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EntryPointNotFoundException_t1312_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EntryPointNotFoundException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &EntryPointNotFoundException_t1312_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1473/* custom_attributes_cache */
	, &EntryPointNotFoundException_t1312_0_0_0/* byval_arg */
	, &EntryPointNotFoundException_t1312_1_0_0/* this_arg */
	, &EntryPointNotFoundException_t1312_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EntryPointNotFoundException_t1312)/* instance_size */
	, sizeof (EntryPointNotFoundException_t1312)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoEnumInfo
#include "mscorlib_System_MonoEnumInfo.h"
// Metadata Definition System.MonoEnumInfo
extern TypeInfo MonoEnumInfo_t1317_il2cpp_TypeInfo;
// System.MonoEnumInfo
#include "mscorlib_System_MonoEnumInfoMethodDeclarations.h"
extern const Il2CppType SByteComparer_t1313_0_0_0;
extern const Il2CppType ShortComparer_t1314_0_0_0;
extern const Il2CppType IntComparer_t1315_0_0_0;
extern const Il2CppType LongComparer_t1316_0_0_0;
static const Il2CppType* MonoEnumInfo_t1317_il2cpp_TypeInfo__nestedTypes[4] =
{
	&SByteComparer_t1313_0_0_0,
	&ShortComparer_t1314_0_0_0,
	&IntComparer_t1315_0_0_0,
	&LongComparer_t1316_0_0_0,
};
static const EncodedMethodIndex MonoEnumInfo_t1317_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoEnumInfo_t1317_0_0_0;
extern const Il2CppType MonoEnumInfo_t1317_1_0_0;
const Il2CppTypeDefinitionMetadata MonoEnumInfo_t1317_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MonoEnumInfo_t1317_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, MonoEnumInfo_t1317_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5260/* fieldStart */
	, 8459/* methodStart */
	, -1/* eventStart */
	, 1619/* propertyStart */

};
TypeInfo MonoEnumInfo_t1317_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoEnumInfo"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoEnumInfo_t1317_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoEnumInfo_t1317_0_0_0/* byval_arg */
	, &MonoEnumInfo_t1317_1_0_0/* this_arg */
	, &MonoEnumInfo_t1317_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoEnumInfo_t1317)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoEnumInfo_t1317)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoEnumInfo_t1317_StaticFields)/* static_fields_size */
	, sizeof(MonoEnumInfo_t1317_ThreadStaticFields)/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 264/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 4/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoEnumInfo/SByteComparer
#include "mscorlib_System_MonoEnumInfo_SByteComparer.h"
// Metadata Definition System.MonoEnumInfo/SByteComparer
extern TypeInfo SByteComparer_t1313_il2cpp_TypeInfo;
// System.MonoEnumInfo/SByteComparer
#include "mscorlib_System_MonoEnumInfo_SByteComparerMethodDeclarations.h"
static const EncodedMethodIndex SByteComparer_t1313_VTable[6] = 
{
	120,
	125,
	122,
	123,
	3056,
	3057,
};
extern const Il2CppType IComparer_1_t2380_0_0_0;
static const Il2CppType* SByteComparer_t1313_InterfacesTypeInfos[] = 
{
	&IComparer_1_t2380_0_0_0,
	&IComparer_t390_0_0_0,
};
static Il2CppInterfaceOffsetPair SByteComparer_t1313_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2380_0_0_0, 4},
	{ &IComparer_t390_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SByteComparer_t1313_1_0_0;
struct SByteComparer_t1313;
const Il2CppTypeDefinitionMetadata SByteComparer_t1313_DefinitionMetadata = 
{
	&MonoEnumInfo_t1317_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, SByteComparer_t1313_InterfacesTypeInfos/* implementedInterfaces */
	, SByteComparer_t1313_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SByteComparer_t1313_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8464/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SByteComparer_t1313_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SByteComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SByteComparer_t1313_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SByteComparer_t1313_0_0_0/* byval_arg */
	, &SByteComparer_t1313_1_0_0/* this_arg */
	, &SByteComparer_t1313_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SByteComparer_t1313)/* instance_size */
	, sizeof (SByteComparer_t1313)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoEnumInfo/ShortComparer
#include "mscorlib_System_MonoEnumInfo_ShortComparer.h"
// Metadata Definition System.MonoEnumInfo/ShortComparer
extern TypeInfo ShortComparer_t1314_il2cpp_TypeInfo;
// System.MonoEnumInfo/ShortComparer
#include "mscorlib_System_MonoEnumInfo_ShortComparerMethodDeclarations.h"
static const EncodedMethodIndex ShortComparer_t1314_VTable[6] = 
{
	120,
	125,
	122,
	123,
	3058,
	3059,
};
extern const Il2CppType IComparer_1_t2381_0_0_0;
static const Il2CppType* ShortComparer_t1314_InterfacesTypeInfos[] = 
{
	&IComparer_1_t2381_0_0_0,
	&IComparer_t390_0_0_0,
};
static Il2CppInterfaceOffsetPair ShortComparer_t1314_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2381_0_0_0, 4},
	{ &IComparer_t390_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ShortComparer_t1314_1_0_0;
struct ShortComparer_t1314;
const Il2CppTypeDefinitionMetadata ShortComparer_t1314_DefinitionMetadata = 
{
	&MonoEnumInfo_t1317_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, ShortComparer_t1314_InterfacesTypeInfos/* implementedInterfaces */
	, ShortComparer_t1314_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ShortComparer_t1314_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8467/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ShortComparer_t1314_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ShortComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ShortComparer_t1314_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ShortComparer_t1314_0_0_0/* byval_arg */
	, &ShortComparer_t1314_1_0_0/* this_arg */
	, &ShortComparer_t1314_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ShortComparer_t1314)/* instance_size */
	, sizeof (ShortComparer_t1314)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoEnumInfo/IntComparer
#include "mscorlib_System_MonoEnumInfo_IntComparer.h"
// Metadata Definition System.MonoEnumInfo/IntComparer
extern TypeInfo IntComparer_t1315_il2cpp_TypeInfo;
// System.MonoEnumInfo/IntComparer
#include "mscorlib_System_MonoEnumInfo_IntComparerMethodDeclarations.h"
static const EncodedMethodIndex IntComparer_t1315_VTable[6] = 
{
	120,
	125,
	122,
	123,
	3060,
	3061,
};
extern const Il2CppType IComparer_1_t2015_0_0_0;
static const Il2CppType* IntComparer_t1315_InterfacesTypeInfos[] = 
{
	&IComparer_1_t2015_0_0_0,
	&IComparer_t390_0_0_0,
};
static Il2CppInterfaceOffsetPair IntComparer_t1315_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2015_0_0_0, 4},
	{ &IComparer_t390_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IntComparer_t1315_1_0_0;
struct IntComparer_t1315;
const Il2CppTypeDefinitionMetadata IntComparer_t1315_DefinitionMetadata = 
{
	&MonoEnumInfo_t1317_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, IntComparer_t1315_InterfacesTypeInfos/* implementedInterfaces */
	, IntComparer_t1315_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IntComparer_t1315_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8470/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IntComparer_t1315_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &IntComparer_t1315_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IntComparer_t1315_0_0_0/* byval_arg */
	, &IntComparer_t1315_1_0_0/* this_arg */
	, &IntComparer_t1315_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IntComparer_t1315)/* instance_size */
	, sizeof (IntComparer_t1315)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoEnumInfo/LongComparer
#include "mscorlib_System_MonoEnumInfo_LongComparer.h"
// Metadata Definition System.MonoEnumInfo/LongComparer
extern TypeInfo LongComparer_t1316_il2cpp_TypeInfo;
// System.MonoEnumInfo/LongComparer
#include "mscorlib_System_MonoEnumInfo_LongComparerMethodDeclarations.h"
static const EncodedMethodIndex LongComparer_t1316_VTable[6] = 
{
	120,
	125,
	122,
	123,
	3062,
	3063,
};
extern const Il2CppType IComparer_1_t2382_0_0_0;
static const Il2CppType* LongComparer_t1316_InterfacesTypeInfos[] = 
{
	&IComparer_1_t2382_0_0_0,
	&IComparer_t390_0_0_0,
};
static Il2CppInterfaceOffsetPair LongComparer_t1316_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2382_0_0_0, 4},
	{ &IComparer_t390_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LongComparer_t1316_1_0_0;
struct LongComparer_t1316;
const Il2CppTypeDefinitionMetadata LongComparer_t1316_DefinitionMetadata = 
{
	&MonoEnumInfo_t1317_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, LongComparer_t1316_InterfacesTypeInfos/* implementedInterfaces */
	, LongComparer_t1316_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LongComparer_t1316_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8473/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LongComparer_t1316_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LongComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &LongComparer_t1316_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LongComparer_t1316_0_0_0/* byval_arg */
	, &LongComparer_t1316_1_0_0/* this_arg */
	, &LongComparer_t1316_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LongComparer_t1316)/* instance_size */
	, sizeof (LongComparer_t1316)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Environment
#include "mscorlib_System_Environment.h"
// Metadata Definition System.Environment
extern TypeInfo Environment_t1320_il2cpp_TypeInfo;
// System.Environment
#include "mscorlib_System_EnvironmentMethodDeclarations.h"
extern const Il2CppType SpecialFolder_t1318_0_0_0;
static const Il2CppType* Environment_t1320_il2cpp_TypeInfo__nestedTypes[1] =
{
	&SpecialFolder_t1318_0_0_0,
};
static const EncodedMethodIndex Environment_t1320_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Environment_t1320_0_0_0;
extern const Il2CppType Environment_t1320_1_0_0;
struct Environment_t1320;
const Il2CppTypeDefinitionMetadata Environment_t1320_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Environment_t1320_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Environment_t1320_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5271/* fieldStart */
	, 8476/* methodStart */
	, -1/* eventStart */
	, 1620/* propertyStart */

};
TypeInfo Environment_t1320_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Environment"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Environment_t1320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1475/* custom_attributes_cache */
	, &Environment_t1320_0_0_0/* byval_arg */
	, &Environment_t1320_1_0_0/* this_arg */
	, &Environment_t1320_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Environment_t1320)/* instance_size */
	, sizeof (Environment_t1320)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Environment_t1320_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Environment/SpecialFolder
#include "mscorlib_System_Environment_SpecialFolder.h"
// Metadata Definition System.Environment/SpecialFolder
extern TypeInfo SpecialFolder_t1318_il2cpp_TypeInfo;
// System.Environment/SpecialFolder
#include "mscorlib_System_Environment_SpecialFolderMethodDeclarations.h"
static const EncodedMethodIndex SpecialFolder_t1318_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair SpecialFolder_t1318_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SpecialFolder_t1318_1_0_0;
const Il2CppTypeDefinitionMetadata SpecialFolder_t1318_DefinitionMetadata = 
{
	&Environment_t1320_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SpecialFolder_t1318_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, SpecialFolder_t1318_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5272/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SpecialFolder_t1318_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpecialFolder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1476/* custom_attributes_cache */
	, &SpecialFolder_t1318_0_0_0/* byval_arg */
	, &SpecialFolder_t1318_1_0_0/* this_arg */
	, &SpecialFolder_t1318_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpecialFolder_t1318)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SpecialFolder_t1318)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.EventArgs
#include "mscorlib_System_EventArgs.h"
// Metadata Definition System.EventArgs
extern TypeInfo EventArgs_t648_il2cpp_TypeInfo;
// System.EventArgs
#include "mscorlib_System_EventArgsMethodDeclarations.h"
static const EncodedMethodIndex EventArgs_t648_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventArgs_t648_1_0_0;
struct EventArgs_t648;
const Il2CppTypeDefinitionMetadata EventArgs_t648_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EventArgs_t648_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5296/* fieldStart */
	, 8491/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventArgs_t648_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventArgs"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &EventArgs_t648_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1477/* custom_attributes_cache */
	, &EventArgs_t648_0_0_0/* byval_arg */
	, &EventArgs_t648_1_0_0/* this_arg */
	, &EventArgs_t648_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventArgs_t648)/* instance_size */
	, sizeof (EventArgs_t648)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EventArgs_t648_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ExecutionEngineException
#include "mscorlib_System_ExecutionEngineException.h"
// Metadata Definition System.ExecutionEngineException
extern TypeInfo ExecutionEngineException_t1321_il2cpp_TypeInfo;
// System.ExecutionEngineException
#include "mscorlib_System_ExecutionEngineExceptionMethodDeclarations.h"
static const EncodedMethodIndex ExecutionEngineException_t1321_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair ExecutionEngineException_t1321_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ExecutionEngineException_t1321_0_0_0;
extern const Il2CppType ExecutionEngineException_t1321_1_0_0;
struct ExecutionEngineException_t1321;
const Il2CppTypeDefinitionMetadata ExecutionEngineException_t1321_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExecutionEngineException_t1321_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, ExecutionEngineException_t1321_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8493/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExecutionEngineException_t1321_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecutionEngineException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ExecutionEngineException_t1321_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1478/* custom_attributes_cache */
	, &ExecutionEngineException_t1321_0_0_0/* byval_arg */
	, &ExecutionEngineException_t1321_1_0_0/* this_arg */
	, &ExecutionEngineException_t1321_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecutionEngineException_t1321)/* instance_size */
	, sizeof (ExecutionEngineException_t1321)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.FieldAccessException
#include "mscorlib_System_FieldAccessException.h"
// Metadata Definition System.FieldAccessException
extern TypeInfo FieldAccessException_t1322_il2cpp_TypeInfo;
// System.FieldAccessException
#include "mscorlib_System_FieldAccessExceptionMethodDeclarations.h"
static const EncodedMethodIndex FieldAccessException_t1322_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair FieldAccessException_t1322_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldAccessException_t1322_0_0_0;
extern const Il2CppType FieldAccessException_t1322_1_0_0;
extern const Il2CppType MemberAccessException_t1323_0_0_0;
struct FieldAccessException_t1322;
const Il2CppTypeDefinitionMetadata FieldAccessException_t1322_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FieldAccessException_t1322_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t1323_0_0_0/* parent */
	, FieldAccessException_t1322_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8495/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FieldAccessException_t1322_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldAccessException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &FieldAccessException_t1322_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1479/* custom_attributes_cache */
	, &FieldAccessException_t1322_0_0_0/* byval_arg */
	, &FieldAccessException_t1322_1_0_0/* this_arg */
	, &FieldAccessException_t1322_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldAccessException_t1322)/* instance_size */
	, sizeof (FieldAccessException_t1322)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// Metadata Definition System.FlagsAttribute
extern TypeInfo FlagsAttribute_t1324_il2cpp_TypeInfo;
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
static const EncodedMethodIndex FlagsAttribute_t1324_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
extern const Il2CppType _Attribute_t2052_0_0_0;
static Il2CppInterfaceOffsetPair FlagsAttribute_t1324_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FlagsAttribute_t1324_0_0_0;
extern const Il2CppType FlagsAttribute_t1324_1_0_0;
extern const Il2CppType Attribute_t96_0_0_0;
struct FlagsAttribute_t1324;
const Il2CppTypeDefinitionMetadata FlagsAttribute_t1324_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FlagsAttribute_t1324_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, FlagsAttribute_t1324_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8498/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FlagsAttribute_t1324_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FlagsAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &FlagsAttribute_t1324_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1480/* custom_attributes_cache */
	, &FlagsAttribute_t1324_0_0_0/* byval_arg */
	, &FlagsAttribute_t1324_1_0_0/* this_arg */
	, &FlagsAttribute_t1324_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FlagsAttribute_t1324)/* instance_size */
	, sizeof (FlagsAttribute_t1324)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.FormatException
#include "mscorlib_System_FormatException.h"
// Metadata Definition System.FormatException
extern TypeInfo FormatException_t334_il2cpp_TypeInfo;
// System.FormatException
#include "mscorlib_System_FormatExceptionMethodDeclarations.h"
static const EncodedMethodIndex FormatException_t334_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair FormatException_t334_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatException_t334_0_0_0;
extern const Il2CppType FormatException_t334_1_0_0;
struct FormatException_t334;
const Il2CppTypeDefinitionMetadata FormatException_t334_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatException_t334_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, FormatException_t334_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5297/* fieldStart */
	, 8499/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormatException_t334_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &FormatException_t334_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1481/* custom_attributes_cache */
	, &FormatException_t334_0_0_0/* byval_arg */
	, &FormatException_t334_1_0_0/* this_arg */
	, &FormatException_t334_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatException_t334)/* instance_size */
	, sizeof (FormatException_t334)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.GC
#include "mscorlib_System_GC.h"
// Metadata Definition System.GC
extern TypeInfo GC_t1325_il2cpp_TypeInfo;
// System.GC
#include "mscorlib_System_GCMethodDeclarations.h"
static const EncodedMethodIndex GC_t1325_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GC_t1325_0_0_0;
extern const Il2CppType GC_t1325_1_0_0;
struct GC_t1325;
const Il2CppTypeDefinitionMetadata GC_t1325_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GC_t1325_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8502/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GC_t1325_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GC"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &GC_t1325_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GC_t1325_0_0_0/* byval_arg */
	, &GC_t1325_1_0_0/* this_arg */
	, &GC_t1325_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GC_t1325)/* instance_size */
	, sizeof (GC_t1325)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Guid
#include "mscorlib_System_Guid.h"
// Metadata Definition System.Guid
extern TypeInfo Guid_t364_il2cpp_TypeInfo;
// System.Guid
#include "mscorlib_System_GuidMethodDeclarations.h"
static const EncodedMethodIndex Guid_t364_VTable[8] = 
{
	3064,
	125,
	3065,
	3066,
	3067,
	3068,
	3069,
	3070,
};
extern const Il2CppType IComparable_1_t2383_0_0_0;
extern const Il2CppType IEquatable_1_t2384_0_0_0;
static const Il2CppType* Guid_t364_InterfacesTypeInfos[] = 
{
	&IFormattable_t1406_0_0_0,
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2383_0_0_0,
	&IEquatable_1_t2384_0_0_0,
};
static Il2CppInterfaceOffsetPair Guid_t364_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IComparable_t1408_0_0_0, 5},
	{ &IComparable_1_t2383_0_0_0, 6},
	{ &IEquatable_1_t2384_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Guid_t364_0_0_0;
extern const Il2CppType Guid_t364_1_0_0;
const Il2CppTypeDefinitionMetadata Guid_t364_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Guid_t364_InterfacesTypeInfos/* implementedInterfaces */
	, Guid_t364_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Guid_t364_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5298/* fieldStart */
	, 8503/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Guid_t364_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Guid"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Guid_t364_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1483/* custom_attributes_cache */
	, &Guid_t364_0_0_0/* byval_arg */
	, &Guid_t364_1_0_0/* this_arg */
	, &Guid_t364_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Guid_t364)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Guid_t364)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Guid_t364 )/* native_size */
	, sizeof(Guid_t364_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.ICustomFormatter
extern TypeInfo ICustomFormatter_t1405_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICustomFormatter_t1405_0_0_0;
extern const Il2CppType ICustomFormatter_t1405_1_0_0;
struct ICustomFormatter_t1405;
const Il2CppTypeDefinitionMetadata ICustomFormatter_t1405_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8524/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICustomFormatter_t1405_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICustomFormatter"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ICustomFormatter_t1405_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1484/* custom_attributes_cache */
	, &ICustomFormatter_t1405_0_0_0/* byval_arg */
	, &ICustomFormatter_t1405_1_0_0/* this_arg */
	, &ICustomFormatter_t1405_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IFormatProvider
extern TypeInfo IFormatProvider_t1388_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatProvider_t1388_0_0_0;
extern const Il2CppType IFormatProvider_t1388_1_0_0;
struct IFormatProvider_t1388;
const Il2CppTypeDefinitionMetadata IFormatProvider_t1388_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8525/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IFormatProvider_t1388_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatProvider"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IFormatProvider_t1388_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1485/* custom_attributes_cache */
	, &IFormatProvider_t1388_0_0_0/* byval_arg */
	, &IFormatProvider_t1388_1_0_0/* this_arg */
	, &IFormatProvider_t1388_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeException.h"
// Metadata Definition System.IndexOutOfRangeException
extern TypeInfo IndexOutOfRangeException_t327_il2cpp_TypeInfo;
// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeExceptionMethodDeclarations.h"
static const EncodedMethodIndex IndexOutOfRangeException_t327_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair IndexOutOfRangeException_t327_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IndexOutOfRangeException_t327_0_0_0;
extern const Il2CppType IndexOutOfRangeException_t327_1_0_0;
struct IndexOutOfRangeException_t327;
const Il2CppTypeDefinitionMetadata IndexOutOfRangeException_t327_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IndexOutOfRangeException_t327_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, IndexOutOfRangeException_t327_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8526/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IndexOutOfRangeException_t327_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IndexOutOfRangeException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IndexOutOfRangeException_t327_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1486/* custom_attributes_cache */
	, &IndexOutOfRangeException_t327_0_0_0/* byval_arg */
	, &IndexOutOfRangeException_t327_1_0_0/* this_arg */
	, &IndexOutOfRangeException_t327_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IndexOutOfRangeException_t327)/* instance_size */
	, sizeof (IndexOutOfRangeException_t327)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.InvalidCastException
#include "mscorlib_System_InvalidCastException.h"
// Metadata Definition System.InvalidCastException
extern TypeInfo InvalidCastException_t1326_il2cpp_TypeInfo;
// System.InvalidCastException
#include "mscorlib_System_InvalidCastExceptionMethodDeclarations.h"
static const EncodedMethodIndex InvalidCastException_t1326_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair InvalidCastException_t1326_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InvalidCastException_t1326_0_0_0;
extern const Il2CppType InvalidCastException_t1326_1_0_0;
struct InvalidCastException_t1326;
const Il2CppTypeDefinitionMetadata InvalidCastException_t1326_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InvalidCastException_t1326_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, InvalidCastException_t1326_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5312/* fieldStart */
	, 8529/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InvalidCastException_t1326_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvalidCastException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &InvalidCastException_t1326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1487/* custom_attributes_cache */
	, &InvalidCastException_t1326_0_0_0/* byval_arg */
	, &InvalidCastException_t1326_1_0_0/* this_arg */
	, &InvalidCastException_t1326_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvalidCastException_t1326)/* instance_size */
	, sizeof (InvalidCastException_t1326)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// Metadata Definition System.InvalidOperationException
extern TypeInfo InvalidOperationException_t573_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
static const EncodedMethodIndex InvalidOperationException_t573_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair InvalidOperationException_t573_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InvalidOperationException_t573_0_0_0;
extern const Il2CppType InvalidOperationException_t573_1_0_0;
struct InvalidOperationException_t573;
const Il2CppTypeDefinitionMetadata InvalidOperationException_t573_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InvalidOperationException_t573_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, InvalidOperationException_t573_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5313/* fieldStart */
	, 8532/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InvalidOperationException_t573_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvalidOperationException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &InvalidOperationException_t573_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1488/* custom_attributes_cache */
	, &InvalidOperationException_t573_0_0_0/* byval_arg */
	, &InvalidOperationException_t573_1_0_0/* this_arg */
	, &InvalidOperationException_t573_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvalidOperationException_t573)/* instance_size */
	, sizeof (InvalidOperationException_t573)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimization.h"
// Metadata Definition System.LoaderOptimization
extern TypeInfo LoaderOptimization_t1327_il2cpp_TypeInfo;
// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimizationMethodDeclarations.h"
static const EncodedMethodIndex LoaderOptimization_t1327_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair LoaderOptimization_t1327_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LoaderOptimization_t1327_0_0_0;
extern const Il2CppType LoaderOptimization_t1327_1_0_0;
const Il2CppTypeDefinitionMetadata LoaderOptimization_t1327_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LoaderOptimization_t1327_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, LoaderOptimization_t1327_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5314/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LoaderOptimization_t1327_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LoaderOptimization"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1489/* custom_attributes_cache */
	, &LoaderOptimization_t1327_0_0_0/* byval_arg */
	, &LoaderOptimization_t1327_1_0_0/* this_arg */
	, &LoaderOptimization_t1327_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LoaderOptimization_t1327)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LoaderOptimization_t1327)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Math
#include "mscorlib_System_Math.h"
// Metadata Definition System.Math
extern TypeInfo Math_t1328_il2cpp_TypeInfo;
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
static const EncodedMethodIndex Math_t1328_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Math_t1328_0_0_0;
extern const Il2CppType Math_t1328_1_0_0;
struct Math_t1328;
const Il2CppTypeDefinitionMetadata Math_t1328_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Math_t1328_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8536/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Math_t1328_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Math"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Math_t1328_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Math_t1328_0_0_0/* byval_arg */
	, &Math_t1328_1_0_0/* this_arg */
	, &Math_t1328_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Math_t1328)/* instance_size */
	, sizeof (Math_t1328)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MemberAccessException
#include "mscorlib_System_MemberAccessException.h"
// Metadata Definition System.MemberAccessException
extern TypeInfo MemberAccessException_t1323_il2cpp_TypeInfo;
// System.MemberAccessException
#include "mscorlib_System_MemberAccessExceptionMethodDeclarations.h"
static const EncodedMethodIndex MemberAccessException_t1323_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair MemberAccessException_t1323_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberAccessException_t1323_1_0_0;
struct MemberAccessException_t1323;
const Il2CppTypeDefinitionMetadata MemberAccessException_t1323_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberAccessException_t1323_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, MemberAccessException_t1323_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8546/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MemberAccessException_t1323_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberAccessException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MemberAccessException_t1323_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1495/* custom_attributes_cache */
	, &MemberAccessException_t1323_0_0_0/* byval_arg */
	, &MemberAccessException_t1323_1_0_0/* this_arg */
	, &MemberAccessException_t1323_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberAccessException_t1323)/* instance_size */
	, sizeof (MemberAccessException_t1323)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MethodAccessException
#include "mscorlib_System_MethodAccessException.h"
// Metadata Definition System.MethodAccessException
extern TypeInfo MethodAccessException_t1329_il2cpp_TypeInfo;
// System.MethodAccessException
#include "mscorlib_System_MethodAccessExceptionMethodDeclarations.h"
static const EncodedMethodIndex MethodAccessException_t1329_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair MethodAccessException_t1329_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodAccessException_t1329_0_0_0;
extern const Il2CppType MethodAccessException_t1329_1_0_0;
struct MethodAccessException_t1329;
const Il2CppTypeDefinitionMetadata MethodAccessException_t1329_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodAccessException_t1329_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t1323_0_0_0/* parent */
	, MethodAccessException_t1329_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8549/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MethodAccessException_t1329_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodAccessException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MethodAccessException_t1329_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1496/* custom_attributes_cache */
	, &MethodAccessException_t1329_0_0_0/* byval_arg */
	, &MethodAccessException_t1329_1_0_0/* this_arg */
	, &MethodAccessException_t1329_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodAccessException_t1329)/* instance_size */
	, sizeof (MethodAccessException_t1329)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingFieldException
#include "mscorlib_System_MissingFieldException.h"
// Metadata Definition System.MissingFieldException
extern TypeInfo MissingFieldException_t1330_il2cpp_TypeInfo;
// System.MissingFieldException
#include "mscorlib_System_MissingFieldExceptionMethodDeclarations.h"
static const EncodedMethodIndex MissingFieldException_t1330_VTable[11] = 
{
	120,
	125,
	122,
	203,
	3071,
	205,
	3072,
	207,
	208,
	3071,
	209,
};
static Il2CppInterfaceOffsetPair MissingFieldException_t1330_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingFieldException_t1330_0_0_0;
extern const Il2CppType MissingFieldException_t1330_1_0_0;
extern const Il2CppType MissingMemberException_t1331_0_0_0;
struct MissingFieldException_t1330;
const Il2CppTypeDefinitionMetadata MissingFieldException_t1330_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingFieldException_t1330_InterfacesOffsets/* interfaceOffsets */
	, &MissingMemberException_t1331_0_0_0/* parent */
	, MissingFieldException_t1330_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8551/* methodStart */
	, -1/* eventStart */
	, 1626/* propertyStart */

};
TypeInfo MissingFieldException_t1330_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingFieldException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MissingFieldException_t1330_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1497/* custom_attributes_cache */
	, &MissingFieldException_t1330_0_0_0/* byval_arg */
	, &MissingFieldException_t1330_1_0_0/* this_arg */
	, &MissingFieldException_t1330_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingFieldException_t1330)/* instance_size */
	, sizeof (MissingFieldException_t1330)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingMemberException
#include "mscorlib_System_MissingMemberException.h"
// Metadata Definition System.MissingMemberException
extern TypeInfo MissingMemberException_t1331_il2cpp_TypeInfo;
// System.MissingMemberException
#include "mscorlib_System_MissingMemberExceptionMethodDeclarations.h"
static const EncodedMethodIndex MissingMemberException_t1331_VTable[11] = 
{
	120,
	125,
	122,
	203,
	3071,
	205,
	3073,
	207,
	208,
	3071,
	209,
};
static Il2CppInterfaceOffsetPair MissingMemberException_t1331_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingMemberException_t1331_1_0_0;
struct MissingMemberException_t1331;
const Il2CppTypeDefinitionMetadata MissingMemberException_t1331_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingMemberException_t1331_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t1323_0_0_0/* parent */
	, MissingMemberException_t1331_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5321/* fieldStart */
	, 8555/* methodStart */
	, -1/* eventStart */
	, 1627/* propertyStart */

};
TypeInfo MissingMemberException_t1331_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingMemberException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MissingMemberException_t1331_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1498/* custom_attributes_cache */
	, &MissingMemberException_t1331_0_0_0/* byval_arg */
	, &MissingMemberException_t1331_1_0_0/* this_arg */
	, &MissingMemberException_t1331_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingMemberException_t1331)/* instance_size */
	, sizeof (MissingMemberException_t1331)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingMethodException
#include "mscorlib_System_MissingMethodException.h"
// Metadata Definition System.MissingMethodException
extern TypeInfo MissingMethodException_t1332_il2cpp_TypeInfo;
// System.MissingMethodException
#include "mscorlib_System_MissingMethodExceptionMethodDeclarations.h"
static const EncodedMethodIndex MissingMethodException_t1332_VTable[11] = 
{
	120,
	125,
	122,
	203,
	3071,
	205,
	3074,
	207,
	208,
	3071,
	209,
};
static Il2CppInterfaceOffsetPair MissingMethodException_t1332_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingMethodException_t1332_0_0_0;
extern const Il2CppType MissingMethodException_t1332_1_0_0;
struct MissingMethodException_t1332;
const Il2CppTypeDefinitionMetadata MissingMethodException_t1332_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingMethodException_t1332_InterfacesOffsets/* interfaceOffsets */
	, &MissingMemberException_t1331_0_0_0/* parent */
	, MissingMethodException_t1332_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5324/* fieldStart */
	, 8561/* methodStart */
	, -1/* eventStart */
	, 1628/* propertyStart */

};
TypeInfo MissingMethodException_t1332_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingMethodException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MissingMethodException_t1332_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1499/* custom_attributes_cache */
	, &MissingMethodException_t1332_0_0_0/* byval_arg */
	, &MissingMethodException_t1332_1_0_0/* this_arg */
	, &MissingMethodException_t1332_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingMethodException_t1332)/* instance_size */
	, sizeof (MissingMethodException_t1332)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoAsyncCall
#include "mscorlib_System_MonoAsyncCall.h"
// Metadata Definition System.MonoAsyncCall
extern TypeInfo MonoAsyncCall_t1333_il2cpp_TypeInfo;
// System.MonoAsyncCall
#include "mscorlib_System_MonoAsyncCallMethodDeclarations.h"
static const EncodedMethodIndex MonoAsyncCall_t1333_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoAsyncCall_t1333_0_0_0;
extern const Il2CppType MonoAsyncCall_t1333_1_0_0;
struct MonoAsyncCall_t1333;
const Il2CppTypeDefinitionMetadata MonoAsyncCall_t1333_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoAsyncCall_t1333_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5325/* fieldStart */
	, 8566/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoAsyncCall_t1333_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoAsyncCall"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoAsyncCall_t1333_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoAsyncCall_t1333_0_0_0/* byval_arg */
	, &MonoAsyncCall_t1333_1_0_0/* this_arg */
	, &MonoAsyncCall_t1333_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoAsyncCall_t1333)/* instance_size */
	, sizeof (MonoAsyncCall_t1333)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoCustomAttrs
#include "mscorlib_System_MonoCustomAttrs.h"
// Metadata Definition System.MonoCustomAttrs
extern TypeInfo MonoCustomAttrs_t1335_il2cpp_TypeInfo;
// System.MonoCustomAttrs
#include "mscorlib_System_MonoCustomAttrsMethodDeclarations.h"
extern const Il2CppType AttributeInfo_t1334_0_0_0;
static const Il2CppType* MonoCustomAttrs_t1335_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AttributeInfo_t1334_0_0_0,
};
static const EncodedMethodIndex MonoCustomAttrs_t1335_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoCustomAttrs_t1335_0_0_0;
extern const Il2CppType MonoCustomAttrs_t1335_1_0_0;
struct MonoCustomAttrs_t1335;
const Il2CppTypeDefinitionMetadata MonoCustomAttrs_t1335_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MonoCustomAttrs_t1335_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoCustomAttrs_t1335_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5332/* fieldStart */
	, 8567/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoCustomAttrs_t1335_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoCustomAttrs"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoCustomAttrs_t1335_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoCustomAttrs_t1335_0_0_0/* byval_arg */
	, &MonoCustomAttrs_t1335_1_0_0/* this_arg */
	, &MonoCustomAttrs_t1335_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoCustomAttrs_t1335)/* instance_size */
	, sizeof (MonoCustomAttrs_t1335)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoCustomAttrs_t1335_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoCustomAttrs/AttributeInfo
#include "mscorlib_System_MonoCustomAttrs_AttributeInfo.h"
// Metadata Definition System.MonoCustomAttrs/AttributeInfo
extern TypeInfo AttributeInfo_t1334_il2cpp_TypeInfo;
// System.MonoCustomAttrs/AttributeInfo
#include "mscorlib_System_MonoCustomAttrs_AttributeInfoMethodDeclarations.h"
static const EncodedMethodIndex AttributeInfo_t1334_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AttributeInfo_t1334_1_0_0;
struct AttributeInfo_t1334;
const Il2CppTypeDefinitionMetadata AttributeInfo_t1334_DefinitionMetadata = 
{
	&MonoCustomAttrs_t1335_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttributeInfo_t1334_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5335/* fieldStart */
	, 8580/* methodStart */
	, -1/* eventStart */
	, 1629/* propertyStart */

};
TypeInfo AttributeInfo_t1334_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &AttributeInfo_t1334_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttributeInfo_t1334_0_0_0/* byval_arg */
	, &AttributeInfo_t1334_1_0_0/* this_arg */
	, &AttributeInfo_t1334_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeInfo_t1334)/* instance_size */
	, sizeof (AttributeInfo_t1334)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTouchAOTHelper
#include "mscorlib_System_MonoTouchAOTHelper.h"
// Metadata Definition System.MonoTouchAOTHelper
extern TypeInfo MonoTouchAOTHelper_t1336_il2cpp_TypeInfo;
// System.MonoTouchAOTHelper
#include "mscorlib_System_MonoTouchAOTHelperMethodDeclarations.h"
static const EncodedMethodIndex MonoTouchAOTHelper_t1336_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoTouchAOTHelper_t1336_0_0_0;
extern const Il2CppType MonoTouchAOTHelper_t1336_1_0_0;
struct MonoTouchAOTHelper_t1336;
const Il2CppTypeDefinitionMetadata MonoTouchAOTHelper_t1336_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoTouchAOTHelper_t1336_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5337/* fieldStart */
	, 8583/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoTouchAOTHelper_t1336_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTouchAOTHelper"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoTouchAOTHelper_t1336_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoTouchAOTHelper_t1336_0_0_0/* byval_arg */
	, &MonoTouchAOTHelper_t1336_1_0_0/* this_arg */
	, &MonoTouchAOTHelper_t1336_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTouchAOTHelper_t1336)/* instance_size */
	, sizeof (MonoTouchAOTHelper_t1336)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoTouchAOTHelper_t1336_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTypeInfo
#include "mscorlib_System_MonoTypeInfo.h"
// Metadata Definition System.MonoTypeInfo
extern TypeInfo MonoTypeInfo_t1337_il2cpp_TypeInfo;
// System.MonoTypeInfo
#include "mscorlib_System_MonoTypeInfoMethodDeclarations.h"
static const EncodedMethodIndex MonoTypeInfo_t1337_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoTypeInfo_t1337_0_0_0;
extern const Il2CppType MonoTypeInfo_t1337_1_0_0;
struct MonoTypeInfo_t1337;
const Il2CppTypeDefinitionMetadata MonoTypeInfo_t1337_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoTypeInfo_t1337_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5338/* fieldStart */
	, 8584/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoTypeInfo_t1337_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTypeInfo"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoTypeInfo_t1337_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoTypeInfo_t1337_0_0_0/* byval_arg */
	, &MonoTypeInfo_t1337_1_0_0/* this_arg */
	, &MonoTypeInfo_t1337_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTypeInfo_t1337)/* instance_size */
	, sizeof (MonoTypeInfo_t1337)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoType
#include "mscorlib_System_MonoType.h"
// Metadata Definition System.MonoType
extern TypeInfo MonoType_t_il2cpp_TypeInfo;
// System.MonoType
#include "mscorlib_System_MonoTypeMethodDeclarations.h"
static const EncodedMethodIndex MonoType_t_VTable[82] = 
{
	1420,
	125,
	1421,
	3075,
	3076,
	3077,
	3078,
	3079,
	3080,
	3081,
	3082,
	3077,
	3083,
	3076,
	3084,
	3085,
	1428,
	3086,
	3087,
	1429,
	1430,
	1431,
	1432,
	1433,
	1434,
	1435,
	1436,
	1437,
	1438,
	1439,
	1440,
	1441,
	1442,
	1443,
	3088,
	3089,
	3090,
	1445,
	3091,
	3092,
	1447,
	1448,
	3093,
	3094,
	3095,
	3096,
	1449,
	1450,
	1451,
	1452,
	3097,
	3098,
	3099,
	1453,
	1454,
	1455,
	1456,
	3100,
	3101,
	3102,
	3103,
	3104,
	3105,
	3106,
	3107,
	1457,
	1458,
	1459,
	1460,
	1461,
	1462,
	1463,
	3108,
	3109,
	3110,
	3111,
	1466,
	3112,
	1468,
	1469,
	3113,
	3114,
};
static const Il2CppType* MonoType_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
extern const Il2CppType IReflect_t2065_0_0_0;
extern const Il2CppType _Type_t2063_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t1404_0_0_0;
extern const Il2CppType _MemberInfo_t2064_0_0_0;
static Il2CppInterfaceOffsetPair MonoType_t_InterfacesOffsets[] = 
{
	{ &IReflect_t2065_0_0_0, 14},
	{ &_Type_t2063_0_0_0, 14},
	{ &ICustomAttributeProvider_t1404_0_0_0, 4},
	{ &_MemberInfo_t2064_0_0_0, 6},
	{ &ISerializable_t1426_0_0_0, 81},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType MonoType_t_1_0_0;
extern const Il2CppType Type_t_0_0_0;
struct MonoType_t;
const Il2CppTypeDefinitionMetadata MonoType_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoType_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoType_t_InterfacesOffsets/* interfaceOffsets */
	, &Type_t_0_0_0/* parent */
	, MonoType_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5340/* fieldStart */
	, 8585/* methodStart */
	, -1/* eventStart */
	, 1631/* propertyStart */

};
TypeInfo MonoType_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoType"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoType_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoType_t_0_0_0/* byval_arg */
	, &MonoType_t_1_0_0/* this_arg */
	, &MonoType_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoType_t)/* instance_size */
	, sizeof (MonoType_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 50/* method_count */
	, 14/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 82/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.MulticastNotSupportedException
#include "mscorlib_System_MulticastNotSupportedException.h"
// Metadata Definition System.MulticastNotSupportedException
extern TypeInfo MulticastNotSupportedException_t1338_il2cpp_TypeInfo;
// System.MulticastNotSupportedException
#include "mscorlib_System_MulticastNotSupportedExceptionMethodDeclarations.h"
static const EncodedMethodIndex MulticastNotSupportedException_t1338_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair MulticastNotSupportedException_t1338_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MulticastNotSupportedException_t1338_0_0_0;
extern const Il2CppType MulticastNotSupportedException_t1338_1_0_0;
struct MulticastNotSupportedException_t1338;
const Il2CppTypeDefinitionMetadata MulticastNotSupportedException_t1338_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MulticastNotSupportedException_t1338_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, MulticastNotSupportedException_t1338_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8635/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MulticastNotSupportedException_t1338_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MulticastNotSupportedException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MulticastNotSupportedException_t1338_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1500/* custom_attributes_cache */
	, &MulticastNotSupportedException_t1338_0_0_0/* byval_arg */
	, &MulticastNotSupportedException_t1338_1_0_0/* this_arg */
	, &MulticastNotSupportedException_t1338_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MulticastNotSupportedException_t1338)/* instance_size */
	, sizeof (MulticastNotSupportedException_t1338)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttribute.h"
// Metadata Definition System.NonSerializedAttribute
extern TypeInfo NonSerializedAttribute_t1339_il2cpp_TypeInfo;
// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttributeMethodDeclarations.h"
static const EncodedMethodIndex NonSerializedAttribute_t1339_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair NonSerializedAttribute_t1339_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NonSerializedAttribute_t1339_0_0_0;
extern const Il2CppType NonSerializedAttribute_t1339_1_0_0;
struct NonSerializedAttribute_t1339;
const Il2CppTypeDefinitionMetadata NonSerializedAttribute_t1339_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NonSerializedAttribute_t1339_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, NonSerializedAttribute_t1339_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8638/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NonSerializedAttribute_t1339_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NonSerializedAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &NonSerializedAttribute_t1339_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1501/* custom_attributes_cache */
	, &NonSerializedAttribute_t1339_0_0_0/* byval_arg */
	, &NonSerializedAttribute_t1339_1_0_0/* this_arg */
	, &NonSerializedAttribute_t1339_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NonSerializedAttribute_t1339)/* instance_size */
	, sizeof (NonSerializedAttribute_t1339)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.NotImplementedException
#include "mscorlib_System_NotImplementedException.h"
// Metadata Definition System.NotImplementedException
extern TypeInfo NotImplementedException_t581_il2cpp_TypeInfo;
// System.NotImplementedException
#include "mscorlib_System_NotImplementedExceptionMethodDeclarations.h"
static const EncodedMethodIndex NotImplementedException_t581_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair NotImplementedException_t581_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NotImplementedException_t581_0_0_0;
extern const Il2CppType NotImplementedException_t581_1_0_0;
struct NotImplementedException_t581;
const Il2CppTypeDefinitionMetadata NotImplementedException_t581_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NotImplementedException_t581_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, NotImplementedException_t581_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8639/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NotImplementedException_t581_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NotImplementedException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &NotImplementedException_t581_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1502/* custom_attributes_cache */
	, &NotImplementedException_t581_0_0_0/* byval_arg */
	, &NotImplementedException_t581_1_0_0/* this_arg */
	, &NotImplementedException_t581_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NotImplementedException_t581)/* instance_size */
	, sizeof (NotImplementedException_t581)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// Metadata Definition System.NotSupportedException
extern TypeInfo NotSupportedException_t575_il2cpp_TypeInfo;
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
static const EncodedMethodIndex NotSupportedException_t575_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair NotSupportedException_t575_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NotSupportedException_t575_0_0_0;
extern const Il2CppType NotSupportedException_t575_1_0_0;
struct NotSupportedException_t575;
const Il2CppTypeDefinitionMetadata NotSupportedException_t575_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NotSupportedException_t575_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, NotSupportedException_t575_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5341/* fieldStart */
	, 8642/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NotSupportedException_t575_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NotSupportedException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &NotSupportedException_t575_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1503/* custom_attributes_cache */
	, &NotSupportedException_t575_0_0_0/* byval_arg */
	, &NotSupportedException_t575_1_0_0/* this_arg */
	, &NotSupportedException_t575_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NotSupportedException_t575)/* instance_size */
	, sizeof (NotSupportedException_t575)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NullReferenceException
#include "mscorlib_System_NullReferenceException.h"
// Metadata Definition System.NullReferenceException
extern TypeInfo NullReferenceException_t312_il2cpp_TypeInfo;
// System.NullReferenceException
#include "mscorlib_System_NullReferenceExceptionMethodDeclarations.h"
static const EncodedMethodIndex NullReferenceException_t312_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair NullReferenceException_t312_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullReferenceException_t312_0_0_0;
extern const Il2CppType NullReferenceException_t312_1_0_0;
struct NullReferenceException_t312;
const Il2CppTypeDefinitionMetadata NullReferenceException_t312_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullReferenceException_t312_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, NullReferenceException_t312_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5342/* fieldStart */
	, 8645/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NullReferenceException_t312_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullReferenceException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &NullReferenceException_t312_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1504/* custom_attributes_cache */
	, &NullReferenceException_t312_0_0_0/* byval_arg */
	, &NullReferenceException_t312_1_0_0/* this_arg */
	, &NullReferenceException_t312_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullReferenceException_t312)/* instance_size */
	, sizeof (NullReferenceException_t312)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NumberFormatter
#include "mscorlib_System_NumberFormatter.h"
// Metadata Definition System.NumberFormatter
extern TypeInfo NumberFormatter_t1341_il2cpp_TypeInfo;
// System.NumberFormatter
#include "mscorlib_System_NumberFormatterMethodDeclarations.h"
extern const Il2CppType CustomInfo_t1340_0_0_0;
static const Il2CppType* NumberFormatter_t1341_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CustomInfo_t1340_0_0_0,
};
static const EncodedMethodIndex NumberFormatter_t1341_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NumberFormatter_t1341_0_0_0;
extern const Il2CppType NumberFormatter_t1341_1_0_0;
struct NumberFormatter_t1341;
const Il2CppTypeDefinitionMetadata NumberFormatter_t1341_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NumberFormatter_t1341_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NumberFormatter_t1341_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5343/* fieldStart */
	, 8648/* methodStart */
	, -1/* eventStart */
	, 1645/* propertyStart */

};
TypeInfo NumberFormatter_t1341_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NumberFormatter"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &NumberFormatter_t1341_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NumberFormatter_t1341_0_0_0/* byval_arg */
	, &NumberFormatter_t1341_1_0_0/* this_arg */
	, &NumberFormatter_t1341_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NumberFormatter_t1341)/* instance_size */
	, sizeof (NumberFormatter_t1341)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(NumberFormatter_t1341_StaticFields)/* static_fields_size */
	, sizeof(NumberFormatter_t1341_ThreadStaticFields)/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 94/* method_count */
	, 6/* property_count */
	, 26/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfo.h"
// Metadata Definition System.NumberFormatter/CustomInfo
extern TypeInfo CustomInfo_t1340_il2cpp_TypeInfo;
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfoMethodDeclarations.h"
static const EncodedMethodIndex CustomInfo_t1340_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CustomInfo_t1340_1_0_0;
struct CustomInfo_t1340;
const Il2CppTypeDefinitionMetadata CustomInfo_t1340_DefinitionMetadata = 
{
	&NumberFormatter_t1341_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CustomInfo_t1340_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5369/* fieldStart */
	, 8742/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CustomInfo_t1340_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CustomInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CustomInfo_t1340_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CustomInfo_t1340_0_0_0/* byval_arg */
	, &CustomInfo_t1340_1_0_0/* this_arg */
	, &CustomInfo_t1340_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CustomInfo_t1340)/* instance_size */
	, sizeof (CustomInfo_t1340)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedException.h"
// Metadata Definition System.ObjectDisposedException
extern TypeInfo ObjectDisposedException_t618_il2cpp_TypeInfo;
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
static const EncodedMethodIndex ObjectDisposedException_t618_VTable[11] = 
{
	120,
	125,
	122,
	203,
	3115,
	205,
	3116,
	207,
	208,
	3115,
	209,
};
static Il2CppInterfaceOffsetPair ObjectDisposedException_t618_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectDisposedException_t618_0_0_0;
extern const Il2CppType ObjectDisposedException_t618_1_0_0;
struct ObjectDisposedException_t618;
const Il2CppTypeDefinitionMetadata ObjectDisposedException_t618_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObjectDisposedException_t618_InterfacesOffsets/* interfaceOffsets */
	, &InvalidOperationException_t573_0_0_0/* parent */
	, ObjectDisposedException_t618_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5383/* fieldStart */
	, 8746/* methodStart */
	, -1/* eventStart */
	, 1651/* propertyStart */

};
TypeInfo ObjectDisposedException_t618_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectDisposedException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ObjectDisposedException_t618_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1506/* custom_attributes_cache */
	, &ObjectDisposedException_t618_0_0_0/* byval_arg */
	, &ObjectDisposedException_t618_1_0_0/* this_arg */
	, &ObjectDisposedException_t618_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectDisposedException_t618)/* instance_size */
	, sizeof (ObjectDisposedException_t618)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OperatingSystem
#include "mscorlib_System_OperatingSystem.h"
// Metadata Definition System.OperatingSystem
extern TypeInfo OperatingSystem_t1319_il2cpp_TypeInfo;
// System.OperatingSystem
#include "mscorlib_System_OperatingSystemMethodDeclarations.h"
static const EncodedMethodIndex OperatingSystem_t1319_VTable[5] = 
{
	120,
	125,
	122,
	3117,
	3118,
};
static const Il2CppType* OperatingSystem_t1319_InterfacesTypeInfos[] = 
{
	&ICloneable_t2056_0_0_0,
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair OperatingSystem_t1319_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OperatingSystem_t1319_0_0_0;
extern const Il2CppType OperatingSystem_t1319_1_0_0;
struct OperatingSystem_t1319;
const Il2CppTypeDefinitionMetadata OperatingSystem_t1319_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, OperatingSystem_t1319_InterfacesTypeInfos/* implementedInterfaces */
	, OperatingSystem_t1319_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OperatingSystem_t1319_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5385/* fieldStart */
	, 8751/* methodStart */
	, -1/* eventStart */
	, 1652/* propertyStart */

};
TypeInfo OperatingSystem_t1319_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OperatingSystem"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &OperatingSystem_t1319_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1507/* custom_attributes_cache */
	, &OperatingSystem_t1319_0_0_0/* byval_arg */
	, &OperatingSystem_t1319_1_0_0/* this_arg */
	, &OperatingSystem_t1319_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OperatingSystem_t1319)/* instance_size */
	, sizeof (OperatingSystem_t1319)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryException.h"
// Metadata Definition System.OutOfMemoryException
extern TypeInfo OutOfMemoryException_t1342_il2cpp_TypeInfo;
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryExceptionMethodDeclarations.h"
static const EncodedMethodIndex OutOfMemoryException_t1342_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair OutOfMemoryException_t1342_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OutOfMemoryException_t1342_0_0_0;
extern const Il2CppType OutOfMemoryException_t1342_1_0_0;
struct OutOfMemoryException_t1342;
const Il2CppTypeDefinitionMetadata OutOfMemoryException_t1342_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OutOfMemoryException_t1342_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, OutOfMemoryException_t1342_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5388/* fieldStart */
	, 8755/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OutOfMemoryException_t1342_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OutOfMemoryException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &OutOfMemoryException_t1342_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1508/* custom_attributes_cache */
	, &OutOfMemoryException_t1342_0_0_0/* byval_arg */
	, &OutOfMemoryException_t1342_1_0_0/* this_arg */
	, &OutOfMemoryException_t1342_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OutOfMemoryException_t1342)/* instance_size */
	, sizeof (OutOfMemoryException_t1342)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OverflowException
#include "mscorlib_System_OverflowException.h"
// Metadata Definition System.OverflowException
extern TypeInfo OverflowException_t1343_il2cpp_TypeInfo;
// System.OverflowException
#include "mscorlib_System_OverflowExceptionMethodDeclarations.h"
static const EncodedMethodIndex OverflowException_t1343_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair OverflowException_t1343_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OverflowException_t1343_0_0_0;
extern const Il2CppType OverflowException_t1343_1_0_0;
struct OverflowException_t1343;
const Il2CppTypeDefinitionMetadata OverflowException_t1343_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OverflowException_t1343_InterfacesOffsets/* interfaceOffsets */
	, &ArithmeticException_t739_0_0_0/* parent */
	, OverflowException_t1343_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5389/* fieldStart */
	, 8757/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OverflowException_t1343_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OverflowException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &OverflowException_t1343_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1509/* custom_attributes_cache */
	, &OverflowException_t1343_0_0_0/* byval_arg */
	, &OverflowException_t1343_1_0_0/* this_arg */
	, &OverflowException_t1343_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OverflowException_t1343)/* instance_size */
	, sizeof (OverflowException_t1343)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
// Metadata Definition System.PlatformID
extern TypeInfo PlatformID_t1344_il2cpp_TypeInfo;
// System.PlatformID
#include "mscorlib_System_PlatformIDMethodDeclarations.h"
static const EncodedMethodIndex PlatformID_t1344_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair PlatformID_t1344_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PlatformID_t1344_0_0_0;
extern const Il2CppType PlatformID_t1344_1_0_0;
const Il2CppTypeDefinitionMetadata PlatformID_t1344_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PlatformID_t1344_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, PlatformID_t1344_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5390/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PlatformID_t1344_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlatformID"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1510/* custom_attributes_cache */
	, &PlatformID_t1344_0_0_0/* byval_arg */
	, &PlatformID_t1344_1_0_0/* this_arg */
	, &PlatformID_t1344_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlatformID_t1344)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PlatformID_t1344)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Random
#include "mscorlib_System_Random.h"
// Metadata Definition System.Random
extern TypeInfo Random_t181_il2cpp_TypeInfo;
// System.Random
#include "mscorlib_System_RandomMethodDeclarations.h"
static const EncodedMethodIndex Random_t181_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Random_t181_0_0_0;
extern const Il2CppType Random_t181_1_0_0;
struct Random_t181;
const Il2CppTypeDefinitionMetadata Random_t181_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Random_t181_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5398/* fieldStart */
	, 8760/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Random_t181_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Random"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Random_t181_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1511/* custom_attributes_cache */
	, &Random_t181_0_0_0/* byval_arg */
	, &Random_t181_1_0_0/* this_arg */
	, &Random_t181_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Random_t181)/* instance_size */
	, sizeof (Random_t181)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RankException
#include "mscorlib_System_RankException.h"
// Metadata Definition System.RankException
extern TypeInfo RankException_t1345_il2cpp_TypeInfo;
// System.RankException
#include "mscorlib_System_RankExceptionMethodDeclarations.h"
static const EncodedMethodIndex RankException_t1345_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair RankException_t1345_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RankException_t1345_0_0_0;
extern const Il2CppType RankException_t1345_1_0_0;
struct RankException_t1345;
const Il2CppTypeDefinitionMetadata RankException_t1345_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RankException_t1345_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, RankException_t1345_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8762/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RankException_t1345_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RankException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &RankException_t1345_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1512/* custom_attributes_cache */
	, &RankException_t1345_0_0_0/* byval_arg */
	, &RankException_t1345_1_0_0/* this_arg */
	, &RankException_t1345_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RankException_t1345)/* instance_size */
	, sizeof (RankException_t1345)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgs.h"
// Metadata Definition System.ResolveEventArgs
extern TypeInfo ResolveEventArgs_t1346_il2cpp_TypeInfo;
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgsMethodDeclarations.h"
static const EncodedMethodIndex ResolveEventArgs_t1346_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ResolveEventArgs_t1346_0_0_0;
extern const Il2CppType ResolveEventArgs_t1346_1_0_0;
struct ResolveEventArgs_t1346;
const Il2CppTypeDefinitionMetadata ResolveEventArgs_t1346_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t648_0_0_0/* parent */
	, ResolveEventArgs_t1346_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5401/* fieldStart */
	, 8765/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ResolveEventArgs_t1346_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResolveEventArgs"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ResolveEventArgs_t1346_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1513/* custom_attributes_cache */
	, &ResolveEventArgs_t1346_0_0_0/* byval_arg */
	, &ResolveEventArgs_t1346_1_0_0/* this_arg */
	, &ResolveEventArgs_t1346_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResolveEventArgs_t1346)/* instance_size */
	, sizeof (ResolveEventArgs_t1346)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
// Metadata Definition System.RuntimeMethodHandle
extern TypeInfo RuntimeMethodHandle_t1347_il2cpp_TypeInfo;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandleMethodDeclarations.h"
static const EncodedMethodIndex RuntimeMethodHandle_t1347_VTable[5] = 
{
	3119,
	125,
	3120,
	152,
	3121,
};
static const Il2CppType* RuntimeMethodHandle_t1347_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair RuntimeMethodHandle_t1347_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeMethodHandle_t1347_0_0_0;
extern const Il2CppType RuntimeMethodHandle_t1347_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeMethodHandle_t1347_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RuntimeMethodHandle_t1347_InterfacesTypeInfos/* implementedInterfaces */
	, RuntimeMethodHandle_t1347_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, RuntimeMethodHandle_t1347_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5402/* fieldStart */
	, 8766/* methodStart */
	, -1/* eventStart */
	, 1653/* propertyStart */

};
TypeInfo RuntimeMethodHandle_t1347_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeMethodHandle"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &RuntimeMethodHandle_t1347_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1514/* custom_attributes_cache */
	, &RuntimeMethodHandle_t1347_0_0_0/* byval_arg */
	, &RuntimeMethodHandle_t1347_1_0_0/* this_arg */
	, &RuntimeMethodHandle_t1347_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeMethodHandle_t1347)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeMethodHandle_t1347)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeMethodHandle_t1347 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.StringComparer
#include "mscorlib_System_StringComparer.h"
// Metadata Definition System.StringComparer
extern TypeInfo StringComparer_t323_il2cpp_TypeInfo;
// System.StringComparer
#include "mscorlib_System_StringComparerMethodDeclarations.h"
static const EncodedMethodIndex StringComparer_t323_VTable[13] = 
{
	120,
	125,
	122,
	123,
	3122,
	3123,
	3124,
	3125,
	3126,
	3127,
	0,
	0,
	0,
};
extern const Il2CppType IComparer_1_t2385_0_0_0;
extern const Il2CppType IEqualityComparer_1_t1488_0_0_0;
extern const Il2CppType IEqualityComparer_t397_0_0_0;
static const Il2CppType* StringComparer_t323_InterfacesTypeInfos[] = 
{
	&IComparer_1_t2385_0_0_0,
	&IEqualityComparer_1_t1488_0_0_0,
	&IComparer_t390_0_0_0,
	&IEqualityComparer_t397_0_0_0,
};
static Il2CppInterfaceOffsetPair StringComparer_t323_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2385_0_0_0, 4},
	{ &IEqualityComparer_1_t1488_0_0_0, 5},
	{ &IComparer_t390_0_0_0, 7},
	{ &IEqualityComparer_t397_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringComparer_t323_0_0_0;
extern const Il2CppType StringComparer_t323_1_0_0;
struct StringComparer_t323;
const Il2CppTypeDefinitionMetadata StringComparer_t323_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StringComparer_t323_InterfacesTypeInfos/* implementedInterfaces */
	, StringComparer_t323_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StringComparer_t323_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5403/* fieldStart */
	, 8772/* methodStart */
	, -1/* eventStart */
	, 1654/* propertyStart */

};
TypeInfo StringComparer_t323_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringComparer"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &StringComparer_t323_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1516/* custom_attributes_cache */
	, &StringComparer_t323_0_0_0/* byval_arg */
	, &StringComparer_t323_1_0_0/* this_arg */
	, &StringComparer_t323_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringComparer_t323)/* instance_size */
	, sizeof (StringComparer_t323)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StringComparer_t323_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparer.h"
// Metadata Definition System.CultureAwareComparer
extern TypeInfo CultureAwareComparer_t1348_il2cpp_TypeInfo;
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparerMethodDeclarations.h"
static const EncodedMethodIndex CultureAwareComparer_t1348_VTable[13] = 
{
	120,
	125,
	122,
	123,
	3128,
	3129,
	3130,
	3125,
	3126,
	3127,
	3128,
	3129,
	3130,
};
static Il2CppInterfaceOffsetPair CultureAwareComparer_t1348_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2385_0_0_0, 4},
	{ &IEqualityComparer_1_t1488_0_0_0, 5},
	{ &IComparer_t390_0_0_0, 7},
	{ &IEqualityComparer_t397_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CultureAwareComparer_t1348_0_0_0;
extern const Il2CppType CultureAwareComparer_t1348_1_0_0;
struct CultureAwareComparer_t1348;
const Il2CppTypeDefinitionMetadata CultureAwareComparer_t1348_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CultureAwareComparer_t1348_InterfacesOffsets/* interfaceOffsets */
	, &StringComparer_t323_0_0_0/* parent */
	, CultureAwareComparer_t1348_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5407/* fieldStart */
	, 8782/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CultureAwareComparer_t1348_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CultureAwareComparer"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &CultureAwareComparer_t1348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CultureAwareComparer_t1348_0_0_0/* byval_arg */
	, &CultureAwareComparer_t1348_1_0_0/* this_arg */
	, &CultureAwareComparer_t1348_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CultureAwareComparer_t1348)/* instance_size */
	, sizeof (CultureAwareComparer_t1348)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparer.h"
// Metadata Definition System.OrdinalComparer
extern TypeInfo OrdinalComparer_t1349_il2cpp_TypeInfo;
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparerMethodDeclarations.h"
static const EncodedMethodIndex OrdinalComparer_t1349_VTable[13] = 
{
	120,
	125,
	122,
	123,
	3131,
	3132,
	3133,
	3125,
	3126,
	3127,
	3131,
	3132,
	3133,
};
static Il2CppInterfaceOffsetPair OrdinalComparer_t1349_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2385_0_0_0, 4},
	{ &IEqualityComparer_1_t1488_0_0_0, 5},
	{ &IComparer_t390_0_0_0, 7},
	{ &IEqualityComparer_t397_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OrdinalComparer_t1349_0_0_0;
extern const Il2CppType OrdinalComparer_t1349_1_0_0;
struct OrdinalComparer_t1349;
const Il2CppTypeDefinitionMetadata OrdinalComparer_t1349_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OrdinalComparer_t1349_InterfacesOffsets/* interfaceOffsets */
	, &StringComparer_t323_0_0_0/* parent */
	, OrdinalComparer_t1349_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5409/* fieldStart */
	, 8786/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OrdinalComparer_t1349_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OrdinalComparer"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &OrdinalComparer_t1349_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OrdinalComparer_t1349_0_0_0/* byval_arg */
	, &OrdinalComparer_t1349_1_0_0/* this_arg */
	, &OrdinalComparer_t1349_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OrdinalComparer_t1349)/* instance_size */
	, sizeof (OrdinalComparer_t1349)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.StringComparison
#include "mscorlib_System_StringComparison.h"
// Metadata Definition System.StringComparison
extern TypeInfo StringComparison_t1350_il2cpp_TypeInfo;
// System.StringComparison
#include "mscorlib_System_StringComparisonMethodDeclarations.h"
static const EncodedMethodIndex StringComparison_t1350_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair StringComparison_t1350_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringComparison_t1350_0_0_0;
extern const Il2CppType StringComparison_t1350_1_0_0;
const Il2CppTypeDefinitionMetadata StringComparison_t1350_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringComparison_t1350_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, StringComparison_t1350_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5410/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StringComparison_t1350_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringComparison"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1517/* custom_attributes_cache */
	, &StringComparison_t1350_0_0_0/* byval_arg */
	, &StringComparison_t1350_1_0_0/* this_arg */
	, &StringComparison_t1350_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringComparison_t1350)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringComparison_t1350)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptions.h"
// Metadata Definition System.StringSplitOptions
extern TypeInfo StringSplitOptions_t1351_il2cpp_TypeInfo;
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptionsMethodDeclarations.h"
static const EncodedMethodIndex StringSplitOptions_t1351_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair StringSplitOptions_t1351_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringSplitOptions_t1351_0_0_0;
extern const Il2CppType StringSplitOptions_t1351_1_0_0;
const Il2CppTypeDefinitionMetadata StringSplitOptions_t1351_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringSplitOptions_t1351_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, StringSplitOptions_t1351_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5417/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StringSplitOptions_t1351_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringSplitOptions"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1518/* custom_attributes_cache */
	, &StringSplitOptions_t1351_0_0_0/* byval_arg */
	, &StringSplitOptions_t1351_1_0_0/* this_arg */
	, &StringSplitOptions_t1351_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringSplitOptions_t1351)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringSplitOptions_t1351)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.SystemException
#include "mscorlib_System_SystemException.h"
// Metadata Definition System.SystemException
extern TypeInfo SystemException_t597_il2cpp_TypeInfo;
// System.SystemException
#include "mscorlib_System_SystemExceptionMethodDeclarations.h"
static const EncodedMethodIndex SystemException_t597_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair SystemException_t597_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SystemException_t597_1_0_0;
struct SystemException_t597;
const Il2CppTypeDefinitionMetadata SystemException_t597_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SystemException_t597_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t65_0_0_0/* parent */
	, SystemException_t597_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8790/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SystemException_t597_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SystemException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &SystemException_t597_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1519/* custom_attributes_cache */
	, &SystemException_t597_0_0_0/* byval_arg */
	, &SystemException_t597_1_0_0/* this_arg */
	, &SystemException_t597_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SystemException_t597)/* instance_size */
	, sizeof (SystemException_t597)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"
// Metadata Definition System.ThreadStaticAttribute
extern TypeInfo ThreadStaticAttribute_t1352_il2cpp_TypeInfo;
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttributeMethodDeclarations.h"
static const EncodedMethodIndex ThreadStaticAttribute_t1352_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair ThreadStaticAttribute_t1352_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadStaticAttribute_t1352_0_0_0;
extern const Il2CppType ThreadStaticAttribute_t1352_1_0_0;
struct ThreadStaticAttribute_t1352;
const Il2CppTypeDefinitionMetadata ThreadStaticAttribute_t1352_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadStaticAttribute_t1352_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, ThreadStaticAttribute_t1352_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8794/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadStaticAttribute_t1352_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadStaticAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ThreadStaticAttribute_t1352_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1520/* custom_attributes_cache */
	, &ThreadStaticAttribute_t1352_0_0_0/* byval_arg */
	, &ThreadStaticAttribute_t1352_1_0_0/* this_arg */
	, &ThreadStaticAttribute_t1352_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadStaticAttribute_t1352)/* instance_size */
	, sizeof (ThreadStaticAttribute_t1352)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// Metadata Definition System.TimeSpan
extern TypeInfo TimeSpan_t463_il2cpp_TypeInfo;
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
static const EncodedMethodIndex TimeSpan_t463_VTable[7] = 
{
	3134,
	125,
	3135,
	3136,
	3137,
	3138,
	3139,
};
extern const Il2CppType IComparable_1_t2386_0_0_0;
extern const Il2CppType IEquatable_1_t2387_0_0_0;
static const Il2CppType* TimeSpan_t463_InterfacesTypeInfos[] = 
{
	&IComparable_t1408_0_0_0,
	&IComparable_1_t2386_0_0_0,
	&IEquatable_1_t2387_0_0_0,
};
static Il2CppInterfaceOffsetPair TimeSpan_t463_InterfacesOffsets[] = 
{
	{ &IComparable_t1408_0_0_0, 4},
	{ &IComparable_1_t2386_0_0_0, 5},
	{ &IEquatable_1_t2387_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimeSpan_t463_0_0_0;
extern const Il2CppType TimeSpan_t463_1_0_0;
const Il2CppTypeDefinitionMetadata TimeSpan_t463_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TimeSpan_t463_InterfacesTypeInfos/* implementedInterfaces */
	, TimeSpan_t463_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, TimeSpan_t463_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5420/* fieldStart */
	, 8795/* methodStart */
	, -1/* eventStart */
	, 1656/* propertyStart */

};
TypeInfo TimeSpan_t463_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeSpan"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &TimeSpan_t463_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1521/* custom_attributes_cache */
	, &TimeSpan_t463_0_0_0/* byval_arg */
	, &TimeSpan_t463_1_0_0/* this_arg */
	, &TimeSpan_t463_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeSpan_t463)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeSpan_t463)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(TimeSpan_t463 )/* native_size */
	, sizeof(TimeSpan_t463_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 41/* method_count */
	, 11/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.TimeZone
#include "mscorlib_System_TimeZone.h"
// Metadata Definition System.TimeZone
extern TypeInfo TimeZone_t1353_il2cpp_TypeInfo;
// System.TimeZone
#include "mscorlib_System_TimeZoneMethodDeclarations.h"
static const EncodedMethodIndex TimeZone_t1353_VTable[9] = 
{
	120,
	125,
	122,
	123,
	0,
	0,
	3140,
	3141,
	3142,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimeZone_t1353_0_0_0;
extern const Il2CppType TimeZone_t1353_1_0_0;
struct TimeZone_t1353;
const Il2CppTypeDefinitionMetadata TimeZone_t1353_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TimeZone_t1353_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5424/* fieldStart */
	, 8836/* methodStart */
	, -1/* eventStart */
	, 1667/* propertyStart */

};
TypeInfo TimeZone_t1353_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeZone"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &TimeZone_t1353_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1522/* custom_attributes_cache */
	, &TimeZone_t1353_0_0_0/* byval_arg */
	, &TimeZone_t1353_1_0_0/* this_arg */
	, &TimeZone_t1353_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeZone_t1353)/* instance_size */
	, sizeof (TimeZone_t1353)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TimeZone_t1353_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZone.h"
// Metadata Definition System.CurrentSystemTimeZone
extern TypeInfo CurrentSystemTimeZone_t1354_il2cpp_TypeInfo;
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZoneMethodDeclarations.h"
static const EncodedMethodIndex CurrentSystemTimeZone_t1354_VTable[10] = 
{
	120,
	125,
	122,
	123,
	3143,
	3144,
	3140,
	3141,
	3142,
	3145,
};
static const Il2CppType* CurrentSystemTimeZone_t1354_InterfacesTypeInfos[] = 
{
	&IDeserializationCallback_t1429_0_0_0,
};
static Il2CppInterfaceOffsetPair CurrentSystemTimeZone_t1354_InterfacesOffsets[] = 
{
	{ &IDeserializationCallback_t1429_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CurrentSystemTimeZone_t1354_0_0_0;
extern const Il2CppType CurrentSystemTimeZone_t1354_1_0_0;
struct CurrentSystemTimeZone_t1354;
const Il2CppTypeDefinitionMetadata CurrentSystemTimeZone_t1354_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CurrentSystemTimeZone_t1354_InterfacesTypeInfos/* implementedInterfaces */
	, CurrentSystemTimeZone_t1354_InterfacesOffsets/* interfaceOffsets */
	, &TimeZone_t1353_0_0_0/* parent */
	, CurrentSystemTimeZone_t1354_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5425/* fieldStart */
	, 8847/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CurrentSystemTimeZone_t1354_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CurrentSystemTimeZone"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &CurrentSystemTimeZone_t1354_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CurrentSystemTimeZone_t1354_0_0_0/* byval_arg */
	, &CurrentSystemTimeZone_t1354_1_0_0/* this_arg */
	, &CurrentSystemTimeZone_t1354_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CurrentSystemTimeZone_t1354)/* instance_size */
	, sizeof (CurrentSystemTimeZone_t1354)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CurrentSystemTimeZone_t1354_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
