﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t151;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t1843;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t279;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t287;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
extern "C" void List_1__ctor_m9428_gshared (List_1_t151 * __this, const MethodInfo* method);
#define List_1__ctor_m9428(__this, method) (( void (*) (List_1_t151 *, const MethodInfo*))List_1__ctor_m9428_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1231_gshared (List_1_t151 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1231(__this, ___capacity, method) (( void (*) (List_1_t151 *, int32_t, const MethodInfo*))List_1__ctor_m1231_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.cctor()
extern "C" void List_1__cctor_m9429_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m9429(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m9429_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9430_gshared (List_1_t151 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9430(__this, method) (( Object_t* (*) (List_1_t151 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9430_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m9431_gshared (List_1_t151 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m9431(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t151 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m9431_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m9432_gshared (List_1_t151 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m9432(__this, method) (( Object_t * (*) (List_1_t151 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m9432_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m9433_gshared (List_1_t151 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m9433(__this, ___item, method) (( int32_t (*) (List_1_t151 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m9433_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m9434_gshared (List_1_t151 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m9434(__this, ___item, method) (( bool (*) (List_1_t151 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m9434_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m9435_gshared (List_1_t151 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m9435(__this, ___item, method) (( int32_t (*) (List_1_t151 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m9435_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m9436_gshared (List_1_t151 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m9436(__this, ___index, ___item, method) (( void (*) (List_1_t151 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m9436_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m9437_gshared (List_1_t151 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m9437(__this, ___item, method) (( void (*) (List_1_t151 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m9437_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9438_gshared (List_1_t151 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9438(__this, method) (( bool (*) (List_1_t151 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9438_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m9439_gshared (List_1_t151 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m9439(__this, method) (( Object_t * (*) (List_1_t151 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m9439_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m9440_gshared (List_1_t151 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m9440(__this, ___index, method) (( Object_t * (*) (List_1_t151 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m9440_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m9441_gshared (List_1_t151 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m9441(__this, ___index, ___value, method) (( void (*) (List_1_t151 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m9441_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(T)
extern "C" void List_1_Add_m9442_gshared (List_1_t151 * __this, UIVertex_t158  ___item, const MethodInfo* method);
#define List_1_Add_m9442(__this, ___item, method) (( void (*) (List_1_t151 *, UIVertex_t158 , const MethodInfo*))List_1_Add_m9442_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m9443_gshared (List_1_t151 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m9443(__this, ___newCount, method) (( void (*) (List_1_t151 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m9443_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
extern "C" void List_1_Clear_m9444_gshared (List_1_t151 * __this, const MethodInfo* method);
#define List_1_Clear_m9444(__this, method) (( void (*) (List_1_t151 *, const MethodInfo*))List_1_Clear_m9444_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool List_1_Contains_m9445_gshared (List_1_t151 * __this, UIVertex_t158  ___item, const MethodInfo* method);
#define List_1_Contains_m9445(__this, ___item, method) (( bool (*) (List_1_t151 *, UIVertex_t158 , const MethodInfo*))List_1_Contains_m9445_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m9446_gshared (List_1_t151 * __this, UIVertexU5BU5D_t287* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m9446(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t151 *, UIVertexU5BU5D_t287*, int32_t, const MethodInfo*))List_1_CopyTo_m9446_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Enumerator_t1538  List_1_GetEnumerator_m9447_gshared (List_1_t151 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m9447(__this, method) (( Enumerator_t1538  (*) (List_1_t151 *, const MethodInfo*))List_1_GetEnumerator_m9447_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m9448_gshared (List_1_t151 * __this, UIVertex_t158  ___item, const MethodInfo* method);
#define List_1_IndexOf_m9448(__this, ___item, method) (( int32_t (*) (List_1_t151 *, UIVertex_t158 , const MethodInfo*))List_1_IndexOf_m9448_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m9449_gshared (List_1_t151 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m9449(__this, ___start, ___delta, method) (( void (*) (List_1_t151 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m9449_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m9450_gshared (List_1_t151 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m9450(__this, ___index, method) (( void (*) (List_1_t151 *, int32_t, const MethodInfo*))List_1_CheckIndex_m9450_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m9451_gshared (List_1_t151 * __this, int32_t ___index, UIVertex_t158  ___item, const MethodInfo* method);
#define List_1_Insert_m9451(__this, ___index, ___item, method) (( void (*) (List_1_t151 *, int32_t, UIVertex_t158 , const MethodInfo*))List_1_Insert_m9451_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool List_1_Remove_m9452_gshared (List_1_t151 * __this, UIVertex_t158  ___item, const MethodInfo* method);
#define List_1_Remove_m9452(__this, ___item, method) (( bool (*) (List_1_t151 *, UIVertex_t158 , const MethodInfo*))List_1_Remove_m9452_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m9453_gshared (List_1_t151 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m9453(__this, ___index, method) (( void (*) (List_1_t151 *, int32_t, const MethodInfo*))List_1_RemoveAt_m9453_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UIVertex>::ToArray()
extern "C" UIVertexU5BU5D_t287* List_1_ToArray_m9454_gshared (List_1_t151 * __this, const MethodInfo* method);
#define List_1_ToArray_m9454(__this, method) (( UIVertexU5BU5D_t287* (*) (List_1_t151 *, const MethodInfo*))List_1_ToArray_m9454_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m9455_gshared (List_1_t151 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m9455(__this, method) (( int32_t (*) (List_1_t151 *, const MethodInfo*))List_1_get_Capacity_m9455_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m9456_gshared (List_1_t151 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m9456(__this, ___value, method) (( void (*) (List_1_t151 *, int32_t, const MethodInfo*))List_1_set_Capacity_m9456_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t List_1_get_Count_m9457_gshared (List_1_t151 * __this, const MethodInfo* method);
#define List_1_get_Count_m9457(__this, method) (( int32_t (*) (List_1_t151 *, const MethodInfo*))List_1_get_Count_m9457_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t158  List_1_get_Item_m9458_gshared (List_1_t151 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m9458(__this, ___index, method) (( UIVertex_t158  (*) (List_1_t151 *, int32_t, const MethodInfo*))List_1_get_Item_m9458_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m9459_gshared (List_1_t151 * __this, int32_t ___index, UIVertex_t158  ___value, const MethodInfo* method);
#define List_1_set_Item_m9459(__this, ___index, ___value, method) (( void (*) (List_1_t151 *, int32_t, UIVertex_t158 , const MethodInfo*))List_1_set_Item_m9459_gshared)(__this, ___index, ___value, method)
