﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$56
struct U24ArrayTypeU2456_t1366;
struct U24ArrayTypeU2456_t1366_marshaled;

void U24ArrayTypeU2456_t1366_marshal(const U24ArrayTypeU2456_t1366& unmarshaled, U24ArrayTypeU2456_t1366_marshaled& marshaled);
void U24ArrayTypeU2456_t1366_marshal_back(const U24ArrayTypeU2456_t1366_marshaled& marshaled, U24ArrayTypeU2456_t1366& unmarshaled);
void U24ArrayTypeU2456_t1366_marshal_cleanup(U24ArrayTypeU2456_t1366_marshaled& marshaled);
