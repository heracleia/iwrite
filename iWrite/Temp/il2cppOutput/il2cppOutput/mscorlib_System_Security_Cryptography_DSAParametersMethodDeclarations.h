﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSAParameters
struct DSAParameters_t586;
struct DSAParameters_t586_marshaled;

void DSAParameters_t586_marshal(const DSAParameters_t586& unmarshaled, DSAParameters_t586_marshaled& marshaled);
void DSAParameters_t586_marshal_back(const DSAParameters_t586_marshaled& marshaled, DSAParameters_t586& unmarshaled);
void DSAParameters_t586_marshal_cleanup(DSAParameters_t586_marshaled& marshaled);
