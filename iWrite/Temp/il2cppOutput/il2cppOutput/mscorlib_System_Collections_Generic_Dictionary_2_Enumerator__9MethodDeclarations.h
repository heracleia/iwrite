﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Enumerator_t1586;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Dictionary_2_t1580;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m10028_gshared (Enumerator_t1586 * __this, Dictionary_2_t1580 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m10028(__this, ___dictionary, method) (( void (*) (Enumerator_t1586 *, Dictionary_2_t1580 *, const MethodInfo*))Enumerator__ctor_m10028_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m10029_gshared (Enumerator_t1586 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m10029(__this, method) (( Object_t * (*) (Enumerator_t1586 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m10029_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t560  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m10030_gshared (Enumerator_t1586 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m10030(__this, method) (( DictionaryEntry_t560  (*) (Enumerator_t1586 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m10030_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m10031_gshared (Enumerator_t1586 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m10031(__this, method) (( Object_t * (*) (Enumerator_t1586 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m10031_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m10032_gshared (Enumerator_t1586 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m10032(__this, method) (( Object_t * (*) (Enumerator_t1586 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m10032_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m10033_gshared (Enumerator_t1586 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m10033(__this, method) (( bool (*) (Enumerator_t1586 *, const MethodInfo*))Enumerator_MoveNext_m10033_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Current()
extern "C" KeyValuePair_2_t1581  Enumerator_get_Current_m10034_gshared (Enumerator_t1586 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m10034(__this, method) (( KeyValuePair_2_t1581  (*) (Enumerator_t1586 *, const MethodInfo*))Enumerator_get_Current_m10034_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_CurrentKey()
extern "C" uint64_t Enumerator_get_CurrentKey_m10035_gshared (Enumerator_t1586 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m10035(__this, method) (( uint64_t (*) (Enumerator_t1586 *, const MethodInfo*))Enumerator_get_CurrentKey_m10035_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m10036_gshared (Enumerator_t1586 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m10036(__this, method) (( Object_t * (*) (Enumerator_t1586 *, const MethodInfo*))Enumerator_get_CurrentValue_m10036_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m10037_gshared (Enumerator_t1586 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m10037(__this, method) (( void (*) (Enumerator_t1586 *, const MethodInfo*))Enumerator_VerifyState_m10037_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m10038_gshared (Enumerator_t1586 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m10038(__this, method) (( void (*) (Enumerator_t1586 *, const MethodInfo*))Enumerator_VerifyCurrent_m10038_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m10039_gshared (Enumerator_t1586 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m10039(__this, method) (( void (*) (Enumerator_t1586 *, const MethodInfo*))Enumerator_Dispose_m10039_gshared)(__this, method)
