﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t395;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Xml.SmallXmlParser/AttrListImpl
struct  AttrListImpl_t859  : public Object_t
{
	// System.Collections.ArrayList Mono.Xml.SmallXmlParser/AttrListImpl::attrNames
	ArrayList_t395 * ___attrNames_0;
	// System.Collections.ArrayList Mono.Xml.SmallXmlParser/AttrListImpl::attrValues
	ArrayList_t395 * ___attrValues_1;
};
