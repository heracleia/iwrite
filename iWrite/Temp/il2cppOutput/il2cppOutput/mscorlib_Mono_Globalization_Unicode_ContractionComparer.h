﻿#pragma once
#include <stdint.h>
// Mono.Globalization.Unicode.ContractionComparer
struct ContractionComparer_t802;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Globalization.Unicode.ContractionComparer
struct  ContractionComparer_t802  : public Object_t
{
};
struct ContractionComparer_t802_StaticFields{
	// Mono.Globalization.Unicode.ContractionComparer Mono.Globalization.Unicode.ContractionComparer::Instance
	ContractionComparer_t802 * ___Instance_0;
};
