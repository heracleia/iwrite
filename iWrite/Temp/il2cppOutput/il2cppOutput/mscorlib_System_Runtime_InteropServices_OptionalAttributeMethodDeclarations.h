﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.OptionalAttribute
struct OptionalAttribute_t778;

// System.Void System.Runtime.InteropServices.OptionalAttribute::.ctor()
extern "C" void OptionalAttribute__ctor_m4201 (OptionalAttribute_t778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
