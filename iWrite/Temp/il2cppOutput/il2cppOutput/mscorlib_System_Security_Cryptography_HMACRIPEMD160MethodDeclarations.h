﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACRIPEMD160
struct HMACRIPEMD160_t1187;
// System.Byte[]
struct ByteU5BU5D_t102;

// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor()
extern "C" void HMACRIPEMD160__ctor_m6734 (HMACRIPEMD160_t1187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor(System.Byte[])
extern "C" void HMACRIPEMD160__ctor_m6735 (HMACRIPEMD160_t1187 * __this, ByteU5BU5D_t102* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
