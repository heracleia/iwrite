﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// Mono.Globalization.Unicode.SimpleCollator
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator.h"
// Metadata Definition Mono.Globalization.Unicode.SimpleCollator
extern TypeInfo SimpleCollator_t812_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.SimpleCollator
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollatorMethodDeclarations.h"
extern const Il2CppType Context_t808_0_0_0;
extern const Il2CppType PreviousInfo_t809_0_0_0;
extern const Il2CppType Escape_t810_0_0_0;
extern const Il2CppType ExtenderType_t811_0_0_0;
static const Il2CppType* SimpleCollator_t812_il2cpp_TypeInfo__nestedTypes[4] =
{
	&Context_t808_0_0_0,
	&PreviousInfo_t809_0_0_0,
	&Escape_t810_0_0_0,
	&ExtenderType_t811_0_0_0,
};
static const EncodedMethodIndex SimpleCollator_t812_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SimpleCollator_t812_0_0_0;
extern const Il2CppType SimpleCollator_t812_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SimpleCollator_t812;
const Il2CppTypeDefinitionMetadata SimpleCollator_t812_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SimpleCollator_t812_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SimpleCollator_t812_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2696/* fieldStart */
	, 4296/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SimpleCollator_t812_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleCollator"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &SimpleCollator_t812_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SimpleCollator_t812_0_0_0/* byval_arg */
	, &SimpleCollator_t812_1_0_0/* this_arg */
	, &SimpleCollator_t812_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleCollator_t812)/* instance_size */
	, sizeof (SimpleCollator_t812)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SimpleCollator_t812_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 52/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 4/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.SimpleCollator/Context
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
// Metadata Definition Mono.Globalization.Unicode.SimpleCollator/Context
extern TypeInfo Context_t808_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.SimpleCollator/Context
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ContextMethodDeclarations.h"
static const EncodedMethodIndex Context_t808_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Context_t808_1_0_0;
extern const Il2CppType ValueType_t757_0_0_0;
const Il2CppTypeDefinitionMetadata Context_t808_DefinitionMetadata = 
{
	&SimpleCollator_t812_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Context_t808_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2709/* fieldStart */
	, 4348/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Context_t808_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Context"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Context_t808_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Context_t808_0_0_0/* byval_arg */
	, &Context_t808_1_0_0/* this_arg */
	, &Context_t808_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Context_t808_marshal/* marshal_to_native_func */
	, (methodPointerType)Context_t808_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Context_t808_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Context_t808)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Context_t808)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Context_t808_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.SimpleCollator/PreviousInfo
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_PreviousI.h"
// Metadata Definition Mono.Globalization.Unicode.SimpleCollator/PreviousInfo
extern TypeInfo PreviousInfo_t809_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.SimpleCollator/PreviousInfo
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_PreviousIMethodDeclarations.h"
static const EncodedMethodIndex PreviousInfo_t809_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PreviousInfo_t809_1_0_0;
const Il2CppTypeDefinitionMetadata PreviousInfo_t809_DefinitionMetadata = 
{
	&SimpleCollator_t812_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, PreviousInfo_t809_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2717/* fieldStart */
	, 4349/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PreviousInfo_t809_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PreviousInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PreviousInfo_t809_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PreviousInfo_t809_0_0_0/* byval_arg */
	, &PreviousInfo_t809_1_0_0/* this_arg */
	, &PreviousInfo_t809_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)PreviousInfo_t809_marshal/* marshal_to_native_func */
	, (methodPointerType)PreviousInfo_t809_marshal_back/* marshal_from_native_func */
	, (methodPointerType)PreviousInfo_t809_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (PreviousInfo_t809)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PreviousInfo_t809)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(PreviousInfo_t809_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.SimpleCollator/Escape
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Escape.h"
// Metadata Definition Mono.Globalization.Unicode.SimpleCollator/Escape
extern TypeInfo Escape_t810_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.SimpleCollator/Escape
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_EscapeMethodDeclarations.h"
static const EncodedMethodIndex Escape_t810_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Escape_t810_1_0_0;
const Il2CppTypeDefinitionMetadata Escape_t810_DefinitionMetadata = 
{
	&SimpleCollator_t812_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Escape_t810_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2719/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Escape_t810_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Escape"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Escape_t810_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Escape_t810_0_0_0/* byval_arg */
	, &Escape_t810_1_0_0/* this_arg */
	, &Escape_t810_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Escape_t810_marshal/* marshal_to_native_func */
	, (methodPointerType)Escape_t810_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Escape_t810_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Escape_t810)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Escape_t810)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Escape_t810_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
// Metadata Definition Mono.Globalization.Unicode.SimpleCollator/ExtenderType
extern TypeInfo ExtenderType_t811_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderTMethodDeclarations.h"
static const EncodedMethodIndex ExtenderType_t811_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
extern const Il2CppType IFormattable_t1406_0_0_0;
extern const Il2CppType IConvertible_t1409_0_0_0;
extern const Il2CppType IComparable_t1408_0_0_0;
static Il2CppInterfaceOffsetPair ExtenderType_t811_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ExtenderType_t811_1_0_0;
extern const Il2CppType Enum_t305_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t320_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata ExtenderType_t811_DefinitionMetadata = 
{
	&SimpleCollator_t812_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExtenderType_t811_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, ExtenderType_t811_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2724/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExtenderType_t811_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtenderType"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExtenderType_t811_0_0_0/* byval_arg */
	, &ExtenderType_t811_1_0_0/* this_arg */
	, &ExtenderType_t811_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExtenderType_t811)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ExtenderType_t811)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Globalization.SortKey
#include "mscorlib_System_Globalization_SortKey.h"
// Metadata Definition System.Globalization.SortKey
extern TypeInfo SortKey_t816_il2cpp_TypeInfo;
// System.Globalization.SortKey
#include "mscorlib_System_Globalization_SortKeyMethodDeclarations.h"
static const EncodedMethodIndex SortKey_t816_VTable[6] = 
{
	1495,
	125,
	1496,
	1497,
	1498,
	1499,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SortKey_t816_0_0_0;
extern const Il2CppType SortKey_t816_1_0_0;
struct SortKey_t816;
const Il2CppTypeDefinitionMetadata SortKey_t816_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SortKey_t816_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2730/* fieldStart */
	, 4350/* methodStart */
	, -1/* eventStart */
	, 820/* propertyStart */

};
TypeInfo SortKey_t816_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SortKey"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &SortKey_t816_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 801/* custom_attributes_cache */
	, &SortKey_t816_0_0_0/* byval_arg */
	, &SortKey_t816_1_0_0/* this_arg */
	, &SortKey_t816_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SortKey_t816)/* instance_size */
	, sizeof (SortKey_t816)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.SortKeyBuffer
#include "mscorlib_Mono_Globalization_Unicode_SortKeyBuffer.h"
// Metadata Definition Mono.Globalization.Unicode.SortKeyBuffer
extern TypeInfo SortKeyBuffer_t817_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.SortKeyBuffer
#include "mscorlib_Mono_Globalization_Unicode_SortKeyBufferMethodDeclarations.h"
static const EncodedMethodIndex SortKeyBuffer_t817_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SortKeyBuffer_t817_0_0_0;
extern const Il2CppType SortKeyBuffer_t817_1_0_0;
struct SortKeyBuffer_t817;
const Il2CppTypeDefinitionMetadata SortKeyBuffer_t817_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SortKeyBuffer_t817_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2734/* fieldStart */
	, 4358/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SortKeyBuffer_t817_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SortKeyBuffer"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &SortKeyBuffer_t817_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SortKeyBuffer_t817_0_0_0/* byval_arg */
	, &SortKeyBuffer_t817_1_0_0/* this_arg */
	, &SortKeyBuffer_t817_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SortKeyBuffer_t817)/* instance_size */
	, sizeof (SortKeyBuffer_t817)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.Prime.Generator.PrimeGeneratorBase
#include "mscorlib_Mono_Math_Prime_Generator_PrimeGeneratorBase.h"
// Metadata Definition Mono.Math.Prime.Generator.PrimeGeneratorBase
extern TypeInfo PrimeGeneratorBase_t818_il2cpp_TypeInfo;
// Mono.Math.Prime.Generator.PrimeGeneratorBase
#include "mscorlib_Mono_Math_Prime_Generator_PrimeGeneratorBaseMethodDeclarations.h"
static const EncodedMethodIndex PrimeGeneratorBase_t818_VTable[8] = 
{
	120,
	125,
	122,
	123,
	1500,
	1501,
	1502,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrimeGeneratorBase_t818_0_0_0;
extern const Il2CppType PrimeGeneratorBase_t818_1_0_0;
struct PrimeGeneratorBase_t818;
const Il2CppTypeDefinitionMetadata PrimeGeneratorBase_t818_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PrimeGeneratorBase_t818_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4369/* methodStart */
	, -1/* eventStart */
	, 822/* propertyStart */

};
TypeInfo PrimeGeneratorBase_t818_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimeGeneratorBase"/* name */
	, "Mono.Math.Prime.Generator"/* namespaze */
	, NULL/* methods */
	, &PrimeGeneratorBase_t818_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimeGeneratorBase_t818_0_0_0/* byval_arg */
	, &PrimeGeneratorBase_t818_1_0_0/* this_arg */
	, &PrimeGeneratorBase_t818_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimeGeneratorBase_t818)/* instance_size */
	, sizeof (PrimeGeneratorBase_t818)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
#include "mscorlib_Mono_Math_Prime_Generator_SequentialSearchPrimeGene.h"
// Metadata Definition Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
extern TypeInfo SequentialSearchPrimeGeneratorBase_t819_il2cpp_TypeInfo;
// Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
#include "mscorlib_Mono_Math_Prime_Generator_SequentialSearchPrimeGeneMethodDeclarations.h"
static const EncodedMethodIndex SequentialSearchPrimeGeneratorBase_t819_VTable[11] = 
{
	120,
	125,
	122,
	123,
	1500,
	1501,
	1502,
	1503,
	1504,
	1505,
	1506,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SequentialSearchPrimeGeneratorBase_t819_0_0_0;
extern const Il2CppType SequentialSearchPrimeGeneratorBase_t819_1_0_0;
struct SequentialSearchPrimeGeneratorBase_t819;
const Il2CppTypeDefinitionMetadata SequentialSearchPrimeGeneratorBase_t819_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PrimeGeneratorBase_t818_0_0_0/* parent */
	, SequentialSearchPrimeGeneratorBase_t819_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4374/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SequentialSearchPrimeGeneratorBase_t819_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SequentialSearchPrimeGeneratorBase"/* name */
	, "Mono.Math.Prime.Generator"/* namespaze */
	, NULL/* methods */
	, &SequentialSearchPrimeGeneratorBase_t819_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SequentialSearchPrimeGeneratorBase_t819_0_0_0/* byval_arg */
	, &SequentialSearchPrimeGeneratorBase_t819_1_0_0/* this_arg */
	, &SequentialSearchPrimeGeneratorBase_t819_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SequentialSearchPrimeGeneratorBase_t819)/* instance_size */
	, sizeof (SequentialSearchPrimeGeneratorBase_t819)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
// Metadata Definition Mono.Math.Prime.ConfidenceFactor
extern TypeInfo ConfidenceFactor_t820_il2cpp_TypeInfo;
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactorMethodDeclarations.h"
static const EncodedMethodIndex ConfidenceFactor_t820_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair ConfidenceFactor_t820_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConfidenceFactor_t820_0_0_0;
extern const Il2CppType ConfidenceFactor_t820_1_0_0;
const Il2CppTypeDefinitionMetadata ConfidenceFactor_t820_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConfidenceFactor_t820_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, ConfidenceFactor_t820_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2756/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConfidenceFactor_t820_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConfidenceFactor"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConfidenceFactor_t820_0_0_0/* byval_arg */
	, &ConfidenceFactor_t820_1_0_0/* this_arg */
	, &ConfidenceFactor_t820_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConfidenceFactor_t820)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ConfidenceFactor_t820)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTests
#include "mscorlib_Mono_Math_Prime_PrimalityTests.h"
// Metadata Definition Mono.Math.Prime.PrimalityTests
extern TypeInfo PrimalityTests_t821_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTests
#include "mscorlib_Mono_Math_Prime_PrimalityTestsMethodDeclarations.h"
static const EncodedMethodIndex PrimalityTests_t821_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrimalityTests_t821_0_0_0;
extern const Il2CppType PrimalityTests_t821_1_0_0;
struct PrimalityTests_t821;
const Il2CppTypeDefinitionMetadata PrimalityTests_t821_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PrimalityTests_t821_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4379/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PrimalityTests_t821_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTests"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, NULL/* methods */
	, &PrimalityTests_t821_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTests_t821_0_0_0/* byval_arg */
	, &PrimalityTests_t821_1_0_0/* this_arg */
	, &PrimalityTests_t821_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTests_t821)/* instance_size */
	, sizeof (PrimalityTests_t821)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.BigInteger
#include "mscorlib_Mono_Math_BigInteger.h"
// Metadata Definition Mono.Math.BigInteger
extern TypeInfo BigInteger_t823_il2cpp_TypeInfo;
// Mono.Math.BigInteger
#include "mscorlib_Mono_Math_BigIntegerMethodDeclarations.h"
extern const Il2CppType Sign_t822_0_0_0;
extern const Il2CppType ModulusRing_t824_0_0_0;
extern const Il2CppType Kernel_t825_0_0_0;
static const Il2CppType* BigInteger_t823_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Sign_t822_0_0_0,
	&ModulusRing_t824_0_0_0,
	&Kernel_t825_0_0_0,
};
static const EncodedMethodIndex BigInteger_t823_VTable[4] = 
{
	1507,
	125,
	1508,
	1509,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BigInteger_t823_0_0_0;
extern const Il2CppType BigInteger_t823_1_0_0;
struct BigInteger_t823;
const Il2CppTypeDefinitionMetadata BigInteger_t823_DefinitionMetadata = 
{
	NULL/* declaringType */
	, BigInteger_t823_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BigInteger_t823_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2763/* fieldStart */
	, 4383/* methodStart */
	, -1/* eventStart */
	, 825/* propertyStart */

};
TypeInfo BigInteger_t823_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BigInteger"/* name */
	, "Mono.Math"/* namespaze */
	, NULL/* methods */
	, &BigInteger_t823_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BigInteger_t823_0_0_0/* byval_arg */
	, &BigInteger_t823_1_0_0/* this_arg */
	, &BigInteger_t823_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BigInteger_t823)/* instance_size */
	, sizeof (BigInteger_t823)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BigInteger_t823_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 49/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.BigInteger/Sign
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
// Metadata Definition Mono.Math.BigInteger/Sign
extern TypeInfo Sign_t822_il2cpp_TypeInfo;
// Mono.Math.BigInteger/Sign
#include "mscorlib_Mono_Math_BigInteger_SignMethodDeclarations.h"
static const EncodedMethodIndex Sign_t822_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair Sign_t822_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Sign_t822_1_0_0;
const Il2CppTypeDefinitionMetadata Sign_t822_DefinitionMetadata = 
{
	&BigInteger_t823_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Sign_t822_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, Sign_t822_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2767/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Sign_t822_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Sign"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Sign_t822_0_0_0/* byval_arg */
	, &Sign_t822_1_0_0/* this_arg */
	, &Sign_t822_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Sign_t822)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Sign_t822)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Math.BigInteger/ModulusRing
#include "mscorlib_Mono_Math_BigInteger_ModulusRing.h"
// Metadata Definition Mono.Math.BigInteger/ModulusRing
extern TypeInfo ModulusRing_t824_il2cpp_TypeInfo;
// Mono.Math.BigInteger/ModulusRing
#include "mscorlib_Mono_Math_BigInteger_ModulusRingMethodDeclarations.h"
static const EncodedMethodIndex ModulusRing_t824_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ModulusRing_t824_1_0_0;
struct ModulusRing_t824;
const Il2CppTypeDefinitionMetadata ModulusRing_t824_DefinitionMetadata = 
{
	&BigInteger_t823_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ModulusRing_t824_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2771/* fieldStart */
	, 4432/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ModulusRing_t824_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ModulusRing"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ModulusRing_t824_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ModulusRing_t824_0_0_0/* byval_arg */
	, &ModulusRing_t824_1_0_0/* this_arg */
	, &ModulusRing_t824_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ModulusRing_t824)/* instance_size */
	, sizeof (ModulusRing_t824)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048837/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.BigInteger/Kernel
#include "mscorlib_Mono_Math_BigInteger_Kernel.h"
// Metadata Definition Mono.Math.BigInteger/Kernel
extern TypeInfo Kernel_t825_il2cpp_TypeInfo;
// Mono.Math.BigInteger/Kernel
#include "mscorlib_Mono_Math_BigInteger_KernelMethodDeclarations.h"
static const EncodedMethodIndex Kernel_t825_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Kernel_t825_1_0_0;
struct Kernel_t825;
const Il2CppTypeDefinitionMetadata Kernel_t825_DefinitionMetadata = 
{
	&BigInteger_t823_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Kernel_t825_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4438/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Kernel_t825_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Kernel"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Kernel_t825_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Kernel_t825_0_0_0/* byval_arg */
	, &Kernel_t825_1_0_0/* this_arg */
	, &Kernel_t825_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Kernel_t825)/* instance_size */
	, sizeof (Kernel_t825)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.CryptoConvert
#include "mscorlib_Mono_Security_Cryptography_CryptoConvert.h"
// Metadata Definition Mono.Security.Cryptography.CryptoConvert
extern TypeInfo CryptoConvert_t826_il2cpp_TypeInfo;
// Mono.Security.Cryptography.CryptoConvert
#include "mscorlib_Mono_Security_Cryptography_CryptoConvertMethodDeclarations.h"
static const EncodedMethodIndex CryptoConvert_t826_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CryptoConvert_t826_0_0_0;
extern const Il2CppType CryptoConvert_t826_1_0_0;
struct CryptoConvert_t826;
const Il2CppTypeDefinitionMetadata CryptoConvert_t826_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CryptoConvert_t826_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4454/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CryptoConvert_t826_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CryptoConvert"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &CryptoConvert_t826_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CryptoConvert_t826_0_0_0/* byval_arg */
	, &CryptoConvert_t826_1_0_0/* this_arg */
	, &CryptoConvert_t826_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CryptoConvert_t826)/* instance_size */
	, sizeof (CryptoConvert_t826)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.KeyBuilder
#include "mscorlib_Mono_Security_Cryptography_KeyBuilder.h"
// Metadata Definition Mono.Security.Cryptography.KeyBuilder
extern TypeInfo KeyBuilder_t827_il2cpp_TypeInfo;
// Mono.Security.Cryptography.KeyBuilder
#include "mscorlib_Mono_Security_Cryptography_KeyBuilderMethodDeclarations.h"
static const EncodedMethodIndex KeyBuilder_t827_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyBuilder_t827_0_0_0;
extern const Il2CppType KeyBuilder_t827_1_0_0;
struct KeyBuilder_t827;
const Il2CppTypeDefinitionMetadata KeyBuilder_t827_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeyBuilder_t827_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2773/* fieldStart */
	, 4462/* methodStart */
	, -1/* eventStart */
	, 826/* propertyStart */

};
TypeInfo KeyBuilder_t827_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyBuilder"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &KeyBuilder_t827_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyBuilder_t827_0_0_0/* byval_arg */
	, &KeyBuilder_t827_1_0_0/* this_arg */
	, &KeyBuilder_t827_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyBuilder_t827)/* instance_size */
	, sizeof (KeyBuilder_t827)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(KeyBuilder_t827_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.BlockProcessor
#include "mscorlib_Mono_Security_Cryptography_BlockProcessor.h"
// Metadata Definition Mono.Security.Cryptography.BlockProcessor
extern TypeInfo BlockProcessor_t828_il2cpp_TypeInfo;
// Mono.Security.Cryptography.BlockProcessor
#include "mscorlib_Mono_Security_Cryptography_BlockProcessorMethodDeclarations.h"
static const EncodedMethodIndex BlockProcessor_t828_VTable[4] = 
{
	120,
	1510,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BlockProcessor_t828_0_0_0;
extern const Il2CppType BlockProcessor_t828_1_0_0;
struct BlockProcessor_t828;
const Il2CppTypeDefinitionMetadata BlockProcessor_t828_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BlockProcessor_t828_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2774/* fieldStart */
	, 4465/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BlockProcessor_t828_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BlockProcessor"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &BlockProcessor_t828_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BlockProcessor_t828_0_0_0/* byval_arg */
	, &BlockProcessor_t828_1_0_0/* this_arg */
	, &BlockProcessor_t828_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BlockProcessor_t828)/* instance_size */
	, sizeof (BlockProcessor_t828)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.DSAManaged
#include "mscorlib_Mono_Security_Cryptography_DSAManaged.h"
// Metadata Definition Mono.Security.Cryptography.DSAManaged
extern TypeInfo DSAManaged_t830_il2cpp_TypeInfo;
// Mono.Security.Cryptography.DSAManaged
#include "mscorlib_Mono_Security_Cryptography_DSAManagedMethodDeclarations.h"
extern const Il2CppType KeyGeneratedEventHandler_t829_0_0_0;
static const Il2CppType* DSAManaged_t830_il2cpp_TypeInfo__nestedTypes[1] =
{
	&KeyGeneratedEventHandler_t829_0_0_0,
};
static const EncodedMethodIndex DSAManaged_t830_VTable[14] = 
{
	120,
	1511,
	122,
	123,
	828,
	1512,
	830,
	1513,
	1514,
	1515,
	1516,
	1517,
	1518,
	1519,
};
extern const Il2CppType IDisposable_t319_0_0_0;
static Il2CppInterfaceOffsetPair DSAManaged_t830_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSAManaged_t830_0_0_0;
extern const Il2CppType DSAManaged_t830_1_0_0;
extern const Il2CppType DSA_t561_0_0_0;
struct DSAManaged_t830;
const Il2CppTypeDefinitionMetadata DSAManaged_t830_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DSAManaged_t830_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DSAManaged_t830_InterfacesOffsets/* interfaceOffsets */
	, &DSA_t561_0_0_0/* parent */
	, DSAManaged_t830_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2778/* fieldStart */
	, 4471/* methodStart */
	, 5/* eventStart */
	, 827/* propertyStart */

};
TypeInfo DSAManaged_t830_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSAManaged"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSAManaged_t830_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DSAManaged_t830_0_0_0/* byval_arg */
	, &DSAManaged_t830_1_0_0/* this_arg */
	, &DSAManaged_t830_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSAManaged_t830)/* instance_size */
	, sizeof (DSAManaged_t830)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 3/* property_count */
	, 13/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Cryptography.DSAManaged/KeyGeneratedEventHandler
#include "mscorlib_Mono_Security_Cryptography_DSAManaged_KeyGeneratedE.h"
// Metadata Definition Mono.Security.Cryptography.DSAManaged/KeyGeneratedEventHandler
extern TypeInfo KeyGeneratedEventHandler_t829_il2cpp_TypeInfo;
// Mono.Security.Cryptography.DSAManaged/KeyGeneratedEventHandler
#include "mscorlib_Mono_Security_Cryptography_DSAManaged_KeyGeneratedEMethodDeclarations.h"
static const EncodedMethodIndex KeyGeneratedEventHandler_t829_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	1520,
	1521,
	1522,
};
extern const Il2CppType ICloneable_t2056_0_0_0;
extern const Il2CppType ISerializable_t1426_0_0_0;
static Il2CppInterfaceOffsetPair KeyGeneratedEventHandler_t829_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyGeneratedEventHandler_t829_1_0_0;
extern const Il2CppType MulticastDelegate_t47_0_0_0;
struct KeyGeneratedEventHandler_t829;
const Il2CppTypeDefinitionMetadata KeyGeneratedEventHandler_t829_DefinitionMetadata = 
{
	&DSAManaged_t830_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, KeyGeneratedEventHandler_t829_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, KeyGeneratedEventHandler_t829_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4488/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyGeneratedEventHandler_t829_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyGeneratedEventHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &KeyGeneratedEventHandler_t829_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyGeneratedEventHandler_t829_0_0_0/* byval_arg */
	, &KeyGeneratedEventHandler_t829_1_0_0/* this_arg */
	, &KeyGeneratedEventHandler_t829_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t829/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyGeneratedEventHandler_t829)/* instance_size */
	, sizeof (KeyGeneratedEventHandler_t829)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Cryptography.KeyPairPersistence
#include "mscorlib_Mono_Security_Cryptography_KeyPairPersistence.h"
// Metadata Definition Mono.Security.Cryptography.KeyPairPersistence
extern TypeInfo KeyPairPersistence_t831_il2cpp_TypeInfo;
// Mono.Security.Cryptography.KeyPairPersistence
#include "mscorlib_Mono_Security_Cryptography_KeyPairPersistenceMethodDeclarations.h"
static const EncodedMethodIndex KeyPairPersistence_t831_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyPairPersistence_t831_0_0_0;
extern const Il2CppType KeyPairPersistence_t831_1_0_0;
struct KeyPairPersistence_t831;
const Il2CppTypeDefinitionMetadata KeyPairPersistence_t831_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeyPairPersistence_t831_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2791/* fieldStart */
	, 4492/* methodStart */
	, -1/* eventStart */
	, 830/* propertyStart */

};
TypeInfo KeyPairPersistence_t831_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyPairPersistence"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &KeyPairPersistence_t831_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyPairPersistence_t831_0_0_0/* byval_arg */
	, &KeyPairPersistence_t831_1_0_0/* this_arg */
	, &KeyPairPersistence_t831_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyPairPersistence_t831)/* instance_size */
	, sizeof (KeyPairPersistence_t831)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(KeyPairPersistence_t831_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 8/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.MACAlgorithm
#include "mscorlib_Mono_Security_Cryptography_MACAlgorithm.h"
// Metadata Definition Mono.Security.Cryptography.MACAlgorithm
extern TypeInfo MACAlgorithm_t832_il2cpp_TypeInfo;
// Mono.Security.Cryptography.MACAlgorithm
#include "mscorlib_Mono_Security_Cryptography_MACAlgorithmMethodDeclarations.h"
static const EncodedMethodIndex MACAlgorithm_t832_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MACAlgorithm_t832_0_0_0;
extern const Il2CppType MACAlgorithm_t832_1_0_0;
struct MACAlgorithm_t832;
const Il2CppTypeDefinitionMetadata MACAlgorithm_t832_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MACAlgorithm_t832_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2800/* fieldStart */
	, 4520/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MACAlgorithm_t832_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MACAlgorithm"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &MACAlgorithm_t832_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MACAlgorithm_t832_0_0_0/* byval_arg */
	, &MACAlgorithm_t832_1_0_0/* this_arg */
	, &MACAlgorithm_t832_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MACAlgorithm_t832)/* instance_size */
	, sizeof (MACAlgorithm_t832)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.PKCS1
#include "mscorlib_Mono_Security_Cryptography_PKCS1.h"
// Metadata Definition Mono.Security.Cryptography.PKCS1
extern TypeInfo PKCS1_t833_il2cpp_TypeInfo;
// Mono.Security.Cryptography.PKCS1
#include "mscorlib_Mono_Security_Cryptography_PKCS1MethodDeclarations.h"
static const EncodedMethodIndex PKCS1_t833_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PKCS1_t833_0_0_0;
extern const Il2CppType PKCS1_t833_1_0_0;
struct PKCS1_t833;
const Il2CppTypeDefinitionMetadata PKCS1_t833_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PKCS1_t833_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2805/* fieldStart */
	, 4524/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PKCS1_t833_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PKCS1"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &PKCS1_t833_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PKCS1_t833_0_0_0/* byval_arg */
	, &PKCS1_t833_1_0_0/* this_arg */
	, &PKCS1_t833_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PKCS1_t833)/* instance_size */
	, sizeof (PKCS1_t833)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PKCS1_t833_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.PKCS8
#include "mscorlib_Mono_Security_Cryptography_PKCS8.h"
// Metadata Definition Mono.Security.Cryptography.PKCS8
extern TypeInfo PKCS8_t836_il2cpp_TypeInfo;
// Mono.Security.Cryptography.PKCS8
#include "mscorlib_Mono_Security_Cryptography_PKCS8MethodDeclarations.h"
extern const Il2CppType PrivateKeyInfo_t834_0_0_0;
extern const Il2CppType EncryptedPrivateKeyInfo_t835_0_0_0;
static const Il2CppType* PKCS8_t836_il2cpp_TypeInfo__nestedTypes[2] =
{
	&PrivateKeyInfo_t834_0_0_0,
	&EncryptedPrivateKeyInfo_t835_0_0_0,
};
static const EncodedMethodIndex PKCS8_t836_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PKCS8_t836_0_0_0;
extern const Il2CppType PKCS8_t836_1_0_0;
struct PKCS8_t836;
const Il2CppTypeDefinitionMetadata PKCS8_t836_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PKCS8_t836_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PKCS8_t836_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PKCS8_t836_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PKCS8"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &PKCS8_t836_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PKCS8_t836_0_0_0/* byval_arg */
	, &PKCS8_t836_1_0_0/* this_arg */
	, &PKCS8_t836_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PKCS8_t836)/* instance_size */
	, sizeof (PKCS8_t836)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
#include "mscorlib_Mono_Security_Cryptography_PKCS8_PrivateKeyInfo.h"
// Metadata Definition Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
extern TypeInfo PrivateKeyInfo_t834_il2cpp_TypeInfo;
// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
#include "mscorlib_Mono_Security_Cryptography_PKCS8_PrivateKeyInfoMethodDeclarations.h"
static const EncodedMethodIndex PrivateKeyInfo_t834_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrivateKeyInfo_t834_1_0_0;
struct PrivateKeyInfo_t834;
const Il2CppTypeDefinitionMetadata PrivateKeyInfo_t834_DefinitionMetadata = 
{
	&PKCS8_t836_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PrivateKeyInfo_t834_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2809/* fieldStart */
	, 4536/* methodStart */
	, -1/* eventStart */
	, 838/* propertyStart */

};
TypeInfo PrivateKeyInfo_t834_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrivateKeyInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PrivateKeyInfo_t834_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrivateKeyInfo_t834_0_0_0/* byval_arg */
	, &PrivateKeyInfo_t834_1_0_0/* this_arg */
	, &PrivateKeyInfo_t834_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrivateKeyInfo_t834)/* instance_size */
	, sizeof (PrivateKeyInfo_t834)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
#include "mscorlib_Mono_Security_Cryptography_PKCS8_EncryptedPrivateKe.h"
// Metadata Definition Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
extern TypeInfo EncryptedPrivateKeyInfo_t835_il2cpp_TypeInfo;
// Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
#include "mscorlib_Mono_Security_Cryptography_PKCS8_EncryptedPrivateKeMethodDeclarations.h"
static const EncodedMethodIndex EncryptedPrivateKeyInfo_t835_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncryptedPrivateKeyInfo_t835_1_0_0;
struct EncryptedPrivateKeyInfo_t835;
const Il2CppTypeDefinitionMetadata EncryptedPrivateKeyInfo_t835_DefinitionMetadata = 
{
	&PKCS8_t836_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EncryptedPrivateKeyInfo_t835_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2813/* fieldStart */
	, 4544/* methodStart */
	, -1/* eventStart */
	, 839/* propertyStart */

};
TypeInfo EncryptedPrivateKeyInfo_t835_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncryptedPrivateKeyInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &EncryptedPrivateKeyInfo_t835_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncryptedPrivateKeyInfo_t835_0_0_0/* byval_arg */
	, &EncryptedPrivateKeyInfo_t835_1_0_0/* this_arg */
	, &EncryptedPrivateKeyInfo_t835_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncryptedPrivateKeyInfo_t835)/* instance_size */
	, sizeof (EncryptedPrivateKeyInfo_t835)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.RSAManaged
#include "mscorlib_Mono_Security_Cryptography_RSAManaged.h"
// Metadata Definition Mono.Security.Cryptography.RSAManaged
extern TypeInfo RSAManaged_t838_il2cpp_TypeInfo;
// Mono.Security.Cryptography.RSAManaged
#include "mscorlib_Mono_Security_Cryptography_RSAManagedMethodDeclarations.h"
extern const Il2CppType KeyGeneratedEventHandler_t837_0_0_0;
static const Il2CppType* RSAManaged_t838_il2cpp_TypeInfo__nestedTypes[1] =
{
	&KeyGeneratedEventHandler_t837_0_0_0,
};
static const EncodedMethodIndex RSAManaged_t838_VTable[14] = 
{
	120,
	1523,
	122,
	123,
	828,
	1524,
	830,
	1525,
	832,
	1526,
	1527,
	1528,
	1529,
	1530,
};
static Il2CppInterfaceOffsetPair RSAManaged_t838_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSAManaged_t838_0_0_0;
extern const Il2CppType RSAManaged_t838_1_0_0;
extern const Il2CppType RSA_t562_0_0_0;
struct RSAManaged_t838;
const Il2CppTypeDefinitionMetadata RSAManaged_t838_DefinitionMetadata = 
{
	NULL/* declaringType */
	, RSAManaged_t838_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RSAManaged_t838_InterfacesOffsets/* interfaceOffsets */
	, &RSA_t562_0_0_0/* parent */
	, RSAManaged_t838_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2817/* fieldStart */
	, 4551/* methodStart */
	, 6/* eventStart */
	, 843/* propertyStart */

};
TypeInfo RSAManaged_t838_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAManaged"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAManaged_t838_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RSAManaged_t838_0_0_0/* byval_arg */
	, &RSAManaged_t838_1_0_0/* this_arg */
	, &RSAManaged_t838_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSAManaged_t838)/* instance_size */
	, sizeof (RSAManaged_t838)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 3/* property_count */
	, 13/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
#include "mscorlib_Mono_Security_Cryptography_RSAManaged_KeyGeneratedE.h"
// Metadata Definition Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
extern TypeInfo KeyGeneratedEventHandler_t837_il2cpp_TypeInfo;
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
#include "mscorlib_Mono_Security_Cryptography_RSAManaged_KeyGeneratedEMethodDeclarations.h"
static const EncodedMethodIndex KeyGeneratedEventHandler_t837_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	1531,
	1532,
	1533,
};
static Il2CppInterfaceOffsetPair KeyGeneratedEventHandler_t837_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyGeneratedEventHandler_t837_1_0_0;
struct KeyGeneratedEventHandler_t837;
const Il2CppTypeDefinitionMetadata KeyGeneratedEventHandler_t837_DefinitionMetadata = 
{
	&RSAManaged_t838_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, KeyGeneratedEventHandler_t837_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, KeyGeneratedEventHandler_t837_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4566/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyGeneratedEventHandler_t837_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyGeneratedEventHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &KeyGeneratedEventHandler_t837_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyGeneratedEventHandler_t837_0_0_0/* byval_arg */
	, &KeyGeneratedEventHandler_t837_1_0_0/* this_arg */
	, &KeyGeneratedEventHandler_t837_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t837/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyGeneratedEventHandler_t837)/* instance_size */
	, sizeof (KeyGeneratedEventHandler_t837)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Cryptography.SymmetricTransform
#include "mscorlib_Mono_Security_Cryptography_SymmetricTransform.h"
// Metadata Definition Mono.Security.Cryptography.SymmetricTransform
extern TypeInfo SymmetricTransform_t839_il2cpp_TypeInfo;
// Mono.Security.Cryptography.SymmetricTransform
#include "mscorlib_Mono_Security_Cryptography_SymmetricTransformMethodDeclarations.h"
static const EncodedMethodIndex SymmetricTransform_t839_VTable[18] = 
{
	120,
	1534,
	122,
	123,
	1535,
	1536,
	1537,
	1538,
	1539,
	1536,
	1540,
	0,
	1541,
	1542,
	1543,
	1544,
	1537,
	1538,
};
extern const Il2CppType ICryptoTransform_t616_0_0_0;
static const Il2CppType* SymmetricTransform_t839_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
	&ICryptoTransform_t616_0_0_0,
};
static Il2CppInterfaceOffsetPair SymmetricTransform_t839_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SymmetricTransform_t839_0_0_0;
extern const Il2CppType SymmetricTransform_t839_1_0_0;
struct SymmetricTransform_t839;
const Il2CppTypeDefinitionMetadata SymmetricTransform_t839_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SymmetricTransform_t839_InterfacesTypeInfos/* implementedInterfaces */
	, SymmetricTransform_t839_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SymmetricTransform_t839_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2830/* fieldStart */
	, 4570/* methodStart */
	, -1/* eventStart */
	, 846/* propertyStart */

};
TypeInfo SymmetricTransform_t839_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SymmetricTransform"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SymmetricTransform_t839_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SymmetricTransform_t839_0_0_0/* byval_arg */
	, &SymmetricTransform_t839_1_0_0/* this_arg */
	, &SymmetricTransform_t839_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SymmetricTransform_t839)/* instance_size */
	, sizeof (SymmetricTransform_t839)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 20/* method_count */
	, 2/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.X509.SafeBag
#include "mscorlib_Mono_Security_X509_SafeBag.h"
// Metadata Definition Mono.Security.X509.SafeBag
extern TypeInfo SafeBag_t841_il2cpp_TypeInfo;
// Mono.Security.X509.SafeBag
#include "mscorlib_Mono_Security_X509_SafeBagMethodDeclarations.h"
static const EncodedMethodIndex SafeBag_t841_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SafeBag_t841_0_0_0;
extern const Il2CppType SafeBag_t841_1_0_0;
struct SafeBag_t841;
const Il2CppTypeDefinitionMetadata SafeBag_t841_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SafeBag_t841_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2842/* fieldStart */
	, 4590/* methodStart */
	, -1/* eventStart */
	, 848/* propertyStart */

};
TypeInfo SafeBag_t841_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SafeBag"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &SafeBag_t841_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SafeBag_t841_0_0_0/* byval_arg */
	, &SafeBag_t841_1_0_0/* this_arg */
	, &SafeBag_t841_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SafeBag_t841)/* instance_size */
	, sizeof (SafeBag_t841)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.PKCS12
#include "mscorlib_Mono_Security_X509_PKCS12.h"
// Metadata Definition Mono.Security.X509.PKCS12
extern TypeInfo PKCS12_t844_il2cpp_TypeInfo;
// Mono.Security.X509.PKCS12
#include "mscorlib_Mono_Security_X509_PKCS12MethodDeclarations.h"
extern const Il2CppType DeriveBytes_t842_0_0_0;
static const Il2CppType* PKCS12_t844_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DeriveBytes_t842_0_0_0,
};
static const EncodedMethodIndex PKCS12_t844_VTable[4] = 
{
	120,
	1545,
	122,
	123,
};
static const Il2CppType* PKCS12_t844_InterfacesTypeInfos[] = 
{
	&ICloneable_t2056_0_0_0,
};
static Il2CppInterfaceOffsetPair PKCS12_t844_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PKCS12_t844_0_0_0;
extern const Il2CppType PKCS12_t844_1_0_0;
struct PKCS12_t844;
const Il2CppTypeDefinitionMetadata PKCS12_t844_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PKCS12_t844_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, PKCS12_t844_InterfacesTypeInfos/* implementedInterfaces */
	, PKCS12_t844_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PKCS12_t844_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2844/* fieldStart */
	, 4593/* methodStart */
	, -1/* eventStart */
	, 850/* propertyStart */

};
TypeInfo PKCS12_t844_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PKCS12"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &PKCS12_t844_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PKCS12_t844_0_0_0/* byval_arg */
	, &PKCS12_t844_1_0_0/* this_arg */
	, &PKCS12_t844_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PKCS12_t844)/* instance_size */
	, sizeof (PKCS12_t844)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PKCS12_t844_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 3/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.X509.PKCS12/DeriveBytes
#include "mscorlib_Mono_Security_X509_PKCS12_DeriveBytes.h"
// Metadata Definition Mono.Security.X509.PKCS12/DeriveBytes
extern TypeInfo DeriveBytes_t842_il2cpp_TypeInfo;
// Mono.Security.X509.PKCS12/DeriveBytes
#include "mscorlib_Mono_Security_X509_PKCS12_DeriveBytesMethodDeclarations.h"
static const EncodedMethodIndex DeriveBytes_t842_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DeriveBytes_t842_1_0_0;
struct DeriveBytes_t842;
const Il2CppTypeDefinitionMetadata DeriveBytes_t842_DefinitionMetadata = 
{
	&PKCS12_t844_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DeriveBytes_t842_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2859/* fieldStart */
	, 4610/* methodStart */
	, -1/* eventStart */
	, 853/* propertyStart */

};
TypeInfo DeriveBytes_t842_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DeriveBytes"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DeriveBytes_t842_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DeriveBytes_t842_0_0_0/* byval_arg */
	, &DeriveBytes_t842_1_0_0/* this_arg */
	, &DeriveBytes_t842_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DeriveBytes_t842)/* instance_size */
	, sizeof (DeriveBytes_t842)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DeriveBytes_t842_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 4/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X501
#include "mscorlib_Mono_Security_X509_X501.h"
// Metadata Definition Mono.Security.X509.X501
extern TypeInfo X501_t845_il2cpp_TypeInfo;
// Mono.Security.X509.X501
#include "mscorlib_Mono_Security_X509_X501MethodDeclarations.h"
static const EncodedMethodIndex X501_t845_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X501_t845_0_0_0;
extern const Il2CppType X501_t845_1_0_0;
struct X501_t845;
const Il2CppTypeDefinitionMetadata X501_t845_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X501_t845_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2866/* fieldStart */
	, 4621/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X501_t845_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X501"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X501_t845_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X501_t845_0_0_0/* byval_arg */
	, &X501_t845_1_0_0/* this_arg */
	, &X501_t845_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X501_t845)/* instance_size */
	, sizeof (X501_t845)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X501_t845_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X509Certificate
#include "mscorlib_Mono_Security_X509_X509Certificate.h"
// Metadata Definition Mono.Security.X509.X509Certificate
extern TypeInfo X509Certificate_t847_il2cpp_TypeInfo;
// Mono.Security.X509.X509Certificate
#include "mscorlib_Mono_Security_X509_X509CertificateMethodDeclarations.h"
static const EncodedMethodIndex X509Certificate_t847_VTable[13] = 
{
	120,
	125,
	122,
	123,
	1546,
	1547,
	1548,
	1549,
	1550,
	1551,
	1552,
	1553,
	1546,
};
static const Il2CppType* X509Certificate_t847_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair X509Certificate_t847_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509Certificate_t847_0_0_0;
extern const Il2CppType X509Certificate_t847_1_0_0;
struct X509Certificate_t847;
const Il2CppTypeDefinitionMetadata X509Certificate_t847_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509Certificate_t847_InterfacesTypeInfos/* implementedInterfaces */
	, X509Certificate_t847_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Certificate_t847_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2881/* fieldStart */
	, 4625/* methodStart */
	, -1/* eventStart */
	, 857/* propertyStart */

};
TypeInfo X509Certificate_t847_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Certificate"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509Certificate_t847_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Certificate_t847_0_0_0/* byval_arg */
	, &X509Certificate_t847_1_0_0/* this_arg */
	, &X509Certificate_t847_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Certificate_t847)/* instance_size */
	, sizeof (X509Certificate_t847)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509Certificate_t847_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 8/* property_count */
	, 21/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.X509.X509CertificateCollection
#include "mscorlib_Mono_Security_X509_X509CertificateCollection.h"
// Metadata Definition Mono.Security.X509.X509CertificateCollection
extern TypeInfo X509CertificateCollection_t843_il2cpp_TypeInfo;
// Mono.Security.X509.X509CertificateCollection
#include "mscorlib_Mono_Security_X509_X509CertificateCollectionMethodDeclarations.h"
extern const Il2CppType X509CertificateEnumerator_t848_0_0_0;
static const Il2CppType* X509CertificateCollection_t843_il2cpp_TypeInfo__nestedTypes[1] =
{
	&X509CertificateEnumerator_t848_0_0_0,
};
static const EncodedMethodIndex X509CertificateCollection_t843_VTable[26] = 
{
	120,
	125,
	1554,
	123,
	1555,
	565,
	566,
	567,
	568,
	569,
	570,
	571,
	572,
	573,
	574,
	575,
	576,
	577,
	578,
	579,
	580,
	581,
	582,
	583,
	584,
	585,
};
extern const Il2CppType IEnumerable_t302_0_0_0;
static const Il2CppType* X509CertificateCollection_t843_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
};
extern const Il2CppType ICollection_t569_0_0_0;
extern const Il2CppType IList_t519_0_0_0;
static Il2CppInterfaceOffsetPair X509CertificateCollection_t843_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509CertificateCollection_t843_0_0_0;
extern const Il2CppType X509CertificateCollection_t843_1_0_0;
extern const Il2CppType CollectionBase_t453_0_0_0;
struct X509CertificateCollection_t843;
const Il2CppTypeDefinitionMetadata X509CertificateCollection_t843_DefinitionMetadata = 
{
	NULL/* declaringType */
	, X509CertificateCollection_t843_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, X509CertificateCollection_t843_InterfacesTypeInfos/* implementedInterfaces */
	, X509CertificateCollection_t843_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t453_0_0_0/* parent */
	, X509CertificateCollection_t843_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4641/* methodStart */
	, -1/* eventStart */
	, 865/* propertyStart */

};
TypeInfo X509CertificateCollection_t843_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509CertificateCollection"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509CertificateCollection_t843_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 806/* custom_attributes_cache */
	, &X509CertificateCollection_t843_0_0_0/* byval_arg */
	, &X509CertificateCollection_t843_1_0_0/* this_arg */
	, &X509CertificateCollection_t843_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509CertificateCollection_t843)/* instance_size */
	, sizeof (X509CertificateCollection_t843)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 26/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
#include "mscorlib_Mono_Security_X509_X509CertificateCollection_X509Ce.h"
// Metadata Definition Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
extern TypeInfo X509CertificateEnumerator_t848_il2cpp_TypeInfo;
// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
#include "mscorlib_Mono_Security_X509_X509CertificateCollection_X509CeMethodDeclarations.h"
static const EncodedMethodIndex X509CertificateEnumerator_t848_VTable[7] = 
{
	120,
	125,
	122,
	123,
	1556,
	1557,
	1558,
};
extern const Il2CppType IEnumerator_t279_0_0_0;
static const Il2CppType* X509CertificateEnumerator_t848_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair X509CertificateEnumerator_t848_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509CertificateEnumerator_t848_1_0_0;
struct X509CertificateEnumerator_t848;
const Il2CppTypeDefinitionMetadata X509CertificateEnumerator_t848_DefinitionMetadata = 
{
	&X509CertificateCollection_t843_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, X509CertificateEnumerator_t848_InterfacesTypeInfos/* implementedInterfaces */
	, X509CertificateEnumerator_t848_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509CertificateEnumerator_t848_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2902/* fieldStart */
	, 4647/* methodStart */
	, -1/* eventStart */
	, 866/* propertyStart */

};
TypeInfo X509CertificateEnumerator_t848_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509CertificateEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &X509CertificateEnumerator_t848_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509CertificateEnumerator_t848_0_0_0/* byval_arg */
	, &X509CertificateEnumerator_t848_1_0_0/* this_arg */
	, &X509CertificateEnumerator_t848_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509CertificateEnumerator_t848)/* instance_size */
	, sizeof (X509CertificateEnumerator_t848)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.X509.X509Extension
#include "mscorlib_Mono_Security_X509_X509Extension.h"
// Metadata Definition Mono.Security.X509.X509Extension
extern TypeInfo X509Extension_t849_il2cpp_TypeInfo;
// Mono.Security.X509.X509Extension
#include "mscorlib_Mono_Security_X509_X509ExtensionMethodDeclarations.h"
static const EncodedMethodIndex X509Extension_t849_VTable[5] = 
{
	1559,
	125,
	1560,
	1561,
	1562,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509Extension_t849_0_0_0;
extern const Il2CppType X509Extension_t849_1_0_0;
struct X509Extension_t849;
const Il2CppTypeDefinitionMetadata X509Extension_t849_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Extension_t849_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2903/* fieldStart */
	, 4652/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509Extension_t849_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Extension"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509Extension_t849_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Extension_t849_0_0_0/* byval_arg */
	, &X509Extension_t849_1_0_0/* this_arg */
	, &X509Extension_t849_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Extension_t849)/* instance_size */
	, sizeof (X509Extension_t849)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X509ExtensionCollection
#include "mscorlib_Mono_Security_X509_X509ExtensionCollection.h"
// Metadata Definition Mono.Security.X509.X509ExtensionCollection
extern TypeInfo X509ExtensionCollection_t846_il2cpp_TypeInfo;
// Mono.Security.X509.X509ExtensionCollection
#include "mscorlib_Mono_Security_X509_X509ExtensionCollectionMethodDeclarations.h"
static const EncodedMethodIndex X509ExtensionCollection_t846_VTable[26] = 
{
	120,
	125,
	122,
	123,
	1563,
	565,
	566,
	567,
	568,
	569,
	570,
	571,
	572,
	573,
	574,
	575,
	576,
	577,
	578,
	579,
	580,
	581,
	582,
	583,
	584,
	585,
};
static const Il2CppType* X509ExtensionCollection_t846_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
};
static Il2CppInterfaceOffsetPair X509ExtensionCollection_t846_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509ExtensionCollection_t846_0_0_0;
extern const Il2CppType X509ExtensionCollection_t846_1_0_0;
struct X509ExtensionCollection_t846;
const Il2CppTypeDefinitionMetadata X509ExtensionCollection_t846_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509ExtensionCollection_t846_InterfacesTypeInfos/* implementedInterfaces */
	, X509ExtensionCollection_t846_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t453_0_0_0/* parent */
	, X509ExtensionCollection_t846_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2906/* fieldStart */
	, 4658/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509ExtensionCollection_t846_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ExtensionCollection"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509ExtensionCollection_t846_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 807/* custom_attributes_cache */
	, &X509ExtensionCollection_t846_0_0_0/* byval_arg */
	, &X509ExtensionCollection_t846_1_0_0/* this_arg */
	, &X509ExtensionCollection_t846_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ExtensionCollection_t846)/* instance_size */
	, sizeof (X509ExtensionCollection_t846)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.ASN1
#include "mscorlib_Mono_Security_ASN1.h"
// Metadata Definition Mono.Security.ASN1
extern TypeInfo ASN1_t840_il2cpp_TypeInfo;
// Mono.Security.ASN1
#include "mscorlib_Mono_Security_ASN1MethodDeclarations.h"
static const EncodedMethodIndex ASN1_t840_VTable[5] = 
{
	120,
	125,
	122,
	1564,
	1565,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ASN1_t840_0_0_0;
extern const Il2CppType ASN1_t840_1_0_0;
struct ASN1_t840;
const Il2CppTypeDefinitionMetadata ASN1_t840_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ASN1_t840_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2907/* fieldStart */
	, 4661/* methodStart */
	, -1/* eventStart */
	, 868/* propertyStart */

};
TypeInfo ASN1_t840_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ASN1"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &ASN1_t840_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 808/* custom_attributes_cache */
	, &ASN1_t840_0_0_0/* byval_arg */
	, &ASN1_t840_1_0_0/* this_arg */
	, &ASN1_t840_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ASN1_t840)/* instance_size */
	, sizeof (ASN1_t840)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.ASN1Convert
#include "mscorlib_Mono_Security_ASN1Convert.h"
// Metadata Definition Mono.Security.ASN1Convert
extern TypeInfo ASN1Convert_t850_il2cpp_TypeInfo;
// Mono.Security.ASN1Convert
#include "mscorlib_Mono_Security_ASN1ConvertMethodDeclarations.h"
static const EncodedMethodIndex ASN1Convert_t850_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ASN1Convert_t850_0_0_0;
extern const Il2CppType ASN1Convert_t850_1_0_0;
struct ASN1Convert_t850;
const Il2CppTypeDefinitionMetadata ASN1Convert_t850_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ASN1Convert_t850_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4678/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ASN1Convert_t850_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ASN1Convert"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &ASN1Convert_t850_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ASN1Convert_t850_0_0_0/* byval_arg */
	, &ASN1Convert_t850_1_0_0/* this_arg */
	, &ASN1Convert_t850_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ASN1Convert_t850)/* instance_size */
	, sizeof (ASN1Convert_t850)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.BitConverterLE
#include "mscorlib_Mono_Security_BitConverterLE.h"
// Metadata Definition Mono.Security.BitConverterLE
extern TypeInfo BitConverterLE_t851_il2cpp_TypeInfo;
// Mono.Security.BitConverterLE
#include "mscorlib_Mono_Security_BitConverterLEMethodDeclarations.h"
static const EncodedMethodIndex BitConverterLE_t851_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BitConverterLE_t851_0_0_0;
extern const Il2CppType BitConverterLE_t851_1_0_0;
struct BitConverterLE_t851;
const Il2CppTypeDefinitionMetadata BitConverterLE_t851_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BitConverterLE_t851_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4681/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BitConverterLE_t851_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BitConverterLE"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &BitConverterLE_t851_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BitConverterLE_t851_0_0_0/* byval_arg */
	, &BitConverterLE_t851_1_0_0/* this_arg */
	, &BitConverterLE_t851_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BitConverterLE_t851)/* instance_size */
	, sizeof (BitConverterLE_t851)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.PKCS7
#include "mscorlib_Mono_Security_PKCS7.h"
// Metadata Definition Mono.Security.PKCS7
extern TypeInfo PKCS7_t854_il2cpp_TypeInfo;
// Mono.Security.PKCS7
#include "mscorlib_Mono_Security_PKCS7MethodDeclarations.h"
extern const Il2CppType ContentInfo_t852_0_0_0;
extern const Il2CppType EncryptedData_t853_0_0_0;
static const Il2CppType* PKCS7_t854_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ContentInfo_t852_0_0_0,
	&EncryptedData_t853_0_0_0,
};
static const EncodedMethodIndex PKCS7_t854_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PKCS7_t854_0_0_0;
extern const Il2CppType PKCS7_t854_1_0_0;
struct PKCS7_t854;
const Il2CppTypeDefinitionMetadata PKCS7_t854_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PKCS7_t854_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PKCS7_t854_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PKCS7_t854_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PKCS7"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &PKCS7_t854_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PKCS7_t854_0_0_0/* byval_arg */
	, &PKCS7_t854_1_0_0/* this_arg */
	, &PKCS7_t854_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PKCS7_t854)/* instance_size */
	, sizeof (PKCS7_t854)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.PKCS7/ContentInfo
#include "mscorlib_Mono_Security_PKCS7_ContentInfo.h"
// Metadata Definition Mono.Security.PKCS7/ContentInfo
extern TypeInfo ContentInfo_t852_il2cpp_TypeInfo;
// Mono.Security.PKCS7/ContentInfo
#include "mscorlib_Mono_Security_PKCS7_ContentInfoMethodDeclarations.h"
static const EncodedMethodIndex ContentInfo_t852_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContentInfo_t852_1_0_0;
struct ContentInfo_t852;
const Il2CppTypeDefinitionMetadata ContentInfo_t852_DefinitionMetadata = 
{
	&PKCS7_t854_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ContentInfo_t852_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2910/* fieldStart */
	, 4690/* methodStart */
	, -1/* eventStart */
	, 873/* propertyStart */

};
TypeInfo ContentInfo_t852_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContentInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ContentInfo_t852_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContentInfo_t852_0_0_0/* byval_arg */
	, &ContentInfo_t852_1_0_0/* this_arg */
	, &ContentInfo_t852_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContentInfo_t852)/* instance_size */
	, sizeof (ContentInfo_t852)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.PKCS7/EncryptedData
#include "mscorlib_Mono_Security_PKCS7_EncryptedData.h"
// Metadata Definition Mono.Security.PKCS7/EncryptedData
extern TypeInfo EncryptedData_t853_il2cpp_TypeInfo;
// Mono.Security.PKCS7/EncryptedData
#include "mscorlib_Mono_Security_PKCS7_EncryptedDataMethodDeclarations.h"
static const EncodedMethodIndex EncryptedData_t853_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncryptedData_t853_1_0_0;
struct EncryptedData_t853;
const Il2CppTypeDefinitionMetadata EncryptedData_t853_DefinitionMetadata = 
{
	&PKCS7_t854_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EncryptedData_t853_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2912/* fieldStart */
	, 4697/* methodStart */
	, -1/* eventStart */
	, 875/* propertyStart */

};
TypeInfo EncryptedData_t853_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncryptedData"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &EncryptedData_t853_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncryptedData_t853_0_0_0/* byval_arg */
	, &EncryptedData_t853_1_0_0/* this_arg */
	, &EncryptedData_t853_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncryptedData_t853)/* instance_size */
	, sizeof (EncryptedData_t853)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.StrongName
#include "mscorlib_Mono_Security_StrongName.h"
// Metadata Definition Mono.Security.StrongName
extern TypeInfo StrongName_t855_il2cpp_TypeInfo;
// Mono.Security.StrongName
#include "mscorlib_Mono_Security_StrongNameMethodDeclarations.h"
static const EncodedMethodIndex StrongName_t855_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StrongName_t855_0_0_0;
extern const Il2CppType StrongName_t855_1_0_0;
struct StrongName_t855;
const Il2CppTypeDefinitionMetadata StrongName_t855_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StrongName_t855_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2916/* fieldStart */
	, 4701/* methodStart */
	, -1/* eventStart */
	, 877/* propertyStart */

};
TypeInfo StrongName_t855_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StrongName"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &StrongName_t855_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StrongName_t855_0_0_0/* byval_arg */
	, &StrongName_t855_1_0_0/* this_arg */
	, &StrongName_t855_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StrongName_t855)/* instance_size */
	, sizeof (StrongName_t855)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StrongName_t855_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Xml.SecurityParser
#include "mscorlib_Mono_Xml_SecurityParser.h"
// Metadata Definition Mono.Xml.SecurityParser
extern TypeInfo SecurityParser_t857_il2cpp_TypeInfo;
// Mono.Xml.SecurityParser
#include "mscorlib_Mono_Xml_SecurityParserMethodDeclarations.h"
static const EncodedMethodIndex SecurityParser_t857_VTable[11] = 
{
	120,
	125,
	122,
	123,
	1566,
	1567,
	1568,
	1569,
	1570,
	1571,
	1572,
};
extern const Il2CppType IContentHandler_t860_0_0_0;
static const Il2CppType* SecurityParser_t857_InterfacesTypeInfos[] = 
{
	&IContentHandler_t860_0_0_0,
};
static Il2CppInterfaceOffsetPair SecurityParser_t857_InterfacesOffsets[] = 
{
	{ &IContentHandler_t860_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityParser_t857_0_0_0;
extern const Il2CppType SecurityParser_t857_1_0_0;
extern const Il2CppType SmallXmlParser_t858_0_0_0;
struct SecurityParser_t857;
const Il2CppTypeDefinitionMetadata SecurityParser_t857_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SecurityParser_t857_InterfacesTypeInfos/* implementedInterfaces */
	, SecurityParser_t857_InterfacesOffsets/* interfaceOffsets */
	, &SmallXmlParser_t858_0_0_0/* parent */
	, SecurityParser_t857_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2922/* fieldStart */
	, 4705/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SecurityParser_t857_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityParser"/* name */
	, "Mono.Xml"/* namespaze */
	, NULL/* methods */
	, &SecurityParser_t857_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SecurityParser_t857_0_0_0/* byval_arg */
	, &SecurityParser_t857_1_0_0/* this_arg */
	, &SecurityParser_t857_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityParser_t857)/* instance_size */
	, sizeof (SecurityParser_t857)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Xml.SmallXmlParser
#include "mscorlib_Mono_Xml_SmallXmlParser.h"
// Metadata Definition Mono.Xml.SmallXmlParser
extern TypeInfo SmallXmlParser_t858_il2cpp_TypeInfo;
// Mono.Xml.SmallXmlParser
#include "mscorlib_Mono_Xml_SmallXmlParserMethodDeclarations.h"
extern const Il2CppType IAttrList_t1394_0_0_0;
extern const Il2CppType AttrListImpl_t859_0_0_0;
static const Il2CppType* SmallXmlParser_t858_il2cpp_TypeInfo__nestedTypes[3] =
{
	&IContentHandler_t860_0_0_0,
	&IAttrList_t1394_0_0_0,
	&AttrListImpl_t859_0_0_0,
};
static const EncodedMethodIndex SmallXmlParser_t858_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SmallXmlParser_t858_1_0_0;
struct SmallXmlParser_t858;
const Il2CppTypeDefinitionMetadata SmallXmlParser_t858_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SmallXmlParser_t858_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SmallXmlParser_t858_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2925/* fieldStart */
	, 4715/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SmallXmlParser_t858_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SmallXmlParser"/* name */
	, "Mono.Xml"/* namespaze */
	, NULL/* methods */
	, &SmallXmlParser_t858_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SmallXmlParser_t858_0_0_0/* byval_arg */
	, &SmallXmlParser_t858_1_0_0/* this_arg */
	, &SmallXmlParser_t858_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SmallXmlParser_t858)/* instance_size */
	, sizeof (SmallXmlParser_t858)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SmallXmlParser_t858_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 23/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition Mono.Xml.SmallXmlParser/IContentHandler
extern TypeInfo IContentHandler_t860_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContentHandler_t860_1_0_0;
struct IContentHandler_t860;
const Il2CppTypeDefinitionMetadata IContentHandler_t860_DefinitionMetadata = 
{
	&SmallXmlParser_t858_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4738/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IContentHandler_t860_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContentHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &IContentHandler_t860_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IContentHandler_t860_0_0_0/* byval_arg */
	, &IContentHandler_t860_1_0_0/* this_arg */
	, &IContentHandler_t860_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 162/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition Mono.Xml.SmallXmlParser/IAttrList
extern TypeInfo IAttrList_t1394_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IAttrList_t1394_1_0_0;
struct IAttrList_t1394;
const Il2CppTypeDefinitionMetadata IAttrList_t1394_DefinitionMetadata = 
{
	&SmallXmlParser_t858_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4745/* methodStart */
	, -1/* eventStart */
	, 880/* propertyStart */

};
TypeInfo IAttrList_t1394_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAttrList"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &IAttrList_t1394_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAttrList_t1394_0_0_0/* byval_arg */
	, &IAttrList_t1394_1_0_0/* this_arg */
	, &IAttrList_t1394_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 162/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Xml.SmallXmlParser/AttrListImpl
#include "mscorlib_Mono_Xml_SmallXmlParser_AttrListImpl.h"
// Metadata Definition Mono.Xml.SmallXmlParser/AttrListImpl
extern TypeInfo AttrListImpl_t859_il2cpp_TypeInfo;
// Mono.Xml.SmallXmlParser/AttrListImpl
#include "mscorlib_Mono_Xml_SmallXmlParser_AttrListImplMethodDeclarations.h"
static const EncodedMethodIndex AttrListImpl_t859_VTable[10] = 
{
	120,
	125,
	122,
	123,
	1573,
	1574,
	1575,
	1576,
	1577,
	1578,
};
static const Il2CppType* AttrListImpl_t859_InterfacesTypeInfos[] = 
{
	&IAttrList_t1394_0_0_0,
};
static Il2CppInterfaceOffsetPair AttrListImpl_t859_InterfacesOffsets[] = 
{
	{ &IAttrList_t1394_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AttrListImpl_t859_1_0_0;
struct AttrListImpl_t859;
const Il2CppTypeDefinitionMetadata AttrListImpl_t859_DefinitionMetadata = 
{
	&SmallXmlParser_t858_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, AttrListImpl_t859_InterfacesTypeInfos/* implementedInterfaces */
	, AttrListImpl_t859_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttrListImpl_t859_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2938/* fieldStart */
	, 4751/* methodStart */
	, -1/* eventStart */
	, 883/* propertyStart */

};
TypeInfo AttrListImpl_t859_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttrListImpl"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &AttrListImpl_t859_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttrListImpl_t859_0_0_0/* byval_arg */
	, &AttrListImpl_t859_1_0_0/* this_arg */
	, &AttrListImpl_t859_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttrListImpl_t859)/* instance_size */
	, sizeof (AttrListImpl_t859)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Xml.SmallXmlParserException
#include "mscorlib_Mono_Xml_SmallXmlParserException.h"
// Metadata Definition Mono.Xml.SmallXmlParserException
extern TypeInfo SmallXmlParserException_t862_il2cpp_TypeInfo;
// Mono.Xml.SmallXmlParserException
#include "mscorlib_Mono_Xml_SmallXmlParserExceptionMethodDeclarations.h"
static const EncodedMethodIndex SmallXmlParserException_t862_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
extern const Il2CppType _Exception_t2066_0_0_0;
static Il2CppInterfaceOffsetPair SmallXmlParserException_t862_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SmallXmlParserException_t862_0_0_0;
extern const Il2CppType SmallXmlParserException_t862_1_0_0;
extern const Il2CppType SystemException_t597_0_0_0;
struct SmallXmlParserException_t862;
const Il2CppTypeDefinitionMetadata SmallXmlParserException_t862_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SmallXmlParserException_t862_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, SmallXmlParserException_t862_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2940/* fieldStart */
	, 4760/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SmallXmlParserException_t862_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SmallXmlParserException"/* name */
	, "Mono.Xml"/* namespaze */
	, NULL/* methods */
	, &SmallXmlParserException_t862_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SmallXmlParserException_t862_0_0_0/* byval_arg */
	, &SmallXmlParserException_t862_1_0_0/* this_arg */
	, &SmallXmlParserException_t862_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SmallXmlParserException_t862)/* instance_size */
	, sizeof (SmallXmlParserException_t862)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Runtime
#include "mscorlib_Mono_Runtime.h"
// Metadata Definition Mono.Runtime
extern TypeInfo Runtime_t863_il2cpp_TypeInfo;
// Mono.Runtime
#include "mscorlib_Mono_RuntimeMethodDeclarations.h"
static const EncodedMethodIndex Runtime_t863_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Runtime_t863_0_0_0;
extern const Il2CppType Runtime_t863_1_0_0;
struct Runtime_t863;
const Il2CppTypeDefinitionMetadata Runtime_t863_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Runtime_t863_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4761/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Runtime_t863_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Runtime"/* name */
	, "Mono"/* namespaze */
	, NULL/* methods */
	, &Runtime_t863_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Runtime_t863_0_0_0/* byval_arg */
	, &Runtime_t863_1_0_0/* this_arg */
	, &Runtime_t863_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Runtime_t863)/* instance_size */
	, sizeof (Runtime_t863)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.CollectionDebuggerView`1
extern TypeInfo CollectionDebuggerView_1_t2067_il2cpp_TypeInfo;
static const EncodedMethodIndex CollectionDebuggerView_1_t2067_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CollectionDebuggerView_1_t2067_0_0_0;
extern const Il2CppType CollectionDebuggerView_1_t2067_1_0_0;
struct CollectionDebuggerView_1_t2067;
const Il2CppTypeDefinitionMetadata CollectionDebuggerView_1_t2067_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CollectionDebuggerView_1_t2067_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CollectionDebuggerView_1_t2067_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CollectionDebuggerView`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &CollectionDebuggerView_1_t2067_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CollectionDebuggerView_1_t2067_0_0_0/* byval_arg */
	, &CollectionDebuggerView_1_t2067_1_0_0/* this_arg */
	, &CollectionDebuggerView_1_t2067_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 78/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.CollectionDebuggerView`2
extern TypeInfo CollectionDebuggerView_2_t2068_il2cpp_TypeInfo;
static const EncodedMethodIndex CollectionDebuggerView_2_t2068_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CollectionDebuggerView_2_t2068_0_0_0;
extern const Il2CppType CollectionDebuggerView_2_t2068_1_0_0;
struct CollectionDebuggerView_2_t2068;
const Il2CppTypeDefinitionMetadata CollectionDebuggerView_2_t2068_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CollectionDebuggerView_2_t2068_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CollectionDebuggerView_2_t2068_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CollectionDebuggerView`2"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &CollectionDebuggerView_2_t2068_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CollectionDebuggerView_2_t2068_0_0_0/* byval_arg */
	, &CollectionDebuggerView_2_t2068_1_0_0/* this_arg */
	, &CollectionDebuggerView_2_t2068_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 79/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Comparer`1
extern TypeInfo Comparer_1_t2069_il2cpp_TypeInfo;
extern const Il2CppType DefaultComparer_t2070_0_0_0;
static const Il2CppType* Comparer_1_t2069_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DefaultComparer_t2070_0_0_0,
};
static const EncodedMethodIndex Comparer_1_t2069_VTable[7] = 
{
	120,
	125,
	122,
	123,
	1579,
	1580,
	0,
};
extern const Il2CppType IComparer_1_t2277_0_0_0;
extern const Il2CppType IComparer_t390_0_0_0;
static const Il2CppType* Comparer_1_t2069_InterfacesTypeInfos[] = 
{
	&IComparer_1_t2277_0_0_0,
	&IComparer_t390_0_0_0,
};
static Il2CppInterfaceOffsetPair Comparer_1_t2069_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2277_0_0_0, 4},
	{ &IComparer_t390_0_0_0, 5},
};
extern const Il2CppType IComparable_1_t2278_0_0_0;
extern const Il2CppType Comparer_1_t2069_gp_0_0_0_0;
extern const Il2CppType Comparer_1_t2280_0_0_0;
extern const Il2CppType DefaultComparer_t2281_0_0_0;
extern const Il2CppRGCTXDefinition Comparer_1_t2069_RGCTXData[9] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4588 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 3121 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3122 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3122 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4589 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2610 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3121 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2611 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Comparer_1_t2069_0_0_0;
extern const Il2CppType Comparer_1_t2069_1_0_0;
struct Comparer_1_t2069;
const Il2CppTypeDefinitionMetadata Comparer_1_t2069_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Comparer_1_t2069_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Comparer_1_t2069_InterfacesTypeInfos/* implementedInterfaces */
	, Comparer_1_t2069_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Comparer_1_t2069_VTable/* vtableMethods */
	, Comparer_1_t2069_RGCTXData/* rgctxDefinition */
	, 2942/* fieldStart */
	, 4762/* methodStart */
	, -1/* eventStart */
	, 886/* propertyStart */

};
TypeInfo Comparer_1_t2069_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &Comparer_1_t2069_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Comparer_1_t2069_0_0_0/* byval_arg */
	, &Comparer_1_t2069_1_0_0/* this_arg */
	, &Comparer_1_t2069_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 80/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 7/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer
extern TypeInfo DefaultComparer_t2070_il2cpp_TypeInfo;
static const EncodedMethodIndex DefaultComparer_t2070_VTable[7] = 
{
	120,
	125,
	122,
	123,
	1581,
	2147485230,
	1581,
};
extern const Il2CppType IComparer_1_t2282_0_0_0;
static Il2CppInterfaceOffsetPair DefaultComparer_t2070_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2282_0_0_0, 4},
	{ &IComparer_t390_0_0_0, 5},
};
extern const Il2CppType Comparer_1_t2283_0_0_0;
extern const Il2CppType DefaultComparer_t2070_gp_0_0_0_0;
extern const Il2CppType IComparable_1_t2285_0_0_0;
extern const Il2CppRGCTXDefinition DefaultComparer_t2070_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2613 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4593 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3124 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4594 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DefaultComparer_t2070_1_0_0;
struct DefaultComparer_t2070;
const Il2CppTypeDefinitionMetadata DefaultComparer_t2070_DefinitionMetadata = 
{
	&Comparer_1_t2069_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultComparer_t2070_InterfacesOffsets/* interfaceOffsets */
	, &Comparer_1_t2283_0_0_0/* parent */
	, DefaultComparer_t2070_VTable/* vtableMethods */
	, DefaultComparer_t2070_RGCTXData/* rgctxDefinition */
	, -1/* fieldStart */
	, 4767/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DefaultComparer_t2070_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DefaultComparer_t2070_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultComparer_t2070_0_0_0/* byval_arg */
	, &DefaultComparer_t2070_1_0_0/* this_arg */
	, &DefaultComparer_t2070_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 81/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.GenericComparer`1
extern TypeInfo GenericComparer_1_t2036_il2cpp_TypeInfo;
static const EncodedMethodIndex GenericComparer_1_t2036_VTable[7] = 
{
	120,
	125,
	122,
	123,
	1583,
	2147485232,
	1583,
};
extern const Il2CppType IComparer_1_t2286_0_0_0;
static Il2CppInterfaceOffsetPair GenericComparer_1_t2036_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2286_0_0_0, 4},
	{ &IComparer_t390_0_0_0, 5},
};
extern const Il2CppType Comparer_1_t2287_0_0_0;
extern const Il2CppType GenericComparer_1_t2036_gp_0_0_0_0;
extern const Il2CppType IComparable_1_t2289_0_0_0;
extern const Il2CppRGCTXDefinition GenericComparer_1_t2036_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2615 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4597 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3125 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3126 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GenericComparer_1_t2036_0_0_0;
extern const Il2CppType GenericComparer_1_t2036_1_0_0;
struct GenericComparer_1_t2036;
const Il2CppTypeDefinitionMetadata GenericComparer_1_t2036_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericComparer_1_t2036_InterfacesOffsets/* interfaceOffsets */
	, &Comparer_1_t2287_0_0_0/* parent */
	, GenericComparer_1_t2036_VTable/* vtableMethods */
	, GenericComparer_1_t2036_RGCTXData/* rgctxDefinition */
	, -1/* fieldStart */
	, 4769/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GenericComparer_1_t2036_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &GenericComparer_1_t2036_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericComparer_1_t2036_0_0_0/* byval_arg */
	, &GenericComparer_1_t2036_1_0_0/* this_arg */
	, &GenericComparer_1_t2036_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 82/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_Link.h"
// Metadata Definition System.Collections.Generic.Link
extern TypeInfo Link_t864_il2cpp_TypeInfo;
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_LinkMethodDeclarations.h"
static const EncodedMethodIndex Link_t864_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Link_t864_0_0_0;
extern const Il2CppType Link_t864_1_0_0;
const Il2CppTypeDefinitionMetadata Link_t864_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Link_t864_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2943/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Link_t864_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &Link_t864_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Link_t864_0_0_0/* byval_arg */
	, &Link_t864_1_0_0/* this_arg */
	, &Link_t864_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Link_t864)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Link_t864)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Link_t864 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2
extern TypeInfo Dictionary_2_t2071_il2cpp_TypeInfo;
extern const Il2CppType Dictionary_2_Do_CopyTo_m13108_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_Do_CopyTo_m13108_gp_1_0_0_0;
extern const Il2CppRGCTXDefinition Dictionary_2_Do_CopyTo_m13108_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2616 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3139 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3137 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m13113_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Dictionary_2_Do_ICollectionCopyTo_m13113_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 3141 }/* Type */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2617 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType ShimEnumerator_t2072_0_0_0;
extern const Il2CppType Enumerator_t2073_0_0_0;
extern const Il2CppType KeyCollection_t2074_0_0_0;
extern const Il2CppType ValueCollection_t2076_0_0_0;
extern const Il2CppType Transform_1_t2078_0_0_0;
static const Il2CppType* Dictionary_2_t2071_il2cpp_TypeInfo__nestedTypes[5] =
{
	&ShimEnumerator_t2072_0_0_0,
	&Enumerator_t2073_0_0_0,
	&KeyCollection_t2074_0_0_0,
	&ValueCollection_t2076_0_0_0,
	&Transform_1_t2078_0_0_0,
};
static const EncodedMethodIndex Dictionary_2_t2071_VTable[34] = 
{
	120,
	125,
	122,
	123,
	1585,
	1586,
	1587,
	1588,
	1589,
	1587,
	1590,
	1591,
	1592,
	1593,
	1594,
	1595,
	1596,
	1597,
	1598,
	1599,
	1600,
	1601,
	1602,
	1603,
	1604,
	1605,
	1606,
	1607,
	1608,
	1609,
	1610,
	1586,
	1609,
	1611,
};
extern const Il2CppType ICollection_1_t2293_0_0_0;
extern const Il2CppType IEnumerable_1_t2294_0_0_0;
extern const Il2CppType IDictionary_2_t2295_0_0_0;
extern const Il2CppType IDictionary_t493_0_0_0;
extern const Il2CppType IDeserializationCallback_t1429_0_0_0;
static const Il2CppType* Dictionary_2_t2071_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ISerializable_t1426_0_0_0,
	&ICollection_t569_0_0_0,
	&ICollection_1_t2293_0_0_0,
	&IEnumerable_1_t2294_0_0_0,
	&IDictionary_2_t2295_0_0_0,
	&IDictionary_t493_0_0_0,
	&IDeserializationCallback_t1429_0_0_0,
};
static Il2CppInterfaceOffsetPair Dictionary_2_t2071_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 5},
	{ &ICollection_t569_0_0_0, 6},
	{ &ICollection_1_t2293_0_0_0, 9},
	{ &IEnumerable_1_t2294_0_0_0, 16},
	{ &IDictionary_2_t2295_0_0_0, 17},
	{ &IDictionary_t493_0_0_0, 23},
	{ &IDeserializationCallback_t1429_0_0_0, 29},
};
extern const Il2CppType IEnumerator_1_t2296_0_0_0;
extern const Il2CppType Dictionary_2_t2071_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t2071_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2U5BU5D_t2299_0_0_0;
extern const Il2CppType Dictionary_2_t2300_0_0_0;
extern const Il2CppType Transform_1_t2301_0_0_0;
extern const Il2CppType Transform_1_t2302_0_0_0;
extern const Il2CppType Enumerator_t2303_0_0_0;
extern const Il2CppType ShimEnumerator_t2304_0_0_0;
extern const Il2CppType IEqualityComparer_1_t2305_0_0_0;
extern const Il2CppType EqualityComparer_1_t2306_0_0_0;
extern const Il2CppType TKeyU5BU5D_t2307_0_0_0;
extern const Il2CppType TValueU5BU5D_t2308_0_0_0;
extern const Il2CppType KeyValuePair_2_t2309_0_0_0;
extern const Il2CppType EqualityComparer_1_t2310_0_0_0;
extern const Il2CppType IEqualityComparer_1_t2311_0_0_0;
extern const Il2CppType KeyCollection_t2312_0_0_0;
extern const Il2CppType ValueCollection_t2313_0_0_0;
extern const Il2CppRGCTXDefinition Dictionary_2_t2071_RGCTXData[61] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2618 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2619 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4605 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4606 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3135 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2620 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2621 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2622 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2623 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2624 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3128 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2625 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2626 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2627 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3130 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2628 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2629 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2630 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2631 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2632 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3134 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2633 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4607 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2634 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4608 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2635 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2636 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2637 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4609 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2638 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2639 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3145 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2640 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4610 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2641 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3127 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2642 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2643 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4611 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2644 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4612 }/* Array */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4613 }/* Array */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2645 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3133 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2646 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2647 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2648 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4614 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4615 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3134 }/* Array */,
	{ IL2CPP_RGCTX_DATA_TYPE, 3127 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 3134 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3143 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2649 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3144 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2650 }/* Method */,
	{ IL2CPP_RGCTX_DATA_TYPE, 3128 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 3130 }/* Type */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2651 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2652 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Dictionary_2_t2071_0_0_0;
extern const Il2CppType Dictionary_2_t2071_1_0_0;
struct Dictionary_2_t2071;
const Il2CppTypeDefinitionMetadata Dictionary_2_t2071_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Dictionary_2_t2071_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Dictionary_2_t2071_InterfacesTypeInfos/* implementedInterfaces */
	, Dictionary_2_t2071_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Dictionary_2_t2071_VTable/* vtableMethods */
	, Dictionary_2_t2071_RGCTXData/* rgctxDefinition */
	, 2945/* fieldStart */
	, 4771/* methodStart */
	, -1/* eventStart */
	, 887/* propertyStart */

};
TypeInfo Dictionary_2_t2071_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Dictionary`2"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &Dictionary_2_t2071_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 810/* custom_attributes_cache */
	, &Dictionary_2_t2071_0_0_0/* byval_arg */
	, &Dictionary_2_t2071_1_0_0/* this_arg */
	, &Dictionary_2_t2071_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 85/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 51/* method_count */
	, 9/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 5/* nested_type_count */
	, 34/* vtable_count */
	, 8/* interfaces_count */
	, 8/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator
extern TypeInfo ShimEnumerator_t2072_il2cpp_TypeInfo;
static const EncodedMethodIndex ShimEnumerator_t2072_VTable[9] = 
{
	120,
	125,
	122,
	123,
	1612,
	1613,
	1614,
	1615,
	1616,
};
extern const Il2CppType IDictionaryEnumerator_t559_0_0_0;
static const Il2CppType* ShimEnumerator_t2072_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDictionaryEnumerator_t559_0_0_0,
};
static Il2CppInterfaceOffsetPair ShimEnumerator_t2072_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &IDictionaryEnumerator_t559_0_0_0, 6},
};
extern const Il2CppType Enumerator_t2314_0_0_0;
extern const Il2CppType ShimEnumerator_t2072_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t2072_gp_1_0_0_0;
extern const Il2CppRGCTXDefinition ShimEnumerator_t2072_RGCTXData[10] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2653 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2654 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4618 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2655 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2656 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3153 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2657 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3154 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2658 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ShimEnumerator_t2072_1_0_0;
struct ShimEnumerator_t2072;
const Il2CppTypeDefinitionMetadata ShimEnumerator_t2072_DefinitionMetadata = 
{
	&Dictionary_2_t2071_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, ShimEnumerator_t2072_InterfacesTypeInfos/* implementedInterfaces */
	, ShimEnumerator_t2072_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ShimEnumerator_t2072_VTable/* vtableMethods */
	, ShimEnumerator_t2072_RGCTXData/* rgctxDefinition */
	, 2961/* fieldStart */
	, 4822/* methodStart */
	, -1/* eventStart */
	, 896/* propertyStart */

};
TypeInfo ShimEnumerator_t2072_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ShimEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ShimEnumerator_t2072_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ShimEnumerator_t2072_0_0_0/* byval_arg */
	, &ShimEnumerator_t2072_1_0_0/* this_arg */
	, &ShimEnumerator_t2072_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 86/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 4/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator
extern TypeInfo Enumerator_t2073_il2cpp_TypeInfo;
static const EncodedMethodIndex Enumerator_t2073_VTable[11] = 
{
	150,
	125,
	151,
	152,
	1617,
	1618,
	1619,
	1620,
	1621,
	1622,
	1623,
};
extern const Il2CppType IEnumerator_1_t2317_0_0_0;
static const Il2CppType* Enumerator_t2073_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDisposable_t319_0_0_0,
	&IEnumerator_1_t2317_0_0_0,
	&IDictionaryEnumerator_t559_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t2073_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &IDisposable_t319_0_0_0, 6},
	{ &IEnumerator_1_t2317_0_0_0, 7},
	{ &IDictionaryEnumerator_t559_0_0_0, 8},
};
extern const Il2CppType KeyValuePair_2_t2318_0_0_0;
extern const Il2CppType Enumerator_t2073_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2073_gp_1_0_0_0;
extern const Il2CppRGCTXDefinition Enumerator_t2073_RGCTXData[11] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2659 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3159 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2660 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3157 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2661 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3158 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2662 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2663 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2664 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2665 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t2073_1_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t2073_DefinitionMetadata = 
{
	&Dictionary_2_t2071_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t2073_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t2073_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Enumerator_t2073_VTable/* vtableMethods */
	, Enumerator_t2073_RGCTXData/* rgctxDefinition */
	, 2962/* fieldStart */
	, 4828/* methodStart */
	, -1/* eventStart */
	, 900/* propertyStart */

};
TypeInfo Enumerator_t2073_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t2073_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t2073_0_0_0/* byval_arg */
	, &Enumerator_t2073_1_0_0/* this_arg */
	, &Enumerator_t2073_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 87/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 7/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/KeyCollection
extern TypeInfo KeyCollection_t2074_il2cpp_TypeInfo;
extern const Il2CppType Enumerator_t2075_0_0_0;
static const Il2CppType* KeyCollection_t2074_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t2075_0_0_0,
};
static const EncodedMethodIndex KeyCollection_t2074_VTable[16] = 
{
	120,
	125,
	122,
	123,
	1624,
	1625,
	1626,
	1627,
	1625,
	1628,
	1629,
	1630,
	1631,
	1632,
	1633,
	1634,
};
extern const Il2CppType ICollection_1_t2321_0_0_0;
extern const Il2CppType IEnumerable_1_t2322_0_0_0;
static const Il2CppType* KeyCollection_t2074_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_t569_0_0_0,
	&ICollection_1_t2321_0_0_0,
	&IEnumerable_1_t2322_0_0_0,
};
static Il2CppInterfaceOffsetPair KeyCollection_t2074_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &ICollection_1_t2321_0_0_0, 8},
	{ &IEnumerable_1_t2322_0_0_0, 15},
};
extern const Il2CppType Enumerator_t2323_0_0_0;
extern const Il2CppType TKeyU5BU5D_t2324_0_0_0;
extern const Il2CppType Transform_1_t2325_0_0_0;
extern const Il2CppRGCTXDefinition KeyCollection_t2074_RGCTXData[14] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2666 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2667 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3167 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3166 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2668 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2669 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2670 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4625 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2671 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2672 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2673 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2674 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2675 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyCollection_t2074_1_0_0;
struct KeyCollection_t2074;
const Il2CppTypeDefinitionMetadata KeyCollection_t2074_DefinitionMetadata = 
{
	&Dictionary_2_t2071_0_0_0/* declaringType */
	, KeyCollection_t2074_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, KeyCollection_t2074_InterfacesTypeInfos/* implementedInterfaces */
	, KeyCollection_t2074_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeyCollection_t2074_VTable/* vtableMethods */
	, KeyCollection_t2074_RGCTXData/* rgctxDefinition */
	, 2966/* fieldStart */
	, 4840/* methodStart */
	, -1/* eventStart */
	, 907/* propertyStart */

};
TypeInfo KeyCollection_t2074_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyCollection"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &KeyCollection_t2074_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 813/* custom_attributes_cache */
	, &KeyCollection_t2074_0_0_0/* byval_arg */
	, &KeyCollection_t2074_1_0_0/* this_arg */
	, &KeyCollection_t2074_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 88/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057026/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 16/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator
extern TypeInfo Enumerator_t2075_il2cpp_TypeInfo;
static const EncodedMethodIndex Enumerator_t2075_VTable[8] = 
{
	150,
	125,
	151,
	152,
	1635,
	1636,
	1637,
	1638,
};
extern const Il2CppType IEnumerator_1_t2326_0_0_0;
static const Il2CppType* Enumerator_t2075_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDisposable_t319_0_0_0,
	&IEnumerator_1_t2326_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t2075_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &IDisposable_t319_0_0_0, 6},
	{ &IEnumerator_1_t2326_0_0_0, 7},
};
extern const Il2CppType Enumerator_t2075_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Enumerator_t2075_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2676 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2677 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3170 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2678 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2679 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2680 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t2075_1_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t2075_DefinitionMetadata = 
{
	&KeyCollection_t2074_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t2075_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t2075_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Enumerator_t2075_VTable/* vtableMethods */
	, Enumerator_t2075_RGCTXData/* rgctxDefinition */
	, 2967/* fieldStart */
	, 4853/* methodStart */
	, -1/* eventStart */
	, 910/* propertyStart */

};
TypeInfo Enumerator_t2075_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t2075_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t2075_0_0_0/* byval_arg */
	, &Enumerator_t2075_1_0_0/* this_arg */
	, &Enumerator_t2075_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 89/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection
extern TypeInfo ValueCollection_t2076_il2cpp_TypeInfo;
extern const Il2CppType Enumerator_t2077_0_0_0;
static const Il2CppType* ValueCollection_t2076_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t2077_0_0_0,
};
static const EncodedMethodIndex ValueCollection_t2076_VTable[16] = 
{
	120,
	125,
	122,
	123,
	1639,
	1640,
	1641,
	1642,
	1640,
	1643,
	1644,
	1645,
	1646,
	1647,
	1648,
	1649,
};
extern const Il2CppType ICollection_1_t2328_0_0_0;
extern const Il2CppType IEnumerable_1_t2329_0_0_0;
static const Il2CppType* ValueCollection_t2076_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_t569_0_0_0,
	&ICollection_1_t2328_0_0_0,
	&IEnumerable_1_t2329_0_0_0,
};
static Il2CppInterfaceOffsetPair ValueCollection_t2076_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &ICollection_1_t2328_0_0_0, 8},
	{ &IEnumerable_1_t2329_0_0_0, 15},
};
extern const Il2CppType Enumerator_t2330_0_0_0;
extern const Il2CppType TValueU5BU5D_t2331_0_0_0;
extern const Il2CppType Transform_1_t2332_0_0_0;
extern const Il2CppRGCTXDefinition ValueCollection_t2076_RGCTXData[14] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2681 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2682 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3178 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3177 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2683 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2684 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2685 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4632 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2686 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2687 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2688 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2689 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2690 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ValueCollection_t2076_1_0_0;
struct ValueCollection_t2076;
const Il2CppTypeDefinitionMetadata ValueCollection_t2076_DefinitionMetadata = 
{
	&Dictionary_2_t2071_0_0_0/* declaringType */
	, ValueCollection_t2076_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ValueCollection_t2076_InterfacesTypeInfos/* implementedInterfaces */
	, ValueCollection_t2076_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ValueCollection_t2076_VTable/* vtableMethods */
	, ValueCollection_t2076_RGCTXData/* rgctxDefinition */
	, 2968/* fieldStart */
	, 4858/* methodStart */
	, -1/* eventStart */
	, 912/* propertyStart */

};
TypeInfo ValueCollection_t2076_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ValueCollection"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ValueCollection_t2076_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 814/* custom_attributes_cache */
	, &ValueCollection_t2076_0_0_0/* byval_arg */
	, &ValueCollection_t2076_1_0_0/* this_arg */
	, &ValueCollection_t2076_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 90/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057026/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 16/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator
extern TypeInfo Enumerator_t2077_il2cpp_TypeInfo;
static const EncodedMethodIndex Enumerator_t2077_VTable[8] = 
{
	150,
	125,
	151,
	152,
	1650,
	1651,
	1652,
	1653,
};
extern const Il2CppType IEnumerator_1_t2333_0_0_0;
static const Il2CppType* Enumerator_t2077_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDisposable_t319_0_0_0,
	&IEnumerator_1_t2333_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t2077_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &IDisposable_t319_0_0_0, 6},
	{ &IEnumerator_1_t2333_0_0_0, 7},
};
extern const Il2CppType Enumerator_t2077_gp_1_0_0_0;
extern const Il2CppRGCTXDefinition Enumerator_t2077_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2691 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2692 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3182 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2693 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2694 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2695 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t2077_1_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t2077_DefinitionMetadata = 
{
	&ValueCollection_t2076_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t2077_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t2077_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Enumerator_t2077_VTable/* vtableMethods */
	, Enumerator_t2077_RGCTXData/* rgctxDefinition */
	, 2969/* fieldStart */
	, 4871/* methodStart */
	, -1/* eventStart */
	, 915/* propertyStart */

};
TypeInfo Enumerator_t2077_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t2077_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t2077_0_0_0/* byval_arg */
	, &Enumerator_t2077_1_0_0/* this_arg */
	, &Enumerator_t2077_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 91/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1
extern TypeInfo Transform_1_t2078_il2cpp_TypeInfo;
static const EncodedMethodIndex Transform_1_t2078_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	1654,
	1655,
	1656,
};
static Il2CppInterfaceOffsetPair Transform_1_t2078_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Transform_1_t2078_1_0_0;
struct Transform_1_t2078;
const Il2CppTypeDefinitionMetadata Transform_1_t2078_DefinitionMetadata = 
{
	&Dictionary_2_t2071_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Transform_1_t2078_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, Transform_1_t2078_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4876/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Transform_1_t2078_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Transform`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Transform_1_t2078_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Transform_1_t2078_0_0_0/* byval_arg */
	, &Transform_1_t2078_1_0_0/* this_arg */
	, &Transform_1_t2078_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 92/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.EqualityComparer`1
extern TypeInfo EqualityComparer_1_t2079_il2cpp_TypeInfo;
extern const Il2CppType DefaultComparer_t2080_0_0_0;
static const Il2CppType* EqualityComparer_1_t2079_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DefaultComparer_t2080_0_0_0,
};
static const EncodedMethodIndex EqualityComparer_1_t2079_VTable[10] = 
{
	120,
	125,
	122,
	123,
	1657,
	1658,
	1659,
	1660,
	0,
	0,
};
extern const Il2CppType IEqualityComparer_1_t2335_0_0_0;
extern const Il2CppType IEqualityComparer_t397_0_0_0;
static const Il2CppType* EqualityComparer_1_t2079_InterfacesTypeInfos[] = 
{
	&IEqualityComparer_1_t2335_0_0_0,
	&IEqualityComparer_t397_0_0_0,
};
static Il2CppInterfaceOffsetPair EqualityComparer_1_t2079_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t2335_0_0_0, 4},
	{ &IEqualityComparer_t397_0_0_0, 6},
};
extern const Il2CppType IEquatable_1_t2336_0_0_0;
extern const Il2CppType EqualityComparer_1_t2079_gp_0_0_0_0;
extern const Il2CppType EqualityComparer_1_t2338_0_0_0;
extern const Il2CppType DefaultComparer_t2339_0_0_0;
extern const Il2CppRGCTXDefinition EqualityComparer_1_t2079_RGCTXData[10] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4639 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 3187 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3188 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3188 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4640 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2696 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3187 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2697 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2698 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EqualityComparer_1_t2079_0_0_0;
extern const Il2CppType EqualityComparer_1_t2079_1_0_0;
struct EqualityComparer_1_t2079;
const Il2CppTypeDefinitionMetadata EqualityComparer_1_t2079_DefinitionMetadata = 
{
	NULL/* declaringType */
	, EqualityComparer_1_t2079_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, EqualityComparer_1_t2079_InterfacesTypeInfos/* implementedInterfaces */
	, EqualityComparer_1_t2079_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EqualityComparer_1_t2079_VTable/* vtableMethods */
	, EqualityComparer_1_t2079_RGCTXData/* rgctxDefinition */
	, 2970/* fieldStart */
	, 4880/* methodStart */
	, -1/* eventStart */
	, 917/* propertyStart */

};
TypeInfo EqualityComparer_1_t2079_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &EqualityComparer_1_t2079_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EqualityComparer_1_t2079_0_0_0/* byval_arg */
	, &EqualityComparer_1_t2079_1_0_0/* this_arg */
	, &EqualityComparer_1_t2079_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 93/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer
extern TypeInfo DefaultComparer_t2080_il2cpp_TypeInfo;
static const EncodedMethodIndex DefaultComparer_t2080_VTable[10] = 
{
	120,
	125,
	122,
	123,
	1661,
	1662,
	2147485311,
	2147485312,
	1662,
	1661,
};
extern const Il2CppType IEqualityComparer_1_t2340_0_0_0;
static Il2CppInterfaceOffsetPair DefaultComparer_t2080_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t2340_0_0_0, 4},
	{ &IEqualityComparer_t397_0_0_0, 6},
};
extern const Il2CppType EqualityComparer_1_t2341_0_0_0;
extern const Il2CppType DefaultComparer_t2080_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition DefaultComparer_t2080_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2701 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4644 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3190 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DefaultComparer_t2080_1_0_0;
struct DefaultComparer_t2080;
const Il2CppTypeDefinitionMetadata DefaultComparer_t2080_DefinitionMetadata = 
{
	&EqualityComparer_1_t2079_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultComparer_t2080_InterfacesOffsets/* interfaceOffsets */
	, &EqualityComparer_1_t2341_0_0_0/* parent */
	, DefaultComparer_t2080_VTable/* vtableMethods */
	, DefaultComparer_t2080_RGCTXData/* rgctxDefinition */
	, -1/* fieldStart */
	, 4887/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DefaultComparer_t2080_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DefaultComparer_t2080_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultComparer_t2080_0_0_0/* byval_arg */
	, &DefaultComparer_t2080_1_0_0/* this_arg */
	, &DefaultComparer_t2080_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 94/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057027/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.GenericEqualityComparer`1
extern TypeInfo GenericEqualityComparer_1_t2035_il2cpp_TypeInfo;
static const EncodedMethodIndex GenericEqualityComparer_1_t2035_VTable[10] = 
{
	120,
	125,
	122,
	123,
	1665,
	1666,
	2147485315,
	2147485316,
	1666,
	1665,
};
extern const Il2CppType IEqualityComparer_1_t2343_0_0_0;
static Il2CppInterfaceOffsetPair GenericEqualityComparer_1_t2035_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t2343_0_0_0, 4},
	{ &IEqualityComparer_t397_0_0_0, 6},
};
extern const Il2CppType EqualityComparer_1_t2344_0_0_0;
extern const Il2CppType GenericEqualityComparer_1_t2035_gp_0_0_0_0;
extern const Il2CppType IEquatable_1_t2346_0_0_0;
extern const Il2CppRGCTXDefinition GenericEqualityComparer_1_t2035_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2704 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4647 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3191 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3192 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GenericEqualityComparer_1_t2035_0_0_0;
extern const Il2CppType GenericEqualityComparer_1_t2035_1_0_0;
struct GenericEqualityComparer_1_t2035;
const Il2CppTypeDefinitionMetadata GenericEqualityComparer_1_t2035_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericEqualityComparer_1_t2035_InterfacesOffsets/* interfaceOffsets */
	, &EqualityComparer_1_t2344_0_0_0/* parent */
	, GenericEqualityComparer_1_t2035_VTable/* vtableMethods */
	, GenericEqualityComparer_1_t2035_RGCTXData/* rgctxDefinition */
	, -1/* fieldStart */
	, 4890/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GenericEqualityComparer_1_t2035_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericEqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &GenericEqualityComparer_1_t2035_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericEqualityComparer_1_t2035_0_0_0/* byval_arg */
	, &GenericEqualityComparer_1_t2035_1_0_0/* this_arg */
	, &GenericEqualityComparer_1_t2035_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 95/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.IComparer`1
extern TypeInfo IComparer_1_t2081_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IComparer_1_t2081_0_0_0;
extern const Il2CppType IComparer_1_t2081_1_0_0;
struct IComparer_1_t2081;
const Il2CppTypeDefinitionMetadata IComparer_1_t2081_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4893/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IComparer_1_t2081_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &IComparer_1_t2081_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IComparer_1_t2081_0_0_0/* byval_arg */
	, &IComparer_1_t2081_1_0_0/* this_arg */
	, &IComparer_1_t2081_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 96/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.IDictionary`2
extern TypeInfo IDictionary_2_t2082_il2cpp_TypeInfo;
extern const Il2CppType ICollection_1_t2347_0_0_0;
extern const Il2CppType IEnumerable_1_t2348_0_0_0;
static const Il2CppType* IDictionary_2_t2082_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_1_t2347_0_0_0,
	&IEnumerable_1_t2348_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDictionary_2_t2082_0_0_0;
extern const Il2CppType IDictionary_2_t2082_1_0_0;
struct IDictionary_2_t2082;
const Il2CppTypeDefinitionMetadata IDictionary_2_t2082_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IDictionary_2_t2082_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4894/* methodStart */
	, -1/* eventStart */
	, 918/* propertyStart */

};
TypeInfo IDictionary_2_t2082_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDictionary`2"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &IDictionary_2_t2082_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 815/* custom_attributes_cache */
	, &IDictionary_2_t2082_0_0_0/* byval_arg */
	, &IDictionary_2_t2082_1_0_0/* this_arg */
	, &IDictionary_2_t2082_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 97/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.IEqualityComparer`1
extern TypeInfo IEqualityComparer_1_t2083_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEqualityComparer_1_t2083_0_0_0;
extern const Il2CppType IEqualityComparer_1_t2083_1_0_0;
struct IEqualityComparer_1_t2083;
const Il2CppTypeDefinitionMetadata IEqualityComparer_1_t2083_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4900/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IEqualityComparer_1_t2083_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &IEqualityComparer_1_t2083_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IEqualityComparer_1_t2083_0_0_0/* byval_arg */
	, &IEqualityComparer_1_t2083_1_0_0/* this_arg */
	, &IEqualityComparer_1_t2083_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 98/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Generic.KeyNotFoundException
#include "mscorlib_System_Collections_Generic_KeyNotFoundException.h"
// Metadata Definition System.Collections.Generic.KeyNotFoundException
extern TypeInfo KeyNotFoundException_t865_il2cpp_TypeInfo;
// System.Collections.Generic.KeyNotFoundException
#include "mscorlib_System_Collections_Generic_KeyNotFoundExceptionMethodDeclarations.h"
static const EncodedMethodIndex KeyNotFoundException_t865_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static const Il2CppType* KeyNotFoundException_t865_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair KeyNotFoundException_t865_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyNotFoundException_t865_0_0_0;
extern const Il2CppType KeyNotFoundException_t865_1_0_0;
struct KeyNotFoundException_t865;
const Il2CppTypeDefinitionMetadata KeyNotFoundException_t865_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, KeyNotFoundException_t865_InterfacesTypeInfos/* implementedInterfaces */
	, KeyNotFoundException_t865_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, KeyNotFoundException_t865_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4902/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyNotFoundException_t865_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyNotFoundException"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &KeyNotFoundException_t865_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 816/* custom_attributes_cache */
	, &KeyNotFoundException_t865_0_0_0/* byval_arg */
	, &KeyNotFoundException_t865_1_0_0/* this_arg */
	, &KeyNotFoundException_t865_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyNotFoundException_t865)/* instance_size */
	, sizeof (KeyNotFoundException_t865)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.KeyValuePair`2
extern TypeInfo KeyValuePair_2_t2084_il2cpp_TypeInfo;
static const EncodedMethodIndex KeyValuePair_2_t2084_VTable[4] = 
{
	150,
	125,
	151,
	1669,
};
extern const Il2CppType KeyValuePair_2_t2084_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t2084_gp_1_0_0_0;
extern const Il2CppRGCTXDefinition KeyValuePair_2_t2084_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2705 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2706 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2707 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3200 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2708 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3201 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyValuePair_2_t2084_0_0_0;
extern const Il2CppType KeyValuePair_2_t2084_1_0_0;
const Il2CppTypeDefinitionMetadata KeyValuePair_2_t2084_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, KeyValuePair_2_t2084_VTable/* vtableMethods */
	, KeyValuePair_2_t2084_RGCTXData/* rgctxDefinition */
	, 2971/* fieldStart */
	, 4904/* methodStart */
	, -1/* eventStart */
	, 921/* propertyStart */

};
TypeInfo KeyValuePair_2_t2084_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &KeyValuePair_2_t2084_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 817/* custom_attributes_cache */
	, &KeyValuePair_2_t2084_0_0_0/* byval_arg */
	, &KeyValuePair_2_t2084_1_0_0/* this_arg */
	, &KeyValuePair_2_t2084_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 99/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.List`1
extern TypeInfo List_1_t2085_il2cpp_TypeInfo;
extern const Il2CppType Enumerator_t2086_0_0_0;
static const Il2CppType* List_1_t2085_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t2086_0_0_0,
};
static const EncodedMethodIndex List_1_t2085_VTable[30] = 
{
	120,
	125,
	122,
	123,
	1670,
	1671,
	1672,
	1673,
	1674,
	1675,
	1676,
	1677,
	1678,
	1679,
	1680,
	1681,
	1682,
	1671,
	1683,
	1684,
	1677,
	1685,
	1686,
	1687,
	1688,
	1689,
	1690,
	1682,
	1691,
	1692,
};
extern const Il2CppType ICollection_1_t2351_0_0_0;
extern const Il2CppType IEnumerable_1_t2352_0_0_0;
extern const Il2CppType IList_1_t2353_0_0_0;
static const Il2CppType* List_1_t2085_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_t569_0_0_0,
	&IList_t519_0_0_0,
	&ICollection_1_t2351_0_0_0,
	&IEnumerable_1_t2352_0_0_0,
	&IList_1_t2353_0_0_0,
};
static Il2CppInterfaceOffsetPair List_1_t2085_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
	{ &ICollection_1_t2351_0_0_0, 17},
	{ &IEnumerable_1_t2352_0_0_0, 24},
	{ &IList_1_t2353_0_0_0, 25},
};
extern const Il2CppType List_1_t2354_0_0_0;
extern const Il2CppType TU5BU5D_t2355_0_0_0;
extern const Il2CppType Enumerator_t2356_0_0_0;
extern const Il2CppType List_1_t2085_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition List_1_t2085_RGCTXData[22] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4665 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3206 }/* Array */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2709 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3207 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3205 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2710 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2711 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2712 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2713 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2714 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2715 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2716 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2717 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2718 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2719 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2720 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2721 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2722 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2723 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2724 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2725 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType List_1_t2085_0_0_0;
extern const Il2CppType List_1_t2085_1_0_0;
struct List_1_t2085;
const Il2CppTypeDefinitionMetadata List_1_t2085_DefinitionMetadata = 
{
	NULL/* declaringType */
	, List_1_t2085_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, List_1_t2085_InterfacesTypeInfos/* implementedInterfaces */
	, List_1_t2085_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, List_1_t2085_VTable/* vtableMethods */
	, List_1_t2085_RGCTXData/* rgctxDefinition */
	, 2973/* fieldStart */
	, 4910/* methodStart */
	, -1/* eventStart */
	, 923/* propertyStart */

};
TypeInfo List_1_t2085_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "List`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &List_1_t2085_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 818/* custom_attributes_cache */
	, &List_1_t2085_0_0_0/* byval_arg */
	, &List_1_t2085_1_0_0/* this_arg */
	, &List_1_t2085_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 100/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 33/* method_count */
	, 6/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 30/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.List`1/Enumerator
extern TypeInfo Enumerator_t2086_il2cpp_TypeInfo;
static const EncodedMethodIndex Enumerator_t2086_VTable[8] = 
{
	150,
	125,
	151,
	152,
	1693,
	1694,
	1695,
	1696,
};
extern const Il2CppType IEnumerator_1_t2358_0_0_0;
static const Il2CppType* Enumerator_t2086_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDisposable_t319_0_0_0,
	&IEnumerator_1_t2358_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t2086_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &IDisposable_t319_0_0_0, 6},
	{ &IEnumerator_1_t2358_0_0_0, 7},
};
extern const Il2CppType Enumerator_t2086_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2360_0_0_0;
extern const Il2CppRGCTXDefinition Enumerator_t2086_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2726 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3211 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4669 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t2086_1_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t2086_DefinitionMetadata = 
{
	&List_1_t2085_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t2086_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t2086_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Enumerator_t2086_VTable/* vtableMethods */
	, Enumerator_t2086_RGCTXData/* rgctxDefinition */
	, 2977/* fieldStart */
	, 4943/* methodStart */
	, -1/* eventStart */
	, 929/* propertyStart */

};
TypeInfo Enumerator_t2086_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t2086_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t2086_0_0_0/* byval_arg */
	, &Enumerator_t2086_1_0_0/* this_arg */
	, &Enumerator_t2086_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 101/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.ObjectModel.Collection`1
extern TypeInfo Collection_1_t2087_il2cpp_TypeInfo;
static const EncodedMethodIndex Collection_1_t2087_VTable[34] = 
{
	120,
	125,
	122,
	123,
	1697,
	1698,
	1699,
	1700,
	1701,
	1702,
	1703,
	1704,
	1705,
	1706,
	1707,
	1708,
	1709,
	1698,
	1710,
	1711,
	1704,
	1712,
	1713,
	1714,
	1715,
	1716,
	1709,
	1717,
	1718,
	1719,
	1720,
	1721,
	1722,
	1723,
};
extern const Il2CppType ICollection_1_t2361_0_0_0;
extern const Il2CppType IList_1_t2362_0_0_0;
extern const Il2CppType IEnumerable_1_t2363_0_0_0;
static const Il2CppType* Collection_1_t2087_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_t569_0_0_0,
	&IList_t519_0_0_0,
	&ICollection_1_t2361_0_0_0,
	&IList_1_t2362_0_0_0,
	&IEnumerable_1_t2363_0_0_0,
};
static Il2CppInterfaceOffsetPair Collection_1_t2087_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
	{ &ICollection_1_t2361_0_0_0, 17},
	{ &IList_1_t2362_0_0_0, 24},
	{ &IEnumerable_1_t2363_0_0_0, 29},
};
extern const Il2CppType List_1_t2364_0_0_0;
extern const Il2CppType Collection_1_t2365_0_0_0;
extern const Il2CppType Collection_1_t2087_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Collection_1_t2087_RGCTXData[17] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4673 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2727 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4671 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4672 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2728 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4674 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2729 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2730 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3214 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3217 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2731 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2732 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2733 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2734 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2735 }/* Method */,
	{ IL2CPP_RGCTX_DATA_TYPE, 3214 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Collection_1_t2087_0_0_0;
extern const Il2CppType Collection_1_t2087_1_0_0;
struct Collection_1_t2087;
const Il2CppTypeDefinitionMetadata Collection_1_t2087_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Collection_1_t2087_InterfacesTypeInfos/* implementedInterfaces */
	, Collection_1_t2087_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Collection_1_t2087_VTable/* vtableMethods */
	, Collection_1_t2087_RGCTXData/* rgctxDefinition */
	, 2981/* fieldStart */
	, 4949/* methodStart */
	, -1/* eventStart */
	, 931/* propertyStart */

};
TypeInfo Collection_1_t2087_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Collection`1"/* name */
	, "System.Collections.ObjectModel"/* namespaze */
	, NULL/* methods */
	, &Collection_1_t2087_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 819/* custom_attributes_cache */
	, &Collection_1_t2087_0_0_0/* byval_arg */
	, &Collection_1_t2087_1_0_0/* this_arg */
	, &Collection_1_t2087_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 102/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 5/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1
extern TypeInfo ReadOnlyCollection_1_t2088_il2cpp_TypeInfo;
static const EncodedMethodIndex ReadOnlyCollection_1_t2088_VTable[31] = 
{
	120,
	125,
	122,
	123,
	1724,
	1725,
	1726,
	1727,
	1728,
	1729,
	1730,
	1731,
	1732,
	1733,
	1734,
	1735,
	1736,
	1725,
	1737,
	1738,
	1739,
	1740,
	1741,
	1742,
	1743,
	1744,
	1745,
	1746,
	1747,
	1748,
	1749,
};
extern const Il2CppType ICollection_1_t2367_0_0_0;
extern const Il2CppType IList_1_t2368_0_0_0;
extern const Il2CppType IEnumerable_1_t2369_0_0_0;
static const Il2CppType* ReadOnlyCollection_1_t2088_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_t569_0_0_0,
	&IList_t519_0_0_0,
	&ICollection_1_t2367_0_0_0,
	&IList_1_t2368_0_0_0,
	&IEnumerable_1_t2369_0_0_0,
};
static Il2CppInterfaceOffsetPair ReadOnlyCollection_1_t2088_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
	{ &ICollection_1_t2367_0_0_0, 17},
	{ &IList_1_t2368_0_0_0, 24},
	{ &IEnumerable_1_t2369_0_0_0, 29},
};
extern const Il2CppType Collection_1_t2370_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t2088_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition ReadOnlyCollection_1_t2088_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 2736 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 2737 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4679 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3220 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4677 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3219 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4678 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReadOnlyCollection_1_t2088_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t2088_1_0_0;
struct ReadOnlyCollection_1_t2088;
const Il2CppTypeDefinitionMetadata ReadOnlyCollection_1_t2088_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ReadOnlyCollection_1_t2088_InterfacesTypeInfos/* implementedInterfaces */
	, ReadOnlyCollection_1_t2088_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReadOnlyCollection_1_t2088_VTable/* vtableMethods */
	, ReadOnlyCollection_1_t2088_RGCTXData/* rgctxDefinition */
	, 2983/* fieldStart */
	, 4980/* methodStart */
	, -1/* eventStart */
	, 936/* propertyStart */

};
TypeInfo ReadOnlyCollection_1_t2088_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReadOnlyCollection`1"/* name */
	, "System.Collections.ObjectModel"/* namespaze */
	, NULL/* methods */
	, &ReadOnlyCollection_1_t2088_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 820/* custom_attributes_cache */
	, &ReadOnlyCollection_1_t2088_0_0_0/* byval_arg */
	, &ReadOnlyCollection_1_t2088_1_0_0/* this_arg */
	, &ReadOnlyCollection_1_t2088_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 103/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 27/* method_count */
	, 6/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 31/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Collections.ArrayList
#include "mscorlib_System_Collections_ArrayList.h"
// Metadata Definition System.Collections.ArrayList
extern TypeInfo ArrayList_t395_il2cpp_TypeInfo;
// System.Collections.ArrayList
#include "mscorlib_System_Collections_ArrayListMethodDeclarations.h"
extern const Il2CppType SimpleEnumerator_t866_0_0_0;
extern const Il2CppType ArrayListWrapper_t867_0_0_0;
extern const Il2CppType SynchronizedArrayListWrapper_t868_0_0_0;
extern const Il2CppType FixedSizeArrayListWrapper_t869_0_0_0;
extern const Il2CppType ReadOnlyArrayListWrapper_t870_0_0_0;
static const Il2CppType* ArrayList_t395_il2cpp_TypeInfo__nestedTypes[5] =
{
	&SimpleEnumerator_t866_0_0_0,
	&ArrayListWrapper_t867_0_0_0,
	&SynchronizedArrayListWrapper_t868_0_0_0,
	&FixedSizeArrayListWrapper_t869_0_0_0,
	&ReadOnlyArrayListWrapper_t870_0_0_0,
};
static const EncodedMethodIndex ArrayList_t395_VTable[45] = 
{
	120,
	125,
	122,
	123,
	1750,
	1751,
	1752,
	1753,
	1754,
	1755,
	1756,
	1757,
	1758,
	1759,
	1760,
	1761,
	1762,
	1754,
	1755,
	1751,
	1763,
	1764,
	1765,
	1766,
	1752,
	1756,
	1757,
	1758,
	1759,
	1767,
	1768,
	1760,
	1769,
	1761,
	1762,
	1770,
	1753,
	1771,
	1750,
	1772,
	1773,
	1774,
	1775,
	1776,
	1777,
};
static const Il2CppType* ArrayList_t395_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICloneable_t2056_0_0_0,
	&ICollection_t569_0_0_0,
	&IList_t519_0_0_0,
};
static Il2CppInterfaceOffsetPair ArrayList_t395_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 5},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayList_t395_0_0_0;
extern const Il2CppType ArrayList_t395_1_0_0;
struct ArrayList_t395;
const Il2CppTypeDefinitionMetadata ArrayList_t395_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ArrayList_t395_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ArrayList_t395_InterfacesTypeInfos/* implementedInterfaces */
	, ArrayList_t395_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArrayList_t395_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2984/* fieldStart */
	, 5007/* methodStart */
	, -1/* eventStart */
	, 942/* propertyStart */

};
TypeInfo ArrayList_t395_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayList"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &ArrayList_t395_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 821/* custom_attributes_cache */
	, &ArrayList_t395_0_0_0/* byval_arg */
	, &ArrayList_t395_1_0_0/* this_arg */
	, &ArrayList_t395_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayList_t395)/* instance_size */
	, sizeof (ArrayList_t395)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ArrayList_t395_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 38/* method_count */
	, 6/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 5/* nested_type_count */
	, 45/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.ArrayList/SimpleEnumerator
#include "mscorlib_System_Collections_ArrayList_SimpleEnumerator.h"
// Metadata Definition System.Collections.ArrayList/SimpleEnumerator
extern TypeInfo SimpleEnumerator_t866_il2cpp_TypeInfo;
// System.Collections.ArrayList/SimpleEnumerator
#include "mscorlib_System_Collections_ArrayList_SimpleEnumeratorMethodDeclarations.h"
static const EncodedMethodIndex SimpleEnumerator_t866_VTable[6] = 
{
	120,
	125,
	122,
	123,
	1778,
	1779,
};
static const Il2CppType* SimpleEnumerator_t866_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&ICloneable_t2056_0_0_0,
};
static Il2CppInterfaceOffsetPair SimpleEnumerator_t866_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SimpleEnumerator_t866_1_0_0;
struct SimpleEnumerator_t866;
const Il2CppTypeDefinitionMetadata SimpleEnumerator_t866_DefinitionMetadata = 
{
	&ArrayList_t395_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, SimpleEnumerator_t866_InterfacesTypeInfos/* implementedInterfaces */
	, SimpleEnumerator_t866_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SimpleEnumerator_t866_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2989/* fieldStart */
	, 5045/* methodStart */
	, -1/* eventStart */
	, 948/* propertyStart */

};
TypeInfo SimpleEnumerator_t866_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SimpleEnumerator_t866_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SimpleEnumerator_t866_0_0_0/* byval_arg */
	, &SimpleEnumerator_t866_1_0_0/* this_arg */
	, &SimpleEnumerator_t866_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleEnumerator_t866)/* instance_size */
	, sizeof (SimpleEnumerator_t866)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SimpleEnumerator_t866_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.ArrayList/ArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_ArrayListWrapper.h"
// Metadata Definition System.Collections.ArrayList/ArrayListWrapper
extern TypeInfo ArrayListWrapper_t867_il2cpp_TypeInfo;
// System.Collections.ArrayList/ArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_ArrayListWrapperMethodDeclarations.h"
static const EncodedMethodIndex ArrayListWrapper_t867_VTable[45] = 
{
	120,
	125,
	122,
	123,
	1780,
	1781,
	1782,
	1783,
	1784,
	1785,
	1786,
	1787,
	1788,
	1789,
	1790,
	1791,
	1792,
	1784,
	1785,
	1781,
	1793,
	1794,
	1795,
	1796,
	1782,
	1786,
	1787,
	1788,
	1789,
	1797,
	1798,
	1790,
	1799,
	1791,
	1792,
	1800,
	1783,
	1801,
	1780,
	1802,
	1803,
	1804,
	1805,
	1806,
	1807,
};
static Il2CppInterfaceOffsetPair ArrayListWrapper_t867_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 5},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayListWrapper_t867_1_0_0;
struct ArrayListWrapper_t867;
const Il2CppTypeDefinitionMetadata ArrayListWrapper_t867_DefinitionMetadata = 
{
	&ArrayList_t395_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArrayListWrapper_t867_InterfacesOffsets/* interfaceOffsets */
	, &ArrayList_t395_0_0_0/* parent */
	, ArrayListWrapper_t867_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2994/* fieldStart */
	, 5049/* methodStart */
	, -1/* eventStart */
	, 949/* propertyStart */

};
TypeInfo ArrayListWrapper_t867_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayListWrapper"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ArrayListWrapper_t867_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 822/* custom_attributes_cache */
	, &ArrayListWrapper_t867_0_0_0/* byval_arg */
	, &ArrayListWrapper_t867_1_0_0/* this_arg */
	, &ArrayListWrapper_t867_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayListWrapper_t867)/* instance_size */
	, sizeof (ArrayListWrapper_t867)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 29/* method_count */
	, 6/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 45/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.ArrayList/SynchronizedArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_SynchronizedArrayListW.h"
// Metadata Definition System.Collections.ArrayList/SynchronizedArrayListWrapper
extern TypeInfo SynchronizedArrayListWrapper_t868_il2cpp_TypeInfo;
// System.Collections.ArrayList/SynchronizedArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_SynchronizedArrayListWMethodDeclarations.h"
static const EncodedMethodIndex SynchronizedArrayListWrapper_t868_VTable[45] = 
{
	120,
	125,
	122,
	123,
	1808,
	1809,
	1810,
	1811,
	1812,
	1813,
	1814,
	1815,
	1816,
	1817,
	1818,
	1819,
	1820,
	1812,
	1813,
	1809,
	1821,
	1822,
	1823,
	1824,
	1810,
	1814,
	1815,
	1816,
	1817,
	1825,
	1826,
	1818,
	1827,
	1819,
	1820,
	1828,
	1811,
	1829,
	1808,
	1830,
	1831,
	1832,
	1833,
	1834,
	1835,
};
static Il2CppInterfaceOffsetPair SynchronizedArrayListWrapper_t868_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 5},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizedArrayListWrapper_t868_1_0_0;
struct SynchronizedArrayListWrapper_t868;
const Il2CppTypeDefinitionMetadata SynchronizedArrayListWrapper_t868_DefinitionMetadata = 
{
	&ArrayList_t395_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SynchronizedArrayListWrapper_t868_InterfacesOffsets/* interfaceOffsets */
	, &ArrayListWrapper_t867_0_0_0/* parent */
	, SynchronizedArrayListWrapper_t868_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2995/* fieldStart */
	, 5078/* methodStart */
	, -1/* eventStart */
	, 955/* propertyStart */

};
TypeInfo SynchronizedArrayListWrapper_t868_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizedArrayListWrapper"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SynchronizedArrayListWrapper_t868_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 823/* custom_attributes_cache */
	, &SynchronizedArrayListWrapper_t868_0_0_0/* byval_arg */
	, &SynchronizedArrayListWrapper_t868_1_0_0/* this_arg */
	, &SynchronizedArrayListWrapper_t868_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizedArrayListWrapper_t868)/* instance_size */
	, sizeof (SynchronizedArrayListWrapper_t868)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057027/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 29/* method_count */
	, 6/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 45/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.ArrayList/FixedSizeArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_FixedSizeArrayListWrap.h"
// Metadata Definition System.Collections.ArrayList/FixedSizeArrayListWrapper
extern TypeInfo FixedSizeArrayListWrapper_t869_il2cpp_TypeInfo;
// System.Collections.ArrayList/FixedSizeArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_FixedSizeArrayListWrapMethodDeclarations.h"
static const EncodedMethodIndex FixedSizeArrayListWrapper_t869_VTable[46] = 
{
	120,
	125,
	122,
	123,
	1780,
	1781,
	1782,
	1783,
	1784,
	1785,
	1836,
	1837,
	1788,
	1789,
	1838,
	1839,
	1840,
	1784,
	1785,
	1781,
	1841,
	1842,
	1795,
	1796,
	1782,
	1836,
	1837,
	1788,
	1789,
	1797,
	1798,
	1838,
	1843,
	1839,
	1840,
	1800,
	1783,
	1801,
	1780,
	1844,
	1803,
	1804,
	1805,
	1806,
	1807,
	1845,
};
static Il2CppInterfaceOffsetPair FixedSizeArrayListWrapper_t869_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 5},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FixedSizeArrayListWrapper_t869_1_0_0;
struct FixedSizeArrayListWrapper_t869;
const Il2CppTypeDefinitionMetadata FixedSizeArrayListWrapper_t869_DefinitionMetadata = 
{
	&ArrayList_t395_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FixedSizeArrayListWrapper_t869_InterfacesOffsets/* interfaceOffsets */
	, &ArrayListWrapper_t867_0_0_0/* parent */
	, FixedSizeArrayListWrapper_t869_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5107/* methodStart */
	, -1/* eventStart */
	, 961/* propertyStart */

};
TypeInfo FixedSizeArrayListWrapper_t869_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FixedSizeArrayListWrapper"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &FixedSizeArrayListWrapper_t869_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FixedSizeArrayListWrapper_t869_0_0_0/* byval_arg */
	, &FixedSizeArrayListWrapper_t869_1_0_0/* this_arg */
	, &FixedSizeArrayListWrapper_t869_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FixedSizeArrayListWrapper_t869)/* instance_size */
	, sizeof (FixedSizeArrayListWrapper_t869)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 46/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.ArrayList/ReadOnlyArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_ReadOnlyArrayListWrapp.h"
// Metadata Definition System.Collections.ArrayList/ReadOnlyArrayListWrapper
extern TypeInfo ReadOnlyArrayListWrapper_t870_il2cpp_TypeInfo;
// System.Collections.ArrayList/ReadOnlyArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_ReadOnlyArrayListWrappMethodDeclarations.h"
static const EncodedMethodIndex ReadOnlyArrayListWrapper_t870_VTable[46] = 
{
	120,
	125,
	122,
	123,
	1780,
	1781,
	1782,
	1783,
	1846,
	1847,
	1836,
	1837,
	1788,
	1789,
	1838,
	1839,
	1840,
	1846,
	1847,
	1781,
	1841,
	1842,
	1848,
	1796,
	1782,
	1836,
	1837,
	1788,
	1789,
	1797,
	1798,
	1838,
	1843,
	1839,
	1840,
	1800,
	1783,
	1801,
	1780,
	1844,
	1849,
	1850,
	1805,
	1806,
	1807,
	1851,
};
static Il2CppInterfaceOffsetPair ReadOnlyArrayListWrapper_t870_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 5},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReadOnlyArrayListWrapper_t870_1_0_0;
struct ReadOnlyArrayListWrapper_t870;
const Il2CppTypeDefinitionMetadata ReadOnlyArrayListWrapper_t870_DefinitionMetadata = 
{
	&ArrayList_t395_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReadOnlyArrayListWrapper_t870_InterfacesOffsets/* interfaceOffsets */
	, &FixedSizeArrayListWrapper_t869_0_0_0/* parent */
	, ReadOnlyArrayListWrapper_t870_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5118/* methodStart */
	, -1/* eventStart */
	, 963/* propertyStart */

};
TypeInfo ReadOnlyArrayListWrapper_t870_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReadOnlyArrayListWrapper"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ReadOnlyArrayListWrapper_t870_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 824/* custom_attributes_cache */
	, &ReadOnlyArrayListWrapper_t870_0_0_0/* byval_arg */
	, &ReadOnlyArrayListWrapper_t870_1_0_0/* this_arg */
	, &ReadOnlyArrayListWrapper_t870_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReadOnlyArrayListWrapper_t870)/* instance_size */
	, sizeof (ReadOnlyArrayListWrapper_t870)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057027/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 46/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.BitArray
#include "mscorlib_System_Collections_BitArray.h"
// Metadata Definition System.Collections.BitArray
extern TypeInfo BitArray_t542_il2cpp_TypeInfo;
// System.Collections.BitArray
#include "mscorlib_System_Collections_BitArrayMethodDeclarations.h"
extern const Il2CppType BitArrayEnumerator_t871_0_0_0;
static const Il2CppType* BitArray_t542_il2cpp_TypeInfo__nestedTypes[1] =
{
	&BitArrayEnumerator_t871_0_0_0,
};
static const EncodedMethodIndex BitArray_t542_VTable[8] = 
{
	120,
	125,
	122,
	123,
	1852,
	1853,
	1854,
	1855,
};
static const Il2CppType* BitArray_t542_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICloneable_t2056_0_0_0,
	&ICollection_t569_0_0_0,
};
static Il2CppInterfaceOffsetPair BitArray_t542_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 5},
	{ &ICollection_t569_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BitArray_t542_0_0_0;
extern const Il2CppType BitArray_t542_1_0_0;
struct BitArray_t542;
const Il2CppTypeDefinitionMetadata BitArray_t542_DefinitionMetadata = 
{
	NULL/* declaringType */
	, BitArray_t542_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, BitArray_t542_InterfacesTypeInfos/* implementedInterfaces */
	, BitArray_t542_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BitArray_t542_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2996/* fieldStart */
	, 5125/* methodStart */
	, -1/* eventStart */
	, 966/* propertyStart */

};
TypeInfo BitArray_t542_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BitArray"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &BitArray_t542_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 825/* custom_attributes_cache */
	, &BitArray_t542_0_0_0/* byval_arg */
	, &BitArray_t542_1_0_0/* this_arg */
	, &BitArray_t542_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BitArray_t542)/* instance_size */
	, sizeof (BitArray_t542)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.BitArray/BitArrayEnumerator
#include "mscorlib_System_Collections_BitArray_BitArrayEnumerator.h"
// Metadata Definition System.Collections.BitArray/BitArrayEnumerator
extern TypeInfo BitArrayEnumerator_t871_il2cpp_TypeInfo;
// System.Collections.BitArray/BitArrayEnumerator
#include "mscorlib_System_Collections_BitArray_BitArrayEnumeratorMethodDeclarations.h"
static const EncodedMethodIndex BitArrayEnumerator_t871_VTable[6] = 
{
	120,
	125,
	122,
	123,
	1856,
	1857,
};
static const Il2CppType* BitArrayEnumerator_t871_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&ICloneable_t2056_0_0_0,
};
static Il2CppInterfaceOffsetPair BitArrayEnumerator_t871_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BitArrayEnumerator_t871_1_0_0;
struct BitArrayEnumerator_t871;
const Il2CppTypeDefinitionMetadata BitArrayEnumerator_t871_DefinitionMetadata = 
{
	&BitArray_t542_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, BitArrayEnumerator_t871_InterfacesTypeInfos/* implementedInterfaces */
	, BitArrayEnumerator_t871_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BitArrayEnumerator_t871_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2999/* fieldStart */
	, 5136/* methodStart */
	, -1/* eventStart */
	, 970/* propertyStart */

};
TypeInfo BitArrayEnumerator_t871_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BitArrayEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &BitArrayEnumerator_t871_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BitArrayEnumerator_t871_0_0_0/* byval_arg */
	, &BitArrayEnumerator_t871_1_0_0/* this_arg */
	, &BitArrayEnumerator_t871_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BitArrayEnumerator_t871)/* instance_size */
	, sizeof (BitArrayEnumerator_t871)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.CaseInsensitiveComparer
#include "mscorlib_System_Collections_CaseInsensitiveComparer.h"
// Metadata Definition System.Collections.CaseInsensitiveComparer
extern TypeInfo CaseInsensitiveComparer_t571_il2cpp_TypeInfo;
// System.Collections.CaseInsensitiveComparer
#include "mscorlib_System_Collections_CaseInsensitiveComparerMethodDeclarations.h"
static const EncodedMethodIndex CaseInsensitiveComparer_t571_VTable[5] = 
{
	120,
	125,
	122,
	123,
	1858,
};
static const Il2CppType* CaseInsensitiveComparer_t571_InterfacesTypeInfos[] = 
{
	&IComparer_t390_0_0_0,
};
static Il2CppInterfaceOffsetPair CaseInsensitiveComparer_t571_InterfacesOffsets[] = 
{
	{ &IComparer_t390_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CaseInsensitiveComparer_t571_0_0_0;
extern const Il2CppType CaseInsensitiveComparer_t571_1_0_0;
struct CaseInsensitiveComparer_t571;
const Il2CppTypeDefinitionMetadata CaseInsensitiveComparer_t571_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CaseInsensitiveComparer_t571_InterfacesTypeInfos/* implementedInterfaces */
	, CaseInsensitiveComparer_t571_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CaseInsensitiveComparer_t571_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3003/* fieldStart */
	, 5140/* methodStart */
	, -1/* eventStart */
	, 971/* propertyStart */

};
TypeInfo CaseInsensitiveComparer_t571_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CaseInsensitiveComparer"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &CaseInsensitiveComparer_t571_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 826/* custom_attributes_cache */
	, &CaseInsensitiveComparer_t571_0_0_0/* byval_arg */
	, &CaseInsensitiveComparer_t571_1_0_0/* this_arg */
	, &CaseInsensitiveComparer_t571_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CaseInsensitiveComparer_t571)/* instance_size */
	, sizeof (CaseInsensitiveComparer_t571)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CaseInsensitiveComparer_t571_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Collections.CaseInsensitiveHashCodeProvider
#include "mscorlib_System_Collections_CaseInsensitiveHashCodeProvider.h"
// Metadata Definition System.Collections.CaseInsensitiveHashCodeProvider
extern TypeInfo CaseInsensitiveHashCodeProvider_t572_il2cpp_TypeInfo;
// System.Collections.CaseInsensitiveHashCodeProvider
#include "mscorlib_System_Collections_CaseInsensitiveHashCodeProviderMethodDeclarations.h"
static const EncodedMethodIndex CaseInsensitiveHashCodeProvider_t572_VTable[5] = 
{
	120,
	125,
	122,
	123,
	1859,
};
extern const Il2CppType IHashCodeProvider_t396_0_0_0;
static const Il2CppType* CaseInsensitiveHashCodeProvider_t572_InterfacesTypeInfos[] = 
{
	&IHashCodeProvider_t396_0_0_0,
};
static Il2CppInterfaceOffsetPair CaseInsensitiveHashCodeProvider_t572_InterfacesOffsets[] = 
{
	{ &IHashCodeProvider_t396_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CaseInsensitiveHashCodeProvider_t572_0_0_0;
extern const Il2CppType CaseInsensitiveHashCodeProvider_t572_1_0_0;
struct CaseInsensitiveHashCodeProvider_t572;
const Il2CppTypeDefinitionMetadata CaseInsensitiveHashCodeProvider_t572_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CaseInsensitiveHashCodeProvider_t572_InterfacesTypeInfos/* implementedInterfaces */
	, CaseInsensitiveHashCodeProvider_t572_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CaseInsensitiveHashCodeProvider_t572_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3006/* fieldStart */
	, 5145/* methodStart */
	, -1/* eventStart */
	, 972/* propertyStart */

};
TypeInfo CaseInsensitiveHashCodeProvider_t572_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CaseInsensitiveHashCodeProvider"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &CaseInsensitiveHashCodeProvider_t572_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 827/* custom_attributes_cache */
	, &CaseInsensitiveHashCodeProvider_t572_0_0_0/* byval_arg */
	, &CaseInsensitiveHashCodeProvider_t572_1_0_0/* this_arg */
	, &CaseInsensitiveHashCodeProvider_t572_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CaseInsensitiveHashCodeProvider_t572)/* instance_size */
	, sizeof (CaseInsensitiveHashCodeProvider_t572)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CaseInsensitiveHashCodeProvider_t572_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Collections.CollectionBase
#include "mscorlib_System_Collections_CollectionBase.h"
// Metadata Definition System.Collections.CollectionBase
extern TypeInfo CollectionBase_t453_il2cpp_TypeInfo;
// System.Collections.CollectionBase
#include "mscorlib_System_Collections_CollectionBaseMethodDeclarations.h"
static const EncodedMethodIndex CollectionBase_t453_VTable[26] = 
{
	120,
	125,
	122,
	123,
	564,
	565,
	566,
	567,
	568,
	569,
	570,
	571,
	572,
	573,
	574,
	575,
	576,
	577,
	578,
	579,
	580,
	581,
	582,
	583,
	584,
	585,
};
static const Il2CppType* CollectionBase_t453_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_t569_0_0_0,
	&IList_t519_0_0_0,
};
static Il2CppInterfaceOffsetPair CollectionBase_t453_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IList_t519_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CollectionBase_t453_1_0_0;
struct CollectionBase_t453;
const Il2CppTypeDefinitionMetadata CollectionBase_t453_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CollectionBase_t453_InterfacesTypeInfos/* implementedInterfaces */
	, CollectionBase_t453_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CollectionBase_t453_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3009/* fieldStart */
	, 5152/* methodStart */
	, -1/* eventStart */
	, 973/* propertyStart */

};
TypeInfo CollectionBase_t453_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CollectionBase"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &CollectionBase_t453_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 828/* custom_attributes_cache */
	, &CollectionBase_t453_0_0_0/* byval_arg */
	, &CollectionBase_t453_1_0_0/* this_arg */
	, &CollectionBase_t453_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CollectionBase_t453)/* instance_size */
	, sizeof (CollectionBase_t453)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 25/* method_count */
	, 5/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.CollectionDebuggerView
#include "mscorlib_System_Collections_CollectionDebuggerView.h"
// Metadata Definition System.Collections.CollectionDebuggerView
extern TypeInfo CollectionDebuggerView_t872_il2cpp_TypeInfo;
// System.Collections.CollectionDebuggerView
#include "mscorlib_System_Collections_CollectionDebuggerViewMethodDeclarations.h"
static const EncodedMethodIndex CollectionDebuggerView_t872_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CollectionDebuggerView_t872_0_0_0;
extern const Il2CppType CollectionDebuggerView_t872_1_0_0;
struct CollectionDebuggerView_t872;
const Il2CppTypeDefinitionMetadata CollectionDebuggerView_t872_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CollectionDebuggerView_t872_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CollectionDebuggerView_t872_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CollectionDebuggerView"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &CollectionDebuggerView_t872_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CollectionDebuggerView_t872_0_0_0/* byval_arg */
	, &CollectionDebuggerView_t872_1_0_0/* this_arg */
	, &CollectionDebuggerView_t872_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CollectionDebuggerView_t872)/* instance_size */
	, sizeof (CollectionDebuggerView_t872)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Comparer
#include "mscorlib_System_Collections_Comparer.h"
// Metadata Definition System.Collections.Comparer
extern TypeInfo Comparer_t873_il2cpp_TypeInfo;
// System.Collections.Comparer
#include "mscorlib_System_Collections_ComparerMethodDeclarations.h"
static const EncodedMethodIndex Comparer_t873_VTable[6] = 
{
	120,
	125,
	122,
	123,
	1860,
	1861,
};
static const Il2CppType* Comparer_t873_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
	&IComparer_t390_0_0_0,
};
static Il2CppInterfaceOffsetPair Comparer_t873_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &IComparer_t390_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Comparer_t873_0_0_0;
extern const Il2CppType Comparer_t873_1_0_0;
struct Comparer_t873;
const Il2CppTypeDefinitionMetadata Comparer_t873_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Comparer_t873_InterfacesTypeInfos/* implementedInterfaces */
	, Comparer_t873_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Comparer_t873_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3010/* fieldStart */
	, 5177/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Comparer_t873_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparer"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &Comparer_t873_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 829/* custom_attributes_cache */
	, &Comparer_t873_0_0_0/* byval_arg */
	, &Comparer_t873_1_0_0/* this_arg */
	, &Comparer_t873_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Comparer_t873)/* instance_size */
	, sizeof (Comparer_t873)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Comparer_t873_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// Metadata Definition System.Collections.DictionaryEntry
extern TypeInfo DictionaryEntry_t560_il2cpp_TypeInfo;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
static const EncodedMethodIndex DictionaryEntry_t560_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DictionaryEntry_t560_0_0_0;
extern const Il2CppType DictionaryEntry_t560_1_0_0;
const Il2CppTypeDefinitionMetadata DictionaryEntry_t560_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, DictionaryEntry_t560_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3013/* fieldStart */
	, 5182/* methodStart */
	, -1/* eventStart */
	, 978/* propertyStart */

};
TypeInfo DictionaryEntry_t560_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DictionaryEntry"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &DictionaryEntry_t560_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 830/* custom_attributes_cache */
	, &DictionaryEntry_t560_0_0_0/* byval_arg */
	, &DictionaryEntry_t560_1_0_0/* this_arg */
	, &DictionaryEntry_t560_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DictionaryEntry_t560)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DictionaryEntry_t560)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Hashtable
#include "mscorlib_System_Collections_Hashtable.h"
// Metadata Definition System.Collections.Hashtable
extern TypeInfo Hashtable_t385_il2cpp_TypeInfo;
// System.Collections.Hashtable
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
extern const Il2CppType Slot_t874_0_0_0;
extern const Il2CppType KeyMarker_t875_0_0_0;
extern const Il2CppType EnumeratorMode_t876_0_0_0;
extern const Il2CppType Enumerator_t877_0_0_0;
extern const Il2CppType HashKeys_t878_0_0_0;
extern const Il2CppType HashValues_t879_0_0_0;
static const Il2CppType* Hashtable_t385_il2cpp_TypeInfo__nestedTypes[6] =
{
	&Slot_t874_0_0_0,
	&KeyMarker_t875_0_0_0,
	&EnumeratorMode_t876_0_0_0,
	&Enumerator_t877_0_0_0,
	&HashKeys_t878_0_0_0,
	&HashValues_t879_0_0_0,
};
static const EncodedMethodIndex Hashtable_t385_VTable[34] = 
{
	120,
	125,
	122,
	123,
	1862,
	1863,
	1864,
	1865,
	1866,
	1867,
	1868,
	1869,
	1870,
	1871,
	1872,
	1873,
	1864,
	1865,
	1874,
	1875,
	1867,
	1868,
	1866,
	1869,
	1876,
	1870,
	1871,
	1872,
	1877,
	1878,
	1863,
	1873,
	1879,
	1880,
};
static const Il2CppType* Hashtable_t385_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICloneable_t2056_0_0_0,
	&ISerializable_t1426_0_0_0,
	&ICollection_t569_0_0_0,
	&IDictionary_t493_0_0_0,
	&IDeserializationCallback_t1429_0_0_0,
};
static Il2CppInterfaceOffsetPair Hashtable_t385_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 5},
	{ &ISerializable_t1426_0_0_0, 5},
	{ &ICollection_t569_0_0_0, 6},
	{ &IDictionary_t493_0_0_0, 9},
	{ &IDeserializationCallback_t1429_0_0_0, 15},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Hashtable_t385_0_0_0;
extern const Il2CppType Hashtable_t385_1_0_0;
struct Hashtable_t385;
const Il2CppTypeDefinitionMetadata Hashtable_t385_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Hashtable_t385_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Hashtable_t385_InterfacesTypeInfos/* implementedInterfaces */
	, Hashtable_t385_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Hashtable_t385_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3015/* fieldStart */
	, 5185/* methodStart */
	, -1/* eventStart */
	, 980/* propertyStart */

};
TypeInfo Hashtable_t385_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Hashtable"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &Hashtable_t385_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 831/* custom_attributes_cache */
	, &Hashtable_t385_0_0_0/* byval_arg */
	, &Hashtable_t385_1_0_0/* this_arg */
	, &Hashtable_t385_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Hashtable_t385)/* instance_size */
	, sizeof (Hashtable_t385)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Hashtable_t385_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 43/* method_count */
	, 7/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 6/* nested_type_count */
	, 34/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_Slot.h"
// Metadata Definition System.Collections.Hashtable/Slot
extern TypeInfo Slot_t874_il2cpp_TypeInfo;
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_SlotMethodDeclarations.h"
static const EncodedMethodIndex Slot_t874_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Slot_t874_1_0_0;
const Il2CppTypeDefinitionMetadata Slot_t874_DefinitionMetadata = 
{
	&Hashtable_t385_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Slot_t874_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3029/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Slot_t874_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Slot"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Slot_t874_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Slot_t874_0_0_0/* byval_arg */
	, &Slot_t874_1_0_0/* this_arg */
	, &Slot_t874_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Slot_t874)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Slot_t874)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057037/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Hashtable/KeyMarker
#include "mscorlib_System_Collections_Hashtable_KeyMarker.h"
// Metadata Definition System.Collections.Hashtable/KeyMarker
extern TypeInfo KeyMarker_t875_il2cpp_TypeInfo;
// System.Collections.Hashtable/KeyMarker
#include "mscorlib_System_Collections_Hashtable_KeyMarkerMethodDeclarations.h"
static const EncodedMethodIndex KeyMarker_t875_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyMarker_t875_1_0_0;
struct KeyMarker_t875;
const Il2CppTypeDefinitionMetadata KeyMarker_t875_DefinitionMetadata = 
{
	&Hashtable_t385_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeyMarker_t875_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3031/* fieldStart */
	, 5228/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyMarker_t875_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyMarker"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &KeyMarker_t875_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyMarker_t875_0_0_0/* byval_arg */
	, &KeyMarker_t875_1_0_0/* this_arg */
	, &KeyMarker_t875_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyMarker_t875)/* instance_size */
	, sizeof (KeyMarker_t875)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(KeyMarker_t875_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056773/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Hashtable/EnumeratorMode
#include "mscorlib_System_Collections_Hashtable_EnumeratorMode.h"
// Metadata Definition System.Collections.Hashtable/EnumeratorMode
extern TypeInfo EnumeratorMode_t876_il2cpp_TypeInfo;
// System.Collections.Hashtable/EnumeratorMode
#include "mscorlib_System_Collections_Hashtable_EnumeratorModeMethodDeclarations.h"
static const EncodedMethodIndex EnumeratorMode_t876_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair EnumeratorMode_t876_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnumeratorMode_t876_1_0_0;
const Il2CppTypeDefinitionMetadata EnumeratorMode_t876_DefinitionMetadata = 
{
	&Hashtable_t385_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EnumeratorMode_t876_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, EnumeratorMode_t876_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3032/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EnumeratorMode_t876_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnumeratorMode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnumeratorMode_t876_0_0_0/* byval_arg */
	, &EnumeratorMode_t876_1_0_0/* this_arg */
	, &EnumeratorMode_t876_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnumeratorMode_t876)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EnumeratorMode_t876)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.Hashtable/Enumerator
#include "mscorlib_System_Collections_Hashtable_Enumerator.h"
// Metadata Definition System.Collections.Hashtable/Enumerator
extern TypeInfo Enumerator_t877_il2cpp_TypeInfo;
// System.Collections.Hashtable/Enumerator
#include "mscorlib_System_Collections_Hashtable_EnumeratorMethodDeclarations.h"
static const EncodedMethodIndex Enumerator_t877_VTable[10] = 
{
	120,
	125,
	122,
	123,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
};
static const Il2CppType* Enumerator_t877_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDictionaryEnumerator_t559_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t877_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &IDictionaryEnumerator_t559_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t877_1_0_0;
struct Enumerator_t877;
const Il2CppTypeDefinitionMetadata Enumerator_t877_DefinitionMetadata = 
{
	&Hashtable_t385_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t877_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t877_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t877_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3036/* fieldStart */
	, 5230/* methodStart */
	, -1/* eventStart */
	, 987/* propertyStart */

};
TypeInfo Enumerator_t877_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t877_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t877_0_0_0/* byval_arg */
	, &Enumerator_t877_1_0_0/* this_arg */
	, &Enumerator_t877_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t877)/* instance_size */
	, sizeof (Enumerator_t877)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Enumerator_t877_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057027/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 4/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Hashtable/HashKeys
#include "mscorlib_System_Collections_Hashtable_HashKeys.h"
// Metadata Definition System.Collections.Hashtable/HashKeys
extern TypeInfo HashKeys_t878_il2cpp_TypeInfo;
// System.Collections.Hashtable/HashKeys
#include "mscorlib_System_Collections_Hashtable_HashKeysMethodDeclarations.h"
static const EncodedMethodIndex HashKeys_t878_VTable[12] = 
{
	120,
	125,
	122,
	123,
	1887,
	1888,
	1889,
	1890,
	1888,
	1889,
	1890,
	1887,
};
static const Il2CppType* HashKeys_t878_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_t569_0_0_0,
};
static Il2CppInterfaceOffsetPair HashKeys_t878_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HashKeys_t878_1_0_0;
struct HashKeys_t878;
const Il2CppTypeDefinitionMetadata HashKeys_t878_DefinitionMetadata = 
{
	&Hashtable_t385_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, HashKeys_t878_InterfacesTypeInfos/* implementedInterfaces */
	, HashKeys_t878_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HashKeys_t878_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3044/* fieldStart */
	, 5239/* methodStart */
	, -1/* eventStart */
	, 991/* propertyStart */

};
TypeInfo HashKeys_t878_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HashKeys"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &HashKeys_t878_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 842/* custom_attributes_cache */
	, &HashKeys_t878_0_0_0/* byval_arg */
	, &HashKeys_t878_1_0_0/* this_arg */
	, &HashKeys_t878_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HashKeys_t878)/* instance_size */
	, sizeof (HashKeys_t878)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Hashtable/HashValues
#include "mscorlib_System_Collections_Hashtable_HashValues.h"
// Metadata Definition System.Collections.Hashtable/HashValues
extern TypeInfo HashValues_t879_il2cpp_TypeInfo;
// System.Collections.Hashtable/HashValues
#include "mscorlib_System_Collections_Hashtable_HashValuesMethodDeclarations.h"
static const EncodedMethodIndex HashValues_t879_VTable[12] = 
{
	120,
	125,
	122,
	123,
	1891,
	1892,
	1893,
	1894,
	1892,
	1893,
	1894,
	1891,
};
static const Il2CppType* HashValues_t879_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_t569_0_0_0,
};
static Il2CppInterfaceOffsetPair HashValues_t879_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HashValues_t879_1_0_0;
struct HashValues_t879;
const Il2CppTypeDefinitionMetadata HashValues_t879_DefinitionMetadata = 
{
	&Hashtable_t385_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, HashValues_t879_InterfacesTypeInfos/* implementedInterfaces */
	, HashValues_t879_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HashValues_t879_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3045/* fieldStart */
	, 5244/* methodStart */
	, -1/* eventStart */
	, 993/* propertyStart */

};
TypeInfo HashValues_t879_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HashValues"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &HashValues_t879_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 843/* custom_attributes_cache */
	, &HashValues_t879_0_0_0/* byval_arg */
	, &HashValues_t879_1_0_0/* this_arg */
	, &HashValues_t879_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HashValues_t879)/* instance_size */
	, sizeof (HashValues_t879)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.IComparer
extern TypeInfo IComparer_t390_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IComparer_t390_1_0_0;
struct IComparer_t390;
const Il2CppTypeDefinitionMetadata IComparer_t390_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5249/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IComparer_t390_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparer"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IComparer_t390_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 844/* custom_attributes_cache */
	, &IComparer_t390_0_0_0/* byval_arg */
	, &IComparer_t390_1_0_0/* this_arg */
	, &IComparer_t390_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
