﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.InternalRemotingServices
struct InternalRemotingServices_t1122;
// System.Runtime.Remoting.Metadata.SoapAttribute
struct SoapAttribute_t1103;
// System.Object
struct Object_t;

// System.Void System.Runtime.Remoting.InternalRemotingServices::.cctor()
extern "C" void InternalRemotingServices__cctor_m6380 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.SoapAttribute System.Runtime.Remoting.InternalRemotingServices::GetCachedSoapAttribute(System.Object)
extern "C" SoapAttribute_t1103 * InternalRemotingServices_GetCachedSoapAttribute_m6381 (Object_t * __this /* static, unused */, Object_t * ___reflectionObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
