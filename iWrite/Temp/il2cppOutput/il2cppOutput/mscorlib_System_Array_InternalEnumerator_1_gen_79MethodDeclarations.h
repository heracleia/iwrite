﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>
struct InternalEnumerator_1_t1767;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.Emit.ILTokenInfo
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11882_gshared (InternalEnumerator_1_t1767 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11882(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1767 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11882_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11883_gshared (InternalEnumerator_1_t1767 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11883(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1767 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11883_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11884_gshared (InternalEnumerator_1_t1767 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11884(__this, method) (( void (*) (InternalEnumerator_1_t1767 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11884_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11885_gshared (InternalEnumerator_1_t1767 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11885(__this, method) (( bool (*) (InternalEnumerator_1_t1767 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11885_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
extern "C" ILTokenInfo_t959  InternalEnumerator_1_get_Current_m11886_gshared (InternalEnumerator_1_t1767 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11886(__this, method) (( ILTokenInfo_t959  (*) (InternalEnumerator_1_t1767 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11886_gshared)(__this, method)
