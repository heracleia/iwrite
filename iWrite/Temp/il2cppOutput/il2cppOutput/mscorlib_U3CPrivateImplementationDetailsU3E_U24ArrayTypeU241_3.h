﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// <PrivateImplementationDetails>/$ArrayType$124
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU24124_t1379 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24124_t1379__padding[124];
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: <PrivateImplementationDetails>/$ArrayType$124
#pragma pack(push, tp, 1)
struct U24ArrayTypeU24124_t1379_marshaled
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24124_t1379__padding[124];
	};
};
#pragma pack(pop, tp)
