﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct KeyValuePair_2_t1676;
// UnityEngine.Event
struct Event_t76;
struct Event_t76_marshaled;
// System.String
struct String_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18MethodDeclarations.h"
#define KeyValuePair_2__ctor_m11135(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1676 *, Event_t76 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m11042_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m11136(__this, method) (( Event_t76 * (*) (KeyValuePair_2_t1676 *, const MethodInfo*))KeyValuePair_2_get_Key_m11043_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m11137(__this, ___value, method) (( void (*) (KeyValuePair_2_t1676 *, Event_t76 *, const MethodInfo*))KeyValuePair_2_set_Key_m11044_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m11138(__this, method) (( int32_t (*) (KeyValuePair_2_t1676 *, const MethodInfo*))KeyValuePair_2_get_Value_m11045_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m11139(__this, ___value, method) (( void (*) (KeyValuePair_2_t1676 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m11046_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m11140(__this, method) (( String_t* (*) (KeyValuePair_2_t1676 *, const MethodInfo*))KeyValuePair_2_ToString_m11047_gshared)(__this, method)
