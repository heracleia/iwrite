﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>
struct Enumerator_t1719;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t495;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m11599_gshared (Enumerator_t1719 * __this, Dictionary_2_t495 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m11599(__this, ___host, method) (( void (*) (Enumerator_t1719 *, Dictionary_2_t495 *, const MethodInfo*))Enumerator__ctor_m11599_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m11600_gshared (Enumerator_t1719 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m11600(__this, method) (( Object_t * (*) (Enumerator_t1719 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11600_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m11601_gshared (Enumerator_t1719 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m11601(__this, method) (( void (*) (Enumerator_t1719 *, const MethodInfo*))Enumerator_Dispose_m11601_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m11602_gshared (Enumerator_t1719 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m11602(__this, method) (( bool (*) (Enumerator_t1719 *, const MethodInfo*))Enumerator_MoveNext_m11602_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m11603_gshared (Enumerator_t1719 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m11603(__this, method) (( int32_t (*) (Enumerator_t1719 *, const MethodInfo*))Enumerator_get_Current_m11603_gshared)(__this, method)
