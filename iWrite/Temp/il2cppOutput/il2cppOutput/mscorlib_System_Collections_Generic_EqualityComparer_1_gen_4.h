﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Int64>
struct EqualityComparer_1_t1564;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Int64>
struct  EqualityComparer_1_t1564  : public Object_t
{
};
struct EqualityComparer_1_t1564_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int64>::_default
	EqualityComparer_1_t1564 * ____default_0;
};
