﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Security.Cryptography.RSAPKCS1SignatureDeformatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SignatureDefor.h"
// Metadata Definition System.Security.Cryptography.RSAPKCS1SignatureDeformatter
extern TypeInfo RSAPKCS1SignatureDeformatter_t746_il2cpp_TypeInfo;
// System.Security.Cryptography.RSAPKCS1SignatureDeformatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SignatureDeforMethodDeclarations.h"
static const EncodedMethodIndex RSAPKCS1SignatureDeformatter_t746_VTable[7] = 
{
	120,
	125,
	122,
	123,
	2763,
	2764,
	2765,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSAPKCS1SignatureDeformatter_t746_0_0_0;
extern const Il2CppType RSAPKCS1SignatureDeformatter_t746_1_0_0;
extern const Il2CppType AsymmetricSignatureDeformatter_t697_0_0_0;
struct RSAPKCS1SignatureDeformatter_t746;
const Il2CppTypeDefinitionMetadata RSAPKCS1SignatureDeformatter_t746_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricSignatureDeformatter_t697_0_0_0/* parent */
	, RSAPKCS1SignatureDeformatter_t746_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4798/* fieldStart */
	, 7377/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSAPKCS1SignatureDeformatter_t746_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAPKCS1SignatureDeformatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAPKCS1SignatureDeformatter_t746_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1221/* custom_attributes_cache */
	, &RSAPKCS1SignatureDeformatter_t746_0_0_0/* byval_arg */
	, &RSAPKCS1SignatureDeformatter_t746_1_0_0/* this_arg */
	, &RSAPKCS1SignatureDeformatter_t746_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSAPKCS1SignatureDeformatter_t746)/* instance_size */
	, sizeof (RSAPKCS1SignatureDeformatter_t746)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.RSAPKCS1SignatureFormatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SignatureForma.h"
// Metadata Definition System.Security.Cryptography.RSAPKCS1SignatureFormatter
extern TypeInfo RSAPKCS1SignatureFormatter_t1198_il2cpp_TypeInfo;
// System.Security.Cryptography.RSAPKCS1SignatureFormatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SignatureFormaMethodDeclarations.h"
static const EncodedMethodIndex RSAPKCS1SignatureFormatter_t1198_VTable[7] = 
{
	120,
	125,
	122,
	123,
	2766,
	2767,
	2768,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSAPKCS1SignatureFormatter_t1198_0_0_0;
extern const Il2CppType RSAPKCS1SignatureFormatter_t1198_1_0_0;
extern const Il2CppType AsymmetricSignatureFormatter_t699_0_0_0;
struct RSAPKCS1SignatureFormatter_t1198;
const Il2CppTypeDefinitionMetadata RSAPKCS1SignatureFormatter_t1198_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricSignatureFormatter_t699_0_0_0/* parent */
	, RSAPKCS1SignatureFormatter_t1198_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4800/* fieldStart */
	, 7382/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSAPKCS1SignatureFormatter_t1198_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAPKCS1SignatureFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAPKCS1SignatureFormatter_t1198_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1222/* custom_attributes_cache */
	, &RSAPKCS1SignatureFormatter_t1198_0_0_0/* byval_arg */
	, &RSAPKCS1SignatureFormatter_t1198_1_0_0/* this_arg */
	, &RSAPKCS1SignatureFormatter_t1198_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSAPKCS1SignatureFormatter_t1198)/* instance_size */
	, sizeof (RSAPKCS1SignatureFormatter_t1198)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.RSAParameters
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
// Metadata Definition System.Security.Cryptography.RSAParameters
extern TypeInfo RSAParameters_t584_il2cpp_TypeInfo;
// System.Security.Cryptography.RSAParameters
#include "mscorlib_System_Security_Cryptography_RSAParametersMethodDeclarations.h"
static const EncodedMethodIndex RSAParameters_t584_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSAParameters_t584_0_0_0;
extern const Il2CppType RSAParameters_t584_1_0_0;
extern const Il2CppType ValueType_t757_0_0_0;
const Il2CppTypeDefinitionMetadata RSAParameters_t584_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, RSAParameters_t584_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4802/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSAParameters_t584_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAParameters"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAParameters_t584_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1223/* custom_attributes_cache */
	, &RSAParameters_t584_0_0_0/* byval_arg */
	, &RSAParameters_t584_1_0_0/* this_arg */
	, &RSAParameters_t584_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)RSAParameters_t584_marshal/* marshal_to_native_func */
	, (methodPointerType)RSAParameters_t584_marshal_back/* marshal_from_native_func */
	, (methodPointerType)RSAParameters_t584_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (RSAParameters_t584)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RSAParameters_t584)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RSAParameters_t584_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.RandomNumberGenerator
#include "mscorlib_System_Security_Cryptography_RandomNumberGenerator.h"
// Metadata Definition System.Security.Cryptography.RandomNumberGenerator
extern TypeInfo RandomNumberGenerator_t604_il2cpp_TypeInfo;
// System.Security.Cryptography.RandomNumberGenerator
#include "mscorlib_System_Security_Cryptography_RandomNumberGeneratorMethodDeclarations.h"
static const EncodedMethodIndex RandomNumberGenerator_t604_VTable[6] = 
{
	120,
	125,
	122,
	123,
	0,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RandomNumberGenerator_t604_0_0_0;
extern const Il2CppType RandomNumberGenerator_t604_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct RandomNumberGenerator_t604;
const Il2CppTypeDefinitionMetadata RandomNumberGenerator_t604_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RandomNumberGenerator_t604_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7386/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RandomNumberGenerator_t604_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RandomNumberGenerator"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RandomNumberGenerator_t604_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RandomNumberGenerator_t604_0_0_0/* byval_arg */
	, &RandomNumberGenerator_t604_1_0_0/* this_arg */
	, &RandomNumberGenerator_t604_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RandomNumberGenerator_t604)/* instance_size */
	, sizeof (RandomNumberGenerator_t604)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.Rijndael
#include "mscorlib_System_Security_Cryptography_Rijndael.h"
// Metadata Definition System.Security.Cryptography.Rijndael
extern TypeInfo Rijndael_t752_il2cpp_TypeInfo;
// System.Security.Cryptography.Rijndael
#include "mscorlib_System_Security_Cryptography_RijndaelMethodDeclarations.h"
static const EncodedMethodIndex Rijndael_t752_VTable[26] = 
{
	120,
	759,
	122,
	123,
	760,
	761,
	762,
	763,
	764,
	765,
	766,
	767,
	768,
	769,
	770,
	771,
	772,
	773,
	774,
	775,
	776,
	0,
	777,
	0,
	0,
	0,
};
extern const Il2CppType IDisposable_t319_0_0_0;
static Il2CppInterfaceOffsetPair Rijndael_t752_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Rijndael_t752_0_0_0;
extern const Il2CppType Rijndael_t752_1_0_0;
extern const Il2CppType SymmetricAlgorithm_t606_0_0_0;
struct Rijndael_t752;
const Il2CppTypeDefinitionMetadata Rijndael_t752_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Rijndael_t752_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricAlgorithm_t606_0_0_0/* parent */
	, Rijndael_t752_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7391/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Rijndael_t752_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Rijndael"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Rijndael_t752_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1224/* custom_attributes_cache */
	, &Rijndael_t752_0_0_0/* byval_arg */
	, &Rijndael_t752_1_0_0/* this_arg */
	, &Rijndael_t752_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Rijndael_t752)/* instance_size */
	, sizeof (Rijndael_t752)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.RijndaelManaged
#include "mscorlib_System_Security_Cryptography_RijndaelManaged.h"
// Metadata Definition System.Security.Cryptography.RijndaelManaged
extern TypeInfo RijndaelManaged_t1199_il2cpp_TypeInfo;
// System.Security.Cryptography.RijndaelManaged
#include "mscorlib_System_Security_Cryptography_RijndaelManagedMethodDeclarations.h"
static const EncodedMethodIndex RijndaelManaged_t1199_VTable[26] = 
{
	120,
	759,
	122,
	123,
	760,
	761,
	762,
	763,
	764,
	765,
	766,
	767,
	768,
	769,
	770,
	771,
	772,
	773,
	774,
	775,
	776,
	2769,
	777,
	2770,
	2771,
	2772,
};
static Il2CppInterfaceOffsetPair RijndaelManaged_t1199_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RijndaelManaged_t1199_0_0_0;
extern const Il2CppType RijndaelManaged_t1199_1_0_0;
struct RijndaelManaged_t1199;
const Il2CppTypeDefinitionMetadata RijndaelManaged_t1199_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RijndaelManaged_t1199_InterfacesOffsets/* interfaceOffsets */
	, &Rijndael_t752_0_0_0/* parent */
	, RijndaelManaged_t1199_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7394/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RijndaelManaged_t1199_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RijndaelManaged"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RijndaelManaged_t1199_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1225/* custom_attributes_cache */
	, &RijndaelManaged_t1199_0_0_0/* byval_arg */
	, &RijndaelManaged_t1199_1_0_0/* this_arg */
	, &RijndaelManaged_t1199_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RijndaelManaged_t1199)/* instance_size */
	, sizeof (RijndaelManaged_t1199)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.RijndaelTransform
#include "mscorlib_System_Security_Cryptography_RijndaelTransform.h"
// Metadata Definition System.Security.Cryptography.RijndaelTransform
extern TypeInfo RijndaelTransform_t1200_il2cpp_TypeInfo;
// System.Security.Cryptography.RijndaelTransform
#include "mscorlib_System_Security_Cryptography_RijndaelTransformMethodDeclarations.h"
static const EncodedMethodIndex RijndaelTransform_t1200_VTable[18] = 
{
	120,
	1534,
	122,
	123,
	1535,
	1536,
	1537,
	1538,
	1539,
	1536,
	1540,
	2773,
	1541,
	1542,
	1543,
	1544,
	1537,
	1538,
};
extern const Il2CppType ICryptoTransform_t616_0_0_0;
static Il2CppInterfaceOffsetPair RijndaelTransform_t1200_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RijndaelTransform_t1200_0_0_0;
extern const Il2CppType RijndaelTransform_t1200_1_0_0;
extern const Il2CppType SymmetricTransform_t839_0_0_0;
struct RijndaelTransform_t1200;
const Il2CppTypeDefinitionMetadata RijndaelTransform_t1200_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RijndaelTransform_t1200_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricTransform_t839_0_0_0/* parent */
	, RijndaelTransform_t1200_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4810/* fieldStart */
	, 7399/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RijndaelTransform_t1200_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RijndaelTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RijndaelTransform_t1200_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RijndaelTransform_t1200_0_0_0/* byval_arg */
	, &RijndaelTransform_t1200_1_0_0/* this_arg */
	, &RijndaelTransform_t1200_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RijndaelTransform_t1200)/* instance_size */
	, sizeof (RijndaelTransform_t1200)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RijndaelTransform_t1200_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.RijndaelManagedTransform
#include "mscorlib_System_Security_Cryptography_RijndaelManagedTransfo.h"
// Metadata Definition System.Security.Cryptography.RijndaelManagedTransform
extern TypeInfo RijndaelManagedTransform_t1201_il2cpp_TypeInfo;
// System.Security.Cryptography.RijndaelManagedTransform
#include "mscorlib_System_Security_Cryptography_RijndaelManagedTransfoMethodDeclarations.h"
static const EncodedMethodIndex RijndaelManagedTransform_t1201_VTable[8] = 
{
	120,
	125,
	122,
	123,
	2774,
	2775,
	2776,
	2777,
};
static const Il2CppType* RijndaelManagedTransform_t1201_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
	&ICryptoTransform_t616_0_0_0,
};
static Il2CppInterfaceOffsetPair RijndaelManagedTransform_t1201_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RijndaelManagedTransform_t1201_0_0_0;
extern const Il2CppType RijndaelManagedTransform_t1201_1_0_0;
struct RijndaelManagedTransform_t1201;
const Il2CppTypeDefinitionMetadata RijndaelManagedTransform_t1201_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RijndaelManagedTransform_t1201_InterfacesTypeInfos/* implementedInterfaces */
	, RijndaelManagedTransform_t1201_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RijndaelManagedTransform_t1201_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4825/* fieldStart */
	, 7410/* methodStart */
	, -1/* eventStart */
	, 1505/* propertyStart */

};
TypeInfo RijndaelManagedTransform_t1201_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RijndaelManagedTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RijndaelManagedTransform_t1201_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1226/* custom_attributes_cache */
	, &RijndaelManagedTransform_t1201_0_0_0/* byval_arg */
	, &RijndaelManagedTransform_t1201_1_0_0/* this_arg */
	, &RijndaelManagedTransform_t1201_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RijndaelManagedTransform_t1201)/* instance_size */
	, sizeof (RijndaelManagedTransform_t1201)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA1
#include "mscorlib_System_Security_Cryptography_SHA1.h"
// Metadata Definition System.Security.Cryptography.SHA1
extern TypeInfo SHA1_t596_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA1
#include "mscorlib_System_Security_Cryptography_SHA1MethodDeclarations.h"
static const EncodedMethodIndex SHA1_t596_VTable[15] = 
{
	120,
	125,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	0,
	0,
	822,
	0,
	823,
};
static Il2CppInterfaceOffsetPair SHA1_t596_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA1_t596_0_0_0;
extern const Il2CppType SHA1_t596_1_0_0;
extern const Il2CppType HashAlgorithm_t642_0_0_0;
struct SHA1_t596;
const Il2CppTypeDefinitionMetadata SHA1_t596_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA1_t596_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t642_0_0_0/* parent */
	, SHA1_t596_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7415/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA1_t596_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA1"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA1_t596_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1227/* custom_attributes_cache */
	, &SHA1_t596_0_0_0/* byval_arg */
	, &SHA1_t596_1_0_0/* this_arg */
	, &SHA1_t596_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA1_t596)/* instance_size */
	, sizeof (SHA1_t596)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA1Internal
#include "mscorlib_System_Security_Cryptography_SHA1Internal.h"
// Metadata Definition System.Security.Cryptography.SHA1Internal
extern TypeInfo SHA1Internal_t1202_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA1Internal
#include "mscorlib_System_Security_Cryptography_SHA1InternalMethodDeclarations.h"
static const EncodedMethodIndex SHA1Internal_t1202_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA1Internal_t1202_0_0_0;
extern const Il2CppType SHA1Internal_t1202_1_0_0;
struct SHA1Internal_t1202;
const Il2CppTypeDefinitionMetadata SHA1Internal_t1202_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SHA1Internal_t1202_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4827/* fieldStart */
	, 7418/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA1Internal_t1202_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA1Internal"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA1Internal_t1202_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SHA1Internal_t1202_0_0_0/* byval_arg */
	, &SHA1Internal_t1202_1_0_0/* this_arg */
	, &SHA1Internal_t1202_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA1Internal_t1202)/* instance_size */
	, sizeof (SHA1Internal_t1202)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.SHA1CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_SHA1CryptoServiceProvi.h"
// Metadata Definition System.Security.Cryptography.SHA1CryptoServiceProvider
extern TypeInfo SHA1CryptoServiceProvider_t1203_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA1CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_SHA1CryptoServiceProviMethodDeclarations.h"
static const EncodedMethodIndex SHA1CryptoServiceProvider_t1203_VTable[15] = 
{
	120,
	2778,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2779,
	2780,
	822,
	2781,
	2782,
};
static Il2CppInterfaceOffsetPair SHA1CryptoServiceProvider_t1203_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA1CryptoServiceProvider_t1203_0_0_0;
extern const Il2CppType SHA1CryptoServiceProvider_t1203_1_0_0;
struct SHA1CryptoServiceProvider_t1203;
const Il2CppTypeDefinitionMetadata SHA1CryptoServiceProvider_t1203_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA1CryptoServiceProvider_t1203_InterfacesOffsets/* interfaceOffsets */
	, &SHA1_t596_0_0_0/* parent */
	, SHA1CryptoServiceProvider_t1203_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4832/* fieldStart */
	, 7427/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA1CryptoServiceProvider_t1203_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA1CryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA1CryptoServiceProvider_t1203_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1228/* custom_attributes_cache */
	, &SHA1CryptoServiceProvider_t1203_0_0_0/* byval_arg */
	, &SHA1CryptoServiceProvider_t1203_1_0_0/* this_arg */
	, &SHA1CryptoServiceProvider_t1203_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA1CryptoServiceProvider_t1203)/* instance_size */
	, sizeof (SHA1CryptoServiceProvider_t1203)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA1Managed
#include "mscorlib_System_Security_Cryptography_SHA1Managed.h"
// Metadata Definition System.Security.Cryptography.SHA1Managed
extern TypeInfo SHA1Managed_t1204_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA1Managed
#include "mscorlib_System_Security_Cryptography_SHA1ManagedMethodDeclarations.h"
static const EncodedMethodIndex SHA1Managed_t1204_VTable[15] = 
{
	120,
	125,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2783,
	2784,
	822,
	2785,
	823,
};
static Il2CppInterfaceOffsetPair SHA1Managed_t1204_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA1Managed_t1204_0_0_0;
extern const Il2CppType SHA1Managed_t1204_1_0_0;
struct SHA1Managed_t1204;
const Il2CppTypeDefinitionMetadata SHA1Managed_t1204_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA1Managed_t1204_InterfacesOffsets/* interfaceOffsets */
	, &SHA1_t596_0_0_0/* parent */
	, SHA1Managed_t1204_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4833/* fieldStart */
	, 7433/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA1Managed_t1204_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA1Managed"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA1Managed_t1204_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1229/* custom_attributes_cache */
	, &SHA1Managed_t1204_0_0_0/* byval_arg */
	, &SHA1Managed_t1204_1_0_0/* this_arg */
	, &SHA1Managed_t1204_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA1Managed_t1204)/* instance_size */
	, sizeof (SHA1Managed_t1204)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA256
#include "mscorlib_System_Security_Cryptography_SHA256.h"
// Metadata Definition System.Security.Cryptography.SHA256
extern TypeInfo SHA256_t744_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA256
#include "mscorlib_System_Security_Cryptography_SHA256MethodDeclarations.h"
static const EncodedMethodIndex SHA256_t744_VTable[15] = 
{
	120,
	125,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	0,
	0,
	822,
	0,
	823,
};
static Il2CppInterfaceOffsetPair SHA256_t744_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA256_t744_0_0_0;
extern const Il2CppType SHA256_t744_1_0_0;
struct SHA256_t744;
const Il2CppTypeDefinitionMetadata SHA256_t744_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA256_t744_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t642_0_0_0/* parent */
	, SHA256_t744_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7437/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA256_t744_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA256"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA256_t744_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1230/* custom_attributes_cache */
	, &SHA256_t744_0_0_0/* byval_arg */
	, &SHA256_t744_1_0_0/* this_arg */
	, &SHA256_t744_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA256_t744)/* instance_size */
	, sizeof (SHA256_t744)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA256Managed
#include "mscorlib_System_Security_Cryptography_SHA256Managed.h"
// Metadata Definition System.Security.Cryptography.SHA256Managed
extern TypeInfo SHA256Managed_t1205_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA256Managed
#include "mscorlib_System_Security_Cryptography_SHA256ManagedMethodDeclarations.h"
static const EncodedMethodIndex SHA256Managed_t1205_VTable[15] = 
{
	120,
	125,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2786,
	2787,
	822,
	2788,
	823,
};
static Il2CppInterfaceOffsetPair SHA256Managed_t1205_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA256Managed_t1205_0_0_0;
extern const Il2CppType SHA256Managed_t1205_1_0_0;
struct SHA256Managed_t1205;
const Il2CppTypeDefinitionMetadata SHA256Managed_t1205_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA256Managed_t1205_InterfacesOffsets/* interfaceOffsets */
	, &SHA256_t744_0_0_0/* parent */
	, SHA256Managed_t1205_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4834/* fieldStart */
	, 7440/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA256Managed_t1205_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA256Managed"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA256Managed_t1205_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1231/* custom_attributes_cache */
	, &SHA256Managed_t1205_0_0_0/* byval_arg */
	, &SHA256Managed_t1205_1_0_0/* this_arg */
	, &SHA256Managed_t1205_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA256Managed_t1205)/* instance_size */
	, sizeof (SHA256Managed_t1205)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA384
#include "mscorlib_System_Security_Cryptography_SHA384.h"
// Metadata Definition System.Security.Cryptography.SHA384
extern TypeInfo SHA384_t1206_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA384
#include "mscorlib_System_Security_Cryptography_SHA384MethodDeclarations.h"
static const EncodedMethodIndex SHA384_t1206_VTable[15] = 
{
	120,
	125,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	0,
	0,
	822,
	0,
	823,
};
static Il2CppInterfaceOffsetPair SHA384_t1206_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA384_t1206_0_0_0;
extern const Il2CppType SHA384_t1206_1_0_0;
struct SHA384_t1206;
const Il2CppTypeDefinitionMetadata SHA384_t1206_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA384_t1206_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t642_0_0_0/* parent */
	, SHA384_t1206_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7447/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA384_t1206_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA384"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA384_t1206_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1232/* custom_attributes_cache */
	, &SHA384_t1206_0_0_0/* byval_arg */
	, &SHA384_t1206_1_0_0/* this_arg */
	, &SHA384_t1206_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA384_t1206)/* instance_size */
	, sizeof (SHA384_t1206)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA384Managed
#include "mscorlib_System_Security_Cryptography_SHA384Managed.h"
// Metadata Definition System.Security.Cryptography.SHA384Managed
extern TypeInfo SHA384Managed_t1208_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA384Managed
#include "mscorlib_System_Security_Cryptography_SHA384ManagedMethodDeclarations.h"
static const EncodedMethodIndex SHA384Managed_t1208_VTable[15] = 
{
	120,
	125,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2789,
	2790,
	822,
	2791,
	823,
};
static Il2CppInterfaceOffsetPair SHA384Managed_t1208_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA384Managed_t1208_0_0_0;
extern const Il2CppType SHA384Managed_t1208_1_0_0;
struct SHA384Managed_t1208;
const Il2CppTypeDefinitionMetadata SHA384Managed_t1208_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA384Managed_t1208_InterfacesOffsets/* interfaceOffsets */
	, &SHA384_t1206_0_0_0/* parent */
	, SHA384Managed_t1208_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4839/* fieldStart */
	, 7448/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA384Managed_t1208_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA384Managed"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA384Managed_t1208_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1233/* custom_attributes_cache */
	, &SHA384Managed_t1208_0_0_0/* byval_arg */
	, &SHA384Managed_t1208_1_0_0/* this_arg */
	, &SHA384Managed_t1208_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA384Managed_t1208)/* instance_size */
	, sizeof (SHA384Managed_t1208)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA512
#include "mscorlib_System_Security_Cryptography_SHA512.h"
// Metadata Definition System.Security.Cryptography.SHA512
extern TypeInfo SHA512_t1209_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA512
#include "mscorlib_System_Security_Cryptography_SHA512MethodDeclarations.h"
static const EncodedMethodIndex SHA512_t1209_VTable[15] = 
{
	120,
	125,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	0,
	0,
	822,
	0,
	823,
};
static Il2CppInterfaceOffsetPair SHA512_t1209_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA512_t1209_0_0_0;
extern const Il2CppType SHA512_t1209_1_0_0;
struct SHA512_t1209;
const Il2CppTypeDefinitionMetadata SHA512_t1209_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA512_t1209_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t642_0_0_0/* parent */
	, SHA512_t1209_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7459/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA512_t1209_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA512"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA512_t1209_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1234/* custom_attributes_cache */
	, &SHA512_t1209_0_0_0/* byval_arg */
	, &SHA512_t1209_1_0_0/* this_arg */
	, &SHA512_t1209_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA512_t1209)/* instance_size */
	, sizeof (SHA512_t1209)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA512Managed
#include "mscorlib_System_Security_Cryptography_SHA512Managed.h"
// Metadata Definition System.Security.Cryptography.SHA512Managed
extern TypeInfo SHA512Managed_t1210_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA512Managed
#include "mscorlib_System_Security_Cryptography_SHA512ManagedMethodDeclarations.h"
static const EncodedMethodIndex SHA512Managed_t1210_VTable[15] = 
{
	120,
	125,
	122,
	123,
	817,
	818,
	819,
	820,
	818,
	821,
	2792,
	2793,
	822,
	2794,
	823,
};
static Il2CppInterfaceOffsetPair SHA512Managed_t1210_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA512Managed_t1210_0_0_0;
extern const Il2CppType SHA512Managed_t1210_1_0_0;
struct SHA512Managed_t1210;
const Il2CppTypeDefinitionMetadata SHA512Managed_t1210_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA512Managed_t1210_InterfacesOffsets/* interfaceOffsets */
	, &SHA512_t1209_0_0_0/* parent */
	, SHA512Managed_t1210_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4853/* fieldStart */
	, 7460/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA512Managed_t1210_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA512Managed"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA512Managed_t1210_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1235/* custom_attributes_cache */
	, &SHA512Managed_t1210_0_0_0/* byval_arg */
	, &SHA512Managed_t1210_1_0_0/* this_arg */
	, &SHA512Managed_t1210_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA512Managed_t1210)/* instance_size */
	, sizeof (SHA512Managed_t1210)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHAConstants
#include "mscorlib_System_Security_Cryptography_SHAConstants.h"
// Metadata Definition System.Security.Cryptography.SHAConstants
extern TypeInfo SHAConstants_t1211_il2cpp_TypeInfo;
// System.Security.Cryptography.SHAConstants
#include "mscorlib_System_Security_Cryptography_SHAConstantsMethodDeclarations.h"
static const EncodedMethodIndex SHAConstants_t1211_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHAConstants_t1211_0_0_0;
extern const Il2CppType SHAConstants_t1211_1_0_0;
struct SHAConstants_t1211;
const Il2CppTypeDefinitionMetadata SHAConstants_t1211_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SHAConstants_t1211_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4867/* fieldStart */
	, 7478/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHAConstants_t1211_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHAConstants"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHAConstants_t1211_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SHAConstants_t1211_0_0_0/* byval_arg */
	, &SHAConstants_t1211_1_0_0/* this_arg */
	, &SHAConstants_t1211_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHAConstants_t1211)/* instance_size */
	, sizeof (SHAConstants_t1211)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SHAConstants_t1211_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.SignatureDescription
#include "mscorlib_System_Security_Cryptography_SignatureDescription.h"
// Metadata Definition System.Security.Cryptography.SignatureDescription
extern TypeInfo SignatureDescription_t1212_il2cpp_TypeInfo;
// System.Security.Cryptography.SignatureDescription
#include "mscorlib_System_Security_Cryptography_SignatureDescriptionMethodDeclarations.h"
static const EncodedMethodIndex SignatureDescription_t1212_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SignatureDescription_t1212_0_0_0;
extern const Il2CppType SignatureDescription_t1212_1_0_0;
struct SignatureDescription_t1212;
const Il2CppTypeDefinitionMetadata SignatureDescription_t1212_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SignatureDescription_t1212_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4869/* fieldStart */
	, 7479/* methodStart */
	, -1/* eventStart */
	, 1506/* propertyStart */

};
TypeInfo SignatureDescription_t1212_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SignatureDescription"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SignatureDescription_t1212_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1236/* custom_attributes_cache */
	, &SignatureDescription_t1212_0_0_0/* byval_arg */
	, &SignatureDescription_t1212_1_0_0/* this_arg */
	, &SignatureDescription_t1212_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SignatureDescription_t1212)/* instance_size */
	, sizeof (SignatureDescription_t1212)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.DSASignatureDescription
#include "mscorlib_System_Security_Cryptography_DSASignatureDescriptio.h"
// Metadata Definition System.Security.Cryptography.DSASignatureDescription
extern TypeInfo DSASignatureDescription_t1213_il2cpp_TypeInfo;
// System.Security.Cryptography.DSASignatureDescription
#include "mscorlib_System_Security_Cryptography_DSASignatureDescriptioMethodDeclarations.h"
static const EncodedMethodIndex DSASignatureDescription_t1213_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSASignatureDescription_t1213_0_0_0;
extern const Il2CppType DSASignatureDescription_t1213_1_0_0;
struct DSASignatureDescription_t1213;
const Il2CppTypeDefinitionMetadata DSASignatureDescription_t1213_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SignatureDescription_t1212_0_0_0/* parent */
	, DSASignatureDescription_t1213_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7484/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DSASignatureDescription_t1213_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSASignatureDescription"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSASignatureDescription_t1213_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DSASignatureDescription_t1213_0_0_0/* byval_arg */
	, &DSASignatureDescription_t1213_1_0_0/* this_arg */
	, &DSASignatureDescription_t1213_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSASignatureDescription_t1213)/* instance_size */
	, sizeof (DSASignatureDescription_t1213)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SHA1SignatureD.h"
// Metadata Definition System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription
extern TypeInfo RSAPKCS1SHA1SignatureDescription_t1214_il2cpp_TypeInfo;
// System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SHA1SignatureDMethodDeclarations.h"
static const EncodedMethodIndex RSAPKCS1SHA1SignatureDescription_t1214_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSAPKCS1SHA1SignatureDescription_t1214_0_0_0;
extern const Il2CppType RSAPKCS1SHA1SignatureDescription_t1214_1_0_0;
struct RSAPKCS1SHA1SignatureDescription_t1214;
const Il2CppTypeDefinitionMetadata RSAPKCS1SHA1SignatureDescription_t1214_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SignatureDescription_t1212_0_0_0/* parent */
	, RSAPKCS1SHA1SignatureDescription_t1214_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7485/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSAPKCS1SHA1SignatureDescription_t1214_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAPKCS1SHA1SignatureDescription"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAPKCS1SHA1SignatureDescription_t1214_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RSAPKCS1SHA1SignatureDescription_t1214_0_0_0/* byval_arg */
	, &RSAPKCS1SHA1SignatureDescription_t1214_1_0_0/* this_arg */
	, &RSAPKCS1SHA1SignatureDescription_t1214_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSAPKCS1SHA1SignatureDescription_t1214)/* instance_size */
	, sizeof (RSAPKCS1SHA1SignatureDescription_t1214)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.SymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_SymmetricAlgorithm.h"
// Metadata Definition System.Security.Cryptography.SymmetricAlgorithm
extern TypeInfo SymmetricAlgorithm_t606_il2cpp_TypeInfo;
// System.Security.Cryptography.SymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_SymmetricAlgorithmMethodDeclarations.h"
static const EncodedMethodIndex SymmetricAlgorithm_t606_VTable[26] = 
{
	120,
	759,
	122,
	123,
	760,
	761,
	762,
	763,
	764,
	765,
	766,
	767,
	768,
	769,
	770,
	771,
	772,
	773,
	774,
	775,
	776,
	0,
	777,
	0,
	0,
	0,
};
static const Il2CppType* SymmetricAlgorithm_t606_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
};
static Il2CppInterfaceOffsetPair SymmetricAlgorithm_t606_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SymmetricAlgorithm_t606_1_0_0;
struct SymmetricAlgorithm_t606;
const Il2CppTypeDefinitionMetadata SymmetricAlgorithm_t606_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SymmetricAlgorithm_t606_InterfacesTypeInfos/* implementedInterfaces */
	, SymmetricAlgorithm_t606_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SymmetricAlgorithm_t606_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4873/* fieldStart */
	, 7486/* methodStart */
	, -1/* eventStart */
	, 1510/* propertyStart */

};
TypeInfo SymmetricAlgorithm_t606_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SymmetricAlgorithm"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SymmetricAlgorithm_t606_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1237/* custom_attributes_cache */
	, &SymmetricAlgorithm_t606_0_0_0/* byval_arg */
	, &SymmetricAlgorithm_t606_1_0_0/* this_arg */
	, &SymmetricAlgorithm_t606_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SymmetricAlgorithm_t606)/* instance_size */
	, sizeof (SymmetricAlgorithm_t606)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 8/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.ToBase64Transform
#include "mscorlib_System_Security_Cryptography_ToBase64Transform.h"
// Metadata Definition System.Security.Cryptography.ToBase64Transform
extern TypeInfo ToBase64Transform_t1215_il2cpp_TypeInfo;
// System.Security.Cryptography.ToBase64Transform
#include "mscorlib_System_Security_Cryptography_ToBase64TransformMethodDeclarations.h"
static const EncodedMethodIndex ToBase64Transform_t1215_VTable[12] = 
{
	120,
	2795,
	122,
	123,
	2796,
	2797,
	2798,
	2799,
	2797,
	2800,
	2801,
	2802,
};
static const Il2CppType* ToBase64Transform_t1215_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
	&ICryptoTransform_t616_0_0_0,
};
static Il2CppInterfaceOffsetPair ToBase64Transform_t1215_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ToBase64Transform_t1215_0_0_0;
extern const Il2CppType ToBase64Transform_t1215_1_0_0;
struct ToBase64Transform_t1215;
const Il2CppTypeDefinitionMetadata ToBase64Transform_t1215_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ToBase64Transform_t1215_InterfacesTypeInfos/* implementedInterfaces */
	, ToBase64Transform_t1215_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ToBase64Transform_t1215_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4883/* fieldStart */
	, 7512/* methodStart */
	, -1/* eventStart */
	, 1518/* propertyStart */

};
TypeInfo ToBase64Transform_t1215_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToBase64Transform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &ToBase64Transform_t1215_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1238/* custom_attributes_cache */
	, &ToBase64Transform_t1215_0_0_0/* byval_arg */
	, &ToBase64Transform_t1215_1_0_0/* this_arg */
	, &ToBase64Transform_t1215_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToBase64Transform_t1215)/* instance_size */
	, sizeof (ToBase64Transform_t1215)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.TripleDES
#include "mscorlib_System_Security_Cryptography_TripleDES.h"
// Metadata Definition System.Security.Cryptography.TripleDES
extern TypeInfo TripleDES_t751_il2cpp_TypeInfo;
// System.Security.Cryptography.TripleDES
#include "mscorlib_System_Security_Cryptography_TripleDESMethodDeclarations.h"
static const EncodedMethodIndex TripleDES_t751_VTable[26] = 
{
	120,
	759,
	122,
	123,
	760,
	761,
	762,
	763,
	764,
	765,
	766,
	2803,
	2804,
	769,
	770,
	771,
	772,
	773,
	774,
	775,
	776,
	0,
	777,
	0,
	0,
	0,
};
static Il2CppInterfaceOffsetPair TripleDES_t751_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TripleDES_t751_0_0_0;
extern const Il2CppType TripleDES_t751_1_0_0;
struct TripleDES_t751;
const Il2CppTypeDefinitionMetadata TripleDES_t751_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TripleDES_t751_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricAlgorithm_t606_0_0_0/* parent */
	, TripleDES_t751_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7522/* methodStart */
	, -1/* eventStart */
	, 1521/* propertyStart */

};
TypeInfo TripleDES_t751_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TripleDES"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &TripleDES_t751_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1239/* custom_attributes_cache */
	, &TripleDES_t751_0_0_0/* byval_arg */
	, &TripleDES_t751_1_0_0/* this_arg */
	, &TripleDES_t751_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TripleDES_t751)/* instance_size */
	, sizeof (TripleDES_t751)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.TripleDESCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_TripleDESCryptoService.h"
// Metadata Definition System.Security.Cryptography.TripleDESCryptoServiceProvider
extern TypeInfo TripleDESCryptoServiceProvider_t1216_il2cpp_TypeInfo;
// System.Security.Cryptography.TripleDESCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_TripleDESCryptoServiceMethodDeclarations.h"
static const EncodedMethodIndex TripleDESCryptoServiceProvider_t1216_VTable[26] = 
{
	120,
	759,
	122,
	123,
	760,
	761,
	762,
	763,
	764,
	765,
	766,
	2803,
	2804,
	769,
	770,
	771,
	772,
	773,
	774,
	775,
	776,
	2805,
	777,
	2806,
	2807,
	2808,
};
static Il2CppInterfaceOffsetPair TripleDESCryptoServiceProvider_t1216_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TripleDESCryptoServiceProvider_t1216_0_0_0;
extern const Il2CppType TripleDESCryptoServiceProvider_t1216_1_0_0;
struct TripleDESCryptoServiceProvider_t1216;
const Il2CppTypeDefinitionMetadata TripleDESCryptoServiceProvider_t1216_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TripleDESCryptoServiceProvider_t1216_InterfacesOffsets/* interfaceOffsets */
	, &TripleDES_t751_0_0_0/* parent */
	, TripleDESCryptoServiceProvider_t1216_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7528/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TripleDESCryptoServiceProvider_t1216_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TripleDESCryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &TripleDESCryptoServiceProvider_t1216_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1240/* custom_attributes_cache */
	, &TripleDESCryptoServiceProvider_t1216_0_0_0/* byval_arg */
	, &TripleDESCryptoServiceProvider_t1216_1_0_0/* this_arg */
	, &TripleDESCryptoServiceProvider_t1216_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TripleDESCryptoServiceProvider_t1216)/* instance_size */
	, sizeof (TripleDESCryptoServiceProvider_t1216)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.TripleDESTransform
#include "mscorlib_System_Security_Cryptography_TripleDESTransform.h"
// Metadata Definition System.Security.Cryptography.TripleDESTransform
extern TypeInfo TripleDESTransform_t1217_il2cpp_TypeInfo;
// System.Security.Cryptography.TripleDESTransform
#include "mscorlib_System_Security_Cryptography_TripleDESTransformMethodDeclarations.h"
static const EncodedMethodIndex TripleDESTransform_t1217_VTable[18] = 
{
	120,
	1534,
	122,
	123,
	1535,
	1536,
	1537,
	1538,
	1539,
	1536,
	1540,
	2809,
	1541,
	1542,
	1543,
	1544,
	1537,
	1538,
};
static Il2CppInterfaceOffsetPair TripleDESTransform_t1217_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ICryptoTransform_t616_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TripleDESTransform_t1217_0_0_0;
extern const Il2CppType TripleDESTransform_t1217_1_0_0;
struct TripleDESTransform_t1217;
const Il2CppTypeDefinitionMetadata TripleDESTransform_t1217_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TripleDESTransform_t1217_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricTransform_t839_0_0_0/* parent */
	, TripleDESTransform_t1217_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4884/* fieldStart */
	, 7533/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TripleDESTransform_t1217_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TripleDESTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &TripleDESTransform_t1217_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TripleDESTransform_t1217_0_0_0/* byval_arg */
	, &TripleDESTransform_t1217_1_0_0/* this_arg */
	, &TripleDESTransform_t1217_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TripleDESTransform_t1217)/* instance_size */
	, sizeof (TripleDESTransform_t1217)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Security.Permissions.IBuiltInPermission
extern TypeInfo IBuiltInPermission_t2117_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IBuiltInPermission_t2117_0_0_0;
extern const Il2CppType IBuiltInPermission_t2117_1_0_0;
struct IBuiltInPermission_t2117;
const Il2CppTypeDefinitionMetadata IBuiltInPermission_t2117_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IBuiltInPermission_t2117_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IBuiltInPermission"/* name */
	, "System.Security.Permissions"/* namespaze */
	, NULL/* methods */
	, &IBuiltInPermission_t2117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IBuiltInPermission_t2117_0_0_0/* byval_arg */
	, &IBuiltInPermission_t2117_1_0_0/* this_arg */
	, &IBuiltInPermission_t2117_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Security.Permissions.IUnrestrictedPermission
extern TypeInfo IUnrestrictedPermission_t2118_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IUnrestrictedPermission_t2118_0_0_0;
extern const Il2CppType IUnrestrictedPermission_t2118_1_0_0;
struct IUnrestrictedPermission_t2118;
const Il2CppTypeDefinitionMetadata IUnrestrictedPermission_t2118_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IUnrestrictedPermission_t2118_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUnrestrictedPermission"/* name */
	, "System.Security.Permissions"/* namespaze */
	, NULL/* methods */
	, &IUnrestrictedPermission_t2118_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1241/* custom_attributes_cache */
	, &IUnrestrictedPermission_t2118_0_0_0/* byval_arg */
	, &IUnrestrictedPermission_t2118_1_0_0/* this_arg */
	, &IUnrestrictedPermission_t2118_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Permissions.SecurityPermission
#include "mscorlib_System_Security_Permissions_SecurityPermission.h"
// Metadata Definition System.Security.Permissions.SecurityPermission
extern TypeInfo SecurityPermission_t1218_il2cpp_TypeInfo;
// System.Security.Permissions.SecurityPermission
#include "mscorlib_System_Security_Permissions_SecurityPermissionMethodDeclarations.h"
static const EncodedMethodIndex SecurityPermission_t1218_VTable[7] = 
{
	2810,
	125,
	2811,
	2812,
	2813,
	2814,
	2815,
};
static const Il2CppType* SecurityPermission_t1218_InterfacesTypeInfos[] = 
{
	&IBuiltInPermission_t2117_0_0_0,
	&IUnrestrictedPermission_t2118_0_0_0,
};
extern const Il2CppType IPermission_t1233_0_0_0;
extern const Il2CppType ISecurityEncodable_t2122_0_0_0;
extern const Il2CppType IStackWalk_t2123_0_0_0;
static Il2CppInterfaceOffsetPair SecurityPermission_t1218_InterfacesOffsets[] = 
{
	{ &IPermission_t1233_0_0_0, 4},
	{ &ISecurityEncodable_t2122_0_0_0, 4},
	{ &IStackWalk_t2123_0_0_0, 4},
	{ &IBuiltInPermission_t2117_0_0_0, 6},
	{ &IUnrestrictedPermission_t2118_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityPermission_t1218_0_0_0;
extern const Il2CppType SecurityPermission_t1218_1_0_0;
extern const Il2CppType CodeAccessPermission_t1219_0_0_0;
struct SecurityPermission_t1218;
const Il2CppTypeDefinitionMetadata SecurityPermission_t1218_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SecurityPermission_t1218_InterfacesTypeInfos/* implementedInterfaces */
	, SecurityPermission_t1218_InterfacesOffsets/* interfaceOffsets */
	, &CodeAccessPermission_t1219_0_0_0/* parent */
	, SecurityPermission_t1218_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4890/* fieldStart */
	, 7536/* methodStart */
	, -1/* eventStart */
	, 1522/* propertyStart */

};
TypeInfo SecurityPermission_t1218_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityPermission"/* name */
	, "System.Security.Permissions"/* namespaze */
	, NULL/* methods */
	, &SecurityPermission_t1218_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1242/* custom_attributes_cache */
	, &SecurityPermission_t1218_0_0_0/* byval_arg */
	, &SecurityPermission_t1218_1_0_0/* this_arg */
	, &SecurityPermission_t1218_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityPermission_t1218)/* instance_size */
	, sizeof (SecurityPermission_t1218)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 2/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Security.Permissions.SecurityPermissionFlag
#include "mscorlib_System_Security_Permissions_SecurityPermissionFlag.h"
// Metadata Definition System.Security.Permissions.SecurityPermissionFlag
extern TypeInfo SecurityPermissionFlag_t1220_il2cpp_TypeInfo;
// System.Security.Permissions.SecurityPermissionFlag
#include "mscorlib_System_Security_Permissions_SecurityPermissionFlagMethodDeclarations.h"
static const EncodedMethodIndex SecurityPermissionFlag_t1220_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
extern const Il2CppType IFormattable_t1406_0_0_0;
extern const Il2CppType IConvertible_t1409_0_0_0;
extern const Il2CppType IComparable_t1408_0_0_0;
static Il2CppInterfaceOffsetPair SecurityPermissionFlag_t1220_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityPermissionFlag_t1220_0_0_0;
extern const Il2CppType SecurityPermissionFlag_t1220_1_0_0;
extern const Il2CppType Enum_t305_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t320_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata SecurityPermissionFlag_t1220_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SecurityPermissionFlag_t1220_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, SecurityPermissionFlag_t1220_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4891/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SecurityPermissionFlag_t1220_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityPermissionFlag"/* name */
	, "System.Security.Permissions"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1243/* custom_attributes_cache */
	, &SecurityPermissionFlag_t1220_0_0_0/* byval_arg */
	, &SecurityPermissionFlag_t1220_1_0_0/* this_arg */
	, &SecurityPermissionFlag_t1220_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityPermissionFlag_t1220)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SecurityPermissionFlag_t1220)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Permissions.StrongNamePublicKeyBlob
#include "mscorlib_System_Security_Permissions_StrongNamePublicKeyBlob.h"
// Metadata Definition System.Security.Permissions.StrongNamePublicKeyBlob
extern TypeInfo StrongNamePublicKeyBlob_t1221_il2cpp_TypeInfo;
// System.Security.Permissions.StrongNamePublicKeyBlob
#include "mscorlib_System_Security_Permissions_StrongNamePublicKeyBlobMethodDeclarations.h"
static const EncodedMethodIndex StrongNamePublicKeyBlob_t1221_VTable[4] = 
{
	2816,
	125,
	2817,
	2818,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StrongNamePublicKeyBlob_t1221_0_0_0;
extern const Il2CppType StrongNamePublicKeyBlob_t1221_1_0_0;
struct StrongNamePublicKeyBlob_t1221;
const Il2CppTypeDefinitionMetadata StrongNamePublicKeyBlob_t1221_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StrongNamePublicKeyBlob_t1221_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4908/* fieldStart */
	, 7543/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StrongNamePublicKeyBlob_t1221_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StrongNamePublicKeyBlob"/* name */
	, "System.Security.Permissions"/* namespaze */
	, NULL/* methods */
	, &StrongNamePublicKeyBlob_t1221_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1244/* custom_attributes_cache */
	, &StrongNamePublicKeyBlob_t1221_0_0_0/* byval_arg */
	, &StrongNamePublicKeyBlob_t1221_1_0_0/* this_arg */
	, &StrongNamePublicKeyBlob_t1221_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StrongNamePublicKeyBlob_t1221)/* instance_size */
	, sizeof (StrongNamePublicKeyBlob_t1221)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Policy.ApplicationTrust
#include "mscorlib_System_Security_Policy_ApplicationTrust.h"
// Metadata Definition System.Security.Policy.ApplicationTrust
extern TypeInfo ApplicationTrust_t1223_il2cpp_TypeInfo;
// System.Security.Policy.ApplicationTrust
#include "mscorlib_System_Security_Policy_ApplicationTrustMethodDeclarations.h"
static const EncodedMethodIndex ApplicationTrust_t1223_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
static const Il2CppType* ApplicationTrust_t1223_InterfacesTypeInfos[] = 
{
	&ISecurityEncodable_t2122_0_0_0,
};
static Il2CppInterfaceOffsetPair ApplicationTrust_t1223_InterfacesOffsets[] = 
{
	{ &ISecurityEncodable_t2122_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ApplicationTrust_t1223_0_0_0;
extern const Il2CppType ApplicationTrust_t1223_1_0_0;
struct ApplicationTrust_t1223;
const Il2CppTypeDefinitionMetadata ApplicationTrust_t1223_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ApplicationTrust_t1223_InterfacesTypeInfos/* implementedInterfaces */
	, ApplicationTrust_t1223_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ApplicationTrust_t1223_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4909/* fieldStart */
	, 7546/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ApplicationTrust_t1223_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ApplicationTrust"/* name */
	, "System.Security.Policy"/* namespaze */
	, NULL/* methods */
	, &ApplicationTrust_t1223_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1245/* custom_attributes_cache */
	, &ApplicationTrust_t1223_0_0_0/* byval_arg */
	, &ApplicationTrust_t1223_1_0_0/* this_arg */
	, &ApplicationTrust_t1223_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ApplicationTrust_t1223)/* instance_size */
	, sizeof (ApplicationTrust_t1223)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Policy.Evidence
#include "mscorlib_System_Security_Policy_Evidence.h"
// Metadata Definition System.Security.Policy.Evidence
extern TypeInfo Evidence_t984_il2cpp_TypeInfo;
// System.Security.Policy.Evidence
#include "mscorlib_System_Security_Policy_EvidenceMethodDeclarations.h"
extern const Il2CppType EvidenceEnumerator_t1224_0_0_0;
static const Il2CppType* Evidence_t984_il2cpp_TypeInfo__nestedTypes[1] =
{
	&EvidenceEnumerator_t1224_0_0_0,
};
static const EncodedMethodIndex Evidence_t984_VTable[8] = 
{
	2819,
	125,
	2820,
	123,
	2821,
	2822,
	2823,
	2824,
};
extern const Il2CppType IEnumerable_t302_0_0_0;
extern const Il2CppType ICollection_t569_0_0_0;
static const Il2CppType* Evidence_t984_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_t569_0_0_0,
};
static Il2CppInterfaceOffsetPair Evidence_t984_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Evidence_t984_0_0_0;
extern const Il2CppType Evidence_t984_1_0_0;
struct Evidence_t984;
const Il2CppTypeDefinitionMetadata Evidence_t984_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Evidence_t984_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Evidence_t984_InterfacesTypeInfos/* implementedInterfaces */
	, Evidence_t984_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Evidence_t984_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4910/* fieldStart */
	, 7547/* methodStart */
	, -1/* eventStart */
	, 1523/* propertyStart */

};
TypeInfo Evidence_t984_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Evidence"/* name */
	, "System.Security.Policy"/* namespaze */
	, NULL/* methods */
	, &Evidence_t984_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1246/* custom_attributes_cache */
	, &Evidence_t984_0_0_0/* byval_arg */
	, &Evidence_t984_1_0_0/* this_arg */
	, &Evidence_t984_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Evidence_t984)/* instance_size */
	, sizeof (Evidence_t984)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Policy.Evidence/EvidenceEnumerator
#include "mscorlib_System_Security_Policy_Evidence_EvidenceEnumerator.h"
// Metadata Definition System.Security.Policy.Evidence/EvidenceEnumerator
extern TypeInfo EvidenceEnumerator_t1224_il2cpp_TypeInfo;
// System.Security.Policy.Evidence/EvidenceEnumerator
#include "mscorlib_System_Security_Policy_Evidence_EvidenceEnumeratorMethodDeclarations.h"
static const EncodedMethodIndex EvidenceEnumerator_t1224_VTable[6] = 
{
	120,
	125,
	122,
	123,
	2825,
	2826,
};
extern const Il2CppType IEnumerator_t279_0_0_0;
static const Il2CppType* EvidenceEnumerator_t1224_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair EvidenceEnumerator_t1224_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EvidenceEnumerator_t1224_1_0_0;
struct EvidenceEnumerator_t1224;
const Il2CppTypeDefinitionMetadata EvidenceEnumerator_t1224_DefinitionMetadata = 
{
	&Evidence_t984_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, EvidenceEnumerator_t1224_InterfacesTypeInfos/* implementedInterfaces */
	, EvidenceEnumerator_t1224_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EvidenceEnumerator_t1224_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4913/* fieldStart */
	, 7556/* methodStart */
	, -1/* eventStart */
	, 1527/* propertyStart */

};
TypeInfo EvidenceEnumerator_t1224_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EvidenceEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &EvidenceEnumerator_t1224_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EvidenceEnumerator_t1224_0_0_0/* byval_arg */
	, &EvidenceEnumerator_t1224_1_0_0/* this_arg */
	, &EvidenceEnumerator_t1224_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EvidenceEnumerator_t1224)/* instance_size */
	, sizeof (EvidenceEnumerator_t1224)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Policy.Hash
#include "mscorlib_System_Security_Policy_Hash.h"
// Metadata Definition System.Security.Policy.Hash
extern TypeInfo Hash_t1225_il2cpp_TypeInfo;
// System.Security.Policy.Hash
#include "mscorlib_System_Security_Policy_HashMethodDeclarations.h"
static const EncodedMethodIndex Hash_t1225_VTable[5] = 
{
	120,
	125,
	122,
	2827,
	2828,
};
extern const Il2CppType ISerializable_t1426_0_0_0;
extern const Il2CppType IBuiltInEvidence_t2119_0_0_0;
static const Il2CppType* Hash_t1225_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
	&IBuiltInEvidence_t2119_0_0_0,
};
static Il2CppInterfaceOffsetPair Hash_t1225_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &IBuiltInEvidence_t2119_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Hash_t1225_0_0_0;
extern const Il2CppType Hash_t1225_1_0_0;
struct Hash_t1225;
const Il2CppTypeDefinitionMetadata Hash_t1225_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Hash_t1225_InterfacesTypeInfos/* implementedInterfaces */
	, Hash_t1225_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Hash_t1225_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4916/* fieldStart */
	, 7559/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Hash_t1225_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Hash"/* name */
	, "System.Security.Policy"/* namespaze */
	, NULL/* methods */
	, &Hash_t1225_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1249/* custom_attributes_cache */
	, &Hash_t1225_0_0_0/* byval_arg */
	, &Hash_t1225_1_0_0/* this_arg */
	, &Hash_t1225_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Hash_t1225)/* instance_size */
	, sizeof (Hash_t1225)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Security.Policy.IBuiltInEvidence
extern TypeInfo IBuiltInEvidence_t2119_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IBuiltInEvidence_t2119_1_0_0;
struct IBuiltInEvidence_t2119;
const Il2CppTypeDefinitionMetadata IBuiltInEvidence_t2119_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IBuiltInEvidence_t2119_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IBuiltInEvidence"/* name */
	, "System.Security.Policy"/* namespaze */
	, NULL/* methods */
	, &IBuiltInEvidence_t2119_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IBuiltInEvidence_t2119_0_0_0/* byval_arg */
	, &IBuiltInEvidence_t2119_1_0_0/* this_arg */
	, &IBuiltInEvidence_t2119_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Security.Policy.IIdentityPermissionFactory
extern TypeInfo IIdentityPermissionFactory_t2120_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IIdentityPermissionFactory_t2120_0_0_0;
extern const Il2CppType IIdentityPermissionFactory_t2120_1_0_0;
struct IIdentityPermissionFactory_t2120;
const Il2CppTypeDefinitionMetadata IIdentityPermissionFactory_t2120_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IIdentityPermissionFactory_t2120_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IIdentityPermissionFactory"/* name */
	, "System.Security.Policy"/* namespaze */
	, NULL/* methods */
	, &IIdentityPermissionFactory_t2120_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1250/* custom_attributes_cache */
	, &IIdentityPermissionFactory_t2120_0_0_0/* byval_arg */
	, &IIdentityPermissionFactory_t2120_1_0_0/* this_arg */
	, &IIdentityPermissionFactory_t2120_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Policy.StrongName
#include "mscorlib_System_Security_Policy_StrongName.h"
// Metadata Definition System.Security.Policy.StrongName
extern TypeInfo StrongName_t1226_il2cpp_TypeInfo;
// System.Security.Policy.StrongName
#include "mscorlib_System_Security_Policy_StrongNameMethodDeclarations.h"
static const EncodedMethodIndex StrongName_t1226_VTable[4] = 
{
	2829,
	125,
	2830,
	2831,
};
static const Il2CppType* StrongName_t1226_InterfacesTypeInfos[] = 
{
	&IBuiltInEvidence_t2119_0_0_0,
	&IIdentityPermissionFactory_t2120_0_0_0,
};
static Il2CppInterfaceOffsetPair StrongName_t1226_InterfacesOffsets[] = 
{
	{ &IBuiltInEvidence_t2119_0_0_0, 4},
	{ &IIdentityPermissionFactory_t2120_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StrongName_t1226_0_0_0;
extern const Il2CppType StrongName_t1226_1_0_0;
struct StrongName_t1226;
const Il2CppTypeDefinitionMetadata StrongName_t1226_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StrongName_t1226_InterfacesTypeInfos/* implementedInterfaces */
	, StrongName_t1226_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StrongName_t1226_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4918/* fieldStart */
	, 7564/* methodStart */
	, -1/* eventStart */
	, 1528/* propertyStart */

};
TypeInfo StrongName_t1226_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StrongName"/* name */
	, "System.Security.Policy"/* namespaze */
	, NULL/* methods */
	, &StrongName_t1226_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1251/* custom_attributes_cache */
	, &StrongName_t1226_0_0_0/* byval_arg */
	, &StrongName_t1226_1_0_0/* this_arg */
	, &StrongName_t1226_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StrongName_t1226)/* instance_size */
	, sizeof (StrongName_t1226)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Security.Principal.IIdentity
extern TypeInfo IIdentity_t2121_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IIdentity_t2121_0_0_0;
extern const Il2CppType IIdentity_t2121_1_0_0;
struct IIdentity_t2121;
const Il2CppTypeDefinitionMetadata IIdentity_t2121_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IIdentity_t2121_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IIdentity"/* name */
	, "System.Security.Principal"/* namespaze */
	, NULL/* methods */
	, &IIdentity_t2121_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1252/* custom_attributes_cache */
	, &IIdentity_t2121_0_0_0/* byval_arg */
	, &IIdentity_t2121_1_0_0/* this_arg */
	, &IIdentity_t2121_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Security.Principal.IPrincipal
extern TypeInfo IPrincipal_t1275_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IPrincipal_t1275_0_0_0;
extern const Il2CppType IPrincipal_t1275_1_0_0;
struct IPrincipal_t1275;
const Il2CppTypeDefinitionMetadata IPrincipal_t1275_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IPrincipal_t1275_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPrincipal"/* name */
	, "System.Security.Principal"/* namespaze */
	, NULL/* methods */
	, &IPrincipal_t1275_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1253/* custom_attributes_cache */
	, &IPrincipal_t1275_0_0_0/* byval_arg */
	, &IPrincipal_t1275_1_0_0/* this_arg */
	, &IPrincipal_t1275_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Principal.PrincipalPolicy
#include "mscorlib_System_Security_Principal_PrincipalPolicy.h"
// Metadata Definition System.Security.Principal.PrincipalPolicy
extern TypeInfo PrincipalPolicy_t1227_il2cpp_TypeInfo;
// System.Security.Principal.PrincipalPolicy
#include "mscorlib_System_Security_Principal_PrincipalPolicyMethodDeclarations.h"
static const EncodedMethodIndex PrincipalPolicy_t1227_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair PrincipalPolicy_t1227_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrincipalPolicy_t1227_0_0_0;
extern const Il2CppType PrincipalPolicy_t1227_1_0_0;
const Il2CppTypeDefinitionMetadata PrincipalPolicy_t1227_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrincipalPolicy_t1227_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, PrincipalPolicy_t1227_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4921/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PrincipalPolicy_t1227_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrincipalPolicy"/* name */
	, "System.Security.Principal"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1254/* custom_attributes_cache */
	, &PrincipalPolicy_t1227_0_0_0/* byval_arg */
	, &PrincipalPolicy_t1227_1_0_0/* this_arg */
	, &PrincipalPolicy_t1227_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrincipalPolicy_t1227)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PrincipalPolicy_t1227)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Principal.WindowsAccountType
#include "mscorlib_System_Security_Principal_WindowsAccountType.h"
// Metadata Definition System.Security.Principal.WindowsAccountType
extern TypeInfo WindowsAccountType_t1228_il2cpp_TypeInfo;
// System.Security.Principal.WindowsAccountType
#include "mscorlib_System_Security_Principal_WindowsAccountTypeMethodDeclarations.h"
static const EncodedMethodIndex WindowsAccountType_t1228_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair WindowsAccountType_t1228_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WindowsAccountType_t1228_0_0_0;
extern const Il2CppType WindowsAccountType_t1228_1_0_0;
const Il2CppTypeDefinitionMetadata WindowsAccountType_t1228_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WindowsAccountType_t1228_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, WindowsAccountType_t1228_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4925/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WindowsAccountType_t1228_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WindowsAccountType"/* name */
	, "System.Security.Principal"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1255/* custom_attributes_cache */
	, &WindowsAccountType_t1228_0_0_0/* byval_arg */
	, &WindowsAccountType_t1228_1_0_0/* this_arg */
	, &WindowsAccountType_t1228_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WindowsAccountType_t1228)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (WindowsAccountType_t1228)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Principal.WindowsIdentity
#include "mscorlib_System_Security_Principal_WindowsIdentity.h"
// Metadata Definition System.Security.Principal.WindowsIdentity
extern TypeInfo WindowsIdentity_t1229_il2cpp_TypeInfo;
// System.Security.Principal.WindowsIdentity
#include "mscorlib_System_Security_Principal_WindowsIdentityMethodDeclarations.h"
static const EncodedMethodIndex WindowsIdentity_t1229_VTable[7] = 
{
	120,
	125,
	122,
	123,
	2832,
	2833,
	2834,
};
extern const Il2CppType IDeserializationCallback_t1429_0_0_0;
static const Il2CppType* WindowsIdentity_t1229_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
	&ISerializable_t1426_0_0_0,
	&IDeserializationCallback_t1429_0_0_0,
	&IIdentity_t2121_0_0_0,
};
static Il2CppInterfaceOffsetPair WindowsIdentity_t1229_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 5},
	{ &IDeserializationCallback_t1429_0_0_0, 6},
	{ &IIdentity_t2121_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WindowsIdentity_t1229_0_0_0;
extern const Il2CppType WindowsIdentity_t1229_1_0_0;
struct WindowsIdentity_t1229;
const Il2CppTypeDefinitionMetadata WindowsIdentity_t1229_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WindowsIdentity_t1229_InterfacesTypeInfos/* implementedInterfaces */
	, WindowsIdentity_t1229_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WindowsIdentity_t1229_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4930/* fieldStart */
	, 7570/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WindowsIdentity_t1229_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WindowsIdentity"/* name */
	, "System.Security.Principal"/* namespaze */
	, NULL/* methods */
	, &WindowsIdentity_t1229_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1256/* custom_attributes_cache */
	, &WindowsIdentity_t1229_0_0_0/* byval_arg */
	, &WindowsIdentity_t1229_1_0_0/* this_arg */
	, &WindowsIdentity_t1229_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WindowsIdentity_t1229)/* instance_size */
	, sizeof (WindowsIdentity_t1229)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WindowsIdentity_t1229_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Security.CodeAccessPermission
#include "mscorlib_System_Security_CodeAccessPermission.h"
// Metadata Definition System.Security.CodeAccessPermission
extern TypeInfo CodeAccessPermission_t1219_il2cpp_TypeInfo;
// System.Security.CodeAccessPermission
#include "mscorlib_System_Security_CodeAccessPermissionMethodDeclarations.h"
static const EncodedMethodIndex CodeAccessPermission_t1219_VTable[6] = 
{
	2810,
	125,
	2811,
	2812,
	0,
	0,
};
static const Il2CppType* CodeAccessPermission_t1219_InterfacesTypeInfos[] = 
{
	&IPermission_t1233_0_0_0,
	&ISecurityEncodable_t2122_0_0_0,
	&IStackWalk_t2123_0_0_0,
};
static Il2CppInterfaceOffsetPair CodeAccessPermission_t1219_InterfacesOffsets[] = 
{
	{ &IPermission_t1233_0_0_0, 4},
	{ &ISecurityEncodable_t2122_0_0_0, 4},
	{ &IStackWalk_t2123_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CodeAccessPermission_t1219_1_0_0;
struct CodeAccessPermission_t1219;
const Il2CppTypeDefinitionMetadata CodeAccessPermission_t1219_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CodeAccessPermission_t1219_InterfacesTypeInfos/* implementedInterfaces */
	, CodeAccessPermission_t1219_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CodeAccessPermission_t1219_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7577/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CodeAccessPermission_t1219_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CodeAccessPermission"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &CodeAccessPermission_t1219_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1258/* custom_attributes_cache */
	, &CodeAccessPermission_t1219_0_0_0/* byval_arg */
	, &CodeAccessPermission_t1219_1_0_0/* this_arg */
	, &CodeAccessPermission_t1219_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CodeAccessPermission_t1219)/* instance_size */
	, sizeof (CodeAccessPermission_t1219)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Security.IPermission
extern TypeInfo IPermission_t1233_il2cpp_TypeInfo;
static const Il2CppType* IPermission_t1233_InterfacesTypeInfos[] = 
{
	&ISecurityEncodable_t2122_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IPermission_t1233_1_0_0;
struct IPermission_t1233;
const Il2CppTypeDefinitionMetadata IPermission_t1233_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IPermission_t1233_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IPermission_t1233_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPermission"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &IPermission_t1233_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1261/* custom_attributes_cache */
	, &IPermission_t1233_0_0_0/* byval_arg */
	, &IPermission_t1233_1_0_0/* this_arg */
	, &IPermission_t1233_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Security.ISecurityEncodable
extern TypeInfo ISecurityEncodable_t2122_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISecurityEncodable_t2122_1_0_0;
struct ISecurityEncodable_t2122;
const Il2CppTypeDefinitionMetadata ISecurityEncodable_t2122_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISecurityEncodable_t2122_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISecurityEncodable"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &ISecurityEncodable_t2122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1262/* custom_attributes_cache */
	, &ISecurityEncodable_t2122_0_0_0/* byval_arg */
	, &ISecurityEncodable_t2122_1_0_0/* this_arg */
	, &ISecurityEncodable_t2122_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Security.IStackWalk
extern TypeInfo IStackWalk_t2123_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IStackWalk_t2123_1_0_0;
struct IStackWalk_t2123;
const Il2CppTypeDefinitionMetadata IStackWalk_t2123_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IStackWalk_t2123_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IStackWalk"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &IStackWalk_t2123_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1263/* custom_attributes_cache */
	, &IStackWalk_t2123_0_0_0/* byval_arg */
	, &IStackWalk_t2123_1_0_0/* this_arg */
	, &IStackWalk_t2123_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.PermissionSet
#include "mscorlib_System_Security_PermissionSet.h"
// Metadata Definition System.Security.PermissionSet
extern TypeInfo PermissionSet_t985_il2cpp_TypeInfo;
// System.Security.PermissionSet
#include "mscorlib_System_Security_PermissionSetMethodDeclarations.h"
static const EncodedMethodIndex PermissionSet_t985_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PermissionSet_t985_0_0_0;
extern const Il2CppType PermissionSet_t985_1_0_0;
struct PermissionSet_t985;
const Il2CppTypeDefinitionMetadata PermissionSet_t985_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PermissionSet_t985_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4937/* fieldStart */
	, 7585/* methodStart */
	, -1/* eventStart */
	, 1531/* propertyStart */

};
TypeInfo PermissionSet_t985_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PermissionSet"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &PermissionSet_t985_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PermissionSet_t985_0_0_0/* byval_arg */
	, &PermissionSet_t985_1_0_0/* this_arg */
	, &PermissionSet_t985_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PermissionSet_t985)/* instance_size */
	, sizeof (PermissionSet_t985)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecurityContext
#include "mscorlib_System_Security_SecurityContext.h"
// Metadata Definition System.Security.SecurityContext
extern TypeInfo SecurityContext_t1231_il2cpp_TypeInfo;
// System.Security.SecurityContext
#include "mscorlib_System_Security_SecurityContextMethodDeclarations.h"
static const EncodedMethodIndex SecurityContext_t1231_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityContext_t1231_0_0_0;
extern const Il2CppType SecurityContext_t1231_1_0_0;
struct SecurityContext_t1231;
const Il2CppTypeDefinitionMetadata SecurityContext_t1231_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SecurityContext_t1231_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4938/* fieldStart */
	, 7589/* methodStart */
	, -1/* eventStart */
	, 1532/* propertyStart */

};
TypeInfo SecurityContext_t1231_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityContext"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SecurityContext_t1231_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SecurityContext_t1231_0_0_0/* byval_arg */
	, &SecurityContext_t1231_1_0_0/* this_arg */
	, &SecurityContext_t1231_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityContext_t1231)/* instance_size */
	, sizeof (SecurityContext_t1231)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecurityElement
#include "mscorlib_System_Security_SecurityElement.h"
// Metadata Definition System.Security.SecurityElement
extern TypeInfo SecurityElement_t856_il2cpp_TypeInfo;
// System.Security.SecurityElement
#include "mscorlib_System_Security_SecurityElementMethodDeclarations.h"
extern const Il2CppType SecurityAttribute_t1232_0_0_0;
static const Il2CppType* SecurityElement_t856_il2cpp_TypeInfo__nestedTypes[1] =
{
	&SecurityAttribute_t1232_0_0_0,
};
static const EncodedMethodIndex SecurityElement_t856_VTable[4] = 
{
	120,
	125,
	122,
	2835,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityElement_t856_0_0_0;
extern const Il2CppType SecurityElement_t856_1_0_0;
struct SecurityElement_t856;
const Il2CppTypeDefinitionMetadata SecurityElement_t856_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SecurityElement_t856_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SecurityElement_t856_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4942/* fieldStart */
	, 7594/* methodStart */
	, -1/* eventStart */
	, 1534/* propertyStart */

};
TypeInfo SecurityElement_t856_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityElement"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SecurityElement_t856_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1266/* custom_attributes_cache */
	, &SecurityElement_t856_0_0_0/* byval_arg */
	, &SecurityElement_t856_1_0_0/* this_arg */
	, &SecurityElement_t856_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityElement_t856)/* instance_size */
	, sizeof (SecurityElement_t856)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SecurityElement_t856_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 3/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecurityElement/SecurityAttribute
#include "mscorlib_System_Security_SecurityElement_SecurityAttribute.h"
// Metadata Definition System.Security.SecurityElement/SecurityAttribute
extern TypeInfo SecurityAttribute_t1232_il2cpp_TypeInfo;
// System.Security.SecurityElement/SecurityAttribute
#include "mscorlib_System_Security_SecurityElement_SecurityAttributeMethodDeclarations.h"
static const EncodedMethodIndex SecurityAttribute_t1232_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityAttribute_t1232_1_0_0;
struct SecurityAttribute_t1232;
const Il2CppTypeDefinitionMetadata SecurityAttribute_t1232_DefinitionMetadata = 
{
	&SecurityElement_t856_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SecurityAttribute_t1232_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4951/* fieldStart */
	, 7612/* methodStart */
	, -1/* eventStart */
	, 1537/* propertyStart */

};
TypeInfo SecurityAttribute_t1232_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityAttribute"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SecurityAttribute_t1232_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SecurityAttribute_t1232_0_0_0/* byval_arg */
	, &SecurityAttribute_t1232_1_0_0/* this_arg */
	, &SecurityAttribute_t1232_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityAttribute_t1232)/* instance_size */
	, sizeof (SecurityAttribute_t1232)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecurityException
#include "mscorlib_System_Security_SecurityException.h"
// Metadata Definition System.Security.SecurityException
extern TypeInfo SecurityException_t1234_il2cpp_TypeInfo;
// System.Security.SecurityException
#include "mscorlib_System_Security_SecurityExceptionMethodDeclarations.h"
static const EncodedMethodIndex SecurityException_t1234_VTable[11] = 
{
	120,
	125,
	122,
	2836,
	2837,
	205,
	206,
	207,
	208,
	2837,
	209,
};
extern const Il2CppType _Exception_t2066_0_0_0;
static Il2CppInterfaceOffsetPair SecurityException_t1234_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityException_t1234_0_0_0;
extern const Il2CppType SecurityException_t1234_1_0_0;
extern const Il2CppType SystemException_t597_0_0_0;
struct SecurityException_t1234;
const Il2CppTypeDefinitionMetadata SecurityException_t1234_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SecurityException_t1234_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, SecurityException_t1234_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4953/* fieldStart */
	, 7615/* methodStart */
	, -1/* eventStart */
	, 1539/* propertyStart */

};
TypeInfo SecurityException_t1234_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityException"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SecurityException_t1234_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1267/* custom_attributes_cache */
	, &SecurityException_t1234_0_0_0/* byval_arg */
	, &SecurityException_t1234_1_0_0/* this_arg */
	, &SecurityException_t1234_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityException_t1234)/* instance_size */
	, sizeof (SecurityException_t1234)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 6/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.RuntimeDeclSecurityEntry
#include "mscorlib_System_Security_RuntimeDeclSecurityEntry.h"
// Metadata Definition System.Security.RuntimeDeclSecurityEntry
extern TypeInfo RuntimeDeclSecurityEntry_t1235_il2cpp_TypeInfo;
// System.Security.RuntimeDeclSecurityEntry
#include "mscorlib_System_Security_RuntimeDeclSecurityEntryMethodDeclarations.h"
static const EncodedMethodIndex RuntimeDeclSecurityEntry_t1235_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeDeclSecurityEntry_t1235_0_0_0;
extern const Il2CppType RuntimeDeclSecurityEntry_t1235_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeDeclSecurityEntry_t1235_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, RuntimeDeclSecurityEntry_t1235_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4961/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RuntimeDeclSecurityEntry_t1235_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeDeclSecurityEntry"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &RuntimeDeclSecurityEntry_t1235_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RuntimeDeclSecurityEntry_t1235_0_0_0/* byval_arg */
	, &RuntimeDeclSecurityEntry_t1235_1_0_0/* this_arg */
	, &RuntimeDeclSecurityEntry_t1235_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeDeclSecurityEntry_t1235)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeDeclSecurityEntry_t1235)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeDeclSecurityEntry_t1235 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.RuntimeSecurityFrame
#include "mscorlib_System_Security_RuntimeSecurityFrame.h"
// Metadata Definition System.Security.RuntimeSecurityFrame
extern TypeInfo RuntimeSecurityFrame_t1237_il2cpp_TypeInfo;
// System.Security.RuntimeSecurityFrame
#include "mscorlib_System_Security_RuntimeSecurityFrameMethodDeclarations.h"
static const EncodedMethodIndex RuntimeSecurityFrame_t1237_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeSecurityFrame_t1237_0_0_0;
extern const Il2CppType RuntimeSecurityFrame_t1237_1_0_0;
struct RuntimeSecurityFrame_t1237;
const Il2CppTypeDefinitionMetadata RuntimeSecurityFrame_t1237_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RuntimeSecurityFrame_t1237_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4964/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RuntimeSecurityFrame_t1237_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeSecurityFrame"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &RuntimeSecurityFrame_t1237_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RuntimeSecurityFrame_t1237_0_0_0/* byval_arg */
	, &RuntimeSecurityFrame_t1237_1_0_0/* this_arg */
	, &RuntimeSecurityFrame_t1237_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeSecurityFrame_t1237)/* instance_size */
	, sizeof (RuntimeSecurityFrame_t1237)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecurityFrame
#include "mscorlib_System_Security_SecurityFrame.h"
// Metadata Definition System.Security.SecurityFrame
extern TypeInfo SecurityFrame_t1238_il2cpp_TypeInfo;
// System.Security.SecurityFrame
#include "mscorlib_System_Security_SecurityFrameMethodDeclarations.h"
static const EncodedMethodIndex SecurityFrame_t1238_VTable[4] = 
{
	150,
	125,
	151,
	2838,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityFrame_t1238_0_0_0;
extern const Il2CppType SecurityFrame_t1238_1_0_0;
const Il2CppTypeDefinitionMetadata SecurityFrame_t1238_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, SecurityFrame_t1238_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4969/* fieldStart */
	, 7626/* methodStart */
	, -1/* eventStart */
	, 1545/* propertyStart */

};
TypeInfo SecurityFrame_t1238_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityFrame"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SecurityFrame_t1238_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SecurityFrame_t1238_0_0_0/* byval_arg */
	, &SecurityFrame_t1238_1_0_0/* this_arg */
	, &SecurityFrame_t1238_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityFrame_t1238)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SecurityFrame_t1238)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecurityManager
#include "mscorlib_System_Security_SecurityManager.h"
// Metadata Definition System.Security.SecurityManager
extern TypeInfo SecurityManager_t1239_il2cpp_TypeInfo;
// System.Security.SecurityManager
#include "mscorlib_System_Security_SecurityManagerMethodDeclarations.h"
static const EncodedMethodIndex SecurityManager_t1239_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityManager_t1239_0_0_0;
extern const Il2CppType SecurityManager_t1239_1_0_0;
struct SecurityManager_t1239;
const Il2CppTypeDefinitionMetadata SecurityManager_t1239_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SecurityManager_t1239_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4974/* fieldStart */
	, 7633/* methodStart */
	, -1/* eventStart */
	, 1547/* propertyStart */

};
TypeInfo SecurityManager_t1239_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityManager"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SecurityManager_t1239_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1269/* custom_attributes_cache */
	, &SecurityManager_t1239_0_0_0/* byval_arg */
	, &SecurityManager_t1239_1_0_0/* this_arg */
	, &SecurityManager_t1239_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityManager_t1239)/* instance_size */
	, sizeof (SecurityManager_t1239)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SecurityManager_t1239_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 385/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttribute.h"
// Metadata Definition System.Security.SecuritySafeCriticalAttribute
extern TypeInfo SecuritySafeCriticalAttribute_t1240_il2cpp_TypeInfo;
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttributeMethodDeclarations.h"
static const EncodedMethodIndex SecuritySafeCriticalAttribute_t1240_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
extern const Il2CppType _Attribute_t2052_0_0_0;
static Il2CppInterfaceOffsetPair SecuritySafeCriticalAttribute_t1240_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecuritySafeCriticalAttribute_t1240_0_0_0;
extern const Il2CppType SecuritySafeCriticalAttribute_t1240_1_0_0;
extern const Il2CppType Attribute_t96_0_0_0;
struct SecuritySafeCriticalAttribute_t1240;
const Il2CppTypeDefinitionMetadata SecuritySafeCriticalAttribute_t1240_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SecuritySafeCriticalAttribute_t1240_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, SecuritySafeCriticalAttribute_t1240_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7637/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SecuritySafeCriticalAttribute_t1240_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecuritySafeCriticalAttribute"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SecuritySafeCriticalAttribute_t1240_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1271/* custom_attributes_cache */
	, &SecuritySafeCriticalAttribute_t1240_0_0_0/* byval_arg */
	, &SecuritySafeCriticalAttribute_t1240_1_0_0/* this_arg */
	, &SecuritySafeCriticalAttribute_t1240_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecuritySafeCriticalAttribute_t1240)/* instance_size */
	, sizeof (SecuritySafeCriticalAttribute_t1240)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttrib.h"
// Metadata Definition System.Security.SuppressUnmanagedCodeSecurityAttribute
extern TypeInfo SuppressUnmanagedCodeSecurityAttribute_t1241_il2cpp_TypeInfo;
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttribMethodDeclarations.h"
static const EncodedMethodIndex SuppressUnmanagedCodeSecurityAttribute_t1241_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair SuppressUnmanagedCodeSecurityAttribute_t1241_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SuppressUnmanagedCodeSecurityAttribute_t1241_0_0_0;
extern const Il2CppType SuppressUnmanagedCodeSecurityAttribute_t1241_1_0_0;
struct SuppressUnmanagedCodeSecurityAttribute_t1241;
const Il2CppTypeDefinitionMetadata SuppressUnmanagedCodeSecurityAttribute_t1241_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SuppressUnmanagedCodeSecurityAttribute_t1241_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, SuppressUnmanagedCodeSecurityAttribute_t1241_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7638/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SuppressUnmanagedCodeSecurityAttribute_t1241_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SuppressUnmanagedCodeSecurityAttribute"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SuppressUnmanagedCodeSecurityAttribute_t1241_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1272/* custom_attributes_cache */
	, &SuppressUnmanagedCodeSecurityAttribute_t1241_0_0_0/* byval_arg */
	, &SuppressUnmanagedCodeSecurityAttribute_t1241_1_0_0/* this_arg */
	, &SuppressUnmanagedCodeSecurityAttribute_t1241_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SuppressUnmanagedCodeSecurityAttribute_t1241)/* instance_size */
	, sizeof (SuppressUnmanagedCodeSecurityAttribute_t1241)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.UnverifiableCodeAttribute
#include "mscorlib_System_Security_UnverifiableCodeAttribute.h"
// Metadata Definition System.Security.UnverifiableCodeAttribute
extern TypeInfo UnverifiableCodeAttribute_t1242_il2cpp_TypeInfo;
// System.Security.UnverifiableCodeAttribute
#include "mscorlib_System_Security_UnverifiableCodeAttributeMethodDeclarations.h"
static const EncodedMethodIndex UnverifiableCodeAttribute_t1242_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair UnverifiableCodeAttribute_t1242_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnverifiableCodeAttribute_t1242_0_0_0;
extern const Il2CppType UnverifiableCodeAttribute_t1242_1_0_0;
struct UnverifiableCodeAttribute_t1242;
const Il2CppTypeDefinitionMetadata UnverifiableCodeAttribute_t1242_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnverifiableCodeAttribute_t1242_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, UnverifiableCodeAttribute_t1242_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7639/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnverifiableCodeAttribute_t1242_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnverifiableCodeAttribute"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &UnverifiableCodeAttribute_t1242_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1273/* custom_attributes_cache */
	, &UnverifiableCodeAttribute_t1242_0_0_0/* byval_arg */
	, &UnverifiableCodeAttribute_t1242_1_0_0/* this_arg */
	, &UnverifiableCodeAttribute_t1242_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnverifiableCodeAttribute_t1242)/* instance_size */
	, sizeof (UnverifiableCodeAttribute_t1242)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.ASCIIEncoding
#include "mscorlib_System_Text_ASCIIEncoding.h"
// Metadata Definition System.Text.ASCIIEncoding
extern TypeInfo ASCIIEncoding_t1243_il2cpp_TypeInfo;
// System.Text.ASCIIEncoding
#include "mscorlib_System_Text_ASCIIEncodingMethodDeclarations.h"
static const EncodedMethodIndex ASCIIEncoding_t1243_VTable[25] = 
{
	2839,
	125,
	2840,
	123,
	2841,
	2842,
	2843,
	2844,
	2845,
	2846,
	2847,
	2848,
	2849,
	2850,
	2851,
	2852,
	2853,
	2854,
	2855,
	2856,
	2857,
	2858,
	2859,
	2860,
	2861,
};
extern const Il2CppType ICloneable_t2056_0_0_0;
static Il2CppInterfaceOffsetPair ASCIIEncoding_t1243_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ASCIIEncoding_t1243_0_0_0;
extern const Il2CppType ASCIIEncoding_t1243_1_0_0;
extern const Il2CppType Encoding_t281_0_0_0;
struct ASCIIEncoding_t1243;
const Il2CppTypeDefinitionMetadata ASCIIEncoding_t1243_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ASCIIEncoding_t1243_InterfacesOffsets/* interfaceOffsets */
	, &Encoding_t281_0_0_0/* parent */
	, ASCIIEncoding_t1243_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7640/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ASCIIEncoding_t1243_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ASCIIEncoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &ASCIIEncoding_t1243_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1274/* custom_attributes_cache */
	, &ASCIIEncoding_t1243_0_0_0/* byval_arg */
	, &ASCIIEncoding_t1243_1_0_0/* this_arg */
	, &ASCIIEncoding_t1243_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ASCIIEncoding_t1243)/* instance_size */
	, sizeof (ASCIIEncoding_t1243)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.Decoder
#include "mscorlib_System_Text_Decoder.h"
// Metadata Definition System.Text.Decoder
extern TypeInfo Decoder_t912_il2cpp_TypeInfo;
// System.Text.Decoder
#include "mscorlib_System_Text_DecoderMethodDeclarations.h"
static const EncodedMethodIndex Decoder_t912_VTable[5] = 
{
	120,
	125,
	122,
	123,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Decoder_t912_0_0_0;
extern const Il2CppType Decoder_t912_1_0_0;
struct Decoder_t912;
const Il2CppTypeDefinitionMetadata Decoder_t912_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Decoder_t912_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4977/* fieldStart */
	, 7656/* methodStart */
	, -1/* eventStart */
	, 1548/* propertyStart */

};
TypeInfo Decoder_t912_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Decoder"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &Decoder_t912_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1278/* custom_attributes_cache */
	, &Decoder_t912_0_0_0/* byval_arg */
	, &Decoder_t912_1_0_0/* this_arg */
	, &Decoder_t912_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Decoder_t912)/* instance_size */
	, sizeof (Decoder_t912)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.DecoderExceptionFallback
#include "mscorlib_System_Text_DecoderExceptionFallback.h"
// Metadata Definition System.Text.DecoderExceptionFallback
extern TypeInfo DecoderExceptionFallback_t1246_il2cpp_TypeInfo;
// System.Text.DecoderExceptionFallback
#include "mscorlib_System_Text_DecoderExceptionFallbackMethodDeclarations.h"
static const EncodedMethodIndex DecoderExceptionFallback_t1246_VTable[5] = 
{
	2862,
	125,
	2863,
	123,
	2864,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderExceptionFallback_t1246_0_0_0;
extern const Il2CppType DecoderExceptionFallback_t1246_1_0_0;
extern const Il2CppType DecoderFallback_t1244_0_0_0;
struct DecoderExceptionFallback_t1246;
const Il2CppTypeDefinitionMetadata DecoderExceptionFallback_t1246_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &DecoderFallback_t1244_0_0_0/* parent */
	, DecoderExceptionFallback_t1246_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7660/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DecoderExceptionFallback_t1246_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderExceptionFallback"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderExceptionFallback_t1246_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderExceptionFallback_t1246_0_0_0/* byval_arg */
	, &DecoderExceptionFallback_t1246_1_0_0/* this_arg */
	, &DecoderExceptionFallback_t1246_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderExceptionFallback_t1246)/* instance_size */
	, sizeof (DecoderExceptionFallback_t1246)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.DecoderExceptionFallbackBuffer
#include "mscorlib_System_Text_DecoderExceptionFallbackBuffer.h"
// Metadata Definition System.Text.DecoderExceptionFallbackBuffer
extern TypeInfo DecoderExceptionFallbackBuffer_t1247_il2cpp_TypeInfo;
// System.Text.DecoderExceptionFallbackBuffer
#include "mscorlib_System_Text_DecoderExceptionFallbackBufferMethodDeclarations.h"
static const EncodedMethodIndex DecoderExceptionFallbackBuffer_t1247_VTable[8] = 
{
	120,
	125,
	122,
	123,
	2865,
	2866,
	2867,
	2868,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderExceptionFallbackBuffer_t1247_0_0_0;
extern const Il2CppType DecoderExceptionFallbackBuffer_t1247_1_0_0;
extern const Il2CppType DecoderFallbackBuffer_t1245_0_0_0;
struct DecoderExceptionFallbackBuffer_t1247;
const Il2CppTypeDefinitionMetadata DecoderExceptionFallbackBuffer_t1247_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &DecoderFallbackBuffer_t1245_0_0_0/* parent */
	, DecoderExceptionFallbackBuffer_t1247_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7664/* methodStart */
	, -1/* eventStart */
	, 1550/* propertyStart */

};
TypeInfo DecoderExceptionFallbackBuffer_t1247_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderExceptionFallbackBuffer"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderExceptionFallbackBuffer_t1247_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderExceptionFallbackBuffer_t1247_0_0_0/* byval_arg */
	, &DecoderExceptionFallbackBuffer_t1247_1_0_0/* this_arg */
	, &DecoderExceptionFallbackBuffer_t1247_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderExceptionFallbackBuffer_t1247)/* instance_size */
	, sizeof (DecoderExceptionFallbackBuffer_t1247)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.DecoderFallback
#include "mscorlib_System_Text_DecoderFallback.h"
// Metadata Definition System.Text.DecoderFallback
extern TypeInfo DecoderFallback_t1244_il2cpp_TypeInfo;
// System.Text.DecoderFallback
#include "mscorlib_System_Text_DecoderFallbackMethodDeclarations.h"
static const EncodedMethodIndex DecoderFallback_t1244_VTable[5] = 
{
	120,
	125,
	122,
	123,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderFallback_t1244_1_0_0;
struct DecoderFallback_t1244;
const Il2CppTypeDefinitionMetadata DecoderFallback_t1244_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DecoderFallback_t1244_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4979/* fieldStart */
	, 7668/* methodStart */
	, -1/* eventStart */
	, 1551/* propertyStart */

};
TypeInfo DecoderFallback_t1244_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderFallback"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderFallback_t1244_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderFallback_t1244_0_0_0/* byval_arg */
	, &DecoderFallback_t1244_1_0_0/* this_arg */
	, &DecoderFallback_t1244_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderFallback_t1244)/* instance_size */
	, sizeof (DecoderFallback_t1244)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DecoderFallback_t1244_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.DecoderFallbackBuffer
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
// Metadata Definition System.Text.DecoderFallbackBuffer
extern TypeInfo DecoderFallbackBuffer_t1245_il2cpp_TypeInfo;
// System.Text.DecoderFallbackBuffer
#include "mscorlib_System_Text_DecoderFallbackBufferMethodDeclarations.h"
static const EncodedMethodIndex DecoderFallbackBuffer_t1245_VTable[8] = 
{
	120,
	125,
	122,
	123,
	0,
	0,
	0,
	2868,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderFallbackBuffer_t1245_1_0_0;
struct DecoderFallbackBuffer_t1245;
const Il2CppTypeDefinitionMetadata DecoderFallbackBuffer_t1245_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DecoderFallbackBuffer_t1245_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7674/* methodStart */
	, -1/* eventStart */
	, 1554/* propertyStart */

};
TypeInfo DecoderFallbackBuffer_t1245_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderFallbackBuffer"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderFallbackBuffer_t1245_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderFallbackBuffer_t1245_0_0_0/* byval_arg */
	, &DecoderFallbackBuffer_t1245_1_0_0/* this_arg */
	, &DecoderFallbackBuffer_t1245_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderFallbackBuffer_t1245)/* instance_size */
	, sizeof (DecoderFallbackBuffer_t1245)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.DecoderFallbackException
#include "mscorlib_System_Text_DecoderFallbackException.h"
// Metadata Definition System.Text.DecoderFallbackException
extern TypeInfo DecoderFallbackException_t1248_il2cpp_TypeInfo;
// System.Text.DecoderFallbackException
#include "mscorlib_System_Text_DecoderFallbackExceptionMethodDeclarations.h"
static const EncodedMethodIndex DecoderFallbackException_t1248_VTable[12] = 
{
	120,
	125,
	122,
	203,
	2869,
	205,
	2870,
	207,
	208,
	2869,
	209,
	2871,
};
static Il2CppInterfaceOffsetPair DecoderFallbackException_t1248_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderFallbackException_t1248_0_0_0;
extern const Il2CppType DecoderFallbackException_t1248_1_0_0;
extern const Il2CppType ArgumentException_t313_0_0_0;
struct DecoderFallbackException_t1248;
const Il2CppTypeDefinitionMetadata DecoderFallbackException_t1248_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DecoderFallbackException_t1248_InterfacesOffsets/* interfaceOffsets */
	, &ArgumentException_t313_0_0_0/* parent */
	, DecoderFallbackException_t1248_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4982/* fieldStart */
	, 7679/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DecoderFallbackException_t1248_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderFallbackException"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderFallbackException_t1248_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderFallbackException_t1248_0_0_0/* byval_arg */
	, &DecoderFallbackException_t1248_1_0_0/* this_arg */
	, &DecoderFallbackException_t1248_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderFallbackException_t1248)/* instance_size */
	, sizeof (DecoderFallbackException_t1248)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.DecoderReplacementFallback
#include "mscorlib_System_Text_DecoderReplacementFallback.h"
// Metadata Definition System.Text.DecoderReplacementFallback
extern TypeInfo DecoderReplacementFallback_t1249_il2cpp_TypeInfo;
// System.Text.DecoderReplacementFallback
#include "mscorlib_System_Text_DecoderReplacementFallbackMethodDeclarations.h"
static const EncodedMethodIndex DecoderReplacementFallback_t1249_VTable[5] = 
{
	2872,
	125,
	2873,
	123,
	2874,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderReplacementFallback_t1249_0_0_0;
extern const Il2CppType DecoderReplacementFallback_t1249_1_0_0;
struct DecoderReplacementFallback_t1249;
const Il2CppTypeDefinitionMetadata DecoderReplacementFallback_t1249_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &DecoderFallback_t1244_0_0_0/* parent */
	, DecoderReplacementFallback_t1249_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4984/* fieldStart */
	, 7682/* methodStart */
	, -1/* eventStart */
	, 1555/* propertyStart */

};
TypeInfo DecoderReplacementFallback_t1249_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderReplacementFallback"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderReplacementFallback_t1249_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderReplacementFallback_t1249_0_0_0/* byval_arg */
	, &DecoderReplacementFallback_t1249_1_0_0/* this_arg */
	, &DecoderReplacementFallback_t1249_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderReplacementFallback_t1249)/* instance_size */
	, sizeof (DecoderReplacementFallback_t1249)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.DecoderReplacementFallbackBuffer
#include "mscorlib_System_Text_DecoderReplacementFallbackBuffer.h"
// Metadata Definition System.Text.DecoderReplacementFallbackBuffer
extern TypeInfo DecoderReplacementFallbackBuffer_t1250_il2cpp_TypeInfo;
// System.Text.DecoderReplacementFallbackBuffer
#include "mscorlib_System_Text_DecoderReplacementFallbackBufferMethodDeclarations.h"
static const EncodedMethodIndex DecoderReplacementFallbackBuffer_t1250_VTable[8] = 
{
	120,
	125,
	122,
	123,
	2875,
	2876,
	2877,
	2878,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderReplacementFallbackBuffer_t1250_0_0_0;
extern const Il2CppType DecoderReplacementFallbackBuffer_t1250_1_0_0;
struct DecoderReplacementFallbackBuffer_t1250;
const Il2CppTypeDefinitionMetadata DecoderReplacementFallbackBuffer_t1250_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &DecoderFallbackBuffer_t1245_0_0_0/* parent */
	, DecoderReplacementFallbackBuffer_t1250_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4985/* fieldStart */
	, 7688/* methodStart */
	, -1/* eventStart */
	, 1556/* propertyStart */

};
TypeInfo DecoderReplacementFallbackBuffer_t1250_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderReplacementFallbackBuffer"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderReplacementFallbackBuffer_t1250_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderReplacementFallbackBuffer_t1250_0_0_0/* byval_arg */
	, &DecoderReplacementFallbackBuffer_t1250_1_0_0/* this_arg */
	, &DecoderReplacementFallbackBuffer_t1250_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderReplacementFallbackBuffer_t1250)/* instance_size */
	, sizeof (DecoderReplacementFallbackBuffer_t1250)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.EncoderExceptionFallback
#include "mscorlib_System_Text_EncoderExceptionFallback.h"
// Metadata Definition System.Text.EncoderExceptionFallback
extern TypeInfo EncoderExceptionFallback_t1251_il2cpp_TypeInfo;
// System.Text.EncoderExceptionFallback
#include "mscorlib_System_Text_EncoderExceptionFallbackMethodDeclarations.h"
static const EncodedMethodIndex EncoderExceptionFallback_t1251_VTable[5] = 
{
	2879,
	125,
	2880,
	123,
	2881,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderExceptionFallback_t1251_0_0_0;
extern const Il2CppType EncoderExceptionFallback_t1251_1_0_0;
extern const Il2CppType EncoderFallback_t1252_0_0_0;
struct EncoderExceptionFallback_t1251;
const Il2CppTypeDefinitionMetadata EncoderExceptionFallback_t1251_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EncoderFallback_t1252_0_0_0/* parent */
	, EncoderExceptionFallback_t1251_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7693/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EncoderExceptionFallback_t1251_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderExceptionFallback"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderExceptionFallback_t1251_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderExceptionFallback_t1251_0_0_0/* byval_arg */
	, &EncoderExceptionFallback_t1251_1_0_0/* this_arg */
	, &EncoderExceptionFallback_t1251_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderExceptionFallback_t1251)/* instance_size */
	, sizeof (EncoderExceptionFallback_t1251)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.EncoderExceptionFallbackBuffer
#include "mscorlib_System_Text_EncoderExceptionFallbackBuffer.h"
// Metadata Definition System.Text.EncoderExceptionFallbackBuffer
extern TypeInfo EncoderExceptionFallbackBuffer_t1253_il2cpp_TypeInfo;
// System.Text.EncoderExceptionFallbackBuffer
#include "mscorlib_System_Text_EncoderExceptionFallbackBufferMethodDeclarations.h"
static const EncodedMethodIndex EncoderExceptionFallbackBuffer_t1253_VTable[8] = 
{
	120,
	125,
	122,
	123,
	2882,
	2883,
	2884,
	2885,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderExceptionFallbackBuffer_t1253_0_0_0;
extern const Il2CppType EncoderExceptionFallbackBuffer_t1253_1_0_0;
extern const Il2CppType EncoderFallbackBuffer_t1254_0_0_0;
struct EncoderExceptionFallbackBuffer_t1253;
const Il2CppTypeDefinitionMetadata EncoderExceptionFallbackBuffer_t1253_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EncoderFallbackBuffer_t1254_0_0_0/* parent */
	, EncoderExceptionFallbackBuffer_t1253_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7697/* methodStart */
	, -1/* eventStart */
	, 1557/* propertyStart */

};
TypeInfo EncoderExceptionFallbackBuffer_t1253_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderExceptionFallbackBuffer"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderExceptionFallbackBuffer_t1253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderExceptionFallbackBuffer_t1253_0_0_0/* byval_arg */
	, &EncoderExceptionFallbackBuffer_t1253_1_0_0/* this_arg */
	, &EncoderExceptionFallbackBuffer_t1253_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderExceptionFallbackBuffer_t1253)/* instance_size */
	, sizeof (EncoderExceptionFallbackBuffer_t1253)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.EncoderFallback
#include "mscorlib_System_Text_EncoderFallback.h"
// Metadata Definition System.Text.EncoderFallback
extern TypeInfo EncoderFallback_t1252_il2cpp_TypeInfo;
// System.Text.EncoderFallback
#include "mscorlib_System_Text_EncoderFallbackMethodDeclarations.h"
static const EncodedMethodIndex EncoderFallback_t1252_VTable[5] = 
{
	120,
	125,
	122,
	123,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderFallback_t1252_1_0_0;
struct EncoderFallback_t1252;
const Il2CppTypeDefinitionMetadata EncoderFallback_t1252_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EncoderFallback_t1252_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4988/* fieldStart */
	, 7702/* methodStart */
	, -1/* eventStart */
	, 1558/* propertyStart */

};
TypeInfo EncoderFallback_t1252_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderFallback"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderFallback_t1252_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderFallback_t1252_0_0_0/* byval_arg */
	, &EncoderFallback_t1252_1_0_0/* this_arg */
	, &EncoderFallback_t1252_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderFallback_t1252)/* instance_size */
	, sizeof (EncoderFallback_t1252)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EncoderFallback_t1252_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.EncoderFallbackBuffer
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
// Metadata Definition System.Text.EncoderFallbackBuffer
extern TypeInfo EncoderFallbackBuffer_t1254_il2cpp_TypeInfo;
// System.Text.EncoderFallbackBuffer
#include "mscorlib_System_Text_EncoderFallbackBufferMethodDeclarations.h"
static const EncodedMethodIndex EncoderFallbackBuffer_t1254_VTable[8] = 
{
	120,
	125,
	122,
	123,
	0,
	0,
	0,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderFallbackBuffer_t1254_1_0_0;
struct EncoderFallbackBuffer_t1254;
const Il2CppTypeDefinitionMetadata EncoderFallbackBuffer_t1254_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EncoderFallbackBuffer_t1254_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7708/* methodStart */
	, -1/* eventStart */
	, 1561/* propertyStart */

};
TypeInfo EncoderFallbackBuffer_t1254_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderFallbackBuffer"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderFallbackBuffer_t1254_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderFallbackBuffer_t1254_0_0_0/* byval_arg */
	, &EncoderFallbackBuffer_t1254_1_0_0/* this_arg */
	, &EncoderFallbackBuffer_t1254_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderFallbackBuffer_t1254)/* instance_size */
	, sizeof (EncoderFallbackBuffer_t1254)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.EncoderFallbackException
#include "mscorlib_System_Text_EncoderFallbackException.h"
// Metadata Definition System.Text.EncoderFallbackException
extern TypeInfo EncoderFallbackException_t1255_il2cpp_TypeInfo;
// System.Text.EncoderFallbackException
#include "mscorlib_System_Text_EncoderFallbackExceptionMethodDeclarations.h"
static const EncodedMethodIndex EncoderFallbackException_t1255_VTable[12] = 
{
	120,
	125,
	122,
	203,
	2869,
	205,
	2870,
	207,
	208,
	2869,
	209,
	2871,
};
static Il2CppInterfaceOffsetPair EncoderFallbackException_t1255_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderFallbackException_t1255_0_0_0;
extern const Il2CppType EncoderFallbackException_t1255_1_0_0;
struct EncoderFallbackException_t1255;
const Il2CppTypeDefinitionMetadata EncoderFallbackException_t1255_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EncoderFallbackException_t1255_InterfacesOffsets/* interfaceOffsets */
	, &ArgumentException_t313_0_0_0/* parent */
	, EncoderFallbackException_t1255_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4991/* fieldStart */
	, 7713/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EncoderFallbackException_t1255_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderFallbackException"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderFallbackException_t1255_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderFallbackException_t1255_0_0_0/* byval_arg */
	, &EncoderFallbackException_t1255_1_0_0/* this_arg */
	, &EncoderFallbackException_t1255_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderFallbackException_t1255)/* instance_size */
	, sizeof (EncoderFallbackException_t1255)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.EncoderReplacementFallback
#include "mscorlib_System_Text_EncoderReplacementFallback.h"
// Metadata Definition System.Text.EncoderReplacementFallback
extern TypeInfo EncoderReplacementFallback_t1256_il2cpp_TypeInfo;
// System.Text.EncoderReplacementFallback
#include "mscorlib_System_Text_EncoderReplacementFallbackMethodDeclarations.h"
static const EncodedMethodIndex EncoderReplacementFallback_t1256_VTable[5] = 
{
	2886,
	125,
	2887,
	123,
	2888,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderReplacementFallback_t1256_0_0_0;
extern const Il2CppType EncoderReplacementFallback_t1256_1_0_0;
struct EncoderReplacementFallback_t1256;
const Il2CppTypeDefinitionMetadata EncoderReplacementFallback_t1256_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EncoderFallback_t1252_0_0_0/* parent */
	, EncoderReplacementFallback_t1256_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4995/* fieldStart */
	, 7717/* methodStart */
	, -1/* eventStart */
	, 1562/* propertyStart */

};
TypeInfo EncoderReplacementFallback_t1256_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderReplacementFallback"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderReplacementFallback_t1256_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderReplacementFallback_t1256_0_0_0/* byval_arg */
	, &EncoderReplacementFallback_t1256_1_0_0/* this_arg */
	, &EncoderReplacementFallback_t1256_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderReplacementFallback_t1256)/* instance_size */
	, sizeof (EncoderReplacementFallback_t1256)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.EncoderReplacementFallbackBuffer
#include "mscorlib_System_Text_EncoderReplacementFallbackBuffer.h"
// Metadata Definition System.Text.EncoderReplacementFallbackBuffer
extern TypeInfo EncoderReplacementFallbackBuffer_t1257_il2cpp_TypeInfo;
// System.Text.EncoderReplacementFallbackBuffer
#include "mscorlib_System_Text_EncoderReplacementFallbackBufferMethodDeclarations.h"
static const EncodedMethodIndex EncoderReplacementFallbackBuffer_t1257_VTable[8] = 
{
	120,
	125,
	122,
	123,
	2889,
	2890,
	2891,
	2892,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderReplacementFallbackBuffer_t1257_0_0_0;
extern const Il2CppType EncoderReplacementFallbackBuffer_t1257_1_0_0;
struct EncoderReplacementFallbackBuffer_t1257;
const Il2CppTypeDefinitionMetadata EncoderReplacementFallbackBuffer_t1257_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EncoderFallbackBuffer_t1254_0_0_0/* parent */
	, EncoderReplacementFallbackBuffer_t1257_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4996/* fieldStart */
	, 7723/* methodStart */
	, -1/* eventStart */
	, 1563/* propertyStart */

};
TypeInfo EncoderReplacementFallbackBuffer_t1257_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderReplacementFallbackBuffer"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderReplacementFallbackBuffer_t1257_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderReplacementFallbackBuffer_t1257_0_0_0/* byval_arg */
	, &EncoderReplacementFallbackBuffer_t1257_1_0_0/* this_arg */
	, &EncoderReplacementFallbackBuffer_t1257_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderReplacementFallbackBuffer_t1257)/* instance_size */
	, sizeof (EncoderReplacementFallbackBuffer_t1257)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.Encoding
#include "mscorlib_System_Text_Encoding.h"
// Metadata Definition System.Text.Encoding
extern TypeInfo Encoding_t281_il2cpp_TypeInfo;
// System.Text.Encoding
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
extern const Il2CppType ForwardingDecoder_t1258_0_0_0;
static const Il2CppType* Encoding_t281_il2cpp_TypeInfo__nestedTypes[1] =
{
	&ForwardingDecoder_t1258_0_0_0,
};
static const EncodedMethodIndex Encoding_t281_VTable[25] = 
{
	2839,
	125,
	2840,
	123,
	0,
	2893,
	2843,
	0,
	2894,
	2846,
	2847,
	2848,
	0,
	0,
	2851,
	2895,
	0,
	0,
	2855,
	2896,
	2857,
	2858,
	2859,
	2897,
	2898,
};
static const Il2CppType* Encoding_t281_InterfacesTypeInfos[] = 
{
	&ICloneable_t2056_0_0_0,
};
static Il2CppInterfaceOffsetPair Encoding_t281_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Encoding_t281_1_0_0;
struct Encoding_t281;
const Il2CppTypeDefinitionMetadata Encoding_t281_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Encoding_t281_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Encoding_t281_InterfacesTypeInfos/* implementedInterfaces */
	, Encoding_t281_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Encoding_t281_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4999/* fieldStart */
	, 7729/* methodStart */
	, -1/* eventStart */
	, 1564/* propertyStart */

};
TypeInfo Encoding_t281_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Encoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &Encoding_t281_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1283/* custom_attributes_cache */
	, &Encoding_t281_0_0_0/* byval_arg */
	, &Encoding_t281_1_0_0/* this_arg */
	, &Encoding_t281_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Encoding_t281)/* instance_size */
	, sizeof (Encoding_t281)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Encoding_t281_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 47/* method_count */
	, 16/* property_count */
	, 28/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 25/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.Encoding/ForwardingDecoder
#include "mscorlib_System_Text_Encoding_ForwardingDecoder.h"
// Metadata Definition System.Text.Encoding/ForwardingDecoder
extern TypeInfo ForwardingDecoder_t1258_il2cpp_TypeInfo;
// System.Text.Encoding/ForwardingDecoder
#include "mscorlib_System_Text_Encoding_ForwardingDecoderMethodDeclarations.h"
static const EncodedMethodIndex ForwardingDecoder_t1258_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2899,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ForwardingDecoder_t1258_1_0_0;
struct ForwardingDecoder_t1258;
const Il2CppTypeDefinitionMetadata ForwardingDecoder_t1258_DefinitionMetadata = 
{
	&Encoding_t281_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Decoder_t912_0_0_0/* parent */
	, ForwardingDecoder_t1258_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5027/* fieldStart */
	, 7776/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ForwardingDecoder_t1258_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ForwardingDecoder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ForwardingDecoder_t1258_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ForwardingDecoder_t1258_0_0_0/* byval_arg */
	, &ForwardingDecoder_t1258_1_0_0/* this_arg */
	, &ForwardingDecoder_t1258_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ForwardingDecoder_t1258)/* instance_size */
	, sizeof (ForwardingDecoder_t1258)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.Latin1Encoding
#include "mscorlib_System_Text_Latin1Encoding.h"
// Metadata Definition System.Text.Latin1Encoding
extern TypeInfo Latin1Encoding_t1259_il2cpp_TypeInfo;
// System.Text.Latin1Encoding
#include "mscorlib_System_Text_Latin1EncodingMethodDeclarations.h"
static const EncodedMethodIndex Latin1Encoding_t1259_VTable[25] = 
{
	2839,
	125,
	2840,
	123,
	2900,
	2901,
	2843,
	2902,
	2903,
	2846,
	2847,
	2848,
	2904,
	2905,
	2851,
	2895,
	2906,
	2907,
	2855,
	2908,
	2909,
	2910,
	2911,
	2897,
	2898,
};
static Il2CppInterfaceOffsetPair Latin1Encoding_t1259_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Latin1Encoding_t1259_0_0_0;
extern const Il2CppType Latin1Encoding_t1259_1_0_0;
struct Latin1Encoding_t1259;
const Il2CppTypeDefinitionMetadata Latin1Encoding_t1259_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Latin1Encoding_t1259_InterfacesOffsets/* interfaceOffsets */
	, &Encoding_t281_0_0_0/* parent */
	, Latin1Encoding_t1259_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7778/* methodStart */
	, -1/* eventStart */
	, 1580/* propertyStart */

};
TypeInfo Latin1Encoding_t1259_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Latin1Encoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &Latin1Encoding_t1259_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Latin1Encoding_t1259_0_0_0/* byval_arg */
	, &Latin1Encoding_t1259_1_0_0/* this_arg */
	, &Latin1Encoding_t1259_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Latin1Encoding_t1259)/* instance_size */
	, sizeof (Latin1Encoding_t1259)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
// Metadata Definition System.Text.StringBuilder
extern TypeInfo StringBuilder_t301_il2cpp_TypeInfo;
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
static const EncodedMethodIndex StringBuilder_t301_VTable[5] = 
{
	120,
	125,
	122,
	2912,
	2913,
};
static const Il2CppType* StringBuilder_t301_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair StringBuilder_t301_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringBuilder_t301_0_0_0;
extern const Il2CppType StringBuilder_t301_1_0_0;
struct StringBuilder_t301;
const Il2CppTypeDefinitionMetadata StringBuilder_t301_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StringBuilder_t301_InterfacesTypeInfos/* implementedInterfaces */
	, StringBuilder_t301_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StringBuilder_t301_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5028/* fieldStart */
	, 7793/* methodStart */
	, -1/* eventStart */
	, 1582/* propertyStart */

};
TypeInfo StringBuilder_t301_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringBuilder"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &StringBuilder_t301_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1290/* custom_attributes_cache */
	, &StringBuilder_t301_0_0_0/* byval_arg */
	, &StringBuilder_t301_1_0_0/* this_arg */
	, &StringBuilder_t301_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringBuilder_t301)/* instance_size */
	, sizeof (StringBuilder_t301)/* actualSize */
	, 0/* element_size */
	, sizeof(char*)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 36/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.UTF32Encoding
#include "mscorlib_System_Text_UTF32Encoding.h"
// Metadata Definition System.Text.UTF32Encoding
extern TypeInfo UTF32Encoding_t1261_il2cpp_TypeInfo;
// System.Text.UTF32Encoding
#include "mscorlib_System_Text_UTF32EncodingMethodDeclarations.h"
extern const Il2CppType UTF32Decoder_t1260_0_0_0;
static const Il2CppType* UTF32Encoding_t1261_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UTF32Decoder_t1260_0_0_0,
};
static const EncodedMethodIndex UTF32Encoding_t1261_VTable[25] = 
{
	2914,
	125,
	2915,
	123,
	2916,
	2917,
	2843,
	2918,
	2919,
	2846,
	2847,
	2848,
	2920,
	2921,
	2851,
	2922,
	2923,
	2924,
	2925,
	2926,
	2857,
	2858,
	2859,
	2927,
	2928,
};
static Il2CppInterfaceOffsetPair UTF32Encoding_t1261_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UTF32Encoding_t1261_0_0_0;
extern const Il2CppType UTF32Encoding_t1261_1_0_0;
struct UTF32Encoding_t1261;
const Il2CppTypeDefinitionMetadata UTF32Encoding_t1261_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UTF32Encoding_t1261_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UTF32Encoding_t1261_InterfacesOffsets/* interfaceOffsets */
	, &Encoding_t281_0_0_0/* parent */
	, UTF32Encoding_t1261_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5033/* fieldStart */
	, 7829/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UTF32Encoding_t1261_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UTF32Encoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &UTF32Encoding_t1261_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UTF32Encoding_t1261_0_0_0/* byval_arg */
	, &UTF32Encoding_t1261_1_0_0/* this_arg */
	, &UTF32Encoding_t1261_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UTF32Encoding_t1261)/* instance_size */
	, sizeof (UTF32Encoding_t1261)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.UTF32Encoding/UTF32Decoder
#include "mscorlib_System_Text_UTF32Encoding_UTF32Decoder.h"
// Metadata Definition System.Text.UTF32Encoding/UTF32Decoder
extern TypeInfo UTF32Decoder_t1260_il2cpp_TypeInfo;
// System.Text.UTF32Encoding/UTF32Decoder
#include "mscorlib_System_Text_UTF32Encoding_UTF32DecoderMethodDeclarations.h"
static const EncodedMethodIndex UTF32Decoder_t1260_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2929,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UTF32Decoder_t1260_1_0_0;
struct UTF32Decoder_t1260;
const Il2CppTypeDefinitionMetadata UTF32Decoder_t1260_DefinitionMetadata = 
{
	&UTF32Encoding_t1261_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Decoder_t912_0_0_0/* parent */
	, UTF32Decoder_t1260_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5035/* fieldStart */
	, 7847/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UTF32Decoder_t1260_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UTF32Decoder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &UTF32Decoder_t1260_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UTF32Decoder_t1260_0_0_0/* byval_arg */
	, &UTF32Decoder_t1260_1_0_0/* this_arg */
	, &UTF32Decoder_t1260_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UTF32Decoder_t1260)/* instance_size */
	, sizeof (UTF32Decoder_t1260)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.UTF7Encoding
#include "mscorlib_System_Text_UTF7Encoding.h"
// Metadata Definition System.Text.UTF7Encoding
extern TypeInfo UTF7Encoding_t1264_il2cpp_TypeInfo;
// System.Text.UTF7Encoding
#include "mscorlib_System_Text_UTF7EncodingMethodDeclarations.h"
extern const Il2CppType UTF7Decoder_t1262_0_0_0;
static const Il2CppType* UTF7Encoding_t1264_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UTF7Decoder_t1262_0_0_0,
};
static const EncodedMethodIndex UTF7Encoding_t1264_VTable[25] = 
{
	2930,
	125,
	2931,
	123,
	2932,
	2933,
	2843,
	2934,
	2935,
	2846,
	2847,
	2848,
	2936,
	2937,
	2851,
	2938,
	2939,
	2940,
	2855,
	2941,
	2857,
	2858,
	2859,
	2942,
	2943,
};
static Il2CppInterfaceOffsetPair UTF7Encoding_t1264_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UTF7Encoding_t1264_0_0_0;
extern const Il2CppType UTF7Encoding_t1264_1_0_0;
struct UTF7Encoding_t1264;
const Il2CppTypeDefinitionMetadata UTF7Encoding_t1264_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UTF7Encoding_t1264_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UTF7Encoding_t1264_InterfacesOffsets/* interfaceOffsets */
	, &Encoding_t281_0_0_0/* parent */
	, UTF7Encoding_t1264_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5038/* fieldStart */
	, 7849/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UTF7Encoding_t1264_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UTF7Encoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &UTF7Encoding_t1264_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1297/* custom_attributes_cache */
	, &UTF7Encoding_t1264_0_0_0/* byval_arg */
	, &UTF7Encoding_t1264_1_0_0/* this_arg */
	, &UTF7Encoding_t1264_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UTF7Encoding_t1264)/* instance_size */
	, sizeof (UTF7Encoding_t1264)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(UTF7Encoding_t1264_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.UTF7Encoding/UTF7Decoder
#include "mscorlib_System_Text_UTF7Encoding_UTF7Decoder.h"
// Metadata Definition System.Text.UTF7Encoding/UTF7Decoder
extern TypeInfo UTF7Decoder_t1262_il2cpp_TypeInfo;
// System.Text.UTF7Encoding/UTF7Decoder
#include "mscorlib_System_Text_UTF7Encoding_UTF7DecoderMethodDeclarations.h"
static const EncodedMethodIndex UTF7Decoder_t1262_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2944,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UTF7Decoder_t1262_1_0_0;
struct UTF7Decoder_t1262;
const Il2CppTypeDefinitionMetadata UTF7Decoder_t1262_DefinitionMetadata = 
{
	&UTF7Encoding_t1264_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Decoder_t912_0_0_0/* parent */
	, UTF7Decoder_t1262_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5041/* fieldStart */
	, 7870/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UTF7Decoder_t1262_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UTF7Decoder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &UTF7Decoder_t1262_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UTF7Decoder_t1262_0_0_0/* byval_arg */
	, &UTF7Decoder_t1262_1_0_0/* this_arg */
	, &UTF7Decoder_t1262_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UTF7Decoder_t1262)/* instance_size */
	, sizeof (UTF7Decoder_t1262)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.UTF8Encoding
#include "mscorlib_System_Text_UTF8Encoding.h"
// Metadata Definition System.Text.UTF8Encoding
extern TypeInfo UTF8Encoding_t1266_il2cpp_TypeInfo;
// System.Text.UTF8Encoding
#include "mscorlib_System_Text_UTF8EncodingMethodDeclarations.h"
extern const Il2CppType UTF8Decoder_t1265_0_0_0;
static const Il2CppType* UTF8Encoding_t1266_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UTF8Decoder_t1265_0_0_0,
};
static const EncodedMethodIndex UTF8Encoding_t1266_VTable[25] = 
{
	2945,
	125,
	2946,
	123,
	2947,
	2948,
	2843,
	2949,
	2950,
	2846,
	2847,
	2848,
	2951,
	2952,
	2851,
	2953,
	2954,
	2955,
	2956,
	2957,
	2857,
	2858,
	2859,
	2958,
	2959,
};
static Il2CppInterfaceOffsetPair UTF8Encoding_t1266_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UTF8Encoding_t1266_0_0_0;
extern const Il2CppType UTF8Encoding_t1266_1_0_0;
struct UTF8Encoding_t1266;
const Il2CppTypeDefinitionMetadata UTF8Encoding_t1266_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UTF8Encoding_t1266_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UTF8Encoding_t1266_InterfacesOffsets/* interfaceOffsets */
	, &Encoding_t281_0_0_0/* parent */
	, UTF8Encoding_t1266_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5042/* fieldStart */
	, 7872/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UTF8Encoding_t1266_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UTF8Encoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &UTF8Encoding_t1266_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1305/* custom_attributes_cache */
	, &UTF8Encoding_t1266_0_0_0/* byval_arg */
	, &UTF8Encoding_t1266_1_0_0/* this_arg */
	, &UTF8Encoding_t1266_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UTF8Encoding_t1266)/* instance_size */
	, sizeof (UTF8Encoding_t1266)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.UTF8Encoding/UTF8Decoder
#include "mscorlib_System_Text_UTF8Encoding_UTF8Decoder.h"
// Metadata Definition System.Text.UTF8Encoding/UTF8Decoder
extern TypeInfo UTF8Decoder_t1265_il2cpp_TypeInfo;
// System.Text.UTF8Encoding/UTF8Decoder
#include "mscorlib_System_Text_UTF8Encoding_UTF8DecoderMethodDeclarations.h"
static const EncodedMethodIndex UTF8Decoder_t1265_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2960,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UTF8Decoder_t1265_1_0_0;
struct UTF8Decoder_t1265;
const Il2CppTypeDefinitionMetadata UTF8Decoder_t1265_DefinitionMetadata = 
{
	&UTF8Encoding_t1266_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Decoder_t912_0_0_0/* parent */
	, UTF8Decoder_t1265_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5043/* fieldStart */
	, 7900/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UTF8Decoder_t1265_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UTF8Decoder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &UTF8Decoder_t1265_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UTF8Decoder_t1265_0_0_0/* byval_arg */
	, &UTF8Decoder_t1265_1_0_0/* this_arg */
	, &UTF8Decoder_t1265_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UTF8Decoder_t1265)/* instance_size */
	, sizeof (UTF8Decoder_t1265)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.UnicodeEncoding
#include "mscorlib_System_Text_UnicodeEncoding.h"
// Metadata Definition System.Text.UnicodeEncoding
extern TypeInfo UnicodeEncoding_t1268_il2cpp_TypeInfo;
// System.Text.UnicodeEncoding
#include "mscorlib_System_Text_UnicodeEncodingMethodDeclarations.h"
extern const Il2CppType UnicodeDecoder_t1267_0_0_0;
static const Il2CppType* UnicodeEncoding_t1268_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UnicodeDecoder_t1267_0_0_0,
};
static const EncodedMethodIndex UnicodeEncoding_t1268_VTable[25] = 
{
	2961,
	125,
	2962,
	123,
	2963,
	2964,
	2843,
	2965,
	2966,
	2846,
	2847,
	2848,
	2967,
	2968,
	2851,
	2969,
	2970,
	2971,
	2972,
	2973,
	2857,
	2858,
	2859,
	2974,
	2975,
};
static Il2CppInterfaceOffsetPair UnicodeEncoding_t1268_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnicodeEncoding_t1268_0_0_0;
extern const Il2CppType UnicodeEncoding_t1268_1_0_0;
struct UnicodeEncoding_t1268;
const Il2CppTypeDefinitionMetadata UnicodeEncoding_t1268_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UnicodeEncoding_t1268_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnicodeEncoding_t1268_InterfacesOffsets/* interfaceOffsets */
	, &Encoding_t281_0_0_0/* parent */
	, UnicodeEncoding_t1268_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5045/* fieldStart */
	, 7902/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnicodeEncoding_t1268_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnicodeEncoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &UnicodeEncoding_t1268_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1309/* custom_attributes_cache */
	, &UnicodeEncoding_t1268_0_0_0/* byval_arg */
	, &UnicodeEncoding_t1268_1_0_0/* this_arg */
	, &UnicodeEncoding_t1268_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnicodeEncoding_t1268)/* instance_size */
	, sizeof (UnicodeEncoding_t1268)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.UnicodeEncoding/UnicodeDecoder
#include "mscorlib_System_Text_UnicodeEncoding_UnicodeDecoder.h"
// Metadata Definition System.Text.UnicodeEncoding/UnicodeDecoder
extern TypeInfo UnicodeDecoder_t1267_il2cpp_TypeInfo;
// System.Text.UnicodeEncoding/UnicodeDecoder
#include "mscorlib_System_Text_UnicodeEncoding_UnicodeDecoderMethodDeclarations.h"
static const EncodedMethodIndex UnicodeDecoder_t1267_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2976,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnicodeDecoder_t1267_1_0_0;
struct UnicodeDecoder_t1267;
const Il2CppTypeDefinitionMetadata UnicodeDecoder_t1267_DefinitionMetadata = 
{
	&UnicodeEncoding_t1268_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Decoder_t912_0_0_0/* parent */
	, UnicodeDecoder_t1267_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5047/* fieldStart */
	, 7923/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnicodeDecoder_t1267_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnicodeDecoder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &UnicodeDecoder_t1267_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnicodeDecoder_t1267_0_0_0/* byval_arg */
	, &UnicodeDecoder_t1267_1_0_0/* this_arg */
	, &UnicodeDecoder_t1267_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnicodeDecoder_t1267)/* instance_size */
	, sizeof (UnicodeDecoder_t1267)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Threading.CompressedStack
#include "mscorlib_System_Threading_CompressedStack.h"
// Metadata Definition System.Threading.CompressedStack
extern TypeInfo CompressedStack_t1230_il2cpp_TypeInfo;
// System.Threading.CompressedStack
#include "mscorlib_System_Threading_CompressedStackMethodDeclarations.h"
static const EncodedMethodIndex CompressedStack_t1230_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2977,
};
static const Il2CppType* CompressedStack_t1230_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair CompressedStack_t1230_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompressedStack_t1230_0_0_0;
extern const Il2CppType CompressedStack_t1230_1_0_0;
struct CompressedStack_t1230;
const Il2CppTypeDefinitionMetadata CompressedStack_t1230_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CompressedStack_t1230_InterfacesTypeInfos/* implementedInterfaces */
	, CompressedStack_t1230_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CompressedStack_t1230_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5049/* fieldStart */
	, 7925/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CompressedStack_t1230_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompressedStack"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &CompressedStack_t1230_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CompressedStack_t1230_0_0_0/* byval_arg */
	, &CompressedStack_t1230_1_0_0/* this_arg */
	, &CompressedStack_t1230_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompressedStack_t1230)/* instance_size */
	, sizeof (CompressedStack_t1230)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.EventResetMode
#include "mscorlib_System_Threading_EventResetMode.h"
// Metadata Definition System.Threading.EventResetMode
extern TypeInfo EventResetMode_t1269_il2cpp_TypeInfo;
// System.Threading.EventResetMode
#include "mscorlib_System_Threading_EventResetModeMethodDeclarations.h"
static const EncodedMethodIndex EventResetMode_t1269_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair EventResetMode_t1269_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventResetMode_t1269_0_0_0;
extern const Il2CppType EventResetMode_t1269_1_0_0;
const Il2CppTypeDefinitionMetadata EventResetMode_t1269_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventResetMode_t1269_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, EventResetMode_t1269_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5050/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventResetMode_t1269_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventResetMode"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1315/* custom_attributes_cache */
	, &EventResetMode_t1269_0_0_0/* byval_arg */
	, &EventResetMode_t1269_1_0_0/* this_arg */
	, &EventResetMode_t1269_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventResetMode_t1269)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EventResetMode_t1269)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Threading.EventWaitHandle
#include "mscorlib_System_Threading_EventWaitHandle.h"
// Metadata Definition System.Threading.EventWaitHandle
extern TypeInfo EventWaitHandle_t1270_il2cpp_TypeInfo;
// System.Threading.EventWaitHandle
#include "mscorlib_System_Threading_EventWaitHandleMethodDeclarations.h"
static const EncodedMethodIndex EventWaitHandle_t1270_VTable[10] = 
{
	120,
	2978,
	122,
	123,
	2979,
	2980,
	2981,
	2982,
	2983,
	2984,
};
static Il2CppInterfaceOffsetPair EventWaitHandle_t1270_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventWaitHandle_t1270_0_0_0;
extern const Il2CppType EventWaitHandle_t1270_1_0_0;
extern const Il2CppType WaitHandle_t738_0_0_0;
struct EventWaitHandle_t1270;
const Il2CppTypeDefinitionMetadata EventWaitHandle_t1270_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventWaitHandle_t1270_InterfacesOffsets/* interfaceOffsets */
	, &WaitHandle_t738_0_0_0/* parent */
	, EventWaitHandle_t1270_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7931/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventWaitHandle_t1270_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventWaitHandle"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &EventWaitHandle_t1270_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1316/* custom_attributes_cache */
	, &EventWaitHandle_t1270_0_0_0/* byval_arg */
	, &EventWaitHandle_t1270_1_0_0/* this_arg */
	, &EventWaitHandle_t1270_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventWaitHandle_t1270)/* instance_size */
	, sizeof (EventWaitHandle_t1270)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.ExecutionContext
#include "mscorlib_System_Threading_ExecutionContext.h"
// Metadata Definition System.Threading.ExecutionContext
extern TypeInfo ExecutionContext_t1081_il2cpp_TypeInfo;
// System.Threading.ExecutionContext
#include "mscorlib_System_Threading_ExecutionContextMethodDeclarations.h"
static const EncodedMethodIndex ExecutionContext_t1081_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2985,
};
static const Il2CppType* ExecutionContext_t1081_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair ExecutionContext_t1081_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ExecutionContext_t1081_0_0_0;
extern const Il2CppType ExecutionContext_t1081_1_0_0;
struct ExecutionContext_t1081;
const Il2CppTypeDefinitionMetadata ExecutionContext_t1081_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ExecutionContext_t1081_InterfacesTypeInfos/* implementedInterfaces */
	, ExecutionContext_t1081_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ExecutionContext_t1081_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5053/* fieldStart */
	, 7935/* methodStart */
	, -1/* eventStart */
	, 1585/* propertyStart */

};
TypeInfo ExecutionContext_t1081_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecutionContext"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ExecutionContext_t1081_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExecutionContext_t1081_0_0_0/* byval_arg */
	, &ExecutionContext_t1081_1_0_0/* this_arg */
	, &ExecutionContext_t1081_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecutionContext_t1081)/* instance_size */
	, sizeof (ExecutionContext_t1081)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.Interlocked
#include "mscorlib_System_Threading_Interlocked.h"
// Metadata Definition System.Threading.Interlocked
extern TypeInfo Interlocked_t1271_il2cpp_TypeInfo;
// System.Threading.Interlocked
#include "mscorlib_System_Threading_InterlockedMethodDeclarations.h"
static const EncodedMethodIndex Interlocked_t1271_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Interlocked_t1271_0_0_0;
extern const Il2CppType Interlocked_t1271_1_0_0;
struct Interlocked_t1271;
const Il2CppTypeDefinitionMetadata Interlocked_t1271_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Interlocked_t1271_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7944/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Interlocked_t1271_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Interlocked"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Interlocked_t1271_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Interlocked_t1271_0_0_0/* byval_arg */
	, &Interlocked_t1271_1_0_0/* this_arg */
	, &Interlocked_t1271_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Interlocked_t1271)/* instance_size */
	, sizeof (Interlocked_t1271)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Threading.ManualResetEvent
#include "mscorlib_System_Threading_ManualResetEvent.h"
// Metadata Definition System.Threading.ManualResetEvent
extern TypeInfo ManualResetEvent_t691_il2cpp_TypeInfo;
// System.Threading.ManualResetEvent
#include "mscorlib_System_Threading_ManualResetEventMethodDeclarations.h"
static const EncodedMethodIndex ManualResetEvent_t691_VTable[10] = 
{
	120,
	2978,
	122,
	123,
	2979,
	2980,
	2981,
	2982,
	2983,
	2984,
};
static Il2CppInterfaceOffsetPair ManualResetEvent_t691_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ManualResetEvent_t691_0_0_0;
extern const Il2CppType ManualResetEvent_t691_1_0_0;
struct ManualResetEvent_t691;
const Il2CppTypeDefinitionMetadata ManualResetEvent_t691_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ManualResetEvent_t691_InterfacesOffsets/* interfaceOffsets */
	, &EventWaitHandle_t1270_0_0_0/* parent */
	, ManualResetEvent_t691_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7945/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ManualResetEvent_t691_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ManualResetEvent"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ManualResetEvent_t691_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1320/* custom_attributes_cache */
	, &ManualResetEvent_t691_0_0_0/* byval_arg */
	, &ManualResetEvent_t691_1_0_0/* this_arg */
	, &ManualResetEvent_t691_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ManualResetEvent_t691)/* instance_size */
	, sizeof (ManualResetEvent_t691)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.Monitor
#include "mscorlib_System_Threading_Monitor.h"
// Metadata Definition System.Threading.Monitor
extern TypeInfo Monitor_t1272_il2cpp_TypeInfo;
// System.Threading.Monitor
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
static const EncodedMethodIndex Monitor_t1272_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Monitor_t1272_0_0_0;
extern const Il2CppType Monitor_t1272_1_0_0;
struct Monitor_t1272;
const Il2CppTypeDefinitionMetadata Monitor_t1272_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Monitor_t1272_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7946/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Monitor_t1272_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Monitor"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Monitor_t1272_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1321/* custom_attributes_cache */
	, &Monitor_t1272_0_0_0/* byval_arg */
	, &Monitor_t1272_1_0_0/* this_arg */
	, &Monitor_t1272_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Monitor_t1272)/* instance_size */
	, sizeof (Monitor_t1272)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Threading.Mutex
#include "mscorlib_System_Threading_Mutex.h"
// Metadata Definition System.Threading.Mutex
extern TypeInfo Mutex_t1073_il2cpp_TypeInfo;
// System.Threading.Mutex
#include "mscorlib_System_Threading_MutexMethodDeclarations.h"
static const EncodedMethodIndex Mutex_t1073_VTable[10] = 
{
	120,
	2978,
	122,
	123,
	2979,
	2980,
	2981,
	2982,
	2983,
	2984,
};
static Il2CppInterfaceOffsetPair Mutex_t1073_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Mutex_t1073_0_0_0;
extern const Il2CppType Mutex_t1073_1_0_0;
struct Mutex_t1073;
const Il2CppTypeDefinitionMetadata Mutex_t1073_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Mutex_t1073_InterfacesOffsets/* interfaceOffsets */
	, &WaitHandle_t738_0_0_0/* parent */
	, Mutex_t1073_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7953/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Mutex_t1073_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mutex"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Mutex_t1073_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1323/* custom_attributes_cache */
	, &Mutex_t1073_0_0_0/* byval_arg */
	, &Mutex_t1073_1_0_0/* this_arg */
	, &Mutex_t1073_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mutex_t1073)/* instance_size */
	, sizeof (Mutex_t1073)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.NativeEventCalls
#include "mscorlib_System_Threading_NativeEventCalls.h"
// Metadata Definition System.Threading.NativeEventCalls
extern TypeInfo NativeEventCalls_t1273_il2cpp_TypeInfo;
// System.Threading.NativeEventCalls
#include "mscorlib_System_Threading_NativeEventCallsMethodDeclarations.h"
static const EncodedMethodIndex NativeEventCalls_t1273_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NativeEventCalls_t1273_0_0_0;
extern const Il2CppType NativeEventCalls_t1273_1_0_0;
struct NativeEventCalls_t1273;
const Il2CppTypeDefinitionMetadata NativeEventCalls_t1273_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NativeEventCalls_t1273_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7957/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NativeEventCalls_t1273_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NativeEventCalls"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &NativeEventCalls_t1273_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NativeEventCalls_t1273_0_0_0/* byval_arg */
	, &NativeEventCalls_t1273_1_0_0/* this_arg */
	, &NativeEventCalls_t1273_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NativeEventCalls_t1273)/* instance_size */
	, sizeof (NativeEventCalls_t1273)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Threading.SynchronizationLockException
#include "mscorlib_System_Threading_SynchronizationLockException.h"
// Metadata Definition System.Threading.SynchronizationLockException
extern TypeInfo SynchronizationLockException_t1274_il2cpp_TypeInfo;
// System.Threading.SynchronizationLockException
#include "mscorlib_System_Threading_SynchronizationLockExceptionMethodDeclarations.h"
static const EncodedMethodIndex SynchronizationLockException_t1274_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair SynchronizationLockException_t1274_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizationLockException_t1274_0_0_0;
extern const Il2CppType SynchronizationLockException_t1274_1_0_0;
struct SynchronizationLockException_t1274;
const Il2CppTypeDefinitionMetadata SynchronizationLockException_t1274_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SynchronizationLockException_t1274_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, SynchronizationLockException_t1274_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7961/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SynchronizationLockException_t1274_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizationLockException"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &SynchronizationLockException_t1274_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1326/* custom_attributes_cache */
	, &SynchronizationLockException_t1274_0_0_0/* byval_arg */
	, &SynchronizationLockException_t1274_1_0_0/* this_arg */
	, &SynchronizationLockException_t1274_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizationLockException_t1274)/* instance_size */
	, sizeof (SynchronizationLockException_t1274)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.Thread
#include "mscorlib_System_Threading_Thread.h"
// Metadata Definition System.Threading.Thread
extern TypeInfo Thread_t1074_il2cpp_TypeInfo;
// System.Threading.Thread
#include "mscorlib_System_Threading_ThreadMethodDeclarations.h"
static const EncodedMethodIndex Thread_t1074_VTable[4] = 
{
	120,
	2986,
	2987,
	123,
};
extern const Il2CppType _Thread_t2110_0_0_0;
static const Il2CppType* Thread_t1074_InterfacesTypeInfos[] = 
{
	&_Thread_t2110_0_0_0,
};
static Il2CppInterfaceOffsetPair Thread_t1074_InterfacesOffsets[] = 
{
	{ &_Thread_t2110_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Thread_t1074_0_0_0;
extern const Il2CppType Thread_t1074_1_0_0;
extern const Il2CppType CriticalFinalizerObject_t1038_0_0_0;
struct Thread_t1074;
const Il2CppTypeDefinitionMetadata Thread_t1074_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Thread_t1074_InterfacesTypeInfos/* implementedInterfaces */
	, Thread_t1074_InterfacesOffsets/* interfaceOffsets */
	, &CriticalFinalizerObject_t1038_0_0_0/* parent */
	, Thread_t1074_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5056/* fieldStart */
	, 7964/* methodStart */
	, -1/* eventStart */
	, 1587/* propertyStart */

};
TypeInfo Thread_t1074_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Thread"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Thread_t1074_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1327/* custom_attributes_cache */
	, &Thread_t1074_0_0_0/* byval_arg */
	, &Thread_t1074_1_0_0/* this_arg */
	, &Thread_t1074_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Thread_t1074)/* instance_size */
	, sizeof (Thread_t1074)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Thread_t1074_StaticFields)/* static_fields_size */
	, sizeof(Thread_t1074_ThreadStaticFields)/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 8/* property_count */
	, 52/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.ThreadAbortException
#include "mscorlib_System_Threading_ThreadAbortException.h"
// Metadata Definition System.Threading.ThreadAbortException
extern TypeInfo ThreadAbortException_t1276_il2cpp_TypeInfo;
// System.Threading.ThreadAbortException
#include "mscorlib_System_Threading_ThreadAbortExceptionMethodDeclarations.h"
static const EncodedMethodIndex ThreadAbortException_t1276_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair ThreadAbortException_t1276_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadAbortException_t1276_0_0_0;
extern const Il2CppType ThreadAbortException_t1276_1_0_0;
struct ThreadAbortException_t1276;
const Il2CppTypeDefinitionMetadata ThreadAbortException_t1276_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadAbortException_t1276_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, ThreadAbortException_t1276_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7994/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadAbortException_t1276_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadAbortException"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ThreadAbortException_t1276_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1337/* custom_attributes_cache */
	, &ThreadAbortException_t1276_0_0_0/* byval_arg */
	, &ThreadAbortException_t1276_1_0_0/* this_arg */
	, &ThreadAbortException_t1276_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadAbortException_t1276)/* instance_size */
	, sizeof (ThreadAbortException_t1276)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
