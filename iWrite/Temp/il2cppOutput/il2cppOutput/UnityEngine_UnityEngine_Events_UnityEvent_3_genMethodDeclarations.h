﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct UnityEvent_3_t1686;

// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
extern "C" void UnityEvent_3__ctor_m11257_gshared (UnityEvent_3_t1686 * __this, const MethodInfo* method);
#define UnityEvent_3__ctor_m11257(__this, method) (( void (*) (UnityEvent_3_t1686 *, const MethodInfo*))UnityEvent_3__ctor_m11257_gshared)(__this, method)
