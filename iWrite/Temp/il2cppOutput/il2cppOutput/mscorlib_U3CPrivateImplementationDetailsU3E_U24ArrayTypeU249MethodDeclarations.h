﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$96
struct U24ArrayTypeU2496_t1380;
struct U24ArrayTypeU2496_t1380_marshaled;

void U24ArrayTypeU2496_t1380_marshal(const U24ArrayTypeU2496_t1380& unmarshaled, U24ArrayTypeU2496_t1380_marshaled& marshaled);
void U24ArrayTypeU2496_t1380_marshal_back(const U24ArrayTypeU2496_t1380_marshaled& marshaled, U24ArrayTypeU2496_t1380& unmarshaled);
void U24ArrayTypeU2496_t1380_marshal_cleanup(U24ArrayTypeU2496_t1380_marshaled& marshaled);
