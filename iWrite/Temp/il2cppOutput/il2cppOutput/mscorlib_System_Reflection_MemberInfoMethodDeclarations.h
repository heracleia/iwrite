﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.Module
struct Module_t965;
// System.Object[]
struct ObjectU5BU5D_t200;
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypes.h"

// System.Void System.Reflection.MemberInfo::.ctor()
extern "C" void MemberInfo__ctor_m4169 (MemberInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.MemberInfo::get_Module()
extern "C" Module_t965 * MemberInfo_get_Module_m4170 (MemberInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
