﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Object>
struct GenericEqualityComparer_1_t1759;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Object>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m11844_gshared (GenericEqualityComparer_1_t1759 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m11844(__this, method) (( void (*) (GenericEqualityComparer_1_t1759 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m11844_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Object>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m11845_gshared (GenericEqualityComparer_1_t1759 * __this, Object_t * ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m11845(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1759 *, Object_t *, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m11845_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Object>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m11846_gshared (GenericEqualityComparer_1_t1759 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m11846(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1759 *, Object_t *, Object_t *, const MethodInfo*))GenericEqualityComparer_1_Equals_m11846_gshared)(__this, ___x, ___y, method)
