﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t824;
// Mono.Math.BigInteger
struct BigInteger_t823;

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
extern "C" void ModulusRing__ctor_m4348 (ModulusRing_t824 * __this, BigInteger_t823 * ___modulus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
extern "C" void ModulusRing_BarrettReduction_m4349 (ModulusRing_t824 * __this, BigInteger_t823 * ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t823 * ModulusRing_Multiply_m4350 (ModulusRing_t824 * __this, BigInteger_t823 * ___a, BigInteger_t823 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t823 * ModulusRing_Difference_m4351 (ModulusRing_t824 * __this, BigInteger_t823 * ___a, BigInteger_t823 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t823 * ModulusRing_Pow_m4352 (ModulusRing_t824 * __this, BigInteger_t823 * ___a, BigInteger_t823 * ___k, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
extern "C" BigInteger_t823 * ModulusRing_Pow_m4353 (ModulusRing_t824 * __this, uint32_t ___b, BigInteger_t823 * ___exp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
