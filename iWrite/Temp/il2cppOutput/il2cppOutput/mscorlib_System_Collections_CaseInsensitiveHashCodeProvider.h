﻿#pragma once
#include <stdint.h>
// System.Collections.CaseInsensitiveHashCodeProvider
struct CaseInsensitiveHashCodeProvider_t572;
// System.Object
struct Object_t;
// System.Globalization.TextInfo
struct TextInfo_t813;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.CaseInsensitiveHashCodeProvider
struct  CaseInsensitiveHashCodeProvider_t572  : public Object_t
{
	// System.Globalization.TextInfo System.Collections.CaseInsensitiveHashCodeProvider::m_text
	TextInfo_t813 * ___m_text_2;
};
struct CaseInsensitiveHashCodeProvider_t572_StaticFields{
	// System.Collections.CaseInsensitiveHashCodeProvider System.Collections.CaseInsensitiveHashCodeProvider::singletonInvariant
	CaseInsensitiveHashCodeProvider_t572 * ___singletonInvariant_0;
	// System.Object System.Collections.CaseInsensitiveHashCodeProvider::sync
	Object_t * ___sync_1;
};
