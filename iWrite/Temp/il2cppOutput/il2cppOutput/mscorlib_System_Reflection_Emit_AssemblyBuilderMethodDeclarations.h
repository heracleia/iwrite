﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t948;
// System.String
struct String_t;
// System.Reflection.Module[]
struct ModuleU5BU5D_t947;
// System.Type[]
struct TypeU5BU5D_t196;
// System.Exception
struct Exception_t65;
// System.Reflection.AssemblyName
struct AssemblyName_t995;

// System.String System.Reflection.Emit.AssemblyBuilder::get_Location()
extern "C" String_t* AssemblyBuilder_get_Location_m5505 (AssemblyBuilder_t948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module[] System.Reflection.Emit.AssemblyBuilder::GetModulesInternal()
extern "C" ModuleU5BU5D_t947* AssemblyBuilder_GetModulesInternal_m5506 (AssemblyBuilder_t948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.AssemblyBuilder::GetTypes(System.Boolean)
extern "C" TypeU5BU5D_t196* AssemblyBuilder_GetTypes_m5507 (AssemblyBuilder_t948 * __this, bool ___exportedOnly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.AssemblyBuilder::get_IsCompilerContext()
extern "C" bool AssemblyBuilder_get_IsCompilerContext_m5508 (AssemblyBuilder_t948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.AssemblyBuilder::not_supported()
extern "C" Exception_t65 * AssemblyBuilder_not_supported_m5509 (AssemblyBuilder_t948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Reflection.Emit.AssemblyBuilder::UnprotectedGetName()
extern "C" AssemblyName_t995 * AssemblyBuilder_UnprotectedGetName_m5510 (AssemblyBuilder_t948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
