﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Decimal>
struct InternalEnumerator_1_t1782;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Decimal
#include "mscorlib_System_Decimal.h"

// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11955_gshared (InternalEnumerator_1_t1782 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11955(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1782 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11955_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11956_gshared (InternalEnumerator_1_t1782 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11956(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1782 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11956_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11957_gshared (InternalEnumerator_1_t1782 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11957(__this, method) (( void (*) (InternalEnumerator_1_t1782 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11957_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11958_gshared (InternalEnumerator_1_t1782 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11958(__this, method) (( bool (*) (InternalEnumerator_1_t1782 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11958_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern "C" Decimal_t347  InternalEnumerator_1_get_Current_m11959_gshared (InternalEnumerator_1_t1782 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11959(__this, method) (( Decimal_t347  (*) (InternalEnumerator_1_t1782 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11959_gshared)(__this, method)
