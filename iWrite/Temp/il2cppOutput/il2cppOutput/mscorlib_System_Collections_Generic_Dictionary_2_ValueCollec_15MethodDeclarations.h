﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Enumerator_t1589;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Dictionary_2_t1580;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m10057_gshared (Enumerator_t1589 * __this, Dictionary_2_t1580 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m10057(__this, ___host, method) (( void (*) (Enumerator_t1589 *, Dictionary_2_t1580 *, const MethodInfo*))Enumerator__ctor_m10057_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m10058_gshared (Enumerator_t1589 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m10058(__this, method) (( Object_t * (*) (Enumerator_t1589 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m10058_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m10059_gshared (Enumerator_t1589 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m10059(__this, method) (( void (*) (Enumerator_t1589 *, const MethodInfo*))Enumerator_Dispose_m10059_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m10060_gshared (Enumerator_t1589 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m10060(__this, method) (( bool (*) (Enumerator_t1589 *, const MethodInfo*))Enumerator_MoveNext_m10060_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m10061_gshared (Enumerator_t1589 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m10061(__this, method) (( Object_t * (*) (Enumerator_t1589 *, const MethodInfo*))Enumerator_get_Current_m10061_gshared)(__this, method)
