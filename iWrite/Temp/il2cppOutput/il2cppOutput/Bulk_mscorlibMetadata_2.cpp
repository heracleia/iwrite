﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// Metadata Definition System.Collections.IDictionary
extern TypeInfo IDictionary_t493_il2cpp_TypeInfo;
extern const Il2CppType IEnumerable_t302_0_0_0;
extern const Il2CppType ICollection_t569_0_0_0;
static const Il2CppType* IDictionary_t493_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_t569_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDictionary_t493_0_0_0;
extern const Il2CppType IDictionary_t493_1_0_0;
struct IDictionary_t493;
const Il2CppTypeDefinitionMetadata IDictionary_t493_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IDictionary_t493_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5250/* methodStart */
	, -1/* eventStart */
	, 995/* propertyStart */

};
TypeInfo IDictionary_t493_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDictionary"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IDictionary_t493_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 845/* custom_attributes_cache */
	, &IDictionary_t493_0_0_0/* byval_arg */
	, &IDictionary_t493_1_0_0/* this_arg */
	, &IDictionary_t493_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.IDictionaryEnumerator
extern TypeInfo IDictionaryEnumerator_t559_il2cpp_TypeInfo;
extern const Il2CppType IEnumerator_t279_0_0_0;
static const Il2CppType* IDictionaryEnumerator_t559_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDictionaryEnumerator_t559_0_0_0;
extern const Il2CppType IDictionaryEnumerator_t559_1_0_0;
struct IDictionaryEnumerator_t559;
const Il2CppTypeDefinitionMetadata IDictionaryEnumerator_t559_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IDictionaryEnumerator_t559_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5256/* methodStart */
	, -1/* eventStart */
	, 996/* propertyStart */

};
TypeInfo IDictionaryEnumerator_t559_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDictionaryEnumerator"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IDictionaryEnumerator_t559_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 846/* custom_attributes_cache */
	, &IDictionaryEnumerator_t559_0_0_0/* byval_arg */
	, &IDictionaryEnumerator_t559_1_0_0/* this_arg */
	, &IDictionaryEnumerator_t559_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.IEqualityComparer
extern TypeInfo IEqualityComparer_t397_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEqualityComparer_t397_0_0_0;
extern const Il2CppType IEqualityComparer_t397_1_0_0;
struct IEqualityComparer_t397;
const Il2CppTypeDefinitionMetadata IEqualityComparer_t397_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5259/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IEqualityComparer_t397_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEqualityComparer"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IEqualityComparer_t397_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 847/* custom_attributes_cache */
	, &IEqualityComparer_t397_0_0_0/* byval_arg */
	, &IEqualityComparer_t397_1_0_0/* this_arg */
	, &IEqualityComparer_t397_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.IHashCodeProvider
extern TypeInfo IHashCodeProvider_t396_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IHashCodeProvider_t396_0_0_0;
extern const Il2CppType IHashCodeProvider_t396_1_0_0;
struct IHashCodeProvider_t396;
const Il2CppTypeDefinitionMetadata IHashCodeProvider_t396_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5261/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IHashCodeProvider_t396_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IHashCodeProvider"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IHashCodeProvider_t396_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 848/* custom_attributes_cache */
	, &IHashCodeProvider_t396_0_0_0/* byval_arg */
	, &IHashCodeProvider_t396_1_0_0/* this_arg */
	, &IHashCodeProvider_t396_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.SortedList
#include "mscorlib_System_Collections_SortedList.h"
// Metadata Definition System.Collections.SortedList
extern TypeInfo SortedList_t579_il2cpp_TypeInfo;
// System.Collections.SortedList
#include "mscorlib_System_Collections_SortedListMethodDeclarations.h"
extern const Il2CppType Slot_t881_0_0_0;
extern const Il2CppType EnumeratorMode_t882_0_0_0;
extern const Il2CppType Enumerator_t883_0_0_0;
static const Il2CppType* SortedList_t579_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Slot_t881_0_0_0,
	&EnumeratorMode_t882_0_0_0,
	&Enumerator_t883_0_0_0,
};
static const EncodedMethodIndex SortedList_t579_VTable[31] = 
{
	120,
	125,
	122,
	123,
	1895,
	1896,
	1897,
	1898,
	1899,
	1900,
	1901,
	1902,
	1903,
	1904,
	1896,
	1897,
	1905,
	1906,
	1899,
	1900,
	1907,
	1908,
	1901,
	1902,
	1903,
	1904,
	1898,
	1909,
	1910,
	1911,
	1912,
};
extern const Il2CppType ICloneable_t2056_0_0_0;
static const Il2CppType* SortedList_t579_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICloneable_t2056_0_0_0,
	&ICollection_t569_0_0_0,
	&IDictionary_t493_0_0_0,
};
static Il2CppInterfaceOffsetPair SortedList_t579_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 5},
	{ &ICollection_t569_0_0_0, 5},
	{ &IDictionary_t493_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SortedList_t579_0_0_0;
extern const Il2CppType SortedList_t579_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SortedList_t579;
const Il2CppTypeDefinitionMetadata SortedList_t579_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SortedList_t579_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, SortedList_t579_InterfacesTypeInfos/* implementedInterfaces */
	, SortedList_t579_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SortedList_t579_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3046/* fieldStart */
	, 5262/* methodStart */
	, -1/* eventStart */
	, 999/* propertyStart */

};
TypeInfo SortedList_t579_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SortedList"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &SortedList_t579_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 849/* custom_attributes_cache */
	, &SortedList_t579_0_0_0/* byval_arg */
	, &SortedList_t579_1_0_0/* this_arg */
	, &SortedList_t579_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SortedList_t579)/* instance_size */
	, sizeof (SortedList_t579)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SortedList_t579_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 27/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 31/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"
// Metadata Definition System.Collections.SortedList/Slot
extern TypeInfo Slot_t881_il2cpp_TypeInfo;
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_SlotMethodDeclarations.h"
static const EncodedMethodIndex Slot_t881_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Slot_t881_1_0_0;
extern const Il2CppType ValueType_t757_0_0_0;
const Il2CppTypeDefinitionMetadata Slot_t881_DefinitionMetadata = 
{
	&SortedList_t579_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Slot_t881_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3052/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Slot_t881_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Slot"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Slot_t881_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Slot_t881_0_0_0/* byval_arg */
	, &Slot_t881_1_0_0/* this_arg */
	, &Slot_t881_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Slot_t881)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Slot_t881)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057037/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.SortedList/EnumeratorMode
#include "mscorlib_System_Collections_SortedList_EnumeratorMode.h"
// Metadata Definition System.Collections.SortedList/EnumeratorMode
extern TypeInfo EnumeratorMode_t882_il2cpp_TypeInfo;
// System.Collections.SortedList/EnumeratorMode
#include "mscorlib_System_Collections_SortedList_EnumeratorModeMethodDeclarations.h"
static const EncodedMethodIndex EnumeratorMode_t882_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
extern const Il2CppType IFormattable_t1406_0_0_0;
extern const Il2CppType IConvertible_t1409_0_0_0;
extern const Il2CppType IComparable_t1408_0_0_0;
static Il2CppInterfaceOffsetPair EnumeratorMode_t882_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnumeratorMode_t882_1_0_0;
extern const Il2CppType Enum_t305_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t320_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata EnumeratorMode_t882_DefinitionMetadata = 
{
	&SortedList_t579_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EnumeratorMode_t882_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, EnumeratorMode_t882_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3054/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EnumeratorMode_t882_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnumeratorMode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnumeratorMode_t882_0_0_0/* byval_arg */
	, &EnumeratorMode_t882_1_0_0/* this_arg */
	, &EnumeratorMode_t882_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnumeratorMode_t882)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EnumeratorMode_t882)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.SortedList/Enumerator
#include "mscorlib_System_Collections_SortedList_Enumerator.h"
// Metadata Definition System.Collections.SortedList/Enumerator
extern TypeInfo Enumerator_t883_il2cpp_TypeInfo;
// System.Collections.SortedList/Enumerator
#include "mscorlib_System_Collections_SortedList_EnumeratorMethodDeclarations.h"
static const EncodedMethodIndex Enumerator_t883_VTable[10] = 
{
	120,
	125,
	122,
	123,
	1913,
	1914,
	1915,
	1916,
	1917,
	1918,
};
static const Il2CppType* Enumerator_t883_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&ICloneable_t2056_0_0_0,
	&IDictionaryEnumerator_t559_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t883_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 6},
	{ &IDictionaryEnumerator_t559_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t883_1_0_0;
struct Enumerator_t883;
const Il2CppTypeDefinitionMetadata Enumerator_t883_DefinitionMetadata = 
{
	&SortedList_t579_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t883_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t883_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t883_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3058/* fieldStart */
	, 5289/* methodStart */
	, -1/* eventStart */
	, 1005/* propertyStart */

};
TypeInfo Enumerator_t883_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t883_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t883_0_0_0/* byval_arg */
	, &Enumerator_t883_1_0_0/* this_arg */
	, &Enumerator_t883_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t883)/* instance_size */
	, sizeof (Enumerator_t883)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Enumerator_t883_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.Stack
#include "mscorlib_System_Collections_Stack.h"
// Metadata Definition System.Collections.Stack
extern TypeInfo Stack_t271_il2cpp_TypeInfo;
// System.Collections.Stack
#include "mscorlib_System_Collections_StackMethodDeclarations.h"
extern const Il2CppType Enumerator_t885_0_0_0;
static const Il2CppType* Stack_t271_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t885_0_0_0,
};
static const EncodedMethodIndex Stack_t271_VTable[16] = 
{
	120,
	125,
	122,
	123,
	442,
	443,
	444,
	445,
	443,
	444,
	446,
	445,
	442,
	447,
	448,
	449,
};
static const Il2CppType* Stack_t271_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICloneable_t2056_0_0_0,
	&ICollection_t569_0_0_0,
};
static Il2CppInterfaceOffsetPair Stack_t271_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 5},
	{ &ICollection_t569_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Stack_t271_0_0_0;
extern const Il2CppType Stack_t271_1_0_0;
struct Stack_t271;
const Il2CppTypeDefinitionMetadata Stack_t271_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Stack_t271_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Stack_t271_InterfacesTypeInfos/* implementedInterfaces */
	, Stack_t271_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Stack_t271_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3067/* fieldStart */
	, 5297/* methodStart */
	, -1/* eventStart */
	, 1009/* propertyStart */

};
TypeInfo Stack_t271_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Stack"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &Stack_t271_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 850/* custom_attributes_cache */
	, &Stack_t271_0_0_0/* byval_arg */
	, &Stack_t271_1_0_0/* this_arg */
	, &Stack_t271_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Stack_t271)/* instance_size */
	, sizeof (Stack_t271)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 16/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.Stack/Enumerator
#include "mscorlib_System_Collections_Stack_Enumerator.h"
// Metadata Definition System.Collections.Stack/Enumerator
extern TypeInfo Enumerator_t885_il2cpp_TypeInfo;
// System.Collections.Stack/Enumerator
#include "mscorlib_System_Collections_Stack_EnumeratorMethodDeclarations.h"
static const EncodedMethodIndex Enumerator_t885_VTable[8] = 
{
	120,
	125,
	122,
	123,
	1919,
	1920,
	1919,
	1920,
};
static const Il2CppType* Enumerator_t885_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&ICloneable_t2056_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t885_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &ICloneable_t2056_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t885_1_0_0;
struct Enumerator_t885;
const Il2CppTypeDefinitionMetadata Enumerator_t885_DefinitionMetadata = 
{
	&Stack_t271_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t885_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t885_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t885_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3072/* fieldStart */
	, 5307/* methodStart */
	, -1/* eventStart */
	, 1011/* propertyStart */

};
TypeInfo Enumerator_t885_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t885_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t885_0_0_0/* byval_arg */
	, &Enumerator_t885_1_0_0/* this_arg */
	, &Enumerator_t885_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t885)/* instance_size */
	, sizeof (Enumerator_t885)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Configuration.Assemblies.AssemblyHashAlgorithm
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgorit.h"
// Metadata Definition System.Configuration.Assemblies.AssemblyHashAlgorithm
extern TypeInfo AssemblyHashAlgorithm_t886_il2cpp_TypeInfo;
// System.Configuration.Assemblies.AssemblyHashAlgorithm
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgoritMethodDeclarations.h"
static const EncodedMethodIndex AssemblyHashAlgorithm_t886_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair AssemblyHashAlgorithm_t886_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyHashAlgorithm_t886_0_0_0;
extern const Il2CppType AssemblyHashAlgorithm_t886_1_0_0;
const Il2CppTypeDefinitionMetadata AssemblyHashAlgorithm_t886_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyHashAlgorithm_t886_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, AssemblyHashAlgorithm_t886_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3075/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyHashAlgorithm_t886_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyHashAlgorithm"/* name */
	, "System.Configuration.Assemblies"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 851/* custom_attributes_cache */
	, &AssemblyHashAlgorithm_t886_0_0_0/* byval_arg */
	, &AssemblyHashAlgorithm_t886_1_0_0/* this_arg */
	, &AssemblyHashAlgorithm_t886_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyHashAlgorithm_t886)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AssemblyHashAlgorithm_t886)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Configuration.Assemblies.AssemblyVersionCompatibility
#include "mscorlib_System_Configuration_Assemblies_AssemblyVersionComp.h"
// Metadata Definition System.Configuration.Assemblies.AssemblyVersionCompatibility
extern TypeInfo AssemblyVersionCompatibility_t887_il2cpp_TypeInfo;
// System.Configuration.Assemblies.AssemblyVersionCompatibility
#include "mscorlib_System_Configuration_Assemblies_AssemblyVersionCompMethodDeclarations.h"
static const EncodedMethodIndex AssemblyVersionCompatibility_t887_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair AssemblyVersionCompatibility_t887_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyVersionCompatibility_t887_0_0_0;
extern const Il2CppType AssemblyVersionCompatibility_t887_1_0_0;
const Il2CppTypeDefinitionMetadata AssemblyVersionCompatibility_t887_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyVersionCompatibility_t887_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, AssemblyVersionCompatibility_t887_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3079/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyVersionCompatibility_t887_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyVersionCompatibility"/* name */
	, "System.Configuration.Assemblies"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 852/* custom_attributes_cache */
	, &AssemblyVersionCompatibility_t887_0_0_0/* byval_arg */
	, &AssemblyVersionCompatibility_t887_1_0_0/* this_arg */
	, &AssemblyVersionCompatibility_t887_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyVersionCompatibility_t887)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AssemblyVersionCompatibility_t887)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
#include "mscorlib_System_Diagnostics_CodeAnalysis_SuppressMessageAttr.h"
// Metadata Definition System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
extern TypeInfo SuppressMessageAttribute_t888_il2cpp_TypeInfo;
// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
#include "mscorlib_System_Diagnostics_CodeAnalysis_SuppressMessageAttrMethodDeclarations.h"
static const EncodedMethodIndex SuppressMessageAttribute_t888_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
extern const Il2CppType _Attribute_t2052_0_0_0;
static Il2CppInterfaceOffsetPair SuppressMessageAttribute_t888_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SuppressMessageAttribute_t888_0_0_0;
extern const Il2CppType SuppressMessageAttribute_t888_1_0_0;
extern const Il2CppType Attribute_t96_0_0_0;
struct SuppressMessageAttribute_t888;
const Il2CppTypeDefinitionMetadata SuppressMessageAttribute_t888_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SuppressMessageAttribute_t888_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, SuppressMessageAttribute_t888_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3083/* fieldStart */
	, 5310/* methodStart */
	, -1/* eventStart */
	, 1012/* propertyStart */

};
TypeInfo SuppressMessageAttribute_t888_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SuppressMessageAttribute"/* name */
	, "System.Diagnostics.CodeAnalysis"/* namespaze */
	, NULL/* methods */
	, &SuppressMessageAttribute_t888_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 853/* custom_attributes_cache */
	, &SuppressMessageAttribute_t888_0_0_0/* byval_arg */
	, &SuppressMessageAttribute_t888_1_0_0/* this_arg */
	, &SuppressMessageAttribute_t888_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SuppressMessageAttribute_t888)/* instance_size */
	, sizeof (SuppressMessageAttribute_t888)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// Metadata Definition System.Diagnostics.DebuggableAttribute
extern TypeInfo DebuggableAttribute_t890_il2cpp_TypeInfo;
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
extern const Il2CppType DebuggingModes_t889_0_0_0;
static const Il2CppType* DebuggableAttribute_t890_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DebuggingModes_t889_0_0_0,
};
static const EncodedMethodIndex DebuggableAttribute_t890_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair DebuggableAttribute_t890_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DebuggableAttribute_t890_0_0_0;
extern const Il2CppType DebuggableAttribute_t890_1_0_0;
struct DebuggableAttribute_t890;
const Il2CppTypeDefinitionMetadata DebuggableAttribute_t890_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DebuggableAttribute_t890_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebuggableAttribute_t890_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, DebuggableAttribute_t890_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3086/* fieldStart */
	, 5312/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DebuggableAttribute_t890_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebuggableAttribute"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &DebuggableAttribute_t890_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 854/* custom_attributes_cache */
	, &DebuggableAttribute_t890_0_0_0/* byval_arg */
	, &DebuggableAttribute_t890_1_0_0/* this_arg */
	, &DebuggableAttribute_t890_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebuggableAttribute_t890)/* instance_size */
	, sizeof (DebuggableAttribute_t890)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.DebuggableAttribute/DebuggingModes
#include "mscorlib_System_Diagnostics_DebuggableAttribute_DebuggingMod.h"
// Metadata Definition System.Diagnostics.DebuggableAttribute/DebuggingModes
extern TypeInfo DebuggingModes_t889_il2cpp_TypeInfo;
// System.Diagnostics.DebuggableAttribute/DebuggingModes
#include "mscorlib_System_Diagnostics_DebuggableAttribute_DebuggingModMethodDeclarations.h"
static const EncodedMethodIndex DebuggingModes_t889_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair DebuggingModes_t889_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DebuggingModes_t889_1_0_0;
const Il2CppTypeDefinitionMetadata DebuggingModes_t889_DefinitionMetadata = 
{
	&DebuggableAttribute_t890_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebuggingModes_t889_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, DebuggingModes_t889_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3089/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DebuggingModes_t889_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebuggingModes"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 855/* custom_attributes_cache */
	, &DebuggingModes_t889_0_0_0/* byval_arg */
	, &DebuggingModes_t889_1_0_0/* this_arg */
	, &DebuggingModes_t889_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebuggingModes_t889)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DebuggingModes_t889)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttribute.h"
// Metadata Definition System.Diagnostics.DebuggerDisplayAttribute
extern TypeInfo DebuggerDisplayAttribute_t891_il2cpp_TypeInfo;
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttributeMethodDeclarations.h"
static const EncodedMethodIndex DebuggerDisplayAttribute_t891_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair DebuggerDisplayAttribute_t891_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DebuggerDisplayAttribute_t891_0_0_0;
extern const Il2CppType DebuggerDisplayAttribute_t891_1_0_0;
struct DebuggerDisplayAttribute_t891;
const Il2CppTypeDefinitionMetadata DebuggerDisplayAttribute_t891_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebuggerDisplayAttribute_t891_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, DebuggerDisplayAttribute_t891_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3095/* fieldStart */
	, 5313/* methodStart */
	, -1/* eventStart */
	, 1013/* propertyStart */

};
TypeInfo DebuggerDisplayAttribute_t891_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebuggerDisplayAttribute"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &DebuggerDisplayAttribute_t891_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 856/* custom_attributes_cache */
	, &DebuggerDisplayAttribute_t891_0_0_0/* byval_arg */
	, &DebuggerDisplayAttribute_t891_1_0_0/* this_arg */
	, &DebuggerDisplayAttribute_t891_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebuggerDisplayAttribute_t891)/* instance_size */
	, sizeof (DebuggerDisplayAttribute_t891)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttribute.h"
// Metadata Definition System.Diagnostics.DebuggerStepThroughAttribute
extern TypeInfo DebuggerStepThroughAttribute_t892_il2cpp_TypeInfo;
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttributeMethodDeclarations.h"
static const EncodedMethodIndex DebuggerStepThroughAttribute_t892_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair DebuggerStepThroughAttribute_t892_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DebuggerStepThroughAttribute_t892_0_0_0;
extern const Il2CppType DebuggerStepThroughAttribute_t892_1_0_0;
struct DebuggerStepThroughAttribute_t892;
const Il2CppTypeDefinitionMetadata DebuggerStepThroughAttribute_t892_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebuggerStepThroughAttribute_t892_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, DebuggerStepThroughAttribute_t892_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5315/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DebuggerStepThroughAttribute_t892_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebuggerStepThroughAttribute"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &DebuggerStepThroughAttribute_t892_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 857/* custom_attributes_cache */
	, &DebuggerStepThroughAttribute_t892_0_0_0/* byval_arg */
	, &DebuggerStepThroughAttribute_t892_1_0_0/* this_arg */
	, &DebuggerStepThroughAttribute_t892_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebuggerStepThroughAttribute_t892)/* instance_size */
	, sizeof (DebuggerStepThroughAttribute_t892)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttribute.h"
// Metadata Definition System.Diagnostics.DebuggerTypeProxyAttribute
extern TypeInfo DebuggerTypeProxyAttribute_t893_il2cpp_TypeInfo;
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttributeMethodDeclarations.h"
static const EncodedMethodIndex DebuggerTypeProxyAttribute_t893_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair DebuggerTypeProxyAttribute_t893_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DebuggerTypeProxyAttribute_t893_0_0_0;
extern const Il2CppType DebuggerTypeProxyAttribute_t893_1_0_0;
struct DebuggerTypeProxyAttribute_t893;
const Il2CppTypeDefinitionMetadata DebuggerTypeProxyAttribute_t893_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebuggerTypeProxyAttribute_t893_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, DebuggerTypeProxyAttribute_t893_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3098/* fieldStart */
	, 5316/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DebuggerTypeProxyAttribute_t893_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebuggerTypeProxyAttribute"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &DebuggerTypeProxyAttribute_t893_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 858/* custom_attributes_cache */
	, &DebuggerTypeProxyAttribute_t893_0_0_0/* byval_arg */
	, &DebuggerTypeProxyAttribute_t893_1_0_0/* this_arg */
	, &DebuggerTypeProxyAttribute_t893_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebuggerTypeProxyAttribute_t893)/* instance_size */
	, sizeof (DebuggerTypeProxyAttribute_t893)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrame.h"
// Metadata Definition System.Diagnostics.StackFrame
extern TypeInfo StackFrame_t378_il2cpp_TypeInfo;
// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrameMethodDeclarations.h"
static const EncodedMethodIndex StackFrame_t378_VTable[9] = 
{
	120,
	125,
	122,
	1921,
	1922,
	1923,
	1924,
	1925,
	1926,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StackFrame_t378_0_0_0;
extern const Il2CppType StackFrame_t378_1_0_0;
struct StackFrame_t378;
const Il2CppTypeDefinitionMetadata StackFrame_t378_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StackFrame_t378_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3099/* fieldStart */
	, 5317/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StackFrame_t378_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackFrame"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &StackFrame_t378_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 859/* custom_attributes_cache */
	, &StackFrame_t378_0_0_0/* byval_arg */
	, &StackFrame_t378_1_0_0/* this_arg */
	, &StackFrame_t378_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackFrame_t378)/* instance_size */
	, sizeof (StackFrame_t378)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Diagnostics.StackTrace
#include "mscorlib_System_Diagnostics_StackTrace.h"
// Metadata Definition System.Diagnostics.StackTrace
extern TypeInfo StackTrace_t309_il2cpp_TypeInfo;
// System.Diagnostics.StackTrace
#include "mscorlib_System_Diagnostics_StackTraceMethodDeclarations.h"
static const EncodedMethodIndex StackTrace_t309_VTable[6] = 
{
	120,
	125,
	122,
	1927,
	1928,
	1929,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StackTrace_t309_0_0_0;
extern const Il2CppType StackTrace_t309_1_0_0;
struct StackTrace_t309;
const Il2CppTypeDefinitionMetadata StackTrace_t309_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StackTrace_t309_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3107/* fieldStart */
	, 5328/* methodStart */
	, -1/* eventStart */
	, 1014/* propertyStart */

};
TypeInfo StackTrace_t309_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackTrace"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &StackTrace_t309_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 860/* custom_attributes_cache */
	, &StackTrace_t309_0_0_0/* byval_arg */
	, &StackTrace_t309_1_0_0/* this_arg */
	, &StackTrace_t309_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackTrace_t309)/* instance_size */
	, sizeof (StackTrace_t309)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Globalization.Calendar
#include "mscorlib_System_Globalization_Calendar.h"
// Metadata Definition System.Globalization.Calendar
extern TypeInfo Calendar_t895_il2cpp_TypeInfo;
// System.Globalization.Calendar
#include "mscorlib_System_Globalization_CalendarMethodDeclarations.h"
static const EncodedMethodIndex Calendar_t895_VTable[10] = 
{
	120,
	125,
	122,
	123,
	0,
	0,
	0,
	0,
	0,
	0,
};
static const Il2CppType* Calendar_t895_InterfacesTypeInfos[] = 
{
	&ICloneable_t2056_0_0_0,
};
static Il2CppInterfaceOffsetPair Calendar_t895_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Calendar_t895_0_0_0;
extern const Il2CppType Calendar_t895_1_0_0;
struct Calendar_t895;
const Il2CppTypeDefinitionMetadata Calendar_t895_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Calendar_t895_InterfacesTypeInfos/* implementedInterfaces */
	, Calendar_t895_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Calendar_t895_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3110/* fieldStart */
	, 5338/* methodStart */
	, -1/* eventStart */
	, 1015/* propertyStart */

};
TypeInfo Calendar_t895_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Calendar"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Calendar_t895_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 861/* custom_attributes_cache */
	, &Calendar_t895_0_0_0/* byval_arg */
	, &Calendar_t895_1_0_0/* this_arg */
	, &Calendar_t895_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Calendar_t895)/* instance_size */
	, sizeof (Calendar_t895)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Globalization.CCMath
#include "mscorlib_System_Globalization_CCMath.h"
// Metadata Definition System.Globalization.CCMath
extern TypeInfo CCMath_t896_il2cpp_TypeInfo;
// System.Globalization.CCMath
#include "mscorlib_System_Globalization_CCMathMethodDeclarations.h"
static const EncodedMethodIndex CCMath_t896_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CCMath_t896_0_0_0;
extern const Il2CppType CCMath_t896_1_0_0;
struct CCMath_t896;
const Il2CppTypeDefinitionMetadata CCMath_t896_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CCMath_t896_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5347/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CCMath_t896_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CCMath"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &CCMath_t896_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CCMath_t896_0_0_0/* byval_arg */
	, &CCMath_t896_1_0_0/* this_arg */
	, &CCMath_t896_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CCMath_t896)/* instance_size */
	, sizeof (CCMath_t896)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Globalization.CCFixed
#include "mscorlib_System_Globalization_CCFixed.h"
// Metadata Definition System.Globalization.CCFixed
extern TypeInfo CCFixed_t897_il2cpp_TypeInfo;
// System.Globalization.CCFixed
#include "mscorlib_System_Globalization_CCFixedMethodDeclarations.h"
static const EncodedMethodIndex CCFixed_t897_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CCFixed_t897_0_0_0;
extern const Il2CppType CCFixed_t897_1_0_0;
struct CCFixed_t897;
const Il2CppTypeDefinitionMetadata CCFixed_t897_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CCFixed_t897_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5350/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CCFixed_t897_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CCFixed"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &CCFixed_t897_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CCFixed_t897_0_0_0/* byval_arg */
	, &CCFixed_t897_1_0_0/* this_arg */
	, &CCFixed_t897_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CCFixed_t897)/* instance_size */
	, sizeof (CCFixed_t897)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Globalization.CCGregorianCalendar
#include "mscorlib_System_Globalization_CCGregorianCalendar.h"
// Metadata Definition System.Globalization.CCGregorianCalendar
extern TypeInfo CCGregorianCalendar_t898_il2cpp_TypeInfo;
// System.Globalization.CCGregorianCalendar
#include "mscorlib_System_Globalization_CCGregorianCalendarMethodDeclarations.h"
static const EncodedMethodIndex CCGregorianCalendar_t898_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CCGregorianCalendar_t898_0_0_0;
extern const Il2CppType CCGregorianCalendar_t898_1_0_0;
struct CCGregorianCalendar_t898;
const Il2CppTypeDefinitionMetadata CCGregorianCalendar_t898_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CCGregorianCalendar_t898_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5352/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CCGregorianCalendar_t898_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CCGregorianCalendar"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &CCGregorianCalendar_t898_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CCGregorianCalendar_t898_0_0_0/* byval_arg */
	, &CCGregorianCalendar_t898_1_0_0/* this_arg */
	, &CCGregorianCalendar_t898_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CCGregorianCalendar_t898)/* instance_size */
	, sizeof (CCGregorianCalendar_t898)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Globalization.CompareInfo
#include "mscorlib_System_Globalization_CompareInfo.h"
// Metadata Definition System.Globalization.CompareInfo
extern TypeInfo CompareInfo_t753_il2cpp_TypeInfo;
// System.Globalization.CompareInfo
#include "mscorlib_System_Globalization_CompareInfoMethodDeclarations.h"
static const EncodedMethodIndex CompareInfo_t753_VTable[15] = 
{
	1930,
	1931,
	1932,
	1933,
	1934,
	1935,
	1936,
	1937,
	1938,
	1939,
	1940,
	1941,
	1942,
	1943,
	1944,
};
extern const Il2CppType IDeserializationCallback_t1429_0_0_0;
static const Il2CppType* CompareInfo_t753_InterfacesTypeInfos[] = 
{
	&IDeserializationCallback_t1429_0_0_0,
};
static Il2CppInterfaceOffsetPair CompareInfo_t753_InterfacesOffsets[] = 
{
	{ &IDeserializationCallback_t1429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompareInfo_t753_0_0_0;
extern const Il2CppType CompareInfo_t753_1_0_0;
struct CompareInfo_t753;
const Il2CppTypeDefinitionMetadata CompareInfo_t753_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CompareInfo_t753_InterfacesTypeInfos/* implementedInterfaces */
	, CompareInfo_t753_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CompareInfo_t753_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3114/* fieldStart */
	, 5362/* methodStart */
	, -1/* eventStart */
	, 1017/* propertyStart */

};
TypeInfo CompareInfo_t753_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompareInfo"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &CompareInfo_t753_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 862/* custom_attributes_cache */
	, &CompareInfo_t753_0_0_0/* byval_arg */
	, &CompareInfo_t753_1_0_0/* this_arg */
	, &CompareInfo_t753_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompareInfo_t753)/* instance_size */
	, sizeof (CompareInfo_t753)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CompareInfo_t753_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 29/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Globalization.CompareOptions
#include "mscorlib_System_Globalization_CompareOptions.h"
// Metadata Definition System.Globalization.CompareOptions
extern TypeInfo CompareOptions_t899_il2cpp_TypeInfo;
// System.Globalization.CompareOptions
#include "mscorlib_System_Globalization_CompareOptionsMethodDeclarations.h"
static const EncodedMethodIndex CompareOptions_t899_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair CompareOptions_t899_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompareOptions_t899_0_0_0;
extern const Il2CppType CompareOptions_t899_1_0_0;
const Il2CppTypeDefinitionMetadata CompareOptions_t899_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CompareOptions_t899_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, CompareOptions_t899_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3120/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CompareOptions_t899_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompareOptions"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 863/* custom_attributes_cache */
	, &CompareOptions_t899_0_0_0/* byval_arg */
	, &CompareOptions_t899_1_0_0/* this_arg */
	, &CompareOptions_t899_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompareOptions_t899)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CompareOptions_t899)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Globalization.CultureInfo
#include "mscorlib_System_Globalization_CultureInfo.h"
// Metadata Definition System.Globalization.CultureInfo
extern TypeInfo CultureInfo_t342_il2cpp_TypeInfo;
// System.Globalization.CultureInfo
#include "mscorlib_System_Globalization_CultureInfoMethodDeclarations.h"
static const EncodedMethodIndex CultureInfo_t342_VTable[14] = 
{
	1945,
	125,
	1946,
	1947,
	1948,
	1949,
	1950,
	1951,
	1952,
	1953,
	1954,
	1955,
	1956,
	1948,
};
extern const Il2CppType IFormatProvider_t1388_0_0_0;
static const Il2CppType* CultureInfo_t342_InterfacesTypeInfos[] = 
{
	&ICloneable_t2056_0_0_0,
	&IFormatProvider_t1388_0_0_0,
};
static Il2CppInterfaceOffsetPair CultureInfo_t342_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &IFormatProvider_t1388_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CultureInfo_t342_0_0_0;
extern const Il2CppType CultureInfo_t342_1_0_0;
struct CultureInfo_t342;
const Il2CppTypeDefinitionMetadata CultureInfo_t342_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CultureInfo_t342_InterfacesTypeInfos/* implementedInterfaces */
	, CultureInfo_t342_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CultureInfo_t342_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3130/* fieldStart */
	, 5391/* methodStart */
	, -1/* eventStart */
	, 1019/* propertyStart */

};
TypeInfo CultureInfo_t342_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CultureInfo"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &CultureInfo_t342_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 864/* custom_attributes_cache */
	, &CultureInfo_t342_0_0_0/* byval_arg */
	, &CultureInfo_t342_1_0_0/* this_arg */
	, &CultureInfo_t342_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CultureInfo_t342)/* instance_size */
	, sizeof (CultureInfo_t342)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CultureInfo_t342_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8193/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 38/* method_count */
	, 13/* property_count */
	, 40/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Globalization.DateTimeFormatFlags
#include "mscorlib_System_Globalization_DateTimeFormatFlags.h"
// Metadata Definition System.Globalization.DateTimeFormatFlags
extern TypeInfo DateTimeFormatFlags_t903_il2cpp_TypeInfo;
// System.Globalization.DateTimeFormatFlags
#include "mscorlib_System_Globalization_DateTimeFormatFlagsMethodDeclarations.h"
static const EncodedMethodIndex DateTimeFormatFlags_t903_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair DateTimeFormatFlags_t903_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTimeFormatFlags_t903_0_0_0;
extern const Il2CppType DateTimeFormatFlags_t903_1_0_0;
const Il2CppTypeDefinitionMetadata DateTimeFormatFlags_t903_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DateTimeFormatFlags_t903_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, DateTimeFormatFlags_t903_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3170/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DateTimeFormatFlags_t903_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTimeFormatFlags"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 867/* custom_attributes_cache */
	, &DateTimeFormatFlags_t903_0_0_0/* byval_arg */
	, &DateTimeFormatFlags_t903_1_0_0/* this_arg */
	, &DateTimeFormatFlags_t903_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTimeFormatFlags_t903)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DateTimeFormatFlags_t903)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Globalization.DateTimeFormatInfo
#include "mscorlib_System_Globalization_DateTimeFormatInfo.h"
// Metadata Definition System.Globalization.DateTimeFormatInfo
extern TypeInfo DateTimeFormatInfo_t901_il2cpp_TypeInfo;
// System.Globalization.DateTimeFormatInfo
#include "mscorlib_System_Globalization_DateTimeFormatInfoMethodDeclarations.h"
static const EncodedMethodIndex DateTimeFormatInfo_t901_VTable[6] = 
{
	120,
	125,
	122,
	123,
	1957,
	1958,
};
static const Il2CppType* DateTimeFormatInfo_t901_InterfacesTypeInfos[] = 
{
	&ICloneable_t2056_0_0_0,
	&IFormatProvider_t1388_0_0_0,
};
static Il2CppInterfaceOffsetPair DateTimeFormatInfo_t901_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &IFormatProvider_t1388_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTimeFormatInfo_t901_0_0_0;
extern const Il2CppType DateTimeFormatInfo_t901_1_0_0;
struct DateTimeFormatInfo_t901;
const Il2CppTypeDefinitionMetadata DateTimeFormatInfo_t901_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DateTimeFormatInfo_t901_InterfacesTypeInfos/* implementedInterfaces */
	, DateTimeFormatInfo_t901_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DateTimeFormatInfo_t901_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3176/* fieldStart */
	, 5429/* methodStart */
	, -1/* eventStart */
	, 1032/* propertyStart */

};
TypeInfo DateTimeFormatInfo_t901_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTimeFormatInfo"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &DateTimeFormatInfo_t901_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 868/* custom_attributes_cache */
	, &DateTimeFormatInfo_t901_0_0_0/* byval_arg */
	, &DateTimeFormatInfo_t901_1_0_0/* this_arg */
	, &DateTimeFormatInfo_t901_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTimeFormatInfo_t901)/* instance_size */
	, sizeof (DateTimeFormatInfo_t901)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DateTimeFormatInfo_t901_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 41/* method_count */
	, 23/* property_count */
	, 58/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Globalization.DateTimeStyles
#include "mscorlib_System_Globalization_DateTimeStyles.h"
// Metadata Definition System.Globalization.DateTimeStyles
extern TypeInfo DateTimeStyles_t904_il2cpp_TypeInfo;
// System.Globalization.DateTimeStyles
#include "mscorlib_System_Globalization_DateTimeStylesMethodDeclarations.h"
static const EncodedMethodIndex DateTimeStyles_t904_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair DateTimeStyles_t904_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTimeStyles_t904_0_0_0;
extern const Il2CppType DateTimeStyles_t904_1_0_0;
const Il2CppTypeDefinitionMetadata DateTimeStyles_t904_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DateTimeStyles_t904_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, DateTimeStyles_t904_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3234/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DateTimeStyles_t904_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTimeStyles"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 869/* custom_attributes_cache */
	, &DateTimeStyles_t904_0_0_0/* byval_arg */
	, &DateTimeStyles_t904_1_0_0/* this_arg */
	, &DateTimeStyles_t904_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTimeStyles_t904)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DateTimeStyles_t904)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Globalization.DaylightTime
#include "mscorlib_System_Globalization_DaylightTime.h"
// Metadata Definition System.Globalization.DaylightTime
extern TypeInfo DaylightTime_t905_il2cpp_TypeInfo;
// System.Globalization.DaylightTime
#include "mscorlib_System_Globalization_DaylightTimeMethodDeclarations.h"
static const EncodedMethodIndex DaylightTime_t905_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DaylightTime_t905_0_0_0;
extern const Il2CppType DaylightTime_t905_1_0_0;
struct DaylightTime_t905;
const Il2CppTypeDefinitionMetadata DaylightTime_t905_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DaylightTime_t905_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3245/* fieldStart */
	, 5470/* methodStart */
	, -1/* eventStart */
	, 1055/* propertyStart */

};
TypeInfo DaylightTime_t905_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DaylightTime"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &DaylightTime_t905_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 870/* custom_attributes_cache */
	, &DaylightTime_t905_0_0_0/* byval_arg */
	, &DaylightTime_t905_1_0_0/* this_arg */
	, &DaylightTime_t905_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DaylightTime_t905)/* instance_size */
	, sizeof (DaylightTime_t905)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Globalization.GregorianCalendar
#include "mscorlib_System_Globalization_GregorianCalendar.h"
// Metadata Definition System.Globalization.GregorianCalendar
extern TypeInfo GregorianCalendar_t906_il2cpp_TypeInfo;
// System.Globalization.GregorianCalendar
#include "mscorlib_System_Globalization_GregorianCalendarMethodDeclarations.h"
static const EncodedMethodIndex GregorianCalendar_t906_VTable[11] = 
{
	120,
	125,
	122,
	123,
	1959,
	1960,
	1961,
	1962,
	1963,
	1964,
	1965,
};
static Il2CppInterfaceOffsetPair GregorianCalendar_t906_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GregorianCalendar_t906_0_0_0;
extern const Il2CppType GregorianCalendar_t906_1_0_0;
struct GregorianCalendar_t906;
const Il2CppTypeDefinitionMetadata GregorianCalendar_t906_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GregorianCalendar_t906_InterfacesOffsets/* interfaceOffsets */
	, &Calendar_t895_0_0_0/* parent */
	, GregorianCalendar_t906_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3248/* fieldStart */
	, 5474/* methodStart */
	, -1/* eventStart */
	, 1058/* propertyStart */

};
TypeInfo GregorianCalendar_t906_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GregorianCalendar"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &GregorianCalendar_t906_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 871/* custom_attributes_cache */
	, &GregorianCalendar_t906_0_0_0/* byval_arg */
	, &GregorianCalendar_t906_1_0_0/* this_arg */
	, &GregorianCalendar_t906_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GregorianCalendar_t906)/* instance_size */
	, sizeof (GregorianCalendar_t906)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Globalization.GregorianCalendarTypes
#include "mscorlib_System_Globalization_GregorianCalendarTypes.h"
// Metadata Definition System.Globalization.GregorianCalendarTypes
extern TypeInfo GregorianCalendarTypes_t907_il2cpp_TypeInfo;
// System.Globalization.GregorianCalendarTypes
#include "mscorlib_System_Globalization_GregorianCalendarTypesMethodDeclarations.h"
static const EncodedMethodIndex GregorianCalendarTypes_t907_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair GregorianCalendarTypes_t907_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GregorianCalendarTypes_t907_0_0_0;
extern const Il2CppType GregorianCalendarTypes_t907_1_0_0;
const Il2CppTypeDefinitionMetadata GregorianCalendarTypes_t907_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GregorianCalendarTypes_t907_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, GregorianCalendarTypes_t907_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3249/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GregorianCalendarTypes_t907_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GregorianCalendarTypes"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 872/* custom_attributes_cache */
	, &GregorianCalendarTypes_t907_0_0_0/* byval_arg */
	, &GregorianCalendarTypes_t907_1_0_0/* this_arg */
	, &GregorianCalendarTypes_t907_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GregorianCalendarTypes_t907)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GregorianCalendarTypes_t907)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Globalization.NumberFormatInfo
#include "mscorlib_System_Globalization_NumberFormatInfo.h"
// Metadata Definition System.Globalization.NumberFormatInfo
extern TypeInfo NumberFormatInfo_t900_il2cpp_TypeInfo;
// System.Globalization.NumberFormatInfo
#include "mscorlib_System_Globalization_NumberFormatInfoMethodDeclarations.h"
static const EncodedMethodIndex NumberFormatInfo_t900_VTable[6] = 
{
	120,
	125,
	122,
	123,
	1966,
	1967,
};
static const Il2CppType* NumberFormatInfo_t900_InterfacesTypeInfos[] = 
{
	&ICloneable_t2056_0_0_0,
	&IFormatProvider_t1388_0_0_0,
};
static Il2CppInterfaceOffsetPair NumberFormatInfo_t900_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &IFormatProvider_t1388_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NumberFormatInfo_t900_0_0_0;
extern const Il2CppType NumberFormatInfo_t900_1_0_0;
struct NumberFormatInfo_t900;
const Il2CppTypeDefinitionMetadata NumberFormatInfo_t900_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NumberFormatInfo_t900_InterfacesTypeInfos/* implementedInterfaces */
	, NumberFormatInfo_t900_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NumberFormatInfo_t900_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3256/* fieldStart */
	, 5483/* methodStart */
	, -1/* eventStart */
	, 1060/* propertyStart */

};
TypeInfo NumberFormatInfo_t900_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NumberFormatInfo"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &NumberFormatInfo_t900_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 873/* custom_attributes_cache */
	, &NumberFormatInfo_t900_0_0_0/* byval_arg */
	, &NumberFormatInfo_t900_1_0_0/* this_arg */
	, &NumberFormatInfo_t900_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NumberFormatInfo_t900)/* instance_size */
	, sizeof (NumberFormatInfo_t900)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(NumberFormatInfo_t900_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 35/* method_count */
	, 27/* property_count */
	, 39/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Globalization.NumberStyles
#include "mscorlib_System_Globalization_NumberStyles.h"
// Metadata Definition System.Globalization.NumberStyles
extern TypeInfo NumberStyles_t908_il2cpp_TypeInfo;
// System.Globalization.NumberStyles
#include "mscorlib_System_Globalization_NumberStylesMethodDeclarations.h"
static const EncodedMethodIndex NumberStyles_t908_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair NumberStyles_t908_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NumberStyles_t908_0_0_0;
extern const Il2CppType NumberStyles_t908_1_0_0;
const Il2CppTypeDefinitionMetadata NumberStyles_t908_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NumberStyles_t908_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, NumberStyles_t908_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3295/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NumberStyles_t908_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NumberStyles"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 874/* custom_attributes_cache */
	, &NumberStyles_t908_0_0_0/* byval_arg */
	, &NumberStyles_t908_1_0_0/* this_arg */
	, &NumberStyles_t908_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NumberStyles_t908)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (NumberStyles_t908)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 18/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Globalization.TextInfo
#include "mscorlib_System_Globalization_TextInfo.h"
// Metadata Definition System.Globalization.TextInfo
extern TypeInfo TextInfo_t813_il2cpp_TypeInfo;
// System.Globalization.TextInfo
#include "mscorlib_System_Globalization_TextInfoMethodDeclarations.h"
extern const Il2CppType Data_t909_0_0_0;
static const Il2CppType* TextInfo_t813_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Data_t909_0_0_0,
};
static const EncodedMethodIndex TextInfo_t813_VTable[9] = 
{
	1968,
	125,
	1969,
	1970,
	1971,
	1972,
	1973,
	1974,
	1975,
};
static const Il2CppType* TextInfo_t813_InterfacesTypeInfos[] = 
{
	&ICloneable_t2056_0_0_0,
	&IDeserializationCallback_t1429_0_0_0,
};
static Il2CppInterfaceOffsetPair TextInfo_t813_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &IDeserializationCallback_t1429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TextInfo_t813_0_0_0;
extern const Il2CppType TextInfo_t813_1_0_0;
struct TextInfo_t813;
const Il2CppTypeDefinitionMetadata TextInfo_t813_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextInfo_t813_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, TextInfo_t813_InterfacesTypeInfos/* implementedInterfaces */
	, TextInfo_t813_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextInfo_t813_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3313/* fieldStart */
	, 5518/* methodStart */
	, -1/* eventStart */
	, 1087/* propertyStart */

};
TypeInfo TextInfo_t813_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextInfo"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &TextInfo_t813_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 875/* custom_attributes_cache */
	, &TextInfo_t813_0_0_0/* byval_arg */
	, &TextInfo_t813_1_0_0/* this_arg */
	, &TextInfo_t813_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextInfo_t813)/* instance_size */
	, sizeof (TextInfo_t813)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Globalization.TextInfo/Data
#include "mscorlib_System_Globalization_TextInfo_Data.h"
// Metadata Definition System.Globalization.TextInfo/Data
extern TypeInfo Data_t909_il2cpp_TypeInfo;
// System.Globalization.TextInfo/Data
#include "mscorlib_System_Globalization_TextInfo_DataMethodDeclarations.h"
static const EncodedMethodIndex Data_t909_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Data_t909_1_0_0;
const Il2CppTypeDefinitionMetadata Data_t909_DefinitionMetadata = 
{
	&TextInfo_t813_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Data_t909_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3319/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Data_t909_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Data"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Data_t909_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Data_t909_0_0_0/* byval_arg */
	, &Data_t909_1_0_0/* this_arg */
	, &Data_t909_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Data_t909)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Data_t909)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Data_t909 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategory.h"
// Metadata Definition System.Globalization.UnicodeCategory
extern TypeInfo UnicodeCategory_t910_il2cpp_TypeInfo;
// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategoryMethodDeclarations.h"
static const EncodedMethodIndex UnicodeCategory_t910_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair UnicodeCategory_t910_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnicodeCategory_t910_0_0_0;
extern const Il2CppType UnicodeCategory_t910_1_0_0;
const Il2CppTypeDefinitionMetadata UnicodeCategory_t910_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnicodeCategory_t910_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, UnicodeCategory_t910_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3324/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnicodeCategory_t910_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnicodeCategory"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 878/* custom_attributes_cache */
	, &UnicodeCategory_t910_0_0_0/* byval_arg */
	, &UnicodeCategory_t910_1_0_0/* this_arg */
	, &UnicodeCategory_t910_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnicodeCategory_t910)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnicodeCategory_t910)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 31/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.IsolatedStorage.IsolatedStorageException
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorageException.h"
// Metadata Definition System.IO.IsolatedStorage.IsolatedStorageException
extern TypeInfo IsolatedStorageException_t911_il2cpp_TypeInfo;
// System.IO.IsolatedStorage.IsolatedStorageException
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorageExceptionMethodDeclarations.h"
static const EncodedMethodIndex IsolatedStorageException_t911_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
extern const Il2CppType ISerializable_t1426_0_0_0;
extern const Il2CppType _Exception_t2066_0_0_0;
static Il2CppInterfaceOffsetPair IsolatedStorageException_t911_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IsolatedStorageException_t911_0_0_0;
extern const Il2CppType IsolatedStorageException_t911_1_0_0;
extern const Il2CppType Exception_t65_0_0_0;
struct IsolatedStorageException_t911;
const Il2CppTypeDefinitionMetadata IsolatedStorageException_t911_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IsolatedStorageException_t911_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t65_0_0_0/* parent */
	, IsolatedStorageException_t911_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5528/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IsolatedStorageException_t911_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IsolatedStorageException"/* name */
	, "System.IO.IsolatedStorage"/* namespaze */
	, NULL/* methods */
	, &IsolatedStorageException_t911_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 879/* custom_attributes_cache */
	, &IsolatedStorageException_t911_0_0_0/* byval_arg */
	, &IsolatedStorageException_t911_1_0_0/* this_arg */
	, &IsolatedStorageException_t911_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IsolatedStorageException_t911)/* instance_size */
	, sizeof (IsolatedStorageException_t911)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.BinaryReader
#include "mscorlib_System_IO_BinaryReader.h"
// Metadata Definition System.IO.BinaryReader
extern TypeInfo BinaryReader_t913_il2cpp_TypeInfo;
// System.IO.BinaryReader
#include "mscorlib_System_IO_BinaryReaderMethodDeclarations.h"
static const EncodedMethodIndex BinaryReader_t913_VTable[24] = 
{
	120,
	125,
	122,
	123,
	1976,
	1977,
	1978,
	1979,
	1980,
	1981,
	1982,
	1983,
	1984,
	1985,
	1986,
	1987,
	1988,
	1989,
	1990,
	1991,
	1992,
	1993,
	1994,
	1995,
};
extern const Il2CppType IDisposable_t319_0_0_0;
static const Il2CppType* BinaryReader_t913_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
};
static Il2CppInterfaceOffsetPair BinaryReader_t913_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryReader_t913_0_0_0;
extern const Il2CppType BinaryReader_t913_1_0_0;
struct BinaryReader_t913;
const Il2CppTypeDefinitionMetadata BinaryReader_t913_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BinaryReader_t913_InterfacesTypeInfos/* implementedInterfaces */
	, BinaryReader_t913_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryReader_t913_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3355/* fieldStart */
	, 5531/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BinaryReader_t913_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryReader"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &BinaryReader_t913_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 880/* custom_attributes_cache */
	, &BinaryReader_t913_0_0_0/* byval_arg */
	, &BinaryReader_t913_1_0_0/* this_arg */
	, &BinaryReader_t913_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryReader_t913)/* instance_size */
	, sizeof (BinaryReader_t913)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 25/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.Directory
#include "mscorlib_System_IO_Directory.h"
// Metadata Definition System.IO.Directory
extern TypeInfo Directory_t914_il2cpp_TypeInfo;
// System.IO.Directory
#include "mscorlib_System_IO_DirectoryMethodDeclarations.h"
static const EncodedMethodIndex Directory_t914_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Directory_t914_0_0_0;
extern const Il2CppType Directory_t914_1_0_0;
struct Directory_t914;
const Il2CppTypeDefinitionMetadata Directory_t914_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Directory_t914_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5556/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Directory_t914_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Directory"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Directory_t914_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 885/* custom_attributes_cache */
	, &Directory_t914_0_0_0/* byval_arg */
	, &Directory_t914_1_0_0/* this_arg */
	, &Directory_t914_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Directory_t914)/* instance_size */
	, sizeof (Directory_t914)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IO.DirectoryInfo
#include "mscorlib_System_IO_DirectoryInfo.h"
// Metadata Definition System.IO.DirectoryInfo
extern TypeInfo DirectoryInfo_t915_il2cpp_TypeInfo;
// System.IO.DirectoryInfo
#include "mscorlib_System_IO_DirectoryInfoMethodDeclarations.h"
static const EncodedMethodIndex DirectoryInfo_t915_VTable[9] = 
{
	120,
	125,
	122,
	1996,
	1997,
	1997,
	1998,
	1999,
	2000,
};
static Il2CppInterfaceOffsetPair DirectoryInfo_t915_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DirectoryInfo_t915_0_0_0;
extern const Il2CppType DirectoryInfo_t915_1_0_0;
extern const Il2CppType FileSystemInfo_t916_0_0_0;
struct DirectoryInfo_t915;
const Il2CppTypeDefinitionMetadata DirectoryInfo_t915_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DirectoryInfo_t915_InterfacesOffsets/* interfaceOffsets */
	, &FileSystemInfo_t916_0_0_0/* parent */
	, DirectoryInfo_t915_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3361/* fieldStart */
	, 5562/* methodStart */
	, -1/* eventStart */
	, 1088/* propertyStart */

};
TypeInfo DirectoryInfo_t915_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DirectoryInfo"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &DirectoryInfo_t915_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 886/* custom_attributes_cache */
	, &DirectoryInfo_t915_0_0_0/* byval_arg */
	, &DirectoryInfo_t915_1_0_0/* this_arg */
	, &DirectoryInfo_t915_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DirectoryInfo_t915)/* instance_size */
	, sizeof (DirectoryInfo_t915)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.DirectoryNotFoundException
#include "mscorlib_System_IO_DirectoryNotFoundException.h"
// Metadata Definition System.IO.DirectoryNotFoundException
extern TypeInfo DirectoryNotFoundException_t917_il2cpp_TypeInfo;
// System.IO.DirectoryNotFoundException
#include "mscorlib_System_IO_DirectoryNotFoundExceptionMethodDeclarations.h"
static const EncodedMethodIndex DirectoryNotFoundException_t917_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair DirectoryNotFoundException_t917_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DirectoryNotFoundException_t917_0_0_0;
extern const Il2CppType DirectoryNotFoundException_t917_1_0_0;
extern const Il2CppType IOException_t754_0_0_0;
struct DirectoryNotFoundException_t917;
const Il2CppTypeDefinitionMetadata DirectoryNotFoundException_t917_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DirectoryNotFoundException_t917_InterfacesOffsets/* interfaceOffsets */
	, &IOException_t754_0_0_0/* parent */
	, DirectoryNotFoundException_t917_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5570/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DirectoryNotFoundException_t917_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DirectoryNotFoundException"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &DirectoryNotFoundException_t917_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 887/* custom_attributes_cache */
	, &DirectoryNotFoundException_t917_0_0_0/* byval_arg */
	, &DirectoryNotFoundException_t917_1_0_0/* this_arg */
	, &DirectoryNotFoundException_t917_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DirectoryNotFoundException_t917)/* instance_size */
	, sizeof (DirectoryNotFoundException_t917)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.EndOfStreamException
#include "mscorlib_System_IO_EndOfStreamException.h"
// Metadata Definition System.IO.EndOfStreamException
extern TypeInfo EndOfStreamException_t918_il2cpp_TypeInfo;
// System.IO.EndOfStreamException
#include "mscorlib_System_IO_EndOfStreamExceptionMethodDeclarations.h"
static const EncodedMethodIndex EndOfStreamException_t918_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair EndOfStreamException_t918_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EndOfStreamException_t918_0_0_0;
extern const Il2CppType EndOfStreamException_t918_1_0_0;
struct EndOfStreamException_t918;
const Il2CppTypeDefinitionMetadata EndOfStreamException_t918_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EndOfStreamException_t918_InterfacesOffsets/* interfaceOffsets */
	, &IOException_t754_0_0_0/* parent */
	, EndOfStreamException_t918_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5573/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EndOfStreamException_t918_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EndOfStreamException"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &EndOfStreamException_t918_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 888/* custom_attributes_cache */
	, &EndOfStreamException_t918_0_0_0/* byval_arg */
	, &EndOfStreamException_t918_1_0_0/* this_arg */
	, &EndOfStreamException_t918_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EndOfStreamException_t918)/* instance_size */
	, sizeof (EndOfStreamException_t918)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.File
#include "mscorlib_System_IO_File.h"
// Metadata Definition System.IO.File
extern TypeInfo File_t919_il2cpp_TypeInfo;
// System.IO.File
#include "mscorlib_System_IO_FileMethodDeclarations.h"
static const EncodedMethodIndex File_t919_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType File_t919_0_0_0;
extern const Il2CppType File_t919_1_0_0;
struct File_t919;
const Il2CppTypeDefinitionMetadata File_t919_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, File_t919_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5575/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo File_t919_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "File"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &File_t919_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 889/* custom_attributes_cache */
	, &File_t919_0_0_0/* byval_arg */
	, &File_t919_1_0_0/* this_arg */
	, &File_t919_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (File_t919)/* instance_size */
	, sizeof (File_t919)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IO.FileAccess
#include "mscorlib_System_IO_FileAccess.h"
// Metadata Definition System.IO.FileAccess
extern TypeInfo FileAccess_t577_il2cpp_TypeInfo;
// System.IO.FileAccess
#include "mscorlib_System_IO_FileAccessMethodDeclarations.h"
static const EncodedMethodIndex FileAccess_t577_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair FileAccess_t577_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileAccess_t577_0_0_0;
extern const Il2CppType FileAccess_t577_1_0_0;
const Il2CppTypeDefinitionMetadata FileAccess_t577_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileAccess_t577_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, FileAccess_t577_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3363/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileAccess_t577_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileAccess"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 890/* custom_attributes_cache */
	, &FileAccess_t577_0_0_0/* byval_arg */
	, &FileAccess_t577_1_0_0/* this_arg */
	, &FileAccess_t577_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileAccess_t577)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FileAccess_t577)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributes.h"
// Metadata Definition System.IO.FileAttributes
extern TypeInfo FileAttributes_t920_il2cpp_TypeInfo;
// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributesMethodDeclarations.h"
static const EncodedMethodIndex FileAttributes_t920_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair FileAttributes_t920_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileAttributes_t920_0_0_0;
extern const Il2CppType FileAttributes_t920_1_0_0;
const Il2CppTypeDefinitionMetadata FileAttributes_t920_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileAttributes_t920_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, FileAttributes_t920_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3367/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileAttributes_t920_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileAttributes"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 891/* custom_attributes_cache */
	, &FileAttributes_t920_0_0_0/* byval_arg */
	, &FileAttributes_t920_1_0_0/* this_arg */
	, &FileAttributes_t920_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileAttributes_t920)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FileAttributes_t920)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.FileMode
#include "mscorlib_System_IO_FileMode.h"
// Metadata Definition System.IO.FileMode
extern TypeInfo FileMode_t921_il2cpp_TypeInfo;
// System.IO.FileMode
#include "mscorlib_System_IO_FileModeMethodDeclarations.h"
static const EncodedMethodIndex FileMode_t921_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair FileMode_t921_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileMode_t921_0_0_0;
extern const Il2CppType FileMode_t921_1_0_0;
const Il2CppTypeDefinitionMetadata FileMode_t921_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileMode_t921_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, FileMode_t921_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3382/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileMode_t921_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileMode"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 892/* custom_attributes_cache */
	, &FileMode_t921_0_0_0/* byval_arg */
	, &FileMode_t921_1_0_0/* this_arg */
	, &FileMode_t921_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileMode_t921)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FileMode_t921)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.FileNotFoundException
#include "mscorlib_System_IO_FileNotFoundException.h"
// Metadata Definition System.IO.FileNotFoundException
extern TypeInfo FileNotFoundException_t922_il2cpp_TypeInfo;
// System.IO.FileNotFoundException
#include "mscorlib_System_IO_FileNotFoundExceptionMethodDeclarations.h"
static const EncodedMethodIndex FileNotFoundException_t922_VTable[11] = 
{
	120,
	125,
	122,
	2001,
	2002,
	205,
	2003,
	207,
	208,
	2002,
	209,
};
static Il2CppInterfaceOffsetPair FileNotFoundException_t922_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileNotFoundException_t922_0_0_0;
extern const Il2CppType FileNotFoundException_t922_1_0_0;
struct FileNotFoundException_t922;
const Il2CppTypeDefinitionMetadata FileNotFoundException_t922_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileNotFoundException_t922_InterfacesOffsets/* interfaceOffsets */
	, &IOException_t754_0_0_0/* parent */
	, FileNotFoundException_t922_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3389/* fieldStart */
	, 5580/* methodStart */
	, -1/* eventStart */
	, 1090/* propertyStart */

};
TypeInfo FileNotFoundException_t922_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileNotFoundException"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &FileNotFoundException_t922_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 893/* custom_attributes_cache */
	, &FileNotFoundException_t922_0_0_0/* byval_arg */
	, &FileNotFoundException_t922_1_0_0/* this_arg */
	, &FileNotFoundException_t922_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileNotFoundException_t922)/* instance_size */
	, sizeof (FileNotFoundException_t922)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.FileOptions
#include "mscorlib_System_IO_FileOptions.h"
// Metadata Definition System.IO.FileOptions
extern TypeInfo FileOptions_t923_il2cpp_TypeInfo;
// System.IO.FileOptions
#include "mscorlib_System_IO_FileOptionsMethodDeclarations.h"
static const EncodedMethodIndex FileOptions_t923_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair FileOptions_t923_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileOptions_t923_0_0_0;
extern const Il2CppType FileOptions_t923_1_0_0;
const Il2CppTypeDefinitionMetadata FileOptions_t923_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileOptions_t923_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, FileOptions_t923_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3391/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileOptions_t923_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileOptions"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 894/* custom_attributes_cache */
	, &FileOptions_t923_0_0_0/* byval_arg */
	, &FileOptions_t923_1_0_0/* this_arg */
	, &FileOptions_t923_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileOptions_t923)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FileOptions_t923)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.FileShare
#include "mscorlib_System_IO_FileShare.h"
// Metadata Definition System.IO.FileShare
extern TypeInfo FileShare_t924_il2cpp_TypeInfo;
// System.IO.FileShare
#include "mscorlib_System_IO_FileShareMethodDeclarations.h"
static const EncodedMethodIndex FileShare_t924_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair FileShare_t924_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileShare_t924_0_0_0;
extern const Il2CppType FileShare_t924_1_0_0;
const Il2CppTypeDefinitionMetadata FileShare_t924_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileShare_t924_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, FileShare_t924_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3399/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileShare_t924_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileShare"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 895/* custom_attributes_cache */
	, &FileShare_t924_0_0_0/* byval_arg */
	, &FileShare_t924_1_0_0/* this_arg */
	, &FileShare_t924_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileShare_t924)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FileShare_t924)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.FileStream
#include "mscorlib_System_IO_FileStream.h"
// Metadata Definition System.IO.FileStream
extern TypeInfo FileStream_t747_il2cpp_TypeInfo;
// System.IO.FileStream
#include "mscorlib_System_IO_FileStreamMethodDeclarations.h"
extern const Il2CppType ReadDelegate_t925_0_0_0;
extern const Il2CppType WriteDelegate_t926_0_0_0;
static const Il2CppType* FileStream_t747_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ReadDelegate_t925_0_0_0,
	&WriteDelegate_t926_0_0_0,
};
static const EncodedMethodIndex FileStream_t747_VTable[24] = 
{
	120,
	2004,
	122,
	123,
	917,
	2005,
	2006,
	2007,
	2008,
	2009,
	2010,
	2011,
	983,
	2012,
	2013,
	2014,
	2015,
	2016,
	2017,
	2018,
	2019,
	2020,
	2021,
	2022,
};
static Il2CppInterfaceOffsetPair FileStream_t747_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileStream_t747_0_0_0;
extern const Il2CppType FileStream_t747_1_0_0;
extern const Il2CppType Stream_t692_0_0_0;
struct FileStream_t747;
const Il2CppTypeDefinitionMetadata FileStream_t747_DefinitionMetadata = 
{
	NULL/* declaringType */
	, FileStream_t747_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileStream_t747_InterfacesOffsets/* interfaceOffsets */
	, &Stream_t692_0_0_0/* parent */
	, FileStream_t747_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3406/* fieldStart */
	, 5586/* methodStart */
	, -1/* eventStart */
	, 1091/* propertyStart */

};
TypeInfo FileStream_t747_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileStream"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &FileStream_t747_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 896/* custom_attributes_cache */
	, &FileStream_t747_0_0_0/* byval_arg */
	, &FileStream_t747_1_0_0/* this_arg */
	, &FileStream_t747_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileStream_t747)/* instance_size */
	, sizeof (FileStream_t747)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 36/* method_count */
	, 5/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 24/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.FileStream/ReadDelegate
#include "mscorlib_System_IO_FileStream_ReadDelegate.h"
// Metadata Definition System.IO.FileStream/ReadDelegate
extern TypeInfo ReadDelegate_t925_il2cpp_TypeInfo;
// System.IO.FileStream/ReadDelegate
#include "mscorlib_System_IO_FileStream_ReadDelegateMethodDeclarations.h"
static const EncodedMethodIndex ReadDelegate_t925_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	2023,
	2024,
	2025,
};
static Il2CppInterfaceOffsetPair ReadDelegate_t925_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReadDelegate_t925_1_0_0;
extern const Il2CppType MulticastDelegate_t47_0_0_0;
struct ReadDelegate_t925;
const Il2CppTypeDefinitionMetadata ReadDelegate_t925_DefinitionMetadata = 
{
	&FileStream_t747_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReadDelegate_t925_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, ReadDelegate_t925_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5622/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReadDelegate_t925_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReadDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ReadDelegate_t925_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReadDelegate_t925_0_0_0/* byval_arg */
	, &ReadDelegate_t925_1_0_0/* this_arg */
	, &ReadDelegate_t925_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ReadDelegate_t925/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReadDelegate_t925)/* instance_size */
	, sizeof (ReadDelegate_t925)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.FileStream/WriteDelegate
#include "mscorlib_System_IO_FileStream_WriteDelegate.h"
// Metadata Definition System.IO.FileStream/WriteDelegate
extern TypeInfo WriteDelegate_t926_il2cpp_TypeInfo;
// System.IO.FileStream/WriteDelegate
#include "mscorlib_System_IO_FileStream_WriteDelegateMethodDeclarations.h"
static const EncodedMethodIndex WriteDelegate_t926_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	2026,
	2027,
	2028,
};
static Il2CppInterfaceOffsetPair WriteDelegate_t926_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WriteDelegate_t926_1_0_0;
struct WriteDelegate_t926;
const Il2CppTypeDefinitionMetadata WriteDelegate_t926_DefinitionMetadata = 
{
	&FileStream_t747_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WriteDelegate_t926_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, WriteDelegate_t926_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5626/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WriteDelegate_t926_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WriteDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &WriteDelegate_t926_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WriteDelegate_t926_0_0_0/* byval_arg */
	, &WriteDelegate_t926_1_0_0/* this_arg */
	, &WriteDelegate_t926_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_WriteDelegate_t926/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WriteDelegate_t926)/* instance_size */
	, sizeof (WriteDelegate_t926)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.FileStreamAsyncResult
#include "mscorlib_System_IO_FileStreamAsyncResult.h"
// Metadata Definition System.IO.FileStreamAsyncResult
extern TypeInfo FileStreamAsyncResult_t927_il2cpp_TypeInfo;
// System.IO.FileStreamAsyncResult
#include "mscorlib_System_IO_FileStreamAsyncResultMethodDeclarations.h"
static const EncodedMethodIndex FileStreamAsyncResult_t927_VTable[7] = 
{
	120,
	125,
	122,
	123,
	2029,
	2030,
	2031,
};
extern const Il2CppType IAsyncResult_t44_0_0_0;
static const Il2CppType* FileStreamAsyncResult_t927_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t44_0_0_0,
};
static Il2CppInterfaceOffsetPair FileStreamAsyncResult_t927_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t44_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileStreamAsyncResult_t927_0_0_0;
extern const Il2CppType FileStreamAsyncResult_t927_1_0_0;
struct FileStreamAsyncResult_t927;
const Il2CppTypeDefinitionMetadata FileStreamAsyncResult_t927_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FileStreamAsyncResult_t927_InterfacesTypeInfos/* implementedInterfaces */
	, FileStreamAsyncResult_t927_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FileStreamAsyncResult_t927_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3420/* fieldStart */
	, 5630/* methodStart */
	, -1/* eventStart */
	, 1096/* propertyStart */

};
TypeInfo FileStreamAsyncResult_t927_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileStreamAsyncResult"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &FileStreamAsyncResult_t927_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FileStreamAsyncResult_t927_0_0_0/* byval_arg */
	, &FileStreamAsyncResult_t927_1_0_0/* this_arg */
	, &FileStreamAsyncResult_t927_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileStreamAsyncResult_t927)/* instance_size */
	, sizeof (FileStreamAsyncResult_t927)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.FileSystemInfo
#include "mscorlib_System_IO_FileSystemInfo.h"
// Metadata Definition System.IO.FileSystemInfo
extern TypeInfo FileSystemInfo_t916_il2cpp_TypeInfo;
// System.IO.FileSystemInfo
#include "mscorlib_System_IO_FileSystemInfoMethodDeclarations.h"
static const EncodedMethodIndex FileSystemInfo_t916_VTable[9] = 
{
	120,
	125,
	122,
	123,
	1997,
	1997,
	0,
	1999,
	2000,
};
static const Il2CppType* FileSystemInfo_t916_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair FileSystemInfo_t916_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileSystemInfo_t916_1_0_0;
extern const Il2CppType MarshalByRefObject_t434_0_0_0;
struct FileSystemInfo_t916;
const Il2CppTypeDefinitionMetadata FileSystemInfo_t916_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FileSystemInfo_t916_InterfacesTypeInfos/* implementedInterfaces */
	, FileSystemInfo_t916_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t434_0_0_0/* parent */
	, FileSystemInfo_t916_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3428/* fieldStart */
	, 5635/* methodStart */
	, -1/* eventStart */
	, 1099/* propertyStart */

};
TypeInfo FileSystemInfo_t916_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileSystemInfo"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &FileSystemInfo_t916_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 897/* custom_attributes_cache */
	, &FileSystemInfo_t916_0_0_0/* byval_arg */
	, &FileSystemInfo_t916_1_0_0/* this_arg */
	, &FileSystemInfo_t916_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileSystemInfo_t916)/* instance_size */
	, sizeof (FileSystemInfo_t916)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.IOException
#include "mscorlib_System_IO_IOException.h"
// Metadata Definition System.IO.IOException
extern TypeInfo IOException_t754_il2cpp_TypeInfo;
// System.IO.IOException
#include "mscorlib_System_IO_IOExceptionMethodDeclarations.h"
static const EncodedMethodIndex IOException_t754_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair IOException_t754_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IOException_t754_1_0_0;
extern const Il2CppType SystemException_t597_0_0_0;
struct IOException_t754;
const Il2CppTypeDefinitionMetadata IOException_t754_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IOException_t754_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, IOException_t754_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5643/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IOException_t754_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IOException"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &IOException_t754_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 899/* custom_attributes_cache */
	, &IOException_t754_0_0_0/* byval_arg */
	, &IOException_t754_1_0_0/* this_arg */
	, &IOException_t754_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IOException_t754)/* instance_size */
	, sizeof (IOException_t754)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.MemoryStream
#include "mscorlib_System_IO_MemoryStream.h"
// Metadata Definition System.IO.MemoryStream
extern TypeInfo MemoryStream_t331_il2cpp_TypeInfo;
// System.IO.MemoryStream
#include "mscorlib_System_IO_MemoryStreamMethodDeclarations.h"
static const EncodedMethodIndex MemoryStream_t331_VTable[26] = 
{
	120,
	125,
	122,
	123,
	917,
	2032,
	2033,
	2034,
	2035,
	2036,
	2037,
	2038,
	983,
	2039,
	2040,
	2041,
	2042,
	2043,
	2044,
	2045,
	989,
	990,
	991,
	992,
	2046,
	2047,
};
static Il2CppInterfaceOffsetPair MemoryStream_t331_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemoryStream_t331_0_0_0;
extern const Il2CppType MemoryStream_t331_1_0_0;
struct MemoryStream_t331;
const Il2CppTypeDefinitionMetadata MemoryStream_t331_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemoryStream_t331_InterfacesOffsets/* interfaceOffsets */
	, &Stream_t692_0_0_0/* parent */
	, MemoryStream_t331_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3432/* fieldStart */
	, 5648/* methodStart */
	, -1/* eventStart */
	, 1101/* propertyStart */

};
TypeInfo MemoryStream_t331_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemoryStream"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &MemoryStream_t331_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 900/* custom_attributes_cache */
	, &MemoryStream_t331_0_0_0/* byval_arg */
	, &MemoryStream_t331_1_0_0/* this_arg */
	, &MemoryStream_t331_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemoryStream_t331)/* instance_size */
	, sizeof (MemoryStream_t331)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 23/* method_count */
	, 6/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileType.h"
// Metadata Definition System.IO.MonoFileType
extern TypeInfo MonoFileType_t929_il2cpp_TypeInfo;
// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileTypeMethodDeclarations.h"
static const EncodedMethodIndex MonoFileType_t929_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair MonoFileType_t929_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoFileType_t929_0_0_0;
extern const Il2CppType MonoFileType_t929_1_0_0;
const Il2CppTypeDefinitionMetadata MonoFileType_t929_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoFileType_t929_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, MonoFileType_t929_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3442/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoFileType_t929_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoFileType"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoFileType_t929_0_0_0/* byval_arg */
	, &MonoFileType_t929_1_0_0/* this_arg */
	, &MonoFileType_t929_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoFileType_t929)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoFileType_t929)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.MonoIO
#include "mscorlib_System_IO_MonoIO.h"
// Metadata Definition System.IO.MonoIO
extern TypeInfo MonoIO_t930_il2cpp_TypeInfo;
// System.IO.MonoIO
#include "mscorlib_System_IO_MonoIOMethodDeclarations.h"
static const EncodedMethodIndex MonoIO_t930_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoIO_t930_0_0_0;
extern const Il2CppType MonoIO_t930_1_0_0;
struct MonoIO_t930;
const Il2CppTypeDefinitionMetadata MonoIO_t930_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoIO_t930_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3448/* fieldStart */
	, 5671/* methodStart */
	, -1/* eventStart */
	, 1107/* propertyStart */

};
TypeInfo MonoIO_t930_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoIO"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &MonoIO_t930_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoIO_t930_0_0_0/* byval_arg */
	, &MonoIO_t930_1_0_0/* this_arg */
	, &MonoIO_t930_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoIO_t930)/* instance_size */
	, sizeof (MonoIO_t930)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoIO_t930_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 7/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOError.h"
// Metadata Definition System.IO.MonoIOError
extern TypeInfo MonoIOError_t931_il2cpp_TypeInfo;
// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOErrorMethodDeclarations.h"
static const EncodedMethodIndex MonoIOError_t931_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair MonoIOError_t931_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoIOError_t931_0_0_0;
extern const Il2CppType MonoIOError_t931_1_0_0;
const Il2CppTypeDefinitionMetadata MonoIOError_t931_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoIOError_t931_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, MonoIOError_t931_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3450/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoIOError_t931_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoIOError"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoIOError_t931_0_0_0/* byval_arg */
	, &MonoIOError_t931_1_0_0/* this_arg */
	, &MonoIOError_t931_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoIOError_t931)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoIOError_t931)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 25/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.MonoIOStat
#include "mscorlib_System_IO_MonoIOStat.h"
// Metadata Definition System.IO.MonoIOStat
extern TypeInfo MonoIOStat_t928_il2cpp_TypeInfo;
// System.IO.MonoIOStat
#include "mscorlib_System_IO_MonoIOStatMethodDeclarations.h"
static const EncodedMethodIndex MonoIOStat_t928_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoIOStat_t928_0_0_0;
extern const Il2CppType MonoIOStat_t928_1_0_0;
const Il2CppTypeDefinitionMetadata MonoIOStat_t928_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, MonoIOStat_t928_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3475/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoIOStat_t928_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoIOStat"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &MonoIOStat_t928_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoIOStat_t928_0_0_0/* byval_arg */
	, &MonoIOStat_t928_1_0_0/* this_arg */
	, &MonoIOStat_t928_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)MonoIOStat_t928_marshal/* marshal_to_native_func */
	, (methodPointerType)MonoIOStat_t928_marshal_back/* marshal_from_native_func */
	, (methodPointerType)MonoIOStat_t928_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (MonoIOStat_t928)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoIOStat_t928)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(MonoIOStat_t928_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IO.Path
#include "mscorlib_System_IO_Path.h"
// Metadata Definition System.IO.Path
extern TypeInfo Path_t600_il2cpp_TypeInfo;
// System.IO.Path
#include "mscorlib_System_IO_PathMethodDeclarations.h"
static const EncodedMethodIndex Path_t600_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Path_t600_0_0_0;
extern const Il2CppType Path_t600_1_0_0;
struct Path_t600;
const Il2CppTypeDefinitionMetadata Path_t600_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Path_t600_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3481/* fieldStart */
	, 5697/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Path_t600_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Path"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Path_t600_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 901/* custom_attributes_cache */
	, &Path_t600_0_0_0/* byval_arg */
	, &Path_t600_1_0_0/* this_arg */
	, &Path_t600_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Path_t600)/* instance_size */
	, sizeof (Path_t600)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Path_t600_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 385/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IO.PathTooLongException
#include "mscorlib_System_IO_PathTooLongException.h"
// Metadata Definition System.IO.PathTooLongException
extern TypeInfo PathTooLongException_t932_il2cpp_TypeInfo;
// System.IO.PathTooLongException
#include "mscorlib_System_IO_PathTooLongExceptionMethodDeclarations.h"
static const EncodedMethodIndex PathTooLongException_t932_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
static Il2CppInterfaceOffsetPair PathTooLongException_t932_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PathTooLongException_t932_0_0_0;
extern const Il2CppType PathTooLongException_t932_1_0_0;
struct PathTooLongException_t932;
const Il2CppTypeDefinitionMetadata PathTooLongException_t932_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PathTooLongException_t932_InterfacesOffsets/* interfaceOffsets */
	, &IOException_t754_0_0_0/* parent */
	, PathTooLongException_t932_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5712/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PathTooLongException_t932_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PathTooLongException"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &PathTooLongException_t932_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 903/* custom_attributes_cache */
	, &PathTooLongException_t932_0_0_0/* byval_arg */
	, &PathTooLongException_t932_1_0_0/* this_arg */
	, &PathTooLongException_t932_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PathTooLongException_t932)/* instance_size */
	, sizeof (PathTooLongException_t932)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.SearchPattern
#include "mscorlib_System_IO_SearchPattern.h"
// Metadata Definition System.IO.SearchPattern
extern TypeInfo SearchPattern_t933_il2cpp_TypeInfo;
// System.IO.SearchPattern
#include "mscorlib_System_IO_SearchPatternMethodDeclarations.h"
static const EncodedMethodIndex SearchPattern_t933_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SearchPattern_t933_0_0_0;
extern const Il2CppType SearchPattern_t933_1_0_0;
struct SearchPattern_t933;
const Il2CppTypeDefinitionMetadata SearchPattern_t933_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SearchPattern_t933_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3489/* fieldStart */
	, 5715/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SearchPattern_t933_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SearchPattern"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &SearchPattern_t933_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SearchPattern_t933_0_0_0/* byval_arg */
	, &SearchPattern_t933_1_0_0/* this_arg */
	, &SearchPattern_t933_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SearchPattern_t933)/* instance_size */
	, sizeof (SearchPattern_t933)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SearchPattern_t933_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IO.SeekOrigin
#include "mscorlib_System_IO_SeekOrigin.h"
// Metadata Definition System.IO.SeekOrigin
extern TypeInfo SeekOrigin_t934_il2cpp_TypeInfo;
// System.IO.SeekOrigin
#include "mscorlib_System_IO_SeekOriginMethodDeclarations.h"
static const EncodedMethodIndex SeekOrigin_t934_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair SeekOrigin_t934_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SeekOrigin_t934_0_0_0;
extern const Il2CppType SeekOrigin_t934_1_0_0;
const Il2CppTypeDefinitionMetadata SeekOrigin_t934_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SeekOrigin_t934_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, SeekOrigin_t934_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3491/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SeekOrigin_t934_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SeekOrigin"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 904/* custom_attributes_cache */
	, &SeekOrigin_t934_0_0_0/* byval_arg */
	, &SeekOrigin_t934_1_0_0/* this_arg */
	, &SeekOrigin_t934_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SeekOrigin_t934)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SeekOrigin_t934)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.Stream
#include "mscorlib_System_IO_Stream.h"
// Metadata Definition System.IO.Stream
extern TypeInfo Stream_t692_il2cpp_TypeInfo;
// System.IO.Stream
#include "mscorlib_System_IO_StreamMethodDeclarations.h"
static const EncodedMethodIndex Stream_t692_VTable[24] = 
{
	120,
	125,
	122,
	123,
	917,
	0,
	0,
	0,
	0,
	0,
	0,
	982,
	983,
	0,
	0,
	928,
	0,
	0,
	0,
	932,
	989,
	990,
	991,
	992,
};
static const Il2CppType* Stream_t692_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
};
static Il2CppInterfaceOffsetPair Stream_t692_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Stream_t692_1_0_0;
struct Stream_t692;
const Il2CppTypeDefinitionMetadata Stream_t692_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Stream_t692_InterfacesTypeInfos/* implementedInterfaces */
	, Stream_t692_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Stream_t692_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3495/* fieldStart */
	, 5716/* methodStart */
	, -1/* eventStart */
	, 1114/* propertyStart */

};
TypeInfo Stream_t692_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Stream"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Stream_t692_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 905/* custom_attributes_cache */
	, &Stream_t692_0_0_0/* byval_arg */
	, &Stream_t692_1_0_0/* this_arg */
	, &Stream_t692_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Stream_t692)/* instance_size */
	, sizeof (Stream_t692)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Stream_t692_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 22/* method_count */
	, 5/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.NullStream
#include "mscorlib_System_IO_NullStream.h"
// Metadata Definition System.IO.NullStream
extern TypeInfo NullStream_t935_il2cpp_TypeInfo;
// System.IO.NullStream
#include "mscorlib_System_IO_NullStreamMethodDeclarations.h"
static const EncodedMethodIndex NullStream_t935_VTable[24] = 
{
	120,
	125,
	122,
	123,
	917,
	2048,
	2049,
	2050,
	2051,
	2052,
	2053,
	982,
	983,
	2054,
	2055,
	2056,
	2057,
	2058,
	2059,
	2060,
	989,
	990,
	991,
	992,
};
static Il2CppInterfaceOffsetPair NullStream_t935_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullStream_t935_0_0_0;
extern const Il2CppType NullStream_t935_1_0_0;
struct NullStream_t935;
const Il2CppTypeDefinitionMetadata NullStream_t935_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullStream_t935_InterfacesOffsets/* interfaceOffsets */
	, &Stream_t692_0_0_0/* parent */
	, NullStream_t935_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5738/* methodStart */
	, -1/* eventStart */
	, 1119/* propertyStart */

};
TypeInfo NullStream_t935_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullStream"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &NullStream_t935_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NullStream_t935_0_0_0/* byval_arg */
	, &NullStream_t935_1_0_0/* this_arg */
	, &NullStream_t935_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullStream_t935)/* instance_size */
	, sizeof (NullStream_t935)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.StreamAsyncResult
#include "mscorlib_System_IO_StreamAsyncResult.h"
// Metadata Definition System.IO.StreamAsyncResult
extern TypeInfo StreamAsyncResult_t936_il2cpp_TypeInfo;
// System.IO.StreamAsyncResult
#include "mscorlib_System_IO_StreamAsyncResultMethodDeclarations.h"
static const EncodedMethodIndex StreamAsyncResult_t936_VTable[7] = 
{
	120,
	125,
	122,
	123,
	2061,
	2062,
	2063,
};
static const Il2CppType* StreamAsyncResult_t936_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t44_0_0_0,
};
static Il2CppInterfaceOffsetPair StreamAsyncResult_t936_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t44_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StreamAsyncResult_t936_0_0_0;
extern const Il2CppType StreamAsyncResult_t936_1_0_0;
struct StreamAsyncResult_t936;
const Il2CppTypeDefinitionMetadata StreamAsyncResult_t936_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StreamAsyncResult_t936_InterfacesTypeInfos/* implementedInterfaces */
	, StreamAsyncResult_t936_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StreamAsyncResult_t936_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3496/* fieldStart */
	, 5752/* methodStart */
	, -1/* eventStart */
	, 1124/* propertyStart */

};
TypeInfo StreamAsyncResult_t936_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StreamAsyncResult"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &StreamAsyncResult_t936_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StreamAsyncResult_t936_0_0_0/* byval_arg */
	, &StreamAsyncResult_t936_1_0_0/* this_arg */
	, &StreamAsyncResult_t936_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StreamAsyncResult_t936)/* instance_size */
	, sizeof (StreamAsyncResult_t936)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.StreamReader
#include "mscorlib_System_IO_StreamReader.h"
// Metadata Definition System.IO.StreamReader
extern TypeInfo StreamReader_t938_il2cpp_TypeInfo;
// System.IO.StreamReader
#include "mscorlib_System_IO_StreamReaderMethodDeclarations.h"
extern const Il2CppType NullStreamReader_t937_0_0_0;
static const Il2CppType* StreamReader_t938_il2cpp_TypeInfo__nestedTypes[1] =
{
	&NullStreamReader_t937_0_0_0,
};
static const EncodedMethodIndex StreamReader_t938_VTable[11] = 
{
	120,
	125,
	122,
	123,
	2064,
	2065,
	2066,
	2067,
	2068,
	2069,
	2070,
};
static Il2CppInterfaceOffsetPair StreamReader_t938_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StreamReader_t938_0_0_0;
extern const Il2CppType StreamReader_t938_1_0_0;
extern const Il2CppType TextReader_t861_0_0_0;
struct StreamReader_t938;
const Il2CppTypeDefinitionMetadata StreamReader_t938_DefinitionMetadata = 
{
	NULL/* declaringType */
	, StreamReader_t938_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StreamReader_t938_InterfacesOffsets/* interfaceOffsets */
	, &TextReader_t861_0_0_0/* parent */
	, StreamReader_t938_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3502/* fieldStart */
	, 5762/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StreamReader_t938_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StreamReader"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &StreamReader_t938_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 906/* custom_attributes_cache */
	, &StreamReader_t938_0_0_0/* byval_arg */
	, &StreamReader_t938_1_0_0/* this_arg */
	, &StreamReader_t938_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StreamReader_t938)/* instance_size */
	, sizeof (StreamReader_t938)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StreamReader_t938_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.StreamReader/NullStreamReader
#include "mscorlib_System_IO_StreamReader_NullStreamReader.h"
// Metadata Definition System.IO.StreamReader/NullStreamReader
extern TypeInfo NullStreamReader_t937_il2cpp_TypeInfo;
// System.IO.StreamReader/NullStreamReader
#include "mscorlib_System_IO_StreamReader_NullStreamReaderMethodDeclarations.h"
static const EncodedMethodIndex NullStreamReader_t937_VTable[11] = 
{
	120,
	125,
	122,
	123,
	2064,
	2065,
	2071,
	2072,
	2073,
	2074,
	2075,
};
static Il2CppInterfaceOffsetPair NullStreamReader_t937_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullStreamReader_t937_1_0_0;
struct NullStreamReader_t937;
const Il2CppTypeDefinitionMetadata NullStreamReader_t937_DefinitionMetadata = 
{
	&StreamReader_t938_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullStreamReader_t937_InterfacesOffsets/* interfaceOffsets */
	, &StreamReader_t938_0_0_0/* parent */
	, NullStreamReader_t937_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5778/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NullStreamReader_t937_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullStreamReader"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &NullStreamReader_t937_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NullStreamReader_t937_0_0_0/* byval_arg */
	, &NullStreamReader_t937_1_0_0/* this_arg */
	, &NullStreamReader_t937_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullStreamReader_t937)/* instance_size */
	, sizeof (NullStreamReader_t937)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.StreamWriter
#include "mscorlib_System_IO_StreamWriter.h"
// Metadata Definition System.IO.StreamWriter
extern TypeInfo StreamWriter_t939_il2cpp_TypeInfo;
// System.IO.StreamWriter
#include "mscorlib_System_IO_StreamWriterMethodDeclarations.h"
static const EncodedMethodIndex StreamWriter_t939_VTable[15] = 
{
	120,
	2076,
	122,
	123,
	2077,
	2078,
	2079,
	2080,
	2081,
	2082,
	2083,
	2084,
	2085,
	2086,
	2087,
};
static Il2CppInterfaceOffsetPair StreamWriter_t939_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StreamWriter_t939_0_0_0;
extern const Il2CppType StreamWriter_t939_1_0_0;
extern const Il2CppType TextWriter_t599_0_0_0;
struct StreamWriter_t939;
const Il2CppTypeDefinitionMetadata StreamWriter_t939_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StreamWriter_t939_InterfacesOffsets/* interfaceOffsets */
	, &TextWriter_t599_0_0_0/* parent */
	, StreamWriter_t939_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3515/* fieldStart */
	, 5784/* methodStart */
	, -1/* eventStart */
	, 1130/* propertyStart */

};
TypeInfo StreamWriter_t939_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StreamWriter"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &StreamWriter_t939_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 907/* custom_attributes_cache */
	, &StreamWriter_t939_0_0_0/* byval_arg */
	, &StreamWriter_t939_1_0_0/* this_arg */
	, &StreamWriter_t939_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StreamWriter_t939)/* instance_size */
	, sizeof (StreamWriter_t939)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StreamWriter_t939_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 1/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.StringReader
#include "mscorlib_System_IO_StringReader.h"
// Metadata Definition System.IO.StringReader
extern TypeInfo StringReader_t330_il2cpp_TypeInfo;
// System.IO.StringReader
#include "mscorlib_System_IO_StringReaderMethodDeclarations.h"
static const EncodedMethodIndex StringReader_t330_VTable[11] = 
{
	120,
	125,
	122,
	123,
	2064,
	2088,
	2089,
	2090,
	2091,
	2092,
	2093,
};
static Il2CppInterfaceOffsetPair StringReader_t330_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringReader_t330_0_0_0;
extern const Il2CppType StringReader_t330_1_0_0;
struct StringReader_t330;
const Il2CppTypeDefinitionMetadata StringReader_t330_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringReader_t330_InterfacesOffsets/* interfaceOffsets */
	, &TextReader_t861_0_0_0/* parent */
	, StringReader_t330_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3525/* fieldStart */
	, 5801/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StringReader_t330_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringReader"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &StringReader_t330_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 908/* custom_attributes_cache */
	, &StringReader_t330_0_0_0/* byval_arg */
	, &StringReader_t330_1_0_0/* this_arg */
	, &StringReader_t330_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringReader_t330)/* instance_size */
	, sizeof (StringReader_t330)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.TextReader
#include "mscorlib_System_IO_TextReader.h"
// Metadata Definition System.IO.TextReader
extern TypeInfo TextReader_t861_il2cpp_TypeInfo;
// System.IO.TextReader
#include "mscorlib_System_IO_TextReaderMethodDeclarations.h"
extern const Il2CppType NullTextReader_t940_0_0_0;
static const Il2CppType* TextReader_t861_il2cpp_TypeInfo__nestedTypes[1] =
{
	&NullTextReader_t940_0_0_0,
};
static const EncodedMethodIndex TextReader_t861_VTable[11] = 
{
	120,
	125,
	122,
	123,
	2064,
	2094,
	2095,
	2096,
	2097,
	2098,
	2099,
};
static const Il2CppType* TextReader_t861_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
};
static Il2CppInterfaceOffsetPair TextReader_t861_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TextReader_t861_1_0_0;
struct TextReader_t861;
const Il2CppTypeDefinitionMetadata TextReader_t861_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextReader_t861_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, TextReader_t861_InterfacesTypeInfos/* implementedInterfaces */
	, TextReader_t861_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextReader_t861_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3528/* fieldStart */
	, 5809/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextReader_t861_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextReader"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &TextReader_t861_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 909/* custom_attributes_cache */
	, &TextReader_t861_0_0_0/* byval_arg */
	, &TextReader_t861_1_0_0/* this_arg */
	, &TextReader_t861_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextReader_t861)/* instance_size */
	, sizeof (TextReader_t861)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextReader_t861_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.TextReader/NullTextReader
#include "mscorlib_System_IO_TextReader_NullTextReader.h"
// Metadata Definition System.IO.TextReader/NullTextReader
extern TypeInfo NullTextReader_t940_il2cpp_TypeInfo;
// System.IO.TextReader/NullTextReader
#include "mscorlib_System_IO_TextReader_NullTextReaderMethodDeclarations.h"
static const EncodedMethodIndex NullTextReader_t940_VTable[11] = 
{
	120,
	125,
	122,
	123,
	2064,
	2094,
	2095,
	2096,
	2097,
	2100,
	2099,
};
static Il2CppInterfaceOffsetPair NullTextReader_t940_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullTextReader_t940_1_0_0;
struct NullTextReader_t940;
const Il2CppTypeDefinitionMetadata NullTextReader_t940_DefinitionMetadata = 
{
	&TextReader_t861_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullTextReader_t940_InterfacesOffsets/* interfaceOffsets */
	, &TextReader_t861_0_0_0/* parent */
	, NullTextReader_t940_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5819/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NullTextReader_t940_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullTextReader"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &NullTextReader_t940_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NullTextReader_t940_0_0_0/* byval_arg */
	, &NullTextReader_t940_1_0_0/* this_arg */
	, &NullTextReader_t940_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullTextReader_t940)/* instance_size */
	, sizeof (NullTextReader_t940)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.SynchronizedReader
#include "mscorlib_System_IO_SynchronizedReader.h"
// Metadata Definition System.IO.SynchronizedReader
extern TypeInfo SynchronizedReader_t941_il2cpp_TypeInfo;
// System.IO.SynchronizedReader
#include "mscorlib_System_IO_SynchronizedReaderMethodDeclarations.h"
static const EncodedMethodIndex SynchronizedReader_t941_VTable[11] = 
{
	120,
	125,
	122,
	123,
	2064,
	2094,
	2101,
	2102,
	2103,
	2104,
	2105,
};
static Il2CppInterfaceOffsetPair SynchronizedReader_t941_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizedReader_t941_0_0_0;
extern const Il2CppType SynchronizedReader_t941_1_0_0;
struct SynchronizedReader_t941;
const Il2CppTypeDefinitionMetadata SynchronizedReader_t941_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SynchronizedReader_t941_InterfacesOffsets/* interfaceOffsets */
	, &TextReader_t861_0_0_0/* parent */
	, SynchronizedReader_t941_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3529/* fieldStart */
	, 5821/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SynchronizedReader_t941_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizedReader"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &SynchronizedReader_t941_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SynchronizedReader_t941_0_0_0/* byval_arg */
	, &SynchronizedReader_t941_1_0_0/* this_arg */
	, &SynchronizedReader_t941_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizedReader_t941)/* instance_size */
	, sizeof (SynchronizedReader_t941)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.TextWriter
#include "mscorlib_System_IO_TextWriter.h"
// Metadata Definition System.IO.TextWriter
extern TypeInfo TextWriter_t599_il2cpp_TypeInfo;
// System.IO.TextWriter
#include "mscorlib_System_IO_TextWriterMethodDeclarations.h"
extern const Il2CppType NullTextWriter_t942_0_0_0;
static const Il2CppType* TextWriter_t599_il2cpp_TypeInfo__nestedTypes[1] =
{
	&NullTextWriter_t942_0_0_0,
};
static const EncodedMethodIndex TextWriter_t599_VTable[14] = 
{
	120,
	125,
	122,
	123,
	2077,
	2106,
	2107,
	2108,
	2109,
	2110,
	2111,
	2112,
	2085,
	2086,
};
static const Il2CppType* TextWriter_t599_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
};
static Il2CppInterfaceOffsetPair TextWriter_t599_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TextWriter_t599_1_0_0;
struct TextWriter_t599;
const Il2CppTypeDefinitionMetadata TextWriter_t599_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextWriter_t599_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, TextWriter_t599_InterfacesTypeInfos/* implementedInterfaces */
	, TextWriter_t599_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextWriter_t599_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3530/* fieldStart */
	, 5827/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextWriter_t599_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextWriter"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &TextWriter_t599_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 910/* custom_attributes_cache */
	, &TextWriter_t599_0_0_0/* byval_arg */
	, &TextWriter_t599_1_0_0/* this_arg */
	, &TextWriter_t599_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextWriter_t599)/* instance_size */
	, sizeof (TextWriter_t599)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextWriter_t599_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 14/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.TextWriter/NullTextWriter
#include "mscorlib_System_IO_TextWriter_NullTextWriter.h"
// Metadata Definition System.IO.TextWriter/NullTextWriter
extern TypeInfo NullTextWriter_t942_il2cpp_TypeInfo;
// System.IO.TextWriter/NullTextWriter
#include "mscorlib_System_IO_TextWriter_NullTextWriterMethodDeclarations.h"
static const EncodedMethodIndex NullTextWriter_t942_VTable[14] = 
{
	120,
	125,
	122,
	123,
	2077,
	2106,
	2107,
	2108,
	2113,
	2110,
	2114,
	2115,
	2085,
	2086,
};
static Il2CppInterfaceOffsetPair NullTextWriter_t942_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullTextWriter_t942_1_0_0;
struct NullTextWriter_t942;
const Il2CppTypeDefinitionMetadata NullTextWriter_t942_DefinitionMetadata = 
{
	&TextWriter_t599_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullTextWriter_t942_InterfacesOffsets/* interfaceOffsets */
	, &TextWriter_t599_0_0_0/* parent */
	, NullTextWriter_t942_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5840/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NullTextWriter_t942_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullTextWriter"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &NullTextWriter_t942_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NullTextWriter_t942_0_0_0/* byval_arg */
	, &NullTextWriter_t942_1_0_0/* this_arg */
	, &NullTextWriter_t942_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullTextWriter_t942)/* instance_size */
	, sizeof (NullTextWriter_t942)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.SynchronizedWriter
#include "mscorlib_System_IO_SynchronizedWriter.h"
// Metadata Definition System.IO.SynchronizedWriter
extern TypeInfo SynchronizedWriter_t943_il2cpp_TypeInfo;
// System.IO.SynchronizedWriter
#include "mscorlib_System_IO_SynchronizedWriterMethodDeclarations.h"
static const EncodedMethodIndex SynchronizedWriter_t943_VTable[14] = 
{
	120,
	125,
	122,
	123,
	2077,
	2116,
	2107,
	2117,
	2118,
	2119,
	2120,
	2121,
	2122,
	2123,
};
static Il2CppInterfaceOffsetPair SynchronizedWriter_t943_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizedWriter_t943_0_0_0;
extern const Il2CppType SynchronizedWriter_t943_1_0_0;
struct SynchronizedWriter_t943;
const Il2CppTypeDefinitionMetadata SynchronizedWriter_t943_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SynchronizedWriter_t943_InterfacesOffsets/* interfaceOffsets */
	, &TextWriter_t599_0_0_0/* parent */
	, SynchronizedWriter_t943_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3532/* fieldStart */
	, 5844/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SynchronizedWriter_t943_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizedWriter"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &SynchronizedWriter_t943_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SynchronizedWriter_t943_0_0_0/* byval_arg */
	, &SynchronizedWriter_t943_1_0_0/* this_arg */
	, &SynchronizedWriter_t943_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizedWriter_t943)/* instance_size */
	, sizeof (SynchronizedWriter_t943)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.UnexceptionalStreamReader
#include "mscorlib_System_IO_UnexceptionalStreamReader.h"
// Metadata Definition System.IO.UnexceptionalStreamReader
extern TypeInfo UnexceptionalStreamReader_t944_il2cpp_TypeInfo;
// System.IO.UnexceptionalStreamReader
#include "mscorlib_System_IO_UnexceptionalStreamReaderMethodDeclarations.h"
static const EncodedMethodIndex UnexceptionalStreamReader_t944_VTable[11] = 
{
	120,
	125,
	122,
	123,
	2064,
	2065,
	2124,
	2125,
	2126,
	2127,
	2128,
};
static Il2CppInterfaceOffsetPair UnexceptionalStreamReader_t944_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnexceptionalStreamReader_t944_0_0_0;
extern const Il2CppType UnexceptionalStreamReader_t944_1_0_0;
struct UnexceptionalStreamReader_t944;
const Il2CppTypeDefinitionMetadata UnexceptionalStreamReader_t944_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnexceptionalStreamReader_t944_InterfacesOffsets/* interfaceOffsets */
	, &StreamReader_t938_0_0_0/* parent */
	, UnexceptionalStreamReader_t944_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3534/* fieldStart */
	, 5853/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnexceptionalStreamReader_t944_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnexceptionalStreamReader"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &UnexceptionalStreamReader_t944_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnexceptionalStreamReader_t944_0_0_0/* byval_arg */
	, &UnexceptionalStreamReader_t944_1_0_0/* this_arg */
	, &UnexceptionalStreamReader_t944_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnexceptionalStreamReader_t944)/* instance_size */
	, sizeof (UnexceptionalStreamReader_t944)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(UnexceptionalStreamReader_t944_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.UnexceptionalStreamWriter
#include "mscorlib_System_IO_UnexceptionalStreamWriter.h"
// Metadata Definition System.IO.UnexceptionalStreamWriter
extern TypeInfo UnexceptionalStreamWriter_t945_il2cpp_TypeInfo;
// System.IO.UnexceptionalStreamWriter
#include "mscorlib_System_IO_UnexceptionalStreamWriterMethodDeclarations.h"
static const EncodedMethodIndex UnexceptionalStreamWriter_t945_VTable[15] = 
{
	120,
	2076,
	122,
	123,
	2077,
	2078,
	2079,
	2129,
	2130,
	2131,
	2132,
	2133,
	2085,
	2086,
	2087,
};
static Il2CppInterfaceOffsetPair UnexceptionalStreamWriter_t945_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnexceptionalStreamWriter_t945_0_0_0;
extern const Il2CppType UnexceptionalStreamWriter_t945_1_0_0;
struct UnexceptionalStreamWriter_t945;
const Il2CppTypeDefinitionMetadata UnexceptionalStreamWriter_t945_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnexceptionalStreamWriter_t945_InterfacesOffsets/* interfaceOffsets */
	, &StreamWriter_t939_0_0_0/* parent */
	, UnexceptionalStreamWriter_t945_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5861/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnexceptionalStreamWriter_t945_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnexceptionalStreamWriter"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &UnexceptionalStreamWriter_t945_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnexceptionalStreamWriter_t945_0_0_0/* byval_arg */
	, &UnexceptionalStreamWriter_t945_1_0_0/* this_arg */
	, &UnexceptionalStreamWriter_t945_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnexceptionalStreamWriter_t945)/* instance_size */
	, sizeof (UnexceptionalStreamWriter_t945)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Emit.AssemblyBuilder
#include "mscorlib_System_Reflection_Emit_AssemblyBuilder.h"
// Metadata Definition System.Reflection.Emit.AssemblyBuilder
extern TypeInfo AssemblyBuilder_t948_il2cpp_TypeInfo;
// System.Reflection.Emit.AssemblyBuilder
#include "mscorlib_System_Reflection_Emit_AssemblyBuilderMethodDeclarations.h"
static const EncodedMethodIndex AssemblyBuilder_t948_VTable[21] = 
{
	120,
	125,
	122,
	2134,
	2135,
	2136,
	2137,
	2138,
	2136,
	2135,
	2139,
	2140,
	2141,
	2142,
	2143,
	2144,
	2145,
	2146,
	2147,
	2148,
	2149,
};
extern const Il2CppType _AssemblyBuilder_t2089_0_0_0;
static const Il2CppType* AssemblyBuilder_t948_InterfacesTypeInfos[] = 
{
	&_AssemblyBuilder_t2089_0_0_0,
};
extern const Il2CppType ICustomAttributeProvider_t1404_0_0_0;
extern const Il2CppType _Assembly_t2099_0_0_0;
static Il2CppInterfaceOffsetPair AssemblyBuilder_t948_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t1404_0_0_0, 4},
	{ &_Assembly_t2099_0_0_0, 6},
	{ &_AssemblyBuilder_t2089_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyBuilder_t948_0_0_0;
extern const Il2CppType AssemblyBuilder_t948_1_0_0;
extern const Il2CppType Assembly_t580_0_0_0;
struct AssemblyBuilder_t948;
const Il2CppTypeDefinitionMetadata AssemblyBuilder_t948_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AssemblyBuilder_t948_InterfacesTypeInfos/* implementedInterfaces */
	, AssemblyBuilder_t948_InterfacesOffsets/* interfaceOffsets */
	, &Assembly_t580_0_0_0/* parent */
	, AssemblyBuilder_t948_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3536/* fieldStart */
	, 5867/* methodStart */
	, -1/* eventStart */
	, 1131/* propertyStart */

};
TypeInfo AssemblyBuilder_t948_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &AssemblyBuilder_t948_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 911/* custom_attributes_cache */
	, &AssemblyBuilder_t948_0_0_0/* byval_arg */
	, &AssemblyBuilder_t948_1_0_0/* this_arg */
	, &AssemblyBuilder_t948_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyBuilder_t948)/* instance_size */
	, sizeof (AssemblyBuilder_t948)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 21/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.Emit.ConstructorBuilder
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder.h"
// Metadata Definition System.Reflection.Emit.ConstructorBuilder
extern TypeInfo ConstructorBuilder_t953_il2cpp_TypeInfo;
// System.Reflection.Emit.ConstructorBuilder
#include "mscorlib_System_Reflection_Emit_ConstructorBuilderMethodDeclarations.h"
static const EncodedMethodIndex ConstructorBuilder_t953_VTable[31] = 
{
	120,
	125,
	122,
	2150,
	2151,
	2152,
	2153,
	2154,
	2155,
	2156,
	2157,
	2152,
	2158,
	2151,
	2159,
	2160,
	2161,
	2162,
	2163,
	2164,
	2165,
	2166,
	2167,
	2168,
	2169,
	2170,
	2171,
	2172,
	2173,
	2174,
	2175,
};
extern const Il2CppType _ConstructorBuilder_t2090_0_0_0;
static const Il2CppType* ConstructorBuilder_t953_InterfacesTypeInfos[] = 
{
	&_ConstructorBuilder_t2090_0_0_0,
};
extern const Il2CppType _ConstructorInfo_t2101_0_0_0;
extern const Il2CppType _MethodBase_t2104_0_0_0;
extern const Il2CppType _MemberInfo_t2064_0_0_0;
static Il2CppInterfaceOffsetPair ConstructorBuilder_t953_InterfacesOffsets[] = 
{
	{ &_ConstructorInfo_t2101_0_0_0, 30},
	{ &_MethodBase_t2104_0_0_0, 14},
	{ &ICustomAttributeProvider_t1404_0_0_0, 4},
	{ &_MemberInfo_t2064_0_0_0, 6},
	{ &_ConstructorBuilder_t2090_0_0_0, 31},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructorBuilder_t953_0_0_0;
extern const Il2CppType ConstructorBuilder_t953_1_0_0;
extern const Il2CppType ConstructorInfo_t202_0_0_0;
struct ConstructorBuilder_t953;
const Il2CppTypeDefinitionMetadata ConstructorBuilder_t953_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructorBuilder_t953_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructorBuilder_t953_InterfacesOffsets/* interfaceOffsets */
	, &ConstructorInfo_t202_0_0_0/* parent */
	, ConstructorBuilder_t953_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3543/* fieldStart */
	, 5873/* methodStart */
	, -1/* eventStart */
	, 1133/* propertyStart */

};
TypeInfo ConstructorBuilder_t953_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructorBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &ConstructorBuilder_t953_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 912/* custom_attributes_cache */
	, &ConstructorBuilder_t953_0_0_0/* byval_arg */
	, &ConstructorBuilder_t953_1_0_0/* this_arg */
	, &ConstructorBuilder_t953_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructorBuilder_t953)/* instance_size */
	, sizeof (ConstructorBuilder_t953)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 9/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 31/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.Emit.EnumBuilder
#include "mscorlib_System_Reflection_Emit_EnumBuilder.h"
// Metadata Definition System.Reflection.Emit.EnumBuilder
extern TypeInfo EnumBuilder_t954_il2cpp_TypeInfo;
// System.Reflection.Emit.EnumBuilder
#include "mscorlib_System_Reflection_Emit_EnumBuilderMethodDeclarations.h"
static const EncodedMethodIndex EnumBuilder_t954_VTable[81] = 
{
	1420,
	125,
	1421,
	1422,
	2176,
	2177,
	2178,
	1426,
	2179,
	2180,
	2181,
	2177,
	2182,
	2176,
	2183,
	2184,
	1428,
	2185,
	2186,
	1429,
	1430,
	1431,
	1432,
	1433,
	1434,
	1435,
	1436,
	1437,
	1438,
	1439,
	1440,
	1441,
	1442,
	1443,
	2187,
	2188,
	2189,
	1445,
	1446,
	2190,
	1447,
	1448,
	2191,
	2192,
	2193,
	2194,
	1449,
	1450,
	1451,
	1452,
	2195,
	2196,
	2197,
	1453,
	1454,
	1455,
	1456,
	2198,
	2199,
	2200,
	2201,
	2202,
	2203,
	2204,
	2205,
	2206,
	1458,
	1459,
	1460,
	1461,
	1462,
	1463,
	2207,
	2208,
	1464,
	1465,
	1466,
	1467,
	1468,
	1469,
	1470,
};
extern const Il2CppType _EnumBuilder_t2091_0_0_0;
static const Il2CppType* EnumBuilder_t954_InterfacesTypeInfos[] = 
{
	&_EnumBuilder_t2091_0_0_0,
};
extern const Il2CppType IReflect_t2065_0_0_0;
extern const Il2CppType _Type_t2063_0_0_0;
static Il2CppInterfaceOffsetPair EnumBuilder_t954_InterfacesOffsets[] = 
{
	{ &IReflect_t2065_0_0_0, 14},
	{ &_Type_t2063_0_0_0, 14},
	{ &ICustomAttributeProvider_t1404_0_0_0, 4},
	{ &_MemberInfo_t2064_0_0_0, 6},
	{ &_EnumBuilder_t2091_0_0_0, 81},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnumBuilder_t954_0_0_0;
extern const Il2CppType EnumBuilder_t954_1_0_0;
extern const Il2CppType Type_t_0_0_0;
struct EnumBuilder_t954;
const Il2CppTypeDefinitionMetadata EnumBuilder_t954_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnumBuilder_t954_InterfacesTypeInfos/* implementedInterfaces */
	, EnumBuilder_t954_InterfacesOffsets/* interfaceOffsets */
	, &Type_t_0_0_0/* parent */
	, EnumBuilder_t954_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3554/* fieldStart */
	, 5899/* methodStart */
	, -1/* eventStart */
	, 1142/* propertyStart */

};
TypeInfo EnumBuilder_t954_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnumBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &EnumBuilder_t954_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 914/* custom_attributes_cache */
	, &EnumBuilder_t954_0_0_0/* byval_arg */
	, &EnumBuilder_t954_1_0_0/* this_arg */
	, &EnumBuilder_t954_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnumBuilder_t954)/* instance_size */
	, sizeof (EnumBuilder_t954)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 34/* method_count */
	, 11/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 81/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.Emit.FieldBuilder
#include "mscorlib_System_Reflection_Emit_FieldBuilder.h"
// Metadata Definition System.Reflection.Emit.FieldBuilder
extern TypeInfo FieldBuilder_t956_il2cpp_TypeInfo;
// System.Reflection.Emit.FieldBuilder
#include "mscorlib_System_Reflection_Emit_FieldBuilderMethodDeclarations.h"
static const EncodedMethodIndex FieldBuilder_t956_VTable[27] = 
{
	120,
	125,
	122,
	123,
	2209,
	2210,
	2211,
	2212,
	2213,
	2214,
	2215,
	2210,
	2216,
	2209,
	2217,
	2218,
	2219,
	2220,
	2221,
	2222,
	2223,
	2224,
	2225,
	2226,
	2227,
	2228,
	2229,
};
extern const Il2CppType _FieldBuilder_t2092_0_0_0;
static const Il2CppType* FieldBuilder_t956_InterfacesTypeInfos[] = 
{
	&_FieldBuilder_t2092_0_0_0,
};
extern const Il2CppType _FieldInfo_t2103_0_0_0;
static Il2CppInterfaceOffsetPair FieldBuilder_t956_InterfacesOffsets[] = 
{
	{ &_FieldInfo_t2103_0_0_0, 14},
	{ &ICustomAttributeProvider_t1404_0_0_0, 4},
	{ &_MemberInfo_t2064_0_0_0, 6},
	{ &_FieldBuilder_t2092_0_0_0, 27},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldBuilder_t956_0_0_0;
extern const Il2CppType FieldBuilder_t956_1_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
struct FieldBuilder_t956;
const Il2CppTypeDefinitionMetadata FieldBuilder_t956_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FieldBuilder_t956_InterfacesTypeInfos/* implementedInterfaces */
	, FieldBuilder_t956_InterfacesOffsets/* interfaceOffsets */
	, &FieldInfo_t_0_0_0/* parent */
	, FieldBuilder_t956_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3556/* fieldStart */
	, 5933/* methodStart */
	, -1/* eventStart */
	, 1153/* propertyStart */

};
TypeInfo FieldBuilder_t956_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &FieldBuilder_t956_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 916/* custom_attributes_cache */
	, &FieldBuilder_t956_0_0_0/* byval_arg */
	, &FieldBuilder_t956_1_0_0/* this_arg */
	, &FieldBuilder_t956_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldBuilder_t956)/* instance_size */
	, sizeof (FieldBuilder_t956)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 8/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 27/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.Emit.GenericTypeParameterBuilder
#include "mscorlib_System_Reflection_Emit_GenericTypeParameterBuilder.h"
// Metadata Definition System.Reflection.Emit.GenericTypeParameterBuilder
extern TypeInfo GenericTypeParameterBuilder_t958_il2cpp_TypeInfo;
// System.Reflection.Emit.GenericTypeParameterBuilder
#include "mscorlib_System_Reflection_Emit_GenericTypeParameterBuilderMethodDeclarations.h"
static const EncodedMethodIndex GenericTypeParameterBuilder_t958_VTable[81] = 
{
	2230,
	125,
	2231,
	2232,
	2233,
	2234,
	2235,
	1426,
	2236,
	2237,
	2238,
	2234,
	2239,
	2233,
	2240,
	2241,
	1428,
	2242,
	2243,
	1429,
	1430,
	1431,
	1432,
	1433,
	1434,
	1435,
	1436,
	1437,
	1438,
	1439,
	1440,
	1441,
	1442,
	1443,
	2244,
	2245,
	2246,
	1445,
	2247,
	2248,
	2249,
	2250,
	2251,
	2252,
	2253,
	2254,
	1449,
	1450,
	1451,
	1452,
	2255,
	2256,
	2257,
	1453,
	1454,
	1455,
	1456,
	2258,
	2259,
	2260,
	2261,
	2262,
	2263,
	2264,
	2265,
	2266,
	1458,
	1459,
	1460,
	1461,
	1462,
	1463,
	2267,
	2268,
	2269,
	2270,
	2271,
	2272,
	2273,
	2274,
	2275,
};
static Il2CppInterfaceOffsetPair GenericTypeParameterBuilder_t958_InterfacesOffsets[] = 
{
	{ &IReflect_t2065_0_0_0, 14},
	{ &_Type_t2063_0_0_0, 14},
	{ &ICustomAttributeProvider_t1404_0_0_0, 4},
	{ &_MemberInfo_t2064_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GenericTypeParameterBuilder_t958_0_0_0;
extern const Il2CppType GenericTypeParameterBuilder_t958_1_0_0;
struct GenericTypeParameterBuilder_t958;
const Il2CppTypeDefinitionMetadata GenericTypeParameterBuilder_t958_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericTypeParameterBuilder_t958_InterfacesOffsets/* interfaceOffsets */
	, &Type_t_0_0_0/* parent */
	, GenericTypeParameterBuilder_t958_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3561/* fieldStart */
	, 5948/* methodStart */
	, -1/* eventStart */
	, 1161/* propertyStart */

};
TypeInfo GenericTypeParameterBuilder_t958_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericTypeParameterBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &GenericTypeParameterBuilder_t958_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 917/* custom_attributes_cache */
	, &GenericTypeParameterBuilder_t958_0_0_0/* byval_arg */
	, &GenericTypeParameterBuilder_t958_1_0_0/* this_arg */
	, &GenericTypeParameterBuilder_t958_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericTypeParameterBuilder_t958)/* instance_size */
	, sizeof (GenericTypeParameterBuilder_t958)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 47/* method_count */
	, 15/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 81/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.Emit.ILTokenInfo
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
// Metadata Definition System.Reflection.Emit.ILTokenInfo
extern TypeInfo ILTokenInfo_t959_il2cpp_TypeInfo;
// System.Reflection.Emit.ILTokenInfo
#include "mscorlib_System_Reflection_Emit_ILTokenInfoMethodDeclarations.h"
static const EncodedMethodIndex ILTokenInfo_t959_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ILTokenInfo_t959_0_0_0;
extern const Il2CppType ILTokenInfo_t959_1_0_0;
const Il2CppTypeDefinitionMetadata ILTokenInfo_t959_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, ILTokenInfo_t959_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3565/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ILTokenInfo_t959_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILTokenInfo"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &ILTokenInfo_t959_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILTokenInfo_t959_0_0_0/* byval_arg */
	, &ILTokenInfo_t959_1_0_0/* this_arg */
	, &ILTokenInfo_t959_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ILTokenInfo_t959)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ILTokenInfo_t959)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Reflection.Emit.TokenGenerator
extern TypeInfo TokenGenerator_t966_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TokenGenerator_t966_0_0_0;
extern const Il2CppType TokenGenerator_t966_1_0_0;
struct TokenGenerator_t966;
const Il2CppTypeDefinitionMetadata TokenGenerator_t966_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5995/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TokenGenerator_t966_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TokenGenerator"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &TokenGenerator_t966_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TokenGenerator_t966_0_0_0/* byval_arg */
	, &TokenGenerator_t966_1_0_0/* this_arg */
	, &TokenGenerator_t966_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.ILGenerator
#include "mscorlib_System_Reflection_Emit_ILGenerator.h"
// Metadata Definition System.Reflection.Emit.ILGenerator
extern TypeInfo ILGenerator_t949_il2cpp_TypeInfo;
// System.Reflection.Emit.ILGenerator
#include "mscorlib_System_Reflection_Emit_ILGeneratorMethodDeclarations.h"
extern const Il2CppType LabelFixup_t960_0_0_0;
extern const Il2CppType LabelData_t961_0_0_0;
static const Il2CppType* ILGenerator_t949_il2cpp_TypeInfo__nestedTypes[2] =
{
	&LabelFixup_t960_0_0_0,
	&LabelData_t961_0_0_0,
};
static const EncodedMethodIndex ILGenerator_t949_VTable[6] = 
{
	120,
	125,
	122,
	123,
	2276,
	2277,
};
extern const Il2CppType _ILGenerator_t2093_0_0_0;
static const Il2CppType* ILGenerator_t949_InterfacesTypeInfos[] = 
{
	&_ILGenerator_t2093_0_0_0,
};
static Il2CppInterfaceOffsetPair ILGenerator_t949_InterfacesOffsets[] = 
{
	{ &_ILGenerator_t2093_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ILGenerator_t949_0_0_0;
extern const Il2CppType ILGenerator_t949_1_0_0;
struct ILGenerator_t949;
const Il2CppTypeDefinitionMetadata ILGenerator_t949_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ILGenerator_t949_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ILGenerator_t949_InterfacesTypeInfos/* implementedInterfaces */
	, ILGenerator_t949_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ILGenerator_t949_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3567/* fieldStart */
	, 5996/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ILGenerator_t949_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILGenerator"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &ILGenerator_t949_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 924/* custom_attributes_cache */
	, &ILGenerator_t949_0_0_0/* byval_arg */
	, &ILGenerator_t949_1_0_0/* this_arg */
	, &ILGenerator_t949_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ILGenerator_t949)/* instance_size */
	, sizeof (ILGenerator_t949)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ILGenerator_t949_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Emit.ILGenerator/LabelFixup
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
// Metadata Definition System.Reflection.Emit.ILGenerator/LabelFixup
extern TypeInfo LabelFixup_t960_il2cpp_TypeInfo;
// System.Reflection.Emit.ILGenerator/LabelFixup
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixupMethodDeclarations.h"
static const EncodedMethodIndex LabelFixup_t960_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LabelFixup_t960_1_0_0;
const Il2CppTypeDefinitionMetadata LabelFixup_t960_DefinitionMetadata = 
{
	&ILGenerator_t949_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, LabelFixup_t960_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3579/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LabelFixup_t960_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LabelFixup"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &LabelFixup_t960_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LabelFixup_t960_0_0_0/* byval_arg */
	, &LabelFixup_t960_1_0_0/* this_arg */
	, &LabelFixup_t960_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LabelFixup_t960)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LabelFixup_t960)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(LabelFixup_t960 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.ILGenerator/LabelData
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
// Metadata Definition System.Reflection.Emit.ILGenerator/LabelData
extern TypeInfo LabelData_t961_il2cpp_TypeInfo;
// System.Reflection.Emit.ILGenerator/LabelData
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelDataMethodDeclarations.h"
static const EncodedMethodIndex LabelData_t961_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LabelData_t961_1_0_0;
const Il2CppTypeDefinitionMetadata LabelData_t961_DefinitionMetadata = 
{
	&ILGenerator_t949_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, LabelData_t961_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3582/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LabelData_t961_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LabelData"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &LabelData_t961_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LabelData_t961_0_0_0/* byval_arg */
	, &LabelData_t961_1_0_0/* this_arg */
	, &LabelData_t961_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LabelData_t961)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LabelData_t961)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(LabelData_t961 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.MethodBuilder
#include "mscorlib_System_Reflection_Emit_MethodBuilder.h"
// Metadata Definition System.Reflection.Emit.MethodBuilder
extern TypeInfo MethodBuilder_t957_il2cpp_TypeInfo;
// System.Reflection.Emit.MethodBuilder
#include "mscorlib_System_Reflection_Emit_MethodBuilderMethodDeclarations.h"
static const EncodedMethodIndex MethodBuilder_t957_VTable[33] = 
{
	2278,
	125,
	2279,
	2280,
	2281,
	2282,
	2283,
	2284,
	2285,
	2286,
	2287,
	2282,
	2288,
	2281,
	2289,
	2290,
	2161,
	2291,
	2292,
	2293,
	2294,
	2166,
	2167,
	2168,
	2169,
	2295,
	2296,
	2297,
	2298,
	2299,
	2300,
	2301,
	2302,
};
extern const Il2CppType _MethodBuilder_t2094_0_0_0;
static const Il2CppType* MethodBuilder_t957_InterfacesTypeInfos[] = 
{
	&_MethodBuilder_t2094_0_0_0,
};
extern const Il2CppType _MethodInfo_t2105_0_0_0;
static Il2CppInterfaceOffsetPair MethodBuilder_t957_InterfacesOffsets[] = 
{
	{ &_MethodInfo_t2105_0_0_0, 30},
	{ &_MethodBase_t2104_0_0_0, 14},
	{ &ICustomAttributeProvider_t1404_0_0_0, 4},
	{ &_MemberInfo_t2064_0_0_0, 6},
	{ &_MethodBuilder_t2094_0_0_0, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodBuilder_t957_0_0_0;
extern const Il2CppType MethodBuilder_t957_1_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
struct MethodBuilder_t957;
const Il2CppTypeDefinitionMetadata MethodBuilder_t957_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodBuilder_t957_InterfacesTypeInfos/* implementedInterfaces */
	, MethodBuilder_t957_InterfacesOffsets/* interfaceOffsets */
	, &MethodInfo_t_0_0_0/* parent */
	, MethodBuilder_t957_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3584/* fieldStart */
	, 6006/* methodStart */
	, -1/* eventStart */
	, 1176/* propertyStart */

};
TypeInfo MethodBuilder_t957_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &MethodBuilder_t957_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 927/* custom_attributes_cache */
	, &MethodBuilder_t957_0_0_0/* byval_arg */
	, &MethodBuilder_t957_1_0_0/* this_arg */
	, &MethodBuilder_t957_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodBuilder_t957)/* instance_size */
	, sizeof (MethodBuilder_t957)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 27/* method_count */
	, 11/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.Emit.MethodToken
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
// Metadata Definition System.Reflection.Emit.MethodToken
extern TypeInfo MethodToken_t968_il2cpp_TypeInfo;
// System.Reflection.Emit.MethodToken
#include "mscorlib_System_Reflection_Emit_MethodTokenMethodDeclarations.h"
static const EncodedMethodIndex MethodToken_t968_VTable[4] = 
{
	2303,
	125,
	2304,
	152,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodToken_t968_0_0_0;
extern const Il2CppType MethodToken_t968_1_0_0;
const Il2CppTypeDefinitionMetadata MethodToken_t968_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, MethodToken_t968_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3596/* fieldStart */
	, 6033/* methodStart */
	, -1/* eventStart */
	, 1187/* propertyStart */

};
TypeInfo MethodToken_t968_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodToken"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &MethodToken_t968_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 930/* custom_attributes_cache */
	, &MethodToken_t968_0_0_0/* byval_arg */
	, &MethodToken_t968_1_0_0/* this_arg */
	, &MethodToken_t968_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodToken_t968)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodToken_t968)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(MethodToken_t968 )/* native_size */
	, sizeof(MethodToken_t968_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.ModuleBuilder
#include "mscorlib_System_Reflection_Emit_ModuleBuilder.h"
// Metadata Definition System.Reflection.Emit.ModuleBuilder
extern TypeInfo ModuleBuilder_t971_il2cpp_TypeInfo;
// System.Reflection.Emit.ModuleBuilder
#include "mscorlib_System_Reflection_Emit_ModuleBuilderMethodDeclarations.h"
static const EncodedMethodIndex ModuleBuilder_t971_VTable[11] = 
{
	120,
	125,
	122,
	2305,
	2306,
	2307,
	2308,
	2307,
	2306,
	2309,
	2308,
};
extern const Il2CppType _ModuleBuilder_t2095_0_0_0;
static const Il2CppType* ModuleBuilder_t971_InterfacesTypeInfos[] = 
{
	&_ModuleBuilder_t2095_0_0_0,
};
extern const Il2CppType _Module_t2106_0_0_0;
static Il2CppInterfaceOffsetPair ModuleBuilder_t971_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &ICustomAttributeProvider_t1404_0_0_0, 5},
	{ &_Module_t2106_0_0_0, 7},
	{ &_ModuleBuilder_t2095_0_0_0, 11},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ModuleBuilder_t971_0_0_0;
extern const Il2CppType ModuleBuilder_t971_1_0_0;
extern const Il2CppType Module_t965_0_0_0;
struct ModuleBuilder_t971;
const Il2CppTypeDefinitionMetadata ModuleBuilder_t971_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ModuleBuilder_t971_InterfacesTypeInfos/* implementedInterfaces */
	, ModuleBuilder_t971_InterfacesOffsets/* interfaceOffsets */
	, &Module_t965_0_0_0/* parent */
	, ModuleBuilder_t971_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3598/* fieldStart */
	, 6038/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ModuleBuilder_t971_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ModuleBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &ModuleBuilder_t971_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 931/* custom_attributes_cache */
	, &ModuleBuilder_t971_0_0_0/* byval_arg */
	, &ModuleBuilder_t971_1_0_0/* this_arg */
	, &ModuleBuilder_t971_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ModuleBuilder_t971)/* instance_size */
	, sizeof (ModuleBuilder_t971)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ModuleBuilder_t971_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.Emit.ModuleBuilderTokenGenerator
#include "mscorlib_System_Reflection_Emit_ModuleBuilderTokenGenerator.h"
// Metadata Definition System.Reflection.Emit.ModuleBuilderTokenGenerator
extern TypeInfo ModuleBuilderTokenGenerator_t970_il2cpp_TypeInfo;
// System.Reflection.Emit.ModuleBuilderTokenGenerator
#include "mscorlib_System_Reflection_Emit_ModuleBuilderTokenGeneratorMethodDeclarations.h"
static const EncodedMethodIndex ModuleBuilderTokenGenerator_t970_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2310,
};
static const Il2CppType* ModuleBuilderTokenGenerator_t970_InterfacesTypeInfos[] = 
{
	&TokenGenerator_t966_0_0_0,
};
static Il2CppInterfaceOffsetPair ModuleBuilderTokenGenerator_t970_InterfacesOffsets[] = 
{
	{ &TokenGenerator_t966_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ModuleBuilderTokenGenerator_t970_0_0_0;
extern const Il2CppType ModuleBuilderTokenGenerator_t970_1_0_0;
struct ModuleBuilderTokenGenerator_t970;
const Il2CppTypeDefinitionMetadata ModuleBuilderTokenGenerator_t970_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ModuleBuilderTokenGenerator_t970_InterfacesTypeInfos/* implementedInterfaces */
	, ModuleBuilderTokenGenerator_t970_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ModuleBuilderTokenGenerator_t970_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3604/* fieldStart */
	, 6045/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ModuleBuilderTokenGenerator_t970_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ModuleBuilderTokenGenerator"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &ModuleBuilderTokenGenerator_t970_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ModuleBuilderTokenGenerator_t970_0_0_0/* byval_arg */
	, &ModuleBuilderTokenGenerator_t970_1_0_0/* this_arg */
	, &ModuleBuilderTokenGenerator_t970_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ModuleBuilderTokenGenerator_t970)/* instance_size */
	, sizeof (ModuleBuilderTokenGenerator_t970)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Emit.OpCode
#include "mscorlib_System_Reflection_Emit_OpCode.h"
// Metadata Definition System.Reflection.Emit.OpCode
extern TypeInfo OpCode_t972_il2cpp_TypeInfo;
// System.Reflection.Emit.OpCode
#include "mscorlib_System_Reflection_Emit_OpCodeMethodDeclarations.h"
static const EncodedMethodIndex OpCode_t972_VTable[4] = 
{
	2311,
	125,
	2312,
	2313,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OpCode_t972_0_0_0;
extern const Il2CppType OpCode_t972_1_0_0;
const Il2CppTypeDefinitionMetadata OpCode_t972_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, OpCode_t972_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3605/* fieldStart */
	, 6047/* methodStart */
	, -1/* eventStart */
	, 1188/* propertyStart */

};
TypeInfo OpCode_t972_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OpCode"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &OpCode_t972_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 932/* custom_attributes_cache */
	, &OpCode_t972_0_0_0/* byval_arg */
	, &OpCode_t972_1_0_0/* this_arg */
	, &OpCode_t972_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OpCode_t972)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (OpCode_t972)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(OpCode_t972 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.OpCodeNames
#include "mscorlib_System_Reflection_Emit_OpCodeNames.h"
// Metadata Definition System.Reflection.Emit.OpCodeNames
extern TypeInfo OpCodeNames_t973_il2cpp_TypeInfo;
// System.Reflection.Emit.OpCodeNames
#include "mscorlib_System_Reflection_Emit_OpCodeNamesMethodDeclarations.h"
static const EncodedMethodIndex OpCodeNames_t973_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OpCodeNames_t973_0_0_0;
extern const Il2CppType OpCodeNames_t973_1_0_0;
struct OpCodeNames_t973;
const Il2CppTypeDefinitionMetadata OpCodeNames_t973_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OpCodeNames_t973_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3613/* fieldStart */
	, 6055/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OpCodeNames_t973_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OpCodeNames"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &OpCodeNames_t973_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OpCodeNames_t973_0_0_0/* byval_arg */
	, &OpCodeNames_t973_1_0_0/* this_arg */
	, &OpCodeNames_t973_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OpCodeNames_t973)/* instance_size */
	, sizeof (OpCodeNames_t973)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(OpCodeNames_t973_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.OpCodes
#include "mscorlib_System_Reflection_Emit_OpCodes.h"
// Metadata Definition System.Reflection.Emit.OpCodes
extern TypeInfo OpCodes_t974_il2cpp_TypeInfo;
// System.Reflection.Emit.OpCodes
#include "mscorlib_System_Reflection_Emit_OpCodesMethodDeclarations.h"
static const EncodedMethodIndex OpCodes_t974_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OpCodes_t974_0_0_0;
extern const Il2CppType OpCodes_t974_1_0_0;
struct OpCodes_t974;
const Il2CppTypeDefinitionMetadata OpCodes_t974_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OpCodes_t974_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3614/* fieldStart */
	, 6056/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OpCodes_t974_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OpCodes"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &OpCodes_t974_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 933/* custom_attributes_cache */
	, &OpCodes_t974_0_0_0/* byval_arg */
	, &OpCodes_t974_1_0_0/* this_arg */
	, &OpCodes_t974_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OpCodes_t974)/* instance_size */
	, sizeof (OpCodes_t974)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(OpCodes_t974_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 226/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.ParameterBuilder
#include "mscorlib_System_Reflection_Emit_ParameterBuilder.h"
// Metadata Definition System.Reflection.Emit.ParameterBuilder
extern TypeInfo ParameterBuilder_t975_il2cpp_TypeInfo;
// System.Reflection.Emit.ParameterBuilder
#include "mscorlib_System_Reflection_Emit_ParameterBuilderMethodDeclarations.h"
static const EncodedMethodIndex ParameterBuilder_t975_VTable[7] = 
{
	120,
	125,
	122,
	123,
	2314,
	2315,
	2316,
};
extern const Il2CppType _ParameterBuilder_t2096_0_0_0;
static const Il2CppType* ParameterBuilder_t975_InterfacesTypeInfos[] = 
{
	&_ParameterBuilder_t2096_0_0_0,
};
static Il2CppInterfaceOffsetPair ParameterBuilder_t975_InterfacesOffsets[] = 
{
	{ &_ParameterBuilder_t2096_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParameterBuilder_t975_0_0_0;
extern const Il2CppType ParameterBuilder_t975_1_0_0;
struct ParameterBuilder_t975;
const Il2CppTypeDefinitionMetadata ParameterBuilder_t975_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ParameterBuilder_t975_InterfacesTypeInfos/* implementedInterfaces */
	, ParameterBuilder_t975_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ParameterBuilder_t975_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3840/* fieldStart */
	, 6057/* methodStart */
	, -1/* eventStart */
	, 1192/* propertyStart */

};
TypeInfo ParameterBuilder_t975_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParameterBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &ParameterBuilder_t975_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 935/* custom_attributes_cache */
	, &ParameterBuilder_t975_0_0_0/* byval_arg */
	, &ParameterBuilder_t975_1_0_0/* this_arg */
	, &ParameterBuilder_t975_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParameterBuilder_t975)/* instance_size */
	, sizeof (ParameterBuilder_t975)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Emit.PropertyBuilder
#include "mscorlib_System_Reflection_Emit_PropertyBuilder.h"
// Metadata Definition System.Reflection.Emit.PropertyBuilder
extern TypeInfo PropertyBuilder_t976_il2cpp_TypeInfo;
// System.Reflection.Emit.PropertyBuilder
#include "mscorlib_System_Reflection_Emit_PropertyBuilderMethodDeclarations.h"
static const EncodedMethodIndex PropertyBuilder_t976_VTable[28] = 
{
	120,
	125,
	122,
	123,
	2317,
	2318,
	2319,
	2320,
	2321,
	2322,
	2323,
	2318,
	2324,
	2317,
	2325,
	2326,
	2327,
	2328,
	2329,
	2330,
	2331,
	2332,
	2333,
	2334,
	2335,
	2336,
	2337,
	2338,
};
extern const Il2CppType _PropertyBuilder_t2097_0_0_0;
static const Il2CppType* PropertyBuilder_t976_InterfacesTypeInfos[] = 
{
	&_PropertyBuilder_t2097_0_0_0,
};
extern const Il2CppType _PropertyInfo_t2108_0_0_0;
static Il2CppInterfaceOffsetPair PropertyBuilder_t976_InterfacesOffsets[] = 
{
	{ &_PropertyInfo_t2108_0_0_0, 14},
	{ &ICustomAttributeProvider_t1404_0_0_0, 4},
	{ &_MemberInfo_t2064_0_0_0, 6},
	{ &_PropertyBuilder_t2097_0_0_0, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PropertyBuilder_t976_0_0_0;
extern const Il2CppType PropertyBuilder_t976_1_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
struct PropertyBuilder_t976;
const Il2CppTypeDefinitionMetadata PropertyBuilder_t976_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, PropertyBuilder_t976_InterfacesTypeInfos/* implementedInterfaces */
	, PropertyBuilder_t976_InterfacesOffsets/* interfaceOffsets */
	, &PropertyInfo_t_0_0_0/* parent */
	, PropertyBuilder_t976_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3843/* fieldStart */
	, 6060/* methodStart */
	, -1/* eventStart */
	, 1195/* propertyStart */

};
TypeInfo PropertyBuilder_t976_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &PropertyBuilder_t976_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 936/* custom_attributes_cache */
	, &PropertyBuilder_t976_0_0_0/* byval_arg */
	, &PropertyBuilder_t976_1_0_0/* this_arg */
	, &PropertyBuilder_t976_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyBuilder_t976)/* instance_size */
	, sizeof (PropertyBuilder_t976)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 20/* method_count */
	, 8/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
