﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t385;
// System.Runtime.Remoting.Messaging.CallContextRemotingData
struct CallContextRemotingData_t1092;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct  LogicalCallContext_t1093  : public Object_t
{
	// System.Collections.Hashtable System.Runtime.Remoting.Messaging.LogicalCallContext::_data
	Hashtable_t385 * ____data_0;
	// System.Runtime.Remoting.Messaging.CallContextRemotingData System.Runtime.Remoting.Messaging.LogicalCallContext::_remotingData
	CallContextRemotingData_t1092 * ____remotingData_1;
};
