﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// Metadata Definition System.Runtime.InteropServices._FieldBuilder
extern TypeInfo _FieldBuilder_t2092_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _FieldBuilder_t2092_0_0_0;
extern const Il2CppType _FieldBuilder_t2092_1_0_0;
struct _FieldBuilder_t2092;
const Il2CppTypeDefinitionMetadata _FieldBuilder_t2092_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _FieldBuilder_t2092_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_FieldBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_FieldBuilder_t2092_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1051/* custom_attributes_cache */
	, &_FieldBuilder_t2092_0_0_0/* byval_arg */
	, &_FieldBuilder_t2092_1_0_0/* this_arg */
	, &_FieldBuilder_t2092_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._FieldInfo
extern TypeInfo _FieldInfo_t2103_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _FieldInfo_t2103_0_0_0;
extern const Il2CppType _FieldInfo_t2103_1_0_0;
struct _FieldInfo_t2103;
const Il2CppTypeDefinitionMetadata _FieldInfo_t2103_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _FieldInfo_t2103_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_FieldInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_FieldInfo_t2103_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1052/* custom_attributes_cache */
	, &_FieldInfo_t2103_0_0_0/* byval_arg */
	, &_FieldInfo_t2103_1_0_0/* this_arg */
	, &_FieldInfo_t2103_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ILGenerator
extern TypeInfo _ILGenerator_t2093_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ILGenerator_t2093_0_0_0;
extern const Il2CppType _ILGenerator_t2093_1_0_0;
struct _ILGenerator_t2093;
const Il2CppTypeDefinitionMetadata _ILGenerator_t2093_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _ILGenerator_t2093_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ILGenerator"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_ILGenerator_t2093_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1053/* custom_attributes_cache */
	, &_ILGenerator_t2093_0_0_0/* byval_arg */
	, &_ILGenerator_t2093_1_0_0/* this_arg */
	, &_ILGenerator_t2093_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodBase
extern TypeInfo _MethodBase_t2104_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodBase_t2104_0_0_0;
extern const Il2CppType _MethodBase_t2104_1_0_0;
struct _MethodBase_t2104;
const Il2CppTypeDefinitionMetadata _MethodBase_t2104_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _MethodBase_t2104_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodBase"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_MethodBase_t2104_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1054/* custom_attributes_cache */
	, &_MethodBase_t2104_0_0_0/* byval_arg */
	, &_MethodBase_t2104_1_0_0/* this_arg */
	, &_MethodBase_t2104_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodBuilder
extern TypeInfo _MethodBuilder_t2094_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodBuilder_t2094_0_0_0;
extern const Il2CppType _MethodBuilder_t2094_1_0_0;
struct _MethodBuilder_t2094;
const Il2CppTypeDefinitionMetadata _MethodBuilder_t2094_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _MethodBuilder_t2094_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_MethodBuilder_t2094_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1055/* custom_attributes_cache */
	, &_MethodBuilder_t2094_0_0_0/* byval_arg */
	, &_MethodBuilder_t2094_1_0_0/* this_arg */
	, &_MethodBuilder_t2094_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodInfo
extern TypeInfo _MethodInfo_t2105_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodInfo_t2105_0_0_0;
extern const Il2CppType _MethodInfo_t2105_1_0_0;
struct _MethodInfo_t2105;
const Il2CppTypeDefinitionMetadata _MethodInfo_t2105_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _MethodInfo_t2105_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_MethodInfo_t2105_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1056/* custom_attributes_cache */
	, &_MethodInfo_t2105_0_0_0/* byval_arg */
	, &_MethodInfo_t2105_1_0_0/* this_arg */
	, &_MethodInfo_t2105_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Module
extern TypeInfo _Module_t2106_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Module_t2106_0_0_0;
extern const Il2CppType _Module_t2106_1_0_0;
struct _Module_t2106;
const Il2CppTypeDefinitionMetadata _Module_t2106_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Module_t2106_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Module"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_Module_t2106_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1057/* custom_attributes_cache */
	, &_Module_t2106_0_0_0/* byval_arg */
	, &_Module_t2106_1_0_0/* this_arg */
	, &_Module_t2106_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ModuleBuilder
extern TypeInfo _ModuleBuilder_t2095_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ModuleBuilder_t2095_0_0_0;
extern const Il2CppType _ModuleBuilder_t2095_1_0_0;
struct _ModuleBuilder_t2095;
const Il2CppTypeDefinitionMetadata _ModuleBuilder_t2095_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _ModuleBuilder_t2095_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ModuleBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_ModuleBuilder_t2095_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1058/* custom_attributes_cache */
	, &_ModuleBuilder_t2095_0_0_0/* byval_arg */
	, &_ModuleBuilder_t2095_1_0_0/* this_arg */
	, &_ModuleBuilder_t2095_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ParameterBuilder
extern TypeInfo _ParameterBuilder_t2096_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ParameterBuilder_t2096_0_0_0;
extern const Il2CppType _ParameterBuilder_t2096_1_0_0;
struct _ParameterBuilder_t2096;
const Il2CppTypeDefinitionMetadata _ParameterBuilder_t2096_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _ParameterBuilder_t2096_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ParameterBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_ParameterBuilder_t2096_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1059/* custom_attributes_cache */
	, &_ParameterBuilder_t2096_0_0_0/* byval_arg */
	, &_ParameterBuilder_t2096_1_0_0/* this_arg */
	, &_ParameterBuilder_t2096_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ParameterInfo
extern TypeInfo _ParameterInfo_t2107_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ParameterInfo_t2107_0_0_0;
extern const Il2CppType _ParameterInfo_t2107_1_0_0;
struct _ParameterInfo_t2107;
const Il2CppTypeDefinitionMetadata _ParameterInfo_t2107_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _ParameterInfo_t2107_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ParameterInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_ParameterInfo_t2107_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1060/* custom_attributes_cache */
	, &_ParameterInfo_t2107_0_0_0/* byval_arg */
	, &_ParameterInfo_t2107_1_0_0/* this_arg */
	, &_ParameterInfo_t2107_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._PropertyBuilder
extern TypeInfo _PropertyBuilder_t2097_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _PropertyBuilder_t2097_0_0_0;
extern const Il2CppType _PropertyBuilder_t2097_1_0_0;
struct _PropertyBuilder_t2097;
const Il2CppTypeDefinitionMetadata _PropertyBuilder_t2097_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _PropertyBuilder_t2097_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_PropertyBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_PropertyBuilder_t2097_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1061/* custom_attributes_cache */
	, &_PropertyBuilder_t2097_0_0_0/* byval_arg */
	, &_PropertyBuilder_t2097_1_0_0/* this_arg */
	, &_PropertyBuilder_t2097_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._PropertyInfo
extern TypeInfo _PropertyInfo_t2108_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _PropertyInfo_t2108_0_0_0;
extern const Il2CppType _PropertyInfo_t2108_1_0_0;
struct _PropertyInfo_t2108;
const Il2CppTypeDefinitionMetadata _PropertyInfo_t2108_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _PropertyInfo_t2108_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_PropertyInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_PropertyInfo_t2108_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1062/* custom_attributes_cache */
	, &_PropertyInfo_t2108_0_0_0/* byval_arg */
	, &_PropertyInfo_t2108_1_0_0/* this_arg */
	, &_PropertyInfo_t2108_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Thread
extern TypeInfo _Thread_t2110_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Thread_t2110_0_0_0;
extern const Il2CppType _Thread_t2110_1_0_0;
struct _Thread_t2110;
const Il2CppTypeDefinitionMetadata _Thread_t2110_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Thread_t2110_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Thread"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_Thread_t2110_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1063/* custom_attributes_cache */
	, &_Thread_t2110_0_0_0/* byval_arg */
	, &_Thread_t2110_1_0_0/* this_arg */
	, &_Thread_t2110_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._TypeBuilder
extern TypeInfo _TypeBuilder_t2098_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _TypeBuilder_t2098_0_0_0;
extern const Il2CppType _TypeBuilder_t2098_1_0_0;
struct _TypeBuilder_t2098;
const Il2CppTypeDefinitionMetadata _TypeBuilder_t2098_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _TypeBuilder_t2098_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_TypeBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_TypeBuilder_t2098_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1064/* custom_attributes_cache */
	, &_TypeBuilder_t2098_0_0_0/* byval_arg */
	, &_TypeBuilder_t2098_1_0_0/* this_arg */
	, &_TypeBuilder_t2098_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServic.h"
// Metadata Definition System.Runtime.Remoting.Activation.ActivationServices
extern TypeInfo ActivationServices_t1058_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServicMethodDeclarations.h"
static const EncodedMethodIndex ActivationServices_t1058_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivationServices_t1058_0_0_0;
extern const Il2CppType ActivationServices_t1058_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct ActivationServices_t1058;
const Il2CppTypeDefinitionMetadata ActivationServices_t1058_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ActivationServices_t1058_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4280/* fieldStart */
	, 6523/* methodStart */
	, -1/* eventStart */
	, 1311/* propertyStart */

};
TypeInfo ActivationServices_t1058_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivationServices"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &ActivationServices_t1058_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ActivationServices_t1058_0_0_0/* byval_arg */
	, &ActivationServices_t1058_1_0_0/* this_arg */
	, &ActivationServices_t1058_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivationServices_t1058)/* instance_size */
	, sizeof (ActivationServices_t1058)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ActivationServices_t1058_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAc.h"
// Metadata Definition System.Runtime.Remoting.Activation.AppDomainLevelActivator
extern TypeInfo AppDomainLevelActivator_t1059_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAcMethodDeclarations.h"
static const EncodedMethodIndex AppDomainLevelActivator_t1059_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern const Il2CppType IActivator_t1057_0_0_0;
static const Il2CppType* AppDomainLevelActivator_t1059_InterfacesTypeInfos[] = 
{
	&IActivator_t1057_0_0_0,
};
static Il2CppInterfaceOffsetPair AppDomainLevelActivator_t1059_InterfacesOffsets[] = 
{
	{ &IActivator_t1057_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainLevelActivator_t1059_0_0_0;
extern const Il2CppType AppDomainLevelActivator_t1059_1_0_0;
struct AppDomainLevelActivator_t1059;
const Il2CppTypeDefinitionMetadata AppDomainLevelActivator_t1059_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AppDomainLevelActivator_t1059_InterfacesTypeInfos/* implementedInterfaces */
	, AppDomainLevelActivator_t1059_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AppDomainLevelActivator_t1059_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4281/* fieldStart */
	, 6528/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AppDomainLevelActivator_t1059_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &AppDomainLevelActivator_t1059_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AppDomainLevelActivator_t1059_0_0_0/* byval_arg */
	, &AppDomainLevelActivator_t1059_1_0_0/* this_arg */
	, &AppDomainLevelActivator_t1059_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainLevelActivator_t1059)/* instance_size */
	, sizeof (AppDomainLevelActivator_t1059)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeve.h"
// Metadata Definition System.Runtime.Remoting.Activation.ConstructionLevelActivator
extern TypeInfo ConstructionLevelActivator_t1060_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeveMethodDeclarations.h"
static const EncodedMethodIndex ConstructionLevelActivator_t1060_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
static const Il2CppType* ConstructionLevelActivator_t1060_InterfacesTypeInfos[] = 
{
	&IActivator_t1057_0_0_0,
};
static Il2CppInterfaceOffsetPair ConstructionLevelActivator_t1060_InterfacesOffsets[] = 
{
	{ &IActivator_t1057_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionLevelActivator_t1060_0_0_0;
extern const Il2CppType ConstructionLevelActivator_t1060_1_0_0;
struct ConstructionLevelActivator_t1060;
const Il2CppTypeDefinitionMetadata ConstructionLevelActivator_t1060_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructionLevelActivator_t1060_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructionLevelActivator_t1060_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ConstructionLevelActivator_t1060_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6529/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConstructionLevelActivator_t1060_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &ConstructionLevelActivator_t1060_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructionLevelActivator_t1060_0_0_0/* byval_arg */
	, &ConstructionLevelActivator_t1060_1_0_0/* this_arg */
	, &ConstructionLevelActivator_t1060_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionLevelActivator_t1060)/* instance_size */
	, sizeof (ConstructionLevelActivator_t1060)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActi.h"
// Metadata Definition System.Runtime.Remoting.Activation.ContextLevelActivator
extern TypeInfo ContextLevelActivator_t1061_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActiMethodDeclarations.h"
static const EncodedMethodIndex ContextLevelActivator_t1061_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
static const Il2CppType* ContextLevelActivator_t1061_InterfacesTypeInfos[] = 
{
	&IActivator_t1057_0_0_0,
};
static Il2CppInterfaceOffsetPair ContextLevelActivator_t1061_InterfacesOffsets[] = 
{
	{ &IActivator_t1057_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContextLevelActivator_t1061_0_0_0;
extern const Il2CppType ContextLevelActivator_t1061_1_0_0;
struct ContextLevelActivator_t1061;
const Il2CppTypeDefinitionMetadata ContextLevelActivator_t1061_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContextLevelActivator_t1061_InterfacesTypeInfos/* implementedInterfaces */
	, ContextLevelActivator_t1061_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ContextLevelActivator_t1061_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4283/* fieldStart */
	, 6530/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ContextLevelActivator_t1061_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &ContextLevelActivator_t1061_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContextLevelActivator_t1061_0_0_0/* byval_arg */
	, &ContextLevelActivator_t1061_1_0_0/* this_arg */
	, &ContextLevelActivator_t1061_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextLevelActivator_t1061)/* instance_size */
	, sizeof (ContextLevelActivator_t1061)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Activation.IActivator
extern TypeInfo IActivator_t1057_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IActivator_t1057_1_0_0;
struct IActivator_t1057;
const Il2CppTypeDefinitionMetadata IActivator_t1057_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IActivator_t1057_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &IActivator_t1057_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1065/* custom_attributes_cache */
	, &IActivator_t1057_0_0_0/* byval_arg */
	, &IActivator_t1057_1_0_0/* this_arg */
	, &IActivator_t1057_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Activation.IConstructionCallMessage
extern TypeInfo IConstructionCallMessage_t1396_il2cpp_TypeInfo;
extern const Il2CppType IMessage_t1084_0_0_0;
extern const Il2CppType IMethodCallMessage_t1401_0_0_0;
extern const Il2CppType IMethodMessage_t1096_0_0_0;
static const Il2CppType* IConstructionCallMessage_t1396_InterfacesTypeInfos[] = 
{
	&IMessage_t1084_0_0_0,
	&IMethodCallMessage_t1401_0_0_0,
	&IMethodMessage_t1096_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IConstructionCallMessage_t1396_0_0_0;
extern const Il2CppType IConstructionCallMessage_t1396_1_0_0;
struct IConstructionCallMessage_t1396;
const Il2CppTypeDefinitionMetadata IConstructionCallMessage_t1396_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IConstructionCallMessage_t1396_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6531/* methodStart */
	, -1/* eventStart */
	, 1312/* propertyStart */

};
TypeInfo IConstructionCallMessage_t1396_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IConstructionCallMessage"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &IConstructionCallMessage_t1396_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1066/* custom_attributes_cache */
	, &IConstructionCallMessage_t1396_0_0_0/* byval_arg */
	, &IConstructionCallMessage_t1396_1_0_0/* this_arg */
	, &IConstructionCallMessage_t1396_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivator.h"
// Metadata Definition System.Runtime.Remoting.Activation.RemoteActivator
extern TypeInfo RemoteActivator_t1062_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivatorMethodDeclarations.h"
static const EncodedMethodIndex RemoteActivator_t1062_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
static const Il2CppType* RemoteActivator_t1062_InterfacesTypeInfos[] = 
{
	&IActivator_t1057_0_0_0,
};
static Il2CppInterfaceOffsetPair RemoteActivator_t1062_InterfacesOffsets[] = 
{
	{ &IActivator_t1057_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemoteActivator_t1062_0_0_0;
extern const Il2CppType RemoteActivator_t1062_1_0_0;
extern const Il2CppType MarshalByRefObject_t434_0_0_0;
struct RemoteActivator_t1062;
const Il2CppTypeDefinitionMetadata RemoteActivator_t1062_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemoteActivator_t1062_InterfacesTypeInfos/* implementedInterfaces */
	, RemoteActivator_t1062_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t434_0_0_0/* parent */
	, RemoteActivator_t1062_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RemoteActivator_t1062_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemoteActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &RemoteActivator_t1062_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemoteActivator_t1062_0_0_0/* byval_arg */
	, &RemoteActivator_t1062_1_0_0/* this_arg */
	, &RemoteActivator_t1062_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemoteActivator_t1062)/* instance_size */
	, sizeof (RemoteActivator_t1062)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttribute.h"
// Metadata Definition System.Runtime.Remoting.Activation.UrlAttribute
extern TypeInfo UrlAttribute_t1063_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttributeMethodDeclarations.h"
static const EncodedMethodIndex UrlAttribute_t1063_VTable[10] = 
{
	2520,
	125,
	2521,
	123,
	2522,
	2523,
	2524,
	2524,
	2522,
	2523,
};
extern const Il2CppType IContextAttribute_t1413_0_0_0;
extern const Il2CppType IContextProperty_t1399_0_0_0;
extern const Il2CppType _Attribute_t2052_0_0_0;
static Il2CppInterfaceOffsetPair UrlAttribute_t1063_InterfacesOffsets[] = 
{
	{ &IContextAttribute_t1413_0_0_0, 4},
	{ &IContextProperty_t1399_0_0_0, 6},
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UrlAttribute_t1063_0_0_0;
extern const Il2CppType UrlAttribute_t1063_1_0_0;
extern const Il2CppType ContextAttribute_t1064_0_0_0;
struct UrlAttribute_t1063;
const Il2CppTypeDefinitionMetadata UrlAttribute_t1063_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UrlAttribute_t1063_InterfacesOffsets/* interfaceOffsets */
	, &ContextAttribute_t1064_0_0_0/* parent */
	, UrlAttribute_t1063_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4284/* fieldStart */
	, 6537/* methodStart */
	, -1/* eventStart */
	, 1317/* propertyStart */

};
TypeInfo UrlAttribute_t1063_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UrlAttribute"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &UrlAttribute_t1063_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1067/* custom_attributes_cache */
	, &UrlAttribute_t1063_0_0_0/* byval_arg */
	, &UrlAttribute_t1063_1_0_0/* this_arg */
	, &UrlAttribute_t1063_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UrlAttribute_t1063)/* instance_size */
	, sizeof (UrlAttribute_t1063)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfo.h"
// Metadata Definition System.Runtime.Remoting.ChannelInfo
extern TypeInfo ChannelInfo_t1065_il2cpp_TypeInfo;
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfoMethodDeclarations.h"
static const EncodedMethodIndex ChannelInfo_t1065_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2525,
};
extern const Il2CppType IChannelInfo_t1123_0_0_0;
static const Il2CppType* ChannelInfo_t1065_InterfacesTypeInfos[] = 
{
	&IChannelInfo_t1123_0_0_0,
};
static Il2CppInterfaceOffsetPair ChannelInfo_t1065_InterfacesOffsets[] = 
{
	{ &IChannelInfo_t1123_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ChannelInfo_t1065_0_0_0;
extern const Il2CppType ChannelInfo_t1065_1_0_0;
struct ChannelInfo_t1065;
const Il2CppTypeDefinitionMetadata ChannelInfo_t1065_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ChannelInfo_t1065_InterfacesTypeInfos/* implementedInterfaces */
	, ChannelInfo_t1065_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelInfo_t1065_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4285/* fieldStart */
	, 6542/* methodStart */
	, -1/* eventStart */
	, 1318/* propertyStart */

};
TypeInfo ChannelInfo_t1065_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ChannelInfo_t1065_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ChannelInfo_t1065_0_0_0/* byval_arg */
	, &ChannelInfo_t1065_1_0_0/* this_arg */
	, &ChannelInfo_t1065_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelInfo_t1065)/* instance_size */
	, sizeof (ChannelInfo_t1065)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServices.h"
// Metadata Definition System.Runtime.Remoting.Channels.ChannelServices
extern TypeInfo ChannelServices_t1067_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServicesMethodDeclarations.h"
static const EncodedMethodIndex ChannelServices_t1067_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ChannelServices_t1067_0_0_0;
extern const Il2CppType ChannelServices_t1067_1_0_0;
struct ChannelServices_t1067;
const Il2CppTypeDefinitionMetadata ChannelServices_t1067_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelServices_t1067_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4286/* fieldStart */
	, 6544/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ChannelServices_t1067_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelServices"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &ChannelServices_t1067_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1070/* custom_attributes_cache */
	, &ChannelServices_t1067_0_0_0/* byval_arg */
	, &ChannelServices_t1067_1_0_0/* this_arg */
	, &ChannelServices_t1067_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelServices_t1067)/* instance_size */
	, sizeof (ChannelServices_t1067)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ChannelServices_t1067_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainData.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainData
extern TypeInfo CrossAppDomainData_t1068_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainDataMethodDeclarations.h"
static const EncodedMethodIndex CrossAppDomainData_t1068_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainData_t1068_0_0_0;
extern const Il2CppType CrossAppDomainData_t1068_1_0_0;
struct CrossAppDomainData_t1068;
const Il2CppTypeDefinitionMetadata CrossAppDomainData_t1068_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainData_t1068_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4291/* fieldStart */
	, 6552/* methodStart */
	, -1/* eventStart */
	, 1319/* propertyStart */

};
TypeInfo CrossAppDomainData_t1068_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainData"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &CrossAppDomainData_t1068_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossAppDomainData_t1068_0_0_0/* byval_arg */
	, &CrossAppDomainData_t1068_1_0_0/* this_arg */
	, &CrossAppDomainData_t1068_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainData_t1068)/* instance_size */
	, sizeof (CrossAppDomainData_t1068)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChan.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainChannel
extern TypeInfo CrossAppDomainChannel_t1069_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChanMethodDeclarations.h"
static const EncodedMethodIndex CrossAppDomainChannel_t1069_VTable[14] = 
{
	120,
	125,
	122,
	123,
	2526,
	2527,
	2528,
	2529,
	2530,
	2526,
	2527,
	2528,
	2529,
	2530,
};
extern const Il2CppType IChannel_t1398_0_0_0;
extern const Il2CppType IChannelReceiver_t1416_0_0_0;
extern const Il2CppType IChannelSender_t1397_0_0_0;
static const Il2CppType* CrossAppDomainChannel_t1069_InterfacesTypeInfos[] = 
{
	&IChannel_t1398_0_0_0,
	&IChannelReceiver_t1416_0_0_0,
	&IChannelSender_t1397_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossAppDomainChannel_t1069_InterfacesOffsets[] = 
{
	{ &IChannel_t1398_0_0_0, 4},
	{ &IChannelReceiver_t1416_0_0_0, 6},
	{ &IChannelSender_t1397_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainChannel_t1069_0_0_0;
extern const Il2CppType CrossAppDomainChannel_t1069_1_0_0;
struct CrossAppDomainChannel_t1069;
const Il2CppTypeDefinitionMetadata CrossAppDomainChannel_t1069_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossAppDomainChannel_t1069_InterfacesTypeInfos/* implementedInterfaces */
	, CrossAppDomainChannel_t1069_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainChannel_t1069_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4294/* fieldStart */
	, 6555/* methodStart */
	, -1/* eventStart */
	, 1321/* propertyStart */

};
TypeInfo CrossAppDomainChannel_t1069_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &CrossAppDomainChannel_t1069_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossAppDomainChannel_t1069_0_0_0/* byval_arg */
	, &CrossAppDomainChannel_t1069_1_0_0/* this_arg */
	, &CrossAppDomainChannel_t1069_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainChannel_t1069)/* instance_size */
	, sizeof (CrossAppDomainChannel_t1069)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossAppDomainChannel_t1069_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSink.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainSink
extern TypeInfo CrossAppDomainSink_t1070_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSinkMethodDeclarations.h"
static const EncodedMethodIndex CrossAppDomainSink_t1070_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern const Il2CppType IMessageSink_t1112_0_0_0;
static const Il2CppType* CrossAppDomainSink_t1070_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1112_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossAppDomainSink_t1070_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1112_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainSink_t1070_0_0_0;
extern const Il2CppType CrossAppDomainSink_t1070_1_0_0;
struct CrossAppDomainSink_t1070;
const Il2CppTypeDefinitionMetadata CrossAppDomainSink_t1070_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossAppDomainSink_t1070_InterfacesTypeInfos/* implementedInterfaces */
	, CrossAppDomainSink_t1070_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainSink_t1070_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4295/* fieldStart */
	, 6563/* methodStart */
	, -1/* eventStart */
	, 1324/* propertyStart */

};
TypeInfo CrossAppDomainSink_t1070_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainSink"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &CrossAppDomainSink_t1070_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1072/* custom_attributes_cache */
	, &CrossAppDomainSink_t1070_0_0_0/* byval_arg */
	, &CrossAppDomainSink_t1070_1_0_0/* this_arg */
	, &CrossAppDomainSink_t1070_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainSink_t1070)/* instance_size */
	, sizeof (CrossAppDomainSink_t1070)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossAppDomainSink_t1070_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannel
extern TypeInfo IChannel_t1398_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannel_t1398_1_0_0;
struct IChannel_t1398;
const Il2CppTypeDefinitionMetadata IChannel_t1398_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6567/* methodStart */
	, -1/* eventStart */
	, 1325/* propertyStart */

};
TypeInfo IChannel_t1398_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &IChannel_t1398_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1073/* custom_attributes_cache */
	, &IChannel_t1398_0_0_0/* byval_arg */
	, &IChannel_t1398_1_0_0/* this_arg */
	, &IChannel_t1398_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelDataStore
extern TypeInfo IChannelDataStore_t1414_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelDataStore_t1414_0_0_0;
extern const Il2CppType IChannelDataStore_t1414_1_0_0;
struct IChannelDataStore_t1414;
const Il2CppTypeDefinitionMetadata IChannelDataStore_t1414_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IChannelDataStore_t1414_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelDataStore"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &IChannelDataStore_t1414_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1074/* custom_attributes_cache */
	, &IChannelDataStore_t1414_0_0_0/* byval_arg */
	, &IChannelDataStore_t1414_1_0_0/* this_arg */
	, &IChannelDataStore_t1414_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelReceiver
extern TypeInfo IChannelReceiver_t1416_il2cpp_TypeInfo;
static const Il2CppType* IChannelReceiver_t1416_InterfacesTypeInfos[] = 
{
	&IChannel_t1398_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelReceiver_t1416_1_0_0;
struct IChannelReceiver_t1416;
const Il2CppTypeDefinitionMetadata IChannelReceiver_t1416_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IChannelReceiver_t1416_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6569/* methodStart */
	, -1/* eventStart */
	, 1327/* propertyStart */

};
TypeInfo IChannelReceiver_t1416_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelReceiver"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &IChannelReceiver_t1416_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1075/* custom_attributes_cache */
	, &IChannelReceiver_t1416_0_0_0/* byval_arg */
	, &IChannelReceiver_t1416_1_0_0/* this_arg */
	, &IChannelReceiver_t1416_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelSender
extern TypeInfo IChannelSender_t1397_il2cpp_TypeInfo;
static const Il2CppType* IChannelSender_t1397_InterfacesTypeInfos[] = 
{
	&IChannel_t1398_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelSender_t1397_1_0_0;
struct IChannelSender_t1397;
const Il2CppTypeDefinitionMetadata IChannelSender_t1397_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IChannelSender_t1397_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6571/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IChannelSender_t1397_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelSender"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &IChannelSender_t1397_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1076/* custom_attributes_cache */
	, &IChannelSender_t1397_0_0_0/* byval_arg */
	, &IChannelSender_t1397_1_0_0/* this_arg */
	, &IChannelSender_t1397_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IClientChannelSinkProvider
extern TypeInfo IClientChannelSinkProvider_t1418_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IClientChannelSinkProvider_t1418_0_0_0;
extern const Il2CppType IClientChannelSinkProvider_t1418_1_0_0;
struct IClientChannelSinkProvider_t1418;
const Il2CppTypeDefinitionMetadata IClientChannelSinkProvider_t1418_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6572/* methodStart */
	, -1/* eventStart */
	, 1328/* propertyStart */

};
TypeInfo IClientChannelSinkProvider_t1418_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IClientChannelSinkProvider"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &IClientChannelSinkProvider_t1418_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1077/* custom_attributes_cache */
	, &IClientChannelSinkProvider_t1418_0_0_0/* byval_arg */
	, &IClientChannelSinkProvider_t1418_1_0_0/* this_arg */
	, &IClientChannelSinkProvider_t1418_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.ISecurableChannel
extern TypeInfo ISecurableChannel_t1415_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISecurableChannel_t1415_0_0_0;
extern const Il2CppType ISecurableChannel_t1415_1_0_0;
struct ISecurableChannel_t1415;
const Il2CppTypeDefinitionMetadata ISecurableChannel_t1415_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6573/* methodStart */
	, -1/* eventStart */
	, 1329/* propertyStart */

};
TypeInfo ISecurableChannel_t1415_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISecurableChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &ISecurableChannel_t1415_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISecurableChannel_t1415_0_0_0/* byval_arg */
	, &ISecurableChannel_t1415_1_0_0/* this_arg */
	, &ISecurableChannel_t1415_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IServerChannelSinkProvider
extern TypeInfo IServerChannelSinkProvider_t1417_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IServerChannelSinkProvider_t1417_0_0_0;
extern const Il2CppType IServerChannelSinkProvider_t1417_1_0_0;
struct IServerChannelSinkProvider_t1417;
const Il2CppTypeDefinitionMetadata IServerChannelSinkProvider_t1417_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6574/* methodStart */
	, -1/* eventStart */
	, 1330/* propertyStart */

};
TypeInfo IServerChannelSinkProvider_t1417_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IServerChannelSinkProvider"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &IServerChannelSinkProvider_t1417_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1078/* custom_attributes_cache */
	, &IServerChannelSinkProvider_t1417_0_0_0/* byval_arg */
	, &IServerChannelSinkProvider_t1417_1_0_0/* this_arg */
	, &IServerChannelSinkProvider_t1417_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.SinkProviderData
#include "mscorlib_System_Runtime_Remoting_Channels_SinkProviderData.h"
// Metadata Definition System.Runtime.Remoting.Channels.SinkProviderData
extern TypeInfo SinkProviderData_t1071_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.SinkProviderData
#include "mscorlib_System_Runtime_Remoting_Channels_SinkProviderDataMethodDeclarations.h"
static const EncodedMethodIndex SinkProviderData_t1071_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SinkProviderData_t1071_0_0_0;
extern const Il2CppType SinkProviderData_t1071_1_0_0;
struct SinkProviderData_t1071;
const Il2CppTypeDefinitionMetadata SinkProviderData_t1071_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SinkProviderData_t1071_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4298/* fieldStart */
	, 6575/* methodStart */
	, -1/* eventStart */
	, 1331/* propertyStart */

};
TypeInfo SinkProviderData_t1071_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SinkProviderData"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &SinkProviderData_t1071_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1079/* custom_attributes_cache */
	, &SinkProviderData_t1071_0_0_0/* byval_arg */
	, &SinkProviderData_t1071_1_0_0/* this_arg */
	, &SinkProviderData_t1071_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SinkProviderData_t1071)/* instance_size */
	, sizeof (SinkProviderData_t1071)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_Context.h"
// Metadata Definition System.Runtime.Remoting.Contexts.Context
extern TypeInfo Context_t1072_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextMethodDeclarations.h"
static const EncodedMethodIndex Context_t1072_VTable[5] = 
{
	120,
	2531,
	122,
	2532,
	2533,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Context_t1072_0_0_0;
extern const Il2CppType Context_t1072_1_0_0;
struct Context_t1072;
const Il2CppTypeDefinitionMetadata Context_t1072_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Context_t1072_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4301/* fieldStart */
	, 6578/* methodStart */
	, -1/* eventStart */
	, 1333/* propertyStart */

};
TypeInfo Context_t1072_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Context"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &Context_t1072_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1080/* custom_attributes_cache */
	, &Context_t1072_0_0_0/* byval_arg */
	, &Context_t1072_1_0_0/* this_arg */
	, &Context_t1072_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Context_t1072)/* instance_size */
	, sizeof (Context_t1072)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Context_t1072_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttribute.h"
// Metadata Definition System.Runtime.Remoting.Contexts.ContextAttribute
extern TypeInfo ContextAttribute_t1064_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttributeMethodDeclarations.h"
static const EncodedMethodIndex ContextAttribute_t1064_VTable[10] = 
{
	2534,
	125,
	2535,
	123,
	2536,
	2537,
	2524,
	2524,
	2536,
	2537,
};
static const Il2CppType* ContextAttribute_t1064_InterfacesTypeInfos[] = 
{
	&IContextAttribute_t1413_0_0_0,
	&IContextProperty_t1399_0_0_0,
};
static Il2CppInterfaceOffsetPair ContextAttribute_t1064_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
	{ &IContextAttribute_t1413_0_0_0, 4},
	{ &IContextProperty_t1399_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContextAttribute_t1064_1_0_0;
extern const Il2CppType Attribute_t96_0_0_0;
struct ContextAttribute_t1064;
const Il2CppTypeDefinitionMetadata ContextAttribute_t1064_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContextAttribute_t1064_InterfacesTypeInfos/* implementedInterfaces */
	, ContextAttribute_t1064_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, ContextAttribute_t1064_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4304/* fieldStart */
	, 6584/* methodStart */
	, -1/* eventStart */
	, 1335/* propertyStart */

};
TypeInfo ContextAttribute_t1064_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &ContextAttribute_t1064_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1081/* custom_attributes_cache */
	, &ContextAttribute_t1064_0_0_0/* byval_arg */
	, &ContextAttribute_t1064_1_0_0/* this_arg */
	, &ContextAttribute_t1064_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextAttribute_t1064)/* instance_size */
	, sizeof (ContextAttribute_t1064)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanne.h"
// Metadata Definition System.Runtime.Remoting.Contexts.CrossContextChannel
extern TypeInfo CrossContextChannel_t1066_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanneMethodDeclarations.h"
static const EncodedMethodIndex CrossContextChannel_t1066_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
static const Il2CppType* CrossContextChannel_t1066_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1112_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossContextChannel_t1066_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1112_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossContextChannel_t1066_0_0_0;
extern const Il2CppType CrossContextChannel_t1066_1_0_0;
struct CrossContextChannel_t1066;
const Il2CppTypeDefinitionMetadata CrossContextChannel_t1066_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossContextChannel_t1066_InterfacesTypeInfos/* implementedInterfaces */
	, CrossContextChannel_t1066_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossContextChannel_t1066_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6590/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CrossContextChannel_t1066_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossContextChannel"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &CrossContextChannel_t1066_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossContextChannel_t1066_0_0_0/* byval_arg */
	, &CrossContextChannel_t1066_1_0_0/* this_arg */
	, &CrossContextChannel_t1066_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossContextChannel_t1066)/* instance_size */
	, sizeof (CrossContextChannel_t1066)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContextAttribute
extern TypeInfo IContextAttribute_t1413_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContextAttribute_t1413_1_0_0;
struct IContextAttribute_t1413;
const Il2CppTypeDefinitionMetadata IContextAttribute_t1413_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6591/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IContextAttribute_t1413_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContextAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &IContextAttribute_t1413_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1082/* custom_attributes_cache */
	, &IContextAttribute_t1413_0_0_0/* byval_arg */
	, &IContextAttribute_t1413_1_0_0/* this_arg */
	, &IContextAttribute_t1413_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContextProperty
extern TypeInfo IContextProperty_t1399_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContextProperty_t1399_1_0_0;
struct IContextProperty_t1399;
const Il2CppTypeDefinitionMetadata IContextProperty_t1399_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6593/* methodStart */
	, -1/* eventStart */
	, 1336/* propertyStart */

};
TypeInfo IContextProperty_t1399_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContextProperty"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &IContextProperty_t1399_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1083/* custom_attributes_cache */
	, &IContextProperty_t1399_0_0_0/* byval_arg */
	, &IContextProperty_t1399_1_0_0/* this_arg */
	, &IContextProperty_t1399_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeClientContextSink
extern TypeInfo IContributeClientContextSink_t2111_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContributeClientContextSink_t2111_0_0_0;
extern const Il2CppType IContributeClientContextSink_t2111_1_0_0;
struct IContributeClientContextSink_t2111;
const Il2CppTypeDefinitionMetadata IContributeClientContextSink_t2111_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IContributeClientContextSink_t2111_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeClientContextSink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &IContributeClientContextSink_t2111_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1084/* custom_attributes_cache */
	, &IContributeClientContextSink_t2111_0_0_0/* byval_arg */
	, &IContributeClientContextSink_t2111_1_0_0/* this_arg */
	, &IContributeClientContextSink_t2111_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeServerContextSink
extern TypeInfo IContributeServerContextSink_t2112_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContributeServerContextSink_t2112_0_0_0;
extern const Il2CppType IContributeServerContextSink_t2112_1_0_0;
struct IContributeServerContextSink_t2112;
const Il2CppTypeDefinitionMetadata IContributeServerContextSink_t2112_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IContributeServerContextSink_t2112_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeServerContextSink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &IContributeServerContextSink_t2112_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1085/* custom_attributes_cache */
	, &IContributeServerContextSink_t2112_0_0_0/* byval_arg */
	, &IContributeServerContextSink_t2112_1_0_0/* this_arg */
	, &IContributeServerContextSink_t2112_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAtt.h"
// Metadata Definition System.Runtime.Remoting.Contexts.SynchronizationAttribute
extern TypeInfo SynchronizationAttribute_t1075_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAttMethodDeclarations.h"
static const EncodedMethodIndex SynchronizationAttribute_t1075_VTable[11] = 
{
	2534,
	125,
	2535,
	123,
	2538,
	2539,
	2524,
	2524,
	2538,
	2539,
	2540,
};
static const Il2CppType* SynchronizationAttribute_t1075_InterfacesTypeInfos[] = 
{
	&IContributeClientContextSink_t2111_0_0_0,
	&IContributeServerContextSink_t2112_0_0_0,
};
static Il2CppInterfaceOffsetPair SynchronizationAttribute_t1075_InterfacesOffsets[] = 
{
	{ &IContextAttribute_t1413_0_0_0, 4},
	{ &IContextProperty_t1399_0_0_0, 6},
	{ &_Attribute_t2052_0_0_0, 4},
	{ &IContributeClientContextSink_t2111_0_0_0, 10},
	{ &IContributeServerContextSink_t2112_0_0_0, 10},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizationAttribute_t1075_0_0_0;
extern const Il2CppType SynchronizationAttribute_t1075_1_0_0;
struct SynchronizationAttribute_t1075;
const Il2CppTypeDefinitionMetadata SynchronizationAttribute_t1075_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SynchronizationAttribute_t1075_InterfacesTypeInfos/* implementedInterfaces */
	, SynchronizationAttribute_t1075_InterfacesOffsets/* interfaceOffsets */
	, &ContextAttribute_t1064_0_0_0/* parent */
	, SynchronizationAttribute_t1075_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4305/* fieldStart */
	, 6594/* methodStart */
	, -1/* eventStart */
	, 1337/* propertyStart */

};
TypeInfo SynchronizationAttribute_t1075_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizationAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &SynchronizationAttribute_t1075_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1086/* custom_attributes_cache */
	, &SynchronizationAttribute_t1075_0_0_0/* byval_arg */
	, &SynchronizationAttribute_t1075_1_0_0/* this_arg */
	, &SynchronizationAttribute_t1075_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizationAttribute_t1075)/* instance_size */
	, sizeof (SynchronizationAttribute_t1075)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 2/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Runtime.Remoting.Lifetime.LeaseManager
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseManager.h"
// Metadata Definition System.Runtime.Remoting.Lifetime.LeaseManager
extern TypeInfo LeaseManager_t1077_il2cpp_TypeInfo;
// System.Runtime.Remoting.Lifetime.LeaseManager
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseManagerMethodDeclarations.h"
static const EncodedMethodIndex LeaseManager_t1077_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LeaseManager_t1077_0_0_0;
extern const Il2CppType LeaseManager_t1077_1_0_0;
struct LeaseManager_t1077;
const Il2CppTypeDefinitionMetadata LeaseManager_t1077_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LeaseManager_t1077_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4310/* fieldStart */
	, 6602/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LeaseManager_t1077_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LeaseManager"/* name */
	, "System.Runtime.Remoting.Lifetime"/* namespaze */
	, NULL/* methods */
	, &LeaseManager_t1077_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LeaseManager_t1077_0_0_0/* byval_arg */
	, &LeaseManager_t1077_1_0_0/* this_arg */
	, &LeaseManager_t1077_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LeaseManager_t1077)/* instance_size */
	, sizeof (LeaseManager_t1077)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Lifetime.LifetimeServices
#include "mscorlib_System_Runtime_Remoting_Lifetime_LifetimeServices.h"
// Metadata Definition System.Runtime.Remoting.Lifetime.LifetimeServices
extern TypeInfo LifetimeServices_t1078_il2cpp_TypeInfo;
// System.Runtime.Remoting.Lifetime.LifetimeServices
#include "mscorlib_System_Runtime_Remoting_Lifetime_LifetimeServicesMethodDeclarations.h"
static const EncodedMethodIndex LifetimeServices_t1078_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LifetimeServices_t1078_0_0_0;
extern const Il2CppType LifetimeServices_t1078_1_0_0;
struct LifetimeServices_t1078;
const Il2CppTypeDefinitionMetadata LifetimeServices_t1078_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LifetimeServices_t1078_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4312/* fieldStart */
	, 6604/* methodStart */
	, -1/* eventStart */
	, 1338/* propertyStart */

};
TypeInfo LifetimeServices_t1078_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LifetimeServices"/* name */
	, "System.Runtime.Remoting.Lifetime"/* namespaze */
	, NULL/* methods */
	, &LifetimeServices_t1078_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1089/* custom_attributes_cache */
	, &LifetimeServices_t1078_0_0_0/* byval_arg */
	, &LifetimeServices_t1078_1_0_0/* this_arg */
	, &LifetimeServices_t1078_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LifetimeServices_t1078)/* instance_size */
	, sizeof (LifetimeServices_t1078)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LifetimeServices_t1078_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoType.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfoType
extern TypeInfo ArgInfoType_t1079_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoTypeMethodDeclarations.h"
static const EncodedMethodIndex ArgInfoType_t1079_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
extern const Il2CppType IFormattable_t1406_0_0_0;
extern const Il2CppType IConvertible_t1409_0_0_0;
extern const Il2CppType IComparable_t1408_0_0_0;
static Il2CppInterfaceOffsetPair ArgInfoType_t1079_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgInfoType_t1079_0_0_0;
extern const Il2CppType ArgInfoType_t1079_1_0_0;
extern const Il2CppType Enum_t305_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t326_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata ArgInfoType_t1079_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArgInfoType_t1079_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, ArgInfoType_t1079_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4317/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArgInfoType_t1079_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgInfoType"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &Byte_t326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgInfoType_t1079_0_0_0/* byval_arg */
	, &ArgInfoType_t1079_1_0_0/* this_arg */
	, &ArgInfoType_t1079_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgInfoType_t1079)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ArgInfoType_t1079)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfo
extern TypeInfo ArgInfo_t1080_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoMethodDeclarations.h"
static const EncodedMethodIndex ArgInfo_t1080_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgInfo_t1080_0_0_0;
extern const Il2CppType ArgInfo_t1080_1_0_0;
struct ArgInfo_t1080;
const Il2CppTypeDefinitionMetadata ArgInfo_t1080_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgInfo_t1080_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4320/* fieldStart */
	, 6609/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArgInfo_t1080_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgInfo"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &ArgInfo_t1080_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgInfo_t1080_0_0_0/* byval_arg */
	, &ArgInfo_t1080_1_0_0/* this_arg */
	, &ArgInfo_t1080_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgInfo_t1080)/* instance_size */
	, sizeof (ArgInfo_t1080)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResult.h"
// Metadata Definition System.Runtime.Remoting.Messaging.AsyncResult
extern TypeInfo AsyncResult_t1085_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResultMethodDeclarations.h"
static const EncodedMethodIndex AsyncResult_t1085_VTable[17] = 
{
	120,
	125,
	122,
	123,
	2541,
	2542,
	2543,
	2541,
	2542,
	2544,
	2543,
	2545,
	2546,
	2547,
	2548,
	2549,
	2550,
};
extern const Il2CppType IAsyncResult_t44_0_0_0;
static const Il2CppType* AsyncResult_t1085_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t44_0_0_0,
	&IMessageSink_t1112_0_0_0,
};
static Il2CppInterfaceOffsetPair AsyncResult_t1085_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t44_0_0_0, 4},
	{ &IMessageSink_t1112_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsyncResult_t1085_0_0_0;
extern const Il2CppType AsyncResult_t1085_1_0_0;
struct AsyncResult_t1085;
const Il2CppTypeDefinitionMetadata AsyncResult_t1085_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AsyncResult_t1085_InterfacesTypeInfos/* implementedInterfaces */
	, AsyncResult_t1085_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsyncResult_t1085_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4323/* fieldStart */
	, 6611/* methodStart */
	, -1/* eventStart */
	, 1342/* propertyStart */

};
TypeInfo AsyncResult_t1085_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsyncResult"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &AsyncResult_t1085_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1090/* custom_attributes_cache */
	, &AsyncResult_t1085_0_0_0/* byval_arg */
	, &AsyncResult_t1085_1_0_0/* this_arg */
	, &AsyncResult_t1085_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsyncResult_t1085)/* instance_size */
	, sizeof (AsyncResult_t1085)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 8/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCall.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCall
extern TypeInfo ConstructionCall_t1086_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallMethodDeclarations.h"
static const EncodedMethodIndex ConstructionCall_t1086_VTable[25] = 
{
	120,
	125,
	122,
	123,
	2551,
	2552,
	2553,
	2554,
	2555,
	2556,
	2557,
	2558,
	2559,
	2560,
	2551,
	2561,
	2562,
	2563,
	2564,
	2565,
	2566,
	2567,
	2568,
	2569,
	2570,
};
static const Il2CppType* ConstructionCall_t1086_InterfacesTypeInfos[] = 
{
	&IConstructionCallMessage_t1396_0_0_0,
	&IMessage_t1084_0_0_0,
	&IMethodCallMessage_t1401_0_0_0,
	&IMethodMessage_t1096_0_0_0,
};
extern const Il2CppType ISerializable_t1426_0_0_0;
extern const Il2CppType IInternalMessage_t1419_0_0_0;
extern const Il2CppType ISerializationRootObject_t2114_0_0_0;
static Il2CppInterfaceOffsetPair ConstructionCall_t1086_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &IInternalMessage_t1419_0_0_0, 5},
	{ &IMessage_t1084_0_0_0, 6},
	{ &IMethodCallMessage_t1401_0_0_0, 6},
	{ &IMethodMessage_t1096_0_0_0, 6},
	{ &ISerializationRootObject_t2114_0_0_0, 13},
	{ &IConstructionCallMessage_t1396_0_0_0, 19},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionCall_t1086_0_0_0;
extern const Il2CppType ConstructionCall_t1086_1_0_0;
extern const Il2CppType MethodCall_t1087_0_0_0;
struct ConstructionCall_t1086;
const Il2CppTypeDefinitionMetadata ConstructionCall_t1086_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructionCall_t1086_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructionCall_t1086_InterfacesOffsets/* interfaceOffsets */
	, &MethodCall_t1087_0_0_0/* parent */
	, ConstructionCall_t1086_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4338/* fieldStart */
	, 6628/* methodStart */
	, -1/* eventStart */
	, 1350/* propertyStart */

};
TypeInfo ConstructionCall_t1086_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionCall"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &ConstructionCall_t1086_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1091/* custom_attributes_cache */
	, &ConstructionCall_t1086_0_0_0/* byval_arg */
	, &ConstructionCall_t1086_1_0_0/* this_arg */
	, &ConstructionCall_t1086_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionCall_t1086)/* instance_size */
	, sizeof (ConstructionCall_t1086)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructionCall_t1086_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 4/* interfaces_count */
	, 7/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallD.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCallDictionary
extern TypeInfo ConstructionCallDictionary_t1088_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallDMethodDeclarations.h"
static const EncodedMethodIndex ConstructionCallDictionary_t1088_VTable[18] = 
{
	120,
	125,
	122,
	123,
	2571,
	2572,
	2573,
	2574,
	2575,
	2576,
	2577,
	2578,
	2579,
	2580,
	2581,
	2582,
	2583,
	2584,
};
extern const Il2CppType IEnumerable_t302_0_0_0;
extern const Il2CppType ICollection_t569_0_0_0;
extern const Il2CppType IDictionary_t493_0_0_0;
static Il2CppInterfaceOffsetPair ConstructionCallDictionary_t1088_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IDictionary_t493_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionCallDictionary_t1088_0_0_0;
extern const Il2CppType ConstructionCallDictionary_t1088_1_0_0;
extern const Il2CppType MethodDictionary_t1089_0_0_0;
struct ConstructionCallDictionary_t1088;
const Il2CppTypeDefinitionMetadata ConstructionCallDictionary_t1088_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConstructionCallDictionary_t1088_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t1089_0_0_0/* parent */
	, ConstructionCallDictionary_t1088_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4345/* fieldStart */
	, 6642/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConstructionCallDictionary_t1088_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionCallDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &ConstructionCallDictionary_t1088_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructionCallDictionary_t1088_0_0_0/* byval_arg */
	, &ConstructionCallDictionary_t1088_1_0_0/* this_arg */
	, &ConstructionCallDictionary_t1088_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionCallDictionary_t1088)/* instance_size */
	, sizeof (ConstructionCallDictionary_t1088)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructionCallDictionary_t1088_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSi.h"
// Metadata Definition System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
extern TypeInfo EnvoyTerminatorSink_t1090_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSiMethodDeclarations.h"
static const EncodedMethodIndex EnvoyTerminatorSink_t1090_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
static const Il2CppType* EnvoyTerminatorSink_t1090_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1112_0_0_0,
};
static Il2CppInterfaceOffsetPair EnvoyTerminatorSink_t1090_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1112_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnvoyTerminatorSink_t1090_0_0_0;
extern const Il2CppType EnvoyTerminatorSink_t1090_1_0_0;
struct EnvoyTerminatorSink_t1090;
const Il2CppTypeDefinitionMetadata EnvoyTerminatorSink_t1090_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnvoyTerminatorSink_t1090_InterfacesTypeInfos/* implementedInterfaces */
	, EnvoyTerminatorSink_t1090_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EnvoyTerminatorSink_t1090_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4348/* fieldStart */
	, 6646/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EnvoyTerminatorSink_t1090_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnvoyTerminatorSink"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &EnvoyTerminatorSink_t1090_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnvoyTerminatorSink_t1090_0_0_0/* byval_arg */
	, &EnvoyTerminatorSink_t1090_1_0_0/* this_arg */
	, &EnvoyTerminatorSink_t1090_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnvoyTerminatorSink_t1090)/* instance_size */
	, sizeof (EnvoyTerminatorSink_t1090)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EnvoyTerminatorSink_t1090_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_Header.h"
// Metadata Definition System.Runtime.Remoting.Messaging.Header
extern TypeInfo Header_t1091_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderMethodDeclarations.h"
static const EncodedMethodIndex Header_t1091_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Header_t1091_0_0_0;
extern const Il2CppType Header_t1091_1_0_0;
struct Header_t1091;
const Il2CppTypeDefinitionMetadata Header_t1091_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Header_t1091_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4349/* fieldStart */
	, 6648/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Header_t1091_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Header"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &Header_t1091_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1095/* custom_attributes_cache */
	, &Header_t1091_0_0_0/* byval_arg */
	, &Header_t1091_1_0_0/* this_arg */
	, &Header_t1091_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Header_t1091)/* instance_size */
	, sizeof (Header_t1091)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IInternalMessage
extern TypeInfo IInternalMessage_t1419_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IInternalMessage_t1419_1_0_0;
struct IInternalMessage_t1419;
const Il2CppTypeDefinitionMetadata IInternalMessage_t1419_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6651/* methodStart */
	, -1/* eventStart */
	, 1357/* propertyStart */

};
TypeInfo IInternalMessage_t1419_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IInternalMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IInternalMessage_t1419_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IInternalMessage_t1419_0_0_0/* byval_arg */
	, &IInternalMessage_t1419_1_0_0/* this_arg */
	, &IInternalMessage_t1419_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessage
extern TypeInfo IMessage_t1084_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessage_t1084_1_0_0;
struct IMessage_t1084;
const Il2CppTypeDefinitionMetadata IMessage_t1084_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMessage_t1084_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IMessage_t1084_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1096/* custom_attributes_cache */
	, &IMessage_t1084_0_0_0/* byval_arg */
	, &IMessage_t1084_1_0_0/* this_arg */
	, &IMessage_t1084_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessageCtrl
extern TypeInfo IMessageCtrl_t1083_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessageCtrl_t1083_0_0_0;
extern const Il2CppType IMessageCtrl_t1083_1_0_0;
struct IMessageCtrl_t1083;
const Il2CppTypeDefinitionMetadata IMessageCtrl_t1083_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMessageCtrl_t1083_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessageCtrl"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IMessageCtrl_t1083_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1097/* custom_attributes_cache */
	, &IMessageCtrl_t1083_0_0_0/* byval_arg */
	, &IMessageCtrl_t1083_1_0_0/* this_arg */
	, &IMessageCtrl_t1083_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessageSink
extern TypeInfo IMessageSink_t1112_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessageSink_t1112_1_0_0;
struct IMessageSink_t1112;
const Il2CppTypeDefinitionMetadata IMessageSink_t1112_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMessageSink_t1112_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessageSink"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IMessageSink_t1112_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1098/* custom_attributes_cache */
	, &IMessageSink_t1112_0_0_0/* byval_arg */
	, &IMessageSink_t1112_1_0_0/* this_arg */
	, &IMessageSink_t1112_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodCallMessage
extern TypeInfo IMethodCallMessage_t1401_il2cpp_TypeInfo;
static const Il2CppType* IMethodCallMessage_t1401_InterfacesTypeInfos[] = 
{
	&IMessage_t1084_0_0_0,
	&IMethodMessage_t1096_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodCallMessage_t1401_1_0_0;
struct IMethodCallMessage_t1401;
const Il2CppTypeDefinitionMetadata IMethodCallMessage_t1401_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodCallMessage_t1401_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMethodCallMessage_t1401_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodCallMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IMethodCallMessage_t1401_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1099/* custom_attributes_cache */
	, &IMethodCallMessage_t1401_0_0_0/* byval_arg */
	, &IMethodCallMessage_t1401_1_0_0/* this_arg */
	, &IMethodCallMessage_t1401_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodMessage
extern TypeInfo IMethodMessage_t1096_il2cpp_TypeInfo;
static const Il2CppType* IMethodMessage_t1096_InterfacesTypeInfos[] = 
{
	&IMessage_t1084_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodMessage_t1096_1_0_0;
struct IMethodMessage_t1096;
const Il2CppTypeDefinitionMetadata IMethodMessage_t1096_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodMessage_t1096_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6652/* methodStart */
	, -1/* eventStart */
	, 1358/* propertyStart */

};
TypeInfo IMethodMessage_t1096_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IMethodMessage_t1096_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1100/* custom_attributes_cache */
	, &IMethodMessage_t1096_0_0_0/* byval_arg */
	, &IMethodMessage_t1096_1_0_0/* this_arg */
	, &IMethodMessage_t1096_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodReturnMessage
extern TypeInfo IMethodReturnMessage_t1400_il2cpp_TypeInfo;
static const Il2CppType* IMethodReturnMessage_t1400_InterfacesTypeInfos[] = 
{
	&IMessage_t1084_0_0_0,
	&IMethodMessage_t1096_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodReturnMessage_t1400_0_0_0;
extern const Il2CppType IMethodReturnMessage_t1400_1_0_0;
struct IMethodReturnMessage_t1400;
const Il2CppTypeDefinitionMetadata IMethodReturnMessage_t1400_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodReturnMessage_t1400_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6659/* methodStart */
	, -1/* eventStart */
	, 1365/* propertyStart */

};
TypeInfo IMethodReturnMessage_t1400_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodReturnMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IMethodReturnMessage_t1400_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1101/* custom_attributes_cache */
	, &IMethodReturnMessage_t1400_0_0_0/* byval_arg */
	, &IMethodReturnMessage_t1400_1_0_0/* this_arg */
	, &IMethodReturnMessage_t1400_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IRemotingFormatter
extern TypeInfo IRemotingFormatter_t2113_il2cpp_TypeInfo;
extern const Il2CppType IFormatter_t2115_0_0_0;
static const Il2CppType* IRemotingFormatter_t2113_InterfacesTypeInfos[] = 
{
	&IFormatter_t2115_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IRemotingFormatter_t2113_0_0_0;
extern const Il2CppType IRemotingFormatter_t2113_1_0_0;
struct IRemotingFormatter_t2113;
const Il2CppTypeDefinitionMetadata IRemotingFormatter_t2113_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IRemotingFormatter_t2113_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IRemotingFormatter_t2113_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IRemotingFormatter"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IRemotingFormatter_t2113_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1102/* custom_attributes_cache */
	, &IRemotingFormatter_t2113_0_0_0/* byval_arg */
	, &IRemotingFormatter_t2113_1_0_0/* this_arg */
	, &IRemotingFormatter_t2113_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.ISerializationRootObject
extern TypeInfo ISerializationRootObject_t2114_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISerializationRootObject_t2114_1_0_0;
struct ISerializationRootObject_t2114;
const Il2CppTypeDefinitionMetadata ISerializationRootObject_t2114_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISerializationRootObject_t2114_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializationRootObject"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &ISerializationRootObject_t2114_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISerializationRootObject_t2114_0_0_0/* byval_arg */
	, &ISerializationRootObject_t2114_1_0_0/* this_arg */
	, &ISerializationRootObject_t2114_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContex.h"
// Metadata Definition System.Runtime.Remoting.Messaging.LogicalCallContext
extern TypeInfo LogicalCallContext_t1093_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContexMethodDeclarations.h"
static const EncodedMethodIndex LogicalCallContext_t1093_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2585,
};
extern const Il2CppType ICloneable_t2056_0_0_0;
static const Il2CppType* LogicalCallContext_t1093_InterfacesTypeInfos[] = 
{
	&ICloneable_t2056_0_0_0,
	&ISerializable_t1426_0_0_0,
};
static Il2CppInterfaceOffsetPair LogicalCallContext_t1093_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LogicalCallContext_t1093_0_0_0;
extern const Il2CppType LogicalCallContext_t1093_1_0_0;
struct LogicalCallContext_t1093;
const Il2CppTypeDefinitionMetadata LogicalCallContext_t1093_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LogicalCallContext_t1093_InterfacesTypeInfos/* implementedInterfaces */
	, LogicalCallContext_t1093_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LogicalCallContext_t1093_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4353/* fieldStart */
	, 6662/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LogicalCallContext_t1093_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogicalCallContext"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &LogicalCallContext_t1093_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1103/* custom_attributes_cache */
	, &LogicalCallContext_t1093_0_0_0/* byval_arg */
	, &LogicalCallContext_t1093_1_0_0/* this_arg */
	, &LogicalCallContext_t1093_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LogicalCallContext_t1093)/* instance_size */
	, sizeof (LogicalCallContext_t1093)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemoti.h"
// Metadata Definition System.Runtime.Remoting.Messaging.CallContextRemotingData
extern TypeInfo CallContextRemotingData_t1092_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemotiMethodDeclarations.h"
static const EncodedMethodIndex CallContextRemotingData_t1092_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
static const Il2CppType* CallContextRemotingData_t1092_InterfacesTypeInfos[] = 
{
	&ICloneable_t2056_0_0_0,
};
static Il2CppInterfaceOffsetPair CallContextRemotingData_t1092_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallContextRemotingData_t1092_0_0_0;
extern const Il2CppType CallContextRemotingData_t1092_1_0_0;
struct CallContextRemotingData_t1092;
const Il2CppTypeDefinitionMetadata CallContextRemotingData_t1092_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CallContextRemotingData_t1092_InterfacesTypeInfos/* implementedInterfaces */
	, CallContextRemotingData_t1092_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CallContextRemotingData_t1092_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6666/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CallContextRemotingData_t1092_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallContextRemotingData"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &CallContextRemotingData_t1092_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CallContextRemotingData_t1092_0_0_0/* byval_arg */
	, &CallContextRemotingData_t1092_1_0_0/* this_arg */
	, &CallContextRemotingData_t1092_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallContextRemotingData_t1092)/* instance_size */
	, sizeof (CallContextRemotingData_t1092)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCall.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCall
extern TypeInfo MethodCall_t1087_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallMethodDeclarations.h"
static const EncodedMethodIndex MethodCall_t1087_VTable[19] = 
{
	120,
	125,
	122,
	123,
	2586,
	2552,
	2553,
	2554,
	2555,
	2556,
	2557,
	2558,
	2559,
	2587,
	2586,
	2588,
	2589,
	2563,
	2564,
};
static const Il2CppType* MethodCall_t1087_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
	&IInternalMessage_t1419_0_0_0,
	&IMessage_t1084_0_0_0,
	&IMethodCallMessage_t1401_0_0_0,
	&IMethodMessage_t1096_0_0_0,
	&ISerializationRootObject_t2114_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodCall_t1087_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &IInternalMessage_t1419_0_0_0, 5},
	{ &IMessage_t1084_0_0_0, 6},
	{ &IMethodCallMessage_t1401_0_0_0, 6},
	{ &IMethodMessage_t1096_0_0_0, 6},
	{ &ISerializationRootObject_t2114_0_0_0, 13},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodCall_t1087_1_0_0;
struct MethodCall_t1087;
const Il2CppTypeDefinitionMetadata MethodCall_t1087_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodCall_t1087_InterfacesTypeInfos/* implementedInterfaces */
	, MethodCall_t1087_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MethodCall_t1087_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4355/* fieldStart */
	, 6667/* methodStart */
	, -1/* eventStart */
	, 1368/* propertyStart */

};
TypeInfo MethodCall_t1087_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodCall"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &MethodCall_t1087_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1104/* custom_attributes_cache */
	, &MethodCall_t1087_0_0_0/* byval_arg */
	, &MethodCall_t1087_1_0_0/* this_arg */
	, &MethodCall_t1087_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodCall_t1087)/* instance_size */
	, sizeof (MethodCall_t1087)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodCall_t1087_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 10/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDiction.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCallDictionary
extern TypeInfo MethodCallDictionary_t1094_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDictionMethodDeclarations.h"
static const EncodedMethodIndex MethodCallDictionary_t1094_VTable[18] = 
{
	120,
	125,
	122,
	123,
	2571,
	2572,
	2573,
	2574,
	2575,
	2576,
	2577,
	2578,
	2579,
	2580,
	2581,
	2590,
	2591,
	2584,
};
static Il2CppInterfaceOffsetPair MethodCallDictionary_t1094_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IDictionary_t493_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodCallDictionary_t1094_0_0_0;
extern const Il2CppType MethodCallDictionary_t1094_1_0_0;
struct MethodCallDictionary_t1094;
const Il2CppTypeDefinitionMetadata MethodCallDictionary_t1094_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodCallDictionary_t1094_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t1089_0_0_0/* parent */
	, MethodCallDictionary_t1094_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4366/* fieldStart */
	, 6688/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MethodCallDictionary_t1094_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodCallDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &MethodCallDictionary_t1094_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodCallDictionary_t1094_0_0_0/* byval_arg */
	, &MethodCallDictionary_t1094_1_0_0/* this_arg */
	, &MethodCallDictionary_t1094_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodCallDictionary_t1094)/* instance_size */
	, sizeof (MethodCallDictionary_t1094)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodCallDictionary_t1094_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary
extern TypeInfo MethodDictionary_t1089_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionaryMethodDeclarations.h"
extern const Il2CppType DictionaryEnumerator_t1095_0_0_0;
static const Il2CppType* MethodDictionary_t1089_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DictionaryEnumerator_t1095_0_0_0,
};
static const EncodedMethodIndex MethodDictionary_t1089_VTable[18] = 
{
	120,
	125,
	122,
	123,
	2571,
	2572,
	2573,
	2574,
	2575,
	2576,
	2577,
	2578,
	2579,
	2580,
	2581,
	2590,
	2591,
	2584,
};
static const Il2CppType* MethodDictionary_t1089_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
	&ICollection_t569_0_0_0,
	&IDictionary_t493_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodDictionary_t1089_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IDictionary_t493_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodDictionary_t1089_1_0_0;
struct MethodDictionary_t1089;
const Il2CppTypeDefinitionMetadata MethodDictionary_t1089_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MethodDictionary_t1089_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, MethodDictionary_t1089_InterfacesTypeInfos/* implementedInterfaces */
	, MethodDictionary_t1089_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MethodDictionary_t1089_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4367/* fieldStart */
	, 6690/* methodStart */
	, -1/* eventStart */
	, 1378/* propertyStart */

};
TypeInfo MethodDictionary_t1089_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &MethodDictionary_t1089_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1106/* custom_attributes_cache */
	, &MethodDictionary_t1089_0_0_0/* byval_arg */
	, &MethodDictionary_t1089_1_0_0/* this_arg */
	, &MethodDictionary_t1089_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodDictionary_t1089)/* instance_size */
	, sizeof (MethodDictionary_t1089)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodDictionary_t1089_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 5/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 18/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
extern TypeInfo DictionaryEnumerator_t1095_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_MethodDeclarations.h"
static const EncodedMethodIndex DictionaryEnumerator_t1095_VTable[9] = 
{
	120,
	125,
	122,
	123,
	2592,
	2593,
	2594,
	2595,
	2596,
};
extern const Il2CppType IEnumerator_t279_0_0_0;
extern const Il2CppType IDictionaryEnumerator_t559_0_0_0;
static const Il2CppType* DictionaryEnumerator_t1095_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
	&IDictionaryEnumerator_t559_0_0_0,
};
static Il2CppInterfaceOffsetPair DictionaryEnumerator_t1095_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
	{ &IDictionaryEnumerator_t559_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DictionaryEnumerator_t1095_1_0_0;
struct DictionaryEnumerator_t1095;
const Il2CppTypeDefinitionMetadata DictionaryEnumerator_t1095_DefinitionMetadata = 
{
	&MethodDictionary_t1089_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, DictionaryEnumerator_t1095_InterfacesTypeInfos/* implementedInterfaces */
	, DictionaryEnumerator_t1095_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DictionaryEnumerator_t1095_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4373/* fieldStart */
	, 6708/* methodStart */
	, -1/* eventStart */
	, 1383/* propertyStart */

};
TypeInfo DictionaryEnumerator_t1095_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DictionaryEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DictionaryEnumerator_t1095_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DictionaryEnumerator_t1095_0_0_0/* byval_arg */
	, &DictionaryEnumerator_t1095_1_0_0/* this_arg */
	, &DictionaryEnumerator_t1095_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DictionaryEnumerator_t1095)/* instance_size */
	, sizeof (DictionaryEnumerator_t1095)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDicti.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodReturnDictionary
extern TypeInfo MethodReturnDictionary_t1097_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDictiMethodDeclarations.h"
static const EncodedMethodIndex MethodReturnDictionary_t1097_VTable[18] = 
{
	120,
	125,
	122,
	123,
	2571,
	2572,
	2573,
	2574,
	2575,
	2576,
	2577,
	2578,
	2579,
	2580,
	2581,
	2590,
	2591,
	2584,
};
static Il2CppInterfaceOffsetPair MethodReturnDictionary_t1097_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
	{ &ICollection_t569_0_0_0, 5},
	{ &IDictionary_t493_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodReturnDictionary_t1097_0_0_0;
extern const Il2CppType MethodReturnDictionary_t1097_1_0_0;
struct MethodReturnDictionary_t1097;
const Il2CppTypeDefinitionMetadata MethodReturnDictionary_t1097_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodReturnDictionary_t1097_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t1089_0_0_0/* parent */
	, MethodReturnDictionary_t1097_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4376/* fieldStart */
	, 6714/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MethodReturnDictionary_t1097_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodReturnDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &MethodReturnDictionary_t1097_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodReturnDictionary_t1097_0_0_0/* byval_arg */
	, &MethodReturnDictionary_t1097_1_0_0/* this_arg */
	, &MethodReturnDictionary_t1097_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodReturnDictionary_t1097)/* instance_size */
	, sizeof (MethodReturnDictionary_t1097)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodReturnDictionary_t1097_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessage.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MonoMethodMessage
extern TypeInfo MonoMethodMessage_t1082_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessageMethodDeclarations.h"
static const EncodedMethodIndex MonoMethodMessage_t1082_VTable[16] = 
{
	120,
	125,
	122,
	123,
	2597,
	2598,
	2599,
	2600,
	2601,
	2602,
	2603,
	2604,
	2605,
	2606,
	2607,
	2608,
};
static const Il2CppType* MonoMethodMessage_t1082_InterfacesTypeInfos[] = 
{
	&IInternalMessage_t1419_0_0_0,
	&IMessage_t1084_0_0_0,
	&IMethodCallMessage_t1401_0_0_0,
	&IMethodMessage_t1096_0_0_0,
	&IMethodReturnMessage_t1400_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoMethodMessage_t1082_InterfacesOffsets[] = 
{
	{ &IInternalMessage_t1419_0_0_0, 4},
	{ &IMessage_t1084_0_0_0, 5},
	{ &IMethodCallMessage_t1401_0_0_0, 5},
	{ &IMethodMessage_t1096_0_0_0, 5},
	{ &IMethodReturnMessage_t1400_0_0_0, 12},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoMethodMessage_t1082_0_0_0;
extern const Il2CppType MonoMethodMessage_t1082_1_0_0;
struct MonoMethodMessage_t1082;
const Il2CppTypeDefinitionMetadata MonoMethodMessage_t1082_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoMethodMessage_t1082_InterfacesTypeInfos/* implementedInterfaces */
	, MonoMethodMessage_t1082_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoMethodMessage_t1082_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4378/* fieldStart */
	, 6716/* methodStart */
	, -1/* eventStart */
	, 1387/* propertyStart */

};
TypeInfo MonoMethodMessage_t1082_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoMethodMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &MonoMethodMessage_t1082_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoMethodMessage_t1082_0_0_0/* byval_arg */
	, &MonoMethodMessage_t1082_1_0_0/* this_arg */
	, &MonoMethodMessage_t1082_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoMethodMessage_t1082)/* instance_size */
	, sizeof (MonoMethodMessage_t1082)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 11/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 16/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate.h"
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogate
extern TypeInfo RemotingSurrogate_t1098_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogateMethodDeclarations.h"
static const EncodedMethodIndex RemotingSurrogate_t1098_VTable[6] = 
{
	120,
	125,
	122,
	123,
	2609,
	2609,
};
extern const Il2CppType ISerializationSurrogate_t1167_0_0_0;
static const Il2CppType* RemotingSurrogate_t1098_InterfacesTypeInfos[] = 
{
	&ISerializationSurrogate_t1167_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingSurrogate_t1098_InterfacesOffsets[] = 
{
	{ &ISerializationSurrogate_t1167_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingSurrogate_t1098_0_0_0;
extern const Il2CppType RemotingSurrogate_t1098_1_0_0;
struct RemotingSurrogate_t1098;
const Il2CppTypeDefinitionMetadata RemotingSurrogate_t1098_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingSurrogate_t1098_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingSurrogate_t1098_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingSurrogate_t1098_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6728/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RemotingSurrogate_t1098_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingSurrogate"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &RemotingSurrogate_t1098_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingSurrogate_t1098_0_0_0/* byval_arg */
	, &RemotingSurrogate_t1098_1_0_0/* this_arg */
	, &RemotingSurrogate_t1098_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingSurrogate_t1098)/* instance_size */
	, sizeof (RemotingSurrogate_t1098)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogate.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ObjRefSurrogate
extern TypeInfo ObjRefSurrogate_t1099_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogateMethodDeclarations.h"
static const EncodedMethodIndex ObjRefSurrogate_t1099_VTable[6] = 
{
	120,
	125,
	122,
	123,
	2610,
	2610,
};
static const Il2CppType* ObjRefSurrogate_t1099_InterfacesTypeInfos[] = 
{
	&ISerializationSurrogate_t1167_0_0_0,
};
static Il2CppInterfaceOffsetPair ObjRefSurrogate_t1099_InterfacesOffsets[] = 
{
	{ &ISerializationSurrogate_t1167_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjRefSurrogate_t1099_0_0_0;
extern const Il2CppType ObjRefSurrogate_t1099_1_0_0;
struct ObjRefSurrogate_t1099;
const Il2CppTypeDefinitionMetadata ObjRefSurrogate_t1099_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ObjRefSurrogate_t1099_InterfacesTypeInfos/* implementedInterfaces */
	, ObjRefSurrogate_t1099_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjRefSurrogate_t1099_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6730/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ObjRefSurrogate_t1099_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjRefSurrogate"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &ObjRefSurrogate_t1099_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjRefSurrogate_t1099_0_0_0/* byval_arg */
	, &ObjRefSurrogate_t1099_1_0_0/* this_arg */
	, &ObjRefSurrogate_t1099_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjRefSurrogate_t1099)/* instance_size */
	, sizeof (ObjRefSurrogate_t1099)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0.h"
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
extern TypeInfo RemotingSurrogateSelector_t1101_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0MethodDeclarations.h"
static const EncodedMethodIndex RemotingSurrogateSelector_t1101_VTable[6] = 
{
	120,
	125,
	122,
	123,
	2611,
	2611,
};
extern const Il2CppType ISurrogateSelector_t1100_0_0_0;
static const Il2CppType* RemotingSurrogateSelector_t1101_InterfacesTypeInfos[] = 
{
	&ISurrogateSelector_t1100_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingSurrogateSelector_t1101_InterfacesOffsets[] = 
{
	{ &ISurrogateSelector_t1100_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingSurrogateSelector_t1101_0_0_0;
extern const Il2CppType RemotingSurrogateSelector_t1101_1_0_0;
struct RemotingSurrogateSelector_t1101;
const Il2CppTypeDefinitionMetadata RemotingSurrogateSelector_t1101_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingSurrogateSelector_t1101_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingSurrogateSelector_t1101_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingSurrogateSelector_t1101_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4386/* fieldStart */
	, 6732/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RemotingSurrogateSelector_t1101_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingSurrogateSelector"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &RemotingSurrogateSelector_t1101_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1109/* custom_attributes_cache */
	, &RemotingSurrogateSelector_t1101_0_0_0/* byval_arg */
	, &RemotingSurrogateSelector_t1101_1_0_0/* this_arg */
	, &RemotingSurrogateSelector_t1101_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingSurrogateSelector_t1101)/* instance_size */
	, sizeof (RemotingSurrogateSelector_t1101)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingSurrogateSelector_t1101_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessage.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ReturnMessage
extern TypeInfo ReturnMessage_t1102_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessageMethodDeclarations.h"
static const EncodedMethodIndex ReturnMessage_t1102_VTable[18] = 
{
	120,
	125,
	122,
	123,
	2612,
	2613,
	2614,
	2615,
	2616,
	2617,
	2618,
	2619,
	2620,
	2621,
	2622,
	2623,
	2624,
	2622,
};
static const Il2CppType* ReturnMessage_t1102_InterfacesTypeInfos[] = 
{
	&IInternalMessage_t1419_0_0_0,
	&IMessage_t1084_0_0_0,
	&IMethodMessage_t1096_0_0_0,
	&IMethodReturnMessage_t1400_0_0_0,
};
static Il2CppInterfaceOffsetPair ReturnMessage_t1102_InterfacesOffsets[] = 
{
	{ &IInternalMessage_t1419_0_0_0, 4},
	{ &IMessage_t1084_0_0_0, 5},
	{ &IMethodMessage_t1096_0_0_0, 5},
	{ &IMethodReturnMessage_t1400_0_0_0, 12},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReturnMessage_t1102_0_0_0;
extern const Il2CppType ReturnMessage_t1102_1_0_0;
struct ReturnMessage_t1102;
const Il2CppTypeDefinitionMetadata ReturnMessage_t1102_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ReturnMessage_t1102_InterfacesTypeInfos/* implementedInterfaces */
	, ReturnMessage_t1102_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReturnMessage_t1102_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4390/* fieldStart */
	, 6735/* methodStart */
	, -1/* eventStart */
	, 1398/* propertyStart */

};
TypeInfo ReturnMessage_t1102_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &ReturnMessage_t1102_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1110/* custom_attributes_cache */
	, &ReturnMessage_t1102_0_0_0/* byval_arg */
	, &ReturnMessage_t1102_1_0_0/* this_arg */
	, &ReturnMessage_t1102_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnMessage_t1102)/* instance_size */
	, sizeof (ReturnMessage_t1102)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 12/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Runtime.Remoting.Metadata.SoapAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapAttribute.h"
// Metadata Definition System.Runtime.Remoting.Metadata.SoapAttribute
extern TypeInfo SoapAttribute_t1103_il2cpp_TypeInfo;
// System.Runtime.Remoting.Metadata.SoapAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapAttributeMethodDeclarations.h"
static const EncodedMethodIndex SoapAttribute_t1103_VTable[7] = 
{
	253,
	125,
	254,
	123,
	2625,
	2626,
	2627,
};
static Il2CppInterfaceOffsetPair SoapAttribute_t1103_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SoapAttribute_t1103_0_0_0;
extern const Il2CppType SoapAttribute_t1103_1_0_0;
struct SoapAttribute_t1103;
const Il2CppTypeDefinitionMetadata SoapAttribute_t1103_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SoapAttribute_t1103_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, SoapAttribute_t1103_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4403/* fieldStart */
	, 6750/* methodStart */
	, -1/* eventStart */
	, 1410/* propertyStart */

};
TypeInfo SoapAttribute_t1103_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoapAttribute"/* name */
	, "System.Runtime.Remoting.Metadata"/* namespaze */
	, NULL/* methods */
	, &SoapAttribute_t1103_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1111/* custom_attributes_cache */
	, &SoapAttribute_t1103_0_0_0/* byval_arg */
	, &SoapAttribute_t1103_1_0_0/* this_arg */
	, &SoapAttribute_t1103_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoapAttribute_t1103)/* instance_size */
	, sizeof (SoapAttribute_t1103)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Metadata.SoapFieldAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapFieldAttribute.h"
// Metadata Definition System.Runtime.Remoting.Metadata.SoapFieldAttribute
extern TypeInfo SoapFieldAttribute_t1104_il2cpp_TypeInfo;
// System.Runtime.Remoting.Metadata.SoapFieldAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapFieldAttributeMethodDeclarations.h"
static const EncodedMethodIndex SoapFieldAttribute_t1104_VTable[7] = 
{
	253,
	125,
	254,
	123,
	2625,
	2626,
	2628,
};
static Il2CppInterfaceOffsetPair SoapFieldAttribute_t1104_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SoapFieldAttribute_t1104_0_0_0;
extern const Il2CppType SoapFieldAttribute_t1104_1_0_0;
struct SoapFieldAttribute_t1104;
const Il2CppTypeDefinitionMetadata SoapFieldAttribute_t1104_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SoapFieldAttribute_t1104_InterfacesOffsets/* interfaceOffsets */
	, &SoapAttribute_t1103_0_0_0/* parent */
	, SoapFieldAttribute_t1104_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4406/* fieldStart */
	, 6754/* methodStart */
	, -1/* eventStart */
	, 1412/* propertyStart */

};
TypeInfo SoapFieldAttribute_t1104_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoapFieldAttribute"/* name */
	, "System.Runtime.Remoting.Metadata"/* namespaze */
	, NULL/* methods */
	, &SoapFieldAttribute_t1104_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1112/* custom_attributes_cache */
	, &SoapFieldAttribute_t1104_0_0_0/* byval_arg */
	, &SoapFieldAttribute_t1104_1_0_0/* this_arg */
	, &SoapFieldAttribute_t1104_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoapFieldAttribute_t1104)/* instance_size */
	, sizeof (SoapFieldAttribute_t1104)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Metadata.SoapMethodAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapMethodAttribut.h"
// Metadata Definition System.Runtime.Remoting.Metadata.SoapMethodAttribute
extern TypeInfo SoapMethodAttribute_t1105_il2cpp_TypeInfo;
// System.Runtime.Remoting.Metadata.SoapMethodAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapMethodAttributMethodDeclarations.h"
static const EncodedMethodIndex SoapMethodAttribute_t1105_VTable[7] = 
{
	253,
	125,
	254,
	123,
	2629,
	2630,
	2631,
};
static Il2CppInterfaceOffsetPair SoapMethodAttribute_t1105_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SoapMethodAttribute_t1105_0_0_0;
extern const Il2CppType SoapMethodAttribute_t1105_1_0_0;
struct SoapMethodAttribute_t1105;
const Il2CppTypeDefinitionMetadata SoapMethodAttribute_t1105_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SoapMethodAttribute_t1105_InterfacesOffsets/* interfaceOffsets */
	, &SoapAttribute_t1103_0_0_0/* parent */
	, SoapMethodAttribute_t1105_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4408/* fieldStart */
	, 6758/* methodStart */
	, -1/* eventStart */
	, 1413/* propertyStart */

};
TypeInfo SoapMethodAttribute_t1105_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoapMethodAttribute"/* name */
	, "System.Runtime.Remoting.Metadata"/* namespaze */
	, NULL/* methods */
	, &SoapMethodAttribute_t1105_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1113/* custom_attributes_cache */
	, &SoapMethodAttribute_t1105_0_0_0/* byval_arg */
	, &SoapMethodAttribute_t1105_1_0_0/* this_arg */
	, &SoapMethodAttribute_t1105_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoapMethodAttribute_t1105)/* instance_size */
	, sizeof (SoapMethodAttribute_t1105)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Metadata.SoapParameterAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapParameterAttri.h"
// Metadata Definition System.Runtime.Remoting.Metadata.SoapParameterAttribute
extern TypeInfo SoapParameterAttribute_t1106_il2cpp_TypeInfo;
// System.Runtime.Remoting.Metadata.SoapParameterAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapParameterAttriMethodDeclarations.h"
static const EncodedMethodIndex SoapParameterAttribute_t1106_VTable[7] = 
{
	253,
	125,
	254,
	123,
	2625,
	2626,
	2627,
};
static Il2CppInterfaceOffsetPair SoapParameterAttribute_t1106_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SoapParameterAttribute_t1106_0_0_0;
extern const Il2CppType SoapParameterAttribute_t1106_1_0_0;
struct SoapParameterAttribute_t1106;
const Il2CppTypeDefinitionMetadata SoapParameterAttribute_t1106_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SoapParameterAttribute_t1106_InterfacesOffsets/* interfaceOffsets */
	, &SoapAttribute_t1103_0_0_0/* parent */
	, SoapParameterAttribute_t1106_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6762/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SoapParameterAttribute_t1106_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoapParameterAttribute"/* name */
	, "System.Runtime.Remoting.Metadata"/* namespaze */
	, NULL/* methods */
	, &SoapParameterAttribute_t1106_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1114/* custom_attributes_cache */
	, &SoapParameterAttribute_t1106_0_0_0/* byval_arg */
	, &SoapParameterAttribute_t1106_1_0_0/* this_arg */
	, &SoapParameterAttribute_t1106_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoapParameterAttribute_t1106)/* instance_size */
	, sizeof (SoapParameterAttribute_t1106)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Metadata.SoapTypeAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapTypeAttribute.h"
// Metadata Definition System.Runtime.Remoting.Metadata.SoapTypeAttribute
extern TypeInfo SoapTypeAttribute_t1107_il2cpp_TypeInfo;
// System.Runtime.Remoting.Metadata.SoapTypeAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapTypeAttributeMethodDeclarations.h"
static const EncodedMethodIndex SoapTypeAttribute_t1107_VTable[7] = 
{
	253,
	125,
	254,
	123,
	2632,
	2633,
	2634,
};
static Il2CppInterfaceOffsetPair SoapTypeAttribute_t1107_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SoapTypeAttribute_t1107_0_0_0;
extern const Il2CppType SoapTypeAttribute_t1107_1_0_0;
struct SoapTypeAttribute_t1107;
const Il2CppTypeDefinitionMetadata SoapTypeAttribute_t1107_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SoapTypeAttribute_t1107_InterfacesOffsets/* interfaceOffsets */
	, &SoapAttribute_t1103_0_0_0/* parent */
	, SoapTypeAttribute_t1107_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4414/* fieldStart */
	, 6763/* methodStart */
	, -1/* eventStart */
	, 1415/* propertyStart */

};
TypeInfo SoapTypeAttribute_t1107_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoapTypeAttribute"/* name */
	, "System.Runtime.Remoting.Metadata"/* namespaze */
	, NULL/* methods */
	, &SoapTypeAttribute_t1107_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1115/* custom_attributes_cache */
	, &SoapTypeAttribute_t1107_0_0_0/* byval_arg */
	, &SoapTypeAttribute_t1107_1_0_0/* this_arg */
	, &SoapTypeAttribute_t1107_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoapTypeAttribute_t1107)/* instance_size */
	, sizeof (SoapTypeAttribute_t1107)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttribute.h"
// Metadata Definition System.Runtime.Remoting.Proxies.ProxyAttribute
extern TypeInfo ProxyAttribute_t1108_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttributeMethodDeclarations.h"
static const EncodedMethodIndex ProxyAttribute_t1108_VTable[8] = 
{
	253,
	125,
	254,
	123,
	2635,
	2636,
	2637,
	2638,
};
static const Il2CppType* ProxyAttribute_t1108_InterfacesTypeInfos[] = 
{
	&IContextAttribute_t1413_0_0_0,
};
static Il2CppInterfaceOffsetPair ProxyAttribute_t1108_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
	{ &IContextAttribute_t1413_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ProxyAttribute_t1108_0_0_0;
extern const Il2CppType ProxyAttribute_t1108_1_0_0;
struct ProxyAttribute_t1108;
const Il2CppTypeDefinitionMetadata ProxyAttribute_t1108_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ProxyAttribute_t1108_InterfacesTypeInfos/* implementedInterfaces */
	, ProxyAttribute_t1108_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, ProxyAttribute_t1108_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6772/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ProxyAttribute_t1108_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProxyAttribute"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, NULL/* methods */
	, &ProxyAttribute_t1108_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1116/* custom_attributes_cache */
	, &ProxyAttribute_t1108_0_0_0/* byval_arg */
	, &ProxyAttribute_t1108_1_0_0/* this_arg */
	, &ProxyAttribute_t1108_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProxyAttribute_t1108)/* instance_size */
	, sizeof (ProxyAttribute_t1108)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.TransparentProxy
extern TypeInfo TransparentProxy_t1110_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxyMethodDeclarations.h"
static const EncodedMethodIndex TransparentProxy_t1110_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TransparentProxy_t1110_0_0_0;
extern const Il2CppType TransparentProxy_t1110_1_0_0;
struct TransparentProxy_t1110;
const Il2CppTypeDefinitionMetadata TransparentProxy_t1110_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TransparentProxy_t1110_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4421/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TransparentProxy_t1110_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TransparentProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, NULL/* methods */
	, &TransparentProxy_t1110_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TransparentProxy_t1110_0_0_0/* byval_arg */
	, &TransparentProxy_t1110_1_0_0/* this_arg */
	, &TransparentProxy_t1110_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TransparentProxy_t1110)/* instance_size */
	, sizeof (TransparentProxy_t1110)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.RealProxy
extern TypeInfo RealProxy_t1109_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxyMethodDeclarations.h"
static const EncodedMethodIndex RealProxy_t1109_VTable[6] = 
{
	120,
	125,
	122,
	123,
	2639,
	2640,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RealProxy_t1109_0_0_0;
extern const Il2CppType RealProxy_t1109_1_0_0;
struct RealProxy_t1109;
const Il2CppTypeDefinitionMetadata RealProxy_t1109_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RealProxy_t1109_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4422/* fieldStart */
	, 6776/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RealProxy_t1109_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RealProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, NULL/* methods */
	, &RealProxy_t1109_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1119/* custom_attributes_cache */
	, &RealProxy_t1109_0_0_0/* byval_arg */
	, &RealProxy_t1109_1_0_0/* this_arg */
	, &RealProxy_t1109_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RealProxy_t1109)/* instance_size */
	, sizeof (RealProxy_t1109)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.RemotingProxy
extern TypeInfo RemotingProxy_t1113_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxyMethodDeclarations.h"
static const EncodedMethodIndex RemotingProxy_t1113_VTable[7] = 
{
	120,
	2641,
	122,
	123,
	2639,
	2640,
	2642,
};
extern const Il2CppType IRemotingTypeInfo_t1124_0_0_0;
static const Il2CppType* RemotingProxy_t1113_InterfacesTypeInfos[] = 
{
	&IRemotingTypeInfo_t1124_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingProxy_t1113_InterfacesOffsets[] = 
{
	{ &IRemotingTypeInfo_t1124_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingProxy_t1113_0_0_0;
extern const Il2CppType RemotingProxy_t1113_1_0_0;
struct RemotingProxy_t1113;
const Il2CppTypeDefinitionMetadata RemotingProxy_t1113_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingProxy_t1113_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingProxy_t1113_InterfacesOffsets/* interfaceOffsets */
	, &RealProxy_t1109_0_0_0/* parent */
	, RemotingProxy_t1113_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4427/* fieldStart */
	, 6784/* methodStart */
	, -1/* eventStart */
	, 1422/* propertyStart */

};
TypeInfo RemotingProxy_t1113_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, NULL/* methods */
	, &RemotingProxy_t1113_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingProxy_t1113_0_0_0/* byval_arg */
	, &RemotingProxy_t1113_1_0_0/* this_arg */
	, &RemotingProxy_t1113_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingProxy_t1113)/* instance_size */
	, sizeof (RemotingProxy_t1113)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingProxy_t1113_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Services.ITrackingHandler
extern TypeInfo ITrackingHandler_t1421_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ITrackingHandler_t1421_0_0_0;
extern const Il2CppType ITrackingHandler_t1421_1_0_0;
struct ITrackingHandler_t1421;
const Il2CppTypeDefinitionMetadata ITrackingHandler_t1421_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6789/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ITrackingHandler_t1421_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ITrackingHandler"/* name */
	, "System.Runtime.Remoting.Services"/* namespaze */
	, NULL/* methods */
	, &ITrackingHandler_t1421_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1120/* custom_attributes_cache */
	, &ITrackingHandler_t1421_0_0_0/* byval_arg */
	, &ITrackingHandler_t1421_1_0_0/* this_arg */
	, &ITrackingHandler_t1421_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServices.h"
// Metadata Definition System.Runtime.Remoting.Services.TrackingServices
extern TypeInfo TrackingServices_t1114_il2cpp_TypeInfo;
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServicesMethodDeclarations.h"
static const EncodedMethodIndex TrackingServices_t1114_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TrackingServices_t1114_0_0_0;
extern const Il2CppType TrackingServices_t1114_1_0_0;
struct TrackingServices_t1114;
const Il2CppTypeDefinitionMetadata TrackingServices_t1114_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackingServices_t1114_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4432/* fieldStart */
	, 6790/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TrackingServices_t1114_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackingServices"/* name */
	, "System.Runtime.Remoting.Services"/* namespaze */
	, NULL/* methods */
	, &TrackingServices_t1114_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1121/* custom_attributes_cache */
	, &TrackingServices_t1114_0_0_0/* byval_arg */
	, &TrackingServices_t1114_1_0_0/* this_arg */
	, &TrackingServices_t1114_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TrackingServices_t1114)/* instance_size */
	, sizeof (TrackingServices_t1114)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TrackingServices_t1114_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntry.h"
// Metadata Definition System.Runtime.Remoting.ActivatedClientTypeEntry
extern TypeInfo ActivatedClientTypeEntry_t1115_il2cpp_TypeInfo;
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntryMethodDeclarations.h"
static const EncodedMethodIndex ActivatedClientTypeEntry_t1115_VTable[4] = 
{
	120,
	125,
	122,
	2643,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivatedClientTypeEntry_t1115_0_0_0;
extern const Il2CppType ActivatedClientTypeEntry_t1115_1_0_0;
extern const Il2CppType TypeEntry_t1116_0_0_0;
struct ActivatedClientTypeEntry_t1115;
const Il2CppTypeDefinitionMetadata ActivatedClientTypeEntry_t1115_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeEntry_t1116_0_0_0/* parent */
	, ActivatedClientTypeEntry_t1115_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4433/* fieldStart */
	, 6792/* methodStart */
	, -1/* eventStart */
	, 1423/* propertyStart */

};
TypeInfo ActivatedClientTypeEntry_t1115_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivatedClientTypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ActivatedClientTypeEntry_t1115_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1122/* custom_attributes_cache */
	, &ActivatedClientTypeEntry_t1115_0_0_0/* byval_arg */
	, &ActivatedClientTypeEntry_t1115_1_0_0/* this_arg */
	, &ActivatedClientTypeEntry_t1115_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivatedClientTypeEntry_t1115)/* instance_size */
	, sizeof (ActivatedClientTypeEntry_t1115)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ActivatedServiceTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedServiceTypeEntry.h"
// Metadata Definition System.Runtime.Remoting.ActivatedServiceTypeEntry
extern TypeInfo ActivatedServiceTypeEntry_t1117_il2cpp_TypeInfo;
// System.Runtime.Remoting.ActivatedServiceTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedServiceTypeEntryMethodDeclarations.h"
static const EncodedMethodIndex ActivatedServiceTypeEntry_t1117_VTable[4] = 
{
	120,
	125,
	122,
	2644,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivatedServiceTypeEntry_t1117_0_0_0;
extern const Il2CppType ActivatedServiceTypeEntry_t1117_1_0_0;
struct ActivatedServiceTypeEntry_t1117;
const Il2CppTypeDefinitionMetadata ActivatedServiceTypeEntry_t1117_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeEntry_t1116_0_0_0/* parent */
	, ActivatedServiceTypeEntry_t1117_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4435/* fieldStart */
	, 6797/* methodStart */
	, -1/* eventStart */
	, 1426/* propertyStart */

};
TypeInfo ActivatedServiceTypeEntry_t1117_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivatedServiceTypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ActivatedServiceTypeEntry_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1123/* custom_attributes_cache */
	, &ActivatedServiceTypeEntry_t1117_0_0_0/* byval_arg */
	, &ActivatedServiceTypeEntry_t1117_1_0_0/* this_arg */
	, &ActivatedServiceTypeEntry_t1117_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivatedServiceTypeEntry_t1117)/* instance_size */
	, sizeof (ActivatedServiceTypeEntry_t1117)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfo.h"
// Metadata Definition System.Runtime.Remoting.EnvoyInfo
extern TypeInfo EnvoyInfo_t1118_il2cpp_TypeInfo;
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfoMethodDeclarations.h"
static const EncodedMethodIndex EnvoyInfo_t1118_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2645,
};
extern const Il2CppType IEnvoyInfo_t1125_0_0_0;
static const Il2CppType* EnvoyInfo_t1118_InterfacesTypeInfos[] = 
{
	&IEnvoyInfo_t1125_0_0_0,
};
static Il2CppInterfaceOffsetPair EnvoyInfo_t1118_InterfacesOffsets[] = 
{
	{ &IEnvoyInfo_t1125_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnvoyInfo_t1118_0_0_0;
extern const Il2CppType EnvoyInfo_t1118_1_0_0;
struct EnvoyInfo_t1118;
const Il2CppTypeDefinitionMetadata EnvoyInfo_t1118_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnvoyInfo_t1118_InterfacesTypeInfos/* implementedInterfaces */
	, EnvoyInfo_t1118_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EnvoyInfo_t1118_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4436/* fieldStart */
	, 6800/* methodStart */
	, -1/* eventStart */
	, 1427/* propertyStart */

};
TypeInfo EnvoyInfo_t1118_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnvoyInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &EnvoyInfo_t1118_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnvoyInfo_t1118_0_0_0/* byval_arg */
	, &EnvoyInfo_t1118_1_0_0/* this_arg */
	, &EnvoyInfo_t1118_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnvoyInfo_t1118)/* instance_size */
	, sizeof (EnvoyInfo_t1118)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IChannelInfo
extern TypeInfo IChannelInfo_t1123_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelInfo_t1123_1_0_0;
struct IChannelInfo_t1123;
const Il2CppTypeDefinitionMetadata IChannelInfo_t1123_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6802/* methodStart */
	, -1/* eventStart */
	, 1428/* propertyStart */

};
TypeInfo IChannelInfo_t1123_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &IChannelInfo_t1123_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1124/* custom_attributes_cache */
	, &IChannelInfo_t1123_0_0_0/* byval_arg */
	, &IChannelInfo_t1123_1_0_0/* this_arg */
	, &IChannelInfo_t1123_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IEnvoyInfo
extern TypeInfo IEnvoyInfo_t1125_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnvoyInfo_t1125_1_0_0;
struct IEnvoyInfo_t1125;
const Il2CppTypeDefinitionMetadata IEnvoyInfo_t1125_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6803/* methodStart */
	, -1/* eventStart */
	, 1429/* propertyStart */

};
TypeInfo IEnvoyInfo_t1125_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnvoyInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &IEnvoyInfo_t1125_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1125/* custom_attributes_cache */
	, &IEnvoyInfo_t1125_0_0_0/* byval_arg */
	, &IEnvoyInfo_t1125_1_0_0/* this_arg */
	, &IEnvoyInfo_t1125_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IRemotingTypeInfo
extern TypeInfo IRemotingTypeInfo_t1124_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IRemotingTypeInfo_t1124_1_0_0;
struct IRemotingTypeInfo_t1124;
const Il2CppTypeDefinitionMetadata IRemotingTypeInfo_t1124_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6804/* methodStart */
	, -1/* eventStart */
	, 1430/* propertyStart */

};
TypeInfo IRemotingTypeInfo_t1124_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IRemotingTypeInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &IRemotingTypeInfo_t1124_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1126/* custom_attributes_cache */
	, &IRemotingTypeInfo_t1124_0_0_0/* byval_arg */
	, &IRemotingTypeInfo_t1124_1_0_0/* this_arg */
	, &IRemotingTypeInfo_t1124_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// Metadata Definition System.Runtime.Remoting.Identity
extern TypeInfo Identity_t1111_il2cpp_TypeInfo;
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_IdentityMethodDeclarations.h"
static const EncodedMethodIndex Identity_t1111_VTable[5] = 
{
	120,
	125,
	122,
	123,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Identity_t1111_0_0_0;
extern const Il2CppType Identity_t1111_1_0_0;
struct Identity_t1111;
const Il2CppTypeDefinitionMetadata Identity_t1111_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Identity_t1111_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4437/* fieldStart */
	, 6805/* methodStart */
	, -1/* eventStart */
	, 1431/* propertyStart */

};
TypeInfo Identity_t1111_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Identity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &Identity_t1111_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Identity_t1111_0_0_0/* byval_arg */
	, &Identity_t1111_1_0_0/* this_arg */
	, &Identity_t1111_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Identity_t1111)/* instance_size */
	, sizeof (Identity_t1111)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentity.h"
// Metadata Definition System.Runtime.Remoting.ClientIdentity
extern TypeInfo ClientIdentity_t1121_il2cpp_TypeInfo;
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentityMethodDeclarations.h"
static const EncodedMethodIndex ClientIdentity_t1121_VTable[5] = 
{
	120,
	125,
	122,
	123,
	2646,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClientIdentity_t1121_0_0_0;
extern const Il2CppType ClientIdentity_t1121_1_0_0;
struct ClientIdentity_t1121;
const Il2CppTypeDefinitionMetadata ClientIdentity_t1121_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Identity_t1111_0_0_0/* parent */
	, ClientIdentity_t1121_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4442/* fieldStart */
	, 6812/* methodStart */
	, -1/* eventStart */
	, 1434/* propertyStart */

};
TypeInfo ClientIdentity_t1121_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ClientIdentity_t1121_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientIdentity_t1121_0_0_0/* byval_arg */
	, &ClientIdentity_t1121_1_0_0/* this_arg */
	, &ClientIdentity_t1121_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientIdentity_t1121)/* instance_size */
	, sizeof (ClientIdentity_t1121)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.InternalRemotingServices
#include "mscorlib_System_Runtime_Remoting_InternalRemotingServices.h"
// Metadata Definition System.Runtime.Remoting.InternalRemotingServices
extern TypeInfo InternalRemotingServices_t1122_il2cpp_TypeInfo;
// System.Runtime.Remoting.InternalRemotingServices
#include "mscorlib_System_Runtime_Remoting_InternalRemotingServicesMethodDeclarations.h"
static const EncodedMethodIndex InternalRemotingServices_t1122_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InternalRemotingServices_t1122_0_0_0;
extern const Il2CppType InternalRemotingServices_t1122_1_0_0;
struct InternalRemotingServices_t1122;
const Il2CppTypeDefinitionMetadata InternalRemotingServices_t1122_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InternalRemotingServices_t1122_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4443/* fieldStart */
	, 6817/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InternalRemotingServices_t1122_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalRemotingServices"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &InternalRemotingServices_t1122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1127/* custom_attributes_cache */
	, &InternalRemotingServices_t1122_0_0_0/* byval_arg */
	, &InternalRemotingServices_t1122_1_0_0/* this_arg */
	, &InternalRemotingServices_t1122_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalRemotingServices_t1122)/* instance_size */
	, sizeof (InternalRemotingServices_t1122)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(InternalRemotingServices_t1122_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRef.h"
// Metadata Definition System.Runtime.Remoting.ObjRef
extern TypeInfo ObjRef_t1119_il2cpp_TypeInfo;
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRefMethodDeclarations.h"
static const EncodedMethodIndex ObjRef_t1119_VTable[15] = 
{
	120,
	125,
	122,
	123,
	2647,
	2648,
	2649,
	2650,
	2651,
	2652,
	2653,
	2654,
	2655,
	2647,
	2648,
};
extern const Il2CppType IObjectReference_t1427_0_0_0;
static const Il2CppType* ObjRef_t1119_InterfacesTypeInfos[] = 
{
	&ISerializable_t1426_0_0_0,
	&IObjectReference_t1427_0_0_0,
};
static Il2CppInterfaceOffsetPair ObjRef_t1119_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &IObjectReference_t1427_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjRef_t1119_0_0_0;
extern const Il2CppType ObjRef_t1119_1_0_0;
struct ObjRef_t1119;
const Il2CppTypeDefinitionMetadata ObjRef_t1119_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ObjRef_t1119_InterfacesTypeInfos/* implementedInterfaces */
	, ObjRef_t1119_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjRef_t1119_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4444/* fieldStart */
	, 6819/* methodStart */
	, -1/* eventStart */
	, 1436/* propertyStart */

};
TypeInfo ObjRef_t1119_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjRef"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ObjRef_t1119_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1128/* custom_attributes_cache */
	, &ObjRef_t1119_0_0_0/* byval_arg */
	, &ObjRef_t1119_1_0_0/* this_arg */
	, &ObjRef_t1119_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjRef_t1119)/* instance_size */
	, sizeof (ObjRef_t1119)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ObjRef_t1119_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfiguration.h"
// Metadata Definition System.Runtime.Remoting.RemotingConfiguration
extern TypeInfo RemotingConfiguration_t1126_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfigurationMethodDeclarations.h"
static const EncodedMethodIndex RemotingConfiguration_t1126_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingConfiguration_t1126_0_0_0;
extern const Il2CppType RemotingConfiguration_t1126_1_0_0;
struct RemotingConfiguration_t1126;
const Il2CppTypeDefinitionMetadata RemotingConfiguration_t1126_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingConfiguration_t1126_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4453/* fieldStart */
	, 6834/* methodStart */
	, -1/* eventStart */
	, 1442/* propertyStart */

};
TypeInfo RemotingConfiguration_t1126_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingConfiguration"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &RemotingConfiguration_t1126_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1131/* custom_attributes_cache */
	, &RemotingConfiguration_t1126_0_0_0/* byval_arg */
	, &RemotingConfiguration_t1126_1_0_0/* this_arg */
	, &RemotingConfiguration_t1126_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingConfiguration_t1126)/* instance_size */
	, sizeof (RemotingConfiguration_t1126)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingConfiguration_t1126_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 2/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ConfigHandler
#include "mscorlib_System_Runtime_Remoting_ConfigHandler.h"
// Metadata Definition System.Runtime.Remoting.ConfigHandler
extern TypeInfo ConfigHandler_t1128_il2cpp_TypeInfo;
// System.Runtime.Remoting.ConfigHandler
#include "mscorlib_System_Runtime_Remoting_ConfigHandlerMethodDeclarations.h"
static const EncodedMethodIndex ConfigHandler_t1128_VTable[11] = 
{
	120,
	125,
	122,
	123,
	2656,
	2657,
	2658,
	2659,
	2660,
	2661,
	2662,
};
extern const Il2CppType IContentHandler_t860_0_0_0;
static const Il2CppType* ConfigHandler_t1128_InterfacesTypeInfos[] = 
{
	&IContentHandler_t860_0_0_0,
};
static Il2CppInterfaceOffsetPair ConfigHandler_t1128_InterfacesOffsets[] = 
{
	{ &IContentHandler_t860_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConfigHandler_t1128_0_0_0;
extern const Il2CppType ConfigHandler_t1128_1_0_0;
struct ConfigHandler_t1128;
const Il2CppTypeDefinitionMetadata ConfigHandler_t1128_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConfigHandler_t1128_InterfacesTypeInfos/* implementedInterfaces */
	, ConfigHandler_t1128_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ConfigHandler_t1128_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4466/* fieldStart */
	, 6850/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConfigHandler_t1128_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConfigHandler"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ConfigHandler_t1128_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConfigHandler_t1128_0_0_0/* byval_arg */
	, &ConfigHandler_t1128_1_0_0/* this_arg */
	, &ConfigHandler_t1128_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConfigHandler_t1128)/* instance_size */
	, sizeof (ConfigHandler_t1128)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConfigHandler_t1128_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.ChannelData
#include "mscorlib_System_Runtime_Remoting_ChannelData.h"
// Metadata Definition System.Runtime.Remoting.ChannelData
extern TypeInfo ChannelData_t1127_il2cpp_TypeInfo;
// System.Runtime.Remoting.ChannelData
#include "mscorlib_System_Runtime_Remoting_ChannelDataMethodDeclarations.h"
static const EncodedMethodIndex ChannelData_t1127_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ChannelData_t1127_0_0_0;
extern const Il2CppType ChannelData_t1127_1_0_0;
struct ChannelData_t1127;
const Il2CppTypeDefinitionMetadata ChannelData_t1127_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelData_t1127_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4476/* fieldStart */
	, 6874/* methodStart */
	, -1/* eventStart */
	, 1444/* propertyStart */

};
TypeInfo ChannelData_t1127_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelData"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ChannelData_t1127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ChannelData_t1127_0_0_0/* byval_arg */
	, &ChannelData_t1127_1_0_0/* this_arg */
	, &ChannelData_t1127_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelData_t1127)/* instance_size */
	, sizeof (ChannelData_t1127)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ProviderData
#include "mscorlib_System_Runtime_Remoting_ProviderData.h"
// Metadata Definition System.Runtime.Remoting.ProviderData
extern TypeInfo ProviderData_t1129_il2cpp_TypeInfo;
// System.Runtime.Remoting.ProviderData
#include "mscorlib_System_Runtime_Remoting_ProviderDataMethodDeclarations.h"
static const EncodedMethodIndex ProviderData_t1129_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ProviderData_t1129_0_0_0;
extern const Il2CppType ProviderData_t1129_1_0_0;
struct ProviderData_t1129;
const Il2CppTypeDefinitionMetadata ProviderData_t1129_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ProviderData_t1129_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4483/* fieldStart */
	, 6879/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ProviderData_t1129_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProviderData"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ProviderData_t1129_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ProviderData_t1129_0_0_0/* byval_arg */
	, &ProviderData_t1129_1_0_0/* this_arg */
	, &ProviderData_t1129_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProviderData_t1129)/* instance_size */
	, sizeof (ProviderData_t1129)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.FormatterData
#include "mscorlib_System_Runtime_Remoting_FormatterData.h"
// Metadata Definition System.Runtime.Remoting.FormatterData
extern TypeInfo FormatterData_t1130_il2cpp_TypeInfo;
// System.Runtime.Remoting.FormatterData
#include "mscorlib_System_Runtime_Remoting_FormatterDataMethodDeclarations.h"
static const EncodedMethodIndex FormatterData_t1130_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterData_t1130_0_0_0;
extern const Il2CppType FormatterData_t1130_1_0_0;
struct FormatterData_t1130;
const Il2CppTypeDefinitionMetadata FormatterData_t1130_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ProviderData_t1129_0_0_0/* parent */
	, FormatterData_t1130_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6881/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormatterData_t1130_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterData"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &FormatterData_t1130_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FormatterData_t1130_0_0_0/* byval_arg */
	, &FormatterData_t1130_1_0_0/* this_arg */
	, &FormatterData_t1130_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterData_t1130)/* instance_size */
	, sizeof (FormatterData_t1130)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingException.h"
// Metadata Definition System.Runtime.Remoting.RemotingException
extern TypeInfo RemotingException_t1131_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingExceptionMethodDeclarations.h"
static const EncodedMethodIndex RemotingException_t1131_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
extern const Il2CppType _Exception_t2066_0_0_0;
static Il2CppInterfaceOffsetPair RemotingException_t1131_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingException_t1131_0_0_0;
extern const Il2CppType RemotingException_t1131_1_0_0;
extern const Il2CppType SystemException_t597_0_0_0;
struct RemotingException_t1131;
const Il2CppTypeDefinitionMetadata RemotingException_t1131_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RemotingException_t1131_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t597_0_0_0/* parent */
	, RemotingException_t1131_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6882/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RemotingException_t1131_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingException"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &RemotingException_t1131_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1135/* custom_attributes_cache */
	, &RemotingException_t1131_0_0_0/* byval_arg */
	, &RemotingException_t1131_1_0_0/* this_arg */
	, &RemotingException_t1131_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingException_t1131)/* instance_size */
	, sizeof (RemotingException_t1131)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
