﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// <Module>
#include "UnityEngine_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo;
// <Module>
#include "UnityEngine_U3CModuleU3EMethodDeclarations.h"
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CModuleU3E_t0_0_0_0;
extern const Il2CppType U3CModuleU3E_t0_1_0_0;
struct U3CModuleU3E_t0;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t0_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CModuleU3E_t0_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t0_0_0_0/* byval_arg */
	, &U3CModuleU3E_t0_1_0_0/* this_arg */
	, &U3CModuleU3E_t0_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t0)/* instance_size */
	, sizeof (U3CModuleU3E_t0)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AssetBundleCreateRequest
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest.h"
// Metadata Definition UnityEngine.AssetBundleCreateRequest
extern TypeInfo AssetBundleCreateRequest_t1_il2cpp_TypeInfo;
// UnityEngine.AssetBundleCreateRequest
#include "UnityEngine_UnityEngine_AssetBundleCreateRequestMethodDeclarations.h"
static const EncodedMethodIndex AssetBundleCreateRequest_t1_VTable[4] = 
{
	120,
	121,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AssetBundleCreateRequest_t1_0_0_0;
extern const Il2CppType AssetBundleCreateRequest_t1_1_0_0;
extern const Il2CppType AsyncOperation_t2_0_0_0;
struct AssetBundleCreateRequest_t1;
const Il2CppTypeDefinitionMetadata AssetBundleCreateRequest_t1_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsyncOperation_t2_0_0_0/* parent */
	, AssetBundleCreateRequest_t1_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 0/* methodStart */
	, -1/* eventStart */
	, 0/* propertyStart */

};
TypeInfo AssetBundleCreateRequest_t1_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssetBundleCreateRequest"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AssetBundleCreateRequest_t1_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AssetBundleCreateRequest_t1_0_0_0/* byval_arg */
	, &AssetBundleCreateRequest_t1_1_0_0/* this_arg */
	, &AssetBundleCreateRequest_t1_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssetBundleCreateRequest_t1)/* instance_size */
	, sizeof (AssetBundleCreateRequest_t1)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AssetBundleRequest
#include "UnityEngine_UnityEngine_AssetBundleRequest.h"
// Metadata Definition UnityEngine.AssetBundleRequest
extern TypeInfo AssetBundleRequest_t4_il2cpp_TypeInfo;
// UnityEngine.AssetBundleRequest
#include "UnityEngine_UnityEngine_AssetBundleRequestMethodDeclarations.h"
static const EncodedMethodIndex AssetBundleRequest_t4_VTable[4] = 
{
	120,
	121,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AssetBundleRequest_t4_0_0_0;
extern const Il2CppType AssetBundleRequest_t4_1_0_0;
struct AssetBundleRequest_t4;
const Il2CppTypeDefinitionMetadata AssetBundleRequest_t4_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsyncOperation_t2_0_0_0/* parent */
	, AssetBundleRequest_t4_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 0/* fieldStart */
	, 3/* methodStart */
	, -1/* eventStart */
	, 1/* propertyStart */

};
TypeInfo AssetBundleRequest_t4_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssetBundleRequest"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AssetBundleRequest_t4_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AssetBundleRequest_t4_0_0_0/* byval_arg */
	, &AssetBundleRequest_t4_1_0_0/* this_arg */
	, &AssetBundleRequest_t4_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssetBundleRequest_t4)/* instance_size */
	, sizeof (AssetBundleRequest_t4)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AssetBundle
#include "UnityEngine_UnityEngine_AssetBundle.h"
// Metadata Definition UnityEngine.AssetBundle
extern TypeInfo AssetBundle_t3_il2cpp_TypeInfo;
// UnityEngine.AssetBundle
#include "UnityEngine_UnityEngine_AssetBundleMethodDeclarations.h"
static const EncodedMethodIndex AssetBundle_t3_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AssetBundle_t3_0_0_0;
extern const Il2CppType AssetBundle_t3_1_0_0;
extern const Il2CppType Object_t5_0_0_0;
struct AssetBundle_t3;
const Il2CppTypeDefinitionMetadata AssetBundle_t3_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t5_0_0_0/* parent */
	, AssetBundle_t3_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssetBundle_t3_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssetBundle"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AssetBundle_t3_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AssetBundle_t3_0_0_0/* byval_arg */
	, &AssetBundle_t3_1_0_0/* this_arg */
	, &AssetBundle_t3_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssetBundle_t3)/* instance_size */
	, sizeof (AssetBundle_t3)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
// Metadata Definition UnityEngine.SendMessageOptions
extern TypeInfo SendMessageOptions_t6_il2cpp_TypeInfo;
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptionsMethodDeclarations.h"
static const EncodedMethodIndex SendMessageOptions_t6_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
extern const Il2CppType IFormattable_t1406_0_0_0;
extern const Il2CppType IConvertible_t1409_0_0_0;
extern const Il2CppType IComparable_t1408_0_0_0;
static Il2CppInterfaceOffsetPair SendMessageOptions_t6_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SendMessageOptions_t6_0_0_0;
extern const Il2CppType SendMessageOptions_t6_1_0_0;
extern const Il2CppType Enum_t305_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t320_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata SendMessageOptions_t6_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SendMessageOptions_t6_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, SendMessageOptions_t6_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SendMessageOptions_t6_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SendMessageOptions"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SendMessageOptions_t6_0_0_0/* byval_arg */
	, &SendMessageOptions_t6_1_0_0/* this_arg */
	, &SendMessageOptions_t6_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SendMessageOptions_t6)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SendMessageOptions_t6)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// Metadata Definition UnityEngine.LayerMask
extern TypeInfo LayerMask_t7_il2cpp_TypeInfo;
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
static const EncodedMethodIndex LayerMask_t7_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LayerMask_t7_0_0_0;
extern const Il2CppType LayerMask_t7_1_0_0;
extern const Il2CppType ValueType_t757_0_0_0;
const Il2CppTypeDefinitionMetadata LayerMask_t7_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, LayerMask_t7_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6/* fieldStart */
	, 9/* methodStart */
	, -1/* eventStart */
	, 3/* propertyStart */

};
TypeInfo LayerMask_t7_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayerMask"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &LayerMask_t7_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayerMask_t7_0_0_0/* byval_arg */
	, &LayerMask_t7_1_0_0/* this_arg */
	, &LayerMask_t7_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayerMask_t7)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LayerMask_t7)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(LayerMask_t7 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"
// Metadata Definition UnityEngine.LogType
extern TypeInfo LogType_t8_il2cpp_TypeInfo;
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogTypeMethodDeclarations.h"
static const EncodedMethodIndex LogType_t8_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair LogType_t8_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LogType_t8_0_0_0;
extern const Il2CppType LogType_t8_1_0_0;
const Il2CppTypeDefinitionMetadata LogType_t8_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LogType_t8_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, LogType_t8_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LogType_t8_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogType"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LogType_t8_0_0_0/* byval_arg */
	, &LogType_t8_1_0_0/* this_arg */
	, &LogType_t8_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LogType_t8)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LogType_t8)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfo.h"
// Metadata Definition UnityEngine.SystemInfo
extern TypeInfo SystemInfo_t9_il2cpp_TypeInfo;
// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
static const EncodedMethodIndex SystemInfo_t9_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SystemInfo_t9_0_0_0;
extern const Il2CppType SystemInfo_t9_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SystemInfo_t9;
const Il2CppTypeDefinitionMetadata SystemInfo_t9_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SystemInfo_t9_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 16/* methodStart */
	, -1/* eventStart */
	, 4/* propertyStart */

};
TypeInfo SystemInfo_t9_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SystemInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SystemInfo_t9_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SystemInfo_t9_0_0_0/* byval_arg */
	, &SystemInfo_t9_1_0_0/* this_arg */
	, &SystemInfo_t9_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SystemInfo_t9)/* instance_size */
	, sizeof (SystemInfo_t9)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
// Metadata Definition UnityEngine.WaitForSeconds
extern TypeInfo WaitForSeconds_t10_il2cpp_TypeInfo;
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
static const EncodedMethodIndex WaitForSeconds_t10_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WaitForSeconds_t10_0_0_0;
extern const Il2CppType WaitForSeconds_t10_1_0_0;
extern const Il2CppType YieldInstruction_t11_0_0_0;
struct WaitForSeconds_t10;
const Il2CppTypeDefinitionMetadata WaitForSeconds_t10_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &YieldInstruction_t11_0_0_0/* parent */
	, WaitForSeconds_t10_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 13/* fieldStart */
	, 17/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WaitForSeconds_t10_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaitForSeconds"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WaitForSeconds_t10_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WaitForSeconds_t10_0_0_0/* byval_arg */
	, &WaitForSeconds_t10_1_0_0/* this_arg */
	, &WaitForSeconds_t10_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)WaitForSeconds_t10_marshal/* marshal_to_native_func */
	, (methodPointerType)WaitForSeconds_t10_marshal_back/* marshal_from_native_func */
	, (methodPointerType)WaitForSeconds_t10_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (WaitForSeconds_t10)/* instance_size */
	, sizeof (WaitForSeconds_t10)/* actualSize */
	, 0/* element_size */
	, sizeof(WaitForSeconds_t10_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WaitForFixedUpdate
#include "UnityEngine_UnityEngine_WaitForFixedUpdate.h"
// Metadata Definition UnityEngine.WaitForFixedUpdate
extern TypeInfo WaitForFixedUpdate_t12_il2cpp_TypeInfo;
// UnityEngine.WaitForFixedUpdate
#include "UnityEngine_UnityEngine_WaitForFixedUpdateMethodDeclarations.h"
static const EncodedMethodIndex WaitForFixedUpdate_t12_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WaitForFixedUpdate_t12_0_0_0;
extern const Il2CppType WaitForFixedUpdate_t12_1_0_0;
struct WaitForFixedUpdate_t12;
const Il2CppTypeDefinitionMetadata WaitForFixedUpdate_t12_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &YieldInstruction_t11_0_0_0/* parent */
	, WaitForFixedUpdate_t12_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 18/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WaitForFixedUpdate_t12_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaitForFixedUpdate"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WaitForFixedUpdate_t12_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WaitForFixedUpdate_t12_0_0_0/* byval_arg */
	, &WaitForFixedUpdate_t12_1_0_0/* this_arg */
	, &WaitForFixedUpdate_t12_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaitForFixedUpdate_t12)/* instance_size */
	, sizeof (WaitForFixedUpdate_t12)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
// Metadata Definition UnityEngine.WaitForEndOfFrame
extern TypeInfo WaitForEndOfFrame_t13_il2cpp_TypeInfo;
// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrameMethodDeclarations.h"
static const EncodedMethodIndex WaitForEndOfFrame_t13_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WaitForEndOfFrame_t13_0_0_0;
extern const Il2CppType WaitForEndOfFrame_t13_1_0_0;
struct WaitForEndOfFrame_t13;
const Il2CppTypeDefinitionMetadata WaitForEndOfFrame_t13_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &YieldInstruction_t11_0_0_0/* parent */
	, WaitForEndOfFrame_t13_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 19/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WaitForEndOfFrame_t13_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaitForEndOfFrame"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WaitForEndOfFrame_t13_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WaitForEndOfFrame_t13_0_0_0/* byval_arg */
	, &WaitForEndOfFrame_t13_1_0_0/* this_arg */
	, &WaitForEndOfFrame_t13_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaitForEndOfFrame_t13)/* instance_size */
	, sizeof (WaitForEndOfFrame_t13)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"
// Metadata Definition UnityEngine.Coroutine
extern TypeInfo Coroutine_t14_il2cpp_TypeInfo;
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_CoroutineMethodDeclarations.h"
static const EncodedMethodIndex Coroutine_t14_VTable[4] = 
{
	120,
	153,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Coroutine_t14_0_0_0;
extern const Il2CppType Coroutine_t14_1_0_0;
struct Coroutine_t14;
const Il2CppTypeDefinitionMetadata Coroutine_t14_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &YieldInstruction_t11_0_0_0/* parent */
	, Coroutine_t14_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 14/* fieldStart */
	, 20/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Coroutine_t14_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Coroutine"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Coroutine_t14_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Coroutine_t14_0_0_0/* byval_arg */
	, &Coroutine_t14_1_0_0/* this_arg */
	, &Coroutine_t14_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Coroutine_t14_marshal/* marshal_to_native_func */
	, (methodPointerType)Coroutine_t14_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Coroutine_t14_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Coroutine_t14)/* instance_size */
	, sizeof (Coroutine_t14)/* actualSize */
	, 0/* element_size */
	, sizeof(Coroutine_t14_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObject.h"
// Metadata Definition UnityEngine.ScriptableObject
extern TypeInfo ScriptableObject_t15_il2cpp_TypeInfo;
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
extern const Il2CppType ScriptableObject_CreateInstance_m12753_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition ScriptableObject_CreateInstance_m12753_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 2223 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 2223 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex ScriptableObject_t15_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ScriptableObject_t15_0_0_0;
extern const Il2CppType ScriptableObject_t15_1_0_0;
struct ScriptableObject_t15;
const Il2CppTypeDefinitionMetadata ScriptableObject_t15_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t5_0_0_0/* parent */
	, ScriptableObject_t15_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 23/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ScriptableObject_t15_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScriptableObject"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ScriptableObject_t15_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScriptableObject_t15_0_0_0/* byval_arg */
	, &ScriptableObject_t15_1_0_0/* this_arg */
	, &ScriptableObject_t15_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)ScriptableObject_t15_marshal/* marshal_to_native_func */
	, (methodPointerType)ScriptableObject_t15_marshal_back/* marshal_from_native_func */
	, (methodPointerType)ScriptableObject_t15_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (ScriptableObject_t15)/* instance_size */
	, sizeof (ScriptableObject_t15)/* actualSize */
	, 0/* element_size */
	, sizeof(ScriptableObject_t15_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCente.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
extern TypeInfo GameCenterPlatform_t25_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCenteMethodDeclarations.h"
static const EncodedMethodIndex GameCenterPlatform_t25_VTable[19] = 
{
	120,
	125,
	122,
	123,
	154,
	155,
	156,
	157,
	158,
	159,
	160,
	161,
	162,
	163,
	164,
	165,
	166,
	167,
	168,
};
extern const Il2CppType ISocialPlatform_t2044_0_0_0;
static const Il2CppType* GameCenterPlatform_t25_InterfacesTypeInfos[] = 
{
	&ISocialPlatform_t2044_0_0_0,
};
static Il2CppInterfaceOffsetPair GameCenterPlatform_t25_InterfacesOffsets[] = 
{
	{ &ISocialPlatform_t2044_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GameCenterPlatform_t25_0_0_0;
extern const Il2CppType GameCenterPlatform_t25_1_0_0;
struct GameCenterPlatform_t25;
const Il2CppTypeDefinitionMetadata GameCenterPlatform_t25_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, GameCenterPlatform_t25_InterfacesTypeInfos/* implementedInterfaces */
	, GameCenterPlatform_t25_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GameCenterPlatform_t25_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 15/* fieldStart */
	, 29/* methodStart */
	, -1/* eventStart */
	, 5/* propertyStart */

};
TypeInfo GameCenterPlatform_t25_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GameCenterPlatform"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, NULL/* methods */
	, &GameCenterPlatform_t25_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GameCenterPlatform_t25_0_0_0/* byval_arg */
	, &GameCenterPlatform_t25_1_0_0/* this_arg */
	, &GameCenterPlatform_t25_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GameCenterPlatform_t25)/* instance_size */
	, sizeof (GameCenterPlatform_t25)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GameCenterPlatform_t25_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 61/* method_count */
	, 1/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderb.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern TypeInfo GcLeaderboard_t27_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderbMethodDeclarations.h"
static const EncodedMethodIndex GcLeaderboard_t27_VTable[4] = 
{
	120,
	169,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcLeaderboard_t27_0_0_0;
extern const Il2CppType GcLeaderboard_t27_1_0_0;
struct GcLeaderboard_t27;
const Il2CppTypeDefinitionMetadata GcLeaderboard_t27_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GcLeaderboard_t27_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 30/* fieldStart */
	, 90/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GcLeaderboard_t27_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcLeaderboard"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, NULL/* methods */
	, &GcLeaderboard_t27_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcLeaderboard_t27_0_0_0/* byval_arg */
	, &GcLeaderboard_t27_1_0_0/* this_arg */
	, &GcLeaderboard_t27_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcLeaderboard_t27)/* instance_size */
	, sizeof (GcLeaderboard_t27)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"
// Metadata Definition UnityEngine.BoneWeight
extern TypeInfo BoneWeight_t28_il2cpp_TypeInfo;
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeightMethodDeclarations.h"
static const EncodedMethodIndex BoneWeight_t28_VTable[4] = 
{
	170,
	125,
	171,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType BoneWeight_t28_0_0_0;
extern const Il2CppType BoneWeight_t28_1_0_0;
const Il2CppTypeDefinitionMetadata BoneWeight_t28_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, BoneWeight_t28_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 32/* fieldStart */
	, 101/* methodStart */
	, -1/* eventStart */
	, 6/* propertyStart */

};
TypeInfo BoneWeight_t28_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BoneWeight"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &BoneWeight_t28_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BoneWeight_t28_0_0_0/* byval_arg */
	, &BoneWeight_t28_1_0_0/* this_arg */
	, &BoneWeight_t28_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BoneWeight_t28)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BoneWeight_t28)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(BoneWeight_t28 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 20/* method_count */
	, 8/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_Screen.h"
// Metadata Definition UnityEngine.Screen
extern TypeInfo Screen_t29_il2cpp_TypeInfo;
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
static const EncodedMethodIndex Screen_t29_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Screen_t29_0_0_0;
extern const Il2CppType Screen_t29_1_0_0;
struct Screen_t29;
const Il2CppTypeDefinitionMetadata Screen_t29_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Screen_t29_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 121/* methodStart */
	, -1/* eventStart */
	, 14/* propertyStart */

};
TypeInfo Screen_t29_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Screen"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Screen_t29_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Screen_t29_0_0_0/* byval_arg */
	, &Screen_t29_1_0_0/* this_arg */
	, &Screen_t29_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Screen_t29)/* instance_size */
	, sizeof (Screen_t29)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// Metadata Definition UnityEngine.Texture
extern TypeInfo Texture_t30_il2cpp_TypeInfo;
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
static const EncodedMethodIndex Texture_t30_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Texture_t30_0_0_0;
extern const Il2CppType Texture_t30_1_0_0;
struct Texture_t30;
const Il2CppTypeDefinitionMetadata Texture_t30_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t5_0_0_0/* parent */
	, Texture_t30_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 123/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Texture_t30_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Texture"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Texture_t30_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Texture_t30_0_0_0/* byval_arg */
	, &Texture_t30_1_0_0/* this_arg */
	, &Texture_t30_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Texture_t30)/* instance_size */
	, sizeof (Texture_t30)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// Metadata Definition UnityEngine.Texture2D
extern TypeInfo Texture2D_t31_il2cpp_TypeInfo;
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
static const EncodedMethodIndex Texture2D_t31_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Texture2D_t31_0_0_0;
extern const Il2CppType Texture2D_t31_1_0_0;
struct Texture2D_t31;
const Il2CppTypeDefinitionMetadata Texture2D_t31_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Texture_t30_0_0_0/* parent */
	, Texture2D_t31_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 124/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Texture2D_t31_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Texture2D"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Texture2D_t31_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Texture2D_t31_0_0_0/* byval_arg */
	, &Texture2D_t31_1_0_0/* this_arg */
	, &Texture2D_t31_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Texture2D_t31)/* instance_size */
	, sizeof (Texture2D_t31)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTexture.h"
// Metadata Definition UnityEngine.RenderTexture
extern TypeInfo RenderTexture_t32_il2cpp_TypeInfo;
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTextureMethodDeclarations.h"
static const EncodedMethodIndex RenderTexture_t32_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderTexture_t32_0_0_0;
extern const Il2CppType RenderTexture_t32_1_0_0;
struct RenderTexture_t32;
const Il2CppTypeDefinitionMetadata RenderTexture_t32_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Texture_t30_0_0_0/* parent */
	, RenderTexture_t32_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RenderTexture_t32_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTexture"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RenderTexture_t32_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderTexture_t32_0_0_0/* byval_arg */
	, &RenderTexture_t32_1_0_0/* this_arg */
	, &RenderTexture_t32_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderTexture_t32)/* instance_size */
	, sizeof (RenderTexture_t32)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.ReflectionProbe
#include "UnityEngine_UnityEngine_ReflectionProbe.h"
// Metadata Definition UnityEngine.ReflectionProbe
extern TypeInfo ReflectionProbe_t33_il2cpp_TypeInfo;
// UnityEngine.ReflectionProbe
#include "UnityEngine_UnityEngine_ReflectionProbeMethodDeclarations.h"
static const EncodedMethodIndex ReflectionProbe_t33_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionProbe_t33_0_0_0;
extern const Il2CppType ReflectionProbe_t33_1_0_0;
extern const Il2CppType Behaviour_t34_0_0_0;
struct ReflectionProbe_t33;
const Il2CppTypeDefinitionMetadata ReflectionProbe_t33_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t34_0_0_0/* parent */
	, ReflectionProbe_t33_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReflectionProbe_t33_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionProbe"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ReflectionProbe_t33_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReflectionProbe_t33_0_0_0/* byval_arg */
	, &ReflectionProbe_t33_1_0_0/* this_arg */
	, &ReflectionProbe_t33_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionProbe_t33)/* instance_size */
	, sizeof (ReflectionProbe_t33)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUIElement
#include "UnityEngine_UnityEngine_GUIElement.h"
// Metadata Definition UnityEngine.GUIElement
extern TypeInfo GUIElement_t35_il2cpp_TypeInfo;
// UnityEngine.GUIElement
#include "UnityEngine_UnityEngine_GUIElementMethodDeclarations.h"
static const EncodedMethodIndex GUIElement_t35_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIElement_t35_0_0_0;
extern const Il2CppType GUIElement_t35_1_0_0;
struct GUIElement_t35;
const Il2CppTypeDefinitionMetadata GUIElement_t35_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t34_0_0_0/* parent */
	, GUIElement_t35_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUIElement_t35_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIElement"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIElement_t35_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIElement_t35_0_0_0/* byval_arg */
	, &GUIElement_t35_1_0_0/* this_arg */
	, &GUIElement_t35_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIElement_t35)/* instance_size */
	, sizeof (GUIElement_t35)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayer
#include "UnityEngine_UnityEngine_GUILayer.h"
// Metadata Definition UnityEngine.GUILayer
extern TypeInfo GUILayer_t36_il2cpp_TypeInfo;
// UnityEngine.GUILayer
#include "UnityEngine_UnityEngine_GUILayerMethodDeclarations.h"
static const EncodedMethodIndex GUILayer_t36_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUILayer_t36_0_0_0;
extern const Il2CppType GUILayer_t36_1_0_0;
struct GUILayer_t36;
const Il2CppTypeDefinitionMetadata GUILayer_t36_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t34_0_0_0/* parent */
	, GUILayer_t36_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 126/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUILayer_t36_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayer"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUILayer_t36_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUILayer_t36_0_0_0/* byval_arg */
	, &GUILayer_t36_1_0_0/* this_arg */
	, &GUILayer_t36_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUILayer_t36)/* instance_size */
	, sizeof (GUILayer_t36)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GradientColorKey
#include "UnityEngine_UnityEngine_GradientColorKey.h"
// Metadata Definition UnityEngine.GradientColorKey
extern TypeInfo GradientColorKey_t37_il2cpp_TypeInfo;
// UnityEngine.GradientColorKey
#include "UnityEngine_UnityEngine_GradientColorKeyMethodDeclarations.h"
static const EncodedMethodIndex GradientColorKey_t37_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GradientColorKey_t37_0_0_0;
extern const Il2CppType GradientColorKey_t37_1_0_0;
const Il2CppTypeDefinitionMetadata GradientColorKey_t37_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, GradientColorKey_t37_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 40/* fieldStart */
	, 128/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GradientColorKey_t37_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GradientColorKey"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GradientColorKey_t37_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GradientColorKey_t37_0_0_0/* byval_arg */
	, &GradientColorKey_t37_1_0_0/* this_arg */
	, &GradientColorKey_t37_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GradientColorKey_t37)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GradientColorKey_t37)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GradientColorKey_t37 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GradientAlphaKey
#include "UnityEngine_UnityEngine_GradientAlphaKey.h"
// Metadata Definition UnityEngine.GradientAlphaKey
extern TypeInfo GradientAlphaKey_t39_il2cpp_TypeInfo;
// UnityEngine.GradientAlphaKey
#include "UnityEngine_UnityEngine_GradientAlphaKeyMethodDeclarations.h"
static const EncodedMethodIndex GradientAlphaKey_t39_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GradientAlphaKey_t39_0_0_0;
extern const Il2CppType GradientAlphaKey_t39_1_0_0;
const Il2CppTypeDefinitionMetadata GradientAlphaKey_t39_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, GradientAlphaKey_t39_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 42/* fieldStart */
	, 129/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GradientAlphaKey_t39_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GradientAlphaKey"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GradientAlphaKey_t39_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GradientAlphaKey_t39_0_0_0/* byval_arg */
	, &GradientAlphaKey_t39_1_0_0/* this_arg */
	, &GradientAlphaKey_t39_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GradientAlphaKey_t39)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GradientAlphaKey_t39)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GradientAlphaKey_t39 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Gradient
#include "UnityEngine_UnityEngine_Gradient.h"
// Metadata Definition UnityEngine.Gradient
extern TypeInfo Gradient_t40_il2cpp_TypeInfo;
// UnityEngine.Gradient
#include "UnityEngine_UnityEngine_GradientMethodDeclarations.h"
static const EncodedMethodIndex Gradient_t40_VTable[4] = 
{
	120,
	172,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Gradient_t40_0_0_0;
extern const Il2CppType Gradient_t40_1_0_0;
struct Gradient_t40;
const Il2CppTypeDefinitionMetadata Gradient_t40_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Gradient_t40_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 44/* fieldStart */
	, 130/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Gradient_t40_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Gradient"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Gradient_t40_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Gradient_t40_0_0_0/* byval_arg */
	, &Gradient_t40_1_0_0/* this_arg */
	, &Gradient_t40_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Gradient_t40_marshal/* marshal_to_native_func */
	, (methodPointerType)Gradient_t40_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Gradient_t40_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Gradient_t40)/* instance_size */
	, sizeof (Gradient_t40)/* actualSize */
	, 0/* element_size */
	, sizeof(Gradient_t40_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUI.h"
// Metadata Definition UnityEngine.GUI
extern TypeInfo GUI_t50_il2cpp_TypeInfo;
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
extern const Il2CppType ScrollViewState_t41_0_0_0;
extern const Il2CppType WindowFunction_t46_0_0_0;
static const Il2CppType* GUI_t50_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ScrollViewState_t41_0_0_0,
	&WindowFunction_t46_0_0_0,
};
static const EncodedMethodIndex GUI_t50_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUI_t50_0_0_0;
extern const Il2CppType GUI_t50_1_0_0;
struct GUI_t50;
const Il2CppTypeDefinitionMetadata GUI_t50_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GUI_t50_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUI_t50_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 45/* fieldStart */
	, 134/* methodStart */
	, -1/* eventStart */
	, 16/* propertyStart */

};
TypeInfo GUI_t50_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUI"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUI_t50_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUI_t50_0_0_0/* byval_arg */
	, &GUI_t50_1_0_0/* this_arg */
	, &GUI_t50_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUI_t50)/* instance_size */
	, sizeof (GUI_t50)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUI_t50_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUI/ScrollViewState
#include "UnityEngine_UnityEngine_GUI_ScrollViewState.h"
// Metadata Definition UnityEngine.GUI/ScrollViewState
extern TypeInfo ScrollViewState_t41_il2cpp_TypeInfo;
// UnityEngine.GUI/ScrollViewState
#include "UnityEngine_UnityEngine_GUI_ScrollViewStateMethodDeclarations.h"
static const EncodedMethodIndex ScrollViewState_t41_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ScrollViewState_t41_1_0_0;
struct ScrollViewState_t41;
const Il2CppTypeDefinitionMetadata ScrollViewState_t41_DefinitionMetadata = 
{
	&GUI_t50_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ScrollViewState_t41_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 60/* fieldStart */
	, 140/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ScrollViewState_t41_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollViewState"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ScrollViewState_t41_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScrollViewState_t41_0_0_0/* byval_arg */
	, &ScrollViewState_t41_1_0_0/* this_arg */
	, &ScrollViewState_t41_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollViewState_t41)/* instance_size */
	, sizeof (ScrollViewState_t41)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048837/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
// Metadata Definition UnityEngine.GUI/WindowFunction
extern TypeInfo WindowFunction_t46_il2cpp_TypeInfo;
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
static const EncodedMethodIndex WindowFunction_t46_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	180,
	181,
	182,
};
extern const Il2CppType ICloneable_t2056_0_0_0;
extern const Il2CppType ISerializable_t1426_0_0_0;
static Il2CppInterfaceOffsetPair WindowFunction_t46_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WindowFunction_t46_1_0_0;
extern const Il2CppType MulticastDelegate_t47_0_0_0;
struct WindowFunction_t46;
const Il2CppTypeDefinitionMetadata WindowFunction_t46_DefinitionMetadata = 
{
	&GUI_t50_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WindowFunction_t46_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, WindowFunction_t46_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 141/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WindowFunction_t46_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WindowFunction"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &WindowFunction_t46_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WindowFunction_t46_0_0_0/* byval_arg */
	, &WindowFunction_t46_1_0_0/* this_arg */
	, &WindowFunction_t46_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_WindowFunction_t46/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WindowFunction_t46)/* instance_size */
	, sizeof (WindowFunction_t46)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.GUILayout
#include "UnityEngine_UnityEngine_GUILayout.h"
// Metadata Definition UnityEngine.GUILayout
extern TypeInfo GUILayout_t52_il2cpp_TypeInfo;
// UnityEngine.GUILayout
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
static const EncodedMethodIndex GUILayout_t52_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUILayout_t52_0_0_0;
extern const Il2CppType GUILayout_t52_1_0_0;
struct GUILayout_t52;
const Il2CppTypeDefinitionMetadata GUILayout_t52_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUILayout_t52_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 145/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUILayout_t52_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayout"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUILayout_t52_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUILayout_t52_0_0_0/* byval_arg */
	, &GUILayout_t52_1_0_0/* this_arg */
	, &GUILayout_t52_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUILayout_t52)/* instance_size */
	, sizeof (GUILayout_t52)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtility.h"
// Metadata Definition UnityEngine.GUILayoutUtility
extern TypeInfo GUILayoutUtility_t57_il2cpp_TypeInfo;
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
extern const Il2CppType LayoutCache_t54_0_0_0;
static const Il2CppType* GUILayoutUtility_t57_il2cpp_TypeInfo__nestedTypes[1] =
{
	&LayoutCache_t54_0_0_0,
};
static const EncodedMethodIndex GUILayoutUtility_t57_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUILayoutUtility_t57_0_0_0;
extern const Il2CppType GUILayoutUtility_t57_1_0_0;
struct GUILayoutUtility_t57;
const Il2CppTypeDefinitionMetadata GUILayoutUtility_t57_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GUILayoutUtility_t57_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUILayoutUtility_t57_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 66/* fieldStart */
	, 147/* methodStart */
	, -1/* eventStart */
	, 19/* propertyStart */

};
TypeInfo GUILayoutUtility_t57_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayoutUtility"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUILayoutUtility_t57_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUILayoutUtility_t57_0_0_0/* byval_arg */
	, &GUILayoutUtility_t57_1_0_0/* this_arg */
	, &GUILayoutUtility_t57_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUILayoutUtility_t57)/* instance_size */
	, sizeof (GUILayoutUtility_t57)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUILayoutUtility_t57_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayoutUtility/LayoutCache
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCache.h"
// Metadata Definition UnityEngine.GUILayoutUtility/LayoutCache
extern TypeInfo LayoutCache_t54_il2cpp_TypeInfo;
// UnityEngine.GUILayoutUtility/LayoutCache
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCacheMethodDeclarations.h"
static const EncodedMethodIndex LayoutCache_t54_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LayoutCache_t54_1_0_0;
struct LayoutCache_t54;
const Il2CppTypeDefinitionMetadata LayoutCache_t54_DefinitionMetadata = 
{
	&GUILayoutUtility_t57_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LayoutCache_t54_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 71/* fieldStart */
	, 159/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LayoutCache_t54_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutCache"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &LayoutCache_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutCache_t54_0_0_0/* byval_arg */
	, &LayoutCache_t54_1_0_0/* this_arg */
	, &LayoutCache_t54_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutCache_t54)/* instance_size */
	, sizeof (LayoutCache_t54)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048837/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntry.h"
// Metadata Definition UnityEngine.GUILayoutEntry
extern TypeInfo GUILayoutEntry_t58_il2cpp_TypeInfo;
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntryMethodDeclarations.h"
static const EncodedMethodIndex GUILayoutEntry_t58_VTable[11] = 
{
	120,
	125,
	122,
	183,
	184,
	185,
	186,
	187,
	188,
	189,
	190,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUILayoutEntry_t58_0_0_0;
extern const Il2CppType GUILayoutEntry_t58_1_0_0;
struct GUILayoutEntry_t58;
const Il2CppTypeDefinitionMetadata GUILayoutEntry_t58_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUILayoutEntry_t58_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 74/* fieldStart */
	, 160/* methodStart */
	, -1/* eventStart */
	, 20/* propertyStart */

};
TypeInfo GUILayoutEntry_t58_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayoutEntry"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUILayoutEntry_t58_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUILayoutEntry_t58_0_0_0/* byval_arg */
	, &GUILayoutEntry_t58_1_0_0/* this_arg */
	, &GUILayoutEntry_t58_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUILayoutEntry_t58)/* instance_size */
	, sizeof (GUILayoutEntry_t58)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUILayoutEntry_t58_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 2/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroup.h"
// Metadata Definition UnityEngine.GUILayoutGroup
extern TypeInfo GUILayoutGroup_t53_il2cpp_TypeInfo;
// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroupMethodDeclarations.h"
static const EncodedMethodIndex GUILayoutGroup_t53_VTable[11] = 
{
	120,
	125,
	122,
	191,
	192,
	193,
	194,
	195,
	196,
	197,
	198,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUILayoutGroup_t53_0_0_0;
extern const Il2CppType GUILayoutGroup_t53_1_0_0;
struct GUILayoutGroup_t53;
const Il2CppTypeDefinitionMetadata GUILayoutGroup_t53_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &GUILayoutEntry_t58_0_0_0/* parent */
	, GUILayoutGroup_t53_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 84/* fieldStart */
	, 172/* methodStart */
	, -1/* eventStart */
	, 22/* propertyStart */

};
TypeInfo GUILayoutGroup_t53_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayoutGroup"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUILayoutGroup_t53_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUILayoutGroup_t53_0_0_0/* byval_arg */
	, &GUILayoutGroup_t53_1_0_0/* this_arg */
	, &GUILayoutGroup_t53_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUILayoutGroup_t53)/* instance_size */
	, sizeof (GUILayoutGroup_t53)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 1/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUIScrollGroup
#include "UnityEngine_UnityEngine_GUIScrollGroup.h"
// Metadata Definition UnityEngine.GUIScrollGroup
extern TypeInfo GUIScrollGroup_t61_il2cpp_TypeInfo;
// UnityEngine.GUIScrollGroup
#include "UnityEngine_UnityEngine_GUIScrollGroupMethodDeclarations.h"
static const EncodedMethodIndex GUIScrollGroup_t61_VTable[11] = 
{
	120,
	125,
	122,
	191,
	192,
	199,
	200,
	201,
	202,
	197,
	198,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIScrollGroup_t61_0_0_0;
extern const Il2CppType GUIScrollGroup_t61_1_0_0;
struct GUIScrollGroup_t61;
const Il2CppTypeDefinitionMetadata GUIScrollGroup_t61_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &GUILayoutGroup_t53_0_0_0/* parent */
	, GUIScrollGroup_t61_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 101/* fieldStart */
	, 182/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUIScrollGroup_t61_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIScrollGroup"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIScrollGroup_t61_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIScrollGroup_t61_0_0_0/* byval_arg */
	, &GUIScrollGroup_t61_1_0_0/* this_arg */
	, &GUIScrollGroup_t61_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIScrollGroup_t61)/* instance_size */
	, sizeof (GUIScrollGroup_t61)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayoutOption
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
// Metadata Definition UnityEngine.GUILayoutOption
extern TypeInfo GUILayoutOption_t63_il2cpp_TypeInfo;
// UnityEngine.GUILayoutOption
#include "UnityEngine_UnityEngine_GUILayoutOptionMethodDeclarations.h"
extern const Il2CppType Type_t62_0_0_0;
static const Il2CppType* GUILayoutOption_t63_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Type_t62_0_0_0,
};
static const EncodedMethodIndex GUILayoutOption_t63_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUILayoutOption_t63_0_0_0;
extern const Il2CppType GUILayoutOption_t63_1_0_0;
struct GUILayoutOption_t63;
const Il2CppTypeDefinitionMetadata GUILayoutOption_t63_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GUILayoutOption_t63_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUILayoutOption_t63_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 113/* fieldStart */
	, 187/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUILayoutOption_t63_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayoutOption"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUILayoutOption_t63_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUILayoutOption_t63_0_0_0/* byval_arg */
	, &GUILayoutOption_t63_1_0_0/* this_arg */
	, &GUILayoutOption_t63_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUILayoutOption_t63)/* instance_size */
	, sizeof (GUILayoutOption_t63)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayoutOption/Type
#include "UnityEngine_UnityEngine_GUILayoutOption_Type.h"
// Metadata Definition UnityEngine.GUILayoutOption/Type
extern TypeInfo Type_t62_il2cpp_TypeInfo;
// UnityEngine.GUILayoutOption/Type
#include "UnityEngine_UnityEngine_GUILayoutOption_TypeMethodDeclarations.h"
static const EncodedMethodIndex Type_t62_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair Type_t62_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Type_t62_1_0_0;
const Il2CppTypeDefinitionMetadata Type_t62_DefinitionMetadata = 
{
	&GUILayoutOption_t63_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Type_t62_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, Type_t62_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 115/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Type_t62_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Type"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Type_t62_0_0_0/* byval_arg */
	, &Type_t62_1_0_0/* this_arg */
	, &Type_t62_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Type_t62)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Type_t62)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 261/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.ExitGUIException
#include "UnityEngine_UnityEngine_ExitGUIException.h"
// Metadata Definition UnityEngine.ExitGUIException
extern TypeInfo ExitGUIException_t64_il2cpp_TypeInfo;
// UnityEngine.ExitGUIException
#include "UnityEngine_UnityEngine_ExitGUIExceptionMethodDeclarations.h"
static const EncodedMethodIndex ExitGUIException_t64_VTable[11] = 
{
	120,
	125,
	122,
	203,
	204,
	205,
	206,
	207,
	208,
	204,
	209,
};
extern const Il2CppType _Exception_t2066_0_0_0;
static Il2CppInterfaceOffsetPair ExitGUIException_t64_InterfacesOffsets[] = 
{
	{ &ISerializable_t1426_0_0_0, 4},
	{ &_Exception_t2066_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExitGUIException_t64_0_0_0;
extern const Il2CppType ExitGUIException_t64_1_0_0;
extern const Il2CppType Exception_t65_0_0_0;
struct ExitGUIException_t64;
const Il2CppTypeDefinitionMetadata ExitGUIException_t64_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExitGUIException_t64_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t65_0_0_0/* parent */
	, ExitGUIException_t64_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExitGUIException_t64_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExitGUIException"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ExitGUIException_t64_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExitGUIException_t64_0_0_0/* byval_arg */
	, &ExitGUIException_t64_1_0_0/* this_arg */
	, &ExitGUIException_t64_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExitGUIException_t64)/* instance_size */
	, sizeof (ExitGUIException_t64)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtility.h"
// Metadata Definition UnityEngine.GUIUtility
extern TypeInfo GUIUtility_t66_il2cpp_TypeInfo;
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
static const EncodedMethodIndex GUIUtility_t66_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIUtility_t66_0_0_0;
extern const Il2CppType GUIUtility_t66_1_0_0;
struct GUIUtility_t66;
const Il2CppTypeDefinitionMetadata GUIUtility_t66_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUIUtility_t66_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 130/* fieldStart */
	, 188/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUIUtility_t66_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIUtility"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIUtility_t66_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIUtility_t66_0_0_0/* byval_arg */
	, &GUIUtility_t66_1_0_0/* this_arg */
	, &GUIUtility_t66_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIUtility_t66)/* instance_size */
	, sizeof (GUIUtility_t66)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUIUtility_t66_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUISettings
#include "UnityEngine_UnityEngine_GUISettings.h"
// Metadata Definition UnityEngine.GUISettings
extern TypeInfo GUISettings_t67_il2cpp_TypeInfo;
// UnityEngine.GUISettings
#include "UnityEngine_UnityEngine_GUISettingsMethodDeclarations.h"
static const EncodedMethodIndex GUISettings_t67_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUISettings_t67_0_0_0;
extern const Il2CppType GUISettings_t67_1_0_0;
struct GUISettings_t67;
const Il2CppTypeDefinitionMetadata GUISettings_t67_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUISettings_t67_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 134/* fieldStart */
	, 197/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUISettings_t67_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUISettings"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUISettings_t67_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUISettings_t67_0_0_0/* byval_arg */
	, &GUISettings_t67_1_0_0/* this_arg */
	, &GUISettings_t67_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUISettings_t67)/* instance_size */
	, sizeof (GUISettings_t67)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkin.h"
// Metadata Definition UnityEngine.GUISkin
extern TypeInfo GUISkin_t48_il2cpp_TypeInfo;
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
extern const Il2CppType SkinChangedDelegate_t68_0_0_0;
static const Il2CppType* GUISkin_t48_il2cpp_TypeInfo__nestedTypes[1] =
{
	&SkinChangedDelegate_t68_0_0_0,
};
static const EncodedMethodIndex GUISkin_t48_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUISkin_t48_0_0_0;
extern const Il2CppType GUISkin_t48_1_0_0;
struct GUISkin_t48;
const Il2CppTypeDefinitionMetadata GUISkin_t48_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GUISkin_t48_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ScriptableObject_t15_0_0_0/* parent */
	, GUISkin_t48_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 139/* fieldStart */
	, 198/* methodStart */
	, -1/* eventStart */
	, 23/* propertyStart */

};
TypeInfo GUISkin_t48_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUISkin"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUISkin_t48_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 59/* custom_attributes_cache */
	, &GUISkin_t48_0_0_0/* byval_arg */
	, &GUISkin_t48_1_0_0/* this_arg */
	, &GUISkin_t48_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUISkin_t48)/* instance_size */
	, sizeof (GUISkin_t48)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUISkin_t48_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 54/* method_count */
	, 24/* property_count */
	, 27/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUISkin/SkinChangedDelegate
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegate.h"
// Metadata Definition UnityEngine.GUISkin/SkinChangedDelegate
extern TypeInfo SkinChangedDelegate_t68_il2cpp_TypeInfo;
// UnityEngine.GUISkin/SkinChangedDelegate
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegateMethodDeclarations.h"
static const EncodedMethodIndex SkinChangedDelegate_t68_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	210,
	211,
	212,
};
static Il2CppInterfaceOffsetPair SkinChangedDelegate_t68_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SkinChangedDelegate_t68_1_0_0;
struct SkinChangedDelegate_t68;
const Il2CppTypeDefinitionMetadata SkinChangedDelegate_t68_DefinitionMetadata = 
{
	&GUISkin_t48_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SkinChangedDelegate_t68_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, SkinChangedDelegate_t68_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 252/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SkinChangedDelegate_t68_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SkinChangedDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SkinChangedDelegate_t68_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SkinChangedDelegate_t68_0_0_0/* byval_arg */
	, &SkinChangedDelegate_t68_1_0_0/* this_arg */
	, &SkinChangedDelegate_t68_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_SkinChangedDelegate_t68/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SkinChangedDelegate_t68)/* instance_size */
	, sizeof (SkinChangedDelegate_t68)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 261/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// Metadata Definition UnityEngine.GUIContent
extern TypeInfo GUIContent_t72_il2cpp_TypeInfo;
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
static const EncodedMethodIndex GUIContent_t72_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIContent_t72_0_0_0;
extern const Il2CppType GUIContent_t72_1_0_0;
struct GUIContent_t72;
const Il2CppTypeDefinitionMetadata GUIContent_t72_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUIContent_t72_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 166/* fieldStart */
	, 256/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUIContent_t72_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIContent"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIContent_t72_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIContent_t72_0_0_0/* byval_arg */
	, &GUIContent_t72_1_0_0/* this_arg */
	, &GUIContent_t72_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIContent_t72)/* instance_size */
	, sizeof (GUIContent_t72)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUIContent_t72_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056777/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleState.h"
// Metadata Definition UnityEngine.GUIStyleState
extern TypeInfo GUIStyleState_t73_il2cpp_TypeInfo;
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"
static const EncodedMethodIndex GUIStyleState_t73_VTable[4] = 
{
	120,
	213,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIStyleState_t73_0_0_0;
extern const Il2CppType GUIStyleState_t73_1_0_0;
struct GUIStyleState_t73;
const Il2CppTypeDefinitionMetadata GUIStyleState_t73_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUIStyleState_t73_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 173/* fieldStart */
	, 260/* methodStart */
	, -1/* eventStart */
	, 47/* propertyStart */

};
TypeInfo GUIStyleState_t73_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIStyleState"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIStyleState_t73_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIStyleState_t73_0_0_0/* byval_arg */
	, &GUIStyleState_t73_1_0_0/* this_arg */
	, &GUIStyleState_t73_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIStyleState_t73)/* instance_size */
	, sizeof (GUIStyleState_t73)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffset.h"
// Metadata Definition UnityEngine.RectOffset
extern TypeInfo RectOffset_t60_il2cpp_TypeInfo;
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
static const EncodedMethodIndex RectOffset_t60_VTable[4] = 
{
	120,
	214,
	122,
	215,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RectOffset_t60_0_0_0;
extern const Il2CppType RectOffset_t60_1_0_0;
struct RectOffset_t60;
const Il2CppTypeDefinitionMetadata RectOffset_t60_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RectOffset_t60_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 176/* fieldStart */
	, 268/* methodStart */
	, -1/* eventStart */
	, 48/* propertyStart */

};
TypeInfo RectOffset_t60_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectOffset"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RectOffset_t60_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RectOffset_t60_0_0_0/* byval_arg */
	, &RectOffset_t60_1_0_0/* this_arg */
	, &RectOffset_t60_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RectOffset_t60)/* instance_size */
	, sizeof (RectOffset_t60)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 6/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// Metadata Definition UnityEngine.FontStyle
extern TypeInfo FontStyle_t74_il2cpp_TypeInfo;
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyleMethodDeclarations.h"
static const EncodedMethodIndex FontStyle_t74_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair FontStyle_t74_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FontStyle_t74_0_0_0;
extern const Il2CppType FontStyle_t74_1_0_0;
const Il2CppTypeDefinitionMetadata FontStyle_t74_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FontStyle_t74_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, FontStyle_t74_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 178/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FontStyle_t74_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FontStyle"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FontStyle_t74_0_0_0/* byval_arg */
	, &FontStyle_t74_1_0_0/* this_arg */
	, &FontStyle_t74_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FontStyle_t74)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FontStyle_t74)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// Metadata Definition UnityEngine.GUIStyle
extern TypeInfo GUIStyle_t56_il2cpp_TypeInfo;
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
static const EncodedMethodIndex GUIStyle_t56_VTable[4] = 
{
	120,
	216,
	122,
	217,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIStyle_t56_0_0_0;
extern const Il2CppType GUIStyle_t56_1_0_0;
struct GUIStyle_t56;
const Il2CppTypeDefinitionMetadata GUIStyle_t56_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUIStyle_t56_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 183/* fieldStart */
	, 285/* methodStart */
	, -1/* eventStart */
	, 54/* propertyStart */

};
TypeInfo GUIStyle_t56_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIStyle"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIStyle_t56_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIStyle_t56_0_0_0/* byval_arg */
	, &GUIStyle_t56_1_0_0/* this_arg */
	, &GUIStyle_t56_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIStyle_t56)/* instance_size */
	, sizeof (GUIStyle_t56)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUIStyle_t56_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 9/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TouchScreenKeyboard
#include "UnityEngine_UnityEngine_TouchScreenKeyboard.h"
// Metadata Definition UnityEngine.TouchScreenKeyboard
extern TypeInfo TouchScreenKeyboard_t75_il2cpp_TypeInfo;
// UnityEngine.TouchScreenKeyboard
#include "UnityEngine_UnityEngine_TouchScreenKeyboardMethodDeclarations.h"
static const EncodedMethodIndex TouchScreenKeyboard_t75_VTable[4] = 
{
	120,
	218,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TouchScreenKeyboard_t75_0_0_0;
extern const Il2CppType TouchScreenKeyboard_t75_1_0_0;
struct TouchScreenKeyboard_t75;
const Il2CppTypeDefinitionMetadata TouchScreenKeyboard_t75_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TouchScreenKeyboard_t75_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 306/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TouchScreenKeyboard_t75_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TouchScreenKeyboard"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TouchScreenKeyboard_t75_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TouchScreenKeyboard_t75_0_0_0/* byval_arg */
	, &TouchScreenKeyboard_t75_1_0_0/* this_arg */
	, &TouchScreenKeyboard_t75_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TouchScreenKeyboard_t75)/* instance_size */
	, sizeof (TouchScreenKeyboard_t75)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Event
#include "UnityEngine_UnityEngine_Event.h"
// Metadata Definition UnityEngine.Event
extern TypeInfo Event_t76_il2cpp_TypeInfo;
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
static const EncodedMethodIndex Event_t76_VTable[4] = 
{
	219,
	220,
	221,
	222,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Event_t76_0_0_0;
extern const Il2CppType Event_t76_1_0_0;
struct Event_t76;
const Il2CppTypeDefinitionMetadata Event_t76_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Event_t76_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 199/* fieldStart */
	, 308/* methodStart */
	, -1/* eventStart */
	, 63/* propertyStart */

};
TypeInfo Event_t76_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Event"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Event_t76_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Event_t76_0_0_0/* byval_arg */
	, &Event_t76_1_0_0/* this_arg */
	, &Event_t76_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Event_t76_marshal/* marshal_to_native_func */
	, (methodPointerType)Event_t76_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Event_t76_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Event_t76)/* instance_size */
	, sizeof (Event_t76)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Event_t76_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 9/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
// Metadata Definition UnityEngine.KeyCode
extern TypeInfo KeyCode_t78_il2cpp_TypeInfo;
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCodeMethodDeclarations.h"
static const EncodedMethodIndex KeyCode_t78_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair KeyCode_t78_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType KeyCode_t78_0_0_0;
extern const Il2CppType KeyCode_t78_1_0_0;
const Il2CppTypeDefinitionMetadata KeyCode_t78_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, KeyCode_t78_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, KeyCode_t78_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 203/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyCode_t78_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyCode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyCode_t78_0_0_0/* byval_arg */
	, &KeyCode_t78_1_0_0/* this_arg */
	, &KeyCode_t78_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyCode_t78)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (KeyCode_t78)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 322/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
// Metadata Definition UnityEngine.EventType
extern TypeInfo EventType_t79_il2cpp_TypeInfo;
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventTypeMethodDeclarations.h"
static const EncodedMethodIndex EventType_t79_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair EventType_t79_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType EventType_t79_0_0_0;
extern const Il2CppType EventType_t79_1_0_0;
const Il2CppTypeDefinitionMetadata EventType_t79_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventType_t79_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, EventType_t79_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 525/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventType_t79_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventType"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EventType_t79_0_0_0/* byval_arg */
	, &EventType_t79_1_0_0/* this_arg */
	, &EventType_t79_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventType_t79)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EventType_t79)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 31/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"
// Metadata Definition UnityEngine.EventModifiers
extern TypeInfo EventModifiers_t80_il2cpp_TypeInfo;
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiersMethodDeclarations.h"
static const EncodedMethodIndex EventModifiers_t80_VTable[23] = 
{
	128,
	125,
	129,
	130,
	131,
	132,
	133,
	134,
	135,
	136,
	137,
	138,
	139,
	140,
	141,
	142,
	143,
	144,
	145,
	146,
	147,
	148,
	149,
};
static Il2CppInterfaceOffsetPair EventModifiers_t80_InterfacesOffsets[] = 
{
	{ &IFormattable_t1406_0_0_0, 4},
	{ &IConvertible_t1409_0_0_0, 5},
	{ &IComparable_t1408_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType EventModifiers_t80_0_0_0;
extern const Il2CppType EventModifiers_t80_1_0_0;
const Il2CppTypeDefinitionMetadata EventModifiers_t80_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventModifiers_t80_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t305_0_0_0/* parent */
	, EventModifiers_t80_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 556/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventModifiers_t80_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventModifiers"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 126/* custom_attributes_cache */
	, &EventModifiers_t80_0_0_0/* byval_arg */
	, &EventModifiers_t80_1_0_0/* this_arg */
	, &EventModifiers_t80_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventModifiers_t80)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EventModifiers_t80)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Metadata Definition UnityEngine.Vector2
extern TypeInfo Vector2_t43_il2cpp_TypeInfo;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
static const EncodedMethodIndex Vector2_t43_VTable[4] = 
{
	223,
	125,
	224,
	225,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Vector2_t43_0_0_0;
extern const Il2CppType Vector2_t43_1_0_0;
const Il2CppTypeDefinitionMetadata Vector2_t43_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Vector2_t43_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 565/* fieldStart */
	, 327/* methodStart */
	, -1/* eventStart */
	, 72/* propertyStart */

};
TypeInfo Vector2_t43_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector2"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Vector2_t43_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 127/* custom_attributes_cache */
	, &Vector2_t43_0_0_0/* byval_arg */
	, &Vector2_t43_1_0_0/* this_arg */
	, &Vector2_t43_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector2_t43)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Vector2_t43)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Vector2_t43 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Metadata Definition UnityEngine.Vector3
extern TypeInfo Vector3_t81_il2cpp_TypeInfo;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
static const EncodedMethodIndex Vector3_t81_VTable[4] = 
{
	226,
	125,
	227,
	228,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Vector3_t81_0_0_0;
extern const Il2CppType Vector3_t81_1_0_0;
const Il2CppTypeDefinitionMetadata Vector3_t81_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Vector3_t81_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 567/* fieldStart */
	, 335/* methodStart */
	, -1/* eventStart */
	, 73/* propertyStart */

};
TypeInfo Vector3_t81_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector3"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Vector3_t81_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 128/* custom_attributes_cache */
	, &Vector3_t81_0_0_0/* byval_arg */
	, &Vector3_t81_1_0_0/* this_arg */
	, &Vector3_t81_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector3_t81)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Vector3_t81)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Vector3_t81 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 14/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// Metadata Definition UnityEngine.Color
extern TypeInfo Color_t38_il2cpp_TypeInfo;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
static const EncodedMethodIndex Color_t38_VTable[4] = 
{
	229,
	125,
	230,
	231,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Color_t38_0_0_0;
extern const Il2CppType Color_t38_1_0_0;
const Il2CppTypeDefinitionMetadata Color_t38_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Color_t38_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 570/* fieldStart */
	, 349/* methodStart */
	, -1/* eventStart */
	, 75/* propertyStart */

};
TypeInfo Color_t38_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Color"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Color_t38_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 129/* custom_attributes_cache */
	, &Color_t38_0_0_0/* byval_arg */
	, &Color_t38_1_0_0/* this_arg */
	, &Color_t38_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Color_t38)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Color_t38)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Color_t38 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// Metadata Definition UnityEngine.Color32
extern TypeInfo Color32_t82_il2cpp_TypeInfo;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
static const EncodedMethodIndex Color32_t82_VTable[4] = 
{
	150,
	125,
	151,
	232,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Color32_t82_0_0_0;
extern const Il2CppType Color32_t82_1_0_0;
const Il2CppTypeDefinitionMetadata Color32_t82_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Color32_t82_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 574/* fieldStart */
	, 358/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Color32_t82_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Color32"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Color32_t82_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 130/* custom_attributes_cache */
	, &Color32_t82_0_0_0/* byval_arg */
	, &Color32_t82_1_0_0/* this_arg */
	, &Color32_t82_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Color32_t82)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Color32_t82)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Color32_t82 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// Metadata Definition UnityEngine.Quaternion
extern TypeInfo Quaternion_t83_il2cpp_TypeInfo;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
static const EncodedMethodIndex Quaternion_t83_VTable[4] = 
{
	233,
	125,
	234,
	235,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Quaternion_t83_0_0_0;
extern const Il2CppType Quaternion_t83_1_0_0;
const Il2CppTypeDefinitionMetadata Quaternion_t83_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Quaternion_t83_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 578/* fieldStart */
	, 361/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Quaternion_t83_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Quaternion"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Quaternion_t83_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 131/* custom_attributes_cache */
	, &Quaternion_t83_0_0_0/* byval_arg */
	, &Quaternion_t83_1_0_0/* this_arg */
	, &Quaternion_t83_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Quaternion_t83)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Quaternion_t83)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Quaternion_t83 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// Metadata Definition UnityEngine.Rect
extern TypeInfo Rect_t42_il2cpp_TypeInfo;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
static const EncodedMethodIndex Rect_t42_VTable[4] = 
{
	236,
	125,
	237,
	238,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Rect_t42_0_0_0;
extern const Il2CppType Rect_t42_1_0_0;
const Il2CppTypeDefinitionMetadata Rect_t42_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Rect_t42_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 582/* fieldStart */
	, 364/* methodStart */
	, -1/* eventStart */
	, 77/* propertyStart */

};
TypeInfo Rect_t42_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Rect"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Rect_t42_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Rect_t42_0_0_0/* byval_arg */
	, &Rect_t42_1_0_0/* this_arg */
	, &Rect_t42_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Rect_t42)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Rect_t42)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Rect_t42 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 17/* method_count */
	, 8/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// Metadata Definition UnityEngine.Matrix4x4
extern TypeInfo Matrix4x4_t84_il2cpp_TypeInfo;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
static const EncodedMethodIndex Matrix4x4_t84_VTable[4] = 
{
	239,
	125,
	240,
	241,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Matrix4x4_t84_0_0_0;
extern const Il2CppType Matrix4x4_t84_1_0_0;
const Il2CppTypeDefinitionMetadata Matrix4x4_t84_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Matrix4x4_t84_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 586/* fieldStart */
	, 381/* methodStart */
	, -1/* eventStart */
	, 85/* propertyStart */

};
TypeInfo Matrix4x4_t84_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Matrix4x4"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Matrix4x4_t84_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 132/* custom_attributes_cache */
	, &Matrix4x4_t84_0_0_0/* byval_arg */
	, &Matrix4x4_t84_1_0_0/* this_arg */
	, &Matrix4x4_t84_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Matrix4x4_t84)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Matrix4x4_t84)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Matrix4x4_t84 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 36/* method_count */
	, 7/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
// Metadata Definition UnityEngine.Bounds
extern TypeInfo Bounds_t85_il2cpp_TypeInfo;
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
static const EncodedMethodIndex Bounds_t85_VTable[4] = 
{
	242,
	125,
	243,
	244,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Bounds_t85_0_0_0;
extern const Il2CppType Bounds_t85_1_0_0;
const Il2CppTypeDefinitionMetadata Bounds_t85_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Bounds_t85_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 602/* fieldStart */
	, 417/* methodStart */
	, -1/* eventStart */
	, 92/* propertyStart */

};
TypeInfo Bounds_t85_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Bounds"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Bounds_t85_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Bounds_t85_0_0_0/* byval_arg */
	, &Bounds_t85_1_0_0/* this_arg */
	, &Bounds_t85_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Bounds_t85)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Bounds_t85)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Bounds_t85 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 36/* method_count */
	, 5/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// Metadata Definition UnityEngine.Vector4
extern TypeInfo Vector4_t86_il2cpp_TypeInfo;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
static const EncodedMethodIndex Vector4_t86_VTable[4] = 
{
	245,
	125,
	246,
	247,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Vector4_t86_0_0_0;
extern const Il2CppType Vector4_t86_1_0_0;
const Il2CppTypeDefinitionMetadata Vector4_t86_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Vector4_t86_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 604/* fieldStart */
	, 453/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Vector4_t86_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector4"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Vector4_t86_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 144/* custom_attributes_cache */
	, &Vector4_t86_0_0_0/* byval_arg */
	, &Vector4_t86_1_0_0/* this_arg */
	, &Vector4_t86_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector4_t86)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Vector4_t86)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Vector4_t86 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// Metadata Definition UnityEngine.Ray
extern TypeInfo Ray_t87_il2cpp_TypeInfo;
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
static const EncodedMethodIndex Ray_t87_VTable[4] = 
{
	150,
	125,
	151,
	248,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Ray_t87_0_0_0;
extern const Il2CppType Ray_t87_1_0_0;
const Il2CppTypeDefinitionMetadata Ray_t87_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Ray_t87_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 608/* fieldStart */
	, 461/* methodStart */
	, -1/* eventStart */
	, 97/* propertyStart */

};
TypeInfo Ray_t87_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Ray"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Ray_t87_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Ray_t87_0_0_0/* byval_arg */
	, &Ray_t87_1_0_0/* this_arg */
	, &Ray_t87_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Ray_t87)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Ray_t87)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Ray_t87 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngineInternal.MathfInternal
#include "UnityEngine_UnityEngineInternal_MathfInternal.h"
// Metadata Definition UnityEngineInternal.MathfInternal
extern TypeInfo MathfInternal_t88_il2cpp_TypeInfo;
// UnityEngineInternal.MathfInternal
#include "UnityEngine_UnityEngineInternal_MathfInternalMethodDeclarations.h"
static const EncodedMethodIndex MathfInternal_t88_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MathfInternal_t88_0_0_0;
extern const Il2CppType MathfInternal_t88_1_0_0;
const Il2CppTypeDefinitionMetadata MathfInternal_t88_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, MathfInternal_t88_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 610/* fieldStart */
	, 463/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MathfInternal_t88_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MathfInternal"/* name */
	, "UnityEngineInternal"/* namespaze */
	, NULL/* methods */
	, &MathfInternal_t88_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MathfInternal_t88_0_0_0/* byval_arg */
	, &MathfInternal_t88_1_0_0/* this_arg */
	, &MathfInternal_t88_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MathfInternal_t88)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MathfInternal_t88)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(MathfInternal_t88 )/* native_size */
	, sizeof(MathfInternal_t88_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_Mathf.h"
// Metadata Definition UnityEngine.Mathf
extern TypeInfo Mathf_t89_il2cpp_TypeInfo;
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
static const EncodedMethodIndex Mathf_t89_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Mathf_t89_0_0_0;
extern const Il2CppType Mathf_t89_1_0_0;
const Il2CppTypeDefinitionMetadata Mathf_t89_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Mathf_t89_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 613/* fieldStart */
	, 464/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Mathf_t89_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mathf"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Mathf_t89_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mathf_t89_0_0_0/* byval_arg */
	, &Mathf_t89_1_0_0/* this_arg */
	, &Mathf_t89_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mathf_t89)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mathf_t89)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Mathf_t89 )/* native_size */
	, sizeof(Mathf_t89_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// Metadata Definition UnityEngine.RectTransform
extern TypeInfo RectTransform_t90_il2cpp_TypeInfo;
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
extern const Il2CppType ReapplyDrivenProperties_t91_0_0_0;
static const Il2CppType* RectTransform_t90_il2cpp_TypeInfo__nestedTypes[1] =
{
	&ReapplyDrivenProperties_t91_0_0_0,
};
static const EncodedMethodIndex RectTransform_t90_VTable[5] = 
{
	124,
	125,
	126,
	127,
	249,
};
extern const Il2CppType IEnumerable_t302_0_0_0;
static Il2CppInterfaceOffsetPair RectTransform_t90_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RectTransform_t90_0_0_0;
extern const Il2CppType RectTransform_t90_1_0_0;
extern const Il2CppType Transform_t92_0_0_0;
struct RectTransform_t90;
const Il2CppTypeDefinitionMetadata RectTransform_t90_DefinitionMetadata = 
{
	NULL/* declaringType */
	, RectTransform_t90_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RectTransform_t90_InterfacesOffsets/* interfaceOffsets */
	, &Transform_t92_0_0_0/* parent */
	, RectTransform_t90_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 614/* fieldStart */
	, 475/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RectTransform_t90_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectTransform"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RectTransform_t90_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RectTransform_t90_0_0_0/* byval_arg */
	, &RectTransform_t90_1_0_0/* this_arg */
	, &RectTransform_t90_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RectTransform_t90)/* instance_size */
	, sizeof (RectTransform_t90)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RectTransform_t90_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RectTransform/ReapplyDrivenProperties
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertie.h"
// Metadata Definition UnityEngine.RectTransform/ReapplyDrivenProperties
extern TypeInfo ReapplyDrivenProperties_t91_il2cpp_TypeInfo;
// UnityEngine.RectTransform/ReapplyDrivenProperties
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertieMethodDeclarations.h"
static const EncodedMethodIndex ReapplyDrivenProperties_t91_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	250,
	251,
	252,
};
static Il2CppInterfaceOffsetPair ReapplyDrivenProperties_t91_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReapplyDrivenProperties_t91_1_0_0;
struct ReapplyDrivenProperties_t91;
const Il2CppTypeDefinitionMetadata ReapplyDrivenProperties_t91_DefinitionMetadata = 
{
	&RectTransform_t90_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReapplyDrivenProperties_t91_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, ReapplyDrivenProperties_t91_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 476/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReapplyDrivenProperties_t91_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReapplyDrivenProperties"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ReapplyDrivenProperties_t91_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReapplyDrivenProperties_t91_0_0_0/* byval_arg */
	, &ReapplyDrivenProperties_t91_1_0_0/* this_arg */
	, &ReapplyDrivenProperties_t91_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ReapplyDrivenProperties_t91/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReapplyDrivenProperties_t91)/* instance_size */
	, sizeof (ReapplyDrivenProperties_t91)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.ResourceRequest
#include "UnityEngine_UnityEngine_ResourceRequest.h"
// Metadata Definition UnityEngine.ResourceRequest
extern TypeInfo ResourceRequest_t93_il2cpp_TypeInfo;
// UnityEngine.ResourceRequest
#include "UnityEngine_UnityEngine_ResourceRequestMethodDeclarations.h"
static const EncodedMethodIndex ResourceRequest_t93_VTable[4] = 
{
	120,
	121,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ResourceRequest_t93_0_0_0;
extern const Il2CppType ResourceRequest_t93_1_0_0;
struct ResourceRequest_t93;
const Il2CppTypeDefinitionMetadata ResourceRequest_t93_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsyncOperation_t2_0_0_0/* parent */
	, ResourceRequest_t93_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 615/* fieldStart */
	, 480/* methodStart */
	, -1/* eventStart */
	, 98/* propertyStart */

};
TypeInfo ResourceRequest_t93_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResourceRequest"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ResourceRequest_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ResourceRequest_t93_0_0_0/* byval_arg */
	, &ResourceRequest_t93_1_0_0/* this_arg */
	, &ResourceRequest_t93_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResourceRequest_t93)/* instance_size */
	, sizeof (ResourceRequest_t93)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Resources
#include "UnityEngine_UnityEngine_Resources.h"
// Metadata Definition UnityEngine.Resources
extern TypeInfo Resources_t94_il2cpp_TypeInfo;
// UnityEngine.Resources
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
static const EncodedMethodIndex Resources_t94_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Resources_t94_0_0_0;
extern const Il2CppType Resources_t94_1_0_0;
struct Resources_t94;
const Il2CppTypeDefinitionMetadata Resources_t94_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Resources_t94_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 482/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Resources_t94_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Resources"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Resources_t94_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Resources_t94_0_0_0/* byval_arg */
	, &Resources_t94_1_0_0/* this_arg */
	, &Resources_t94_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Resources_t94)/* instance_size */
	, sizeof (Resources_t94)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SerializePrivateVariables
#include "UnityEngine_UnityEngine_SerializePrivateVariables.h"
// Metadata Definition UnityEngine.SerializePrivateVariables
extern TypeInfo SerializePrivateVariables_t95_il2cpp_TypeInfo;
// UnityEngine.SerializePrivateVariables
#include "UnityEngine_UnityEngine_SerializePrivateVariablesMethodDeclarations.h"
static const EncodedMethodIndex SerializePrivateVariables_t95_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
extern const Il2CppType _Attribute_t2052_0_0_0;
static Il2CppInterfaceOffsetPair SerializePrivateVariables_t95_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SerializePrivateVariables_t95_0_0_0;
extern const Il2CppType SerializePrivateVariables_t95_1_0_0;
extern const Il2CppType Attribute_t96_0_0_0;
struct SerializePrivateVariables_t95;
const Il2CppTypeDefinitionMetadata SerializePrivateVariables_t95_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SerializePrivateVariables_t95_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, SerializePrivateVariables_t95_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 483/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SerializePrivateVariables_t95_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializePrivateVariables"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SerializePrivateVariables_t95_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 146/* custom_attributes_cache */
	, &SerializePrivateVariables_t95_0_0_0/* byval_arg */
	, &SerializePrivateVariables_t95_1_0_0/* this_arg */
	, &SerializePrivateVariables_t95_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializePrivateVariables_t95)/* instance_size */
	, sizeof (SerializePrivateVariables_t95)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// Metadata Definition UnityEngine.SerializeField
extern TypeInfo SerializeField_t97_il2cpp_TypeInfo;
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
static const EncodedMethodIndex SerializeField_t97_VTable[4] = 
{
	253,
	125,
	254,
	123,
};
static Il2CppInterfaceOffsetPair SerializeField_t97_InterfacesOffsets[] = 
{
	{ &_Attribute_t2052_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SerializeField_t97_0_0_0;
extern const Il2CppType SerializeField_t97_1_0_0;
struct SerializeField_t97;
const Il2CppTypeDefinitionMetadata SerializeField_t97_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SerializeField_t97_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t96_0_0_0/* parent */
	, SerializeField_t97_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 484/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SerializeField_t97_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializeField"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SerializeField_t97_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SerializeField_t97_0_0_0/* byval_arg */
	, &SerializeField_t97_1_0_0/* this_arg */
	, &SerializeField_t97_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializeField_t97)/* instance_size */
	, sizeof (SerializeField_t97)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.ISerializationCallbackReceiver
extern TypeInfo ISerializationCallbackReceiver_t2037_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ISerializationCallbackReceiver_t2037_0_0_0;
extern const Il2CppType ISerializationCallbackReceiver_t2037_1_0_0;
struct ISerializationCallbackReceiver_t2037;
const Il2CppTypeDefinitionMetadata ISerializationCallbackReceiver_t2037_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 485/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISerializationCallbackReceiver_t2037_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializationCallbackReceiver"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ISerializationCallbackReceiver_t2037_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISerializationCallbackReceiver_t2037_0_0_0/* byval_arg */
	, &ISerializationCallbackReceiver_t2037_1_0_0/* this_arg */
	, &ISerializationCallbackReceiver_t2037_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
// Metadata Definition UnityEngine.Rendering.SphericalHarmonicsL2
extern TypeInfo SphericalHarmonicsL2_t98_il2cpp_TypeInfo;
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2MethodDeclarations.h"
static const EncodedMethodIndex SphericalHarmonicsL2_t98_VTable[4] = 
{
	255,
	125,
	256,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SphericalHarmonicsL2_t98_0_0_0;
extern const Il2CppType SphericalHarmonicsL2_t98_1_0_0;
const Il2CppTypeDefinitionMetadata SphericalHarmonicsL2_t98_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, SphericalHarmonicsL2_t98_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 617/* fieldStart */
	, 487/* methodStart */
	, -1/* eventStart */
	, 99/* propertyStart */

};
TypeInfo SphericalHarmonicsL2_t98_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SphericalHarmonicsL2"/* name */
	, "UnityEngine.Rendering"/* namespaze */
	, NULL/* methods */
	, &SphericalHarmonicsL2_t98_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 147/* custom_attributes_cache */
	, &SphericalHarmonicsL2_t98_0_0_0/* byval_arg */
	, &SphericalHarmonicsL2_t98_1_0_0/* this_arg */
	, &SphericalHarmonicsL2_t98_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SphericalHarmonicsL2_t98)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SphericalHarmonicsL2_t98)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(SphericalHarmonicsL2_t98 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 18/* method_count */
	, 1/* property_count */
	, 27/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWW.h"
// Metadata Definition UnityEngine.WWW
extern TypeInfo WWW_t99_il2cpp_TypeInfo;
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"
static const EncodedMethodIndex WWW_t99_VTable[5] = 
{
	120,
	257,
	122,
	123,
	258,
};
extern const Il2CppType IDisposable_t319_0_0_0;
static const Il2CppType* WWW_t99_InterfacesTypeInfos[] = 
{
	&IDisposable_t319_0_0_0,
};
static Il2CppInterfaceOffsetPair WWW_t99_InterfacesOffsets[] = 
{
	{ &IDisposable_t319_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WWW_t99_0_0_0;
extern const Il2CppType WWW_t99_1_0_0;
struct WWW_t99;
const Il2CppTypeDefinitionMetadata WWW_t99_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WWW_t99_InterfacesTypeInfos/* implementedInterfaces */
	, WWW_t99_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WWW_t99_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 505/* methodStart */
	, -1/* eventStart */
	, 100/* propertyStart */

};
TypeInfo WWW_t99_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WWW"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WWW_t99_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WWW_t99_0_0_0/* byval_arg */
	, &WWW_t99_1_0_0/* this_arg */
	, &WWW_t99_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WWW_t99)/* instance_size */
	, sizeof (WWW_t99)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.WWWForm
#include "UnityEngine_UnityEngine_WWWForm.h"
// Metadata Definition UnityEngine.WWWForm
extern TypeInfo WWWForm_t103_il2cpp_TypeInfo;
// UnityEngine.WWWForm
#include "UnityEngine_UnityEngine_WWWFormMethodDeclarations.h"
static const EncodedMethodIndex WWWForm_t103_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WWWForm_t103_0_0_0;
extern const Il2CppType WWWForm_t103_1_0_0;
struct WWWForm_t103;
const Il2CppTypeDefinitionMetadata WWWForm_t103_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WWWForm_t103_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 644/* fieldStart */
	, 520/* methodStart */
	, -1/* eventStart */
	, 107/* propertyStart */

};
TypeInfo WWWForm_t103_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WWWForm"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WWWForm_t103_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WWWForm_t103_0_0_0/* byval_arg */
	, &WWWForm_t103_1_0_0/* this_arg */
	, &WWWForm_t103_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WWWForm_t103)/* instance_size */
	, sizeof (WWWForm_t103)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WWWTranscoder
#include "UnityEngine_UnityEngine_WWWTranscoder.h"
// Metadata Definition UnityEngine.WWWTranscoder
extern TypeInfo WWWTranscoder_t104_il2cpp_TypeInfo;
// UnityEngine.WWWTranscoder
#include "UnityEngine_UnityEngine_WWWTranscoderMethodDeclarations.h"
static const EncodedMethodIndex WWWTranscoder_t104_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WWWTranscoder_t104_0_0_0;
extern const Il2CppType WWWTranscoder_t104_1_0_0;
struct WWWTranscoder_t104;
const Il2CppTypeDefinitionMetadata WWWTranscoder_t104_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WWWTranscoder_t104_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 650/* fieldStart */
	, 526/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WWWTranscoder_t104_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WWWTranscoder"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WWWTranscoder_t104_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WWWTranscoder_t104_0_0_0/* byval_arg */
	, &WWWTranscoder_t104_1_0_0/* this_arg */
	, &WWWTranscoder_t104_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WWWTranscoder_t104)/* instance_size */
	, sizeof (WWWTranscoder_t104)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WWWTranscoder_t104_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CacheIndex
#include "UnityEngine_UnityEngine_CacheIndex.h"
// Metadata Definition UnityEngine.CacheIndex
extern TypeInfo CacheIndex_t105_il2cpp_TypeInfo;
// UnityEngine.CacheIndex
#include "UnityEngine_UnityEngine_CacheIndexMethodDeclarations.h"
static const EncodedMethodIndex CacheIndex_t105_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CacheIndex_t105_0_0_0;
extern const Il2CppType CacheIndex_t105_1_0_0;
const Il2CppTypeDefinitionMetadata CacheIndex_t105_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, CacheIndex_t105_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 658/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CacheIndex_t105_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CacheIndex"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &CacheIndex_t105_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 161/* custom_attributes_cache */
	, &CacheIndex_t105_0_0_0/* byval_arg */
	, &CacheIndex_t105_1_0_0/* this_arg */
	, &CacheIndex_t105_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)CacheIndex_t105_marshal/* marshal_to_native_func */
	, (methodPointerType)CacheIndex_t105_marshal_back/* marshal_from_native_func */
	, (methodPointerType)CacheIndex_t105_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (CacheIndex_t105)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CacheIndex_t105)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(CacheIndex_t105_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityString.h"
// Metadata Definition UnityEngine.UnityString
extern TypeInfo UnityString_t106_il2cpp_TypeInfo;
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
static const EncodedMethodIndex UnityString_t106_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityString_t106_0_0_0;
extern const Il2CppType UnityString_t106_1_0_0;
struct UnityString_t106;
const Il2CppTypeDefinitionMetadata UnityString_t106_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnityString_t106_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 534/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityString_t106_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityString"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &UnityString_t106_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityString_t106_0_0_0/* byval_arg */
	, &UnityString_t106_1_0_0/* this_arg */
	, &UnityString_t106_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityString_t106)/* instance_size */
	, sizeof (UnityString_t106)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AsyncOperation
#include "UnityEngine_UnityEngine_AsyncOperation.h"
// Metadata Definition UnityEngine.AsyncOperation
extern TypeInfo AsyncOperation_t2_il2cpp_TypeInfo;
// UnityEngine.AsyncOperation
#include "UnityEngine_UnityEngine_AsyncOperationMethodDeclarations.h"
static const EncodedMethodIndex AsyncOperation_t2_VTable[4] = 
{
	120,
	121,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AsyncOperation_t2_1_0_0;
struct AsyncOperation_t2;
const Il2CppTypeDefinitionMetadata AsyncOperation_t2_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &YieldInstruction_t11_0_0_0/* parent */
	, AsyncOperation_t2_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 661/* fieldStart */
	, 535/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AsyncOperation_t2_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsyncOperation"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AsyncOperation_t2_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AsyncOperation_t2_0_0_0/* byval_arg */
	, &AsyncOperation_t2_1_0_0/* this_arg */
	, &AsyncOperation_t2_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)AsyncOperation_t2_marshal/* marshal_to_native_func */
	, (methodPointerType)AsyncOperation_t2_marshal_back/* marshal_from_native_func */
	, (methodPointerType)AsyncOperation_t2_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (AsyncOperation_t2)/* instance_size */
	, sizeof (AsyncOperation_t2)/* actualSize */
	, 0/* element_size */
	, sizeof(AsyncOperation_t2_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Application
#include "UnityEngine_UnityEngine_Application.h"
// Metadata Definition UnityEngine.Application
extern TypeInfo Application_t108_il2cpp_TypeInfo;
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
extern const Il2CppType LogCallback_t107_0_0_0;
static const Il2CppType* Application_t108_il2cpp_TypeInfo__nestedTypes[1] =
{
	&LogCallback_t107_0_0_0,
};
static const EncodedMethodIndex Application_t108_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Application_t108_0_0_0;
extern const Il2CppType Application_t108_1_0_0;
struct Application_t108;
const Il2CppTypeDefinitionMetadata Application_t108_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Application_t108_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Application_t108_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 662/* fieldStart */
	, 538/* methodStart */
	, -1/* eventStart */
	, 109/* propertyStart */

};
TypeInfo Application_t108_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Application"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Application_t108_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Application_t108_0_0_0/* byval_arg */
	, &Application_t108_1_0_0/* this_arg */
	, &Application_t108_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Application_t108)/* instance_size */
	, sizeof (Application_t108)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Application_t108_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Application/LogCallback
#include "UnityEngine_UnityEngine_Application_LogCallback.h"
// Metadata Definition UnityEngine.Application/LogCallback
extern TypeInfo LogCallback_t107_il2cpp_TypeInfo;
// UnityEngine.Application/LogCallback
#include "UnityEngine_UnityEngine_Application_LogCallbackMethodDeclarations.h"
static const EncodedMethodIndex LogCallback_t107_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	259,
	260,
	261,
};
static Il2CppInterfaceOffsetPair LogCallback_t107_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LogCallback_t107_1_0_0;
struct LogCallback_t107;
const Il2CppTypeDefinitionMetadata LogCallback_t107_DefinitionMetadata = 
{
	&Application_t108_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LogCallback_t107_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, LogCallback_t107_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 540/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LogCallback_t107_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogCallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &LogCallback_t107_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LogCallback_t107_0_0_0/* byval_arg */
	, &LogCallback_t107_1_0_0/* this_arg */
	, &LogCallback_t107_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_LogCallback_t107/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LogCallback_t107)/* instance_size */
	, sizeof (LogCallback_t107)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// Metadata Definition UnityEngine.Behaviour
extern TypeInfo Behaviour_t34_il2cpp_TypeInfo;
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
static const EncodedMethodIndex Behaviour_t34_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Behaviour_t34_1_0_0;
extern const Il2CppType Component_t109_0_0_0;
struct Behaviour_t34;
const Il2CppTypeDefinitionMetadata Behaviour_t34_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Component_t109_0_0_0/* parent */
	, Behaviour_t34_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 544/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Behaviour_t34_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Behaviour"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Behaviour_t34_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Behaviour_t34_0_0_0/* byval_arg */
	, &Behaviour_t34_1_0_0/* this_arg */
	, &Behaviour_t34_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Behaviour_t34)/* instance_size */
	, sizeof (Behaviour_t34)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// Metadata Definition UnityEngine.Camera
extern TypeInfo Camera_t110_il2cpp_TypeInfo;
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
extern const Il2CppType CameraCallback_t111_0_0_0;
static const Il2CppType* Camera_t110_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CameraCallback_t111_0_0_0,
};
static const EncodedMethodIndex Camera_t110_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Camera_t110_0_0_0;
extern const Il2CppType Camera_t110_1_0_0;
struct Camera_t110;
const Il2CppTypeDefinitionMetadata Camera_t110_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Camera_t110_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t34_0_0_0/* parent */
	, Camera_t110_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 664/* fieldStart */
	, 545/* methodStart */
	, -1/* eventStart */
	, 110/* propertyStart */

};
TypeInfo Camera_t110_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Camera"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Camera_t110_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Camera_t110_0_0_0/* byval_arg */
	, &Camera_t110_1_0_0/* this_arg */
	, &Camera_t110_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Camera_t110)/* instance_size */
	, sizeof (Camera_t110)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Camera_t110_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 8/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Camera/CameraCallback
#include "UnityEngine_UnityEngine_Camera_CameraCallback.h"
// Metadata Definition UnityEngine.Camera/CameraCallback
extern TypeInfo CameraCallback_t111_il2cpp_TypeInfo;
// UnityEngine.Camera/CameraCallback
#include "UnityEngine_UnityEngine_Camera_CameraCallbackMethodDeclarations.h"
static const EncodedMethodIndex CameraCallback_t111_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	262,
	263,
	264,
};
static Il2CppInterfaceOffsetPair CameraCallback_t111_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CameraCallback_t111_1_0_0;
struct CameraCallback_t111;
const Il2CppTypeDefinitionMetadata CameraCallback_t111_DefinitionMetadata = 
{
	&Camera_t110_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CameraCallback_t111_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, CameraCallback_t111_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 564/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CameraCallback_t111_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraCallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraCallback_t111_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 177/* custom_attributes_cache */
	, &CameraCallback_t111_0_0_0/* byval_arg */
	, &CameraCallback_t111_1_0_0/* this_arg */
	, &CameraCallback_t111_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CameraCallback_t111/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraCallback_t111)/* instance_size */
	, sizeof (CameraCallback_t111)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_Debug.h"
// Metadata Definition UnityEngine.Debug
extern TypeInfo Debug_t112_il2cpp_TypeInfo;
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
static const EncodedMethodIndex Debug_t112_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Debug_t112_0_0_0;
extern const Il2CppType Debug_t112_1_0_0;
struct Debug_t112;
const Il2CppTypeDefinitionMetadata Debug_t112_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Debug_t112_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 568/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Debug_t112_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Debug"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Debug_t112_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Debug_t112_0_0_0/* byval_arg */
	, &Debug_t112_1_0_0/* this_arg */
	, &Debug_t112_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Debug_t112)/* instance_size */
	, sizeof (Debug_t112)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Display
#include "UnityEngine_UnityEngine_Display.h"
// Metadata Definition UnityEngine.Display
extern TypeInfo Display_t115_il2cpp_TypeInfo;
// UnityEngine.Display
#include "UnityEngine_UnityEngine_DisplayMethodDeclarations.h"
extern const Il2CppType DisplaysUpdatedDelegate_t113_0_0_0;
static const Il2CppType* Display_t115_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DisplaysUpdatedDelegate_t113_0_0_0,
};
static const EncodedMethodIndex Display_t115_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Display_t115_0_0_0;
extern const Il2CppType Display_t115_1_0_0;
struct Display_t115;
const Il2CppTypeDefinitionMetadata Display_t115_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Display_t115_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Display_t115_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 667/* fieldStart */
	, 572/* methodStart */
	, 0/* eventStart */
	, 118/* propertyStart */

};
TypeInfo Display_t115_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Display"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Display_t115_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Display_t115_0_0_0/* byval_arg */
	, &Display_t115_1_0_0/* this_arg */
	, &Display_t115_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Display_t115)/* instance_size */
	, sizeof (Display_t115)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Display_t115_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 7/* property_count */
	, 4/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Display/DisplaysUpdatedDelegate
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegate.h"
// Metadata Definition UnityEngine.Display/DisplaysUpdatedDelegate
extern TypeInfo DisplaysUpdatedDelegate_t113_il2cpp_TypeInfo;
// UnityEngine.Display/DisplaysUpdatedDelegate
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegateMethodDeclarations.h"
static const EncodedMethodIndex DisplaysUpdatedDelegate_t113_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	265,
	266,
	267,
};
static Il2CppInterfaceOffsetPair DisplaysUpdatedDelegate_t113_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DisplaysUpdatedDelegate_t113_1_0_0;
struct DisplaysUpdatedDelegate_t113;
const Il2CppTypeDefinitionMetadata DisplaysUpdatedDelegate_t113_DefinitionMetadata = 
{
	&Display_t115_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DisplaysUpdatedDelegate_t113_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, DisplaysUpdatedDelegate_t113_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 600/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DisplaysUpdatedDelegate_t113_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DisplaysUpdatedDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DisplaysUpdatedDelegate_t113_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DisplaysUpdatedDelegate_t113_0_0_0/* byval_arg */
	, &DisplaysUpdatedDelegate_t113_1_0_0/* this_arg */
	, &DisplaysUpdatedDelegate_t113_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t113/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DisplaysUpdatedDelegate_t113)/* instance_size */
	, sizeof (DisplaysUpdatedDelegate_t113)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Metadata Definition UnityEngine.MonoBehaviour
extern TypeInfo MonoBehaviour_t116_il2cpp_TypeInfo;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
static const EncodedMethodIndex MonoBehaviour_t116_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MonoBehaviour_t116_0_0_0;
extern const Il2CppType MonoBehaviour_t116_1_0_0;
struct MonoBehaviour_t116;
const Il2CppTypeDefinitionMetadata MonoBehaviour_t116_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t34_0_0_0/* parent */
	, MonoBehaviour_t116_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 604/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoBehaviour_t116_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoBehaviour"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &MonoBehaviour_t116_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoBehaviour_t116_0_0_0/* byval_arg */
	, &MonoBehaviour_t116_1_0_0/* this_arg */
	, &MonoBehaviour_t116_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoBehaviour_t116)/* instance_size */
	, sizeof (MonoBehaviour_t116)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Input
#include "UnityEngine_UnityEngine_Input.h"
// Metadata Definition UnityEngine.Input
extern TypeInfo Input_t117_il2cpp_TypeInfo;
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
static const EncodedMethodIndex Input_t117_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Input_t117_0_0_0;
extern const Il2CppType Input_t117_1_0_0;
struct Input_t117;
const Il2CppTypeDefinitionMetadata Input_t117_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Input_t117_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 607/* methodStart */
	, -1/* eventStart */
	, 125/* propertyStart */

};
TypeInfo Input_t117_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Input"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Input_t117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Input_t117_0_0_0/* byval_arg */
	, &Input_t117_1_0_0/* this_arg */
	, &Input_t117_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Input_t117)/* instance_size */
	, sizeof (Input_t117)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// Metadata Definition UnityEngine.Object
extern TypeInfo Object_t5_il2cpp_TypeInfo;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
static const EncodedMethodIndex Object_t5_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Object_t5_1_0_0;
struct Object_t5;
const Il2CppTypeDefinitionMetadata Object_t5_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Object_t5_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 671/* fieldStart */
	, 612/* methodStart */
	, -1/* eventStart */
	, 126/* propertyStart */

};
TypeInfo Object_t5_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Object"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Object_t5_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Object_t5_0_0_0/* byval_arg */
	, &Object_t5_1_0_0/* this_arg */
	, &Object_t5_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Object_t5_marshal/* marshal_to_native_func */
	, (methodPointerType)Object_t5_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Object_t5_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Object_t5)/* instance_size */
	, sizeof (Object_t5)/* actualSize */
	, 0/* element_size */
	, sizeof(Object_t5_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
// Metadata Definition UnityEngine.Component
extern TypeInfo Component_t109_il2cpp_TypeInfo;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
extern const Il2CppType Component_GetComponent_m12756_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Component_GetComponent_m12756_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 2340 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex Component_t109_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Component_t109_1_0_0;
struct Component_t109;
const Il2CppTypeDefinitionMetadata Component_t109_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t5_0_0_0/* parent */
	, Component_t109_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 624/* methodStart */
	, -1/* eventStart */
	, 127/* propertyStart */

};
TypeInfo Component_t109_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Component"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Component_t109_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Component_t109_0_0_0/* byval_arg */
	, &Component_t109_1_0_0/* this_arg */
	, &Component_t109_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Component_t109)/* instance_size */
	, sizeof (Component_t109)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// Metadata Definition UnityEngine.GameObject
extern TypeInfo GameObject_t118_il2cpp_TypeInfo;
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
static const EncodedMethodIndex GameObject_t118_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GameObject_t118_0_0_0;
extern const Il2CppType GameObject_t118_1_0_0;
struct GameObject_t118;
const Il2CppTypeDefinitionMetadata GameObject_t118_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t5_0_0_0/* parent */
	, GameObject_t118_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 628/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GameObject_t118_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GameObject"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GameObject_t118_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GameObject_t118_0_0_0/* byval_arg */
	, &GameObject_t118_1_0_0/* this_arg */
	, &GameObject_t118_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GameObject_t118)/* instance_size */
	, sizeof (GameObject_t118)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// Metadata Definition UnityEngine.Transform
extern TypeInfo Transform_t92_il2cpp_TypeInfo;
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern const Il2CppType Enumerator_t119_0_0_0;
static const Il2CppType* Transform_t92_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t119_0_0_0,
};
static const EncodedMethodIndex Transform_t92_VTable[5] = 
{
	124,
	125,
	126,
	127,
	249,
};
static const Il2CppType* Transform_t92_InterfacesTypeInfos[] = 
{
	&IEnumerable_t302_0_0_0,
};
static Il2CppInterfaceOffsetPair Transform_t92_InterfacesOffsets[] = 
{
	{ &IEnumerable_t302_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Transform_t92_1_0_0;
struct Transform_t92;
const Il2CppTypeDefinitionMetadata Transform_t92_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Transform_t92_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Transform_t92_InterfacesTypeInfos/* implementedInterfaces */
	, Transform_t92_InterfacesOffsets/* interfaceOffsets */
	, &Component_t109_0_0_0/* parent */
	, Transform_t92_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 629/* methodStart */
	, -1/* eventStart */
	, 128/* propertyStart */

};
TypeInfo Transform_t92_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Transform"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Transform_t92_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Transform_t92_0_0_0/* byval_arg */
	, &Transform_t92_1_0_0/* this_arg */
	, &Transform_t92_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Transform_t92)/* instance_size */
	, sizeof (Transform_t92)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Transform/Enumerator
#include "UnityEngine_UnityEngine_Transform_Enumerator.h"
// Metadata Definition UnityEngine.Transform/Enumerator
extern TypeInfo Enumerator_t119_il2cpp_TypeInfo;
// UnityEngine.Transform/Enumerator
#include "UnityEngine_UnityEngine_Transform_EnumeratorMethodDeclarations.h"
static const EncodedMethodIndex Enumerator_t119_VTable[6] = 
{
	120,
	125,
	122,
	123,
	268,
	269,
};
extern const Il2CppType IEnumerator_t279_0_0_0;
static const Il2CppType* Enumerator_t119_InterfacesTypeInfos[] = 
{
	&IEnumerator_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t119_InterfacesOffsets[] = 
{
	{ &IEnumerator_t279_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Enumerator_t119_1_0_0;
struct Enumerator_t119;
const Il2CppTypeDefinitionMetadata Enumerator_t119_DefinitionMetadata = 
{
	&Transform_t92_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t119_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t119_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t119_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 673/* fieldStart */
	, 632/* methodStart */
	, -1/* eventStart */
	, 129/* propertyStart */

};
TypeInfo Enumerator_t119_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t119_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t119_0_0_0/* byval_arg */
	, &Enumerator_t119_1_0_0/* this_arg */
	, &Enumerator_t119_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t119)/* instance_size */
	, sizeof (Enumerator_t119)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Random
#include "UnityEngine_UnityEngine_Random.h"
// Metadata Definition UnityEngine.Random
extern TypeInfo Random_t120_il2cpp_TypeInfo;
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
static const EncodedMethodIndex Random_t120_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Random_t120_0_0_0;
extern const Il2CppType Random_t120_1_0_0;
struct Random_t120;
const Il2CppTypeDefinitionMetadata Random_t120_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Random_t120_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 635/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Random_t120_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Random"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Random_t120_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Random_t120_0_0_0/* byval_arg */
	, &Random_t120_1_0_0/* this_arg */
	, &Random_t120_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Random_t120)/* instance_size */
	, sizeof (Random_t120)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstruction.h"
// Metadata Definition UnityEngine.YieldInstruction
extern TypeInfo YieldInstruction_t11_il2cpp_TypeInfo;
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstructionMethodDeclarations.h"
static const EncodedMethodIndex YieldInstruction_t11_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType YieldInstruction_t11_1_0_0;
struct YieldInstruction_t11;
const Il2CppTypeDefinitionMetadata YieldInstruction_t11_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, YieldInstruction_t11_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 637/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo YieldInstruction_t11_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "YieldInstruction"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &YieldInstruction_t11_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &YieldInstruction_t11_0_0_0/* byval_arg */
	, &YieldInstruction_t11_1_0_0/* this_arg */
	, &YieldInstruction_t11_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)YieldInstruction_t11_marshal/* marshal_to_native_func */
	, (methodPointerType)YieldInstruction_t11_marshal_back/* marshal_from_native_func */
	, (methodPointerType)YieldInstruction_t11_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (YieldInstruction_t11)/* instance_size */
	, sizeof (YieldInstruction_t11)/* actualSize */
	, 0/* element_size */
	, sizeof(YieldInstruction_t11_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefs.h"
// Metadata Definition UnityEngine.PlayerPrefs
extern TypeInfo PlayerPrefs_t121_il2cpp_TypeInfo;
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
static const EncodedMethodIndex PlayerPrefs_t121_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PlayerPrefs_t121_0_0_0;
extern const Il2CppType PlayerPrefs_t121_1_0_0;
struct PlayerPrefs_t121;
const Il2CppTypeDefinitionMetadata PlayerPrefs_t121_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PlayerPrefs_t121_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 638/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PlayerPrefs_t121_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlayerPrefs"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &PlayerPrefs_t121_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PlayerPrefs_t121_0_0_0/* byval_arg */
	, &PlayerPrefs_t121_1_0_0/* this_arg */
	, &PlayerPrefs_t121_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlayerPrefs_t121)/* instance_size */
	, sizeof (PlayerPrefs_t121)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Particle
#include "UnityEngine_UnityEngine_Particle.h"
// Metadata Definition UnityEngine.Particle
extern TypeInfo Particle_t122_il2cpp_TypeInfo;
// UnityEngine.Particle
#include "UnityEngine_UnityEngine_ParticleMethodDeclarations.h"
static const EncodedMethodIndex Particle_t122_VTable[4] = 
{
	150,
	125,
	151,
	152,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Particle_t122_0_0_0;
extern const Il2CppType Particle_t122_1_0_0;
const Il2CppTypeDefinitionMetadata Particle_t122_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t757_0_0_0/* parent */
	, Particle_t122_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 675/* fieldStart */
	, 640/* methodStart */
	, -1/* eventStart */
	, 130/* propertyStart */

};
TypeInfo Particle_t122_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Particle"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Particle_t122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Particle_t122_0_0_0/* byval_arg */
	, &Particle_t122_1_0_0/* this_arg */
	, &Particle_t122_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Particle_t122)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Particle_t122)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Particle_t122 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 16/* method_count */
	, 8/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AudioSettings
#include "UnityEngine_UnityEngine_AudioSettings.h"
// Metadata Definition UnityEngine.AudioSettings
extern TypeInfo AudioSettings_t124_il2cpp_TypeInfo;
// UnityEngine.AudioSettings
#include "UnityEngine_UnityEngine_AudioSettingsMethodDeclarations.h"
extern const Il2CppType AudioConfigurationChangeHandler_t123_0_0_0;
static const Il2CppType* AudioSettings_t124_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AudioConfigurationChangeHandler_t123_0_0_0,
};
static const EncodedMethodIndex AudioSettings_t124_VTable[4] = 
{
	120,
	125,
	122,
	123,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AudioSettings_t124_0_0_0;
extern const Il2CppType AudioSettings_t124_1_0_0;
struct AudioSettings_t124;
const Il2CppTypeDefinitionMetadata AudioSettings_t124_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AudioSettings_t124_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AudioSettings_t124_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 683/* fieldStart */
	, 656/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AudioSettings_t124_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AudioSettings"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AudioSettings_t124_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AudioSettings_t124_0_0_0/* byval_arg */
	, &AudioSettings_t124_1_0_0/* this_arg */
	, &AudioSettings_t124_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AudioSettings_t124)/* instance_size */
	, sizeof (AudioSettings_t124)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AudioSettings_t124_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChan.h"
// Metadata Definition UnityEngine.AudioSettings/AudioConfigurationChangeHandler
extern TypeInfo AudioConfigurationChangeHandler_t123_il2cpp_TypeInfo;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChanMethodDeclarations.h"
static const EncodedMethodIndex AudioConfigurationChangeHandler_t123_VTable[13] = 
{
	173,
	125,
	174,
	123,
	175,
	176,
	175,
	177,
	178,
	179,
	270,
	271,
	272,
};
static Il2CppInterfaceOffsetPair AudioConfigurationChangeHandler_t123_InterfacesOffsets[] = 
{
	{ &ICloneable_t2056_0_0_0, 4},
	{ &ISerializable_t1426_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AudioConfigurationChangeHandler_t123_1_0_0;
struct AudioConfigurationChangeHandler_t123;
const Il2CppTypeDefinitionMetadata AudioConfigurationChangeHandler_t123_DefinitionMetadata = 
{
	&AudioSettings_t124_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AudioConfigurationChangeHandler_t123_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t47_0_0_0/* parent */
	, AudioConfigurationChangeHandler_t123_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 657/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AudioConfigurationChangeHandler_t123_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AudioConfigurationChangeHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &AudioConfigurationChangeHandler_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AudioConfigurationChangeHandler_t123_0_0_0/* byval_arg */
	, &AudioConfigurationChangeHandler_t123_1_0_0/* this_arg */
	, &AudioConfigurationChangeHandler_t123_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t123/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AudioConfigurationChangeHandler_t123)/* instance_size */
	, sizeof (AudioConfigurationChangeHandler_t123)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClip.h"
// Metadata Definition UnityEngine.AudioClip
extern TypeInfo AudioClip_t128_il2cpp_TypeInfo;
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClipMethodDeclarations.h"
extern const Il2CppType PCMReaderCallback_t126_0_0_0;
extern const Il2CppType PCMSetPositionCallback_t127_0_0_0;
static const Il2CppType* AudioClip_t128_il2cpp_TypeInfo__nestedTypes[2] =
{
	&PCMReaderCallback_t126_0_0_0,
	&PCMSetPositionCallback_t127_0_0_0,
};
static const EncodedMethodIndex AudioClip_t128_VTable[4] = 
{
	124,
	125,
	126,
	127,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AudioClip_t128_0_0_0;
extern const Il2CppType AudioClip_t128_1_0_0;
struct AudioClip_t128;
const Il2CppTypeDefinitionMetadata AudioClip_t128_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AudioClip_t128_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t5_0_0_0/* parent */
	, AudioClip_t128_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 684/* fieldStart */
	, 661/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AudioClip_t128_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AudioClip"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AudioClip_t128_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AudioClip_t128_0_0_0/* byval_arg */
	, &AudioClip_t128_1_0_0/* this_arg */
	, &AudioClip_t128_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AudioClip_t128)/* instance_size */
	, sizeof (AudioClip_t128)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
