﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>
struct Enumerator_t329;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t280;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"
#define Enumerator__ctor_m9300(__this, ___dictionary, method) (( void (*) (Enumerator_t329 *, Dictionary_2_t280 *, const MethodInfo*))Enumerator__ctor_m8893_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m9301(__this, method) (( Object_t * (*) (Enumerator_t329 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8894_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m9302(__this, method) (( DictionaryEntry_t560  (*) (Enumerator_t329 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8895_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m9303(__this, method) (( Object_t * (*) (Enumerator_t329 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8896_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m9304(__this, method) (( Object_t * (*) (Enumerator_t329 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8897_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::MoveNext()
#define Enumerator_MoveNext_m1214(__this, method) (( bool (*) (Enumerator_t329 *, const MethodInfo*))Enumerator_MoveNext_m8898_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_Current()
#define Enumerator_get_Current_m1210(__this, method) (( KeyValuePair_2_t328  (*) (Enumerator_t329 *, const MethodInfo*))Enumerator_get_Current_m8899_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m9305(__this, method) (( String_t* (*) (Enumerator_t329 *, const MethodInfo*))Enumerator_get_CurrentKey_m8900_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m9306(__this, method) (( String_t* (*) (Enumerator_t329 *, const MethodInfo*))Enumerator_get_CurrentValue_m8901_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::VerifyState()
#define Enumerator_VerifyState_m9307(__this, method) (( void (*) (Enumerator_t329 *, const MethodInfo*))Enumerator_VerifyState_m8902_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m9308(__this, method) (( void (*) (Enumerator_t329 *, const MethodInfo*))Enumerator_VerifyCurrent_m8903_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::Dispose()
#define Enumerator_Dispose_m9309(__this, method) (( void (*) (Enumerator_t329 *, const MethodInfo*))Enumerator_Dispose_m8904_gshared)(__this, method)
