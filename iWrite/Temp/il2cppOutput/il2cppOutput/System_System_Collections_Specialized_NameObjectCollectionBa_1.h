﻿#pragma once
#include <stdint.h>
// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t392;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct  KeysCollection_t394  : public Object_t
{
	// System.Collections.Specialized.NameObjectCollectionBase System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::m_collection
	NameObjectCollectionBase_t392 * ___m_collection_0;
};
