﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct GenericComparer_1_t1433;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericComparer_1__ctor_m8292_gshared (GenericComparer_1_t1433 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m8292(__this, method) (( void (*) (GenericComparer_1_t1433 *, const MethodInfo*))GenericComparer_1__ctor_m8292_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m12030_gshared (GenericComparer_1_t1433 * __this, DateTimeOffset_t363  ___x, DateTimeOffset_t363  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m12030(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t1433 *, DateTimeOffset_t363 , DateTimeOffset_t363 , const MethodInfo*))GenericComparer_1_Compare_m12030_gshared)(__this, ___x, ___y, method)
