﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t580;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t1346;
// System.IAsyncResult
struct IAsyncResult_t44;
// System.AsyncCallback
struct AsyncCallback_t45;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.ResolveEventHandler
struct  ResolveEventHandler_t1290  : public MulticastDelegate_t47
{
};
